﻿/*
 Pre-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be executed before the build script.	
 Use SQLCMD syntax to include a file in the pre-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the pre-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

INSERT INTO [dbo].[ZnodePaymentGateway]
(GatewayName ,WebsiteURL ,CreatedDate ,ModifiedDate  ,GatewayCode)
select GatewayName ,WebsiteURL ,CreatedDate ,ModifiedDate  ,GatewayCode
from (values ('Adyen' ,'https://www.adyen.com/',getdate(),getdate(),'adyen') ) a
(GatewayName,WebsiteURL,CreatedDate,ModifiedDate,GatewayCode)
where not exists (select Top 1 1 from ZnodePaymentGateway ZPG where ZPG.GatewayCode =a.GatewayCode and ZPG.GatewayName = a.GatewayName and a.WebsiteURL = zpg.WebsiteURL )
