﻿namespace Znode.Multifront.PaymentApplication.Models.Response
{
    public class AdyenResponse : BaseResponse
    {
        public AdyenDropinResponseModel ResponseModel { get; set; }
    }
}
