﻿namespace Znode.Multifront.PaymentApplication.Models
{
    public class AdyenDropinRequestModel
    {
        public string PaymentCode { get; set; }
        public bool IsTestMode { get; set; }

        public string RequestDomain { get; set; }
    }
}
