﻿namespace Znode.Multifront.PaymentApplication.Models
{
    public class AdyenModel
    {

        public string Type { get; set; }
        public string EncryptedCardNumber { get; set; }
        public string EncryptedExpiryMonth { get; set; }
        public string EncryptedExpiryYear { get; set; }
        public string EncryptedSecurityCode { get; set; }
        public string HolderName { get; set; }
    }
}
