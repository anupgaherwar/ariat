﻿namespace Znode.Multifront.PaymentApplication.Models
{
    public class AdyenDropinResponseModel
    {
        public bool IsSuccess { get; set; }
        public string PaymentMethods { get; set; }
        public string Environment { get; set; }
        public string OriginKey { get; set; }
    }
}
