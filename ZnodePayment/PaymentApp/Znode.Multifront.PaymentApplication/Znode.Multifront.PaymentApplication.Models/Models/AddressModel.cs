﻿namespace Znode.Multifront.PaymentApplication.Models
{
    public class AddressModel
    {
        public string StreetAddress1 { get; set; }
        public string StreetAddress2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }

        public string Email { get; set; }
    }
}
