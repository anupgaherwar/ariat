﻿using Adyen;
using Adyen.Model.Checkout;
using Adyen.Service;

using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Znode.Multifront.PaymentApplication.Models;

namespace Znode.Multifront.PaymentApplication.Data
{
    public class AdyenPaymentService
    {
        public AdyenDropinResponseModel GetDropinData(AdyenDropinRequestModel adyenDropinRequestModel)
        {
            PaymentSettingCredentialsService paymentrepository = new PaymentSettingCredentialsService();

            PaymentSettingCredentialsModel paymentSettingCredentials = paymentrepository.GetPaymentSettingCredentials(adyenDropinRequestModel.PaymentCode, adyenDropinRequestModel.IsTestMode);

            Adyen.Model.Enum.Environment TransactionMode = Adyen.Model.Enum.Environment.Test;
            Client client = new Client(paymentSettingCredentials.TransactionKey, TransactionMode);

            if (!paymentSettingCredentials.TestMode)
            {
                TransactionMode = Adyen.Model.Enum.Environment.Live;
                client = new Client(paymentSettingCredentials.TransactionKey, TransactionMode, paymentSettingCredentials.Partner);
            }

            Checkout checkout = new Checkout(client);


            PaymentMethodsRequest paymentMethodsRequest = new PaymentMethodsRequest(merchantAccount: paymentSettingCredentials.GatewayUsername)
            {
                Channel = PaymentMethodsRequest.ChannelEnum.Web
            };

            string paymentMethodsResponse = checkout.PaymentMethods(paymentMethodsRequest).ToJson();

            CheckoutUtility checkoutUtility = new CheckoutUtility(client);

            var originKeysRequest = new Adyen.Model.CheckoutUtility.OriginKeysRequest
            {
                OriginDomains = new List<string> { adyenDropinRequestModel.RequestDomain }
            };

            Adyen.Model.CheckoutUtility.OriginKeysResponse originKeysResponse = checkoutUtility.OriginKeys(originKeysRequest);
            string originKey = originKeysResponse.OriginKeys.FirstOrDefault().Value;

            AdyenDropinResponseModel responseModel = new AdyenDropinResponseModel();
            responseModel.OriginKey = originKey;
            responseModel.PaymentMethods = paymentMethodsResponse;
            responseModel.Environment = paymentSettingCredentials.TestMode ? Adyen.Model.Enum.Environment.Test.ToString() : Adyen.Model.Enum.Environment.Live.ToString();

            return responseModel;
        }
    }
}
