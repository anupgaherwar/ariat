﻿using Adyen;
using Adyen.Model.Checkout;
using Adyen.Model.Enum;
using Adyen.Service;

using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Web;

using Znode.Multifront.PaymentApplication.Data;
using Znode.Multifront.PaymentApplication.Helpers;
using Znode.Multifront.PaymentApplication.Models;

namespace Znode.Multifront.PaymentApplication.Providers
{
    public class AdyenProvider : BaseProvider, IPaymentProviders
    {
        /// <summary>
        /// Validate the credit card transaction number and returns status
        /// </summary>
        /// <param name="paymentModel"></param>
        /// <returns></returns>
        public GatewayResponseModel ValidateCreditcard(PaymentModel paymentModel)
        {
            GatewayResponseModel response = new GatewayResponseModel();
            try
            {
                return CreateTransaction(paymentModel);
            }
            catch (Exception ex)
            {
                Logging.LogMessage(ex, Logging.Components.Payment.ToString(), TraceLevel.Error);
                LoggingService.LogActivity(paymentModel.PaymentApplicationSettingId, ex.Message);

                response.IsSuccess = false;
                response.GatewayResponseData = ex.Message;
            }
            return response;
        }

        //create transaction
        private GatewayResponseModel CreateTransaction(PaymentModel paymentModel)
        {
            try
            {
                var shopperIPAddress = HttpContext.Current.Request.Headers["User-IpAddress"];


                GatewayResponseModel gatewayResponse = new GatewayResponseModel();

                PaymentSettingCredentialsService paymentrepository = new PaymentSettingCredentialsService();

                PaymentSettingCredentialsModel paymentSettingCredentials = paymentrepository.GetPaymentSettingCredentials(paymentModel.PaymentCode, paymentModel.GatewayTestMode);

                Adyen.Model.Enum.Environment TransactionMode = Adyen.Model.Enum.Environment.Test;
                Client client = new Client(paymentSettingCredentials.TransactionKey, TransactionMode);

                if (!paymentSettingCredentials.TestMode)
                {
                    TransactionMode = Adyen.Model.Enum.Environment.Live;
                    client = new Client(paymentSettingCredentials.TransactionKey, TransactionMode, paymentSettingCredentials.Partner);
                }

                Checkout checkout = new Checkout(client);

                Amount amount = new Amount(paymentModel.Custom3, 0);

                DefaultPaymentMethodDetails details = GetPaymentMethodDetails(paymentModel);

                PaymentRequest paymentRequest = CreatePaymentRequest(paymentModel, shopperIPAddress, paymentSettingCredentials, amount, details);

                PaymentsResponse paymentResponse = checkout.Payments(paymentRequest);
              
                Logging.LogMessage($"Response For Order Number : {paymentModel.OrderId} is {JsonConvert.SerializeObject(paymentResponse)}", Logging.Components.Payment.ToString(), TraceLevel.Error);
             
                if (string.IsNullOrEmpty(paymentResponse.RefusalReason))
                {
                    Dictionary<string, string> keyValuePairs = GetPaymentInformation(paymentResponse);
                    keyValuePairs.Add("name", details.HolderName);
                    gatewayResponse.IsSuccess = true;
                    gatewayResponse.TransactionId = paymentResponse.PspReference;
                    gatewayResponse.Token = JsonConvert.SerializeObject(keyValuePairs);
                    gatewayResponse.IsGatewayPreAuthorize = true;
                    gatewayResponse.Custom1 = JsonConvert.SerializeObject(keyValuePairs);
                }
                else
                {
                    gatewayResponse.IsSuccess = false;
                    gatewayResponse.ErrorMessage = $"{paymentResponse.RefusalReasonCode} : {paymentResponse.RefusalReason}";
                    gatewayResponse.ResponseCode = paymentResponse.RefusalReasonCode;
                    gatewayResponse.ResponseText = paymentResponse.RefusalReason;
                }

                return gatewayResponse;
            }
            catch (Exception ex)
            {
                Logging.LogMessage(ex, Logging.Components.Payment.ToString(), TraceLevel.Error);
                throw;
            }

        }

        private DefaultPaymentMethodDetails GetPaymentMethodDetails(PaymentModel paymentModel)
        {
            AdyenModel adyenModel = JsonConvert.DeserializeObject<AdyenModel>(paymentModel.Custom2);

            DefaultPaymentMethodDetails details = new DefaultPaymentMethodDetails
            {
                Type = adyenModel.Type,
                EncryptedCardNumber = adyenModel.EncryptedCardNumber,
                EncryptedExpiryMonth = adyenModel.EncryptedExpiryMonth,
                EncryptedExpiryYear = adyenModel.EncryptedExpiryYear,
                EncryptedSecurityCode = adyenModel.EncryptedSecurityCode,
                HolderName = adyenModel.HolderName,
            };
            return details;
        }

        private Address MapBillingAddress(PaymentModel paymentModel)
        {
            AddressModel addressModel = JsonConvert.DeserializeObject<AddressModel>(paymentModel.Custom1);

            if (addressModel != null)
            {
                var streetAdddress = $"{addressModel.StreetAddress1} {System.Environment.NewLine} {addressModel.StreetAddress1}";
                Address billingAdddress = new Address(addressModel.City, addressModel.Country, "", addressModel.PostalCode, addressModel.State, streetAdddress);
                return billingAdddress;
            }

            return null;
        }

        private Address MapShippingAddress(PaymentModel paymentModel)
        {
            var streetAdddress = $"{paymentModel.ShippingStreetAddress1} {System.Environment.NewLine} {paymentModel.ShippingStreetAddress2}";
            Address shippingAdddress = new Address(paymentModel.ShippingCity, paymentModel.ShippingCountryCode, "", paymentModel.ShippingPostalCode, paymentModel.ShippingStateCode, streetAdddress);
            return shippingAdddress;

        }
        private PaymentRequest CreatePaymentRequest(PaymentModel paymentModel, string shopperIPAddress, PaymentSettingCredentialsModel paymentSettingCredentials, Amount amount, DefaultPaymentMethodDetails details)
        {

            AddressModel addressModel = JsonConvert.DeserializeObject<AddressModel>(paymentModel.Custom1);

            PaymentRequest request = new PaymentRequest
            {

                Reference = paymentModel.OrderId,
                Amount = amount,
                MerchantAccount = paymentSettingCredentials.GatewayUsername,
                PaymentMethod = details,
                Recurring = new Adyen.Model.Checkout.Recurring(Adyen.Model.Checkout.Recurring.ContractEnum.RECURRING),
                ShopperEmail = addressModel?.Email,
                ShopperReference = paymentModel.OrderId,
                ShopperIP = shopperIPAddress,
                EnableRecurring = true,
                RecurringProcessingModel = (PaymentRequest.RecurringProcessingModelEnum?)RecurringProcessingModelEnum.UnscheduledCardOnFile
            };

            Address billingAddress = MapBillingAddress(paymentModel);
            if (billingAddress != null)
                request.BillingAddress = billingAddress;

            Address shippingAddress = MapShippingAddress(paymentModel);
            if (shippingAddress != null)
                request.DeliveryAddress = billingAddress;

            return request;

        }

        //Get Payment informtion from the response
        private Dictionary<string, string> GetPaymentInformation(PaymentsResponse paymentResponse)
        {
            Dictionary<string, string> keyValuePairs = new Dictionary<string, string>();

            var paymentAdditionalData = paymentResponse.AdditionalData;

            string cardExpiryDetails = paymentResponse.AdditionalData["expiryDate"];

            if (cardExpiryDetails != null)
            {
                var splitData = cardExpiryDetails.Split('/');
                var expiryMonth = splitData[0];
                keyValuePairs.Add("expirymonth", expiryMonth);
                var expiryYear = splitData[1];
                keyValuePairs.Add("expiryyear", expiryYear);
            }

            DateTime currentTimeDetails = DateTime.UtcNow;

            if (paymentAdditionalData.ContainsKey("recurring.recurringDetailReference"))
                keyValuePairs.Add("recurringpspreference", paymentAdditionalData["recurring.recurringDetailReference"]);
            if (paymentAdditionalData.ContainsKey("paymentMethod"))
                keyValuePairs.Add("cardtype", paymentResponse.AdditionalData["paymentMethod"]);
            if (paymentAdditionalData.ContainsKey("cardPaymentMethod"))
                keyValuePairs.Add("cardpaymentmethod", paymentAdditionalData["cardPaymentMethod"]);
           
            keyValuePairs.Add("authorizationdate", currentTimeDetails.ToShortDateString());
            keyValuePairs.Add("authorizationtime", currentTimeDetails.ToShortTimeString());
            keyValuePairs.Add("authorizationnumber", paymentResponse.PspReference);
            return keyValuePairs;
        }


        public GatewayResponseModel Void(PaymentModel paymentModel)
        {
            return null;
        }

        public GatewayResponseModel Subscription(PaymentModel paymentModel)
        {
            return null;
        }

        public TransactionDetailsModel GetTransactionDetails(PaymentModel paymentModel)
        {
            return null;
        }

        public GatewayResponseModel Refund(PaymentModel paymentModel)
        {
            return null;
        }
    }

}
