﻿using Renci.SshNet;
using Renci.SshNet.Sftp;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

using Znode.Engine.Api.Models;
using Znode.Libraries.Framework.Business;

namespace Znode.Engine.ERPConnector
{
    public static class SFTPHelper
    {
        //Upload File To SFTP Server
        public static bool UploadFileToSFTPServer(ERPConnectorControlListModel erpConnectorControlListModel, string folderName, string fileName, string LocalFilePath)
        {
            string host = GetConfigValue(erpConnectorControlListModel, SAPConnectorConstants.HostName);
            int portNumber = Convert.ToInt32(GetConfigValue(erpConnectorControlListModel, SAPConnectorConstants.PortNumber));
            string username = GetConfigValue(erpConnectorControlListModel, SAPConnectorConstants.UserName);
            string password = GetConfigValue(erpConnectorControlListModel, SAPConnectorConstants.Password);
            string remoteDirectoryPath = string.Concat(GetConfigValue(erpConnectorControlListModel, SAPConnectorConstants.FileDirectoryPath), folderName);

            bool isUpload = false;
            using (var sftpClient = new SftpClient(host, portNumber, username, password))
            {
                sftpClient.Connect();
                if (!sftpClient.Exists(remoteDirectoryPath))
                {
                    sftpClient.CreateDirectory(remoteDirectoryPath);
                }

                using (Stream file = new FileStream(LocalFilePath, FileMode.Open))
                {
                    sftpClient.UploadFile(file, $"{remoteDirectoryPath}/{fileName}");
                    isUpload = true;
                }
            }
            return isUpload;
        }

        //Get All Files From SFTP Server
        public static List<string> GetAllFilesFromSFTPServer(ERPConnectorControlListModel erpConnectorControlListModel, string folderName, string downloadFolderPath)
        {
            List<string> files = new List<string>();
            if (string.IsNullOrEmpty(folderName))
            {
                ZnodeLogging.LogMessage($"ZnodeSAPConnector.GetAllFilesFromSFTPServer : folderName is null or empty", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
                return files;
            }
            try
            {
                string host = GetConfigValue(erpConnectorControlListModel, SAPConnectorConstants.HostName);
                int portNumber = Convert.ToInt32(GetConfigValue(erpConnectorControlListModel, SAPConnectorConstants.PortNumber));
                string username = GetConfigValue(erpConnectorControlListModel, SAPConnectorConstants.UserName);
                string password = GetConfigValue(erpConnectorControlListModel, SAPConnectorConstants.Password);
                string remoteDirectoryPath = string.Concat(GetConfigValue(erpConnectorControlListModel, SAPConnectorConstants.FileDirectoryPath), folderName);

                if (string.IsNullOrEmpty(folderName) || string.IsNullOrEmpty(host) || string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                {
                    ZnodeLogging.LogMessage($"folderName: {folderName} or remoteDirectoryPath :{remoteDirectoryPath} or sftphostName:{host} or sftpusername:{username} or sftppassword:{password} not found.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
                    return files;
                }

                bool hasFiles = false;
                using (var sftpClient = new SftpClient(host, portNumber, username, password))
                {
                    sftpClient.Connect();
                    IEnumerable<SftpFile> sftpFiles = sftpClient.ListDirectory(remoteDirectoryPath)?.Where(x => !x.IsDirectory && !string.IsNullOrEmpty(x.Name) && x.Name.Contains(SAPConnectorConstants.CSVFileExtension));

                    ZnodeLogging.LogMessage($"sftpFiles count: {sftpFiles.Count()}", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);

                    if (sftpFiles.Count() > 0)
                    {
                        FileInfo fileInfo = new FileInfo(downloadFolderPath);
                        if (fileInfo.Directory.Exists)
                            fileInfo.Directory.Delete(true);
                        fileInfo.Directory.Create();
                    }
                    foreach (SftpFile file in sftpFiles)
                    {
                        string filePath = $"{downloadFolderPath}/{file.Name}";
                        hasFiles = true;
                        if (DownloadFilesFromSFTPServer(sftpClient, file, downloadFolderPath))
                            files.Add(file.Name);
                        else
                            ZnodeLogging.LogMessage($"ZnodeSAPConnector.{folderName}: {file.Name} failed to download.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
                    }
                    if (!hasFiles)
                    {
                        ZnodeLogging.LogMessage($"ZnodeSAPConnector.{folderName}: File Not Fount.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                    }
                    else
                    {
                        if (sftpFiles.Count() > 0)
                        {
                            string archiveDirectoryPathPath = string.Concat(GetConfigValue(erpConnectorControlListModel, SAPConnectorConstants.ArchiveDirectoryPath), folderName);
                            ArchiveFile(sftpClient, sftpFiles, remoteDirectoryPath, archiveDirectoryPathPath);
                        }
                    }
                    sftpClient.Disconnect();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return files;
        }

        //Download Files From SFTP Server
        private static bool DownloadFilesFromSFTPServer(SftpClient sftpClient, SftpFile file, string downloadFolderPath)
        {
            bool isDownload = false;
            try
            {
                if (string.IsNullOrEmpty(downloadFolderPath))
                {
                    ZnodeLogging.LogMessage($"ZnodeSAPConnector.DownloadFilesFromSFTPServer : downloadFolderPath is null or empty", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
                    return isDownload;
                }
                FileInfo fileInfo = new FileInfo(downloadFolderPath);

                string filePath = $"{downloadFolderPath}{file.Name}";

                using (Stream fileStream = File.OpenWrite(filePath))
                {
                    sftpClient.DownloadFile(file.FullName, fileStream);
                }
                isDownload = true;
            }
            catch (Exception ex)
            {
                isDownload = false;
                throw ex;
            }
            return isDownload;
        }

        //Archive Files from one location to another
        private static void ArchiveFile(SftpClient sftpClient, IEnumerable<SftpFile> sftpFiles, string remoteDirectoryPath, string archiveDirectoryPath)
        {
            if (string.IsNullOrEmpty(remoteDirectoryPath) || string.IsNullOrEmpty(archiveDirectoryPath))
            {
                ZnodeLogging.LogMessage($"ZnodeSAPConnector.ArchiveFile : remoteDirectoryPath: {remoteDirectoryPath} is null or empty or archiveDirectoryPath: {archiveDirectoryPath} is null or empty ", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
            }
            else
            {
                if (!sftpClient.Exists(archiveDirectoryPath))
                    sftpClient.CreateDirectory(archiveDirectoryPath);

                foreach (SftpFile file in sftpFiles)
                {
                    string archiveFile = $"/{archiveDirectoryPath}/{file.Name.Replace(SAPConnectorConstants.CSVFileExtension, "_")}{DateTime.UtcNow.Day}_{DateTime.UtcNow.Month}_{DateTime.UtcNow.Year}_{DateTime.UtcNow.Hour}_{DateTime.UtcNow.Minute}_{DateTime.UtcNow.Second}{SAPConnectorConstants.CSVFileExtension}";
                    sftpClient.RenameFile(string.Concat(remoteDirectoryPath, "/", file.Name), archiveFile);
                }
            }
        }

        // Get value from saved json setting
        public static string GetConfigValue(ERPConnectorControlListModel erpConnectorControlListModel, string key) =>
            erpConnectorControlListModel?.ERPConnectorControlList?.FirstOrDefault(x => x.Id.Equals(key, StringComparison.OrdinalIgnoreCase))?.Value;
    }
}
