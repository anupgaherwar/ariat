﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using System;
using System.Collections.Generic;
using System.Linq;

using Znode.Engine.Api.Models;
using Znode.Engine.ERPConnector.Model;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Sample.Api.Model;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;

namespace Znode.Engine.ERPConnector
{
    public static class SAPMapper
    {
        //Map OrderModel to SAPOrderModel
        public static SAPOrderModel ToSAPOrderModel(OrderModel orderModel)
        {
            if (IsNull(orderModel) && orderModel?.OrderLineItems.Count == 0)
            {
                return null;
            }

            decimal shippingDiscount = IsNotNull(orderModel.Custom4) ? Convert.ToDecimal(orderModel.Custom4) : 0;
            decimal totalDiscountWithoutShipping = shippingDiscount > 0 && orderModel.DiscountAmount > 0 ? GetTotalOrderDiscount(orderModel.OmsOrderDetailsId).GetValueOrDefault() - shippingDiscount : orderModel.DiscountAmount;
            SAPOrderModel sapOrderModel = new SAPOrderModel
            {
                OrderNumber = orderModel.OrderNumber,
                OrderDate = orderModel.OrderDate,
                OrderState = orderModel.OrderState,
                AccountNumber = orderModel.AccountNumber,
                UserId = orderModel.UserName,
                AccountStoreId = Convert.ToInt32(orderModel?.BillingAddress?.Custom1),
                SubTotal = orderModel.SubTotal,
                OrderLineItems = ToSAPOrderLineItemModel(orderModel.OrderLineItems, orderModel.PortalId)
            };
            sapOrderModel.DiscountAmount = totalDiscountWithoutShipping - sapOrderModel.OrderLineItems.Sum(x => x.DiscountAmount).GetValueOrDefault();
            sapOrderModel.TaxCost = orderModel.TaxCost;
            sapOrderModel.ShippingCost = orderModel.ShippingCost + shippingDiscount;
            sapOrderModel.ShippingDiscount = shippingDiscount;
            sapOrderModel.Total = orderModel.Total;
            sapOrderModel.VoucherAmount = orderModel.GiftCardAmount;
            sapOrderModel.OrderNotes = ToSAPOrderNotesModel(orderModel.OrderNotes);
            sapOrderModel.CheckoutReferenceNumber = orderModel.JobName;
            sapOrderModel.ShippingTypeName = orderModel.ShippingTypeName;
            sapOrderModel.DropShip = DropShip(orderModel.AccountId, orderModel.ShippingAddress.AddressId);
            sapOrderModel.ShippingAddress = ToSAPAaddressModel(orderModel?.ShippingAddress);
            sapOrderModel.CouponCode = orderModel?.CouponCode?.Replace("<br/>", ",");

            Dictionary<string, string> keyValuePairs = new Dictionary<string, string>();

            if (!string.IsNullOrEmpty(orderModel.Custom3))
            {
                keyValuePairs = JsonConvert.DeserializeObject<Dictionary<string, string>>(orderModel.Custom3);
            }

            sapOrderModel.PaymentType = orderModel.PaymentType;
            if (!string.IsNullOrEmpty(orderModel.PaymentDisplayName)){
                sapOrderModel.PaymentType = orderModel.PaymentDisplayName;
            }
            sapOrderModel.PaymentTransactionToken = orderModel.PaymentTransactionToken;
            sapOrderModel.ShopperId = orderModel.OrderNumber;
            sapOrderModel.RecurringPSPReference = keyValuePairs.ContainsKey("recurringpspreference") ? keyValuePairs["recurringpspreference"] : "";
            sapOrderModel.CreateCardName = keyValuePairs.ContainsKey("name") ? keyValuePairs["name"] : "";
            sapOrderModel.ExpiryMonth = Convert.ToString(orderModel.CreditCardExpMonth);
            sapOrderModel.ExpiryYear = Convert.ToString(orderModel.CreditCardExpYear);
            sapOrderModel.CardType = keyValuePairs.ContainsKey("cardtype") ? keyValuePairs["cardtype"] : "";
            sapOrderModel.AuthorizationDate = keyValuePairs.ContainsKey("authorizationdate") ? keyValuePairs["authorizationdate"] : "";
            sapOrderModel.AuthorizationTime = keyValuePairs.ContainsKey("authorizationtime") ? keyValuePairs["authorizationtime"] : ""; ;
            sapOrderModel.AuthorizationReferenceNumber = keyValuePairs.ContainsKey("authorizationnumber") ? keyValuePairs["authorizationnumber"] : "";
            sapOrderModel.AuthorizationAmount = 0;
            return sapOrderModel;
        }

        //Map order Line Items to sap Order Line Item
        public static List<SAPOrderLineItemModel> ToSAPOrderLineItemModel(List<OrderLineItemModel> orderLineItems, int portalId)
        {
            List<SAPOrderLineItemModel> sapOrderLineItemModelList = null;
            if (orderLineItems?.Count > 0)
            {
                sapOrderLineItemModelList = new List<SAPOrderLineItemModel>();
                int? WarehouseId = new ZnodeRepository<ZnodePortalWarehouse>().Table?.FirstOrDefault(x => x.PortalId == portalId)?.WarehouseId;

                List<string> skuList = orderLineItems?.Where(y => y.OrderLineItemRelationshipTypeId == (int)ZnodeCartItemRelationshipTypeEnum.Configurable || y.OrderLineItemRelationshipTypeId == (int)ZnodeCartItemRelationshipTypeEnum.Simple)?.Select(x => x.Sku)?.ToList();
                List<ZnodeInventory> inventoryList = new ZnodeRepository<ZnodeInventory>().Table?.Where(x => skuList.Contains(x.SKU) && x.WarehouseId == WarehouseId)?.ToList();

                List<int> OrderLineItemIds = orderLineItems?.Where(y => y.OrderLineItemRelationshipTypeId == (int)ZnodeCartItemRelationshipTypeEnum.Configurable || y.OrderLineItemRelationshipTypeId == (int)ZnodeCartItemRelationshipTypeEnum.Simple)?.Select(x => x.OmsOrderLineItemsId)?.ToList();
                List<ZnodeOmsOrderLineItem> decorationList = new ZnodeRepository<ZnodeOmsOrderLineItem>().Table.Where(x => OrderLineItemIds.Contains((int)x.ParentOmsOrderLineItemsId) && x.OrderLineItemRelationshipTypeId == (int)ZnodeCartItemRelationshipTypeEnum.AddOns)?.ToList();

                foreach (OrderLineItemModel orderLineItemModel in orderLineItems?.Where(x => x.OrderLineItemRelationshipTypeId == (int)ZnodeCartItemRelationshipTypeEnum.Configurable || x.OrderLineItemRelationshipTypeId == (int)ZnodeCartItemRelationshipTypeEnum.Simple))
                {
                    orderLineItemModel.PersonaliseValuesDetail = orderLineItems?.FirstOrDefault(x => x.OmsOrderLineItemsId == orderLineItemModel.ParentOmsOrderLineItemsId)?.PersonaliseValuesDetail;
                    List<SAPPersonaliseValueModel> sapPersonaliseValueList = null;
                    if (orderLineItemModel?.PersonaliseValuesDetail?.Count > 0)
                    {
                        sapPersonaliseValueList = ToSAPPersonaliseValueModel(orderLineItemModel.PersonaliseValuesDetail);
                    }

                    List<AriatOrderLineItemsDetailModel> ariatOrderLineItemsDetailModels = new List<AriatOrderLineItemsDetailModel>();
                    DateTime? backOrderDate = null;
                    decimal? msrp = null;
                    if (!string.IsNullOrEmpty(orderLineItemModel.Custom4))
                    {
                        ariatOrderLineItemsDetailModels = JsonConvert.DeserializeObject<List<AriatOrderLineItemsDetailModel>>(orderLineItemModel.Custom4);
                        if (!string.IsNullOrEmpty(ariatOrderLineItemsDetailModels[0].BackorderDate))
                        {
                            backOrderDate = DateTime.Parse(ariatOrderLineItemsDetailModels[0].BackorderDate);
                        }
                        if (!string.IsNullOrEmpty(ariatOrderLineItemsDetailModels[0].MSRP))
                        {
                            msrp = Convert.ToDecimal(ariatOrderLineItemsDetailModels[0].MSRP);
                        }
                    }
                    sapOrderLineItemModelList.Add(new SAPOrderLineItemModel()
                    {
                        OmsOrderLineItemsId = orderLineItemModel.OmsOrderLineItemsId,
                        Sku = orderLineItemModel.Sku,
                        Quantity = orderLineItemModel.Quantity,
                        Price = Convert.ToDecimal(orderLineItemModel.Custom2),
                        DiscountAmount = orderLineItemModel?.DiscountAmount != 0 ? orderLineItemModel?.DiscountAmount : orderLineItems?.FirstOrDefault(x => x.OmsOrderLineItemsId == orderLineItemModel.ParentOmsOrderLineItemsId)?.DiscountAmount,
                        DecorationDiscount = decorationList?.Where(x => x.ParentOmsOrderLineItemsId == orderLineItemModel.OmsOrderLineItemsId)?.Sum(x => x.DiscountAmount),
                        MSRP = msrp,
                        SalesTax = orderLineItemModel.SalesTax,
                        BackOrderedDate = backOrderDate,
                        OrderLineItemState = orderLineItemModel.OrderLineItemState,
                        RegularLogoPriceTotal = decorationList?.Where(y => y.Sku == SAPConnectorConstants.RegularLogoSku && y.ParentOmsOrderLineItemsId == orderLineItemModel.OmsOrderLineItemsId)?.Sum(x => x.Quantity * x.Price),
                        LargeLogoPriceTotal = decorationList?.Where(y => y.Sku == SAPConnectorConstants.LargeLogoSku && y.ParentOmsOrderLineItemsId == orderLineItemModel.OmsOrderLineItemsId)?.Sum(x => x.Quantity * x.Price),
                        TextPriceTotal = decorationList?.Where(y => y.Sku == SAPConnectorConstants.TextPriceSku && y.ParentOmsOrderLineItemsId == orderLineItemModel.OmsOrderLineItemsId)?.Sum(x => x.Quantity * x.Price),
                        Total = orderLineItemModel.Total,
                        PersonalizeValuesDetail = sapPersonaliseValueList,
                    });
                }
            }
            return sapOrderLineItemModelList;
        }

        //Map AddressModel to SAPAddressModel
        private static SAPAddressModel ToSAPAaddressModel(AddressModel addressModel)
        {
            if (IsNull(addressModel))
            {
                return null;
            }

            SAPAddressModel sapAddressModel = new SAPAddressModel()
            {
                DisplayName = addressModel.DisplayName,
                FirstName = addressModel.FirstName,
                LastName = addressModel.LastName,
                CompanyName = addressModel.CompanyName,
                Address1 = addressModel.Address1,
                Address2 = addressModel.Address2,
                Address3 = addressModel.Address3,
                CountryName = addressModel.CountryName,
                StateName = addressModel.StateName,
                CityName = addressModel.CityName,
                PostalCode = addressModel.PostalCode,
                PhoneNumber = addressModel.PhoneNumber,
            };
            return sapAddressModel;
        }

        //Map order Line Items to sap Order Line Item
        private static SAPOrderNotesModel ToSAPOrderNotesModel(List<OrderNotesModel> orderNotesList)
        {
            return new SAPOrderNotesModel()
            {
                Note = orderNotesList?.FirstOrDefault()?.Notes,
                UserName = orderNotesList?.FirstOrDefault()?.UserName
            };
        }

        //map PersonaliseValueModelList to SAPPersonaliseValueModelList
        private static List<SAPPersonaliseValueModel> ToSAPPersonaliseValueModel(List<PersonaliseValueModel> personaliseValueList)
        {
            List<SAPPersonaliseValueModel> sapPersonaliseValueList = new List<SAPPersonaliseValueModel>();
            foreach (PersonaliseValueModel personaliseValueModel in personaliseValueList?.Where(x => x.PersonalizeCode != "ArtifiDesignId" && x.PersonalizeCode != "ArtifiImagePath" && x.PersonalizeCode != "ArtifiSKU"))
            {
                JObject jsonString = JObject.Parse(personaliseValueModel?.PersonalizeValue);
                if (IsNotNull(jsonString))
                {
                    SAPPersonaliseValueModel sapPersonaliseValueModel = new SAPPersonaliseValueModel()
                    {
                        PlacementCode = GetValueFromJson(jsonString, false, null, "viewCode"),
                        Font = GetValueFromJson(jsonString, true, "text", "fontFamily"),
                        ThreadColor = GetValueFromJson(jsonString, true, "text", "colorName"),
                        LetterHeight = GetValueFromJson(jsonString, true, "text", "fontSize"),
                        Text = GetValueFromJson(jsonString, true, "text", "text"),
                        ApprovedImageFileName = GetValueFromJson(jsonString, true, "image", "imageName")
                    };
                    sapPersonaliseValueList.Add(sapPersonaliseValueModel);
                }
            }
            return sapPersonaliseValueList;
        }

        //Get Value From Json
        private static string GetValueFromJson(JObject jsonString, bool isInnerNode, string innerNodeName, string nodeName)
        {
            string nodeValue = null;
            try
            {
                if (isInnerNode)
                {
                    foreach (JToken result in jsonString[innerNodeName])
                    {
                        nodeValue = (string)result[nodeName];
                        if (IsNotNull(nodeValue))
                        {
                            break;
                        }
                    }
                }
                else
                {
                    nodeValue = (string)jsonString[nodeName];
                }
            }
            catch
            {
                nodeValue = null;
            }
            return nodeValue;
        }

        private static decimal? GetTotalOrderDiscount(int omsOrderDetailsId)
        {
            return new ZnodeRepository<ZnodeOmsOrderDiscount>().Table.Where(x => x.OmsOrderDetailsId == omsOrderDetailsId && (x.OmsDiscountTypeId == (int)OrderDiscountTypeEnum.PROMOCODE || x.OmsDiscountTypeId == (int)OrderDiscountTypeEnum.COUPONCODE))?.Sum(y => y.DiscountAmount);
        }

        private static bool DropShip(int accountId, int addressId)
        {
            return !new ZnodeRepository<ZnodeAccountAddress>().Table.Any(x => x.AccountId == accountId && x.AddressId == addressId);
        }
    }
}
