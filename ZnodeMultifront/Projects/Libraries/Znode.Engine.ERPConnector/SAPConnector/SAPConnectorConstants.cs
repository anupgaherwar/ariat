﻿using System;
using System.Collections.Specialized;
using System.Configuration;

namespace Znode.Engine.ERPConnector
{
    public struct SAPConnectorConstants
    {
        private static NameValueCollection settings = ConfigurationManager.AppSettings;
        public const string HostName = "txSFTPtHostName";
        public const string UserName = "txtSFTPUserName";
        public const string Password = "txtSFTPPassword";
        public const string PortNumber = "txtSFTPPortNumber";
        public const string FileDirectoryPath = "txtFileDirectoryPath";
        public const string ArchiveDirectoryPath = "txtArchiveDirectoryPath";
        public const string InventoryFolder = "Inventory";
        public const string OrderFolder = "Order";
        public const string ProductFolder = "Product";
        public const string ProductAssociationFolder = "ProductAssociation";
        public const string ProductLongTextFolder = "ProductLongText";
        public const string QuantityBasedRate = "QuantityBasedRate";
        public const string SAPProductTemplate = "SAPProductTemplate";
        public const string SAPInventoryTemplate = "SAPInventoryTemplate";
        public const string SAPProductAssociationTemplate = "SAPProductAssociationTemplate";
        public const string ProductUpdate = "ProductUpdate";
        public const string PricingFolder = "Pricing";
        public const string PriceListCode = "MasterPrice";
        public const string SAPPriceTemplate = "SAPPriceTemplate";
        public static string SAPConnectorFilePath { get; } = Convert.ToString(settings["SAPConnectorFilePath"]);
        public const string CSVFileExtension = ".csv";
        public const string AttributeDefaultValueFolder = "AttributeDefaultValue";
        public const string SAPAttributeDefaultValueTemplate = "AttributeDefaultValueTemplate";
        public const string SAPDiscountFileTemplate = "SAPDiscountFileTemplate";
        public const string DiscountFileFolder = "DiscountFile";
        public const string AccountFolder = "Accounts";
        public const string SAPAccountTemplate = "SAPAccountTemplate";
        public const string SalesRepFolder = "SalesRep";
        public const string SAPSalesRepTemplate = "SAPSalesRepTemplate";
        public static string RegularLogoSku { get; } = Convert.ToString(settings["RegularLogoSku"]);
        public static string LargeLogoSku { get; } = Convert.ToString(settings["LargeLogoSku"]);
        public static string TextPriceSku { get; } = Convert.ToString(settings["TextPriceSku"]);
        public const string OrderProcessingBatchSize = "txtOrderProcessingBatchSize";
        public const string OrderHistoryFolder = "OrderHistory";
    }
}
