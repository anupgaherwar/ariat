﻿namespace Znode.Engine.ERPConnector
{
    public enum SAPImportHeadEnum
    {
        AttributeDefaultValue = 15,
        SAPDiscountFile = 16,
        SAPAccount = 17,
        SAPSalesRep = 18
    }
}
