﻿namespace Znode.Engine.ERPConnector.Model
{
    public class SAPOrderNotesModel
    {
        public string Note { get; set; }
        public string UserName { get; set; }
    }
}
