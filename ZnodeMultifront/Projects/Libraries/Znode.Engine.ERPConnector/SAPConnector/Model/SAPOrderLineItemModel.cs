﻿using System;
using System.Collections.Generic;

namespace Znode.Engine.ERPConnector.Model
{
    public class SAPOrderLineItemModel
    {
        public int OmsOrderLineItemsId { get; set; }
        public string Sku { get; set; }
        public decimal Quantity { get; set; }
        public decimal? MSRP { get; set; }
        public decimal Price { get; set; }
        public decimal? DiscountAmount { get; set; }
        public decimal? DecorationDiscount { get; set; }
        public decimal? SalesTax { get; set; }
        public DateTime? BackOrderedDate { get; set; }
        public string OrderLineItemState { get; set; }
        public decimal? RegularLogoPriceTotal { get; set; }
        public decimal? LargeLogoPriceTotal { get; set; }
        public decimal? TextPriceTotal { get; set; }
        public decimal Total { get; set; }
        public List<SAPPersonaliseValueModel> PersonalizeValuesDetail { get; set; }       
    }
}
