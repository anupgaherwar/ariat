﻿namespace Znode.Engine.ERPConnector.Model
{
    public class SAPPersonaliseValueModel
    {
        public string PlacementCode { get; set; }
        public string Font { get; set; }
        public string ThreadColor { get; set; }
        public string LetterHeight { get; set; }
        public string Text { get; set; }
        public string ApprovedImageFileName { get; set; }
    }
}
