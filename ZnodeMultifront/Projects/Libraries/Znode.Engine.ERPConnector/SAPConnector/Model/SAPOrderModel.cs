﻿using Newtonsoft.Json;

using System;
using System.Collections.Generic;

using Znode.Engine.Api.Models;

namespace Znode.Engine.ERPConnector.Model
{
    public class SAPOrderModel
    {
        public string OrderNumber { get; set; }
        [JsonConverter(typeof(CustomDateTimeConverter))]
        public DateTime OrderDate { get; set; }
        public string OrderState { get; set; }
        public string AccountNumber { get; set; }
        public string UserId { get; set; }
        public int AccountStoreId { get; set; }
        public decimal SubTotal { get; set; }
        public decimal DiscountAmount { get; set; }
        public decimal TaxCost { get; set; }
        public decimal ShippingCost { get; set; }
        public decimal ShippingDiscount { get; set; }
        public decimal Total { get; set; }
        public decimal VoucherAmount { get; set; }
        public SAPOrderNotesModel OrderNotes { get; set; }
        public string CheckoutReferenceNumber { get; set; }
        public string ShippingTypeName { get; set; }
        public bool DropShip { get; set; }
        public SAPAddressModel ShippingAddress { get; set; }
        public string PaymentType { get; set; }
        public string PaymentTransactionToken { get; set; }
        public string ShopperId { get; set; }
        public string RecurringPSPReference { get; set; }
        public string CreateCardName { get; set; }
        public string ExpiryMonth { get; set; }
        public string ExpiryYear { get; set; }
        public string CardType { get; set; }
        public string AuthorizationDate { get; set; }
        public string AuthorizationTime { get; set; }
        public string AuthorizationReferenceNumber { get; set; }
        public decimal? AuthorizationAmount { get; set; }
        public string CouponCode { get; set; }
        public List<SAPOrderLineItemModel> OrderLineItems { get; set; }

    }
}