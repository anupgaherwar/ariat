﻿using GenericParsing;

using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Service;
using Znode.Engine.ERPConnector.Model;
using Znode.Engine.Exceptions;
using Znode.Engine.Services;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;

using static Znode.Libraries.ECommerce.Utilities.HelperUtility;

namespace Znode.Engine.ERPConnector
{
    public class ZnodeSAPConnector : BaseERP
    {
        #region Private Variables     
        private readonly string currentClassName = string.Empty;
        #endregion

        #region Constructor
        public ZnodeSAPConnector()
        {
            currentClassName = this.GetType().Name;
        }
        #endregion

        #region public method
        #region Batch Process
        //Product Refresh - Batch Process.
        public override bool ProductRefresh()
        {
            bool status = false;
            string touchPointName = GetCurrentMethod();
            string errorMessage = string.Empty;
            try
            {
                ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} : Process for product refresh started.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                status = ProcessImportData(SAPConnectorConstants.SAPProductTemplate, SAPConnectorConstants.ProductFolder, ImportHeadEnum.Product.ToString(), out errorMessage, 1);

                if (status)
                {
                    ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} : Process for ProductRefresh" + (status ? "initiated." : "failed."), ZnodeLogging.Components.ERP.ToString(), status ? TraceLevel.Info : TraceLevel.Error);
                }
                else
                {
                    ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} : Process for ProductRefresh touchpoint trigger failed.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                    SendSchedulerActivityLog(false, $"{currentClassName}.{touchPointName} : Process for ProductRefresh touchpoint trigger failed. {errorMessage}", touchPointName);
                }
            }
            catch (ZnodeException ex)
            {
                status = false;
                ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} {ex.Message}", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error, ex);
                SendSchedulerActivityLog(false, $"{currentClassName}.{touchPointName} : {ex.Message}", GetCurrentMethod());
            }
            catch (Exception ex)
            {
                status = false;
                ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} {ex.Message}", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error, ex);
                SendSchedulerActivityLog(false, $"{currentClassName}.{touchPointName} : {ex.Message}", touchPointName);
            }
            return status;
        }

        //Process ProductAssociation file from ERP to Znode on a scheduled basis.
        public override bool ProductAssociation()
        {
            bool status = false;
            string touchPointName = GetCurrentMethod();
            string errorMessage = string.Empty;
            try
            {
                ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} : Process for ProductAssociation started.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                status = ProcessImportData(SAPConnectorConstants.SAPProductAssociationTemplate, SAPConnectorConstants.ProductAssociationFolder, ImportHeadEnum.Product.ToString(), out errorMessage);

                if (status)
                {
                    ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} : Process for ProductAssociation" + (status ? "initiated." : "failed."), ZnodeLogging.Components.ERP.ToString(), status ? TraceLevel.Info : TraceLevel.Error);
                }
                else
                {
                    ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} : Process for ProductAssociation touchpoint trigger failed.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                    SendSchedulerActivityLog(false, $"{currentClassName}.{touchPointName} : Process for ProductAssociation touchpoint trigger failed. {errorMessage}", touchPointName);
                }
            }
            catch (ZnodeException ex)
            {
                status = false;
                ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} {ex.Message}", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error, ex);
                SendSchedulerActivityLog(false, $"{currentClassName}.{touchPointName} : {ex.Message}", touchPointName);
            }
            catch (Exception ex)
            {
                status = false;
                ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} {ex.Message}", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error, ex);
                SendSchedulerActivityLog(false, $"{currentClassName}.{touchPointName} : {ex.Message}", touchPointName);
            }
            return status;
        }

        //Process ProductLongText file from ERP to Znode on a scheduled basis.
        public override bool ProductLongText()
        {
            bool status = false;
            string touchPointName = GetCurrentMethod();
            string errorMessage = string.Empty;
            try
            {
                ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} : Process for ProductLongText started.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                status = ProcessImportData(SAPConnectorConstants.SAPProductTemplate, SAPConnectorConstants.ProductLongTextFolder, ImportHeadEnum.Product.ToString(), out errorMessage, 1, 0, true);

                if (status)
                {
                    ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} : Process for ProductLongText" + (status ? "initiated." : "failed."), ZnodeLogging.Components.ERP.ToString(), status ? TraceLevel.Info : TraceLevel.Error);
                }
                else
                {
                    ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} : Process for ProductLongText touchpoint trigger failed.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                    SendSchedulerActivityLog(false, $"{currentClassName}.{touchPointName} : Process for ProductLongText touchpoint trigger failed. {errorMessage}", touchPointName);
                }
            }
            catch (ZnodeException ex)
            {
                status = false;
                ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} {ex.Message}", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error, ex);
                SendSchedulerActivityLog(false, $"{currentClassName}.{touchPointName} : {ex.Message}", touchPointName);
            }
            catch (Exception ex)
            {
                status = false;
                ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} {ex.Message}", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error, ex);
                SendSchedulerActivityLog(false, $"{currentClassName}.{touchPointName} : {ex.Message}", touchPointName);
            }
            return status;
        }

        //Refresh inventory for products by warehouse from ERP to Znode on a scheduled basis.
        public override bool InventoryRefresh()
        {
            bool status = false;
            string touchPointName = GetCurrentMethod();
            string errorMessage = string.Empty;
            try
            {
                ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} : Process for inventory refresh started.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);

                status = ProcessImportData(SAPConnectorConstants.SAPInventoryTemplate, SAPConnectorConstants.InventoryFolder, ImportHeadEnum.Inventory.ToString(), out errorMessage);

                if (status)
                {
                    ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} : Inventory refresh touchpoint trigger successfully.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                }
                else
                {
                    ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} : Inventory refresh touchpoint trigger failed.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                    SendSchedulerActivityLog(false, $"{currentClassName}.{touchPointName} : Inventory refresh touchpoint trigger failed. {errorMessage}", touchPointName);
                }
            }
            catch (ZnodeException ex)
            {
                status = false;
                ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} {ex.Message}", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error, ex);
                SendSchedulerActivityLog(false, $"{currentClassName}.{touchPointName} : {ex.Message}", touchPointName);
            }
            catch (Exception ex)
            {
                status = false;
                ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} {ex.Message}", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error, ex);
                SendSchedulerActivityLog(false, $"{currentClassName}.{touchPointName} : {ex.Message}", touchPointName);
            }
            return status;
        }

        //Refresh standard price list from ERP to Znode on a scheduled basis.
        public override bool PricingRefresh()
        {
            bool status = false;
            string touchPointName = GetCurrentMethod();
            string errorMessage = string.Empty;
            try
            {
                ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} : Process for Pricing refresh started.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                int? priceListId = new ZnodeRepository<ZnodePriceList>().Table.FirstOrDefault(x => x.ListCode == SAPConnectorConstants.PriceListCode)?.PriceListId;
                if (IsNotNull(priceListId) && priceListId > 0)
                {
                    status = ProcessImportData(SAPConnectorConstants.SAPPriceTemplate, SAPConnectorConstants.PricingFolder, ImportHeadEnum.Pricing.ToString(), out errorMessage, 0, priceListId.GetValueOrDefault());

                    if (status)
                    {
                        ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} : Pricing refresh touchpoint trigger successfully.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                    }
                    else
                    {
                        ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} : Pricing refresh touchpoint trigger failed.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                        SendSchedulerActivityLog(false, $"{currentClassName}.{touchPointName} : Pricing refresh touchpoint trigger failed. {errorMessage}", touchPointName);
                    }
                }
                else
                {
                    status = false;
                    ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName}: PriceListId getting zero for MasterPrice.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
                    SendSchedulerActivityLog(false, $"{currentClassName}.{touchPointName} : PriceListId getting zero for MasterPrice.", touchPointName);
                }
            }
            catch (ZnodeException ex)
            {
                status = false;
                ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} {ex.Message}", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error, ex);
                SendSchedulerActivityLog(false, $"{currentClassName}.{touchPointName} : {ex.Message}", touchPointName);
            }
            catch (Exception ex)
            {
                status = false;
                ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} {ex.Message}", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error, ex);
                SendSchedulerActivityLog(false, $"{currentClassName}.{touchPointName} : {ex.Message}", touchPointName);
            }
            return status;
        }

        //Refresh Attribute Default Value from ERP to Znode on a scheduled basis.
        public override bool AttributeDefaultValue()
        {
            bool status = false;
            string touchPointName = GetCurrentMethod();
            string errorMessage = string.Empty;
            try
            {
                ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} : Process for attribute default value started.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);

                status = ProcessImportData(SAPConnectorConstants.SAPAttributeDefaultValueTemplate, SAPConnectorConstants.AttributeDefaultValueFolder, ((int)SAPImportHeadEnum.AttributeDefaultValue).ToString(), out errorMessage);

                if (status)
                {
                    ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} : AttributeDefaultValue trigger successfully.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                }
                else
                {
                    ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} : AttributeDefaultValue touchpoint trigger failed. {errorMessage}", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                    SendSchedulerActivityLog(false, $"{currentClassName}.{touchPointName} : AttributeDefaultValue touchpoint trigger failed.", touchPointName);
                }
            }
            catch (ZnodeException ex)
            {
                status = false;
                ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} {ex.Message}", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error, ex);
                SendSchedulerActivityLog(false, $"{currentClassName}.{touchPointName} : {ex.Message}", touchPointName);
            }
            catch (Exception ex)
            {
                status = false;
                ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} {ex.Message}", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error, ex);
                SendSchedulerActivityLog(false, $"{currentClassName}.{touchPointName} : {ex.Message}", touchPointName);
            }
            return status;
        }

        //Process discount file from ERP to Znode on a scheduled basis.
        public override bool DiscountFile()
        {
            bool status = false;
            string touchPointName = GetCurrentMethod();
            string errorMessage = string.Empty;
            try
            {
                ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} : Process for discount file started.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);

                status = ProcessImportData(SAPConnectorConstants.SAPDiscountFileTemplate, SAPConnectorConstants.DiscountFileFolder, SAPImportHeadEnum.SAPDiscountFile.ToString(), out errorMessage);

                if (status)
                {
                    ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} : DiscountFile touchpoint trigger successfully.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                }
                else
                {
                    ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} : DiscountFile touchpoint trigger failed.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                    SendSchedulerActivityLog(false, $"{currentClassName}.{touchPointName} : DiscountFile touchpoint trigger failed. {errorMessage}", touchPointName);
                }
            }
            catch (ZnodeException ex)
            {
                status = false;
                ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} {ex.Message}", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error, ex);
                SendSchedulerActivityLog(false, $"{currentClassName}.{touchPointName} : {ex.Message}", touchPointName);
            }
            catch (Exception ex)
            {
                status = false;
                ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} {ex.Message}", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error, ex);
                SendSchedulerActivityLog(false, $"{currentClassName}.{touchPointName} : {ex.Message}", touchPointName);
            }
            return status;
        }

        //Process Accounts file from ERP to Znode on a scheduled basis.
        public override bool GetAccounts()
        {
            bool status = false;
            string touchPointName = GetCurrentMethod();
            string errorMessage = string.Empty;
            try
            {
                ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} : Process for account file started.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);

                status = ProcessImportData(SAPConnectorConstants.SAPAccountTemplate, SAPConnectorConstants.AccountFolder, SAPImportHeadEnum.SAPAccount.ToString(), out errorMessage);

                if (status)
                {
                    ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} : GetAccounts touchpoint trigger successfully.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                }
                else
                {
                    ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} : GetAccounts touchpoint trigger failed.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                    SendSchedulerActivityLog(false, $"{currentClassName}.{touchPointName} : GetAccounts touchpoint trigger failed. {errorMessage}", touchPointName);
                }
            }
            catch (ZnodeException ex)
            {
                status = false;
                ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} {ex.Message}", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error, ex);
                SendSchedulerActivityLog(false, $"{currentClassName}.{touchPointName} : {ex.Message}", touchPointName);
            }
            catch (Exception ex)
            {
                status = false;
                ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} {ex.Message}", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error, ex);
                SendSchedulerActivityLog(false, $"{currentClassName}.{touchPointName} : {ex.Message}", touchPointName);
            }
            return status;
        }

        //Process SalesRep file from ERP to Znode on a scheduled basis.
        public override bool SalesRep()
        {
            bool status = false;
            string touchPointName = GetCurrentMethod();
            string errorMessage = string.Empty;
            try
            {
                ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} : Process for sales rep file started.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);

                status = ProcessImportData(SAPConnectorConstants.SAPSalesRepTemplate, SAPConnectorConstants.SalesRepFolder, SAPImportHeadEnum.SAPSalesRep.ToString(), out errorMessage);

                if (status)
                {
                    ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} : SalesRep touchpoint trigger successfully.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                }
                else
                {
                    ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} : SalesRep touchpoint trigger failed.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                    SendSchedulerActivityLog(false, $"{currentClassName}.{touchPointName} : SalesRep touchpoint trigger failed. {errorMessage}", touchPointName);
                }
            }
            catch (ZnodeException ex)
            {
                status = false;
                ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} {ex.Message}", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error, ex);
                SendSchedulerActivityLog(false, $"{currentClassName}.{touchPointName} : {ex.Message}", touchPointName);
            }
            catch (Exception ex)
            {
                status = false;
                ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} {ex.Message}", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error, ex);
                SendSchedulerActivityLog(false, $"{currentClassName}.{touchPointName} : {ex.Message}", touchPointName);
            }
            return status;
        }

        //Get Order History from ERP to Znode       
        public override bool OrderHistory()
        {
            bool status = false;
            string touchPointName = GetCurrentMethod();
            string errorMessage = string.Empty;

            ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} : Process for order history started.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
            try
            {
                ERPConnectorControlListModel erpConnectorControlListModel = GetActiveERPConnectorControls();

                string csvPath = GetHostedServerERPFilePath(SAPConnectorConstants.OrderHistoryFolder);
                List<string> fileList = SFTPHelper.GetAllFilesFromSFTPServer(erpConnectorControlListModel, SAPConnectorConstants.OrderHistoryFolder, csvPath);
                if (fileList?.Count == 0)
                {
                    ZnodeLogging.LogMessage($"ZnodeSAPConnector.{SAPConnectorConstants.OrderHistoryFolder}: File Not Found.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                    return true;
                }

                foreach (string fileName in fileList)
                {
                    bool isRecordPresent = false;
                    Guid tableGuid = Guid.NewGuid();
                    string tableName = ReadAndCreateTable($"{csvPath}/{fileName}", SAPConnectorConstants.OrderHistoryFolder, tableGuid, out isRecordPresent);
                    if (isRecordPresent)
                    {
                        SqlConnection conn = GetSqlConnection();
                        SqlCommand cmd = new SqlCommand("Znode_Ariat_UpdateOrders", conn)
                        {
                            CommandType = CommandType.StoredProcedure
                        };
                        cmd.Parameters.AddWithValue("@TableName", tableName);
                        cmd.Parameters.AddWithValue("@UserId", 2);
                        cmd.Parameters.AddWithValue("@SchedulerName", GetCurrentMethod());
                        if (conn.State.Equals(ConnectionState.Closed))
                        {
                            conn.Open();
                        }
                        cmd.ExecuteReader();
                    }
                }
                status = true;
            }
            catch (ZnodeException ex)
            {
                status = false;
                ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} {ex.Message}", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error, ex);
                SendSchedulerActivityLog(false, $"{currentClassName}.{touchPointName} : {ex.Message}", GetCurrentMethod());
            }
            catch (Exception ex)
            {
                status = false;
                ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} {ex.Message}", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error, ex);
                SendSchedulerActivityLog(false, $"{currentClassName}.{touchPointName} : {ex.Message}", touchPointName);
            }
            return status;
        }

        //Bulk order status update.
        public override bool SendShippingNotification()
        {
            bool status = false;
            string touchPointName = GetCurrentMethod();
            string errorMessage = string.Empty;
            try
            {
                ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} : Process for Send Shipping Notification started.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                new ArtifiOrderService().SendShippingNotification();
                status = true;
            }
            catch (ZnodeException ex)
            {
                status = false;
                ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} {ex.Message}", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error, ex);
                SendSchedulerActivityLog(false, $"{currentClassName}.{touchPointName} : {ex.Message}", touchPointName);
            }
            catch (Exception ex)
            {
                status = false;
                ZnodeLogging.LogMessage($"{currentClassName}.{touchPointName} {ex.Message}", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error, ex);
                SendSchedulerActivityLog(false, $"{currentClassName}.{touchPointName} : {ex.Message}", touchPointName);
            }
            return status;
        }

        /// <summary>
        /// This method generates JSON files for orders which never had a json file created or for which the file creation was failed.
        /// </summary>
        /// <returns></returns>
        public override bool GeneratePendingOrderJsonFiles()
        {
            List<int> OmsOrderIds = GetOrdersWithMissingJsonFiles();
            foreach (int OmsOrderId in OmsOrderIds)
            {
                CreateOrder(new OrderModel() { OmsOrderId = OmsOrderId });
            }
            return true;
        }
        #endregion

        #region Real Time
        /// <summary>
        /// Generates a JSON file and pushes it's data to SAP from Znode.
        /// </summary>
        /// <param name="orderModel"></param>
        /// <returns></returns>
        [RealTime("RealTime")]
        public override bool CreateOrder(OrderModel orderModel)
        {
            if (VerifyExportDirectory(GetHostedServerERPFilePath(SAPConnectorConstants.OrderFolder)))
            {
                OrderJsonFileStatusEnum fileStatus = OrderJsonFileStatusEnum.Pending;
                try
                {
                    //Line Items and Cost Price setting in OrderModel.
                    orderModel = PrepareRequiredDetailsInOrderModel(orderModel);

                    string fileName = $"{orderModel.OrderNumber}.json";
                    string filePath = GetHostedServerERPFilePath(SAPConnectorConstants.OrderFolder, fileName);

                    fileStatus = CreateOrderJsonFile(orderModel, filePath);

                    if (HelperUtility.IsNotNull(orderModel) && fileStatus == OrderJsonFileStatusEnum.Pending)
                    {
                        if (SFTPHelper.UploadFileToSFTPServer(GetActiveERPConnectorControls(), SAPConnectorConstants.OrderFolder, fileName, filePath))
                        {
                            fileStatus = OrderJsonFileStatusEnum.FilePushed;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
                    SendSchedulerActivityLog(false, $"{currentClassName}.{GetCurrentMethod()} : {ex.Message}", GetCurrentMethod());
                }
                finally
                {
                    //Update Order
                    UpdateOrderFileStatus(orderModel.OmsOrderId, fileStatus);
                }
            }
            else
            {
                //Update Order
                UpdateOrderFileStatus(orderModel.OmsOrderId, OrderJsonFileStatusEnum.Pending);
            }
            return true;
        }

        #endregion
        #endregion

        #region private method

        //Process Import Data
        private bool ProcessImportData(string templateName, string folderName, string importHead, out string errorMessage, int familyId = 0, int priceListId = 0, bool isProductLongText = false)
        {
            bool status = false;
            errorMessage = string.Empty;
            ZnodeImportTemplate importTemplate = GetImportTemplateDetailsByTemplateName(templateName);

            if (IsNotNull(importTemplate))
            {
                List<ImportMappingModel> mappingList = GetImportMappingData(importTemplate);

                if (IsNotNull(mappingList))
                {
                    ERPConnectorControlListModel erpConnectorControlListModel = GetActiveERPConnectorControls();

                    string csvPath = GetHostedServerERPFilePath(folderName);
                    List<string> fileList = SFTPHelper.GetAllFilesFromSFTPServer(erpConnectorControlListModel, folderName, csvPath);
                    if (fileList?.Count == 0)
                    {
                        ZnodeLogging.LogMessage($"ZnodeSAPConnector.{folderName}: File Not Fount.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                        return true;
                    }
                    foreach (string fileName in fileList)
                    {
                        string fileNamePath = $"{csvPath}{fileName}";

                        if (templateName == SAPConnectorConstants.SAPProductTemplate)
                        {
                            AppendDefaultProductDataToCSV(fileNamePath, isProductLongText);
                        }
                        else if (templateName == SAPConnectorConstants.SAPPriceTemplate)
                        {
                            ProcessPricingCSVFiles(fileNamePath);
                        }
                        else if (templateName == SAPConnectorConstants.SAPAttributeDefaultValueTemplate)
                        {
                            ProcessAttributeDefaultValueCSVFiles(fileNamePath);
                        }

                        //Import API call.
                        GetClient<ImportService>().ProcessData(new ImportModel { FileName = fileNamePath, ImportType = importHead, ImportTypeId = importTemplate.ImportHeadId, LocaleId = GetDefaultLocaleId(), TemplateName = templateName, TemplateId = importTemplate.ImportTemplateId, PortalId = 0, FamilyId = familyId, PriceListId = priceListId, Mappings = new ImportMappingListModel { DataMappings = mappingList } });
                        status = true;
                    }
                }
                else
                {
                    errorMessage = $"{currentClassName}.{folderName}: Import Mapping data not found for {templateName}.";
                    ZnodeLogging.LogMessage(errorMessage, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
                    status = false;
                }
            }
            else
            {
                errorMessage = $"{currentClassName}.{folderName}: {templateName} not found.";
                ZnodeLogging.LogMessage(errorMessage, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
                status = false;
            }
            return status;
        }

        // Get Active ERP connector model
        private ERPConnectorControlListModel GetActiveERPConnectorControls()
        {
            FilterCollection filters = new FilterCollection { { FilterKeys.IsActive, FilterOperators.Equals, ZnodeConstant.TrueValue } };

            ERPConfiguratorListModel erpConfiguratorListViewModel = GetClient<ERPConfiguratorService>().GetERPConfiguratorList(null, filters, null, null);

            ERPConnectorControlListModel erpConnectorControlListModel = new ERPConnectorControlListModel();

            if (erpConfiguratorListViewModel?.ERPConfiguratorList?.Count > 0)
            {
                erpConnectorControlListModel = GetClient<ERPConnectorService>().GetERPConnectorControls(erpConfiguratorListViewModel?.ERPConfiguratorList?.FirstOrDefault());
            }

            return erpConnectorControlListModel;
        }

        //Get default locale id.
        private int GetDefaultLocaleId()
        {
            return new ZnodeRepository<ZnodeLocale>().Table.FirstOrDefault(x => x.IsDefault).LocaleId;
        }

        //Get Import Mapping Data
        private List<ImportMappingModel> GetImportMappingData(ZnodeImportTemplate importTemplate)
        {
            List<ZnodeImportTemplateMapping> importMappingList = new ZnodeRepository<ZnodeImportTemplateMapping>().Table.Where(x => x.ImportTemplateId == importTemplate.ImportTemplateId)?.ToList();
            ZnodeLogging.LogMessage($"ImportTemplateId : {importTemplate.ImportTemplateId} importMappingList count: {importMappingList.Count}", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);

            List<ImportMappingModel> mappingList = null;
            if (importMappingList?.Count > 0)
            {
                //Get the template mapping list.
                mappingList = (from _item in importMappingList
                               select new ImportMappingModel
                               {
                                   MapCsvColumn = _item.SourceColumnName,
                                   MapTargetColumn = _item.TargetColumnName,
                                   MappingId = _item.ImportTemplateMappingId,
                               }).ToList();
            }
            return mappingList;
        }

        //Get Import Template Details By Template Name
        private ZnodeImportTemplate GetImportTemplateDetailsByTemplateName(string templateName)
        {
            return new ZnodeRepository<ZnodeImportTemplate>().Table.FirstOrDefault(y => y.TemplateName == templateName);
        }

        //Get Hosted Server ERP File Path
        private string GetHostedServerERPFilePath(string foldername, string fileName = null)
        {
            ZnodeLogging.LogMessage($"GetHostedServerERPFilePath : {SAPConnectorConstants.SAPConnectorFilePath}{foldername}/{fileName}", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
            return $"{SAPConnectorConstants.SAPConnectorFilePath}{foldername}/{fileName}";
        }

        #region Product Refresh
        //Append Default Product Data To CSV file
        private void AppendDefaultProductDataToCSV(string csvFilePath, bool isProductLongText = false)
        {
            string fileText = File.ReadAllText(csvFilePath);
            List<string> lines = fileText.SplitToLines().ToList();

            List<dynamic> newColumnData = new List<dynamic>() { 1, 99999, "Quantity-Based Rate", true, "Allow back-ordering of products" };

            if (lines?.Count > 0)
            {
                //add new columns to the header row
                lines[0] += $",{ZnodeConstant.MinimumQuantity},{ZnodeConstant.MaximumQuantity},{ZnodeConstant.ShippingCost},{ZnodeConstant.IsActive},{ZnodeConstant.OutOfStockOptions}";
                int index = 1;
                //add new column value for each row.
                lines.Skip(1)?.ToList()?.ForEach(line =>
                {
                    if (line?.Length > 10)
                    {
                        if (isProductLongText && lines[index]?.Substring(0, 5) == "</br>")
                        {
                            lines[index] = lines[index].Substring(5);
                        }
                        foreach (dynamic data in newColumnData)
                        {
                            lines[index] += "," + data;
                        }
                    }

                    index++;
                });
                //write the new content
                File.WriteAllLines(csvFilePath, lines);
            }
        }

        #endregion

        #region Create Order Process 

        //Create Order Json File
        private OrderJsonFileStatusEnum CreateOrderJsonFile(OrderModel orderModel, string filePath)
        {
            OrderJsonFileStatusEnum fileStatus = OrderJsonFileStatusEnum.Pending;

            if (IsNotNull(orderModel))
            {
                SAPOrderModel sapOrderModel = SAPMapper.ToSAPOrderModel(orderModel);
                string orderJson = JsonConvert.SerializeObject(sapOrderModel);
                if (!string.IsNullOrEmpty(orderJson))
                {
                    try
                    {
                        WriteTextStorage(orderJson, filePath, Mode.Write);
                        fileStatus = OrderJsonFileStatusEnum.Pending;
                    }
                    catch (Exception ex)
                    {
                        ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
                        SendSchedulerActivityLog(false, $"{currentClassName}.{GetCurrentMethod()} : {ex.Message}", "CreateOrder");
                    }
                }
                else
                {
                    ZnodeLogging.LogMessage($"Create Order json file not created for {orderModel.OrderNumber} OrderNumber.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
                    SendSchedulerActivityLog(false, $"Create Order json file not created for {orderModel.OrderNumber} OrderNumber.", "CreateOrder");
                }
            }
            else
            {
                ZnodeLogging.LogMessage("Create Order Model getting null.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
                SendSchedulerActivityLog(false, "Create Order Model getting null.", "CreateOrder");
            }
            return fileStatus;
        }

        //Set Expand For Order Details.
        private NameValueCollection SetExpandForOrderDetails()
        {
            NameValueCollection expands = new NameValueCollection
            {
                { ZnodeOmsOrderDetailEnum.ZnodeOmsPaymentState.ToString().ToLower(), ZnodeOmsOrderDetailEnum.ZnodeOmsPaymentState.ToString().ToLower() },
                { ZnodeOmsOrderDetailEnum.ZnodeOmsOrderState.ToString().ToLower(), ZnodeOmsOrderDetailEnum.ZnodeOmsOrderState.ToString().ToLower() },
                { ZnodeOmsOrderDetailEnum.ZnodePaymentType.ToString().ToLower(), ZnodeOmsOrderDetailEnum.ZnodePaymentType.ToString().ToLower() },
                { ZnodeOmsOrderDetailEnum.ZnodeOmsOrderLineItems.ToString().ToLower(), ZnodeOmsOrderDetailEnum.ZnodeOmsOrderLineItems.ToString().ToLower() },
                { ZnodeOmsOrderDetailEnum.ZnodeOmsNotes.ToString().ToLower(), ZnodeOmsOrderDetailEnum.ZnodeOmsNotes.ToString().ToLower() },
                { ZnodeOmsOrderDetailEnum.ZnodeOmsOrder.ToString().ToLower(), ZnodeOmsOrderDetailEnum.ZnodeOmsOrder.ToString().ToLower() },
                { ExpandKeys.ZnodeShipping.ToString().ToLower(), ExpandKeys.ZnodeShipping.ToString().ToLower() },
                { ExpandKeys.ZnodeUser.ToString().ToLower(), ExpandKeys.ZnodeUser.ToString().ToLower() }
            };
            ZnodeLogging.LogMessage("Get order expands parameter for ERP:", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Verbose, expands);
            return expands;
        }

        //Verify Export Directory
        private bool VerifyExportDirectory(string path)
        {
            try
            {
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                return true;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
                SendSchedulerActivityLog(false, $"{currentClassName}.{GetCurrentMethod()} : {ex.Message}", "CreateOrder");
                return false;
            }
        }

        //Prepare Required Details In OrderModel
        private OrderModel PrepareRequiredDetailsInOrderModel(OrderModel orderModel)
        {
            if (orderModel?.OmsOrderId > 0)
            {
                orderModel = GetClient<ArtifiOrderService>()?.GetOrderById(orderModel.OmsOrderId, null, SetExpandForOrderDetails());

                if (IsNotNull(orderModel))
                {
                    orderModel.AccountNumber = orderModel.AccountId > 0 ? GetAccountNumber(orderModel.AccountId) : null;

                    if (IsNull(orderModel?.ShippingAddress?.DisplayName) || string.IsNullOrEmpty(orderModel?.ShippingAddress?.Custom1))
                    {
                        int accountStoreId = 0;
                        ZnodeAddress znodeAddress = GetAddressDetails(orderModel.ShippingAddress.AddressId);
                        orderModel.ShippingAddress.DisplayName = znodeAddress?.DisplayName;
                        if (!string.IsNullOrEmpty(znodeAddress?.Custom1) && int.TryParse(znodeAddress?.Custom1, out accountStoreId))
                        {
                            accountStoreId = Convert.ToInt32(znodeAddress?.Custom1);
                        }

                        orderModel.ShippingAddress.Custom1 = accountStoreId.ToString();
                    }

                    if ((IsNull(orderModel?.BillingAddress?.DisplayName) || string.IsNullOrEmpty(orderModel?.BillingAddress?.Custom1)) && orderModel?.ShippingAddress.AddressId != orderModel?.BillingAddress.AddressId)
                    {
                        int accountStoreId = 0;
                        ZnodeAddress znodeAddress = GetAddressDetails(orderModel.BillingAddress.AddressId);
                        orderModel.BillingAddress.DisplayName = znodeAddress?.DisplayName;
                        if (!string.IsNullOrEmpty(znodeAddress?.Custom1) && int.TryParse(znodeAddress?.Custom1, out accountStoreId))
                        {
                            accountStoreId = Convert.ToInt32(znodeAddress?.Custom1);
                        }
                        orderModel.BillingAddress.Custom1 = accountStoreId.ToString();
                    }
                    else
                    {
                        orderModel.BillingAddress.DisplayName = orderModel.ShippingAddress.DisplayName;
                        orderModel.BillingAddress.Custom1 = orderModel.ShippingAddress.Custom1;
                    }
                }
            }
            return orderModel;
        }

        //Update Order File Status
        private bool UpdateOrderFileStatus(int omsOrderId, OrderJsonFileStatusEnum fileStatus)
        {
            if (omsOrderId > 0)
            {
                ZnodeRepository<ZnodeOmsOrderDetail> _orderDetailRepository = new ZnodeRepository<ZnodeOmsOrderDetail>();
                ZnodeOmsOrderDetail orderDetail = _orderDetailRepository.Table?.FirstOrDefault(x => x.OmsOrderId == omsOrderId && x.IsActive);
                if (IsNotNull(orderDetail))
                {
                    int? omsOrderStateId = new ZnodeRepository<ZnodeOmsOrderState>().Table.FirstOrDefault(x => x.OrderStateName.ToUpper() == "IN PROGRESS")?.OmsOrderStateId;

                    orderDetail.Custom5 = fileStatus.ToString();
                    if (fileStatus == OrderJsonFileStatusEnum.FilePushed && IsNotNull(omsOrderStateId) && omsOrderStateId > 0)
                    {
                        orderDetail.OmsOrderStateId = omsOrderStateId.GetValueOrDefault();
                    }

                    return _orderDetailRepository.Update(orderDetail);
                }
            }
            return false;
        }

        //Get DisplayName From Address.
        private ZnodeAddress GetAddressDetails(int addressId)
        {
            return new ZnodeRepository<ZnodeAddress>().Table.FirstOrDefault(x => x.AddressId == addressId);
        }

        //Get AccountNumber.
        private string GetAccountNumber(int accountId)
        {
            return new ZnodeRepository<ZnodeAccount>().Table.FirstOrDefault(x => x.AccountId == accountId)?.ExternalId;
        }
        #endregion

        //Process Pricing CSVFiles
        private void ProcessPricingCSVFiles(string csvFilePath)
        {
            List<string> lines = File.ReadAllLines(csvFilePath).ToList();
            if (lines?.Count > 0)
            {
                //add new columns to the header row
                lines[0] += $",TierStartQuantity,TierPrice,Custom1,Custom2,Custom3,CostPrice";
                int index = 1;
                //add new column value for each row.
                lines.Skip(1)?.ToList()?.ForEach(line =>
                {
                    lines[index] += ",,,,,,";
                    index++;
                });
                //write the new content into the file
                File.WriteAllLines(csvFilePath, lines);
            }
        }

        //Process attribute Default Value CSV Files
        private void ProcessAttributeDefaultValueCSVFiles(string csvFilePath)
        {
            List<string> lines = File.ReadAllLines(csvFilePath).ToList();
            if (lines?.Count > 0)
            {
                //add new columns to the header row
                lines[0] = $"AttributeCode,AttributeDefaultValueCode,AttributeDefaultValue,IsEditable,DisplayOrder,IsDefault,SwatchText,SwatchImage,SwatchImagePath";
                int index = 1;
                //add new column value for each row.
                lines.Skip(1)?.ToList()?.ForEach(line =>
                {
                    string[] splitedData = lines[index].Split(',');

                    if (splitedData.Count() == 3)
                    {
                        lines[index] = $"{splitedData[0]},{ReplaceAllCharacter(splitedData[2])},{splitedData[2]},true,{splitedData[1]},false,,,";
                    }

                    index++;
                });
                //write the new content into the file
                File.WriteAllLines(csvFilePath, lines);
            }
        }

        //Replace all unwanted character
        private string ReplaceAllCharacter(string data)
        {
            data = new Regex(@"[^\u0000-\u007F]", RegexOptions.Compiled).Replace(data, string.Empty);
            data = data.Replace("\"", "").Replace("&", "and").Replace("/", "or").Replace(".", "Point");
            data = Regex.Replace(data, @"[^0-9a-zA-Z]+", "");
            return data;
        }

        //Send Scheduler Activity Log
        private void SendSchedulerActivityLog(bool schedulerStatus, string errorMessage, string touchPointName)
        {
            int? erpTaskSchedulerId = new ZnodeRepository<ZnodeERPTaskScheduler>().Table.FirstOrDefault(x => x.TouchPointName.ToLower() == touchPointName.ToLower())?.ERPTaskSchedulerId;
            int? portalId = new ZnodeRepository<ZnodeDomain>().Table.FirstOrDefault(x => x.IsActive && x.IsDefault && x.ApplicationType.ToLower() == ZnodeConstant.Webstore.ToLower())?.PortalId;
            if (IsNotNull(erpTaskSchedulerId) && erpTaskSchedulerId > 0 && IsNotNull(portalId) && portalId > 0)
            {
                ERPSchedulerLogActivityModel erpSchedulerLogActivityModel = new ERPSchedulerLogActivityModel()
                {
                    SchedulerStatus = schedulerStatus,
                    ErrorMessage = errorMessage,
                    ERPTaskSchedulerId = erpTaskSchedulerId.GetValueOrDefault(),
                    PortalId = IsNotNull(portalId) && portalId > 0 ? portalId.GetValueOrDefault() : 0
                };
                GetClient<TouchPointConfigurationService>()?.SendSchedulerActivityLog(erpSchedulerLogActivityModel);
            }
        }

        //Get Orders With Missing Json Files
        private List<int> GetOrdersWithMissingJsonFiles()
        {
            int batchSize = 5;

            ERPConnectorControlListModel erpConnectorControlListModel = GetActiveERPConnectorControls();
            if (IsNotNull(erpConnectorControlListModel))
            {
                string orderProcessingBatchSize = SFTPHelper.GetConfigValue(erpConnectorControlListModel, SAPConnectorConstants.OrderProcessingBatchSize);
                if (!int.TryParse(orderProcessingBatchSize, out batchSize))
                {
                    batchSize = 5;
                }
            }

            List<int> orderDetails = (from q in new ZnodeRepository<ZnodeOmsOrderDetail>().Table
                                      orderby q.OmsOrderId
                                      where q.Custom5 == OrderJsonFileStatusEnum.Pending.ToString() && q.IsActive
                                      select q.OmsOrderId
                                 )?.Take(batchSize)?.ToList();

            return orderDetails;
        }

        //This method will provide the SQL connection
        private SqlConnection GetSqlConnection()
        {
            return new SqlConnection(HelperMethods.ConnectionString);
        }

        //private string ReadAndCreateTable(string fileName, string importType, out string colNames)
        private string ReadAndCreateTable(string fileName, string importType, Guid tableGuid, out bool isRecordPresent)
        {
            string tableName = string.Empty;
            DataTable dt = new DataTable();
            isRecordPresent = false;
            using (GenericParserAdapter parser = new GenericParserAdapter(fileName))
            {
                parser.ColumnDelimiter = ZnodeConstant.ColumnDelimiter;
                parser.FirstRowHasHeader = true;
                parser.MaxBufferSize = 32768;
                dt = parser.GetDataTable();
            }

            //added square [] bracket to avoid special character error.
            string[] columnNames = (from dc in dt.Columns.Cast<DataColumn>()
                                    select dc.ColumnName.Replace(dc.ColumnName, $"[{dc.ColumnName}]")).ToArray();

            if (IsNotNull(columnNames) && columnNames.Any())
            {
                //generate the doble hash temp table name
                tableName = $"tempdb..[##{importType}_{tableGuid}]";

                //add the same guid in datatable
                DataColumn guidCol = new DataColumn("guid", typeof(String))
                {
                    DefaultValue = tableGuid
                };
                dt.Columns.Add(guidCol);

                //create the doble hash temp table
                MakeTable(tableName, GetTableColumnsFromFirstLine(columnNames));
            }

            //If table created and it has headers then dump the CSV data in double hash temp table
            if (HelperUtility.IsNotNull(tableName) && columnNames.Count() > 0)
            {
                SaveDataInChunk(dt, tableName, out isRecordPresent);
            }

            return tableName;
        }

        //This method will create ##temp table in SQL
        private void MakeTable(string tableName, string columnList)
        {
            SqlConnection conn = GetSqlConnection();
            SqlCommand cmd = new SqlCommand("CREATE TABLE " + tableName + "  " + columnList + " ", conn);

            if (conn.State.Equals(ConnectionState.Closed))
            {
                conn.Open();
            }

            cmd.ExecuteNonQuery();
        }

        //This method will create the table columns and append the datatype to the column headers
        private string GetTableColumnsFromFirstLine(string[] firstLine)
        {
            StringBuilder sbColumnList = new StringBuilder();
            sbColumnList.Append("(");
            sbColumnList.Append(string.Join(" nvarchar(max) , ", firstLine));
            sbColumnList.Append(" nvarchar(max), guid nvarchar(max))");
            return sbColumnList.ToString();
        }

        //This method will divide the data in chunk.  The chunk size is 5000.
        private void SaveDataInChunk(DataTable dt, string tableName, out bool isRecordPresent)
        {
            if (IsNotNull(dt) && dt.Rows.Count > 0)
            {
                isRecordPresent = true;
                int chunkSize = int.Parse(ConfigurationManager.AppSettings["ZnodeImportChunkLimit"].ToString());
                int startIndex = 0;
                int totalRows = dt.Rows.Count;
                int totalRowsCount = totalRows / chunkSize;

                if (totalRows % chunkSize > 0)
                {
                    totalRowsCount++;
                }

                for (int iCount = 0; iCount < totalRowsCount; iCount++)
                {
                    DataTable fileData = dt.Rows.Cast<DataRow>().Skip(startIndex).Take(chunkSize).CopyToDataTable();
                    startIndex = startIndex + chunkSize;
                    InsertData(tableName, fileData);
                }
            }
            else
            {
                isRecordPresent = false;
            }
        }

        //This method will save the chunk data in ##temp table using Bulk upload
        private void InsertData(string tableName, DataTable fileData)
        {
            SqlConnection conn = GetSqlConnection();
            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(conn))
            {
                bulkCopy.DestinationTableName = tableName;

                if (conn.State.Equals(ConnectionState.Closed))
                {
                    conn.Open();
                }

                bulkCopy.WriteToServer(fileData);
                conn.Close();
            }
        }

        #endregion
    }
    public static class CSVReader
    {
        public static IEnumerable<string> SplitToLines(this string input)
        {
            if (input == null)
            {
                yield break;
            }

            using (System.IO.StringReader reader = new System.IO.StringReader(input))
            {
                string line = string.Empty;
                string line1 = string.Empty;
                while ((line = reader.ReadLine()) != null)
                {
                    if (!string.IsNullOrEmpty(line1))
                        line = string.Concat(line1, "</br>", line);
                    if (line.Count(x => x == '"') % 2 == 0)
                    {
                        line1 = string.Empty;
                        yield return line;
                    }
                    else
                    {
                        line1 = line;
                    }
                }
            }
        }
    }
}
