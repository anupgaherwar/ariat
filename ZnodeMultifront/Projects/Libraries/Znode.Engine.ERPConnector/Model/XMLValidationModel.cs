﻿using System.Configuration;

namespace Znode.Engine.ERPConnector.Model
{
    public class XMLValidationModel : ConfigurationElement
    {
        [ConfigurationProperty("isrequired", IsKey = true)]
        public string IsRequired
        {
            get { return (string)this["isrequired"]; }
            set { this["isrequired"] = value; }
        }
        [ConfigurationProperty("regexpattern", IsKey = true)]
        public string RegexPattern
        {
            get { return (string)this["regexpattern"]; }
            set { this["regexpattern"] = value; }
        }
    }
}
