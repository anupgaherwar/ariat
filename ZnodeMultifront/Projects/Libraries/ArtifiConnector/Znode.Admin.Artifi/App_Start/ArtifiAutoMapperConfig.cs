﻿using AutoMapper;
using Znode.Engine.Admin.ViewModel;
using Znode.Engine.Admin.ViewModels;
using Znode.Engine.Api.Models;
using Znode.Artifi.Api.Model;

namespace Znode.Engine.Admin
{
    public static class ArtifiAutoMapperConfig
    {
        public static void Execute()
        {
            Mapper.CreateMap<StoreFeatureViewModel, PortalFeatureModel>().ReverseMap();
            Mapper.CreateMap<PortalCustomizationSettingViewModel, PortalCustomizationSettingModel>().ReverseMap();
            Mapper.CreateMap<ArtifiCartItemViewModel, ArtifiShoppingCartItemModel>().ReverseMap();
        }            
    }
}