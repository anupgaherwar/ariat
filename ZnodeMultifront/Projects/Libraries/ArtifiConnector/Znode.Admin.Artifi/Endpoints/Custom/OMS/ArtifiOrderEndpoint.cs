﻿using System;
using Znode.Engine.Api.Client.Endpoints;

namespace Znode.Engine.Admin.Endpoints
{
    public class ArtifiOrderEndpoint : BaseEndpoint
    {
        //Get artifi credentials        
        public static string GetPortalCustomizationArtifiSetting(int portalId) => $"{ApiRoot}/artifiportal/getportalcustomizationsetting/{portalId}";

        public static string UpdateCartItemDesign() => $"{ApiRoot}/artifiorder/updatecartitemdesign";

        public static string UpdateSavedCartItemDesign() => $"{ApiRoot}/artifiorder/updatesavedcartitemdesign";

    }
}