﻿using Znode.Engine.Api.Client.Endpoints;

namespace Znode.Engine.Admin.Endpoints
{
    public class ArtifiPortalEndpoint : BaseEndpoint
    {
        internal static string CreateEditCustomizationSetting() => $"{ApiRoot}/artifiportal/createeditcustomizationsetting";

        internal static string GetPortalCustomizationSetting(int portalId) => $"{ApiRoot}/artifiportal/getportalcustomizationsetting/{portalId}";
    
    }
}