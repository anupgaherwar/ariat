﻿using System;
using Znode.Engine.Admin.ViewModel;
using Znode.Engine.Admin.ViewModels;
using Znode.Artifi.Api.Model;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
using System.Collections.Generic;
using System.Linq;

namespace Znode.Engine.Admin.Maps
{
    public class ArtifiCartItemViewModelMap
    {
        public static ArtifiCartItemViewModel ToViewModel(CartItemViewModel cartItem, ArtifiDesignDetailsModel artifiDesignDetailsModel)
        {
            ArtifiCartItemViewModel artifiCartItemViewModel = new ArtifiCartItemViewModel();
            if (IsNotNull(cartItem))
            {
                artifiCartItemViewModel.AddOnProductSKUs = cartItem.AddOnProductSKUs;
                artifiCartItemViewModel.AutoAddonSKUs = cartItem.AutoAddonSKUs;
                artifiCartItemViewModel.BundleProductSKUs = cartItem.BundleProductSKUs;
                artifiCartItemViewModel.CartDescription = cartItem.CartDescription;
                artifiCartItemViewModel.ConfigurableProductSKUs = cartItem.ConfigurableProductSKUs;
                artifiCartItemViewModel.CurrencyCode = cartItem.CurrencyCode;
                artifiCartItemViewModel.ExtendedPrice = cartItem.ExtendedPrice;
                artifiCartItemViewModel.ExternalId = cartItem.ExternalId;
                artifiCartItemViewModel.GroupProducts = cartItem.GroupProducts;
                artifiCartItemViewModel.GroupProductSKUs = cartItem.GroupProductSKUs;
                artifiCartItemViewModel.GroupProductsQuantity = cartItem.GroupProductsQuantity;
                artifiCartItemViewModel.ImagePath = string.IsNullOrEmpty(artifiDesignDetailsModel?.AISavedDesigns) ? cartItem.ImagePath : artifiDesignDetailsModel?.AISavedDesigns;
                artifiCartItemViewModel.InsufficientQuantity = cartItem.InsufficientQuantity;
                artifiCartItemViewModel.IsAllowedTerritories = cartItem.IsAllowedTerritories;
                artifiCartItemViewModel.MaxQuantity = cartItem.MaxQuantity;
                artifiCartItemViewModel.MinQuantity = cartItem.MinQuantity;
                artifiCartItemViewModel.PersonalisedCodes = cartItem.PersonalisedCodes;
                artifiCartItemViewModel.PersonalisedValues = cartItem.PersonalisedValues;
                artifiCartItemViewModel.PersonaliseValuesList = cartItem.PersonaliseValuesList;
                artifiCartItemViewModel.ProductCode = cartItem.ProductCode;
                artifiCartItemViewModel.ProductName = cartItem.ProductName;
                artifiCartItemViewModel.ProductType = cartItem.ProductType;
                artifiCartItemViewModel.Quantity = cartItem.Quantity;


                artifiCartItemViewModel.ShippingId = cartItem.ShippingId;
                artifiCartItemViewModel.SKU = cartItem.SKU;
                artifiCartItemViewModel.Total = cartItem.Total;
                artifiCartItemViewModel.UnitPrice = cartItem.UnitPrice;
                artifiCartItemViewModel.UOM = cartItem.UOM;
                artifiCartItemViewModel.UserId = cartItem.UserId;
                artifiCartItemViewModel.OrderLineItemsDesignId = Convert.ToInt32(artifiDesignDetailsModel?.OrderLineItemsDesignId);
                artifiCartItemViewModel.AISavedDesigns = artifiDesignDetailsModel?.AISavedDesigns;
                artifiCartItemViewModel.CatalogId = cartItem.CatalogId;
                artifiCartItemViewModel.CustomQuantity = cartItem.CustomQuantity;
                artifiCartItemViewModel.GroupProductNames = cartItem.GroupProductNames;
                artifiCartItemViewModel.IsActive = cartItem.IsActive;
                artifiCartItemViewModel.IsEditStatus = cartItem.IsEditStatus;
                artifiCartItemViewModel.IsReviewPage = cartItem.IsReviewPage;
                artifiCartItemViewModel.IsSendEmail = cartItem.IsSendEmail;
                artifiCartItemViewModel.LocaleId = cartItem.LocaleId;
                artifiCartItemViewModel.OmsOrderId = cartItem.OmsOrderId;
                artifiCartItemViewModel.OmsOrderLineItemsId = cartItem.OmsOrderLineItemsId;
                artifiCartItemViewModel.OrderLineItemStatus = cartItem.OrderLineItemStatus;
                artifiCartItemViewModel.OrderLineItemStatusId = cartItem.OrderLineItemStatusId;
                artifiCartItemViewModel.PortalId = cartItem.PortalId;
                artifiCartItemViewModel.PublishProductId = cartItem.PublishProductId;
                artifiCartItemViewModel.ShippingCost = cartItem.ShippingCost;
                artifiCartItemViewModel.ShippingStatusList = cartItem.ShippingStatusList;
                artifiCartItemViewModel.ShipSeperately = cartItem.ShipSeperately;
                artifiCartItemViewModel.TrackingNumber = cartItem.TrackingNumber;
                artifiCartItemViewModel.TrackingUrl = cartItem.TrackingUrl;
                artifiCartItemViewModel.IsConfigurableProduct = Convert.ToBoolean(artifiDesignDetailsModel?.IsConfigurableProduct);
                artifiCartItemViewModel.IsGroupProduct = Convert.ToBoolean(artifiDesignDetailsModel?.IsGroupProduct);
                //artifiCartItemViewModel.OmsSavedCartLineItemId = cartItem.OmsSavedCartLineItemId;
                //artifiCartItemViewModel.LineItemDetails = cartItem.LineItemDetails;
                return artifiCartItemViewModel;
            }
            return artifiCartItemViewModel;
        }

        public static List<ArtifiShoppingCartItemModel> ToArtifiCartItemList(ArtifiShoppingCartModel artifiShoppingCartModel)
        {
            List<ArtifiShoppingCartItemModel> artifiShoppingCartItemModelList = new List<Artifi.Api.Model.ArtifiShoppingCartItemModel>();
            foreach (ArtifiDesignDetailsModel designDetailItem in artifiShoppingCartModel?.DetailsModel ?? new List<ArtifiDesignDetailsModel>())
            {
                artifiShoppingCartItemModelList.Add(new Artifi.Api.Model.ArtifiShoppingCartItemModel() { OrderLineItemsDesignId = designDetailItem.OrderLineItemsDesignId, AIImagePath = designDetailItem.AISavedDesigns, IsConfigurableProduct = designDetailItem.IsConfigurableProduct, IsGroupProduct = designDetailItem.IsGroupProduct, OmsSavedCartLineItemId = designDetailItem.OmsSavedCartLineItemId });
            }
            return artifiShoppingCartItemModelList ?? new List<ArtifiShoppingCartItemModel>();

        }
    }
}