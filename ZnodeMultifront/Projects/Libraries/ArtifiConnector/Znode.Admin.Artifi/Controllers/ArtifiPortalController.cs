﻿using System.Web.Mvc;
using Znode.Engine.Admin.Agents;
using Znode.Engine.Admin.ViewModel;
using Znode.Engine.Admin.ViewModels;
using Znode.Engine.Api.Client.Expands;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Resources;

namespace Znode.Engine.Admin.Controllers
{
    public class ArtifiPortalController : BaseController
    {
        #region Private Variable       
        private readonly IArtifiPortalAgent _customPortalAgent;
        private readonly IStoreAgent _portalAgent;
        private const string createEditView = "";
        #endregion

        #region Constructor
        public ArtifiPortalController(IArtifiPortalAgent customPortalAgent, IStoreAgent portalAgent)
        {
            _customPortalAgent = customPortalAgent;
            _portalAgent = portalAgent;
        }
        #endregion

        #region Public Methods
        [HttpGet]
        public virtual ActionResult GetProductCustomizationSetting(int portalId)
        {
            ActionResult action = GotoBackURL();

            var model = _customPortalAgent.GetPortalCustomizationSetting(portalId);
            return HelperUtility.IsNotNull(action) ? action : View("CreateEditCustomizationSetting", model);
        }

        public ActionResult CreateEditCustomizationSetting(PortalCustomizationSettingViewModel storeViewModel)
        {
            if (ModelState.IsValid)
            {
                PortalCustomizationSettingViewModel portalCustomizationSettingViewModel = _customPortalAgent.CreateEditCustomizationSetting(storeViewModel);
                SetNotificationMessage(portalCustomizationSettingViewModel.HasError || HelperUtility.IsNull(portalCustomizationSettingViewModel) ? GetErrorNotificationMessage(Admin_Resources.SaveErrorMessage)
                  : GetSuccessNotificationMessage(Admin_Resources.SaveMessage));

                return RedirectToAction<ArtifiPortalController>(x => x.GetProductCustomizationSetting(portalCustomizationSettingViewModel.PortalId));
            }

            return RedirectToAction<ArtifiPortalController>(x => x.GetProductCustomizationSetting(storeViewModel.PortalId));
        }
        #endregion
    }
}