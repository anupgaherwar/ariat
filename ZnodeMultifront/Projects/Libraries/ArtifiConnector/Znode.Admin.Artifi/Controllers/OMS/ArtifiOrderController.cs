﻿using System;
using System.Web.Mvc;
using Znode.Artifi.Api.Model;
using Znode.Engine.Admin.Agents;
using Znode.Engine.Admin.Helpers;
using Znode.Engine.Admin.ViewModel;
using Znode.Engine.Admin.ViewModels;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;

namespace Znode.Engine.Admin.Controllers
{
    public class ArtifiOrderController : OrderController//BaseController
    {
        #region Private ReadOnly members
        private readonly IArtifiOrderAgent _artifiOrderAgent;
        private readonly IOrderAgent _orderAgent;
        private readonly ICartAgent _cartAgent;

        private const string orderLineItemView = "_OrderLineItemList";
        private const string ShoppingCartView = "ShoppingCart";
        #endregion

        #region Constructor
        public ArtifiOrderController(IOrderAgent orderAgent, IUserAgent userAgent, IShippingAgent shippingAgent, ICartAgent cartAgent, IAccountQuoteAgent quoteAgent, IStoreAgent storeAgent, IRMARequestAgent rmaRequestAgent, IWebSiteAgent websiteAgent, IArtifiOrderAgent artifiOrderAgent) : base(orderAgent, userAgent, shippingAgent, cartAgent, quoteAgent, storeAgent, rmaRequestAgent, websiteAgent)
        {
            _orderAgent = orderAgent;
            _cartAgent = cartAgent;
            _artifiOrderAgent = artifiOrderAgent;
        }
        //public ArtifiOrderController(IArtifiOrderAgent artifiOrderAgent)
        //{
        //    _artifiOrderAgent = artifiOrderAgent;
        //}
        #endregion

        public ActionResult GetArtifiCredentials(int portalId, int userId = 0)
        {
            PortalCustomizationSettingViewModel portalCustomizationSettingViewModel = _artifiOrderAgent.GetArtifiCredentials(portalId);
            return Json(new
            {
                WebApiClientKey = portalCustomizationSettingViewModel.WebApiClientKey,
                WebsiteCode = portalCustomizationSettingViewModel.WebsiteCode,
                JsUrl = portalCustomizationSettingViewModel.DomainName,
                userId = userId
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult UpdateArtifiCartItem(int OrderLineItemsDesignId, string AIImagePath, int OmsOrderId, decimal quantity)
        {
            bool status = _artifiOrderAgent.UpdateArtifiCartItem(new ArtifiShoppingCartItemModel { OrderLineItemsDesignId = OrderLineItemsDesignId, AIImagePath = AIImagePath, OmsOrderId = OmsOrderId, Quantity = quantity });

            return Json(new
            {
                designId = OrderLineItemsDesignId,
                quantity = quantity,
                status = status
            }, JsonRequestBehavior.AllowGet);
        }

        //Open the panel for Add and Edit Artifi Customisation window.
        public ActionResult AddEditArtifiDesign(bool isEdit = false)
        {
            ViewBag.isEdit = isEdit;
            return PartialView("~/Views/ArtifiOrder/_AddEditArtifiDesign.cshtml");
        }

        //Open the panel for Add and Edit Artifi Customized product.
        public ActionResult PreviewArtifiDesign(string url)
        {
            ViewBag.url = url;
            return PartialView("~/Views/ArtifiOrder/_PreviewArtifiDesign.cshtml");
        }

        public override ActionResult Manage(int OmsOrderId, int accountId = 0, string updatePageType = null)
        {
            OrderViewModel orderViewModel = _orderAgent.Manage(OmsOrderId);
            if (IsNotNull(TempData["SuccessMessage"]))
                SetNotificationMessage(GetSuccessNotificationMessage(Convert.ToString(TempData["SuccessMessage"])));
            else if (IsNotNull(TempData["ErrorMessage"]))
                SetNotificationMessage(GetErrorNotificationMessage(Convert.ToString(TempData["ErrorMessage"])));
            orderViewModel.UpdatePageType = updatePageType;
            return ActionView("Manage", orderViewModel);
        }

        public override ActionResult GetPublishProduct(int publishProductId, int localeId, int portalId, int? userId = 0, int? catalogId = 0, bool isQuote = false) => ActionView("_ProductDetails", _orderAgent.GetPublishProduct(publishProductId, localeId, portalId, Convert.ToInt32(userId), Convert.ToInt32(catalogId), isQuote));

        public override ActionResult AddProductToCart(bool cartItems, int orderId, int? userId = 0)
        {
            CartViewModel cartViewModel = _cartAgent.AddProductToCart(cartItems, orderId, userId);
            _orderAgent.SetTrackingUrlByOrderId(orderId, cartViewModel?.ShoppingCartItems);

            string shoppingCartView = RenderRazorViewToString(orderId > 0 ? orderLineItemView : ShoppingCartView, cartViewModel);
            return Json(new
            {
                html = shoppingCartView,
                coupons = cartViewModel.Coupons
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public override ActionResult GetReviewOrder(CreateOrderViewModel createOrderModel)
   => ActionView(AdminConstants.CheckoutReviewView, _orderAgent.GetReviewOrder(createOrderModel));

    }
}