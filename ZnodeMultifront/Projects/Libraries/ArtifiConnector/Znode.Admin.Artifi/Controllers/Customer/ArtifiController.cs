﻿using System.Web.Mvc;
using Znode.Engine.Admin.Agents;
using Znode.Engine.Admin.ViewModels;

namespace Znode.Engine.Admin.Controllers
{
    public class ArtifiController : BaseController
    {
        private readonly IUserAgent _userAgent;
        private const string getAllArtifiDesign = "GetAllArtifiDesign";
        public ArtifiController(IUserAgent userAgent)
        {
            _userAgent = userAgent;
        }

        [HttpGet]
        public ActionResult GetAllArtifiDesign(int userId)
        {
            if (userId > 0)
            {
                CustomerViewModel customerAccountDetails = _userAgent.GetCustomerAccountById(userId);

                if (customerAccountDetails.UserId > 0)
                    _userAgent.SetCustomerViewModel(customerAccountDetails);

                return ActionView(getAllArtifiDesign, customerAccountDetails);
            }

            else
                return ActionView(getAllArtifiDesign, new CustomerViewModel());

        }
    }
}