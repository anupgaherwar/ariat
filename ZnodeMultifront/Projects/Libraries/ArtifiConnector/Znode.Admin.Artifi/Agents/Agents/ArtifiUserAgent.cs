﻿using Znode.Engine.Api.Client;

namespace Znode.Engine.Admin.Agents
{
    //This is sample custom code implementation structure.
    //Here CustomUserAgent demonstrate override of Logout() of znode base code.
    public class ArtifiUserAgent : UserAgent
    {
        public ArtifiUserAgent(IUserClient userClient, IPortalClient portalClient, IAccountClient accountClient, IRoleClient roleClient, IDomainClient domainClient, IStateClient stateClient, IGlobalAttributeEntityClient globalAttributeEntityClient, IShoppingCartClient shoppingCartClient) : base(userClient, portalClient, accountClient, roleClient, domainClient, stateClient, globalAttributeEntityClient, shoppingCartClient)
        {
        }

        //public ArtifiUserAgent(IUserClient userClient, IPortalClient portalClient, IAccountClient accountClient, IRoleClient roleClient, IDomainClient domainClient, IStateClient stateClient, IGlobalAttributeEntityClient globalAttributeEntityClient) : base(userClient, portalClient, accountClient, roleClient, domainClient, stateClient, globalAttributeEntityClient)
        //{
        //}

        //public ArtifiUserAgent(IUserClient userClient, IPortalClient portalClient, IAccountClient accountClient, IRoleClient roleClient, IDomainClient domainClient)
        //    :base(userClient,portalClient,accountClient,roleClient,domainClient)
        //{

        //}

        //"Sample for znode base code override".
        //Here we have over ride the log out method of znode base code i.e. UserAgent.
        //Like wise you can override any znode base code.
        public override void Logout()
        {
            base.Logout();
        }

    }
}