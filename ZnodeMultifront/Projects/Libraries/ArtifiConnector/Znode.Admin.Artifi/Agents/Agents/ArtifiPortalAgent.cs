﻿using Znode.Artifi.Api.Model;
using Znode.Engine.Admin.Clients;
using Znode.Engine.Admin.Extensions;
using Znode.Engine.Admin.ViewModel;
using Znode.Engine.Api.Client.Expands;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Resources;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;

namespace Znode.Engine.Admin.Agents
{
    //This is sample custom code implementation structure.
    //Here CustomPortalAgent demonstrate CRUD operations custom store.
    //Here CRUD operations for custom store with three custom fields i.e. (CustomData1, CustomData2, CustomData3) are done.
    public class ArtifiPortalAgent : BaseAgent, IArtifiPortalAgent
    {
        #region Private Variables
        private readonly IArtifiPortalClient _customPortalClient;

        #endregion

        #region Constructor
        public ArtifiPortalAgent()
        {
            _customPortalClient = GetClient<ArtifiPortalClient>();
        }
        #endregion

        #region Public Methods

        //Get portal product customization setting
        public PortalCustomizationSettingViewModel GetPortalCustomizationSetting(int portalId)
       => (portalId > 0) ? (_customPortalClient.GetPortalCustomizationSetting(portalId, SetExpandForPortal())).ToViewModel<PortalCustomizationSettingViewModel>() : new PortalCustomizationSettingViewModel { PortalId = portalId };

        //create edit portal product customization setting
        public PortalCustomizationSettingViewModel CreateEditCustomizationSetting(PortalCustomizationSettingViewModel viewModel)
        {
            try
            {
                PortalCustomizationSettingModel portalcustomizationsettingmodel = _customPortalClient.CreateEditCustomizationSetting(viewModel.ToModel<PortalCustomizationSettingModel>());
                return IsNotNull(portalcustomizationsettingmodel) ? portalcustomizationsettingmodel.ToViewModel<PortalCustomizationSettingViewModel>() : new PortalCustomizationSettingViewModel();
            }
            catch
            {
                return (PortalCustomizationSettingViewModel)GetViewModelWithErrorMessage(viewModel, Admin_Resources.UpdateErrorMessage);
            }
        }
        #endregion

        #region Private Methods

        private ExpandCollection SetExpandForPortal()
        {
            ExpandCollection expands = new ExpandCollection();
            expands.Add(ZnodeGoogleTagManagerEnum.ZnodePortal.ToString());
            return expands;
        }

        #endregion
    }
}