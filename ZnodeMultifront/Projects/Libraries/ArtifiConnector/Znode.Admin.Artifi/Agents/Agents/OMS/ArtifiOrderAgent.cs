﻿using ArtifiZnode.Engine.Admin.Helper;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Znode.Artifi.Api.Model;
using Znode.Engine.Admin.Clients;
using Znode.Engine.Admin.Helpers;
using Znode.Engine.Admin.ViewModel;
using Znode.Engine.Admin.ViewModels;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;

using static Znode.Libraries.ECommerce.Utilities.HelperUtility;

namespace Znode.Engine.Admin.Agents
{
    public class ArtifiOrderAgent : OrderAgent, IArtifiOrderAgent
    {
        #region Private Variable       
        private readonly IArtifiOrderClient _artifiOrderClient;
        private readonly IOrderClient _orderClient;
        #endregion

        #region Constructor

        public ArtifiOrderAgent(IShippingClient shippingClient, IShippingTypeClient shippingTypeClient, IStateClient stateClient,
             ICityClient cityClient, IProductsClient productClient, IBrandClient brandClient,
             IUserClient userClient, IPortalClient portalClient, IAccountClient accountClient, IRoleClient roleClient,
             IDomainClient domainClient, IOrderClient orderClient, IEcommerceCatalogClient ecomCatalogClient,
             ICustomerClient customerClient, IPublishProductClient publishProductClient, IMediaConfigurationClient mediaConfigClient,
             IPaymentClient paymentClient, IShoppingCartClient shoppingCartClient, IAccountQuoteClient accountQuoteClient,
             IOrderStateClient orderStateClient, IPIMAttributeClient pimAttributeClient, ICountryClient countryClient, IAddressClient addressClient, IArtifiOrderClient artifiOrderClient) : base 
            (shippingClient, shippingTypeClient, stateClient, cityClient, productClient, brandClient, userClient, portalClient, accountClient, roleClient,
              domainClient,  orderClient,  ecomCatalogClient,  customerClient,  publishProductClient,  mediaConfigClient,
             paymentClient,  shoppingCartClient,  accountQuoteClient,
            orderStateClient,  pimAttributeClient,  countryClient,  addressClient)
        {
            _artifiOrderClient = GetClient<IArtifiOrderClient>(artifiOrderClient);
            _orderClient = GetClient<IOrderClient>(orderClient);
        }

        #endregion

        #region Public Methods

        public bool UpdateArtifiCartItem(ArtifiShoppingCartItemModel artifiShoppingCartItemModel)
        {
            bool status = false;
            if (artifiShoppingCartItemModel.OmsOrderId > 0)
            {
                ArtifiOrderModel artifiOrderModel = GetFromSession<ArtifiOrderModel>(ArtifiZnodeAdminConstants.ArtifiOrderModel + artifiShoppingCartItemModel.OmsOrderId);

                foreach (ArtifiShoppingCartItemModel artifiShoppingCartItem in artifiOrderModel?.ArtifiShoppingCartItemListModel ?? new List<ArtifiShoppingCartItemModel>())
                {
                    if (Equals(artifiShoppingCartItemModel.OrderLineItemsDesignId, artifiShoppingCartItem.OrderLineItemsDesignId))
                    {
                        artifiShoppingCartItem.AIImagePath = artifiShoppingCartItemModel.AIImagePath;
                        artifiShoppingCartItem.Quantity = artifiShoppingCartItemModel.Quantity;
                    }
                }
                status = _artifiOrderClient.UpdateCartItemDesign(artifiShoppingCartItemModel);
                SaveInSession<ArtifiOrderModel>(ArtifiZnodeAdminConstants.ArtifiOrderModel + artifiShoppingCartItemModel.OmsOrderId, artifiOrderModel);

            }
            else
            {
                status = _artifiOrderClient.UpdateSavedCartItemDesign(artifiShoppingCartItemModel);
            }

            return status;
        }

        public PortalCustomizationSettingViewModel GetArtifiCredentials(int portalId)
        {
            PortalCustomizationSettingModel portalCustomizationSettingModel = _artifiOrderClient.GetArtifiCredentials(portalId);

            PortalCustomizationSettingViewModel portalCustomizationSettingViewModel = new PortalCustomizationSettingViewModel() { WebApiClientKey = portalCustomizationSettingModel.WebApiClientKey, WebsiteCode = portalCustomizationSettingModel.WebsiteCode, DomainName = portalCustomizationSettingModel.DomainName };
            if (IsNotNull(portalCustomizationSettingViewModel))
                return portalCustomizationSettingViewModel;
            else
                return new ViewModel.PortalCustomizationSettingViewModel();
        }

        public override CartViewModel GetOrderLineItems(int orderId)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            OrderModel orderModel = GetFromSession<OrderModel>(AdminConstants.OMSOrderSessionKey + orderId);
            SetPersonaliseForShoppingCart(orderModel.ShoppingCartModel, orderModel.OrderLineItems);
            if (IsNotNull(orderModel?.ShoppingCartModel))
            {
                CartViewModel cartViewModel = GetCartOrderStatusList(orderModel?.ShoppingCartModel, orderModel.TrackingUrl);
                cartViewModel.ShippingName = orderModel.ShippingTypeName;
                cartViewModel.OmsOrderId = orderId;
                cartViewModel.UserId = cartViewModel.UserId > 0 ? cartViewModel.UserId : orderModel.UserId;
                cartViewModel.LocaleId = cartViewModel.LocaleId > 0 ? cartViewModel.LocaleId : orderModel.ShoppingCartModel.LocaleId;

                foreach (CartItemViewModel cartItem in cartViewModel.ShoppingCartItems)
                {
                    foreach (OrderLineItemModel orderLineItem in orderModel?.OrderLineItems)
                    {
                        if (cartItem.OmsOrderLineItemsId == orderLineItem.OmsOrderLineItemsId)
                        {
                            cartItem.Custom1 = orderLineItem.Custom1;
                            cartItem.UnitPrice = Convert.ToDecimal(orderLineItem.Custom3);
                            cartItem.ExtendedPrice = cartItem.UnitPrice * cartItem.Quantity;
                        }
                    }
                    cartItem.TrackingUrl = orderModel.TrackingUrl;
                }
                cartViewModel.ShoppingCartItems?.ForEach(x => x.TrackingUrl = orderModel.TrackingUrl);
                cartViewModel.OrderState = orderModel?.OrderState;
                cartViewModel.Discount = orderModel.DiscountAmount;
                SetAdditionalCostForShoppingCart(cartViewModel, orderModel.OrderLineItems);
                SaveLineItemHistorySession(orderModel?.ShoppingCartModel.ShoppingCartItems);
                return cartViewModel;
            }
            return new CartViewModel();
        }

        #endregion

        #region Private Methods
        //To Set personalise attribute in ShoppingCartModel.
        private void SetPersonaliseForShoppingCart(ShoppingCartModel shoppingCartModel, List<OrderLineItemModel> orderLineItemModel)
        {
            List<OrderLineItemModel> orderLineModel = orderLineItemModel?.Where(w => w.ParentOmsOrderLineItemsId == null && w.OrderLineItemState != ZnodeOrderStatusEnum.RETURNED.ToString() && w.PersonaliseValueList.Count > 0).ToList();
            ZnodeLogging.LogMessage("OrderLineItemModel list count:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { OrderLineModelCount = orderLineModel?.Count });
            if (IsNotNull(orderLineModel))
                foreach (ShoppingCartItemModel shoppingCart in shoppingCartModel.ShoppingCartItems)
                {
                    shoppingCart.PersonaliseValuesList = GetPersonaliseAttributeById(shoppingCart.OmsOrderLineItemsId, orderLineModel);
                    shoppingCart.PersonaliseValuesDetail = orderLineItemModel.Where(x => x.OmsOrderLineItemsId == shoppingCart.OmsOrderLineItemsId)?.FirstOrDefault()?.PersonaliseValuesDetail;
                    shoppingCart.GroupId = orderLineItemModel.Where(x => x.OmsOrderLineItemsId == shoppingCart.OmsOrderLineItemsId).Select(y => y.GroupId).FirstOrDefault();
                }
            ZnodeLogging.LogMessage("ShoppingCartItems count:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { ShoppingCartItemsCount = shoppingCartModel?.ShoppingCartItems?.Count });
        }

        //To Set personalise attribute in ShoppingCartModel.
        private void SetAdditionalCostForShoppingCart(CartViewModel cartViewModel, List<OrderLineItemModel> orderLineItemModel)
        {
            if (IsNotNull(cartViewModel) && IsNotNull(cartViewModel.ShoppingCartItems))
                cartViewModel.ShoppingCartItems.ForEach(x =>
                {
                    var _item = orderLineItemModel.FirstOrDefault(y => y.OmsOrderLineItemsId == x.OmsOrderLineItemsId);

                    x.AdditionalCost = _item?.AdditionalCost;
                    x.CartDescription = _item?.Description;
                    //x.DownloadableProductKey = _item?.DownloadableProductKey;
                });
        }

        private void SaveLineItemHistorySession(List<ShoppingCartItemModel> ShoppingCartItems)
        {
            if (ShoppingCartItems.Any())
            {
                ZnodeLogging.LogMessage("ShoppingCartItems count: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { ShoppingCartItemsCount = ShoppingCartItems?.Count });
                IDictionary<string, Tuple<string, string>> lineItemHistory = new Dictionary<string, Tuple<string, string>>();
                foreach (var item in ShoppingCartItems)
                {
                    if (lineItemHistory.Any(x => x.Key != item.SKU))
                        lineItemHistory.Add(GetProductSKU(item), GetItemHistory(item));
                }

                SaveInSession(AdminConstants.LineItemHistorySession, lineItemHistory);
            }
        }


        private Dictionary<string, object> GetPersonaliseAttributeById(int omsOrderLineItemsId, List<OrderLineItemModel> orderLineItemModel)
         => orderLineItemModel.Where(x => x.OmsOrderLineItemsId == omsOrderLineItemsId)?.FirstOrDefault()?.PersonaliseValueList;

        internal static string GetProductSKU(ShoppingCartItemModel cartItem)
        {
            if (cartItem.GroupProducts.Any())
            {
                if (!string.IsNullOrEmpty(cartItem.AddOnProductSKUs))
                    return (!string.IsNullOrEmpty(cartItem.GroupId)) ? $"{cartItem.SKU}_{cartItem.GroupProducts.FirstOrDefault()?.Sku}_{cartItem.AddOnProductSKUs}_{cartItem.GroupId}" : $"{cartItem.SKU}_{cartItem.GroupProducts.FirstOrDefault()?.Sku}_{cartItem.AddOnProductSKUs}";
                else if (!string.IsNullOrEmpty(cartItem.AutoAddonSKUs))
                    return (!string.IsNullOrEmpty(cartItem.GroupId)) ? $"{cartItem.SKU}_{cartItem.GroupProducts.FirstOrDefault()?.Sku}_{cartItem.AutoAddonSKUs}_{cartItem.GroupId}" : $"{cartItem.SKU}_{cartItem.GroupProducts.FirstOrDefault()?.Sku}_{cartItem.AutoAddonSKUs}";
                else
                    return (!string.IsNullOrEmpty(cartItem.GroupId)) ? $"{cartItem.SKU}_{cartItem.GroupProducts.FirstOrDefault()?.Sku}_{cartItem.GroupId}" : $"{cartItem.SKU}_{cartItem.GroupProducts.FirstOrDefault()?.Sku}";
            }
            if (!string.IsNullOrEmpty(cartItem.ConfigurableProductSKUs))
                return (!string.IsNullOrEmpty(cartItem.GroupId)) ? $"{cartItem.SKU}_{cartItem.ConfigurableProductSKUs}_{cartItem.GroupId}" : $"{cartItem.SKU}_{cartItem.ConfigurableProductSKUs}";
            if (!string.IsNullOrEmpty(cartItem.AddOnProductSKUs))
                return (!string.IsNullOrEmpty(cartItem.GroupId)) ? $"{cartItem.SKU}_{cartItem.AddOnProductSKUs}_{cartItem.GroupId}" : $"{cartItem.SKU}_{cartItem.AddOnProductSKUs}";

            if (!string.IsNullOrEmpty(cartItem.AutoAddonSKUs))
                return (!string.IsNullOrEmpty(cartItem.GroupId)) ? $"{cartItem.SKU}_{cartItem.AutoAddonSKUs}_{cartItem.GroupId}" : $"{cartItem.SKU}_{cartItem.AutoAddonSKUs}";

            return (!string.IsNullOrEmpty(cartItem.GroupId)) ? $"{cartItem.SKU}_{cartItem.GroupId}" : $"{cartItem.SKU}";
        }

        internal static Tuple<string, string> GetItemHistory(ShoppingCartItemModel cartItem)
        {
            if (cartItem.GroupProducts.Any())
                return Tuple.Create(GetProductSKU(cartItem), $"{cartItem.GroupProducts.FirstOrDefault()?.Quantity}");

            return Tuple.Create(GetProductSKU(cartItem), $"{cartItem.Quantity}");
        } 
        #endregion
    }
}
