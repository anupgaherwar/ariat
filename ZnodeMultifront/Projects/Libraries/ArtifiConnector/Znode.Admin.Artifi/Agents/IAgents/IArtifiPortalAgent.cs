﻿using Znode.Engine.Admin.ViewModel;

namespace Znode.Engine.Admin.Agents
{
    public interface IArtifiPortalAgent
    {
        /// <summary>
        /// get portal product customization setting
        /// </summary>
        /// <param name="portalId">portal id.</param>
        /// <returns>model with portal customization data.</returns>
        PortalCustomizationSettingViewModel GetPortalCustomizationSetting(int portalId);

        /// <summary>
        /// create edit portal customzation setting.
        /// </summary>
        /// <param name="storeViewModel">Model with customization setting</param>
        /// <returns>create model data.</returns>
        PortalCustomizationSettingViewModel CreateEditCustomizationSetting(PortalCustomizationSettingViewModel storeViewModel);
    }
}
