﻿using Znode.Artifi.Api.Model;
using Znode.Engine.Admin.ViewModel;

namespace Znode.Engine.Admin.Agents
{
    public interface IArtifiOrderAgent
    {
        /// <summary>
        /// Get artifi vredentials.
        /// </summary>
        /// <param name="portalId"></param>
        /// <returns>PortalCustomizationSettingViewModel</returns>
        PortalCustomizationSettingViewModel GetArtifiCredentials(int portalId);
        
        /// <summary>
        /// Update customised cart item.
        /// </summary>
        /// <param name="artifiShoppingCartItemModel"></param>
        /// <returns>true/false</returns>
        bool UpdateArtifiCartItem(ArtifiShoppingCartItemModel artifiShoppingCartItemModel);

    }
}
