﻿using System.ComponentModel.DataAnnotations;

namespace Znode.Engine.Admin.ViewModels
{
    public class ArtifiPortalDetailViewModel : StoreViewModel
    {
        public int CustomePortalDetailsId { get; set; }
        public string PortalName { get; set; }
        [Required]
        public string CustomeData1 { get; set; }
        public string CustomeData2 { get; set; }
        public string CustomeData3 { get; set; }
    }
}