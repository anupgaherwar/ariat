﻿using System.Collections.Generic;

namespace Znode.Engine.Admin.ViewModels
{
    public class ArtifiReviewOrderViewModel : BaseViewModel
    {
        public ReviewOrderViewModel ReviewOrderModel { get; set; }
        public List<ArtifiCartItemViewModel> ArtifiCartItemViewListModel { get; set; }
    }
}
