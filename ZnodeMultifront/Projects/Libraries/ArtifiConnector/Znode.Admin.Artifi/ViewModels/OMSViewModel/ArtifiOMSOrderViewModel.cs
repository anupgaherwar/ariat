﻿using System.Collections.Generic;
using Znode.Artifi.Api.Model;

namespace Znode.Engine.Admin.ViewModels
{
    public class ArtifiOMSOrderViewModel : BaseViewModel
    {
        public OrderViewModel OrderViewModel { get; set; }
        public List<ArtifiShoppingCartItemModel> ArtifiShoppingCartItemListModel { get; set; }
        public List<ArtifiCartItemViewModel> ArtifiCartItemViewListModel { get; set; }

        public int WebsiteId { get; set; }
        public string WebApiClientKey { get; set; }
        public ArtifiOMSOrderViewModel()
        {
            ArtifiCartItemViewListModel = new List<ArtifiCartItemViewModel>();
            OrderViewModel = new OrderViewModel();
        }
    }
}