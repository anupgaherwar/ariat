﻿using Znode.Engine.Api.Models;

namespace Znode.Artifi.Api.Model
{
    public class ArtifiGroupCartItemViewModel : BaseModel
    {
        public string PublishProductId { get; set; }
        public string ProductName { get; set; }
        public string SKU { get; set; }
        public int PortalId { get; set; }
        public int CatalogId { get; set; }
        public int UserId { get; set; }
        public int LocaleId { get; set; }
        public string ProductType { get; set; }
        public string AddOnProductSKUs { get; set; }
        public string BundleProductSKUs { get; set; }
        public decimal Quantity { get; set; }
        public string GroupProductsQuantity { get; set; }
        public string GroupProductSKUs { get; set; }
        public int ShippingId { get; set; }
        public int OmsOrderId { get; set; }
        public string ConfigurableProductSKUs { get; set; }
        public string PersonalisedCodes { get; set; }
        public string PersonalisedValues { get; set; }
        public string GroupProductNames { get; set; }
        public string AutoAddonSKUs { get; set; }
    }
}


