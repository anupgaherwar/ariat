﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Admin.ViewModels;

namespace Znode.Engine.Admin.ViewModel
{
    public class ArtifiCartViewModel : BaseViewModel
    {
        public CartViewModel CartViewModel { get; set; }

        public List<ArtifiCartItemViewModel> ArtifiCartItemViewModel { get; set; }
    }
}