﻿
namespace Znode.Engine.Admin.ViewModels
{
    public class ArtifiCartItemViewModel : CartItemViewModel
    {
        public int OrderLineItemsDesignId { get; set; }

        public string AISavedDesigns { get; set; }
        public bool IsConfigurableProduct { get; set; }
        public bool IsGroupProduct { get; set; }
    }
}