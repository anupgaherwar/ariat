﻿namespace Znode.Engine.Admin.ViewModels
{
    public class ArtifiPortalViewModel : BaseViewModel
    {
        public StoreViewModel Portal { get; set; }
        public ArtifiPortalDetailViewModel PortalCustomDetail { get; set; }
    }
}