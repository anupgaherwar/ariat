﻿using System.Collections.Generic;
using Znode.Engine.Admin.Models;


namespace Znode.Engine.Admin.ViewModels
{
    public class ArtifiPortalDetailListViewModel : BaseViewModel
    {
        public ArtifiPortalDetailListViewModel()
        {
            CustomPortalDetailList = new List<ArtifiPortalDetailViewModel>();
        }

        public List<ArtifiPortalDetailViewModel> CustomPortalDetailList { get; set; }
        public GridModel GridModel { get; set; }
    }
}