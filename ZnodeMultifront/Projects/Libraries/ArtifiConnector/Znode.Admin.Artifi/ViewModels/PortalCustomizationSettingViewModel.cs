﻿using System.ComponentModel.DataAnnotations;
using Znode.Engine.Admin.ViewModels;
using Znode.Libraries.Resources;

namespace Znode.Engine.Admin.ViewModel
{
    public class PortalCustomizationSettingViewModel : BaseViewModel
    {
        public int AIPortalSettingId { get; set; }

        public int PortalId { get; set; }
        [Required(ErrorMessageResourceType = typeof(Artifi_Resource), ErrorMessageResourceName = ZnodeArtifi_Resource.ErrorWebApiClientKey)]
        public string WebApiClientKey { get; set; }
        [Required(ErrorMessageResourceType = typeof(Artifi_Resource), ErrorMessageResourceName = ZnodeArtifi_Resource.ErrorWebSiteCode)]
        [Range(1, 99999, ErrorMessageResourceType = typeof(Artifi_Resource), ErrorMessageResourceName = ZnodeArtifi_Resource.ErrorWebSiteCodeRange)]
        public int WebsiteCode { get; set; }

        [Required(ErrorMessageResourceType = typeof(Artifi_Resource), ErrorMessageResourceName = ZnodeArtifi_Resource.ErrorDomainName)]
        public string DomainName { get; set; }


        public string PortalName { get; set; }
        public int UserId { get; internal set; }
        public string Email { get; internal set; }
    }
}