﻿namespace ArtifiZnode.Engine.Admin.Helper
{
    public struct ArtifiZnodeAdminConstants
    {
        public const string ArtifiOrderModel = "ArtifiOrderModel_";
        public const string ShoppingCartDesignDetails = "ShoppingCartDesignDetails";
    }
}