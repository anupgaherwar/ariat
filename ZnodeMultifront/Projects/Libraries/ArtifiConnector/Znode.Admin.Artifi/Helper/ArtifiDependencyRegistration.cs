﻿using Autofac;
using Znode.Engine.Admin.Agents;
using Znode.Engine.Admin.Clients;
using Znode.Engine.Admin.Controllers;
using Znode.Libraries.Framework.Business;

namespace Znode.Engine.Admin.Helper
{
    public class ArtifiDependencyRegistration : IDependencyRegistration
    {
        /// <summary>
        /// Register the Dependency Injection types.
        /// </summary>
        /// <param name="builder">Autofac Container Builder</param>
        public virtual void Register(ContainerBuilder builder)
        {
            builder.RegisterType<ArtifiPortalAgent>().As<IArtifiPortalAgent>().InstancePerLifetimeScope();

            //Here override znode base code method by injecting dependancy metion as below.
            //"In CustomUserAgent.cs we have override 'LogOut()' of znode base code" .
            builder.RegisterType<ArtifiUserAgent>().As<IUserAgent>().InstancePerLifetimeScope();
            builder.RegisterType<ArtifiOrderAgent>().As<IArtifiOrderAgent>().InstancePerLifetimeScope();
            builder.RegisterType<ArtifiOrderClient>().As<IArtifiOrderClient>().InstancePerLifetimeScope();
            builder.RegisterType<ArtifiOrderAgent>().As<IOrderAgent>().InstancePerLifetimeScope();

            builder.RegisterType<ArtifiOrderController>().As<OrderController>().InstancePerDependency();
        }

        /// <summary>
        /// Order method represents Dependency Injection Registration Order.
        /// For znode base code Library the DI registration order set to 0.
        /// For custom code library the DI registration order should be incremental.
        /// </summary>
        public int Order
        {
            get { return 2; }
        }
    }
}