﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Net;
using System.Web.Mvc;
using Znode.Artifi.Api.Model;
using Znode.Artifi.Api.Model.Responses;
using Znode.Engine.Admin.Endpoints;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Admin.Clients
{
    public class ArtifiPortalClient : BaseClient, IArtifiPortalClient
    {
        //Get portal product customization setting.
        public virtual PortalCustomizationSettingModel GetPortalCustomizationSetting(int portalId, ExpandCollection expands)
        {
            string endpoint = ArtifiPortalEndpoint.GetPortalCustomizationSetting(portalId);
            endpoint += BuildEndpointQueryString(expands);

            ApiStatus status = new ApiStatus();

            PortalCustomizationResponse response = GetResourceFromEndpoint<PortalCustomizationResponse>(endpoint, status);
            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return response?.CustomizationSettingModel;
        }

        //create edit portal product customization setting.
        [HttpPut]
        public virtual PortalCustomizationSettingModel CreateEditCustomizationSetting(PortalCustomizationSettingModel model)
        {
            string endpoint = ArtifiPortalEndpoint.CreateEditCustomizationSetting();

            ApiStatus status = new ApiStatus();
            PortalCustomizationResponse response = PutResourceToEndpoint<PortalCustomizationResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return response?.CustomizationSettingModel;
        }
    }
}