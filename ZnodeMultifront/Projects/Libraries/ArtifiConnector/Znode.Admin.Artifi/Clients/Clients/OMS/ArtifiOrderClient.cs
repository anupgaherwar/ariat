﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Net;
using Znode.Artifi.Api.Model;
using Znode.Artifi.Api.Model.Responses;
using Znode.Engine.Admin.Endpoints;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Admin.Clients
{
    public class ArtifiOrderClient : BaseClient, IArtifiOrderClient
    {
        public PortalCustomizationSettingModel GetArtifiCredentials(int portalId)
        {
            string endpoint = ArtifiOrderEndpoint.GetPortalCustomizationArtifiSetting(portalId);
            endpoint += BuildEndpointQueryString(null);

            ApiStatus status = new ApiStatus();

            PortalCustomizationResponse response = GetResourceFromEndpoint<PortalCustomizationResponse>(endpoint, status);
            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return response?.CustomizationSettingModel;
        }

        public bool UpdateCartItemDesign(ArtifiShoppingCartItemModel artifiShoppingCartItemModel)
        {
            string endpoint = ArtifiOrderEndpoint.UpdateCartItemDesign();

            ApiStatus status = new ApiStatus();
            TrueFalseResponse response = PutResourceToEndpoint<TrueFalseResponse>(endpoint, JsonConvert.SerializeObject(artifiShoppingCartItemModel), status);
            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return response.IsSuccess;
        }
        public bool UpdateSavedCartItemDesign(ArtifiShoppingCartItemModel artifiShoppingCartItemModel)
        {
            string endpoint = ArtifiOrderEndpoint.UpdateSavedCartItemDesign();

            ApiStatus status = new ApiStatus();
            TrueFalseResponse response = PutResourceToEndpoint<TrueFalseResponse>(endpoint, JsonConvert.SerializeObject(artifiShoppingCartItemModel), status);
            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return response.IsSuccess;
        }
    }
}