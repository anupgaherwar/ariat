﻿using Znode.Artifi.Api.Model;
using Znode.Engine.Api.Client;

namespace Znode.Engine.Admin.Clients
{
    public interface IArtifiOrderClient : IBaseClient
    {
        /// <summary>
        /// Get artifi credential.
        /// </summary>
        /// <param name="portalId"></param>
        /// <returns></returns>
        PortalCustomizationSettingModel GetArtifiCredentials(int portalId);

        /// <summary>
        /// Update customised cart item design.
        /// </summary>
        /// <param name="artifiShoppingCartItemModel"></param>
        /// <returns></returns>
        bool UpdateCartItemDesign(ArtifiShoppingCartItemModel artifiShoppingCartItemModel);

        /// <summary>
        /// Update the save cart item.
        /// </summary>
        /// <param name="artifiShoppingCartItemModel"></param>
        /// <returns></returns>
        bool UpdateSavedCartItemDesign(ArtifiShoppingCartItemModel artifiShoppingCartItemModel);

    }
}
