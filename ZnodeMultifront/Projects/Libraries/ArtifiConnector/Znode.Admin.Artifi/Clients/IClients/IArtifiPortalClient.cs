﻿using Znode.Artifi.Api.Model;
using Znode.Engine.Api.Client.Expands;

namespace Znode.Engine.Admin.Clients
{
    public interface IArtifiPortalClient    {
        
        PortalCustomizationSettingModel GetPortalCustomizationSetting(int portalId, ExpandCollection expands);

        PortalCustomizationSettingModel CreateEditCustomizationSetting(PortalCustomizationSettingModel portalCustomizationSettingModel);
    }
}
