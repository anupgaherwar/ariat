﻿namespace Znode.Engine.Api.Cache
{
    public interface IArtifiPortalCache
    {
        /// <summary>
        /// Get Portal product customization setting data.
        /// </summary>
        /// <param name="portalId">portal id.</param>
        /// <param name="routeUri">URI to route.</param>
        /// <param name="routeTemplate">Template of route.</param>
        /// <returns>portal customization setting.</returns>
        string GetPortalCustomizationSetting(int portalId, string routeUri, string routeTemplate);

    }
}
