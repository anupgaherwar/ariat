﻿namespace Znode.Engine.Api.Cache
{
    interface IArtfiPriceCache
    {
        string GetProductPrice(int portalId, int userId, string sku, string clipArtSKU, string routeUri, string routeTemplate);
    }
}
