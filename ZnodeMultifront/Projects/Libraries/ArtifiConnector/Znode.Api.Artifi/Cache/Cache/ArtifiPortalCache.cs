﻿using Znode.Artifi.Api.Model;
using Znode.Artifi.Api.Model.Responses;
using Znode.Engine.Api.Service;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Api.Cache
{
    public class ArtifiPortalCache : BaseCache, IArtifiPortalCache
    {
        #region Private Variables
        private readonly IArtifiPortalService _portalService;
        #endregion

        #region Constructor
        public ArtifiPortalCache(IArtifiPortalService portalService)
        {
            _portalService = portalService;
        }
        #endregion

        #region Public Methods

        public virtual string GetPortalCustomizationSetting(int portalId, string routeUri, string routeTemplate)
        {
            //Get data from cache.
            string data = GetFromCache(routeUri);
            if (string.IsNullOrEmpty(data))
            {
                //get tag manager details by portal id.
                PortalCustomizationSettingModel model = _portalService.GetPortalCustomizationSetting(portalId, Expands);
                if (HelperUtility.IsNotNull(model))
                {
                    PortalCustomizationResponse response = new PortalCustomizationResponse { CustomizationSettingModel = model };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }

        #endregion
    }
}