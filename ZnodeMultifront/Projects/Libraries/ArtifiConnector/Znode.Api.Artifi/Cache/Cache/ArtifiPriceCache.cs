﻿using Znode.Artifi.Api.Model.Responses;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Service;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;

namespace Znode.Engine.Api.Cache.Cache
{
    public class ArtifiPriceCache : BaseCache, IArtfiPriceCache
    {
        #region Private Variables
        private readonly IArtifiPriceService _artifiPriceService;
        #endregion

        #region Constructor
        public ArtifiPriceCache(IArtifiPriceService artifiPriceService)
        {
            _artifiPriceService = artifiPriceService;
        }
        #endregion

        #region Public Method
        public string GetProductPrice(int portalId, int userId, string sku, string clipArtSKU, string routeUri, string routeTemplate)
        {
            string data = GetFromCache(routeUri);
            if (string.IsNullOrEmpty(data))
            {
                //Get data from service
                PriceSKUModel productPrice = _artifiPriceService.GetProductPrice(portalId, userId, sku, clipArtSKU);
                if (IsNotNull(productPrice))
                {
                    ArtifiPriceResponse response = new ArtifiPriceResponse { ProductPrice = productPrice };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        } 
        #endregion
    }
}