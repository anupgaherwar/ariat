﻿using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using Znode.Api.Custom.Helper;
using Znode.Artifi.Api.Model;
using Znode.Custom.Data;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.Services;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Fulfillment;
using Znode.Libraries.ECommerce.ShoppingCart;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;
namespace Znode.Engine.Api.Service
{
    public class ArtifiOrderService : OrderService, IArtifiOrderService
    {
        #region Private Variable
        private readonly IZnodeRepository<ZnodeOmsPersonalizeCartItem> _omsPersonlizedCartItemRepository;
        private readonly IZnodeRepository<ZnodeOmsPersonalizeItem> _omsPersonlizedItemRepository;
        private readonly IZnodeRepository<ZnodeOmsOrderDetail> _orderDetailRepository;
        private readonly IZnodeRepository<ZnodeOmsSavedCartLineItem> _omsSavedCartItemRepository;
        private readonly IZnodeRepository<ZnodeOmsOrderLineItem> _omsOrderLineItemRepository;
        private readonly IZnodeRepository<ZnodeOmsOrderShipment> _orderShipmentRepository;
        private readonly IZnodeRepository<ZnodePimDownloadableProduct> _pimDownloadableProduct;
        private readonly IZnodeRepository<ZnodeOmsOrder> _omsOrderRepository;
        private readonly string _orderNumberFileName = HttpContext.Current.Server.MapPath(WebConfigurationManager.AppSettings["OrderNumberFilePath"]);
        #endregion

        #region Constructor
        public ArtifiOrderService()
        {
            _omsPersonlizedCartItemRepository = new ZnodeRepository<ZnodeOmsPersonalizeCartItem>();
            _omsPersonlizedItemRepository = new ZnodeRepository<ZnodeOmsPersonalizeItem>();
            _orderDetailRepository = new ZnodeRepository<ZnodeOmsOrderDetail>();
            _omsSavedCartItemRepository = new ZnodeRepository<ZnodeOmsSavedCartLineItem>();
            _omsOrderLineItemRepository = new ZnodeRepository<ZnodeOmsOrderLineItem>();
            _orderShipmentRepository = new ZnodeRepository<ZnodeOmsOrderShipment>();
            _pimDownloadableProduct = new ZnodeRepository<ZnodePimDownloadableProduct>();
            _omsOrderRepository = new ZnodeRepository<ZnodeOmsOrder>();
        }
        #endregion

        #region Public Methods
        public bool UpdateCartItemDesign(ArtifiShoppingCartItemModel artifiShoppingCartItemModel)
        {
            if (IsNull(artifiShoppingCartItemModel))
                throw new ZnodeException(ErrorCodes.InvalidData, "ArtifiShoppingCartItem Model can not be Nulls");

            if (artifiShoppingCartItemModel.OrderLineItemsDesignId < 1)
                throw new ZnodeException(ErrorCodes.IdLessThanOne, "Line Item Id less than 1");

            ZnodeOmsPersonalizeItem omsPersonalizedLineItemDesign = _omsPersonlizedItemRepository.Table.FirstOrDefault(x => x.PersonalizeValue == artifiShoppingCartItemModel.OrderLineItemsDesignId.ToString());
            if (IsNotNull(omsPersonalizedLineItemDesign))
            {
                ZnodeOmsPersonalizeItem omsPersonalizedLineItem = _omsPersonlizedItemRepository.Table.FirstOrDefault(x => x.OmsOrderLineItemsId == omsPersonalizedLineItemDesign.OmsOrderLineItemsId && x.PersonalizeValue != artifiShoppingCartItemModel.OrderLineItemsDesignId.ToString());
                if (IsNotNull(omsPersonalizedLineItemDesign))
                {
                    omsPersonalizedLineItem.PersonalizeValue = artifiShoppingCartItemModel.AIImagePath;
                    _omsPersonlizedItemRepository.Update(omsPersonalizedLineItem);

                    if (artifiShoppingCartItemModel.Quantity > 0)
                        return UpdateSavedCartItemQuantity(artifiShoppingCartItemModel, omsPersonalizedLineItem.OmsOrderLineItemsId, true);
                    else
                        return true;
                }
            }
            return false;
        }


        public bool UpdateSavedCartItemDesign(ArtifiShoppingCartItemModel artifiShoppingCartItemModel)
        {
            if (IsNull(artifiShoppingCartItemModel))
                throw new ZnodeException(ErrorCodes.InvalidData, "ArtifiShoppingCartItem Model can not be Nulls");

            if (artifiShoppingCartItemModel.OrderLineItemsDesignId < 1)
                throw new ZnodeException(ErrorCodes.IdLessThanOne, "Line Item Id less than 1");

            ZnodeOmsPersonalizeCartItem omsPersonalizedCartLineItemDesign = _omsPersonlizedCartItemRepository.Table.FirstOrDefault(x => x.PersonalizeValue == artifiShoppingCartItemModel.OrderLineItemsDesignId.ToString());
            if (IsNotNull(omsPersonalizedCartLineItemDesign))
            {
                List<ZnodeOmsPersonalizeCartItem> omsPersonalizedCartLineItem = _omsPersonlizedCartItemRepository.Table.Where(x => x.OmsSavedCartLineItemId == omsPersonalizedCartLineItemDesign.OmsSavedCartLineItemId && x.PersonalizeValue != artifiShoppingCartItemModel.OrderLineItemsDesignId.ToString()).ToList();
                if (IsNotNull(omsPersonalizedCartLineItem))
                {
                    ZnodeOmsPersonalizeCartItem artifiImage = omsPersonalizedCartLineItem.FirstOrDefault(x => x.PersonalizeCode == "ArtifiImagePath");
                    artifiImage.PersonalizeValue = artifiShoppingCartItemModel.AIImagePath;
                    _omsPersonlizedCartItemRepository.Update(artifiImage);
                    if (artifiShoppingCartItemModel.Quantity > 0)
                        return UpdateSavedCartItemQuantity(artifiShoppingCartItemModel, omsPersonalizedCartLineItemDesign.OmsSavedCartLineItemId.GetValueOrDefault());
                    else
                        return true;
                }
            }
            return false;
        }

        public DataTable ConvertCustomizationDataToDataTable(ArtifiShoppingCartItemModel artifiShoppingCartItemModel)
        {
            DataTable table = new DataTable("OmsCartKeyValue");
            table.Columns.Add("Key", typeof(string));
            table.Columns.Add("Value", typeof(string));
            table.Columns.Add("IsAddon", typeof(bool));
            table.Columns.Add("ProductName", typeof(string));
            table.Columns.Add("Description", typeof(string));

            table.Rows.Add("ArtifiDesignId", artifiShoppingCartItemModel.OrderLineItemsDesignId, false, "", "");
            table.Rows.Add("ArtifiImagePath", artifiShoppingCartItemModel.AIImagePath, false, "", "");
            foreach (var item in artifiShoppingCartItemModel.Customization)
            {
                if (item.Key != "Logo" && item.Key != "TextLines" && item.Key != "LargeLogo")
                {
                    table.Rows.Add(item.Key, item.Value, false, "", "");
                }
                else if (item.Key == "Logo" || item.Key == "TextLines" || item.Key == "LargeLogo")
                {
                    string customizedQuantity = artifiShoppingCartItemModel.Customization[item.Key];
                    if (!string.IsNullOrEmpty(customizedQuantity))
                    {
                        decimal? quantity = artifiShoppingCartItemModel.Quantity * Convert.ToDecimal(customizedQuantity);
                        customizedQuantity = (quantity.ToString());
                    }
                    table.Rows.Add(item.Key, customizedQuantity, true, "", "");
                }
            }

            return table;
        }

        public int GetUserIdByOrderId(int omsOrderId)
        {
            if (omsOrderId < 1)
                throw new ZnodeException(ErrorCodes.IdLessThanOne, "omsOrderId should be greater than 1");

            FilterCollection filters = new FilterCollection();

            filters.Add(new FilterTuple(Services.Constants.FilterKeys.OmsOrderId, FilterOperators.Equals, omsOrderId.ToString()));
            filters.Add(new FilterTuple(Services.Constants.FilterKeys.IsActive, FilterOperators.Equals, ZnodeConstant.TrueValue));

            return (_orderDetailRepository.GetEntity(DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filters.ToFilterDataCollection())?.WhereClause)?.UserId).GetValueOrDefault();
        }


        //to send the adyen credit card info in email template:start
        public override string GetOrderReceipt(ZnodeOrderFulfillment order, IZnodeCheckout checkout, string feedbackUrl, int localeId, bool isUpdate, out bool isEnableBcc, int accountId = 0)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            foreach (OrderLineItemModel item in order.OrderLineItems)
            {
                if (item.PersonaliseValueList != null)
                    item.PersonaliseValueList.Remove("AllocatedLineItems");

                if (item.PersonaliseValuesDetail != null)
                    item.PersonaliseValuesDetail.RemoveAll(pv => pv.PersonalizeCode == "AllocatedLineItems");
            }

            AriatOrderReceipt receipt = (AriatOrderReceipt)GetOrderReceiptInstance(order, checkout.ShoppingCart);


            //Method to get Email Template.
            EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode(ZnodeConstant.OrderReceipt, (order.PortalId > 0) ? order.PortalId : PortalId, localeId);
            if (HelperUtility.IsNotNull(emailTemplateMapperModel))
            {
                string receiptContent = ShowOrderAdditionalDetails(emailTemplateMapperModel.Descriptions, order.Custom1);
                isEnableBcc = emailTemplateMapperModel.IsEnableBcc;
                ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
                return EmailTemplateHelper.ReplaceTemplateTokens(receipt.GetOrderReceiptHtml(receiptContent));
            }
            isEnableBcc = false;

            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return string.Empty;
        }
        //to generate order receipt
        public override string GetOrderReceipt(ZnodeOrderFulfillment order, IZnodeCheckout checkout, string feedbackUrl, int localeId, bool isUpdate = false, string emailTemplate = "OrderReceipt")
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            foreach (OrderLineItemModel item in order.OrderLineItems)
            {
                if (item.PersonaliseValueList != null)
                    item.PersonaliseValueList.Remove("AllocatedLineItems");

                if (item.PersonaliseValuesDetail != null)
                    item.PersonaliseValuesDetail.RemoveAll(pv => pv.PersonalizeCode == "AllocatedLineItems");
            }

            AriatOrderReceipt receipt = (AriatOrderReceipt)GetOrderReceiptInstance(order, checkout.ShoppingCart);

            //Method to get Email Template.
            EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode(ZnodeConstant.OrderReceipt, (order.PortalId > 0) ? order.PortalId : PortalId, localeId);
            if (HelperUtility.IsNotNull(emailTemplateMapperModel))
            {
                string receiptContent = ShowOrderAdditionalDetails(emailTemplateMapperModel.Descriptions);
                return EmailTemplateHelper.ReplaceTemplateTokens(receipt.GetOrderReceiptHtml(receiptContent));
            }
            return string.Empty;
        }

        //Generate sequential order number.
        public override string GenerateOrderNumber(SubmitOrderModel submitOrderModel, ParameterModel portalId = null)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);

            try
            {
                string OrderNumber = (new ArtifiOrderService()).GetLastOrderNumberFromTextFile();
                return OrderNumber;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage($"Error GenerateOrderNumber : {ex}", ZnodeLogging.Components.OMS.ToString());
                return base.GenerateOrderNumber(submitOrderModel, portalId);
            }

        }
        public string GetLastOrderNumberFromTextFile()
        {
            string OrderNumber = string.Empty;
            if (File.Exists(_orderNumberFileName))
            {
                // Open the text file using a stream reader.
                using (StreamReader streamReader = new StreamReader(_orderNumberFileName))
                {
                    OrderNumber = streamReader.ReadToEnd();
                    streamReader.Close();
                    long nextOrderNumber = (Convert.ToInt64(OrderNumber) + 1);
                    return WriteOrderNumberInTextFile(String.Format("{0:0000000000}", nextOrderNumber));
                }
            }
            else
            {
                File.Create(_orderNumberFileName);
                OrderNumber = "0014009000";
                return WriteOrderNumberInTextFile((Convert.ToInt64(OrderNumber) + 1).ToString());
            }
        }
        public string WriteOrderNumberInTextFile(string lastWriteData)
        {
            using (StreamWriter streamWriter = new StreamWriter(_orderNumberFileName))
            {
                streamWriter.Write(lastWriteData);
                streamWriter.Close();
                return lastWriteData;
            }
        }

        public override OrderModel GetOrderById(int orderId, FilterCollection filters, NameValueCollection expands)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            OrderModel orderModel = GetOrderByOrderDetails(orderId, string.Empty, filters, expands);

            if (orderModel == null)
                return null;

            if (IsNotNull(orderModel?.OrderLineItems) && orderModel.OrderLineItems.Count > 0)
                orderModel.OrderLineItems = orderHelper.FormalizeOrderLineItems(orderModel);
            IAccountQuoteService _accountQuoteService = GetService<IAccountQuoteService>();
            orderModel.QuoteApproverComments = _accountQuoteService.GetApproverComments(orderModel.OmsQuoteId);
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return orderModel;
        }

        public override OrderModel GetOrderDetails(ZnodeOmsOrder order, ZnodeOmsOrderDetail orderDetail, bool isFromOrderReceipt, bool isOrderHistory, bool isFromReorder, NameValueCollection expands = null, bool isFromReturnLineItem = false)
       {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            OrderModel orderModel = new OrderModel();
            List<ZnodeOmsOrderLineItem> orderLineItemsAddon = new List<ZnodeOmsOrderLineItem>();
            //null check for order detail object.
            if (IsNotNull(orderDetail))
            {
                ZnodeLogging.LogMessage("OmsOrderDetailsId: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { OmsOrderDetailsId = orderDetail?.OmsOrderDetailsId });
                //Map order detail object to OrderMOdel object.
                orderModel = orderDetail.ToModel<OrderModel>();

                //If expand key contains PortalTrackingPixel key then get portal tracking pixel details.
                if (!string.IsNullOrEmpty(expands?[ExpandKeys.PortalTrackingPixel]))
                    GetPortalPixelTracking(orderModel);

                if (!isFromReturnLineItem)
                {
                    List<ZnodeOmsOrderLineItem> orderLineItems = orderHelper.GetOrderLineItemByOrderId(orderDetail.OmsOrderDetailsId).ToList();
                    if (isFromReorder)
                        orderModel.OrderLineItems = orderLineItems.Where(m => m.OrderLineItemRelationshipTypeId != Convert.ToInt32(ZnodeCartItemRelationshipTypeEnum.AddOns) && m.OrderLineItemRelationshipTypeId != Convert.ToInt32(ZnodeCartItemRelationshipTypeEnum.Bundles))?
                                                        .ToModel<OrderLineItemModel>()?.ToList();
                    else
                        orderModel.OrderLineItems = orderLineItems.Where(m => m.OrderLineItemRelationshipTypeId != Convert.ToInt32(ZnodeCartItemRelationshipTypeEnum.Bundles))?
                                                        .ToModel<OrderLineItemModel>()?.ToList();
                    if (orderModel.OrderLineItems?.Count > 0)
                    {
                        foreach (OrderLineItemModel item in orderModel.OrderLineItems)
                        {
                            item.Description = orderLineItems.FirstOrDefault(m => m.OrderLineItemRelationshipTypeId == Convert.ToInt16(ZnodeCartItemRelationshipTypeEnum.AddOns)
                                                           && m.ParentOmsOrderLineItemsId == item.OmsOrderLineItemsId)?.Description ?? item.Description;
                            item.Quantity = Convert.ToDecimal(ServiceHelper.ToInventoryRoundOff(item.Quantity));
                            ZnodeOmsOrderLineItem lineItem = orderLineItems.FirstOrDefault(y => y.OmsOrderLineItemsId == item.OmsOrderLineItemsId);

                            orderLineItemsAddon = orderLineItems
                                    .Where(z => z.OrderLineItemRelationshipTypeId == (int)ZnodeCartItemRelationshipTypeEnum.AddOns
                                    && z.ParentOmsOrderLineItemsId == item.OmsOrderLineItemsId).ToList();
                            item.Custom2 = Convert.ToString(item.Price);
                            if (orderLineItemsAddon.Count > 0)
                            {
                                foreach (ZnodeOmsOrderLineItem lineItemaddon in orderLineItemsAddon)
                                {
                                    if (lineItemaddon.Quantity != item.Quantity && lineItemaddon.Quantity > item.Quantity)
                                    {
                                        decimal? noOfTextOrLogo = lineItemaddon.Quantity / item.Quantity;
                                        if (IsNotNull(noOfTextOrLogo) && noOfTextOrLogo > 0)
                                        {
                                            decimal price = Convert.ToDecimal(noOfTextOrLogo) * lineItemaddon.Price;
                                            item.Price += price;
                                        }

                                    }
                                    else if (lineItemaddon.Quantity == item.Quantity)
                                    {
                                        item.Price += lineItemaddon.Price;
                                    }
                                }
                            }
                            else
                            {
                                item.Price += orderLineItems
                                                                 .Where(z => z.OrderLineItemRelationshipTypeId == (int)ZnodeCartItemRelationshipTypeEnum.AddOns
                                                                 && z.ParentOmsOrderLineItemsId == item.OmsOrderLineItemsId
                                                                 ).Sum(z => z.Price);
                            }

                            item.OrderWarehouse = orderLineItems.FirstOrDefault(y => y.OmsOrderLineItemsId == item.OmsOrderLineItemsId)?.ZnodeOmsOrderWarehouses.ToModel<OrderWarehouseModel>()?.ToList();
                            if (lineItem.ZnodeOmsOrderAttributes?.Count > 0)
                            {
                                item.Attributes = new List<OrderAttributeModel>();
                                foreach (ZnodeOmsOrderAttribute items in lineItem.ZnodeOmsOrderAttributes)
                                    item.Attributes.Add(new OrderAttributeModel { AttributeCode = items.AttributeCode, AttributeValue = items.AttributeValue, AttributeValueCode = items.AttributeValueCode });
                            }
                            ZnodeOmsOrderLineItem parentLineItem = orderLineItems.FirstOrDefault(y => y.OmsOrderLineItemsId == item.ParentOmsOrderLineItemsId);
                            if (IsNotNull(parentLineItem))
                                item.GroupId = parentLineItem.GroupId;
                        }
                    }
                }
                else
                {
                    orderModel.OrderLineItems = orderDetail.ZnodeOmsOrderLineItems.ToModel<OrderLineItemModel>().ToList() ?? new List<OrderLineItemModel>();
                    orderModel.ReturnedOrderLineItems = IsNotNull(orderModel.ReturnedOrderLineItems) ? orderModel.ReturnedOrderLineItems : new List<OrderLineItemModel>();
                }

                //Map order data from ZnodeOmsOrder.
                orderModel.IsQuoteOrder = order.IsQuoteOrder;
                orderModel.OrderNumber = order.OrderNumber;
                orderModel.OmsQuoteId = order.OMSQuoteId.GetValueOrDefault();
                ZnodeLogging.LogMessage("OrderNumber:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { OrderNumber = orderModel.OrderNumber });

                //check for omsOrderDetailsId greater than 0.
                if (orderModel?.OmsOrderDetailsId > 0)
                {
                    //Map order data from ZnodeOmsOrder.
                    orderModel.IsQuoteOrder = order.IsQuoteOrder;
                    orderModel.OrderNumber = order.OrderNumber;

                    //Map Order details data to OrderModel.
                    MapOrderData(orderModel, orderDetail, isOrderHistory);

                    //Get Ordered billing address.
                    orderModel.BillingAddressHtml = GetOrderBillingAddress(orderModel);

                    //set the amount of disserent discount applied during order creation.
                    SetOrderDiscount(orderModel);

                    //Set Rma details for RMA validation checks
                    GetAndMapRmaDetails(orderModel);

                    // Map Customer Shipping
                    CustomerShipping(orderModel);

                    if (IsNotNull(orderModel?.OrderLineItems))
                    {
                        List<ZnodeOmsOrderStateShowToCustomer> orderStatusList = GetOrderStatusForCustomerList(orderModel.OrderLineItems);

                        List<ZnodeOmsOrderLineItemsAdditionalCost> additionalCostList = GetAdditionalCostList(orderModel.OrderLineItems);

                        List<ZnodeOmsOrderShipment> orderShipmentList = GetOrderShipmentList(orderModel.OrderLineItems);

                        List<ZnodeAddress> addressList = GetOrderShipmentAddressList(orderModel.OrderLineItems.Select(x => x.ZnodeOmsOrderShipment)?.ToList());

                        List<ZnodeOmsPersonalizeItem> personalizeList = GetPersonalisedValueOrderLineItemList(orderModel.OrderLineItems);

                        List<string> downloadableProductkeys = GetDownloadableProductKeyList(orderModel.OrderLineItems.Select(x => x.Sku)?.Distinct()?.ToList());

                        foreach (OrderLineItemModel lineItem in orderModel.OrderLineItems)
                        {
                            //If expands constains IsWebStoreOrderReciept key and line item contains IsShowToCustomer false, get order status that will be shown to customer.
                            if (!string.IsNullOrEmpty(expands?[ExpandKeys.IsWebStoreOrderReciept]) && !lineItem.IsShowToCustomer)
                                lineItem.OrderLineItemState = GetOrderStatusForCustomer(lineItem.OrderLineItemStateId, orderStatusList);

                            lineItem.ZnodeOmsOrderShipment = orderShipmentList?.FirstOrDefault(x => x.OmsOrderShipmentId == lineItem.OmsOrderShipmentId)?.ToModel<OrderShipmentModel>();

                            lineItem.ShippingAddressHtml = GetOrderShipmentAddress(lineItem.ZnodeOmsOrderShipment, addressList);
                            //get personalise attributes by omsorderlineitemid
                            lineItem.PersonaliseValueList = GetPersonalisedValueOrderLineItem(Convert.ToInt32(lineItem.ParentOmsOrderLineItemsId) > 0 ? Convert.ToInt32(lineItem.ParentOmsOrderLineItemsId) : lineItem.OmsOrderLineItemsId, personalizeList);

                            lineItem.PersonaliseValuesDetail = orderHelper.GetPersonalisedAttributeLineItemDetails(lineItem.PersonaliseValueList, string.Empty);

                            lineItem.DownloadableProductKey = GetProductKey(lineItem.Sku, lineItem.OmsOrderLineItemsId, downloadableProductkeys);

                            lineItem.AdditionalCost = additionalCostList?.Where(x => x.OmsOrderLineItemsId == lineItem.OmsOrderLineItemsId)?.ToDictionary(x => x.KeyName, y => y.KeyValue);
                            lineItem.Description = lineItem.Description;
                        }
                    }
                }
            }       
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return orderModel;
        }

        public override OrderModel GetOrderReceiptByOrderId(int orderId)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            ZnodeOmsOrder order = null;

            if (orderId <= 0)

                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ErrorPlaceOrder);
            //Get active order by order id.
            order = _omsOrderRepository.Table.FirstOrDefault(x => x.OmsOrderId == orderId);

            OrderModel orderModel = new OrderModel();

            if (IsNotNull(order))
            {
                ZnodeOmsOrderDetail orderDetail = null;
                orderId = order.OmsOrderId;

                FilterCollection filters = new FilterCollection();
                filters.Add(new FilterTuple(Engine.Services.Constants.FilterKeys.OmsOrderId, FilterOperators.Equals, orderId.ToString()));
                orderDetail = _orderDetailsRepository.GetEntity(DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filters.ToFilterDataCollection())?.WhereClause, new List<string> { ZnodeOmsOrderDetailEnum.ZnodeShipping.ToString() });

                //null check for order detail object.
                if (IsNotNull(orderDetail))
                {
                    orderModel = orderDetail.ToModel<OrderModel>();
                    //Map order detail object to OrderMOdel object.
                    filters.Clear();
                    filters.Add(new FilterTuple(ZnodeOmsOrderDetailEnum.OmsOrderDetailsId.ToString(), FilterOperators.Equals, orderDetail.OmsOrderDetailsId.ToString()));
                    filters.Add(new FilterTuple(ZnodeOmsOrderDetailEnum.IsActive.ToString(), FilterOperators.Equals, Convert.ToString(true)));
                    List<ZnodeOmsOrderLineItem> orderLineItems = _omsOrderLineItemRepository.GetEntityList(DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filters.ToFilterDataCollection())?.WhereClause, new List<string> { ZnodeOmsOrderLineItemEnum.ZnodeOmsOrderState.ToString(), ZnodeOmsOrderLineItemEnum.ZnodeOmsOrderShipment.ToString() })?.ToList();

                    orderModel.OrderLineItems = orderLineItems?.Where(m => m.OrderLineItemRelationshipTypeId != Convert.ToInt32(ZnodeCartItemRelationshipTypeEnum.AddOns) && m.OrderLineItemRelationshipTypeId != Convert.ToInt32(ZnodeCartItemRelationshipTypeEnum.Bundles))?
                                                        .ToModel<OrderLineItemModel>()?.ToList();
                    if (orderModel.OrderLineItems?.Count > 0)
                    {
                        orderModel.OrderLineItems.ForEach(x =>
                        {
                            x.Description = orderLineItems.FirstOrDefault(m => m.OrderLineItemRelationshipTypeId == Convert.ToInt16(ZnodeCartItemRelationshipTypeEnum.AddOns)
                                                           && m.ParentOmsOrderLineItemsId == x.OmsOrderLineItemsId)?.Description ?? x.Description;
                            x.Quantity = Convert.ToDecimal(ServiceHelper.ToInventoryRoundOff(x.Quantity));
                            ZnodeOmsOrderLineItem lineItem = orderLineItems.FirstOrDefault(y => y.OmsOrderLineItemsId == x.OmsOrderLineItemsId);
                            x.Price += orderLineItems
                                    .Where(z => z.OrderLineItemRelationshipTypeId == (int)ZnodeCartItemRelationshipTypeEnum.AddOns
                                    && z.ParentOmsOrderLineItemsId == x.OmsOrderLineItemsId
                                    ).Sum(z => z.Price);
                            x.OrderWarehouse = orderLineItems.FirstOrDefault(y => y.OmsOrderLineItemsId == x.OmsOrderLineItemsId)?.ZnodeOmsOrderWarehouses.ToModel<OrderWarehouseModel>()?.ToList();
                            if (lineItem.ZnodeOmsOrderAttributes?.Count > 0)
                            {
                                x.Attributes = new List<OrderAttributeModel>();
                                foreach (ZnodeOmsOrderAttribute item in lineItem.ZnodeOmsOrderAttributes)
                                    x.Attributes.Add(new OrderAttributeModel { AttributeCode = item.AttributeCode, AttributeValue = item.AttributeValue, AttributeValueCode = item.AttributeValueCode });
                            }
                            ZnodeOmsOrderLineItem parentLineItem = orderLineItems.FirstOrDefault(y => y.OmsOrderLineItemsId == x.ParentOmsOrderLineItemsId);
                            if (IsNotNull(parentLineItem))
                                x.GroupId = parentLineItem.GroupId;
                            if (x.ParentOmsOrderLineItemsId == null)
                                x.ProductType = GetLineItemProductType(x, orderLineItems);
                        });
                    }
                    //Map order data from ZnodeOmsOrder.
                    orderModel.IsQuoteOrder = order.IsQuoteOrder;
                    orderModel.OrderNumber = order.OrderNumber;
                    orderModel.OmsQuoteId = order.OMSQuoteId.GetValueOrDefault();

                    //check for omsOrderDetailsId greater than 0.
                    if (orderModel.OmsOrderDetailsId > 0)
                    {
                        orderModel.CustomerPaymentGUID = orderDetail.ZnodeUser?.CustomerPaymentGUID;
                        orderModel.ShippingId = (orderDetail.ZnodeShipping?.ShippingId).GetValueOrDefault();
                        orderModel.TrackingUrl = orderDetail.ZnodeShipping?.TrackingUrl;
                        int orderShipmentId = (orderModel.OrderLineItems.FirstOrDefault()?.OmsOrderShipmentId).GetValueOrDefault();

                        orderModel.BillingAddress = orderDetail.ToModel<AddressModel>();

                        SetOrderBillingAddress(orderModel);
                        //Get Ordered billing address.
                        orderModel.BillingAddressHtml = GetOrderBillingAddress(orderModel);

                        //set the amount of disserent discount applied during order creation.
                        SetOrderDiscount(orderModel);

                        //Set Rma details for RMA validation checks
                        GetAndMapRmaDetails(orderModel);

                        // Map Customer Shipping
                        CustomerShipping(orderModel);

                        if (IsNotNull(orderModel.OrderLineItems) && orderModel.OrderLineItems.Count() > 0)
                        {
                            List<ZnodeAddress> addressList = GetOrderShipmentAddressList(orderModel.OrderLineItems.Select(x => x.ZnodeOmsOrderShipment).ToList());
                            List<ZnodeOmsOrderStateShowToCustomer> orderStatusList = GetOrderStatusForCustomerList(orderModel.OrderLineItems);
                            List<ZnodeOmsOrderShipment> orderShipmentList = GetOrderShipmentList(orderModel.OrderLineItems);
                            List<ZnodeOmsPersonalizeItem> personalizeList = GetPersonalisedValueOrderLineItemList(orderModel.OrderLineItems);
                            List<string> downloadableProductkeys = GetDownloadableProductKeyList(orderModel.OrderLineItems?.Select(x => x.Sku)?.Distinct()?.ToList());
                            List<ZnodeOmsOrderLineItemsAdditionalCost> additionalCostList = GetAdditionalCostList(orderModel.OrderLineItems);

                            foreach (OrderLineItemModel lineItem in orderModel.OrderLineItems)
                            {
                                if (!lineItem.IsShowToCustomer)
                                    lineItem.OrderLineItemState = GetOrderStatusForCustomer(lineItem.OrderLineItemStateId, orderStatusList);

                                lineItem.ZnodeOmsOrderShipment = orderShipmentList?.FirstOrDefault(x => x.OmsOrderShipmentId == lineItem.OmsOrderShipmentId)?.ToModel<OrderShipmentModel>();

                                lineItem.ShippingAddressHtml = GetOrderShipmentAddress(lineItem.ZnodeOmsOrderShipment, addressList);
                                //get personalise attributes by omsorderlineitemid
                                lineItem.PersonaliseValueList = GetPersonalisedValueOrderLineItem(Convert.ToInt32(lineItem.ParentOmsOrderLineItemsId) > 0 ? Convert.ToInt32(lineItem.ParentOmsOrderLineItemsId) : lineItem.OmsOrderLineItemsId, personalizeList);
                                lineItem.PersonaliseValuesDetail = orderHelper.GetPersonalisedAttributeLineItemDetails(lineItem.PersonaliseValueList, string.Empty);

                                lineItem.DownloadableProductKey = GetProductKey(lineItem.Sku, lineItem.OmsOrderLineItemsId, downloadableProductkeys);

                                lineItem.AdditionalCost = additionalCostList?.Where(x => x.OmsOrderLineItemsId == lineItem.OmsOrderLineItemsId)?.ToDictionary(x => x.KeyName, y => y.KeyValue);
                                lineItem.Description = lineItem.Description;
                            }
                        }
                    }
                }
            }
            if (orderModel?.OrderLineItems?.Count > 0)
                orderModel.OrderLineItems = orderHelper.FormalizeOrderLineItems(orderModel);
           
            if (orderModel.OmsOrderId > 0)
            {
                string ids = string.Empty;
                decimal amount = 0;
                string culterCode = string.Empty;
                IZnodeRepository<ZnodeGiftCard> _giftCardRepository = new ZnodeRepository<ZnodeGiftCard>();
                List<GiftCardModel> giftCardRestrictVocher = _giftCardRepository.Table.Where(x => x.RestrictToCustomerAccount == false && x.IsActive == true)?.ToModel<GiftCardModel>().ToList();
                List<GiftCardModel> giftCardModels = _giftCardRepository.Table.Where(x => x.UserId == orderModel.UserId && x.ExpirationDate >= DateTime.Today && x.RemainingAmount > 0)?.ToModel<GiftCardModel>().ToList();
                if (giftCardModels.Count > 0)
                {


                    foreach (GiftCardModel item in giftCardModels)
                    {

                        if (HelperUtility.IsNotNull(item.RemainingAmount) && item.RemainingAmount > 0 && item.IsActive == true)
                        {
                            amount += item.RemainingAmount.HasValue ? item.RemainingAmount.Value : 0;
                        }
                    }
                    ids = string.Join(",", giftCardModels.Select(x => x.GiftCardId));
                }
                if (giftCardRestrictVocher.Count > 0)
                {
                    foreach (GiftCardModel item in giftCardRestrictVocher)
                    {
                        if (!ids.Contains(item.GiftCardId.ToString()) && HelperUtility.IsNotNull(item.RemainingAmount) && item.RemainingAmount > 0 && item.IsActive == true)
                        {
                            amount += item.RemainingAmount.HasValue ? item.RemainingAmount.Value : 0;
                        }
                    }

                }
                if (HelperUtility.IsNotNull(amount) && amount > 0)
                {
                    List<DefaultGlobalConfigModel> defaultGlobalSettingData = GetDefaultGlobalSettingData();
                    orderModel.Custom2 = Convert.ToString(amount);
                    orderModel.Custom2 = FormatPriceWithCurrency(amount, string.IsNullOrEmpty(culterCode) ? GetDefaultCulture(defaultGlobalSettingData) : culterCode, GetDefaultPriceRoundOff(defaultGlobalSettingData));
                }
            }

            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return orderModel;
        }
     
        public override void SetOrderDiscount(OrderModel model)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);

            List<OrderDiscountModel> discountList = orderHelper.GetOrderDiscountAmount(model.OmsOrderDetailsId);
            if (discountList?.Count > 0)
            {
                decimal csrDiscount = (discountList.FirstOrDefault(x => x.DiscountType == OrderDiscountTypeEnum.CSRDISCOUNT.ToString())?.DiscountAmount).GetValueOrDefault();
                if (csrDiscount > 0)
                {
                    model.CSRDiscountAmount = csrDiscount;
                    model.DiscountAmount = (model.DiscountAmount - csrDiscount);
                }

                decimal giftCardDiscount = (discountList.FirstOrDefault(x => x.DiscountType == OrderDiscountTypeEnum.GIFTCARD.ToString())?.DiscountAmount).GetValueOrDefault();
                if (giftCardDiscount > 0)
                {
                    IZnodeRepository<ZnodeGiftCard> _giftCardRepository = new ZnodeRepository<ZnodeGiftCard>();
                    model.GiftCardAmount = giftCardDiscount;
                    model.GiftCardNumber = (discountList.FirstOrDefault(x => x.DiscountType == OrderDiscountTypeEnum.GIFTCARD.ToString())?.DiscountCode);
                    if (model.OmsOrderId > 0 && !string.IsNullOrEmpty(model.GiftCardNumber))
                    {
                        string voucherName = _giftCardRepository.Table.FirstOrDefault(x => x.CardNumber == model.GiftCardNumber)?.Name;
                        if (!string.IsNullOrEmpty(voucherName))
                            model.GiftCardNumber = voucherName;
                    }
                }

                if (model.DiscountAmount == 0 && model.Custom4 == null)
                {
                    decimal couponDiscount = (discountList.FirstOrDefault(x => x.DiscountType == OrderDiscountTypeEnum.COUPONCODE.ToString())?.DiscountAmount).GetValueOrDefault();
                    if (couponDiscount > 0)
                        model.DiscountAmount = couponDiscount;
                }
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
        }

        //Send Shipping Notification to customer.
        public void SendShippingNotification(int omsOrderId = 0, IEnumerable<int> lineitemIds = null)
        {
            IZnodeRepository<ZnodeAriatOMSShippedOrder> _znodeAriatOMSShippedOrder = new ZnodeRepository<ZnodeAriatOMSShippedOrder>(new Custom_Entities());

            List<ZnodeAriatOMSShippedOrder> shippedOrders = null;

            if (omsOrderId > 0)
                shippedOrders = _znodeAriatOMSShippedOrder.Table.Where(x => x.IsMailSent == false && x.OMSOrderId == omsOrderId && x.OrderLineItemStateId == 20).ToList();
            else
                shippedOrders = _znodeAriatOMSShippedOrder.Table.Where(x => x.IsMailSent == false && x.OrderLineItemStateId == 20).ToList();

            if (shippedOrders.Count > 0)
            {
                var shippedOrderLine = shippedOrders.GroupBy(w => w.OMSOrderId).Select(g => new
                {
                    orderId = g.Key,
                    lineitemIds = g.Select(c => c.OmsOrderLineItemsId)
                });

                foreach (var item in shippedOrderLine)
                {
                    int orderId = item.orderId;

                    if (orderId > 0)
                    {
                        SendShippingNotification(orderId, item.lineitemIds, shippedOrders.Where(x => x.OMSOrderId == item.orderId).ToList(), _znodeAriatOMSShippedOrder);
                    }
                }
            }
        }
        #endregion

        #region Protected Method

        protected override IZnodeOrderReceipt GetOrderReceiptInstance(ZnodeOrderFulfillment order, Znode.Libraries.ECommerce.ShoppingCart.ZnodeShoppingCart shoppingCart)
        => new AriatOrderReceipt(order, shoppingCart);

        protected override IZnodeOrderReceipt GetOrderReceiptInstance(OrderModel order)
        {
            var objZnodeOrderReceipt = new AriatOrderReceipt();
            objZnodeOrderReceipt.FromApi = true;
            objZnodeOrderReceipt.OrderModel = order;
            return objZnodeOrderReceipt;
        }
        #endregion

        #region Private Method
        private void SendShippingNotification(int omsOrderId, IEnumerable<int> enumerable, List<ZnodeAriatOMSShippedOrder> lineItems, IZnodeRepository<ZnodeAriatOMSShippedOrder> _znodeAriatOMSShippedOrder)
        {
            try
            {
                ArtifiOrderService orderService = new ArtifiOrderService();

                OrderModel orderModel = orderService.GetOrderById(omsOrderId, null, new NameValueCollection
                {
                    { ZnodeOmsOrderDetailEnum.ZnodeOmsOrderLineItems.ToString().ToLower(), ZnodeOmsOrderDetailEnum.ZnodeOmsOrderLineItems.ToString().ToLower() },
                     { ZnodeOmsOrderDetailEnum.ZnodeShipping.ToString().ToLower(), ZnodeOmsOrderDetailEnum.ZnodeShipping.ToString().ToLower() },
                                 { ZnodeOmsOrderDetailEnum.ZnodeOmsOrder.ToString().ToLower(), ZnodeOmsOrderDetailEnum.ZnodeOmsOrder.ToString().ToLower() },{ ZnodeOmsOrderDetailEnum.ZnodePaymentType.ToString().ToLower(),ZnodeOmsOrderDetailEnum.ZnodePaymentType.ToString().ToLower()}
                });

                orderModel.OrderLineItems.RemoveAll(x => x.ParentOmsOrderLineItemsId == null || (!enumerable.Contains(Convert.ToInt32(x.Custom5)) || !Equals(x.OrderLineItemStateId, 20)));

                orderModel.OrderLineItems?.ForEach(orderLine =>
                {
                    ZnodeAriatOMSShippedOrder order = lineItems?
                           .FirstOrDefault(item => item.OmsOrderLineItemsId == Convert.ToInt32(orderLine.Custom5));

                    if (HelperUtility.IsNotNull(order))
                        orderLine.Quantity = order.ShippedQuantity;
                });

                //Send Mail to customer.
                orderService.SendOrderStatusEmail(orderModel);

                //Set Is mail sent to true.
                lineItems.Where(x => x.OMSOrderId == omsOrderId && x.OrderLineItemStateId == 20).ToList().ForEach(x => { x.IsMailSent = true; });

                //Update records in DB
                _znodeAriatOMSShippedOrder.BatchUpdate(lineItems);
            }
            catch (Exception ex)
            {

                ZnodeLogging.LogMessage($"Failed to send email notifcation for order number {lineItems?.First()?.OrderNumber}", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);

                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
            }
        }

        //returns the product type of the order line item by the relationship id of its child.

        private string GetLineItemProductType(OrderLineItemModel orderLineItem, List<ZnodeOmsOrderLineItem> orderLineItems)
        {
            switch (orderLineItems.FirstOrDefault(x => x.ParentOmsOrderLineItemsId == orderLineItem.OmsOrderLineItemsId)?.OrderLineItemRelationshipTypeId)
            {
                case (int)ZnodeCartItemRelationshipTypeEnum.Bundles: return ZnodeConstant.BundleProduct;
                case (int)ZnodeCartItemRelationshipTypeEnum.Group: return ZnodeConstant.GroupedProduct;
                case (int)ZnodeCartItemRelationshipTypeEnum.Simple: return ZnodeConstant.SimpleProduct;
                case (int)ZnodeCartItemRelationshipTypeEnum.Configurable: return ZnodeConstant.ConfigurableProduct;
                default: return null;
            }
        }

        private List<string> GetDownloadableProductKeyList(List<string> lineItemSKU)
        {
            List<string> orderProductKeySKUList = null;
            if (lineItemSKU?.Count > 0)
                orderProductKeySKUList = _pimDownloadableProduct.Table.Where(x => lineItemSKU.Contains(x.SKU)).Select(x => x.SKU)?.ToList();

            return orderProductKeySKUList;
        }

        private List<DefaultGlobalConfigModel> GetDefaultGlobalSettingData()
        {
            IDefaultGlobalConfigService defaultGlobalConfigService = ZnodeDependencyResolver.GetService<IDefaultGlobalConfigService>();
            return defaultGlobalConfigService.GetDefaultGlobalConfigList()?.DefaultGlobalConfigs;
        }

        private string GetDefaultPriceRoundOff(List<DefaultGlobalConfigModel> defaultGlobalSettingData)
           => defaultGlobalSettingData?.Where(x => string.Equals(x.FeatureName, GlobalSettingEnum.PriceRoundOff.ToString(), StringComparison.CurrentCultureIgnoreCase))?.Select(x => x.FeatureValues)?.FirstOrDefault();

        //Get default currency code.
        private string GetDefaultCulture(List<DefaultGlobalConfigModel> defaultGlobalSettingData)
           => defaultGlobalSettingData?.Where(x => string.Equals(x.FeatureName, GlobalSettingEnum.Culture.ToString(), StringComparison.CurrentCultureIgnoreCase))?.Select(x => x.FeatureValues)?.FirstOrDefault();
        //For Price according to currency.
        private static string FormatPriceWithCurrency(decimal? price, string CultureName, string defaultPriceRoundOff)
        {
            string currencyValue;
            if (IsNotNull(CultureName))
            {
                CultureInfo info = new CultureInfo(CultureName);
                info.NumberFormat.CurrencyDecimalDigits = Convert.ToInt32(defaultPriceRoundOff);
                currencyValue = $"{price.GetValueOrDefault().ToString("c", info.NumberFormat)}";
            }
            else
                currencyValue = Convert.ToString(price);

            return currencyValue;
        }

        private Dictionary<string, object> GetPersonalisedValueOrderLineItem(int orderLineItemId, List<ZnodeOmsPersonalizeItem> personalizeItems)
        {
            Dictionary<string, object> personaliseItem = new Dictionary<string, object>();
            if (orderLineItemId > 0 && personalizeItems?.Count > 0)
            {
                foreach (KeyValuePair<string, string> personaliseAttr in personalizeItems.Where(x => x.OmsOrderLineItemsId == orderLineItemId)?.ToDictionary(x => x.PersonalizeCode, x => x.PersonalizeValue))
                    personaliseItem.Add(personaliseAttr.Key, (object)personaliseAttr.Value);
            }

            return personaliseItem;
        }

        //Get the Order State to Customer List based on the Order Line Status Ids.
        private List<ZnodeOmsOrderShipment> GetOrderShipmentList(List<OrderLineItemModel> lineItems)
        {
            List<ZnodeOmsOrderShipment> orderShipmentList = null;
            if (lineItems?.Count > 0)
            {
                List<int> orderShipmentIds = lineItems.Select(x => x.OmsOrderShipmentId)?.Distinct()?.ToList();
                if (orderShipmentIds?.Count > 0)
                    orderShipmentList = _orderShipmentRepository.Table.Where(x => orderShipmentIds.Contains(x.OmsOrderShipmentId))?.ToList();
            }
            return orderShipmentList;
        }

        private List<ZnodeOmsPersonalizeItem> GetPersonalisedValueOrderLineItemList(List<OrderLineItemModel> lineItems)
        {
            List<ZnodeOmsPersonalizeItem> orderPersonalizeItemList = null;
            if (lineItems?.Count > 0)
            {
                List<int?> lineItemIds = lineItems.Select(x => Convert.ToInt32(x.ParentOmsOrderLineItemsId) > 0 ? x.ParentOmsOrderLineItemsId : x.OmsOrderLineItemsId)?.Distinct()?.ToList();
                if (lineItemIds?.Count > 0)
                    orderPersonalizeItemList = new ZnodeRepository<ZnodeOmsPersonalizeItem>().Table.Where(x => lineItemIds.Contains(x.OmsOrderLineItemsId)).ToList();
            }
            return orderPersonalizeItemList;
        }

        //Get the Order status list for Customer List based on the Order Line Status Ids.
        private List<ZnodeOmsOrderStateShowToCustomer> GetOrderStatusForCustomerList(List<OrderLineItemModel> lineItems)
        {
            List<ZnodeOmsOrderStateShowToCustomer> orderStatusList = null;
            if (lineItems?.Count > 0)
            {
                List<int?> statusList = lineItems.Select(x => x.OrderLineItemStateId)?.Distinct()?.ToList();
                if (statusList?.Count > 0)
                {
                    IZnodeRepository<ZnodeOmsOrderStateShowToCustomer> _orderStateShowToCustomerRepository = new ZnodeRepository<ZnodeOmsOrderStateShowToCustomer>();
                    orderStatusList = _orderStateShowToCustomerRepository.Table.Where(x => statusList.Contains(x.OmsOrderStateId))?.ToList();
                }

            }
            return orderStatusList;
        }

        private List<ZnodeOmsOrderLineItemsAdditionalCost> GetAdditionalCostList(List<OrderLineItemModel> orderLineItems)
        {
            List<ZnodeOmsOrderLineItemsAdditionalCost> additionalCostList = null;
            if (orderLineItems?.Count > 0)
            {
                List<int> parentLineItemIds = orderLineItems?.Where(x => x.ParentOmsOrderLineItemsId == null).Select(x => x.OmsOrderLineItemsId)?.ToList();
                if (parentLineItemIds?.Count > 0)
                    additionalCostList = new ZnodeRepository<ZnodeOmsOrderLineItemsAdditionalCost>().Table.Where(x => parentLineItemIds.Contains(x.OmsOrderLineItemsId.HasValue ? x.OmsOrderLineItemsId.Value : 0))?.ToList();
            }
            return additionalCostList;
        }

        //to Sett billing address
        private void SetOrderBillingAddress(OrderModel orderModel)
        {
            if (IsNotNull(orderModel))
            {

                if (IsNotNull(orderModel.BillingAddress))
                {
                    orderModel.BillingAddress.StateCode = new ZnodeRepository<ZnodeState>().Table.FirstOrDefault(x => x.StateName == orderModel.BillingAddress.StateName)?.StateCode;
                    ZnodeAddress billing = _addressRepository.Table.FirstOrDefault(x => x.AddressId == orderModel.BillingAddress.AddressId);
                    orderModel.BillingAddress.CompanyName = billing?.CompanyName;
                    //Sets the external id for billing address.
                    orderModel.BillingAddress.ExternalId = billing?.ExternalId;
                }
            }
        }

        //Get the Address List based on Order Shipment Address Ids.
        private List<ZnodeAddress> GetOrderShipmentAddressList(List<OrderShipmentModel> orderShipments)
        {
            List<ZnodeAddress> addressList = null;
            if (orderShipments?.Count > 0)
            {
                List<int> addressIds = orderShipments.Where(y => IsNotNull(y)).Select(x => x.AddressId)?.Distinct()?.ToList();
                if (addressIds?.Count > 0)
                    addressList = _addressRepository.Table.Where(x => addressIds.Contains(x.AddressId))?.ToList();
            }
            return addressList;
        }

        private string ShowOrderAdditionalDetails(string receiptContent, string customData)
        {
            if (!string.IsNullOrEmpty(customData))
            {
                return receiptContent.Replace("#AdyenCCAddress#", GenerateOrderAdditionalInfoTemplate(customData));
            }
            else
            {
                return receiptContent;
            }
        }
        //to generate order additional information template
        private string GenerateOrderAdditionalInfoTemplate(string customData)
        {
            string template = string.Empty;
            try
            {
                Dictionary<string, string> CustomDict = JsonConvert.DeserializeObject<Dictionary<string, string>>(customData);
                if (HelperUtility.IsNotNull(CustomDict))
                {
                    template = ("<b>Additional Information</b>");

                    if (CustomDict.ContainsKey("streetAddress1"))
                    {
                        template += $" <br />Address  { CustomDict["streetAddress1"]}";
                    }

                    if (CustomDict.ContainsKey("streetAddress2"))
                    {
                        template += $" <br />{CustomDict["streetAddress2"]}";
                    }

                    if (CustomDict.ContainsKey("city"))
                    {
                        template += $" <br />City {CustomDict["city"]}";
                    }

                    if (CustomDict.ContainsKey("state"))
                    {
                        template += $" <br />State {CustomDict["state"]}";
                    }

                    if (CustomDict.ContainsKey("postalCode"))
                    {
                        template += $" <br />Postal Code {CustomDict["postalCode"]}";
                    }
                    if (CustomDict.ContainsKey("country"))
                    {
                        template += $" <br />Country {CustomDict["country"]}";
                    }
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
            }
            return template;
        }

        private bool UpdateSavedCartItemQuantity(ArtifiShoppingCartItemModel artifiShoppingCartItemModel, int omsPersonalizedLineItemId, bool isOrder = false)
        {
            if (isOrder)
            {
                ZnodeOmsOrderLineItem orderLineItem = _omsOrderLineItemRepository.Table.FirstOrDefault(x => x.OmsOrderLineItemsId == omsPersonalizedLineItemId);
                if (IsNotNull(orderLineItem))
                {
                    orderLineItem.Quantity = artifiShoppingCartItemModel.Quantity;
                    orderLineItem.Sku = string.IsNullOrEmpty(artifiShoppingCartItemModel.SKU) ? orderLineItem.Sku : artifiShoppingCartItemModel.SKU;
                    bool status = _omsOrderLineItemRepository.Update(orderLineItem);
                    if (status)
                        UpdateAddonQuantity(artifiShoppingCartItemModel, omsPersonalizedLineItemId, isOrder);
                }
            }
            else
            {
                ZnodeOmsSavedCartLineItem savedCartLineItem = _omsSavedCartItemRepository.Table.FirstOrDefault(x => x.OmsSavedCartLineItemId == omsPersonalizedLineItemId);
                if (IsNotNull(savedCartLineItem))
                {
                    savedCartLineItem.Quantity = artifiShoppingCartItemModel.Quantity;
                    savedCartLineItem.SKU = string.IsNullOrEmpty(artifiShoppingCartItemModel.SKU) ? savedCartLineItem.SKU : artifiShoppingCartItemModel.SKU;
                    bool status = _omsSavedCartItemRepository.Update(savedCartLineItem);
                }
                if (IsNotNull(omsPersonalizedLineItemId) && omsPersonalizedLineItemId > 0)
                {
                    UpdateAddonQuantity(artifiShoppingCartItemModel, omsPersonalizedLineItemId, isOrder, true);
                }
            }

            return false;
        }

        //update addon quantity.
        private bool UpdateAddonQuantity(ArtifiShoppingCartItemModel artifiShoppingCartItemModel, int omsPersonalizedLineItemId, bool isOrder = false, bool isSaveCart = false)
        {
            if (isOrder)
            {
                IQueryable<ZnodeOmsOrderLineItem> orderLineItem = _omsOrderLineItemRepository.Table.Where(x => x.ParentOmsOrderLineItemsId == omsPersonalizedLineItemId);
                if (IsNotNull(orderLineItem))
                {
                    foreach (var item in orderLineItem)
                    {
                        item.Quantity = (item.Quantity * artifiShoppingCartItemModel.Quantity);
                        _omsOrderLineItemRepository.Update(item);
                    }
                }
                return true;
            }
            else
            {
                DataTable dataTable = ConvertCustomizationDataToDataTable(artifiShoppingCartItemModel);
                if (omsPersonalizedLineItemId > 0)
                {
                    ExecuteSpHelper objStoredProc = new ExecuteSpHelper();
                    objStoredProc.SetTableValueParameter("@OmsCartKeyValue", dataTable, ParameterDirection.Input, SqlDbType.Structured, "dbo.OmsCartKeyValue");
                    objStoredProc.GetParameter("@OmsSavedCartLineItemId", omsPersonalizedLineItemId, ParameterDirection.Input, SqlDbType.Int);
                    var savedCartLineItemList = objStoredProc.GetSPResultInDataSet("Znode_Ariat_UpdateSavedCartItem");
                }

                return true;
            }
        } 
        #endregion

    }
}