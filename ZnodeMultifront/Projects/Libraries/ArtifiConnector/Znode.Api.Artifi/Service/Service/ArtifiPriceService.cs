﻿using System.Collections.Generic;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Libraries.Admin;

namespace Znode.Engine.Api.Service
{
    public class ArtifiPriceService : BaseService, IArtifiPriceService
    {
        PublishProductHelper publishProductHelper = new PublishProductHelper();

        public PriceSKUModel GetProductPrice(int portalId, int userId, string sku, string clipArtSKU)
        {            
            List<string> skuList = new List<string>();
            skuList.Add(sku);

            if (!string.IsNullOrEmpty(clipArtSKU))
                skuList.Add(clipArtSKU);
            //sku.Split(',')?.ToList();
            
            List<PriceSKUModel> list = publishProductHelper.GetPricingBySKUs(skuList, portalId, userId);

            return new PriceSKUModel
            {
                RetailPrice = list?.Sum(x => x.RetailPrice),
                CurrencyCode = list.FirstOrDefault().CultureCode
                
            };
        }
    }
}