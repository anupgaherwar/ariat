﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using Znode.Artifi.Api.Model;
using Znode.Engine.Exceptions;
using Znode.Engine.Services;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Api.Service
{
    public class ArtifiPortalService : PortalService, IArtifiPortalService
    {
        #region Private Variables
        private readonly IZnodeRepository<ZnodePortal> _portalRepository;
        private readonly IZnodeRepository<AIZnodePortalSetting> _artifiZnodePortalSetting;
        #endregion

        #region Constructor
        public ArtifiPortalService()
        {
            _portalRepository = new ZnodeRepository<ZnodePortal>();
            _artifiZnodePortalSetting = new ZnodeRepository<AIZnodePortalSetting>();
        }

        #endregion

        #region Public Methods

        //Get tag manager data for store.
        public virtual PortalCustomizationSettingModel GetPortalCustomizationSetting(int portalId, NameValueCollection expands)
        {
            List<string> navigationProperties = GetExpands(expands);

            FilterCollection filter = new FilterCollection();
            filter.Add(ZnodeGoogleTagManagerEnum.PortalId.ToString(), FilterOperators.Equals, portalId.ToString());

            EntityWhereClauseModel whereClauseModel = DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filter.ToFilterDataCollection());

            AIZnodePortalSetting entity = _artifiZnodePortalSetting.GetEntity(whereClauseModel.WhereClause, navigationProperties);

            PortalCustomizationSettingModel portalcustomizationsettingmodel = entity?.ToModel<PortalCustomizationSettingModel>();

            if (HelperUtility.IsNull(portalcustomizationsettingmodel))
                return new PortalCustomizationSettingModel { PortalId = portalId, PortalName = _portalRepository.Table.FirstOrDefault(x => x.PortalId == portalId)?.StoreName };

            portalcustomizationsettingmodel.PortalName = _portalRepository.Table.FirstOrDefault(x => x.PortalId == portalId)?.StoreName;

            return portalcustomizationsettingmodel;
        }

        //Save tag manager data for store.
        public virtual bool CreateEditCustomizationSetting(PortalCustomizationSettingModel portalCustomizationSettingModel)
        {
            if (HelperUtility.IsNull(portalCustomizationSettingModel))
                throw new ZnodeException(ErrorCodes.NullModel, "Portal Customization Setting Model can not be null");

            bool isUpdateStatus = true;

            portalCustomizationSettingModel.DomainName = portalCustomizationSettingModel.DomainName.Contains("http://") || portalCustomizationSettingModel.DomainName.Contains("https://") ? portalCustomizationSettingModel.DomainName : string.Format("{0}{1}", "http://", portalCustomizationSettingModel.DomainName);

            if (portalCustomizationSettingModel.AIPortalSettingId < 1)
                _artifiZnodePortalSetting.Insert(portalCustomizationSettingModel.ToEntity<AIZnodePortalSetting>());
            else
                isUpdateStatus = _artifiZnodePortalSetting.Update(portalCustomizationSettingModel.ToEntity<AIZnodePortalSetting>());

            return isUpdateStatus;
        }
        #endregion

        #region Private Methods
        //Get expands and add them to navigation properties
        private List<string> GetExpands(NameValueCollection expands)
        {
            List<string> navigationProperties = new List<string>();
            if (expands != null && expands.HasKeys())
            {

                foreach (string key in expands.Keys)
                {
                    if (Equals(key, ZnodePortalEnum.ZnodeDomains.ToString().ToLower())) SetExpands(ZnodePortalEnum.ZnodeDomains.ToString(), navigationProperties);
                    if (Equals(key, ZnodePortalFeatureMapperEnum.ZnodePortalFeature.ToString().ToLower())) SetExpands(ZnodePortalFeatureMapperEnum.ZnodePortalFeature.ToString(), navigationProperties);
                    if (Equals(key, ZnodePortalEnum.ZnodeOmsOrderState.ToString().ToLower())) SetExpands(ZnodePortalEnum.ZnodeOmsOrderState.ToString(), navigationProperties);
                    if (Equals(key, ZnodePortalAlternateWarehouseEnum.ZnodeWarehouse.ToString().ToLower())) SetExpands(ZnodePortalAlternateWarehouseEnum.ZnodeWarehouse.ToString(), navigationProperties);
                    if (Equals(key, ZnodePortalEnum.ZnodePortalCatalogs.ToString().ToLower())) SetExpands(ZnodePortalEnum.ZnodePortalCatalogs.ToString(), navigationProperties);
                    if (Equals(key, ZnodePortalEnum.ZnodePortalLocales.ToString().ToLower())) SetExpands(ZnodePortalEnum.ZnodePortalLocales.ToString(), navigationProperties);
                }
            }
            return navigationProperties;
        }
        #endregion
    }
}