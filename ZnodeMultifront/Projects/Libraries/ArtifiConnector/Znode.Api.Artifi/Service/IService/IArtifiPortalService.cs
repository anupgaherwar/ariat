﻿using System.Collections.Specialized;
using Znode.Artifi.Api.Model;

namespace Znode.Engine.Api.Service
{
    public interface IArtifiPortalService
    {
        /// <summary>
        /// Create or update portal product customization setting
        /// </summary>
        /// <param name="customizationModel">model with data</param>
        /// <returns>true or false</returns>
        bool CreateEditCustomizationSetting(PortalCustomizationSettingModel customizationModel);

        /// <summary>
        /// Get Portal product customization setting data.
        /// </summary>
        /// <param name="portalId">portal id.</param>
        /// <param name="expands">Expand Collection.</param>
        /// <returns>model with portal product customization setting</returns>
        PortalCustomizationSettingModel GetPortalCustomizationSetting(int portalId, NameValueCollection expands);
    }
}
