﻿using Znode.Artifi.Api.Model;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Service
{
    public interface IArtifiOrderService:IOrderService
    {

        /// <summary>
        /// Get updated cart with customised design.
        /// </summary>
        /// <param name="artifiShoppingCartItemModel"></param>
        /// <returns>bool</returns>
        bool UpdateCartItemDesign(ArtifiShoppingCartItemModel artifiShoppingCartItemModel);        

        /// <summary>
        /// Update the customised cart item design.
        /// </summary>
        /// <param name="artifiShoppingCartItemModel"></param>
        /// <returns></returns>
        bool UpdateSavedCartItemDesign(ArtifiShoppingCartItemModel artifiShoppingCartItemModel);

        /// <summary>
        /// Get UserId against order.
        /// </summary>
        /// <param name="omsOrderId">Id of order.</param>
        /// <returns></returns>
        int GetUserIdByOrderId(int omsOrderId);
        //string GenerateOrderNumber(SubmitOrderModel submitOrderModel, ParameterModel portalId = null);
    }
}
