﻿using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Service
{
    public interface IArtifiPriceService
    {
        PriceSKUModel GetProductPrice(int portalId, int userId, string sku, string clipArtSKU);
    }
}
