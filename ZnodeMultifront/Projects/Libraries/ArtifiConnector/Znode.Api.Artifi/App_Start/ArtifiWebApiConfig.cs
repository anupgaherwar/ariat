﻿using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Routing;

namespace Znode.Engine.Api
{
    public static class ArtifiWebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute("artifiorder-create", "artifiorder/create", new { controller = "artifiorder", action = "create" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) });

            config.Routes.MapHttpRoute("artifiorder-get", "artifiorder/{orderId}", new { controller = "artifiorder", action = "get" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Get) });
            config.Routes.MapHttpRoute("portalcustomization-create", "artifiportal/createeditcustomizationsetting", new { controller = "artifiportal", action = "createeditcustomizationsetting" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Put) });

            config.Routes.MapHttpRoute("portalcustomization-get", "artifiportal/getportalcustomizationsetting/{portalId}", new { controller = "artifiportal", action = "getportalcustomizationsetting" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Get) });

            config.Routes.MapHttpRoute("artifishoppingcarts-create", "artifishoppingcarts/create", new { controller = "artifishoppingcart", action = "create" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) });

            config.Routes.MapHttpRoute("artifigetartificustomisedcartitemlist-get", "artifiorder/getartificustomisedcartitemlist/{userId}", new { controller = "artifiorder", action = "getartificustomisedcartitemlist" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Get) });

            config.Routes.MapHttpRoute("artifiupdatecartitemdesign", "artifiorder/updatecartitemdesign", new { controller = "artifiorder", action = "updatecartitemdesign" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Put) });

            config.Routes.MapHttpRoute("artifiupdatesavedcartitemdesign", "artifiorder/updatesavedcartitemdesign", new { controller = "artifiorder", action = "updatesavedcartitemdesign" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Put) });

            config.Routes.MapHttpRoute("artifigetfromcookiedetailsmodel-get", "artifiorder/getfromcookiedetailsmodel", new { controller = "artifiorder", action = "GetFromCookieDetailsModel" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) });

            config.Routes.MapHttpRoute("artifi-removeallcustomiseddesignbyuserid", "artifiorder/removeallcustomiseddesignbyuserid", new { controller = "artifiorder", action = "removeallcustomiseddesignbyuserid" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) });

            config.Routes.MapHttpRoute("artifiremovecustomiseddesignbyskuanduserid", "artifiorder/removecustomiseddesignbyskuanduserid", new { controller = "artifiorder", action = "removecustomiseddesignbyskuanduserid" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) });

            config.Routes.MapHttpRoute("artifiremovecustomiseddesignid", "artifiorder/removecustomiseddesignid/{designId}", new { controller = "artifiorder", action = "removecustomiseddesignid" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Get) });

            config.Routes.MapHttpRoute("artifiupdatemanageorderdesign", "artifiorder/updatemanageorderdesign", new { controller = "artifiorder", action = "updatemanageorderdesign" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) });

            config.Routes.MapHttpRoute("artifiorder-update", "artifiorder/update", new { controller = "artifiorder", action = "update" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Put) });

            config.Routes.MapHttpRoute("artifiorder-getorderlineitembyid", "artifiorder/getorderlineitembyid/{lineItemId}", new { controller = "artifiorder", action = "getorderlineitembyid" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Get) });
            config.Routes.MapHttpRoute("artifiorder-getuseridbyorderid", "artifiorder/getuseridbyorderid/{omsOrderId}", new { controller = "artifiorder", action = "getuseridbyorderid" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Get) });
            //config.Routes.MapHttpRoute("artifiorder-generateordernumber", "artifiorder/generateordernumber", new { controller = "artifiorder", action = "generateordernumber" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Get) });
        }
    }
}