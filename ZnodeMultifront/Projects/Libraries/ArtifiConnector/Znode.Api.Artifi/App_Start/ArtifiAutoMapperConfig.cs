﻿using AutoMapper;

using Znode.Artifi.Api.Model;
using Znode.Artifi.Data;
using Znode.Libraries.Data.DataModel;

namespace Znode.Engine.Api
{
    public static class ArtifiAutoMapperConfig
    {
        public static void Execute()
        {
            Mapper.CreateMap<AIZnodePortalSetting, PortalCustomizationSettingModel>().ReverseMap();

            Mapper.CreateMap<AIZnodeSavedCartLineItemDesign, ArtifiDesignDetailsModel>()
                .ForMember(d => d.OrderLineItemsDesignId, opt => opt.MapFrom(src => src.AIDesignId));

            Mapper.CreateMap<ArtifiDesignDetailsModel, AIZnodeSavedCartLineItemDesign>()
           .ForMember(d => d.AIDesignId, opt => opt.MapFrom(src => src.OrderLineItemsDesignId));
        }
    }
}