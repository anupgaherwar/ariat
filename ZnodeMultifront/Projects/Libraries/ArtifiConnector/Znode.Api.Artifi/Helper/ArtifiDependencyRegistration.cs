﻿using Autofac;
using Znode.Engine.Api.Service;
using Znode.Engine.Services;
using Znode.Libraries.Framework.Business;

namespace Znode.Engine.Api
{
    public class ArtifiDependencyRegistration : IDependencyRegistration
    {
        /// <summary>
        /// Register the Dependency Injection types.
        /// </summary>
        /// <param name="builder">Autofac Container Builder</param>
        public virtual void Register(ContainerBuilder builder)
        {
            builder.RegisterType<ArtifiPortalService>().As<IArtifiPortalService>().InstancePerRequest();

            //Here override znode base code method by injecting dependancy mention as below.
            //"In CustomPortalService.cs we have override 'DeletePortal()' of znode base code".
            builder.RegisterType<ArtifiPortalService>().As<IPortalService>().InstancePerRequest();

            builder.RegisterType<ArtifiOrderService>().As<IOrderService>().InstancePerRequest();
            builder.RegisterType<ArtifiOrderService>().As<OrderService>().InstancePerRequest();
            builder.RegisterType<ArtifiOrderService>().As<IArtifiOrderService>().InstancePerRequest();
            builder.RegisterType<ArtifiPriceService>().As<IArtifiPriceService>().InstancePerRequest();
        }

        /// <summary>
        /// Order method represents Dependency Injection Registration Order.
        /// For znode base code Library the DI registration order set to 0.
        /// For custom code library the DI registration order should be incremental.
        /// </summary>
        public int Order
        {
            get { return 2; }
        }
    }
}