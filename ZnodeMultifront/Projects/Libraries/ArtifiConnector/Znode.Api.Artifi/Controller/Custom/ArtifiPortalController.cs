﻿using System;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Znode.Artifi.Api.Model;
using Znode.Artifi.Api.Model.Responses;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Controllers;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Api.Service;
using Znode.Engine.Exceptions;
using Znode.Libraries.Framework.Business;

namespace Znode.Engine.Api.Controller.Sample
{
    public class ArtifiPortalController : BaseController
    {
        #region Private Variables
        private readonly IArtifiPortalCache _customCache;
        private readonly IArtifiPortalService _customService;

        #endregion

        #region Constructor
        public ArtifiPortalController(IArtifiPortalService portalService)
        {
            _customService = portalService;
            _customCache = new ArtifiPortalCache(_customService);
        }
        #endregion

        #region Public Methods

        [ResponseType(typeof(PortalCustomizationResponse))]
        [HttpGet]
        public virtual HttpResponseMessage GetPortalCustomizationSetting(int portalId)
        {
            HttpResponseMessage response;
            try
            {
                string data = _customCache.GetPortalCustomizationSetting(portalId, RouteUri, RouteTemplate);
                response = !string.IsNullOrEmpty(data) ? CreateOKResponse<PortalCustomizationResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                PortalCustomizationResponse data = new PortalCustomizationResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// Save tag manager data.
        /// </summary>
        /// <param name="customizationModel">Model with tag manager data.</param>
        /// <returns>Returns tag manager information.</returns>
        [ResponseType(typeof(PortalCustomizationResponse))]
        [HttpPut]
        public virtual HttpResponseMessage CreateEditCustomizationSetting([FromBody] PortalCustomizationSettingModel customizationModel)
        {
            HttpResponseMessage response;
            try
            {
                response = _customService.CreateEditCustomizationSetting(customizationModel)
                    ? CreateOKResponse(new PortalCustomizationResponse { CustomizationSettingModel = customizationModel })
                    : CreateInternalServerErrorResponse();
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex.ToString(), ZnodeLogging.Components.Setup.ToString());
                response = CreateInternalServerErrorResponse(new TagManagerResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode });
            }
            catch (Exception ex)
            {
                response = CreateInternalServerErrorResponse(new TagManagerResponse { HasError = true, ErrorMessage = ex.Message });
                ZnodeLogging.LogMessage(ex.ToString(), ZnodeLogging.Components.Setup.ToString());
            }
            return response;
        } 

        #endregion
    }
}