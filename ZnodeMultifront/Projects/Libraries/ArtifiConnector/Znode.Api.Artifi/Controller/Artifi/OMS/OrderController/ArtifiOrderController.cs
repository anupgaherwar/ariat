﻿using System;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Znode.Artifi.Api.Model;
using Znode.Artifi.Api.Model.Responses;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Controllers;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Api.Service;
using Znode.Engine.Exceptions;
using Znode.Libraries.Framework.Business;

namespace Znode.Engine.Api.Controller
{
    public class ArtifiOrderController : BaseController
    {

        #region Private Variables
        private readonly IArtifiOrderService _artifiOrderService;
        #endregion

        public ArtifiOrderController(IArtifiOrderService artifiOrderService)
        {
            _artifiOrderService = artifiOrderService;
        }


        [ResponseType(typeof(TrueFalseResponse))]
        [HttpPut]
        public virtual HttpResponseMessage UpdateCartItemDesign([FromBody] ArtifiShoppingCartItemModel artifiShoppingCartItemModel)
        {
            HttpResponseMessage response;
            try
            {
                response = CreateOKResponse(new TrueFalseResponse { IsSuccess = _artifiOrderService.UpdateCartItemDesign(artifiShoppingCartItemModel) });
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex.ToString());
                response = CreateInternalServerErrorResponse(new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode });
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex.ToString());
                response = CreateInternalServerErrorResponse(new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message });
            }
            return response;
        }

        [ResponseType(typeof(TrueFalseResponse))]
        [HttpPut]
        public virtual HttpResponseMessage UpdateSavedCartItemDesign([FromBody] ArtifiShoppingCartItemModel artifiShoppingCartItemModel)
        {
            HttpResponseMessage response;
            try
            {
                response = CreateOKResponse(new TrueFalseResponse { IsSuccess = _artifiOrderService.UpdateSavedCartItemDesign(artifiShoppingCartItemModel) });
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex.ToString());
                response = CreateInternalServerErrorResponse(new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode });
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex.ToString());
                response = CreateInternalServerErrorResponse(new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message });
            }
            return response;
        }

        /// <summary>
        /// Get order details by order id.
        /// </summary>
        /// <param name="orderId">order Id</param>
        /// <returns>HttpResponseMessage</returns>
        [HttpGet]
        [ResponseType(typeof(CountResponse))]
        public HttpResponseMessage GetUserIdByOrderId(int omsOrderId)
        {
            HttpResponseMessage response;

            try
            {
                int userId = _artifiOrderService.GetUserIdByOrderId(omsOrderId);
                response = userId > 0 ? CreateOKResponse(new CountResponse { Count = userId }) : CreateNoContentResponse();
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex.ToString());
                response = CreateInternalServerErrorResponse(new CountResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode });
            }
            catch (Exception ex)
            {
                response = CreateInternalServerErrorResponse(new CountResponse { HasError = true, ErrorMessage = ex.Message });
                ZnodeLogging.LogMessage(ex.ToString(), ZnodeLogging.Components.OMS.ToString());
            }

            return response;
        }

        [HttpGet]
        [ResponseType(typeof(GenerateOrderResponse))]
        public HttpResponseMessage GenerateOrderNumber(SubmitOrderModel submitOrderModel, ParameterModel portalId = null)
        {
            HttpResponseMessage response;

            try
            {
                string OrderNumber = _artifiOrderService.GenerateOrderNumber(submitOrderModel,portalId);
                response = !string.IsNullOrEmpty(OrderNumber) ? CreateOKResponse<GenerateOrderResponse>(OrderNumber) : CreateNoContentResponse();
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex.ToString());
                response = CreateInternalServerErrorResponse(new GenerateOrderResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode });
            }
            catch (Exception ex)
            {
                response = CreateInternalServerErrorResponse(new GenerateOrderResponse { HasError = true, ErrorMessage = ex.Message });
                ZnodeLogging.LogMessage(ex.ToString(), ZnodeLogging.Components.OMS.ToString());
            }

            return response;
        }
    }
}