﻿using System;
using System.Globalization;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Service;
using Znode.Engine.Exceptions;
using Znode.Engine.Services;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;

namespace Znode.Engine.Api.Controllers
{
    public class ArtifiPriceController : BaseController
    {

        #region Private Variables
        private readonly IArtifiPriceService _artifiPriceService;
        #endregion

        #region Public Constructor
        public ArtifiPriceController(IArtifiPriceService artifiPriceService)
        {
            _artifiPriceService = artifiPriceService;
        }
        #endregion

        /// <summary>
        /// Creates a new shopping cart and saves it to the database.
        /// </summary>
        /// <param name="portalId">Id of portal</param>        
        /// <param name="sku">sku of product.</param>
        /// <param name="userId">Id of user.</param>
        /// <returns></returns>
        [HttpGet, Route("artifiprice/getproductprice/{portalId=portalId}/{sku=sku}/{clipArtSKU_Image=clipArtSKU_Image}/{userId=userId}")]
        public HttpResponseMessage GetProductPrice(int portalId, string sku, string clipArtSKU_Image, int? userId)
        {
            HttpResponseMessage response;
            try
            {
                PriceSKUModel productPrice = _artifiPriceService.GetProductPrice(portalId, userId.GetValueOrDefault(), sku, clipArtSKU_Image);
                string price = productPrice.SalesPrice > 0 ? FormatPriceWithCurrency(productPrice.SalesPrice, productPrice.CurrencyCode) :
                    FormatPriceWithCurrency(productPrice.RetailPrice, productPrice.CurrencyCode);
                response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(price);
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");

            }
            catch (ZnodeException ex)
            {
                response = CreateInternalServerErrorResponse(ex.ErrorMessage);
                ZnodeLogging.LogMessage(ex.Message, ZnodeLogging.Components.Portal.ToString());
            }
            catch (Exception ex)
            {
                response = CreateInternalServerErrorResponse(ex.Message);
                ZnodeLogging.LogMessage(ex.Message, ZnodeLogging.Components.Portal.ToString());
            }
            return response;
        }

        #region Private Method
        private string FormatPriceWithCurrency(decimal? price, string currencyName, string UOM = "")
        {
            if (HelperUtility.IsNotNull(price))
            {
                string currencyValue;
                if (!string.IsNullOrEmpty(currencyName))
                {
                    CultureInfo info = new CultureInfo(currencyName);
                    info.NumberFormat.CurrencyDecimalDigits = 2;
                    currencyValue = ServiceHelper.FormatPriceWithCurrency(price, currencyName); 
                }
                else
                {
                    currencyValue = Convert.ToString(price);
                }

                return !string.IsNullOrEmpty(UOM) ? string.Format("{0} / {1}", currencyValue, UOM) : currencyValue;
            }
            return null;
        }
        #endregion
    }
}