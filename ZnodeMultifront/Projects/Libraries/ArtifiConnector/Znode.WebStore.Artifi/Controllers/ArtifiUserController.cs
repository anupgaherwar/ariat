﻿using System.Web.Mvc;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.ViewModels;

namespace Znode.Engine.WebStore.Controllers
{
    public class ArtifiUserController : BaseController
    {
        [Authorize]
        public ActionResult GetAllArtifiDesign()
        {
            //If session expires redirect to login page.
            if (string.IsNullOrEmpty(((UserViewModel)Session[WebStoreConstants.UserAccountKey])?.RoleName))
                return RedirectToAction<UserController>(x => x.Login(string.Empty));

            ViewBag.PortalId = PortalAgent.CurrentPortal.PortalId;
            string getAllArtifiDesignView = $"~/Views/Themes/{PortalAgent.CurrentPortal.Theme}/Views/User/GetAllArtifiDesign.cshtml";
            return View(getAllArtifiDesignView);
        }
    }
}