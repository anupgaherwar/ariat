﻿using System.Web.Mvc;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.ViewModel;
using Znode.Engine.WebStore.ViewModels;

namespace Znode.Engine.WebStore.Controllers
{
    public class ArtifiCheckoutController : CheckoutController
    {
        #region Private Variable 
        private readonly IArtifiCheckoutAgent _artifiCheckoutAgent;
        private readonly ICartAgent _cartAgent;
        #endregion

        public ArtifiCheckoutController(IUserAgent userAgent, ICheckoutAgent checkoutAgent, ICartAgent cartAgent, IPaymentAgent paymentAgent, IArtifiCheckoutAgent artifiCheckoutAgent) :
            base(userAgent, checkoutAgent, cartAgent, paymentAgent)
        {
            _artifiCheckoutAgent = artifiCheckoutAgent;
            _cartAgent = cartAgent;
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult GetArtifiCredentials(int portalId, int userId = 0)
        {
            PortalCustomizationSettingViewModel portalCustomizationSettingViewModel = _artifiCheckoutAgent.GetArtifiCredentials(portalId, userId);
            return Json(new
            {
                WebApiClientKey = portalCustomizationSettingViewModel.WebApiClientKey,
                WebsiteCode = portalCustomizationSettingViewModel.WebsiteCode,
                JsUrl = portalCustomizationSettingViewModel.JsUrl,
                userId = portalCustomizationSettingViewModel.UserId,
                portalId = portalId,
                isGuest = portalCustomizationSettingViewModel.IsGuset
            }, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult GetUserIdByOrderId(int omsOrderId)
        => Json(new
        {
            userId = _artifiCheckoutAgent.GetUserIdByOrderId(omsOrderId),
        }, JsonRequestBehavior.AllowGet);

        public virtual ActionResult CartReviewOnCheckout(int? shippingOptionId, int? shippingAddressId, string shippingCode, string additionalInstruction = "", bool isquoteRequest = false, string poNumber = "")
        {
            CartViewModel cartViewModel = _cartAgent.CalculateShipping(shippingOptionId.GetValueOrDefault(), shippingAddressId.GetValueOrDefault(), shippingCode, additionalInstruction, isquoteRequest);
            cartViewModel.JobName = poNumber;
            return PartialView("_CartReview", cartViewModel);
        }
    }
}