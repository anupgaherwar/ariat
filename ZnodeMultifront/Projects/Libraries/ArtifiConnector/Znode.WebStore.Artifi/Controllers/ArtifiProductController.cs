﻿using System.Collections.Generic;
using System.Web.Mvc;

using Znode.Artifi.Api.Model;
using Znode.Engine.WebStore.Agents;

namespace Znode.Engine.WebStore.Controllers
{
    public class ArtifiProductController : BaseController
    {
        #region Private Variable 
        private readonly IArtifiCartAgent _artifiCartAgent;
        private readonly IArtifiProductAgent _artifiProductAgent;
        #endregion

        public ArtifiProductController(IArtifiCartAgent artifiCartAgent, IArtifiProductAgent artifiProductAgent)
        {
            _artifiCartAgent = artifiCartAgent;
            _artifiProductAgent = artifiProductAgent;
        }

        [Route("Product/EditArtifiProduct")]
        public virtual ActionResult EditArtifiProduct(int id, string configurablesku) => View(_artifiProductAgent.GetProductDetailsForEditDesign(id, configurablesku));

        [HttpPost]
        public ActionResult UpdateArtifiCartItem(int orderLineItemsDesignId, string aiImagePath, decimal quantity, string sku = null, Dictionary<string, string> customization = null)
        {
            bool status = _artifiCartAgent.UpdateArtifiCartItem(new ArtifiShoppingCartItemModel { OrderLineItemsDesignId = orderLineItemsDesignId, AIImagePath = aiImagePath, Quantity = quantity, SKU = sku, Customization = customization });

            return Json(new
            {
                status = status
            }, JsonRequestBehavior.AllowGet);
        }
    }
}