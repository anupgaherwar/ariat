﻿using Znode.Artifi.Api.Model;
using Znode.Engine.Api.Client;

namespace Znode.Engine.WebStore.Clients
{
    public interface IArtifiOrderClient : IBaseClient
    {       
        /// <summary>
        /// Get artifi credentials by portal Id.
        /// </summary>
        /// <param name="portalId"></param>
        /// <returns></returns>
        PortalCustomizationSettingModel GetArtifiCredentials(int portalId);

        /// <summary>
        /// Get UserId against order.
        /// </summary>
        /// <param name="omsOrderId">Id of order.</param>
        /// <returns></returns>
        int GetUserIdByOrderId(int omsOrderId);
        /// <summary>
        /// Get Order Number.
        /// </summary>
        /// <param name="portalId">Id of portal.</param>
        /// <returns></returns>
        string GenerateOrderNumber(int portalId);
    }
}
