﻿using Znode.Engine.Api.Client;
using Znode.Artifi.Api.Model;

namespace Znode.Engine.WebStore.Clients
{
    public interface IArtifiCartClient : IBaseClient
    {
        /// <summary>
        /// Update the save cart item.
        /// </summary>
        /// <param name="artifiShoppingCartItemModel"></param>
        /// <returns></returns>
        bool UpdateSavedCartItemDesign(ArtifiShoppingCartItemModel artifiShoppingCartItemModel);
    }
}
