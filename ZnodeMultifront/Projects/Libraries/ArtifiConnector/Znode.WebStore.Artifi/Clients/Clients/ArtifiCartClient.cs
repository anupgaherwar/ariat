﻿using Newtonsoft.Json;
using System.Net;
using Znode.Artifi.Api.Model;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
using Znode.Engine.WebStore.Endpoints;

namespace Znode.Engine.WebStore.Clients
{
    public class ArtifiCartClient : BaseClient, IArtifiCartClient
    {
        public bool UpdateSavedCartItemDesign(ArtifiShoppingCartItemModel artifiShoppingCartItemModel)
        {
            string endpoint = ArtifiEndpoint.UpdateSavedCartItemDesign();

            ApiStatus status = new ApiStatus();
            TrueFalseResponse response = PutResourceToEndpoint<TrueFalseResponse>(endpoint, JsonConvert.SerializeObject(artifiShoppingCartItemModel), status);
            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return response.IsSuccess;
        }
    }
}