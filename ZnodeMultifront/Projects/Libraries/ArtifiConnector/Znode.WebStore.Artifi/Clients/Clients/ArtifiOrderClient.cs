﻿using System.Collections.ObjectModel;
using System.Net;
using Znode.Artifi.Api.Model;
using Znode.Artifi.Api.Model.Responses;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
using Znode.Engine.WebStore.Clients;
using Znode.Engine.WebStore.Endpoints;

namespace Znode.Engine.WebStore.Client
{
    public class ArtifiOrderClient : BaseClient, IArtifiOrderClient
    {

        public PortalCustomizationSettingModel GetArtifiCredentials(int portalId)
        {
            string endpoint = ArtifiEndpoint.GetPortalCustomizationArtifiSetting(portalId);
            endpoint += BuildEndpointQueryString(null);

            ApiStatus status = new ApiStatus();

            PortalCustomizationResponse response = GetResourceFromEndpoint<PortalCustomizationResponse>(endpoint, status);
            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return response?.CustomizationSettingModel;
        }

        public int GetUserIdByOrderId(int omsOrderId)
        {
            string endpoint = ArtifiEndpoint.GetUserIdByOrderId(omsOrderId);
            ApiStatus status = new ApiStatus();

            CountResponse response = GetResourceFromEndpoint<CountResponse>(endpoint, status);
            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (response?.Count).GetValueOrDefault();
        }

        public string GenerateOrderNumber(int portalId)
        {
            string endpoint = ArtifiEndpoint.GenerateOrderNumber(portalId);
            endpoint += BuildEndpointQueryString(null);

            ApiStatus status = new ApiStatus();
            GenerateOrderResponse response = GetResourceFromEndpoint<GenerateOrderResponse>(endpoint, status);

            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NoContent };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return response.OrderNumber;
        }
    }
}