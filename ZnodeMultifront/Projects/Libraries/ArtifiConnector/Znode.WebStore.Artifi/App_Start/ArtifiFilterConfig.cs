﻿using System.Web.Mvc;

namespace Znode.Engine.WebStore
{
    public class ArtifiFilterConfig
    {
        public static void ArtifiRegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new ArtifiAuthenticationHelper());
        }
    }
}