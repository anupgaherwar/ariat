﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace Znode.Engine.WebStore
{
    public class ArtifiBundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                      "~/Scripts/lib/jquery-1.10.2.js",
                      "~/Scripts/lib/jquery-ui.js",
                      "~/Scripts/lib/jquery.validate.js",
                      "~/Scripts/lib/jquery.validate.unobtrusive.js",
                      "~/Scripts/lib/DynamicValidation.js",
                      "~/Scripts/lib/jquery.unobtrusive-ajax.js"));



            bundles.Add(new ScriptBundle("~/bundles/CoreJs").Include(
                      "~/Scripts/Core/Model/Znode.Model.js",
                      "~/Scripts/Core/Common/ZnodeGlobal.js",
                      "~/Scripts/Core/Common/ZnodeHelper.js",
                      "~/Scripts/Core/Endpoint/ZnodeEndpoint.js",
                      "~/Scripts/Core/Controls/WebGrid/DynamicGrid.js",
                      "~/Scripts/Core/Controls/WebGrid/CustomJurl.js",
                      "~/Scripts/Core/Controls/WebGrid/GridPager.js",
                      "~/Scripts/Core/Controls/WebGrid/EditableDynamicGrid.js",
                      "~/Scripts/Core/Controls/WebGrid/jurl.js",
                      "~/Scripts/Core/Controls/WebGrid/jurl.min.js",
                      "~/Scripts/References/WebGrid/xmlGenerator.js",
                      "~/Scripts/Core/Controls/ZnodeMultiSelect.js",
                      "~/Scripts/ClientResource/Resource.en.js",
                      "~/Scripts/ClientResource/Resource.de.js",
                      "~/Scripts/ClientResource/Resource.fr.js"));




            bundles.Add(new ScriptBundle("~/bundles/CustomJs").Include(
                      "~/Scripts/Custom/Newsletter.js",
                         "~/Scripts/Custom/Product.js",
                         "~/Scripts/Custom/Category.js",
                         "~/Scripts/Custom/Navigation.js",
                         "~/Scripts/Custom/QuickOrder.js",
                         "~/Scripts/Custom/QuickOrderPad.js",
                         "~/Scripts/Custom/Search.js",
                         "~/Scripts/Custom/ZSearch.js",
                         "~/Scripts/Custom/Cart.js",
                         "~/Scripts/Custom/User.js",
                         "~/Scripts/Custom/ZnodeNotification.js",
                         "~/Scripts/Custom/Search.js",
                         "~/Scripts/Custom/Brand.js",
                         "~/Scripts/Custom/Checkout.js",
                         "~/Scripts/Custom/CaseRequest.js",
                         "~/Scripts/Custom/Home.js",
                         "~/Scripts/Custom/Config.js",
                         "~/Scripts/Custom/SiteMap.js",
                         "~/Scripts/lib/typeahead.bundle.js",
                         "~/Scripts/lib/typeahead.bundle.orig.js",
                         "~/Scripts/lib/typeahead.mvc.model.js"));

            //var bu = new ScriptBundle("~/bundles/CustomJs").Include(
            //             "~/Scripts/Custom/Newsletter.js",
            //             "~/Scripts/Custom/Product.js",
            //             "~/Scripts/Custom/Category.js",
            //             "~/Scripts/Custom/Navigation.js",
            //             "~/Scripts/Custom/QuickOrder.js",
            //             "~/Scripts/Custom/QuickOrderPad.js",
            //             "~/Scripts/Custom/Search.js",
            //             "~/Scripts/Custom/ZSearch.js",
            //             "~/Scripts/Custom/Cart.js",
            //             "~/Scripts/Custom/User.js",
            //             "~/Scripts/Custom/ZnodeNotification.js",
            //             "~/Scripts/Custom/Search.js",
            //             "~/Scripts/Custom/Brand.js",
            //             "~/Scripts/Custom/Checkout.js",
            //             "~/Scripts/Custom/CaseRequest.js",
            //             "~/Scripts/Custom/Home.js",
            //             "~/Scripts/Custom/Config.js",
            //             "~/Scripts/Custom/SiteMap.js",
            //             "~/Scripts/lib/typeahead.bundle.js",
            //             "~/Scripts/lib/typeahead.bundle.orig.js",
            //             "~/Scripts/lib/typeahead.mvc.model.js");

            //bu.Transforms.Clear();
            //bundles.Add(bu);
            BundleTable.EnableOptimizations = false;
        }
    }
}