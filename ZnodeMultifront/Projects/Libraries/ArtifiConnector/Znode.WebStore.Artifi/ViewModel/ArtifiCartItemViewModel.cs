﻿namespace Znode.Engine.WebStore.ViewModel
{
    public class ArtifiCartItemViewModel : BaseViewModel
    {
        public string AISavedDesigns { get; set; }
        public int OrderLineItemsDesignId { get; set; }
    }
}