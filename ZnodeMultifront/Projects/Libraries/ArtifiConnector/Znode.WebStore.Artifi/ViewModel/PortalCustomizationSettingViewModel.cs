﻿namespace Znode.Engine.WebStore.ViewModel
{
    public class PortalCustomizationSettingViewModel : BaseViewModel
    {
        public string WebApiClientKey { get; set; }
        public int WebsiteCode { get; set; }
        public string JsUrl { get; set; }
        public int UserId { get; set; }

        //This property is used only to idetify the guest and logged in user.
        public bool IsGuset { get; set; }
    }
}