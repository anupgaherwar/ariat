﻿using Znode.Artifi.Api.Model;

namespace Znode.Engine.WebStore.Agents
{
    public interface IArtifiCartAgent
    {

        /// <summary>
        /// Update customised cart item.
        /// </summary>
        /// <param name="artifiShoppingCartItemModel"></param>
        /// <returns>true/false</returns>
        bool UpdateArtifiCartItem(ArtifiShoppingCartItemModel artifiShoppingCartItemModel);       
    }
}
