﻿using Znode.Engine.WebStore.ViewModel;

namespace Znode.Engine.WebStore.Agents
{
    public interface IArtifiCheckoutAgent
    {
        /// <summary>
        /// To get artifi credentials.
        /// </summary>
        /// <param name="portalId"></param>
        /// <param name="userId"></param>
        /// <returns>PortalCustomizationSettingViewModel</returns>
        PortalCustomizationSettingViewModel GetArtifiCredentials(int portalId, int userId);

        /// <summary>
        /// Get UserId against order.
        /// </summary>
        /// <param name="omsOrderId">Id of order.</param>
        /// <returns></returns>
        int GetUserIdByOrderId(int omsOrderId);
    }
}
