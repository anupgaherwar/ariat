﻿using Znode.Engine.Api.Models;
using Znode.Engine.WebStore.ViewModels;

namespace Znode.Engine.WebStore.Agents
{
    public interface IArtifiProductAgent
    {
        ProductViewModel GetProductDetails(int id);
       
        /// <summary>
        /// Get Product For Artifi edit mode.
        /// </summary>
        /// <param name="id">Product Id</param>
        /// <param name="configurablesku">SKU</param>
        /// <returns>Product Model</returns>
        ProductViewModel GetProductDetailsForEditDesign(int id, string configurablesku);
    }
}