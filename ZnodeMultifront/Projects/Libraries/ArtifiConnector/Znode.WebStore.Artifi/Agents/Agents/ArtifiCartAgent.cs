﻿using Znode.Artifi.Api.Model;
using Znode.Engine.WebStore.Clients;

namespace Znode.Engine.WebStore.Agents
{
    public class ArtifiCartAgent : BaseAgent, IArtifiCartAgent
    {
        private readonly IArtifiCartClient _artifiCartClient;

        public ArtifiCartAgent(IArtifiCartClient cartClient)
        {
            _artifiCartClient = GetClient<IArtifiCartClient>(cartClient);
        }

        public bool UpdateArtifiCartItem(ArtifiShoppingCartItemModel artifiShoppingCartItemModel)
        {
            RemoveInSession(WebStoreConstants.CartModelSessionKey);
            RemoveInSession(WebStoreConstants.CartCount);
            return _artifiCartClient.UpdateSavedCartItemDesign(artifiShoppingCartItemModel);
        }

    }
}