﻿using ArtifiZnode.Engine.Webstore.Helper;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web;
using Znode.Artifi.Api.Model;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models;
using Znode.Engine.WebStore.Clients;
using Znode.Engine.WebStore.ViewModel;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;

namespace Znode.Engine.WebStore.Agents
{
    public class ArtifiCheckoutAgent : CheckoutAgent, IArtifiCheckoutAgent
    {
        #region Private Variables        
        private readonly IArtifiOrderClient _artifiOrderClient;
        private readonly string _orderStatus = "Placed";
        #endregion

        public ArtifiCheckoutAgent(IShippingClient shippingsClient, IPaymentClient paymentClient, IPortalProfileClient profileClient, ICustomerClient customerClient, IUserClient userClient, IOrderClient orderClient, IAccountClient accountClient, IWebStoreUserClient webStoreAccountClient, IPortalClient portalClient, IShoppingCartClient shoppingCartClient, IAddressClient addressClient, IArtifiOrderClient artifiOrderClient) : base(shippingsClient, paymentClient, profileClient, customerClient, userClient, orderClient, accountClient, webStoreAccountClient, portalClient, shoppingCartClient, addressClient)
        {
            _artifiOrderClient = GetClient(artifiOrderClient);
        }


        public PortalCustomizationSettingViewModel GetArtifiCredentials(int portalId, int userId)
        {
            PortalCustomizationSettingModel portalCustomizationSettingModel = new PortalCustomizationSettingModel();
            string cacheKey = HttpContext.Current.Request.Url.Authority + ArtifiZnodeWebstoreConstants.ArtifiCredentials;
            if (IsNull(HttpContext.Current.Cache[cacheKey]))
            {
                portalCustomizationSettingModel = _artifiOrderClient.GetArtifiCredentials(portalId);
                if (IsNotNull(portalCustomizationSettingModel))
                {
                    Helper.AddIntoCache(portalCustomizationSettingModel, cacheKey, "CurrentPortalCacheDuration");
                }
            }

            portalCustomizationSettingModel = Helper.GetFromCache<PortalCustomizationSettingModel>(cacheKey) ?? new PortalCustomizationSettingModel();

            UserViewModel userViewModel = GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey);
            PortalCustomizationSettingViewModel portalCustomizationSettingViewModel = new PortalCustomizationSettingViewModel() { WebApiClientKey = portalCustomizationSettingModel.WebApiClientKey, WebsiteCode = portalCustomizationSettingModel.WebsiteCode, JsUrl = portalCustomizationSettingModel.DomainName };
            if (userViewModel?.UserId > 0)
            {
                portalCustomizationSettingViewModel.UserId = userViewModel.UserId;
                portalCustomizationSettingViewModel.IsGuset = false;
            }
            else
            {
                PortalCustomizationSettingViewModel portalCustomizationViewModel = SessionHelper.GetDataFromSession<PortalCustomizationSettingViewModel>("artifiguestuser")?? new PortalCustomizationSettingViewModel();

                if (Equals(portalCustomizationViewModel.UserId, 0))
                {
                    portalCustomizationSettingViewModel.UserId = userId > 0 ? userId : new Random().Next();
                    portalCustomizationSettingViewModel.IsGuset = true;
                    SessionHelper.SaveDataInSession<PortalCustomizationSettingViewModel>("artifiguestuser", portalCustomizationSettingViewModel);
                }
                else
                {
                    portalCustomizationSettingViewModel = portalCustomizationViewModel;
                }
            }

            if (IsNotNull(portalCustomizationSettingViewModel))
            {
                return portalCustomizationSettingViewModel;
            }
            else
            {
                return new PortalCustomizationSettingViewModel();
            }
        }

        // Get UserId against order.        
        public int GetUserIdByOrderId(int omsOrderId)
           => _artifiOrderClient.GetUserIdByOrderId(omsOrderId);

        public override OrdersViewModel SubmitOrder(SubmitOrderViewModel submitOrderViewModel)
        {
            string orderStatus = _orderStatus;
            ShoppingCartModel cartModel = (ShoppingCartModel)this.GetFromSession<ShoppingCartModel>("ShoppingCartModel");

            OrdersViewModel orderDetails = base.SubmitOrder(submitOrderViewModel);
            int userId = GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey)?.UserId ?? GetFromSession<UserViewModel>(WebStoreConstants.GuestUserKey).UserId;
            PortalCustomizationSettingViewModel artifiCredentials = GetArtifiCredentials(PortalAgent.CurrentPortal.PortalId, userId);

            List<string> artifiDesignIds = new List<string>();
            string keys = "";
            foreach (var p in cartModel.ShoppingCartItems)
            {
                foreach (var x in p.PersonaliseValuesDetail)
                {
                    if (Equals(x.PersonalizeCode == "ArtifiDesignId"))
                    {
                        artifiDesignIds.Add(Convert.ToString(x.PersonalizeValue));
                    }
                }
            }

            keys = string.Join(",", artifiDesignIds);

            string url = HttpUtility.HtmlDecode(artifiCredentials.JsUrl + "//Designer/Services/UpdateMultipleCustomizedProductOrderStatus?");
            if (!url.ToLower().StartsWith("http:") && !url.ToLower().StartsWith("https:"))
            {
                url = string.Concat("http://", url);
            }

            url = string.Concat(url, "customizedProductIds=", keys,
                                     "&websiteid=", artifiCredentials.WebsiteCode,
                                     "&webApiClientKey=", artifiCredentials.WebApiClientKey,
                                     "&orderStatus=", orderStatus);



            try
            {
                WebRequest request = WebRequest.Create(url);
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    string desc = response.StatusDescription;
                }
            }
            catch (Exception ex)
            {
                LogMessage(ex.ToString());
            }
            return orderDetails;
        }

        
        public override string GenerateOrderNumber(int portalId)
        {
            string orderNumber = _artifiOrderClient.GenerateOrderNumber(portalId);
            return orderNumber;
        }
    }
}