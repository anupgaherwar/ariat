﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Znode.Engine.Api.Client;
using Znode.Engine.WebStore.Clients;
using Znode.Engine.WebStore.ViewModel;
using Znode.Engine.WebStore.ViewModels;

namespace Znode.Engine.WebStore.Agents
{
    public class ArtifiUserAgent : UserAgent
    {
        #region Private Variables        
        private readonly IArtifiOrderClient _artifiOrderClient;
        #endregion

        public ArtifiUserAgent(ICountryClient countryClient, IWebStoreUserClient webStoreAccountClient, IWishListClient wishListClient, IUserClient userClient, IPublishProductClient productClient, ICustomerReviewClient customerReviewClient, IOrderClient orderClient, IGiftCardClient giftCardClient, IAccountClient accountClient, IAccountQuoteClient accountQuoteClient, IOrderStateClient orderStateClient, IPortalCountryClient portalCountryClient, IShippingClient shippingClient, IPaymentClient paymentClient, ICustomerClient customerClient, IStateClient stateClient, IPortalProfileClient portalProfileClient, IArtifiOrderClient artifiOrderClient) : 
            base(countryClient, webStoreAccountClient, wishListClient, userClient, productClient, customerReviewClient, orderClient, giftCardClient, accountClient, accountQuoteClient, orderStateClient, portalCountryClient, shippingClient, paymentClient, customerClient, stateClient, portalProfileClient)
        {
            _artifiOrderClient = GetClient(artifiOrderClient);
        }

        public override List<CartItemViewModel> GetReorderItems(int orderId)
        {
            List<CartItemViewModel> list = base.GetReorderItems(orderId);

            PortalCustomizationSettingViewModel artifiCredentials = GetArtifiCredentials();

            foreach (CartItemViewModel item in list ?? new List<CartItemViewModel>())
            {
                GetReOrderArtifiDesign(item, artifiCredentials);
            }
            return list;
        }

        public override CartItemViewModel GetOrderByOrderLineItemId(int orderLineItemId)
        {
            CartItemViewModel cartItemViewModel = base.GetOrderByOrderLineItemId(orderLineItemId);

            GetReOrderArtifiDesign(cartItemViewModel, GetArtifiCredentials());

            return cartItemViewModel;

        }

        public virtual PortalCustomizationSettingViewModel GetArtifiCredentials()
        {
            ArtifiCheckoutAgent checkout = new ArtifiCheckoutAgent(GetClient<IShippingClient>(), GetClient<IPaymentClient>(), GetClient<IPortalProfileClient>(), GetClient<ICustomerClient>(), GetClient<IUserClient>(), GetClient<IOrderClient>(), GetClient<IAccountClient>(), GetClient<IWebStoreUserClient>(), GetClient<IPortalClient>(), GetClient<IShoppingCartClient>(), GetClient<IAddressClient>(), _artifiOrderClient);
            return checkout.GetArtifiCredentials(PortalAgent.CurrentPortal.PortalId, GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey).UserId);
        }

        public virtual void GetReOrderArtifiDesign(CartItemViewModel cartItem, PortalCustomizationSettingViewModel portalCustomizationSettingViewModel)
        {
            if (cartItem?.PersonaliseValuesList?.Count > 0)
            {
                if (Convert.ToBoolean(cartItem?.PersonaliseValuesList?.ContainsKey("ArtifiDesignId")))
                {
                    string designId = Convert.ToString(cartItem.PersonaliseValuesList.Where(w => w.Key == "ArtifiDesignId").Select(s => s.Value)?.FirstOrDefault());

                    string url = HttpUtility.HtmlDecode(portalCustomizationSettingViewModel.JsUrl + "/Designer/Services/GetReorder?");

                    if (!url.ToLower().StartsWith("http:") && !url.ToLower().StartsWith("https:"))
                        url = string.Concat("http://", url);

                    url = string.Concat(url, "designId=", designId,
                                             "&userid=", portalCustomizationSettingViewModel.UserId,
                                             "&websiteid=", portalCustomizationSettingViewModel.WebsiteCode,
                                             "&webApiClientKey=", portalCustomizationSettingViewModel.WebApiClientKey);

                    WebRequest request = HttpWebRequest.Create(url);

                    try
                    {
                        string html = string.Empty;
                        using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                        using (Stream stream = response.GetResponseStream())
                        using (StreamReader reader = new StreamReader(stream))
                        {
                            html = reader.ReadToEnd();

                            dynamic obj = JsonConvert.DeserializeObject(html);
                            cartItem.PersonaliseValuesList = new Dictionary<string, object>();

                            cartItem.PersonaliseValuesList.Add("ArtifiDesignId", (string)obj["Data"]["Id"]);
                            cartItem.PersonaliseValuesList.Add("ArtifiImagePath", (string)obj["Data"]["CustomizedDesignList"][0]["ImagePath"]);
                        }
                    }
                    catch
                    {

                    }
                }
            }
        }
    }
}
