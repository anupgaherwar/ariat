﻿using Znode.Engine.WebStore.ViewModels;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Resources;

namespace Znode.Engine.WebStore.Agents
{
    public class ArtifiProductAgent : BaseAgent, IArtifiProductAgent
    {
        private readonly IPublishProductClient _productClient;

        public ArtifiProductAgent(IPublishProductClient productClient)
        {
            _productClient = GetClient(productClient);
        }

        public ProductViewModel GetProductDetails(int id)
        {
            PublishProductModel model = _productClient.GetPublishProduct(id, GetRequiredFilters(), GetProductExpands());
            if (HelperUtility.IsNotNull(model))
            {
                ProductViewModel viewModel = model.ToViewModel<ProductViewModel>();
                return viewModel;
            }
            throw new ZnodeException(ErrorCodes.NotFound, WebStore_Resources.ErrorProductNotFound);
        }

        //Get publish product by sku
        public ProductViewModel GetProductDetailsForEditDesign(int id, string configurablesku)
        {
            ParameterProductModel parameterProductModel = new ParameterProductModel { SKU = configurablesku, PublishCatalogId = PortalAgent.CurrentPortal.PublishCatalogId, LocaleId = PortalAgent.CurrentPortal.LocaleId };
            PublishProductModel model = _productClient.GetPublishProductBySKU(parameterProductModel,  GetProductExpands(), GetRequiredFilters());
            if (HelperUtility.IsNotNull(model))
            {
                ProductViewModel viewModel = model.ToViewModel<ProductViewModel>();
                return viewModel;
            }
            throw new ZnodeException(ErrorCodes.NotFound, WebStore_Resources.ErrorProductNotFound);
        }

        public ExpandCollection GetProductExpands(bool isProductDetails = false)
        {
            ExpandCollection expands = new ExpandCollection
            {
                ExpandKeys.Inventory,
                ExpandKeys.Pricing,
                ExpandKeys.ProductTemplate,
                ExpandKeys.AddOns
            };

            if (isProductDetails)
                expands.Add(ExpandKeys.ConfigurableAttribute);
            return expands;
        }
    }
}