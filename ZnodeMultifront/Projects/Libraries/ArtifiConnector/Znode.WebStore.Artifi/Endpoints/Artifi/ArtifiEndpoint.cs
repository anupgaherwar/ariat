﻿using Znode.Engine.Api.Client.Endpoints;

namespace Znode.Engine.WebStore.Endpoints
{
    public class ArtifiEndpoint : BaseEndpoint
    {
        //Get artifi credentials        
        public static string GetPortalCustomizationArtifiSetting(int portalId) => $"{ApiRoot}/artifiportal/getportalcustomizationsetting/{portalId}";

        public static string UpdateSavedCartItemDesign() => $"{ApiRoot}/artifiorder/updatesavedcartitemdesign";

        //Get userId by orderId 
        public static string GetUserIdByOrderId(int omsOrderId) => $"{ApiRoot}/artifiorder/getuseridbyorderid/{omsOrderId}";

        public static string GenerateOrderNumber(int portalId) => $"{ApiRoot}/artifiorder/generateordernumber/{portalId}";

    }
}