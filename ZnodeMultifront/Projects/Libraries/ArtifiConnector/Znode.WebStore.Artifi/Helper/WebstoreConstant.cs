﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ArtifiZnode.Engine.Webstore.Helper
{
    public struct ArtifiZnodeWebstoreConstants
    {
        public const string ArtifiCredentials = "artificredentials";
        public const string ShoppingCartDesignDetails = "ShoppingCartDesignDetails";

    }
}