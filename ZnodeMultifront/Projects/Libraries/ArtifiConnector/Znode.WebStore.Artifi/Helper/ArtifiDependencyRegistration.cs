﻿using Autofac;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.Client;
using Znode.Engine.WebStore.Clients;
using Znode.Engine.WebStore.Controllers;
using Znode.Libraries.Framework.Business;

namespace Znode.Engine.WebStore
{
    public class ArtifiDependencyRegistration : IDependencyRegistration
    {
        /// <summary>
        /// Register the Dependency Injection types.
        /// </summary>
        /// <param name="builder">Autofac Container Builder</param>
        public virtual void Register(ContainerBuilder builder)
        {

            //Here override znode base code method by injecting dependancy metion as below.          

            builder.RegisterType<ArtifiCheckoutAgent>().As<IArtifiCheckoutAgent>().InstancePerLifetimeScope();
            builder.RegisterType<ArtifiCheckoutAgent>().As<ICheckoutAgent>().InstancePerLifetimeScope();
            builder.RegisterType<ArtifiCheckoutAgent>().As<CheckoutAgent>().InstancePerLifetimeScope();
            builder.RegisterType<ArtifiCartAgent>().As<IArtifiCartAgent>().InstancePerLifetimeScope();
            builder.RegisterType<ArtifiUserAgent>().As<IUserAgent>().InstancePerLifetimeScope();
            builder.RegisterType<ArtifiUserAgent>().As<UserAgent>().InstancePerLifetimeScope();

            builder.RegisterType<ArtifiOrderClient>().As<IArtifiOrderClient>().InstancePerLifetimeScope();
            builder.RegisterType<ArtifiCartClient>().As<IArtifiCartClient>().InstancePerLifetimeScope();
            builder.RegisterType<ArtifiProductAgent>().As<IArtifiProductAgent>().InstancePerLifetimeScope();
            builder.RegisterType<ArtifiCheckoutController>().As<CheckoutController>().InstancePerDependency();

        }


        /// <summary>
        /// Order method represents Dependency Injection Registration Order.
        /// For znode base code Library the DI registration order set to 0.
        /// For custom code library the DI registration order should be incremental.
        /// </summary>
        public int Order
        {
            get { return 1; }
        }
    }
}