﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Znode.Engine.WebStore
{
    public class ArtifiAuthenticationHelper : AuthorizeAttribute
    {
        private string textReturnUrl = "returnUrl";
        private string defaultControllerName = "User";
        private string defaultActionName = "Login";

        //Overloaded method for Authorize attribute, user to authenticate & authorize the user for each action.
        public override void OnAuthorization(AuthorizationContext filterContext) => AuthenticateUser(filterContext);

        //Method Used to Authenticate the user.
        public void AuthenticateUser(AuthorizationContext filterContext)
        {
            var isAuthorized = base.AuthorizeCore(filterContext.HttpContext);

            //skipAuthorization get sets to true when the action has the [AllowAnonymous] attributes, If true then skip authentication.
            bool skipAuthorization = filterContext.ActionDescriptor.IsDefined(typeof(AllowAnonymousAttribute), inherit: true)
                            || filterContext.ActionDescriptor.ControllerDescriptor.IsDefined(typeof(AllowAnonymousAttribute), inherit: true);
            if (!skipAuthorization)
            {
                if (!isAuthorized && !filterContext.HttpContext.Request.IsAuthenticated && (string.IsNullOrEmpty(filterContext.HttpContext.User.Identity.Name)))
                    HandleUnauthorizedRequest(filterContext);
                else
                {
                    if (!SessionProxyHelper.IsLoginUser())
                        HandleUnauthorizedRequest(filterContext);
                }
            }
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            string returnUrl = (filterContext.RequestContext.HttpContext.Request.IsAjaxRequest())
                 ? (!Equals(filterContext.RequestContext.HttpContext.Request.UrlReferrer, null))
                     ? filterContext.RequestContext.HttpContext.Request.UrlReferrer.PathAndQuery
                     : string.Empty
                 : (Equals(filterContext.RequestContext.HttpContext.Request.HttpMethod, HttpMethod.Post.ToString()))
                 ? (!Equals(filterContext.RequestContext.HttpContext.Request.UrlReferrer, null))
                     ? filterContext.RequestContext.HttpContext.Request.UrlReferrer.PathAndQuery
                     : string.Empty
                 : filterContext.RequestContext.HttpContext.Request.RawUrl;

            returnUrl = returnUrl.Contains(textReturnUrl) ? filterContext.RequestContext.HttpContext.Request.RawUrl : returnUrl;

            if (filterContext.RequestContext.HttpContext.Request.IsAjaxRequest())
            {
                filterContext.RequestContext.HttpContext.Response.StatusCode = (int)HttpStatusCode.Forbidden;
                string routeName = (Equals(filterContext.RequestContext.HttpContext.Request.RequestContext.RouteData.DataTokens["area"], null)) ? string.Empty : Convert.ToString(filterContext.RequestContext.HttpContext.Request.RequestContext.RouteData.DataTokens["area"]);
                routeName = (string.IsNullOrEmpty(routeName)) ? GetAreaNameFromUrlReferrer(filterContext) : routeName;
                filterContext.RequestContext.HttpContext.Response.StatusDescription = HttpUtility.UrlEncode(returnUrl);
                filterContext.Result = new JsonResult
                {
                    Data = new
                    {
                        ErrorCode = "101",
                        ReturnUrl = returnUrl,
                        Area = routeName,
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                };
                filterContext.RequestContext.HttpContext.Response.End();
            }
            else
            {
                filterContext.RequestContext.RouteData.DataTokens["area"] = string.Empty;
                filterContext.Result = new RedirectToRouteResult(
                          new RouteValueDictionary {
                        { "area", string.Empty },
                        { "controller", defaultControllerName },
                        { "action", defaultActionName },
                        { textReturnUrl, returnUrl}
                          });
            }
        }

        // Get Area name from the current request UrlReferrer
        private string GetAreaNameFromUrlReferrer(AuthorizationContext filterContext)
        {
            string areaName = string.Empty;
            var fullUrl = filterContext.RequestContext.HttpContext.Request.UrlReferrer.ToString();
            var questionMarkIndex = fullUrl.IndexOf('?');
            string queryString = null;
            string url = fullUrl;
            if (!Equals(questionMarkIndex, -1)) // There is a QueryString
            {
                url = fullUrl.Substring(0, questionMarkIndex);
                queryString = fullUrl.Substring(questionMarkIndex + 1);
            }
            // Arranges
            var request = new HttpRequest(null, url, queryString);
            var response = new HttpResponse(new StringWriter());
            var httpContext = new HttpContext(request, response);
            var routeData = RouteTable.Routes.GetRouteData(new HttpContextWrapper(httpContext));
            return areaName = (Equals(routeData.DataTokens["area"], null)) ? string.Empty : Convert.ToString(routeData.DataTokens["area"]);
        }

    }
}