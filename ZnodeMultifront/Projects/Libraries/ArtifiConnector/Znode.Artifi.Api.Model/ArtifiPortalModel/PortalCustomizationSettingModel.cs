﻿using Znode.Engine.Api.Models;

namespace Znode.Artifi.Api.Model
{
    public class PortalCustomizationSettingModel : BaseModel
    {
        public int AIPortalSettingId { get; set; }
        public int PortalId { get; set; }

        public string PortalName { get; set; }
        public string WebApiClientKey { get; set; }
        public int WebsiteCode { get; set; }
        public string DomainName { get; set; }

    }
}
