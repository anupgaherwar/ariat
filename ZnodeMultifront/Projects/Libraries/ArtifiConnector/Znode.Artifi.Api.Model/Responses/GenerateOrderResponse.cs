﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Artifi.Api.Model.Responses
{
    public class GenerateOrderResponse: BaseResponse
    {
        public string OrderNumber { get; set; }
    }
}
