﻿using Znode.Engine.Api.Models.Responses;

namespace Znode.Artifi.Api.Model.Responses
{
    public class ArtifiDesignDetailsListResponse : BaseResponse
    {

        public ArtifiDesignDetailsListModel DesignDetailsModel { get; set; }
    }
}
