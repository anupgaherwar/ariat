﻿using Znode.Engine.Api.Models.Responses;

namespace Znode.Artifi.Api.Model.Responses
{
    public class ArtifiOrderResponse : BaseResponse
    {
        public ArtifiOrderModel Order { get; set; }
    }
}
