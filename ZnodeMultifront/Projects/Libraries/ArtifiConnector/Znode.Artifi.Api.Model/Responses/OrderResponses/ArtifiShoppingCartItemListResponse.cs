﻿using System.Collections.Generic;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Artifi.Api.Model.Responses
{
    public class ArtifiShoppingCartItemListResponse : BaseResponse
    {
        public List<ArtifiShoppingCartItemModel> ArtifiShoppingCartItemList { get; set; }
    }
}
