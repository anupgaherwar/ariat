﻿using Znode.Artifi.Api.Model;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Artifi.Api.Model.Responses
{
    public class ArtifiShoppingCartItemResponse : BaseResponse
    {
        public ArtifiShoppingCartItemModel ArtifiShoppingCartItem { get; set; }
    }
}
