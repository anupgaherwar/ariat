﻿using Znode.Engine.Api.Models.Responses;

namespace Znode.Artifi.Api.Model.Responses
{
    public class ArtifiCartResponse : BaseResponse
    {
        public ArtifiShoppingCartModel ShoppingCart { get; set; }
    }
}
