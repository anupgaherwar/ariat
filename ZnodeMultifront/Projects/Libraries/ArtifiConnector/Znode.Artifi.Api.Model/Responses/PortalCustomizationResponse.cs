﻿using Znode.Engine.Api.Models.Responses;

namespace Znode.Artifi.Api.Model.Responses
{
    public class PortalCustomizationResponse : BaseResponse
    {
        public PortalCustomizationSettingModel CustomizationSettingModel { get; set; }
    }
}
