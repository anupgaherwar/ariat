﻿using Znode.Engine.Api.Models;

namespace Znode.Artifi.Api.Model
{
    public class ArtifiDesignDetailsModel : BaseModel
    {
        public string AISavedDesigns { get; set; }
        public int OrderLineItemsDesignId { get; set; }
        public string SKU { get; set; }
        public bool IsConfigurableProduct { get; set; } = false;
        public bool IsGroupProduct { get; set; } = false;
        public int SequenceNumber { get; set; } = 1;
        public bool IsCustomizableProduct { get; set; } = true;
        public int OmsSavedCartLineItemId { get; set; }

    }
}
