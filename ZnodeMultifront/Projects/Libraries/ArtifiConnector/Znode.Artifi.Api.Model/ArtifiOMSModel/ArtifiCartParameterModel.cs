﻿using Znode.Engine.Api.Models;

namespace Znode.Artifi.Api.Model
{
    public class ArtifiCartParameterModel : BaseModel
    {
        public string CookieMappingId { get; set; }
        public int UserId { get; set; }
        public string SKU { get; set; }
        public int? OmsSavedcartLineItemId { get; set; }
    }
}
