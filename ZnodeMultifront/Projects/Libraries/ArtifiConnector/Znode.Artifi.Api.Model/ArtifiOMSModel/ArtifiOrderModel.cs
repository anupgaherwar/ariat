﻿using System.Collections.Generic;
using Znode.Engine.Api.Models;

namespace Znode.Artifi.Api.Model
{
    public class ArtifiOrderModel : BaseModel
    {
        public OrderModel OrderModel { get; set; }

        public List<ArtifiShoppingCartItemModel> ArtifiShoppingCartItemListModel { get; set; }

        public int WebsiteId { get; set; }
        public string WebApiClientKey { get; set; }
    }
}
