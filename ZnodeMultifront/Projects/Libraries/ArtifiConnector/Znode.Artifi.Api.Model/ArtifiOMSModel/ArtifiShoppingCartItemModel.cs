﻿using System.Collections.Generic;
using Znode.Engine.Api.Models;

namespace Znode.Artifi.Api.Model
{
    public class ArtifiShoppingCartItemModel : BaseModel
    {
        public int OrderLineItemsDesignId { get; set; }
        public string AIImagePath { get; set; }
        public bool IsConfigurableProduct { get; set; }
        public bool IsGroupProduct { get; set; }
        public int OrderLineItemId { get; set; }
        public int OmsSavedCartLineItemId { get; set; }
        public int OmsOrderId { get; set; }
        public decimal Quantity { get; set; }
        public string SKU { get; set; }
        public Dictionary<string, string> Customization { get; set; }
    }
}
