﻿using System.Collections.Generic;
using Znode.Engine.Api.Models;

namespace Znode.Artifi.Api.Model
{
    public class ArtifiShoppingCartItemListModel : BaseModel
    {
        public ArtifiShoppingCartItemListModel()
        {
            ArtifiShoppingCartItemList = new List<ArtifiShoppingCartItemModel>();

        }
        public List<ArtifiShoppingCartItemModel> ArtifiShoppingCartItemList { get; set; }

    }
}
