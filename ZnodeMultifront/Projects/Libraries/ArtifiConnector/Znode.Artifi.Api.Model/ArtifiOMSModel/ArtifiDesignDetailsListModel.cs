﻿using System.Collections.Generic;
using Znode.Engine.Api.Models;

namespace Znode.Artifi.Api.Model
{
    public class ArtifiDesignDetailsListModel : BaseListModel
    {
        public List<ArtifiDesignDetailsModel> DesignDetailsListModel { get; set; }

        public ArtifiDesignDetailsListModel()
        {
            DesignDetailsListModel = new List<ArtifiDesignDetailsModel>();
        }
    }
}
