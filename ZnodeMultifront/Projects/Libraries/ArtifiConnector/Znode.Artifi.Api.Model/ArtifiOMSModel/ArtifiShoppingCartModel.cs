﻿using System.Collections.Generic;
using Znode.Engine.Api.Models;

namespace Znode.Artifi.Api.Model
{
    public class ArtifiShoppingCartModel : BaseModel
    {
        public ShoppingCartModel ShoppingCartModel { get; set; }
        public List<ArtifiDesignDetailsModel> DetailsModel { get; set; }
    }
}
