﻿using Autofac;

using Znode.Admin.Custom.Agents;
using Znode.Admin.Custom.Agents.Agents;
using Znode.Api.Client.Custom.Clients;
using Znode.Api.Client.Custom.Clients.Clients;
using Znode.Engine.Admin.Agents;
using Znode.Libraries.Framework.Business;

namespace Znode.Admin.Custom.Helper
{
    public class DependencyRegistration : IDependencyRegistration
    {
        /// <summary>
        /// Register the Dependency Injection types.
        /// </summary>
        /// <param name="builder">Autofac Container Builder</param>
        public virtual void Register(ContainerBuilder builder)
        {
            builder.RegisterType<CustomPortalAgent>().As<ICustomPortalAgent>().InstancePerLifetimeScope();

            //Here override znode base code method by injecting dependancy metion as below.
            //"In CustomUserAgent.cs we have override 'LogOut()' of znode base code" .
            builder.RegisterType<CustomUserAgent>().As<IUserAgent>().InstancePerLifetimeScope();
            builder.RegisterType<AriatShippingAgent>().As<IAriatShippingAgent>().InstancePerLifetimeScope();
            builder.RegisterType<AriatAccountAgent>().As<IAriatAccountAgent>().InstancePerLifetimeScope();
            /*Adyen Start:Add adyen payment*/
            builder.RegisterType<AriatPaymentAgent>().As<IPaymentAgent>().InstancePerLifetimeScope();
            /*Adyen End:Add adyen payment*/

            builder.RegisterType<AriatShippingClient>().As<IAriatShippingClient>().InstancePerLifetimeScope();
            builder.RegisterType<AriatGiftCardAgent>().As<IGiftCardAgent>().InstancePerLifetimeScope();
            builder.RegisterType<AriatDevExpressAgent>().As<IDevExpressReportAgent>().InstancePerLifetimeScope();
            builder.RegisterType<AriatCustomerAgent>().As<ICustomerAgent>().InstancePerLifetimeScope();
           
            builder.RegisterType<AriatAccountClient>().As<IAriatAccountClient>().InstancePerLifetimeScope();
            builder.RegisterType<AriatDevExpressClient>().As<IAriatDevExpressClient>().InstancePerLifetimeScope();
            builder.RegisterType<AriatImportAgent>().As<IImportAgent>().InstancePerLifetimeScope();
        }

        /// <summary>
        /// Order method represents Dependency Injection Registration Order.
        /// For znode base code Library the DI registration order set to 0.
        /// For custom code library the DI registration order should be incremental.
        /// </summary>
        public int Order
        {
            get { return 1; }
        }
    }
}
