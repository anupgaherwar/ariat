﻿
using Newtonsoft.Json;
using System;
using System.Web.Mvc;
using Znode.Admin.Custom.Agents;
using Znode.Admin.Custom.ViewModel;
using Znode.Engine.Admin.Controllers;
using Znode.Engine.Admin.Helpers;
using Znode.Engine.Admin.ViewModels;
using Znode.Libraries.Resources;

namespace Znode.Admin.Custom.Controllers
{
    public class AriatShippingController : BaseController
    {
        #region Private Variable       
        private readonly IAriatShippingAgent _ariatShippingAgent;
        #endregion

        #region Constructor
        public AriatShippingController(IAriatShippingAgent Agent)
        {
            _ariatShippingAgent = Agent;
        }
        #endregion

        #region Public Method

        //Get Shipping rate list.
        public ActionResult ShippingRateList([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model, int shippingId, int shippingTypeId)
        {
            //Get and Set Filters from Cookies if exists.
            FilterHelpers.GetSetFiltersFromCookies("AriatShippinRateList", model);

            //Assign default view filter and sorting if exists for the first request.
            FilterHelpers.GetDefaultView("AriatShippinRateList", model);

            AriatShippingListViewModel rateList = _ariatShippingAgent.ShippingRateList(model.Filters, model.SortCollection, model.Page, model.RecordPerPage, shippingId);

            rateList.ShippingTypeId = shippingTypeId;
            //Get the grid model.
            rateList.GridModel = FilterHelpers.GetDynamicGridModel(model, rateList.ShippingRateList, "AriatShippinRateList", string.Empty, null, true, true, rateList?.GridModel?.FilterColumn?.ToolMenuList);

            //Set the total record count
            rateList.GridModel.TotalRecordCount = rateList.TotalResults;

            return ActionView("~/Views/CustomViews/AriatShipping/ShippingRateList.cshtml", rateList);
        }

        //Get Create shipping rate view
        [HttpGet]
        public ActionResult CreateShippinRate(int shippingId, string shippingCode)
          => ActionView("~/Views/CustomViews/AriatShipping/CreateEditShippingRate.cshtml", _ariatShippingAgent.CreateShippinRate(shippingId, shippingCode));

        //Create the shipping rate details.
        [HttpPost]
        public ActionResult CreateShippinRate(AriatShippingViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                AriatShippingViewModel result = _ariatShippingAgent.CreateShippingRate(viewModel);

                SetNotificationMessage(result.AriatCustomShippingChargesId > 0 && !result.HasError
                    ? GetSuccessNotificationMessage(Admin_Resources.RecordCreationSuccessMessage)
                     : GetErrorNotificationMessage(string.IsNullOrEmpty(result.ErrorMessage) ? Admin_Resources.ErrorFailedToCreate : result.ErrorMessage));


                if (result.AriatCustomShippingChargesId > 0)
                    return RedirectToAction<AriatShippingController>(x => x.EditShippingRate(result.AriatCustomShippingChargesId, viewModel.ShippingId));
                else
                    return ActionView("~/Views/CustomViews/AriatShipping/CreateEditShippingRate.cshtml", viewModel);
            }
            SetNotificationMessage(GetErrorNotificationMessage(Admin_Resources.ErrorFailedToCreate));
            return ActionView("~/Views/CustomViews/AriatShipping/CreateEditShippingRate.cshtml", viewModel);
        }


        //Edit Shipping rate Details view.
        public virtual ActionResult EditShippingRate(int ariatCustomShippingChargesId, int shippingId)
        {
            ActionResult action = GotoBackURL();
            if (action != null)
                return action;

            return ActionView("~/Views/CustomViews/AriatShipping/CreateEditShippingRate.cshtml", _ariatShippingAgent.GetShippingRateDetails(ariatCustomShippingChargesId, shippingId));
        }

        [HttpPost]
        public virtual ActionResult EditShippingRate(AriatShippingViewModel viewmodel)
        {
            if (ModelState.IsValid)
            {
                bool result = _ariatShippingAgent.EditShippingRate(viewmodel);
                SetNotificationMessage(!(viewmodel.HasError)
                ? GetSuccessNotificationMessage(Admin_Resources.UpdateMessage)
                 : GetErrorNotificationMessage(Admin_Resources.UpdateError));
            }
            else
                SetNotificationMessage(GetErrorNotificationMessage(Admin_Resources.UpdateError));
            return RedirectToAction<AriatShippingController>(x => x.EditShippingRate(viewmodel.AriatCustomShippingChargesId, viewmodel.ShippingId));
        }

        //Edit Shipping rate Details view.
        public virtual JsonResult InLineEditShippingRate(int ariatCustomShippingChargesId, string data)
        {
            try
            {
                AriatShippingViewModel catalogAssociationViewModel = JsonConvert.DeserializeObject<AriatShippingViewModel[]>(data)[0];

                bool status = false;
                string message = string.Empty;
                if (ModelState.IsValid)
                    status = _ariatShippingAgent.EditShippingRate(catalogAssociationViewModel);


                message = status ? PIM_Resources.UpdateMessage : PIM_Resources.UpdateErrorMessage;

                return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
            }
            catch (JsonReaderException ex)
            {
                return Json(new { status = false, message = "The value must be a number." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = false, message = PIM_Resources.UpdateErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }

        //Delete shipping rate
        public virtual JsonResult DeleteShippingRate(string ariatCustomShippingChargesId)
        {
            if (!string.IsNullOrEmpty(ariatCustomShippingChargesId))
            {
                bool status = _ariatShippingAgent.DeleteShippingRate(ariatCustomShippingChargesId);
                return Json(new { status = status, message = status ? Admin_Resources.DeleteMessage : Admin_Resources.DeleteErrorMessage }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { status = false, message = Admin_Resources.DeleteErrorMessage }, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}
