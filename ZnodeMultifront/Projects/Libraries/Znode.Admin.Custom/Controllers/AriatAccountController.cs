﻿using System;
using System.Collections.Generic;

using System.Web.Mvc;

using Znode.Admin.Custom.Agents;
using Znode.Admin.Custom.ViewModel;
using Znode.Engine.Admin.Controllers;
using Znode.Engine.Admin.Helpers;
using Znode.Engine.Admin.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Resources;

namespace Znode.Admin.Custom.Controllers
{
    public class AriatAccountController : BaseController
    {
        #region Private Variable       
        private readonly IAriatAccountAgent _ariatAccountAgent;
        #endregion

        #region Public controller

        public AriatAccountController(IAriatAccountAgent ariatAccountAgent)
        {
            _ariatAccountAgent = ariatAccountAgent;
        }
        #endregion

        #region Public methods
        //Get program catalog product list.
        public ActionResult ProgramCatalogProductList([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model, int accountId, string accountExternalId = "")
        {
            //Get and Set Filters from Cookies if exists.
            FilterHelpers.GetSetFiltersFromCookies("AriatProgramCatalogProductList", model);

            //Assign default view filter and sorting if exists for the first request.
            FilterHelpers.GetDefaultView("AriatProgramCatalogProductList", model);

            AriatProgramCatalogProductListViewModel productListViewModel = _ariatAccountAgent.ProgramCatalogProductList(model.Filters, model.SortCollection, model.Page, model.RecordPerPage, accountId, accountExternalId);
            productListViewModel.AccountId = accountId;
            //Get the grid model.
            productListViewModel.GridModel = FilterHelpers.GetDynamicGridModel(model, productListViewModel.ProductList, "AriatProgramCatalogProductList", string.Empty, null, true, true, productListViewModel?.GridModel?.FilterColumn?.ToolMenuList);

            //Set the total record count
            productListViewModel.GridModel.TotalRecordCount = productListViewModel.TotalResults;
            productListViewModel.AccountId = accountId;

            return ActionView("~/Views/CustomViews/AriatAccount/ProgramCatalogProductList.cshtml", productListViewModel);
        }


        //Export the catalog product into csv
        public virtual JsonResult ExportFullCatalog(string accountId)
        {
            
            int accountsId = 0;
            if (!string.IsNullOrEmpty(accountId))
            {
                accountsId = Convert.ToInt32(accountId);
            }
            List<dynamic> productList = _ariatAccountAgent.GetFullCatalogDetails(null, null, null, null, accountsId);
            string exportProductContent = string.Empty;
            if (HelperUtility.IsNotNull(productList) && productList.Count > 0)
            {
                //1-Excel 2-CSV
                exportProductContent = GetProductExportData("2", productList, "FullCatalog");
            }
            JsonResult jsonResult = Json(new { content = exportProductContent, fileName = Equals("2", "1") ? $"{"FullCatalog"}.xls" : $"{"FullCatalog"}.csv", status = Equals(string.IsNullOrEmpty(exportProductContent), false), message = Admin_Resources.NoRecordFoundText }, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        // Export the program catalog product into csv
        public virtual JsonResult ExportProgramCatalog(string accountId)
        {

            int accountsId = 0;
            if (!string.IsNullOrEmpty(accountId))
            {
                accountsId = Convert.ToInt32(accountId);
            }
            List<dynamic> productList = _ariatAccountAgent.GetProgramCatalogDetails(null, null, null, null, accountsId);
            string exportProductContent = string.Empty;
            if (HelperUtility.IsNotNull(productList) && productList.Count > 0)
            {
                //1-Excel 2-CSV
                exportProductContent = GetProductExportData("2", productList, "ProgramCatalog");
            }
            JsonResult jsonResult = Json(new { content = exportProductContent, fileName = Equals("2", "1") ? $"{"ProgramCatalog"}.xls" : $"{"ProgramCatalog"}.csv", status = Equals(string.IsNullOrEmpty(exportProductContent), false), message = Admin_Resources.NoRecordFoundText }, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        //Delete the product according to ariatProgramCatalogProductId.
        public virtual JsonResult Delete(string ariatProgramCatalogProductId)
        {
            string message = Admin_Resources.DeleteErrorMessage;
            bool status = false;
            if (!string.IsNullOrEmpty(ariatProgramCatalogProductId))
            {
                status = _ariatAccountAgent.DeleteProducts(ariatProgramCatalogProductId, out message);
                message = status ? Admin_Resources.DeleteMessage : message;
            }
            return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
        }

        //Publish program catalog.
        public virtual JsonResult Publish(string accountExternalId)
        {
            if (!string.IsNullOrEmpty(accountExternalId))
            {
                string errorMessage;
                bool status = _ariatAccountAgent.Publish(accountExternalId, out errorMessage);
                return Json(new { status = status, message = status ? PIM_Resources.TextPublishStarted : errorMessage }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { status = false, message = PIM_Resources.ErrorPublished }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Protected Methods
        protected string GetProductExportData(string exportFileTypeId, List<dynamic> exportList, string catalogType = "")
        {
            string exportProductData = new DownloadHelper().ExportDownload(exportFileTypeId, _ariatAccountAgent.CreateDataSource(exportList, catalogType), null, null, null, false);
            return exportProductData;

        } 
        #endregion
    }
}
