﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Admin.ViewModels;
using Znode.Libraries.Resources;

namespace Znode.Admin.Custom.ViewModel
{
    public class AriatShippingViewModel : BaseViewModel
    {
        public int AriatCustomShippingChargesId { get; set; }

        [Display(Name = ZnodeAdmin_Resources.LabelShippingCode, ResourceType = typeof(Admin_Resources))]
        public string ShippingCode { get; set; }

        [Display(Name = ZnodeAdmin_Resources.LabelLowerLimit, ResourceType = typeof(Admin_Resources))]
        [Required(ErrorMessageResourceType = typeof(Admin_Resources), ErrorMessageResourceName = ZnodeAdmin_Resources.ErrorMessageLowerLimitIsRequired)]
        [Range(1, 9999, ErrorMessageResourceType = typeof(Admin_Resources), ErrorMessageResourceName = ZnodeAdmin_Resources.ErrorLowerLimit)]
        public int? LowerLimit { get; set; }

        [Display(Name = ZnodeAdmin_Resources.LabelUpperLimit, ResourceType = typeof(Admin_Resources))]
        [Required(ErrorMessageResourceType = typeof(Admin_Resources), ErrorMessageResourceName = ZnodeAdmin_Resources.ErrorMessageUpperLimitRequired)]
        [Range(1, 9999, ErrorMessageResourceType = typeof(Admin_Resources), ErrorMessageResourceName = ZnodeAdmin_Resources.ErrorUpperLimit)]
        public int? UpperLimit { get; set; }

        [Display(Name = ZnodeAdmin_Resources.LabelShippingAmount, ResourceType = typeof(Admin_Resources))]
        [Required(ErrorMessageResourceType = typeof(Admin_Resources), ErrorMessageResourceName = ZnodeAdmin_Resources.ErrorMessageShippingAmountRequired)]
        [Range(0.1, 9999, ErrorMessageResourceType = typeof(Admin_Resources), ErrorMessageResourceName = ZnodeAdmin_Resources.ErrorShippingAmountRange)]
        public decimal? ShippingAmount { get; set; }

        public int ShippingId { get; set; }

         public int ShippingTypeId { get; set; }
    }
}
