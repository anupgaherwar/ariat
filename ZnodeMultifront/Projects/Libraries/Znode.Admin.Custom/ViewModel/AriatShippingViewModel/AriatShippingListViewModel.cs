﻿using System.Collections.Generic;

using Znode.Engine.Admin.Models;
using Znode.Engine.Admin.ViewModels;

namespace Znode.Admin.Custom.ViewModel
{
    public class AriatShippingListViewModel : BaseViewModel
    {
        public List<AriatShippingViewModel> ShippingRateList { get; set; }
        public GridModel GridModel { get; set; }
        public string ShippingCode { get; set; }
        public string ClassName { get; set; } = "AriatShippingCustom";
        public int ShippingId { get; set; }
        public int ShippingTypeId { get; set; }
    }
}
