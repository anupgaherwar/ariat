﻿using Znode.Engine.Admin.ViewModels;

namespace Znode.Admin.Custom.ViewModel
{
    public class AriatProgramCatalogProductViewModel : BaseViewModel
    {
        public int AriatProgramCatalogProductId { get; set; }

        public string ProductName { get; set; }
        public string StyleNumber { get; set; }
        public string SpecificColor { get; set; }
        public string ProductFamily { get; set; }
        public string SAPCategory { get; set; }
        public string SAPProductGroup { get; set; }
        public string SAPSubGroup { get; set; }
        public string Gender { get; set; }
        public string IsCustomizableFlag { get; set; }
        public string WaterproofFlag { get; set; }
        public string ExtendedSizes { get; set; }
        public string SafetyToe { get; set; }
        public string SafetySpecifications { get; set; }
        public string ToeShape { get; set; }
        public string Technology { get; set; }
        public string AccountExternalID { get; set; }
    }
}
