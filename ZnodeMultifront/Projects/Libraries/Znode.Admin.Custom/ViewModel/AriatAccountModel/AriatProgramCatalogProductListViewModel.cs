﻿using System.Collections.Generic;

using Znode.Engine.Admin.Models;
using Znode.Engine.Admin.ViewModels;

namespace Znode.Admin.Custom.ViewModel
{
    public class AriatProgramCatalogProductListViewModel : BaseViewModel
    {
        public List<AriatProgramCatalogProductViewModel> ProductList { get; set; }
        public GridModel GridModel { get; set; }
        public string AccountExternalId { get; set; }
        public int AccountId { get; set; }
        public string AccountName { get; set; }
        public bool HasParentAccounts { get; set; }
    }
}
