﻿namespace Znode.Admin.Custom.ViewModel
{
    public class AriatOrderLineItemsDetailViewModel
    {
        public string InvoiceDate { get; set; }

        public string BackorderDate { get; set; }

        public string InvoiceNumber { get; set; }

        public string ShipDate { get; set; }

        public string ExpectedShipDate { get; set; }
    }
}
