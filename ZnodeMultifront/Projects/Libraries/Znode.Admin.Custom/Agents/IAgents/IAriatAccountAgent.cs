﻿
using System.Collections.Generic;

using Znode.Admin.Custom.ViewModel;
using Znode.Engine.Api.Client.Sorts;
using Znode.Libraries.ECommerce.Utilities;
namespace Znode.Admin.Custom.Agents
{
    public interface IAriatAccountAgent
    {
        /// <summary>
        /// Program Catalog Product List
        /// </summary>
        /// <param name="filters">filters for program catalog product list.</param>
        /// <param name="sortCollection">sorts for program catalog product list.</param>
        /// <param name="page">page no. for program catalog product list.</param>
        /// <param name="recordPerPage">record per page for program catalog product list.</param>
        /// <param name="accountId">account id for program catalog product list.</param>
        /// <param name="accountCode">account external id for program catalog product list.</param>
        /// <returns>Product list for program catalog.</returns>
        AriatProgramCatalogProductListViewModel ProgramCatalogProductList(FilterCollection filters, SortCollection sortCollection, int page, int recordPerPage, int accountId, string accountCode);

        /// <summary>
        /// Return Catalog related product list.
        /// </summary>
        /// <param name="filters">filters for program catalog product list.</param>
        /// <param name="sorts">sorts for program catalog product list.</param>
        /// <param name="pageIndex">page no. for program catalog product list.</param>
        /// <param name="pageSize">page size for program catalog product list.</param>
        /// <param name="accountId">account id for program catalog product list.</param>
        /// <returns></returns>
        List<dynamic> GetFullCatalogDetails(FilterCollection filters = null, SortCollection sorts = null, int? pageIndex = null, int? pageSize = null, int accountId = 0);

        /// <summary>
        /// Dynamic list of product
        /// </summary>
        /// <param name="dataModel">list of product</param>
        /// <param name="catalogType">catalog type whether it is full catalog or program catalog.</param>
        /// <returns>dynamic list of product</returns>
        List<dynamic> CreateDataSource(List<dynamic> dataModel,string catalogType = "");

        /// <summary>
        /// Publish program catalog.
        /// </summary>
        /// <param name="accountExternalId">Account external id.</param>
        /// <param name="errorMessage">Error Message.</param>
        /// <returns>True or False for publish result.</returns>
        bool Publish(string accountExternalId, out string errorMessage);

        /// <summary>
        /// Delete Products
        /// </summary>
        /// <param name="ariatProgramCatalogProductId">AriatProgramCatalogProductId associated to product</param>
        /// <param name="errorMessage">ErrorMessage</param>
        /// <returns>True or False whether the product is deleted</returns>
        bool DeleteProducts(string ariatProgramCatalogProductId, out string errorMessage);

        /// <summary>
        /// Return Program Catalog Product list.
        /// </summary>
        /// <param name="filters">filters for program catalog product list.</param>
        /// <param name="sorts">sorts for program catalog product list.</param>
        /// <param name="pageIndex">page no. for program catalog product list.</param>
        /// <param name="pageSize">page size for program catalog product list.</param>
        /// <param name="accountId">account id for program catalog product list.</param>
        /// <returns></returns>
        List<dynamic> GetProgramCatalogDetails(FilterCollection filters = null, SortCollection sorts = null, int? pageIndex = null, int? pageSize = null, int accountId = 0);
    }
}
