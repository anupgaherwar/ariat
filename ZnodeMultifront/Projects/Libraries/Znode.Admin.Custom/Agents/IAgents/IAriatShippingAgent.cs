﻿using Znode.Admin.Custom.ViewModel;
using Znode.Engine.Api.Client.Sorts;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Admin.Custom.Agents
{
    public interface IAriatShippingAgent
    {
        /// <summary>
        /// Create new shipping rate
        /// </summary>
        /// <param name="viewModel">model with shipping rate details</param>
        /// <returns>Model with shipping rate</returns>
        AriatShippingViewModel CreateShippingRate(AriatShippingViewModel viewModel);
       
        /// <summary>
        /// Shipping rate list
        /// </summary>
        /// <param name="filters">Filters for shipping rate list</param>
        /// <param name="sortCollection">sorts for shipping rate list</param>
        /// <param name="page">Paging details for shipping rate list</param>
        /// <param name="recordPerPage">per page record</param>
        /// <param name="shippingId">shipping id</param>
        /// <returns>Shipping rate list</returns>
        AriatShippingListViewModel ShippingRateList(FilterCollection filters, SortCollection sortCollection, int page, int recordPerPage, int shippingId);
      
        /// <summary>
        /// Update and existing shipping rate details
        /// </summary>
        /// <param name="viewModel">Model with shipping rate details</param>
        /// <returns>True or false</returns>
        bool EditShippingRate(AriatShippingViewModel viewModel);

        /// <summary>
        /// Delete An existing record 
        /// </summary>
        /// <param name="ariatCustomShippingChargesId">shipping rate id</param>
        /// <returns>True or false</returns>
        bool DeleteShippingRate(string ariatCustomShippingChargesId);
        
        /// <summary>
        /// Get Shipping rate view.
        /// </summary>
        /// <param name="shippingId">Shipping id</param>
        /// <param name="shippingCode">Shipping code</param>
        /// <returns>Model shipping rate details</returns>
        AriatShippingViewModel CreateShippinRate(int shippingId, string shippingCode);
        AriatShippingViewModel GetShippingRateDetails(int ariatCustomShippingChargesId, int shippingId);
    }
}
