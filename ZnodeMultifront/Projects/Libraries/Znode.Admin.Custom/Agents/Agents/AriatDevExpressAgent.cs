﻿
using System;
using System.Collections.Generic;
using System.Diagnostics;
using Znode.Api.Client.Custom.Clients;
using Znode.Engine.Admin.Agents;
using Znode.Engine.Api.Client;
using Znode.Libraries.DevExpress.Report;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using System.Linq;
using Znode.Engine.Admin.Helpers;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Client.Sorts;

namespace Znode.Admin.Custom.Agents.Agents
{
    public class AriatDevExpressAgent : DevExpressReportAgent, IAriatDevExpressAgent
    {
        #region private variables.
        private readonly IAriatDevExpressClient _ariatdevExpressReportClient;
        private readonly IDevExpressReportClient _devExpressReportClient;
        private readonly IAccountClient _accountClient;
        #endregion

        #region Public Constructor.

        public AriatDevExpressAgent(IAriatDevExpressClient aritDevExpressReportClient, IDevExpressReportClient devExpressReportClient, IPortalClient portalClient, IWarehouseClient warehouseClient, Znode.Libraries.DevExpress.Report.IReportHelper deportHelper, IAccountClient accountClient) : base(devExpressReportClient, portalClient, warehouseClient, deportHelper)
        {
            _ariatdevExpressReportClient = GetClient<IAriatDevExpressClient>(aritDevExpressReportClient);
            _devExpressReportClient = devExpressReportClient;
            _accountClient = accountClient;
        }
        #endregion

        #region Public method

        public override Znode.Libraries.DevExpress.Report.ReportSettingModel GetReportSetting(string reportCode)
        {
            Libraries.DevExpress.Report.ReportSettingModel baseModel = base.GetReportSetting(reportCode);
            baseModel.AccountList = GetAccountList();
            return baseModel;
        }

        public virtual List<DevExpressReportParameterModel> GetAccountList()
        {
            FilterCollection filters = new FilterCollection();
            SortCollection sortCollection = new SortCollection();
            sortCollection.Add("Name", SortDirections.Ascending);
            
            AccountListModel accountList = _accountClient.GetAccountList(null, filters, sortCollection, null, null);

            List<DevExpressReportParameterModel> accountSelectList = new List<DevExpressReportParameterModel>();

            if (accountList.Accounts.Count > 0)
                accountList.Accounts.ForEach(item => { accountSelectList.Add(new DevExpressReportParameterModel() { Value = item.Name, Id = item.AccountId, DisplayMember = item.Name }); });

            ZnodeLogging.LogMessage("Agent method executed.", ZnodeLogging.Components.Reports.ToString(), TraceLevel.Info);
            return accountSelectList.Count > 0 ? accountSelectList : new List<DevExpressReportParameterModel>();
        }
        public override dynamic GetReportDataFromAPI(string reportCode, dynamic reportFilterModel)
        {
            var filterCollection = AssignValuesToFilters(reportFilterModel);
            switch (reportCode.ToLower())
            {
                case "vouchers":
                    return (dynamic)_ariatdevExpressReportClient.GetVouchersReport(filterCollection);

                //Report type "Sales"
                case ReportConstants.Orders:
                    return (dynamic)_devExpressReportClient.GetOrdersReport(filterCollection);
                case ReportConstants.Coupons:
                    return (dynamic)_devExpressReportClient.GetCouponReport(filterCollection);
                case ReportConstants.SalesTax:
                    return (dynamic)_devExpressReportClient.GetSalesTaxReport(filterCollection);
                case ReportConstants.AffiliateOrders:
                    return (dynamic)_devExpressReportClient.GetAffiliateOrderReport(filterCollection);
                case ReportConstants.OrderPickList:
                    return (dynamic)_devExpressReportClient.GetOrderPickListReport(filterCollection);

                //Report type "Product"
                case ReportConstants.BestSellerProduct:
                    return (dynamic)_devExpressReportClient.GetBestSellerProductReport(filterCollection);
                case ReportConstants.InventoryReorder:
                    return (dynamic)_devExpressReportClient.GetInventoryReorderReport(filterCollection);
                case ReportConstants.TopEarningProduct:
                    return (dynamic)_devExpressReportClient.GetBestSellerProductReport(filterCollection);
                case ReportConstants.PopularSearch:
                    return (dynamic)_devExpressReportClient.GetPopularSearchReport(filterCollection);

                //Report type "Review"
                case ReportConstants.ServiceRequest:
                    return (dynamic)_devExpressReportClient.GetServiceRequestReport(filterCollection);

                //Report type "Customer"
                case ReportConstants.Users:
                    return (dynamic)_devExpressReportClient.GetUsersReport(filterCollection);
                case ReportConstants.EmailOptInCustomer:
                    return (dynamic)_devExpressReportClient.GetEmailOptInCustomerReport(filterCollection);
                case ReportConstants.MostFrequentCustomer:
                    return (dynamic)_devExpressReportClient.GetMostFrequentCustomerReport(filterCollection);
                case ReportConstants.TopSpendingCustomers:
                    return (dynamic)_devExpressReportClient.GetTopSpendingCustomersReport(filterCollection);

                //Report type "General"
                case ReportConstants.Vendors:
                    return (dynamic)_devExpressReportClient.GetVendorsReport(filterCollection);
                //Report type "General"
                case ReportConstants.AbandonedCart:
                    return (dynamic)_devExpressReportClient.GetAbandonedCartReport(filterCollection);

                default:
                    return (dynamic)_devExpressReportClient.GetOrdersReport(filterCollection);
            }
        }
        #endregion
    }
}
