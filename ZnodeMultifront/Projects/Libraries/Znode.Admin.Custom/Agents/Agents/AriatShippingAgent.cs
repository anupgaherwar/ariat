﻿using System;
using System.Collections.Generic;

using System.Diagnostics;
using System.Linq;
using Znode.Admin.Custom.ViewModel;
using Znode.Api.Client.Custom.Clients;
using Znode.Engine.Admin.Agents;
using Znode.Engine.Admin.Extensions;
using Znode.Engine.Admin.Models;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;
using Znode.Sample.Api.Model;

namespace Znode.Admin.Custom.Agents
{
    public class AriatShippingAgent : BaseAgent, IAriatShippingAgent
    {
        #region Private variable
        private readonly IAriatShippingClient _ariatShippingClient;
        private readonly IShippingClient _shippingsClient;
        public const string LoggingComponent = "Custom";
        #endregion

        #region Controller

        public AriatShippingAgent(IAriatShippingClient client, IShippingClient shippingClient)
        {
            _ariatShippingClient = GetClient<IAriatShippingClient>(client);
            _shippingsClient = GetClient<IShippingClient>(shippingClient);
        }
        #endregion

        //Get Shipping rate list
        public AriatShippingListViewModel ShippingRateList(FilterCollection filters, SortCollection sortCollection, int page, int recordPerPage, int shippingId)
        {
            //If the sort key is not present the default order will be according to shipping amount.
            if (HelperUtility.IsNull(sortCollection) || sortCollection.Count == 0)
            {
                sortCollection = new SortCollection();
                sortCollection.Add("ShippingAmount", SortDirections.Ascending);
            }

            filters.RemoveAll(x => x.FilterName == ZnodeShippingEnum.ShippingId.ToString());

            filters.Add(ZnodeShippingEnum.ShippingId.ToString(), FilterOperators.Equals, shippingId.ToString());

            AriatShippingListModel list = _ariatShippingClient.ShippingRateList(null, filters, sortCollection, page, recordPerPage);

            AriatShippingListViewModel listViewModel = new AriatShippingListViewModel { ShippingRateList = list?.ShippingList?.ToViewModel<AriatShippingViewModel>().ToList(), ShippingId = shippingId };

            SetListPagingData(listViewModel, list);

            //Set tool menu for gender list.
            SetToolMenu(listViewModel);

            return listViewModel?.ShippingRateList?.Count > 0 ? listViewModel
                : new AriatShippingListViewModel { ShippingRateList = new List<AriatShippingViewModel>(), ShippingId = shippingId };
        }

        private void SetToolMenu(AriatShippingListViewModel model)
        {
            if (HelperUtility.IsNotNull(model))
            {
                model.GridModel = new GridModel
                {
                    FilterColumn = new FilterColumnListModel
                    {
                        ToolMenuList = new List<ToolMenuModel>()
                    }
                };
            }
        }

        //Create shipping rate
        public AriatShippingViewModel CreateShippingRate(AriatShippingViewModel viewModel)
        {
            try
            {
                AriatShippingModel model = _ariatShippingClient.CreateShippingRate(viewModel.ToModel<AriatShippingModel>());
                return HelperUtility.IsNotNull(model) ? model.ToViewModel<AriatShippingViewModel>() : new AriatShippingViewModel();
            }
            catch (ZnodeException ex)
            {
                switch (ex.ErrorCode)
                {
                    case ErrorCodes.AlreadyExist:
                        return (AriatShippingViewModel)GetViewModelWithErrorMessage(new AriatShippingViewModel(), Admin_Resources.AlreadyExistCode);
                    default:
                        return (AriatShippingViewModel)GetViewModelWithErrorMessage(new AriatShippingViewModel(), Admin_Resources.ErrorFailedToCreate);
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, LoggingComponent, TraceLevel.Error);
                return (AriatShippingViewModel)GetViewModelWithErrorMessage(viewModel, Admin_Resources.SaveErrorMessage);
            }
        }

        //Edit shipping rate details
        public bool EditShippingRate(AriatShippingViewModel viewModel)
        {
            try
            {
                return _ariatShippingClient.EditShippingRate(viewModel.ToModel<AriatShippingModel>());
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, LoggingComponent, TraceLevel.Error);
                return false;
            }
        }

        //Delete shipping rate
        public bool DeleteShippingRate(string ids)
        {
            try
            {
                return _ariatShippingClient.DeleteShippingRate(new ParameterModel { Ids = ids });
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, LoggingComponent, TraceLevel.Error);
                return false;
            }
        }

        //Create shipping rate details
        public AriatShippingViewModel CreateShippinRate(int shippingId, string shippingCode)
        {
            if (string.IsNullOrEmpty(shippingCode))
            {
                var shippingModel = _shippingsClient.GetShipping(shippingId);

                return new AriatShippingViewModel { ShippingCode = shippingModel.ShippingCode, ShippingId = shippingId, ShippingTypeId = shippingModel.ShippingTypeId};
            }

            return new AriatShippingViewModel { ShippingCode = shippingCode, ShippingId = shippingId };
        }

        public AriatShippingViewModel GetShippingRateDetails(int ariatCustomShippingChargesId, int shippingId)
        {
            AriatShippingModel shippingModel = _ariatShippingClient.GetShippingRateDetails(ariatCustomShippingChargesId);
            AriatShippingViewModel viewModel = HelperUtility.IsNotNull(shippingModel) ? shippingModel.ToViewModel<AriatShippingViewModel>() : new AriatShippingViewModel();

            viewModel.ShippingId = shippingId;
            return viewModel;
        }
    }
}
