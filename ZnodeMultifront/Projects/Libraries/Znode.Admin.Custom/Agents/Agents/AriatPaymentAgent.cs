﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Admin.Agents;
using Znode.Engine.Admin.Extensions;
using Znode.Engine.Admin.Helpers;
using Znode.Engine.Admin.ViewModels;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;

namespace Znode.Admin.Custom.Agents.Agents
{
    public class AriatPaymentAgent : PaymentAgent
    {
        #region constant  
        public const string AdyenPay = "ADYEN_PAY";
        public const string Adyen = "adyen";
        public const string AdyenPayView = "~/Views/Payment/_CreditCartDetails.cshtml";
        public const string AdyenView = "~/Views/Payment/_AdyenGateway.cshtml";
        #endregion

        #region constructor
        public AriatPaymentAgent(IPaymentClient paymentClient, IProfileClient profileClient):base(paymentClient, profileClient)
        {
          
        }
        #endregion

        #region public method
        public override string GetPaymentTypeView(string paymentCode)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("PaymentCode to get payment type view:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { PaymentCode = paymentCode });
            switch (paymentCode)
            {
                case ZnodeConstant.CreditCard:
                    return AdminConstants.CreditCardView;
                case ZnodeConstant.PurchaseOrder:
                    return AdminConstants.PurchaseOrderView;
                case ZnodeConstant.PAYPAL_EXPRESS:
                    return AdminConstants.PayPalExpressView;
                case ZnodeConstant.COD:
                    return AdminConstants.CODView;
                case ZnodeConstant.Amazon_Pay:
                    return AdminConstants.AmazonPayView;
                /*Adyen Start:Add adyen payment view start*/
                case AdyenPay:
                    return AdyenPayView;
                /*Adyen End:adyen payment view end*/
            }
            return AdminConstants.CODView;
        }

        public override string GetPaymentGatewayView(string gatewayCode)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("GatewayCode to get payment gateway view:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { GatewayCode = gatewayCode });
            if (!String.IsNullOrEmpty(gatewayCode))
            {
                switch (gatewayCode)
                {
                    case AdminConstants.AuthorizeNet:
                        return AdminConstants.AuthorizeNetView;
                    case AdminConstants.Payflow:
                        return AdminConstants.PayflowView;
                    case AdminConstants.PaymentechOrbital:
                        return AdminConstants.PaymentechOrbitalView;
                    case AdminConstants.PayPalDirectPayment:
                        return AdminConstants.PayPalDirectPaymentView;
                    case AdminConstants.WorldPay:
                        return AdminConstants.WorldPayView;
                    case AdminConstants.CyberSource:
                        return AdminConstants.CyberSourceView;
                    case AdminConstants.Checkout2:
                        return AdminConstants.Checkout2View;
                    case AdminConstants.Stripe:
                        return AdminConstants.StripeView;
                    case AdminConstants.Braintree:
                        return AdminConstants.BraintreeView;
                    case ZnodeConstant.Amazon_Pay_Gateway:
                        return AdminConstants.AmazonPayView;
                    /*Add adyen payment view start*/
                    case Adyen:
                        return AdyenView;
                    /*Add adyen payment view end*/
                }
                return AdminConstants.CustomGatewayView;
            }
            else
            {
                return AdminConstants.PayPalExpressView;
            }
        }
        #endregion

    }
}
