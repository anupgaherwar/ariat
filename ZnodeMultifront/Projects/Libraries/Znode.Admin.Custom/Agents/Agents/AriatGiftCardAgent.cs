﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using Znode.Engine.Admin.Agents;
using Znode.Engine.Admin.Extensions;
using Znode.Engine.Admin.Helpers;
using Znode.Engine.Admin.ViewModels;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;

using static Znode.Libraries.ECommerce.Utilities.HelperUtility;

namespace Znode.Admin.Custom.Agents.Agents
{
    public class AriatGiftCardAgent : GiftCardAgent
    {
        private readonly IGiftCardClient _giftCardClient;

        public AriatGiftCardAgent(IGiftCardClient giftCardClient, IPortalClient portalClient, IUserClient userClient, IRMARequestItemClient rmaRequestItemClient, IRMARequestClient rmaRequestClient, IAccountClient accountClient, ICurrencyClient currencyClient, IPortalUnitClient portalUnitClient) : base(giftCardClient, portalClient, userClient, rmaRequestItemClient, rmaRequestClient, accountClient, currencyClient, portalUnitClient)
        {
            _giftCardClient = GetClient<IGiftCardClient>(giftCardClient);
        }

        public override GiftCardListViewModel GetGiftCardList(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize, bool isExcludeExpired, int userId = 0)
        {
            //Setting isExcludeExpired to false explicitly to change the base code behaviour.
            isExcludeExpired = false;
           
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters filters, expands and sortCollection.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new object[] { expands, filters, sorts });
            //If exclude expired is true, add filter for expiration date.
            if (isExcludeExpired)
                SetFilterForExpiredGiftCard(filters);
            else
                filters.RemoveAll(x => string.Equals(x.FilterName, $"{ZnodeGiftCardEnum.ExpirationDate.ToString()}", StringComparison.CurrentCultureIgnoreCase));

            SetUserFilter(filters, userId);
            //Get the sort collection for gift card id desc.
            sorts = HelperMethods.SortDesc(ZnodeGiftCardEnum.GiftCardId.ToString(), sorts);

            GiftCardListModel giftCardList = _giftCardClient.GetGiftCardList(expands, filters, sorts, pageIndex, pageSize);
            GiftCardListViewModel listViewModel = new GiftCardListViewModel { GiftCardList = giftCardList?.GiftCardList?.ToViewModel<GiftCardViewModel>().ToList() };
            listViewModel.IsExcludeExpired = isExcludeExpired;
            if (userId > 0)
                listViewModel.UserId = userId;
            SetListPagingData(listViewModel, giftCardList);

            //Set the Tool Menus for Gift card List Grid View.
            SetGiftCardToolMenus(listViewModel);
            ZnodeLogging.LogMessage("Agent method execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return giftCardList?.GiftCardList?.Count > 0 ? listViewModel : new GiftCardListViewModel() { GiftCardList = new List<GiftCardViewModel>(), IsExcludeExpired = isExcludeExpired, UserId = userId };
        }

        private void SetFilterForExpiredGiftCard(FilterCollection filters)
        {
            filters.RemoveAll(x => string.Equals(x.FilterName, $"{ZnodeGiftCardEnum.ExpirationDate.ToString()}", System.StringComparison.CurrentCultureIgnoreCase));
            filters.Add($"{ZnodeGiftCardEnum.ExpirationDate.ToString()}", FilterOperators.GreaterThanOrEqual, System.DateTime.Today.ToShortDateString());
        }

        private void SetGiftCardToolMenus(GiftCardListViewModel model)
        {
            if (IsNotNull(model))
            {
                model.GridModel = new Znode.Engine.Admin.Models.GridModel();
                model.GridModel.FilterColumn = new Znode.Engine.Admin.Models.FilterColumnListModel();
                model.GridModel.FilterColumn.ToolMenuList = new List<Znode.Engine.Admin.Models.ToolMenuModel>();
                if (model.UserId <= 0)
                    model.GridModel.FilterColumn.ToolMenuList.Add(new Znode.Engine.Admin.Models.ToolMenuModel { DisplayText = Admin_Resources.ButtonDelete, JSFunctionName = "EditableText.prototype.DialogDelete('GiftCardDeletePopup')", ControllerName = "GiftCard", ActionName = "Delete" });

                model.GridModel.FilterColumn.ToolMenuList.Add(new Znode.Engine.Admin.Models.ToolMenuModel { DisplayText = Admin_Resources.Activate, JSFunctionName = "EditableText.prototype.DialogDelete('ActivateVoucher')", ControllerName = "GiftCard", ActionName = "ActiveDeactiveVouchers" });
                model.GridModel.FilterColumn.ToolMenuList.Add(new Znode.Engine.Admin.Models.ToolMenuModel { DisplayText = Admin_Resources.TextInactive, JSFunctionName = "EditableText.prototype.DialogDelete('DeactivateVoucher')", ControllerName = "GiftCard", ActionName = "ActiveDeactiveVouchers" });
            }
        }
    }
}
