﻿
using System;
using System.Diagnostics;
using System.IO;
using System.Web;

using Znode.Engine.Admin.Agents;
using Znode.Engine.Admin.Helpers;
using Znode.Engine.Admin.Maps;
using Znode.Engine.Admin.ViewModels;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Libraries.Framework.Business;

namespace Znode.Admin.Custom.Agents
{
    public class AriatImportAgent : ImportAgent
    {

        #region Private Varaibles
        private readonly IImportClient _importClient;
        #endregion

        #region Public constructor

        public AriatImportAgent(IImportClient importClient, IPriceClient priceClient, ICountryClient countryClient, IPortalClient portalClient) : base(importClient, priceClient, countryClient, portalClient)
        {
            _importClient = importClient;
        }

        #endregion


        #region Public method
        public override bool ImportData(ImportViewModel importviewModel)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.Import.ToString(), TraceLevel.Info);
            string fileName = importviewModel?.FileName;
            ZnodeLogging.LogMessage("fileName property of importviewModel: ", ZnodeLogging.Components.Import.ToString(), TraceLevel.Verbose, new { fileName = fileName });
            try
            {
                importviewModel.FileName = fileName;
                ImportModel importModel = ImportViewModelMap.ToModel(importviewModel);
                importModel.Custom1 = importviewModel.Custom1;
                bool isSuccess = _importClient.ImportData(importModel);
                RemoveTemporaryFiles(fileName);
                ZnodeLogging.LogMessage("Agent method execution done.", ZnodeLogging.Components.Import.ToString(), TraceLevel.Info);
                return isSuccess;
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Import.ToString(), TraceLevel.Warning);
                //Remove the file if any error comes in.
                RemoveTemporaryFiles(fileName);
                return false;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Import.ToString(), TraceLevel.Error);
                return false;
            }
        }
        #endregion

        #region private methods

        private void RemoveTemporaryFiles(string fileName)
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(HttpContext.Current.Server.MapPath(AdminConstants.ImportFolderPath));

            //Delete file from the directory.
            foreach (FileInfo file in directoryInfo.GetFiles())
            {
                if (file.FullName.Equals(fileName))
                {
                    file.Delete();
                    break;
                }
            }
        } 
        #endregion
    }
}
