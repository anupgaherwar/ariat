﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;


using Znode.Engine.Admin.Agents;
using Znode.Engine.Admin.Helpers;
using Znode.Engine.Admin.Models;
using Znode.Engine.Admin.ViewModels;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Models;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Admin.Extensions;

namespace Znode.Admin.Custom
{
    public class AriatCustomerAgent : CustomerAgent
    {
        #region Private Variables
        private readonly ICustomerClient _customerClient;
        private readonly IAccountClient _accountClient;
        #endregion

        #region Constructor
        public AriatCustomerAgent(ICustomerClient customerClient, IProfileClient profileClient, IAccountClient accountClient, IDomainClient domainClient, IPriceClient priceClient,
            IGiftCardClient giftCardClient, IOrderClient orderClient) : base
            (customerClient, profileClient, accountClient, domainClient, priceClient, giftCardClient, orderClient)
        {
            _customerClient = GetClient<ICustomerClient>(customerClient);
            _accountClient = GetClient<IAccountClient>(accountClient);
        }
        #endregion


        #region Public Method
        public override AddressListViewModel GetAddressList(int userId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            ZnodeLogging.LogMessage("Agent Method Execution started.", ZnodeLogging.Components.Customers.ToString(), TraceLevel.Info);
            IUserClient _userClient = new UserClient();
            UserModel userModel = _userClient.GetUserAccountData(userId);
            if (IsNotNull(userModel))
            {
                SetFiltersForAddress(filters, userModel);

                //Get the sort collection for address id desc.
                sortCollection = HelperMethods.SortDesc(ZnodeAddressEnum.AddressId.ToString(), sortCollection);


                //expand for address.
                ExpandCollection expands = new ExpandCollection();
                expands.Add(ZnodeUserAddressEnum.ZnodeAddress.ToString());
                ZnodeLogging.LogMessage("Input parameters:", ZnodeLogging.Components.Customers.ToString(), TraceLevel.Verbose, new { expands = expands, filters = filters, sorts = sortCollection });

                AddressListModel addressList = new AddressListModel();
                AddressListModel userAddresslist = new AddressListModel();
                if (userModel.AccountId > 0)
                {
                    addressList = _accountClient.GetAddressList(expands, filters, sortCollection, pageIndex, recordPerPage);
                }

                if (userModel.UserId > 0)
                {
                    SetFiltersForUser(filters, userModel);
                    userAddresslist = _customerClient.GetAddressList(expands, filters, sortCollection, pageIndex, recordPerPage);
                }
                if (HelperUtility.IsNotNull(userAddresslist?.AddressList) && userAddresslist?.AddressList.Count > 0)
                {
                    addressList?.AddressList.AddRange(userAddresslist?.AddressList);
                    addressList.TotalResults = addressList?.AddressList.Count();
                }
                AddressListViewModel addressListViewModel = new AddressListViewModel { AddressList = addressList?.AddressList?.ToViewModel<AddressViewModel>().ToList() };
                SetListPagingData(addressListViewModel, addressList);
                addressListViewModel.AccountId = Convert.ToInt32(userModel.AccountId);
                addressListViewModel.AccountName = userModel.Accountname;
                addressListViewModel.CustomerName = $"{userModel.FirstName} {userModel.LastName}";
                if (userModel.IsGuestUser && IsNotNull(addressListViewModel.AddressList))
                    addressListViewModel.AddressList.First().IsGuest = userModel.IsGuestUser;

                //Check if role name is administrator, set IsRoleAdministrator flag to true.
                addressListViewModel.IsRoleAdministrator = string.Equals(userModel.RoleName, ZnodeRoleEnum.Administrator.ToString(), StringComparison.CurrentCultureIgnoreCase);

                //Set the Tool Menus for Customer Address List Grid View.
                SetCustomerAddressToolMenus(addressListViewModel);
                ZnodeLogging.LogMessage("Agent Method Executed.", ZnodeLogging.Components.Customers.ToString(), TraceLevel.Info);
                return addressListViewModel?.AddressList?.Count > 0 ? addressListViewModel
                    : new AddressListViewModel { AddressList = new List<AddressViewModel>(), AccountId = Convert.ToInt32(userModel.AccountId), AccountName = userModel.Accountname, CustomerName = $"{userModel.FirstName} {userModel.LastName}" };
            }
            return new AddressListViewModel { AddressList = new List<AddressViewModel>() };
        }
        #endregion

        #region Private Methods
        private void SetFiltersForAddress(FilterCollection filters, UserModel userModel)
        {
            ZnodeLogging.LogMessage("Input parameters filters:", ZnodeLogging.Components.Customers.ToString(), TraceLevel.Verbose, new { filters = filters });
            if (userModel.AccountId > 0)
            {
                //If Id Already present in filters Remove It
                filters.RemoveAll(x => x.Item1 == ZnodeNoteEnum.UserId.ToString());
                //Set filters for account id.
                HelperMethods.SetAccountIdFilters(filters, Convert.ToInt32(userModel.AccountId));
            }
            else
            {
                //If AccountId Already prsent in filters Remove It
                filters.RemoveAll(x => x.Item1.ToLower() == ZnodeAccountPermissionEnum.AccountId.ToString().ToLower());
                //Set filters for user id.
                HelperMethods.SetUserIdFilters(filters, userModel.UserId);
            }
        }

        private void SetFiltersForUser(FilterCollection filters, UserModel userModel)
        {
            ZnodeLogging.LogMessage("Input parameters filters:", ZnodeLogging.Components.Customers.ToString(), TraceLevel.Verbose, new { filters = filters });

            //If AccountId Already prsent in filters Remove It
            filters.RemoveAll(x => x.Item1.ToLower() == ZnodeAccountPermissionEnum.AccountId.ToString().ToLower());
            //Set filters for user id.
            HelperMethods.SetUserIdFilters(filters, userModel.UserId);

        }

        private void SetCustomerAddressToolMenus(AddressListViewModel model)
        {
            if (IsNotNull(model) && (model.AccountId < 1 || model.IsRoleAdministrator) && IsNotNull(model.AddressList) && IsNotNull(model.AddressList.First()) && !model.AddressList.First().IsGuest)
            {
                model.GridModel = new GridModel();
                model.GridModel.FilterColumn = new FilterColumnListModel();
                model.GridModel.FilterColumn.ToolMenuList = new List<ToolMenuModel>();
                model.GridModel.FilterColumn.ToolMenuList.Add(new ToolMenuModel { DisplayText = Admin_Resources.ButtonDelete, JSFunctionName = "EditableText.prototype.DialogDelete('AddressDeletePopup')", ControllerName = "Customer", ActionName = "DeleteAddress" });
            }
        }

        #endregion


    }


}
