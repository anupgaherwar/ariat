﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;

using Znode.Admin.Custom.ViewModel;
using Znode.Api.Client.Custom.Clients;
using Znode.Custom.Api.Model;
using Znode.Engine.Admin.Agents;
using Znode.Engine.Admin.Extensions;
using Znode.Engine.Admin.Models;
using Znode.Engine.Admin.ViewModels;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;

namespace Znode.Admin.Custom.Agents
{
    public class AriatAccountAgent : AccountAgent, IAriatAccountAgent
    {
        #region Private variables
        private readonly IAriatAccountClient _ariatAccountClient;
        public const string LoggingComponent = "CustomAccount";
        public const string AccountExternalID = "AccountExternalID";
        #endregion

        #region Public constructor
        public AriatAccountAgent(IAriatAccountClient ariatAccountClient, IAccountClient accountClient, IAccessPermissionClient accessPermissionClient, IUserClient userClient, IPriceClient priceClient) : base(accountClient, accessPermissionClient, userClient, priceClient)
        {
            _ariatAccountClient = GetClient<IAriatAccountClient>(ariatAccountClient);
        }
        #endregion

        #region Public Method
        //Get program catalog product list.
        public AriatProgramCatalogProductListViewModel ProgramCatalogProductList(FilterCollection filters, SortCollection sortCollection, int page, int recordPerPage, int accountId, string accountExternalId)
        {
            //Get Parent Account Details, to bind the details.
            AccountDataViewModel accountDetails = GetAccountById(accountId);
            SetFilters(filters, accountId, accountDetails);
            AriatProgramCatalogProductListModel list = _ariatAccountClient.ProgramCatalogProductList(null, filters, sortCollection, page, recordPerPage);
            RemoveFilters(filters);
            AriatProgramCatalogProductListViewModel listViewModel = new AriatProgramCatalogProductListViewModel { ProductList = list.ProductList.ToViewModel<AriatProgramCatalogProductViewModel>().ToList() };
            SetListPagingData(listViewModel, list);
            listViewModel.HasParentAccounts = accountDetails.HasParentAccounts;
            listViewModel.AccountName = accountDetails?.CompanyAccount?.Name;
            listViewModel.AccountExternalId = accountDetails?.CompanyAccount?.ExternalId;
            //Set tool menu for gender list.
            SetToolMenu(listViewModel);
            return listViewModel?.ProductList?.Count > 0 ? listViewModel
                                : new AriatProgramCatalogProductListViewModel { ProductList = new List<AriatProgramCatalogProductViewModel>(), AccountExternalId = accountDetails?.CompanyAccount?.ExternalId, AccountName = accountDetails?.CompanyAccount?.Name, HasParentAccounts = accountDetails.HasParentAccounts };
        }

        //Get the Full Catalog details
        public List<dynamic> GetFullCatalogDetails(FilterCollection filters = null, SortCollection sorts = null, int? pageIndex = null, int? pageSize = null, int accountId = 0)
        {
            List<dynamic> dataList = new List<dynamic>();
            try
            {
                ZnodeLogging.LogMessage("Agent method execution started.", LoggingComponent, TraceLevel.Info);
                filters = SetCatalogFilters(filters, accountId);
                int totalRecordCount = 0;
                pageIndex = 1;
                pageSize = 100;
                do
                {
                    AriatFullCatalogProductListModel catalogProductListModel = _ariatAccountClient.FullCatalogProductListModel(null, filters, sorts, pageIndex, pageSize);
                    dataList.AddRange(catalogProductListModel.CatalogProductsDetailList);
                    // Set value of total records count
                    totalRecordCount = Convert.ToInt32(catalogProductListModel.TotalResults);
                    // Increase pageIndex to fetch next chunk data
                    pageIndex++;
                }
                while (dataList.Count < totalRecordCount);
                ZnodeLogging.LogMessage("Agent method execution done.", LoggingComponent, TraceLevel.Info);
                return dataList;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, LoggingComponent, TraceLevel.Error);
                return dataList;
            }
        }

        //Get the Program Catalog Product details
        public List<dynamic> GetProgramCatalogDetails(FilterCollection filters = null, SortCollection sorts = null, int? pageIndex = null, int? pageSize = null, int accountId = 0)
        {
            List<dynamic> dataList = new List<dynamic>();
            try
            {
                ZnodeLogging.LogMessage("Agent method execution started.", LoggingComponent, TraceLevel.Info);
                AccountDataViewModel accountDetails = GetAccountById(accountId);
                if (HelperUtility.IsNull(filters))
                    filters = new FilterCollection();
                SetFilters(filters, accountId, accountDetails);
                int totalRecordCount = 0;
                pageIndex = 1;
                pageSize = 100;
                do
                {
                    AriatProgramCatalogProductListModel list = _ariatAccountClient.ProgramCatalogProductList(null, filters, sorts, (int)pageIndex, (int)pageSize);
                    if (list?.ProductList?.Count > 0)
                    {
                        list.ProductList.ForEach(x => x.AccountExternalID = "");
                    }
                    dataList.AddRange(list.ProductList);
                    // Set value of total records count
                    totalRecordCount = Convert.ToInt32(list.TotalResults);
                    // Increase pageIndex to fetch next chunk data
                    pageIndex++;
                }
                while (dataList.Count < totalRecordCount);
                ZnodeLogging.LogMessage("Agent method execution done.", LoggingComponent, TraceLevel.Info);
                return dataList;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, LoggingComponent, TraceLevel.Error);
                return dataList;
            }
        }

        //Create the data for the CSV.
        public virtual List<dynamic> CreateDataSource(List<dynamic> dataModel, string catalogType = "")
        {
            ZnodeLogging.LogMessage("Agent method execution started.", string.Empty, TraceLevel.Info);
            List<dynamic> _resultList = new List<dynamic>();
            if (!string.IsNullOrEmpty(catalogType))
            {
                Dictionary<string, string> keyValuePairs = new Dictionary<string, string>();
                if (catalogType == "FullCatalog")
                {
                    keyValuePairs = GetKeyValueNamesForCsv();
                }else if (catalogType == "ProgramCatalog")
                {
                    keyValuePairs = GetKeyValueNamesForPcCsv();
                }
                dataModel.ForEach(row =>
                {
                    IDictionary<string, object> columnObject = new ExpandoObject();
                    foreach (var item in keyValuePairs)
                    {
                        columnObject.Add(item.Key, GetColumnData(row, item.Value));
                    }
                    _resultList.Add(columnObject);
                });
            }
            ZnodeLogging.LogMessage("Agent method execution done.", string.Empty, TraceLevel.Info);
            return _resultList;
        }

        //Delete the products by ariatProgramCatalogProductId
        public virtual bool DeleteProducts(string ariatProgramCatalogProductId, out string errorMessage)
        {
            ZnodeLogging.LogMessage("Agent method execution started.",LoggingComponent, TraceLevel.Info);
            errorMessage = Admin_Resources.ErrorFailedToDelete;

            if (!string.IsNullOrEmpty(ariatProgramCatalogProductId))
            {
                try
                {
                    return _ariatAccountClient.DeleteProducts(new ParameterModel { Ids = ariatProgramCatalogProductId });
                }              
                catch (Exception ex)
                {
                    ZnodeLogging.LogMessage(ex,LoggingComponent, TraceLevel.Error);
                    errorMessage = Admin_Resources.ErrorFailedToDelete;
                    return false;
                }
            }
            return false;
        }

        //CSV headers and their mapping with model in key value pairs.
        public Dictionary<string, string> GetKeyValueNamesForCsv()
        {
            Dictionary<string, string> keyValuePairs = new Dictionary<string, string>();
            keyValuePairs.Add("StyleNumber", "StyleNumber");
            keyValuePairs.Add("ProductName", "ProductName");
            keyValuePairs.Add("SAPCategory", "SAPCategory");
            keyValuePairs.Add("SAPProductGroup", "SAPProductGroup");
            keyValuePairs.Add("SAPSubGroup", "SAPSubGroup");
            keyValuePairs.Add("Gender", "Gender");
            keyValuePairs.Add("WaterproofFlag", "Waterproof");
            keyValuePairs.Add("ExtendedSizes", "ExtendedSizes");
            keyValuePairs.Add("SafetyToe", "SafetyToe");
            keyValuePairs.Add("SafetySpecifications", "SafetySpecification");
            keyValuePairs.Add("ToeShape", "ToeShape");
            keyValuePairs.Add("SpecificColor", "ColorSpecific");
            keyValuePairs.Add("IsCustomizableFlag", "IsCustomizable");
            keyValuePairs.Add("ProductFamily", "ProductFamily");
            keyValuePairs.Add("Technology", "Technology");
            keyValuePairs.Add("AccountExternalID", "AccountExternalID");
            return keyValuePairs;
        }

        //Publish program catalog
        public bool Publish(string accountExternalId, out string errorMessage)
        {
            errorMessage = PIM_Resources.ErrorPublished;

            try
            {
                return Convert.ToBoolean(_ariatAccountClient.Publish(new ParameterModel { Ids = accountExternalId }).IsPublished);
            }
            catch (ZnodeException ex)
            {
                switch (ex.ErrorCode)
                {
                    case ErrorCodes.NotPermited:
                        errorMessage = PIM_Resources.ErrorPublishCatalog;
                        break;
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
            }
            return false;
        }

        //CSV headers and their mapping with model in key value pairs.
        public Dictionary<string, string> GetKeyValueNamesForPcCsv()
        {
            Dictionary<string, string> keyValuePairs = new Dictionary<string, string>();
            keyValuePairs.Add("StyleNumber", "StyleNumber");
            keyValuePairs.Add("ProductName", "ProductName");
            keyValuePairs.Add("SAPCategory", "SAPCategory");
            keyValuePairs.Add("SAPProductGroup", "SAPProductGroup");
            keyValuePairs.Add("SAPSubGroup", "SAPSubGroup");
            keyValuePairs.Add("Gender", "Gender");
            keyValuePairs.Add("WaterproofFlag", "Waterproof");
            keyValuePairs.Add("ExtendedSizes", "ExtendedSizes");
            keyValuePairs.Add("SafetyToe", "SafetyToe");
            keyValuePairs.Add("SafetySpecifications", "SafetySpecification");
            keyValuePairs.Add("ToeShape", "ToeShape");
            keyValuePairs.Add("SpecificColor", "ColorSpecific");
            keyValuePairs.Add("IsCustomizableFlag", "IsCustomizable");
            keyValuePairs.Add("ProductFamily", "ProductFamily");
            keyValuePairs.Add("Technology", "Technology");
            keyValuePairs.Add("AccountExternalID", "AccountExternalID");
            return keyValuePairs;
        }
        #endregion

        #region Private method

        private void SetToolMenu(AriatProgramCatalogProductListViewModel model)
        {
            if (HelperUtility.IsNotNull(model))
            {
                model.GridModel = new GridModel();
                model.GridModel.FilterColumn = new FilterColumnListModel();
                model.GridModel.FilterColumn.ToolMenuList = new List<ToolMenuModel>();
                model.GridModel.FilterColumn.ToolMenuList.Add(new ToolMenuModel { DisplayText = Admin_Resources.ButtonDelete, JSFunctionName = "EditableText.prototype.DialogDelete('ProgramCatalogProductDeletePopup')", ControllerName = "AriatAccount", ActionName = "Delete" });
                model.GridModel.FilterColumn.ToolMenuList.Add(new ToolMenuModel
                {
                    DisplayText = "Export Program Catalog To CSV",
                    ControllerName = "AriatAccount",
                    ActionName = "ExportProgramCatalog",
                    Value = "2",
                    Type = "ProgramCatalog",
                    JSFunctionName = "AriatAccount.prototype.ExportProgramCatalog()"
                });
                model.GridModel.FilterColumn.ToolMenuList.Add(new ToolMenuModel
                {
                    DisplayText = "Export Full Catalog To CSV",
                    ControllerName = "AriatAccount",
                    ActionName = "ExportFullCatalog",
                    Value = "1",
                    Type = "FullCatalog",
                    JSFunctionName = "AriatAccount.prototype.ExportFullCatalog()"
                });
            }
        }
        private object GetColumnData(dynamic row, dynamic col)
        {
            if (HelperUtility.IsNotNull(row.GetType().GetProperty(col)))
                return row.GetType().GetProperty(col).GetValue(row, null);

            return row[col];
        }

        //Set Filters
        private void SetFilters(FilterCollection filters, int accountId, AccountDataViewModel accountDetails)
        {          
            if (HelperUtility.IsNotNull(accountDetails) && HelperUtility.IsNotNull(accountDetails?.CompanyAccount?.ExternalId))
            {
                if (filters.Exists(x => x.Item1 == AccountExternalID))
                {
                    filters.RemoveAll(x => x.Item1 == AccountExternalID);
                    filters.Add(new FilterTuple(AccountExternalID, FilterOperators.Equals, accountDetails?.CompanyAccount?.ExternalId.ToString()));
                }
                else
                {
                    filters.Add(new FilterTuple(AccountExternalID, FilterOperators.Equals, accountDetails?.CompanyAccount?.ExternalId.ToString()));
                    filters.Add(new FilterTuple(ZnodeAccountEnum.AccountId.ToString(), FilterOperators.Equals, accountId.ToString()));
                }
            }
        }

        //Remove filters
        private void RemoveFilters(FilterCollection filters)
        {
            if (filters?.Count > 0)
            {
                filters.RemoveAll(x => x.Item1 == AccountExternalID);
                filters.RemoveAll(x => x.Item1 == ZnodeAccountEnum.AccountId.ToString());
            }
        }

        //Set Catalog Filters
        private FilterCollection SetCatalogFilters(FilterCollection filters, int accountId)
        {
            if (HelperUtility.IsNull(filters))
                filters = new FilterCollection();
            if (filters.Exists(x => x.Item1 == ZnodeAccountEnum.AccountId.ToString()))
            {
                filters.RemoveAll(x => x.Item1 == ZnodeAccountEnum.AccountId.ToString());
                filters.Add(new FilterTuple(ZnodeAccountEnum.AccountId.ToString(), FilterOperators.Equals, accountId.ToString()));
            }
            else
            {
                filters.Add(new FilterTuple(ZnodeAccountEnum.AccountId.ToString(), FilterOperators.Equals, accountId.ToString()));
            }

            return filters;
        }
        #endregion
    }
}
