﻿using AutoMapper;
using Znode.Admin.Custom.ViewModel;
using Znode.Admin.Custom.ViewModels;
using Znode.Custom.Api.Model;
using Znode.Engine.Admin.ViewModels;
using Znode.Engine.Api.Models;
using Znode.Sample.Api.Model;

namespace Znode.Admin.Custom
{
    public static class CustomAutoMapperConfig
    {
        public static void Execute()
        {
            Mapper.CreateMap<CustomPortalDetailViewModel, CustomPortalDetailModel>().ReverseMap();
            Mapper.CreateMap<StoreFeatureViewModel, PortalFeatureModel>().ReverseMap();

            Mapper.CreateMap<AriatShippingModel, AriatShippingViewModel>().ReverseMap();
            Mapper.CreateMap<AriatProgramCatalogProductModel, AriatProgramCatalogProductViewModel>().ForMember(d => d.IsCustomizableFlag, opt => opt.MapFrom(src => src.IsCustomizable))
                .ForMember(d => d.SpecificColor, opt => opt.MapFrom(src => src.ColorSpecific))
                .ForMember(d => d.WaterproofFlag, opt => opt.MapFrom(src => src.Waterproof))
                .ForMember(d => d.SafetySpecifications, opt => opt.MapFrom(src => src.SafetySpecification));
        }
    }
}
