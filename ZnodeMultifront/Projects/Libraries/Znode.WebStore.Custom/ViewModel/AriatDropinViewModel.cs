﻿
using Znode.Engine.WebStore;

namespace Znode.WebStore.Custom.ViewModel
{
    public class AriatDropinViewModel : BaseViewModel
    {
        public bool IsSuccess { get; set; }
        public string PaymentMethods { get; set; }
        public string Environment { get; set; }
        public string OriginKey { get; set; }
        public string CurrencyCode { get; set; }
        public string Locale { get; set; }
    }
}
