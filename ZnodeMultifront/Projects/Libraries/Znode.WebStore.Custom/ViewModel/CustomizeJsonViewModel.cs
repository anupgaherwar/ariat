﻿using System.Collections.Generic;

namespace Znode.WebStore.Custom.ViewModel
{
    public class CustomizeJsonViewModel
    {
        List<LocationDetails> CustomizedJson { get; set; }
    }

    public class LocationDetails
    {
        public List<TextWiget> Text { get; set; }
        public List<ImageWidget> Image { get; set; }
        public string ViewName { get; set; }
        public bool IsLargeLocation { get; set; }
    }

    public class TextWiget
    {
        public string Text { get; set; }
        public int NoOfLines { get; set; }
    }

    public class ImageWidget
    {
        public string ImageName { get; set; }
    }
}
