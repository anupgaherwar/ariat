﻿namespace Znode.WebStore.Custom.ViewModel
{
    public class LocationPriceDetailViewModel
    {
        public string DecorationLocation { get; set; }
        public string Text { get; set; }
        public string Logo { get; set; }
        public decimal LocationUnitPrice { get; set; }
     
        public string TextUnitPriceWithCurrency { get; set; }
        public string TextTotalPriceWithCurrency { get; set; }
        public string LogoUnitPriceWithCurrency { get; set; }
        public string LogoTotalPriceWithCurrency { get; set; }

        public string LargeLogoUnitPriceWithCurrency { get; set; }
        public string LargeLogoTotalPriceWithCurrency { get; set; }
    }
}
