﻿using Znode.Engine.WebStore;

namespace Znode.WebStore.Custom.ViewModel
{
    public class ZnodeSKURequestViewModel : BaseViewModel
    {
        public string SKU { get; set; }
        public string ArtifiSKU { get; set; }
        public string StyleNumber { get; set; }
    }
}
