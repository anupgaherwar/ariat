﻿using System.Collections.Generic;

namespace Znode.WebStore.Custom.ViewModel
{
    public class DecorationPriceDetailsViewModel
    {
        public string Name { get; set; }

        public int Quantity { get; set; }

        public string TotalPrice { get; set; }

        public decimal ProductUnitPrice { get; set; }

        public string ProductTotalPrice { get; set; }

        public List<LocationPriceDetailViewModel> LocationDetails { get; set; }

    }
}
