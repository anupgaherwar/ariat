﻿using Znode.Engine.WebStore;

namespace Znode.WebStore.Custom.ViewModel
{
    public class PriceParameterViewModel : BaseViewModel
    {
        public string SKU { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }
        public string CustomizedJson { get; set; }

        public int RegularLogoCount { get; set; }
        public int LargeLogoCount { get; set; }

        public int TotalTextLineCount { get; set; }
        public string LogoSKU { get; set; }
        public string LargeLogoSKU { get; set; }
        public string TextLineSKU { get; set; }
    }
}
