﻿using System.Web.Optimization;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.WebStore
{
    public class CustomBundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/ZnodeCustomJs").Include(
                                                "~/Scripts/Custom/AriatProduct.js",
                                                "~/Scripts/Custom/AriatCheckout.js",
                                                "~/Scripts/Core/Endpoint/CustomEndpoint.js"));              
        }
    }
}
