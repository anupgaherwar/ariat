﻿using System.Diagnostics;

using Znode.Api.Client.Custom.Clients;
using Znode.Custom.Api.Model;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.WebStore.Custom.ViewModel;

namespace Znode.WebStore.Custom.Agents
{
    public class AriatAdyenAgent : BaseAgent, IAriatAdyenAgent
    {
        #region private variables
        private readonly IAriatAdyenClient _ariatAdyenClient;
        private readonly IPaymentClient _paymentClient;

        #endregion

        #region constructor
        public AriatAdyenAgent(IPaymentClient paymentClient)
        {
            _ariatAdyenClient = new AriatAdyenClient();
            _paymentClient = GetClient<IPaymentClient>(paymentClient);
        }
        #endregion

        #region public methods
        //Get dropin component
        public AriatDropinViewModel GetDropin(string paymentCode)
        {
            try
            {
                PortalViewModel portalViewModel = PortalAgent.CurrentPortal;

                PaymentSettingModel _paymentModel = _paymentClient.GetPaymentSettingByPaymentCode(paymentCode);

                AdyenDropinRequestModel adyenDropinRequestModel = new AdyenDropinRequestModel();
                adyenDropinRequestModel.PaymentCode = paymentCode;
                adyenDropinRequestModel.IsTestMode = _paymentModel.TestMode;
                adyenDropinRequestModel.RequestDomain = ZnodeWebstoreSettings.ZnodeWebStoreUri;
                AriatAdyenDropInModel adyenResponse = _ariatAdyenClient.GetDropin(adyenDropinRequestModel);

                return new AriatDropinViewModel { PaymentMethods = adyenResponse.PaymentMethods, CurrencyCode = portalViewModel.CurrencyCode, Environment = adyenResponse.Environment, OriginKey = adyenResponse.OriginKey };
            }
            catch (System.Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Payment.ToString(), TraceLevel.Error);
                return new AriatDropinViewModel { HasError = true };
            }

        }
        #endregion
    }
}
