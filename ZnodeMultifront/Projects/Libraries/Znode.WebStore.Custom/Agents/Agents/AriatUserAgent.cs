﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Znode.Api.Client.Custom.Clients;
using Znode.Custom.Engine.WebStore;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.Maps;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.Data;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;

namespace Znode.WebStore.Custom.Agents
{
    public class AriatUserAgent : UserAgent, IAriatUserAgent
    {
        #region Private Variables
        private readonly IWebStoreUserClient _webStoreAccountClient;
        private readonly IAccountClient _accountClient;
        private readonly IAriatUserClient _ariatUserClient;
        #endregion

        #region Constructor
        public AriatUserAgent(ICountryClient countryClient, IWebStoreUserClient webStoreAccountClient, IWishListClient wishListClient, IUserClient userClient, IPublishProductClient productClient, ICustomerReviewClient customerReviewClient, IOrderClient orderClient,
          IGiftCardClient giftCardClient, IAccountClient accountClient, IAccountQuoteClient accountQuoteClient, IOrderStateClient orderStateClient, IPortalCountryClient portalCountryClient, IShippingClient shippingClient, IPaymentClient paymentClient, ICustomerClient customerClient, IStateClient stateClient, IPortalProfileClient portalProfileClient, IAriatUserClient ariatUserClient) : base(countryClient, webStoreAccountClient, wishListClient, userClient, productClient, customerReviewClient, orderClient,
           giftCardClient, accountClient, accountQuoteClient, orderStateClient, portalCountryClient, shippingClient, paymentClient, customerClient, stateClient, portalProfileClient)
        {
            _webStoreAccountClient = GetClient<IWebStoreUserClient>(webStoreAccountClient);
            _accountClient = GetClient<IAccountClient>(accountClient);
            _ariatUserClient = GetClient<IAriatUserClient>(ariatUserClient);
        }
        #endregion

        #region Public Methods
        // Create/Update the address of the user.       
        public override AddressViewModel CreateUpdateAddress(AddressViewModel addressViewModel, string addressType = null, bool isShippingBillingDifferent = false, bool isCreateAccountForGuestUser = false)
        {
            try
            {
                SetBillingShippingFlags(addressViewModel, addressType);
                UserViewModel model = GetUserViewModelFromSession();

                #region Checks to validate address.               

                BooleanModel booleanModel = IsValidAddress(addressViewModel);
                //Validate address if enable address validation flag is set to true.
                if (!booleanModel.IsSuccess)
                {
                    return (AddressViewModel)GetViewModelWithErrorMessage(addressViewModel, booleanModel.ErrorMessage ?? WebStore_Resources.AddressValidationError);
                }

                if (isShippingBillingDifferent && isCreateAccountForGuestUser)
                {
                    addressViewModel.IsShippingBillingDifferent = true;
                }

                if (HelperUtility.IsNotNull(model) && !isShippingBillingDifferent)
                {
                    //Validate default address.
                    addressViewModel = ValidateDefaultAddress(addressViewModel, model);
                }

                if (addressViewModel.HasError)
                {
                    return addressViewModel;
                }

                //if user is null then save address for anonymous user or
                // if aspnetUserId is null then save address for anonymous user 
                if (model?.AspNetUserId == null || (model?.UserId).GetValueOrDefault() < 1)
                {
                    return SetAnonymousUserAddresses(addressViewModel, addressType);
                }
                #endregion

                addressViewModel.UserId = model.UserId;
                if (model?.RoleName?.ToLower() != "user")
                {
                    addressViewModel.AccountId = model.AccountId.GetValueOrDefault();
                }

                //Create/Update address.
                addressViewModel = CreateUpdateAddress(addressViewModel);

                //Remove address from cache and session.
                if (addressViewModel?.AddressId > 0)
                {
                    RemoveAddressFromCacheAndSession(addressViewModel);
                }
                return addressViewModel;
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Warning);
                switch (ex.ErrorCode)
                {
                    case ErrorCodes.InvalidData:
                        return (AddressViewModel)GetViewModelWithErrorMessage(new AddressViewModel(), WebStore_Resources.ErrorAddUpdateAddressNewCustomer);
                    default:
                        return (AddressViewModel)GetViewModelWithErrorMessage(new AddressViewModel(), Admin_Resources.ErrorFailedToCreate);
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                return (AddressViewModel)GetViewModelWithErrorMessage(new AddressViewModel(), Admin_Resources.ErrorFailedToCreate);
            }
        }

        public override LoginViewModel Login(LoginViewModel model)
        {
            LoginViewModel loginViewModel;
            try
            {
                model.PortalId = PortalAgent.CurrentPortal?.PortalId;
                //Authenticate the user credentials.
                UserModel userModel = _userClient.Login(UserViewModelMap.ToLoginModel(model), GetExpands());
                if (HelperUtility.IsNotNull(userModel))
                {
                    //Check of Reset password Condition.
                    if (HelperUtility.IsNotNull(userModel.User) && !string.IsNullOrEmpty(userModel.User.PasswordToken))
                    {
                        loginViewModel = UserViewModelMap.ToLoginViewModel(userModel);
                        return ReturnErrorModel(WebStore_Resources.InvalidUserNamePassword, true, loginViewModel);
                    }
                    // Check user associated profiles.                
                    if (!userModel.Profiles.Any() && !userModel.IsAdminUser)
                        return ReturnErrorModel(WebStore_Resources.ProfileLoginFailedErrorMessage);

                    SetLoginUserProfile(userModel);

                    //Save the User Details in Session.
                    SaveInSession(WebStoreConstants.UserAccountKey, userModel.ToViewModel<UserViewModel>());
                    LoginViewModel loginModel = UserViewModelMap.ToLoginViewModel(userModel);

                    if (userModel?.UserGlobalAttributes?.Count() > 0)
                    {
                        string errorMessage = userModel.UserGlobalAttributes.FirstOrDefault(xx => xx.AttributeCode == "ShowNoProgramCatalogMessage")?.HelpDescription;
                        if (!string.IsNullOrEmpty(errorMessage))
                        {
                            loginModel.Custom1 = errorMessage;
                        }
                    }
                    //To clear the session for CartCount session key.
                    if (!loginModel.HasError)
                        _cartAgent.ClearCartCountFromSession();

                    return loginModel;
                }
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Warning);
                switch (ex.ErrorCode)
                {
                    case 2://Error Code to Reset the Password for the first time login.
                        return ReturnErrorModel(ex.ErrorMessage, true);
                    case ErrorCodes.AccountLocked:
                        return ReturnErrorModel(WebStore_Resources.ErrorAccountLocked);
                    case ErrorCodes.LoginFailed:
                        return ReturnErrorModel(WebStore_Resources.InvalidUserNamePassword);
                    case ErrorCodes.TwoAttemptsToAccountLocked:
                        return ReturnErrorModel(WebStore_Resources.ErrorTwoAttemptsRemain);
                    case ErrorCodes.OneAttemptToAccountLocked:
                        return ReturnErrorModel(WebStore_Resources.ErrorOneAttemptRemain);
                    case ErrorCodes.AdminApproval:
                        return ReturnErrorModel(WebStore_Resources.LoginFailAdminApproval);
                    default:
                        return ReturnErrorModel(WebStore_Resources.InvalidUserNamePassword);
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                return ReturnErrorModel(WebStore_Resources.InvalidUserNamePassword);
            }
            return ReturnErrorModel(WebStore_Resources.InvalidUserNamePassword);
        }

        public override UserViewModel UpdateProfile(UserViewModel model, bool webStoreUser)
        {
            UserViewModel userViewModel = GetUserViewModelFromSession();

            string giftcardAmount = userViewModel.Custom5;

            if (HelperUtility.IsNotNull(userViewModel))
                MapUserProfileData(model, userViewModel);
            try
            {
                _userClient.UpdateUserAccountData(userViewModel.ToModel<UserModel>(), webStoreUser);
                userViewModel.Custom5 = giftcardAmount;
                SaveInSession(WebStoreConstants.UserAccountKey, userViewModel);
                userViewModel.SuccessMessage = WebStore_Resources.SuccessProfileUpdated;
                Helper.ClearCache($"UserAccountAddressList{userViewModel.UserId}");
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Warning);
                switch (ex.ErrorCode)
                {
                    case ErrorCodes.AlreadyExist:
                        return (UserViewModel)GetViewModelWithErrorMessage(model, WebStore_Resources.ErrorUserNameAlreadyExists);
                    default:
                        return (UserViewModel)GetViewModelWithErrorMessage(userViewModel, WebStore_Resources.ErrorUpdateProfile);
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                return (UserViewModel)GetViewModelWithErrorMessage(userViewModel, WebStore_Resources.ErrorUpdateProfile);
            }
            return userViewModel;
        }

        public AddressViewModel GetAdyenShippingAddress()
        {
            UserViewModel userViewModel = GetUserViewModelFromSession();
            AddressModel shippingaddress = new AddressModel();
            ShoppingCartModel cartModel = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey);

            if (HelperUtility.IsNotNull(cartModel) && HelperUtility.IsNotNull(cartModel.ShippingAddress))
            {
                shippingaddress = cartModel.ShippingAddress;
            }
            if (userViewModel.Addresses.Count > 0)
            {
                shippingaddress.EmailAddress = userViewModel.Addresses.FirstOrDefault(xx => xx.AddressId == cartModel.ShippingAddress.AddressId)?.EmailAddress;
            }
            AddressViewModel address = _ariatUserClient.GetAdyenShippingAddress(shippingaddress).ToViewModel<AddressViewModel>();

            if ( HelperUtility.IsNotNull(address)&& !string.IsNullOrEmpty(address.PostalCode) && address.PostalCode.Length > 4)
            {
                address.PostalCode = address.PostalCode.Substring(0, 5);
            }

            return address;
        }
        #endregion

        #region Protected Methods
        //Get User address list and add into cache.
        protected override void GetUserAddressList(AddressListViewModel addressViewModel, string cacheKey, int accountId)
        {
            FilterCollection filters = new FilterCollection();
            if (accountId > 0)
            {
                string accountStoreId = "0";
                if (addressViewModel?.RoleName?.ToLower() == "user")
                {
                    accountStoreId = !string.IsNullOrEmpty(GetUserViewModelFromSession()?.Custom1) ? GetUserViewModelFromSession()?.Custom1 : accountStoreId;

                    //Get Address List of logged in user.
                    filters.Add(new FilterTuple(ZnodeNoteEnum.UserId.ToString(), FilterOperators.Equals, addressViewModel.UserId.ToString()));
                    addressViewModel.AddressList = _webStoreAccountClient.GetUserAddressList(new ExpandCollection() { ZnodeAccountAddressEnum.ZnodeAddress.ToString(), ZnodeUserAddressEnum.ZnodeUser.ToString() }, filters)?.ToViewModel<AddressViewModel>().ToList();
                }
                filters = new FilterCollection
                {
                    //Get Address List of logged in account user.
                    { ZnodeAccountEnum.AccountId.ToString(), FilterOperators.Equals, accountId.ToString() }
                };
                List<AddressViewModel> accountAddressList = _accountClient.GetAddressList(new ExpandCollection() { ZnodeAccountAddressEnum.ZnodeAddress.ToString() }, filters, null, null, null)?.AddressList?.ToViewModel<AddressViewModel>()?.ToList();
                if (addressViewModel?.RoleName?.ToLower() == "user")
                {
                    accountAddressList = accountAddressList?.Where(x => x.IsDefaultShipping || x.IsDefaultBilling || x.Custom1 == accountStoreId)?.ToList();
                }
                if (addressViewModel?.AddressList?.Count > 0)
                {
                    foreach (AddressViewModel addressModel in accountAddressList)
                    {
                        addressViewModel.AddressList.Add(addressModel);
                    }
                }
                else
                {
                    addressViewModel.AddressList = accountAddressList;
                }
            }
            else
            {
                //Get Address List of logged in user.
                filters.Add(new FilterTuple(ZnodeNoteEnum.UserId.ToString(), FilterOperators.Equals, addressViewModel.UserId.ToString()));
                addressViewModel.AddressList = _webStoreAccountClient.GetUserAddressList(new ExpandCollection() { ZnodeAccountAddressEnum.ZnodeAddress.ToString(), ZnodeUserAddressEnum.ZnodeUser.ToString() }, filters)?.ToViewModel<AddressViewModel>().ToList();
            }
            addressViewModel.BillingAddress = addressViewModel.AddressList?.FirstOrDefault(x => x.IsDefaultBilling);
            addressViewModel.ShippingAddress = addressViewModel.AddressList?.FirstOrDefault(x => x.IsDefaultShipping);

            if (HelperUtility.IsNotNull(addressViewModel.AddressList))
            {
                Helper.AddIntoCache(addressViewModel, cacheKey, "CurrentPortalCacheDuration");

                //Save created/updated address in session.
                SaveAddressInSession(addressViewModel);
            }
        } 
        #endregion
    }
}