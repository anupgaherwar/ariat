﻿using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Znode.Api.Client.Custom.Clients.IClients;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Models;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.Maps;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;

namespace Znode.WebStore.Custom.Agents
{
    public class AriatCheckoutAgent : CheckoutAgent
    {
        #region Private Variables
        private readonly ICartAgent _cartAgent;
        private readonly IUserClient _userClient;
        private readonly IPaymentAgent _paymentAgent;
        private readonly IAriatOrderClient _ariatOrderClient;
        #endregion

        #region Constructor
        public AriatCheckoutAgent(IShippingClient shippingsClient, IPaymentClient paymentClient, IPortalProfileClient profileClient, ICustomerClient customerClient, IUserClient userClient, IAriatOrderClient orderClient, IAccountClient accountClient, IWebStoreUserClient webStoreAccountClient, IPortalClient portalClient, IShoppingCartClient shoppingCartClient, IAddressClient addressClient)
          : base(shippingsClient, paymentClient, profileClient, customerClient, userClient, orderClient, accountClient,
                 webStoreAccountClient, portalClient, shoppingCartClient, addressClient)
        {
            _cartAgent = new CartAgent(GetClient<ShoppingCartClient>(), GetClient<PublishProductClient>(), GetClient<AccountQuoteClient>(), GetClient<UserClient>());
            _paymentAgent = new PaymentAgent(GetClient<PaymentClient>(), GetClient<OrderClient>());
            _userClient = GetClient<IUserClient>(userClient);
            _ariatOrderClient = orderClient;
        }
        #endregion

        #region public method
        public override OrdersViewModel SubmitOrder(SubmitOrderViewModel submitOrderViewModel)
        {
            ShoppingCartModel cartModel = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey) ??
                      _cartAgent.GetCartFromCookie();
            /*custom1 to custom5 model mapping*/
            cartModel.Custom1 = submitOrderViewModel.Custom1;
            //cartModel.Custom2 = submitOrderViewModel.Custom2;
            cartModel.Custom3 = submitOrderViewModel.Custom3;
            cartModel.Custom4 = submitOrderViewModel.Custom4;
            cartModel.Custom5 = submitOrderViewModel.Custom5;
            return base.SubmitOrder(submitOrderViewModel);
        }

        public override GatewayResponseModel GetPaymentResponse(ShoppingCartModel cartModel, SubmitOrderViewModel submitOrderViewModel)
        {
            // Map shopping Cart model and submit Payment view model to Submit payment model 
            SubmitPaymentModel model = PaymentViewModelMap.ToModel(cartModel, submitOrderViewModel);

            model.Custom1 = submitOrderViewModel.Custom1;
            model.Custom2 = submitOrderViewModel.Custom2;
            model.Custom3 = cartModel.CurrencyCode;

            // Map Customer Payment Guid for Save Credit Card 
            if (!string.IsNullOrEmpty(submitOrderViewModel.CustomerGuid) && string.IsNullOrEmpty(cartModel.UserDetails.CustomerPaymentGUID))
            {
                UserModel userModel = _userClient.GetUserAccountData(submitOrderViewModel.UserId);
                userModel.CustomerPaymentGUID = submitOrderViewModel.CustomerGuid;
                _userClient.UpdateCustomerAccount(userModel);

                UserViewModel userViewModel = GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey);

                if (string.IsNullOrEmpty(userViewModel.CustomerPaymentGUID))
                {
                    userViewModel.CustomerPaymentGUID = submitOrderViewModel.CustomerGuid;
                    SaveInSession(WebStoreConstants.UserAccountKey, userViewModel);
                }
            }

            model.Total = _paymentAgent.GetOrderTotal();

            return _paymentAgent.ProcessPayNow(model);
        }

        /*Generate OrderNumber Start*/
        public override string GenerateOrderNumber(int portalId) =>
             _ariatOrderClient.GenerateOrderNumber(portalId);
        /*Generate OrderNumber End*/

        protected override OrdersViewModel ProcessCreditCardPayment(SubmitOrderViewModel submitOrderViewModel, ShoppingCartModel cartModel)
        {
            SetUsersPaymentDetails(submitOrderViewModel.PaymentSettingId, cartModel);
            submitOrderViewModel.PaymentType = cartModel?.Payment?.PaymentName;
            GatewayResponseModel gatewayResponse = GetPaymentResponse(cartModel, submitOrderViewModel);
            if (gatewayResponse?.HasError ?? true || string.IsNullOrEmpty(gatewayResponse?.Token))
            {
                //RemoveInSession(WebStoreConstants.CartModelSessionKey);
                return (OrdersViewModel)GetViewModelWithErrorMessage(new OrdersViewModel(), !string.IsNullOrEmpty(gatewayResponse?.ErrorMessage) ? gatewayResponse.ErrorMessage : WebStore_Resources.ErrorProcessPayment);
            }

            //Map payment token
            cartModel.Token = gatewayResponse.Token;
            cartModel.IsGatewayPreAuthorize = gatewayResponse.IsGatewayPreAuthorize;

            if (!string.IsNullOrEmpty(gatewayResponse.Custom1))
            {
                var keyValuePairs = JsonConvert.DeserializeObject<Dictionary<string, string>>(gatewayResponse.Custom1);
                cartModel.CardType = keyValuePairs["cardtype"];
                cartModel.CreditCardExpMonth = Convert.ToInt32(keyValuePairs["expirymonth"]);
                cartModel.CreditCardExpYear = Convert.ToInt32(keyValuePairs["expiryyear"]);
                cartModel.Custom3 = gatewayResponse.Custom1;
            }
            return new OrdersViewModel();
        }

        public override OrdersViewModel GetOrderViewModel(int omsOrderId)
        {
            OrderModel orderModel = _orderClient.GetOrderReceiptByOrderId(omsOrderId);

            if (orderModel?.OmsOrderId > 0)
            {
                List<OrderLineItemModel> orderLineItemListModel = new List<OrderLineItemModel>();

                //Create new order line item model.
                CreateSingleOrderLineItem(orderModel, orderLineItemListModel);

                orderModel.OrderLineItems = orderLineItemListModel;
            }
            OrdersViewModel viewModel = orderModel?.ToViewModel<OrdersViewModel>();
            int userId = orderModel.IsQuoteOrder ? orderModel.UserId : GetUserUserIdFromSession();

            if (IsNotNull(viewModel))
            {
                UserViewModel userViewModel = GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey) ?? new UserViewModel();
                if (userId <= 0 && userViewModel?.GuestUserId <= 0)
                {
                    userViewModel.GuestUserId = viewModel.UserId;
                    SaveInSession(WebStoreConstants.UserAccountKey, userViewModel);
                }

                if (userId > 0)
                {

                    userViewModel.Custom5 = orderModel.Custom2;
                    SaveInSession(WebStoreConstants.UserAccountKey, userViewModel);
                    orderModel.Custom1 = null;
                }


                if ((userViewModel.GuestUserId == viewModel.UserId || viewModel.UserId == userId) && viewModel.OrderLineItems?.Count() > 0)
                {
                    //Order Receipt
                    string trackingUrl = GetTrackingUrlByShippingId(orderModel.ShippingId);
                    viewModel.TrackingNumber = SetTrackingUrl(orderModel.TrackingNumber, trackingUrl);
                    viewModel.CurrencyCode = PortalAgent.CurrentPortal?.CurrencyCode;
                    viewModel.CultureCode = PortalAgent.CurrentPortal?.CultureCode;
                    viewModel.CouponCode = viewModel.CouponCode?.Replace("<br/>", ", ");
                    viewModel?.OrderLineItems?.ForEach(item =>
                    {
                        item.UOM = orderModel?.ShoppingCartModel?.ShoppingCartItems?.FirstOrDefault(x => x.SKU == item.Sku)?.UOM;
                        item.TrackingNumber = SetTrackingUrl(item.TrackingNumber, trackingUrl);
                    });

                    int count = 0;
                    StringBuilder cjURL = new StringBuilder();
                    //Append line item sku, quantity and amount to url.
                    foreach (OrderLineItemViewModel orderDetail in viewModel.OrderLineItems)
                    {
                        count++;
                        cjURL.Append($"&ITEM{count}={orderDetail.Sku}");
                        cjURL.Append($"&AMT{count}={orderDetail.Price}");
                        cjURL.Append($"&QTY{count}={orderDetail.Quantity}");
                    }
                    viewModel.OrderLineItemQueryString = cjURL.ToString();
                    return viewModel;
                }
            }
            return null;
        }

        protected override void SetUsersPaymentDetails(int paymentSettingId, ShoppingCartModel model, bool isRequiredExpand = false)
        {
            PaymentSettingModel paymentSetting = isRequiredExpand ? _paymentClient.GetPaymentSetting(paymentSettingId, false, new ExpandCollection { ZnodePaymentSettingEnum.ZnodePaymentType.ToString() }, PortalAgent.CurrentPortal.PortalId) : _paymentAgent.GetPaymentSetting(paymentSettingId, PortalAgent.CurrentPortal.PortalId)?.ToModel<PaymentSettingModel>();

            string paymentName = string.Empty;
            if (IsNotNull(paymentSetting))
            {
                paymentName = paymentSetting.PaymentTypeName;
            }

            model.Payment = PaymentViewModelMap.ToPaymentModel(model, paymentSetting, paymentName);

            if (model.GiftCardAmount > 0)
            {
                if (!string.IsNullOrEmpty(model.Payment.PaymentDisplayName))
                    model.Payment.PaymentDisplayName = model.Payment.PaymentDisplayName + " & Allowance";
                else
                    model.Payment.PaymentDisplayName = "Allowance";
            }

        }
        #endregion


    }
}
