﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Xml;

using Znode.Api.Client.Custom.Clients;
using Znode.Custom.Api.Model;
using Znode.Custom.Api.Model.ParameterModel;
using Znode.Custom.Engine.WebStore;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.Helpers;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;
using Znode.WebStore.Custom.Agents.IAgents;
using Znode.WebStore.Custom.ViewModel;

namespace Znode.WebStore.Custom.Agents.Agents
{
    public class AriatProductAgent : ProductAgent, IAriatProductAgent
    {
        #region Private variables
        private readonly ICartAgent _cartAgent;
        private readonly IAriatReviewClient _reviewsClient;
        private readonly IDecorationPriceClient _decorationPriceClient;
        private readonly IAriatProductClient _ariatproductClient;
        private readonly IPublishProductClient _productClient;
        #endregion

        #region Constructor

        public AriatProductAgent(ICustomerReviewClient reviewClient,
            IPublishProductClient productClient,
            IWebStoreProductClient webstoreProductClient,
            ISearchClient searchClient, IHighlightClient highlightClient,
            IPublishCategoryClient publishCategoryClient, IAriatReviewClient reviewsClient, IDecorationPriceClient decorationPriceClient, IAriatProductClient AriatproductClient)
            : base(
                reviewClient, productClient, webstoreProductClient, searchClient, highlightClient, publishCategoryClient)
        {
            _reviewsClient = reviewsClient;
            _decorationPriceClient = GetClient<IDecorationPriceClient>(decorationPriceClient);
            _ariatproductClient = GetClient<IAriatProductClient>(AriatproductClient);
            _productClient = GetClient<IPublishProductClient>(productClient);
            _cartAgent = new CartAgent(GetClient<ShoppingCartClient>(), GetClient<PublishProductClient>(), GetClient<AccountQuoteClient>(), GetClient<UserClient>());

        }
        #endregion

        #region Constants and variables
        const string YoutubeEmbededUrl = "http://www.youtube.com/embed/";
        const string ProductStatistics = "ProductStatistics";
        const string ContentId = "ContentId";
        const string FeedbackType = "FeedbackType";
        const string ContentType = "ContentType";
        const string Review = "review";
        GlobalAttributeEntityDetailsModel globalAttribute = PortalAgent.CurrentPortal.GlobalAttributes;
        private object DecorationPriceModel;
        #endregion

        #region Public Methods
        //Get the Bazaar Voice Data from sku
        public BVModel GetBazarVoiceData(string sku)
        {
            BVModel model = new BVModel();
            try
            {
                //var globalAttribute = PortalAgent.CurrentPortal.GlobalAttributes;
                string bvUrl = globalAttribute?.Attributes?.FirstOrDefault(o => o.AttributeCode == "BVUrl")?.AttributeValue;
                string apiVersion = globalAttribute?.Attributes?.FirstOrDefault(o => o.AttributeCode == "ApiVersion")?.AttributeValue;
                string passKey = globalAttribute?.Attributes?.FirstOrDefault(o => o.AttributeCode == "PassKey")?.AttributeValue;

                string restUrl = "/submitreview.json?";
                List<string> parameters = new List<string>
            {
                "ApiVersion",
                "ProductId",
                "PassKey"
            };
                bvUrl = GetUrl(sku, bvUrl, apiVersion, passKey, restUrl, parameters);
                string response = _reviewsClient.GetBazaarVoiceData(bvUrl);
                model = JsonToModel(response);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
            }
            return model;
        }

        //Get the review list from sku.
        public BVReviewListModel GetReviewList(string sku, int offset = 0, string sort = "")
        {
            BVReviewListModel bVReviewListModel = new BVReviewListModel();
            try
            {
                string restUrl = "/reviews.json?";
                string bvUrl = GetApiPassKeyUrl(restUrl, string.Empty);
                sort = sort.Contains(",") ? sort.Replace(',', ':') : string.Empty;
                string limit = PortalAgent.CurrentPortal.GlobalAttributes?.Attributes?.FirstOrDefault(o => o.AttributeCode == "Limit")?.AttributeValue;
                limit = string.IsNullOrEmpty(limit) ? "10" : limit;
                string endpoint = "&Filter=ProductId:{0}&Limit={1}&Offset={2}&Include=Products,Authors&Stats=Reviews&Sort={3}";
                endpoint = string.Concat(bvUrl, endpoint);
                endpoint = string.Format(endpoint, sku, limit, offset, sort);
                string xmlstr = _reviewsClient.GetBazaarVoiceData(endpoint);
                BVReviewListModel dynamicData = JsonConvert.DeserializeObject<BVReviewListModel>(xmlstr);

                foreach (Result item in dynamicData.Results)
                {
                    if (!(item.SubmissionTime == DateTime.MinValue))
                        item.SubmissionTimeAsString = AriatTimeStampHelper.TimeAgo(item.SubmissionTime);
                    if (item.Videos.Count > 0)
                    {
                        foreach (Videos videos in item.Videos)
                            videos.EmbededUrl = string.Concat(YoutubeEmbededUrl, videos.VideoId);
                    }

                }
                return dynamicData;

            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
            }
            return bVReviewListModel;
        }

        //Get the main url of BV.
        public string GetApiPassKeyUrl(string restUrl, string sku)
        {
            string bvUrl, apiVersion, passKey;
            bvUrl = globalAttribute?.Attributes?.FirstOrDefault(o => o.AttributeCode == "BVUrl")?.AttributeValue;
            apiVersion = globalAttribute?.Attributes?.FirstOrDefault(o => o.AttributeCode == "ApiVersion")?.AttributeValue;
            passKey = globalAttribute?.Attributes?.FirstOrDefault(o => o.AttributeCode == "PassKey")?.AttributeValue;
            List<string> parameters = new List<string>
                  {
                "ApiVersion",
                "PassKey"
                  };
            bvUrl = GetUrl(sku, bvUrl, apiVersion, passKey, restUrl, parameters);
            return bvUrl;
        }

        //Get the product rating from the comma separated skus. 
        public List<BVRatingCount> GetAllProductRating(string skus)
        {

            List<BVRatingCount> bVRatingCounts = new List<BVRatingCount>();
            try
            {
                string restUrl = "/statistics.xml?";
                string bvUrl = GetApiPassKeyUrl(restUrl, string.Empty);
                string endpoint = "&filter=productid:{0}&stats=Reviews";
                endpoint = string.Concat(bvUrl, endpoint);
                endpoint = string.Format(endpoint, skus);
                string xmlstr = _reviewsClient.GetBazaarVoiceData(endpoint);
                GetProductRating(bVRatingCounts, xmlstr);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
            }
            return bVRatingCounts;
        }

        //Get the photo urls
        public string GetPhotoUrl(XmlNode nodes)
        {
            string thumb = nodes["Sizes"] == null ? string.Empty : nodes["Sizes"].ChildNodes[1].Attributes["id"].Value;
            if (thumb == "thumbnail")
                thumb = nodes["Sizes"].ChildNodes[1].Attributes["url"].Value;
            return thumb;
        }

        //Get the photo captions
        public string GetPhotoCaption(XmlNode nodes)
        {
            return nodes["Caption"] == null ? string.Empty : nodes["Caption"].InnerText;
        }

        //Form the Bazaar Voice urls.
        public string GetUrl(string sku, string bvUrl, string apiVersion, string passKey, string restUrl, List<string> parameters)
        {
            try
            {
                int count = 0;
                string concatUrl = "";
                foreach (string item in parameters)
                {
                    string parameter = string.Concat(item, "={");
                    string counter = Convert.ToString(count);
                    parameter = string.Concat(parameter, counter);
                    parameter = string.Concat(parameter, "}&");
                    concatUrl = string.Concat(concatUrl, parameter);
                    count++;
                }
                concatUrl = concatUrl.Remove(concatUrl.Length - 1, 1);
                concatUrl = string.Concat(restUrl, concatUrl);
                if (!string.IsNullOrEmpty(sku))
                    concatUrl = string.Format(concatUrl, apiVersion, sku, passKey);
                else
                    concatUrl = string.Format(concatUrl, apiVersion, passKey);
                bvUrl = string.Concat(bvUrl, concatUrl);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
            }
            return bvUrl;
        }

        //Convert video string to Model.
        public BVVideoModel ToModel(string videoString)
        {
            BVVideoModel bVVideoModel = new BVVideoModel();
            try
            {
                if (!string.IsNullOrEmpty(videoString))
                {
                    string[] videoStringArray = videoString.Split(',');
                    bVVideoModel.Video = videoStringArray[0].Split('[')[1];
                    bVVideoModel.VideoName = videoStringArray[1];
                    bVVideoModel.VideoURL = videoStringArray[2];
                    bVVideoModel.VideoCaption = videoStringArray[3].Split(']')[0];
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
            }
            return bVVideoModel;
        }

        //Get the rating count for comma separated skus.
        public virtual BVRatingCount GetRatingCount(string sku)
        {
            BVRatingCount bVRatingCount = new BVRatingCount();
            try
            {
                string restUrl = "/reviews.xml?";
                string bvUrl = GetApiPassKeyUrl(restUrl, string.Empty);
                string limit = PortalAgent.CurrentPortal.GlobalAttributes?.Attributes?.FirstOrDefault(o => o.AttributeCode == "Limit")?.AttributeValue;
                limit = string.IsNullOrEmpty(limit) ? "10" : limit;
                string endpoint = "&Filter=ProductId:{0}&Limit={1}&Offset=0&Include=Products,Authors&Stats=Reviews";
                endpoint = string.Concat(bvUrl, endpoint);
                endpoint = string.Format(endpoint, sku, limit);
                string xmlstr = _reviewsClient.GetBazaarVoiceData(endpoint);
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(xmlstr);
                XmlNodeList xmlNodeList = xml.GetElementsByTagName("ReviewStatistics");
                int counter = 1;
                foreach (XmlNode node in xmlNodeList)
                {
                    if (counter == 1)
                    {
                        GetRatingFromNode(bVRatingCount, node);
                        return bVRatingCount;
                    }
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
            }
            return bVRatingCount;
        }

        //Get the key value pair for form fields.
        public List<KeyValuePair<string, object>> GetKeyValue(SubmitBVModel submitBVModel)
        {
            return new List<KeyValuePair<string, object>>(){
                new KeyValuePair<string, object>("rating", submitBVModel.Rating) ,
                new KeyValuePair<string, object>("usernickname", submitBVModel.UserNickname),
                new KeyValuePair<string, object>("isrecommended", submitBVModel.IsRecommedToFriend),
                new KeyValuePair<string, object>("reviewtext", submitBVModel.Review),
                new KeyValuePair<string, object>("title", submitBVModel.ReviewTitle),
                new KeyValuePair<string, object>("useremail", submitBVModel.Email),
                new KeyValuePair<string, object>("photourl_1", submitBVModel.PhotoUrl_1),
                new KeyValuePair<string, object>("photourl_2", submitBVModel.PhotoUrl_2),
                new KeyValuePair<string, object>("photourl_3", submitBVModel.PhotoUrl_3),
                new KeyValuePair<string, object>("photourl_4", submitBVModel.PhotoUrl_4),
                new KeyValuePair<string, object>("photourl_5", submitBVModel.PhotoUrl_5),
                new KeyValuePair<string, object>("photourl_6", submitBVModel.PhotoUrl_6),
                new KeyValuePair<string, object>("photocaption_1", submitBVModel.PhotoCaption_1),
                new KeyValuePair<string, object>("photocaption_2", submitBVModel.PhotoCaption_2),
                new KeyValuePair<string, object>("photocaption_3", submitBVModel.PhotoCaption_3),
                new KeyValuePair<string, object>("photocaption_4", submitBVModel.PhotoCaption_4),
                new KeyValuePair<string, object>("photocaption_5", submitBVModel.PhotoCaption_5),
                new KeyValuePair<string, object>("photocaption_6", submitBVModel.PhotoCaption_6),
                new KeyValuePair<string, object>("userlocation", submitBVModel.UserLocation),
                new KeyValuePair<string, object>("videourl_1", submitBVModel.VideoUrl_1),
                new KeyValuePair<string, object>("videocaption_1", submitBVModel.VideoCaption_1)
            };
        }

        //Post the Image url to BV.
        public async Task<string> GetPostUrl()
        {
            string restUrl = "/uploadphoto.json?";
            string bvUrl = PortalAgent.CurrentPortal.GlobalAttributes?.Attributes?.FirstOrDefault(o => o.AttributeCode == "BVUrl")?.AttributeValue;
            string apiVersion = PortalAgent.CurrentPortal.GlobalAttributes?.Attributes?.FirstOrDefault(o => o.AttributeCode == "ApiVersion")?.AttributeValue;
            string passKey = PortalAgent.CurrentPortal.GlobalAttributes?.Attributes?.FirstOrDefault(o => o.AttributeCode == "PassKey")?.AttributeValue;
            List<string> parameters = new List<string>
            {
                "ApiVersion",
                "contenttype",
                "PassKey"
            };
            int count = 0;
            string concatUrl = "";
            foreach (string item in parameters)
            {
                string parameter = string.Concat(item, "={");
                string counter = Convert.ToString(count);
                parameter = string.Concat(parameter, counter);
                parameter = string.Concat(parameter, "}&");
                concatUrl = string.Concat(concatUrl, parameter);
                count++;
            }
            concatUrl = concatUrl.Remove(concatUrl.Length - 1, 1);
            concatUrl = string.Concat(restUrl, concatUrl);
            concatUrl = string.Format(concatUrl, apiVersion, "Review", passKey);
            bvUrl = string.Concat(bvUrl, concatUrl);
            return bvUrl;
        }

        //Get Image Url from Baazar voice
        public async Task<string> GetImageUrl(string imageUrls, HttpPostedFileBase files, string photoUploadUrl)
        {
            try
            {
                imageUrls = await _reviewsClient.GetImageURL(imageUrls, files, photoUploadUrl);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
            }
            return imageUrls;
        }

        //Submit Review form.
        public string SubmitForm(SubmitBVModel submitBVModel)
        {
            string data = "";
            try
            {
                List<KeyValuePair<string, object>> postParams = GetKeyValue(submitBVModel);

                string restUrl = "/submitreview.json?";
                string bvUrl = GetApiPassKeyUrl(restUrl, string.Empty);
                string endPoint = "&ProductId={0}&action=submit";
                endPoint = string.Concat(bvUrl, endPoint);
                endPoint = string.Format(endPoint, submitBVModel.ProductSKU);
                var formString = string.Join("&", postParams.Select(x => string.Format("{0}={1}", x.Key, x.Value)));
                string errorCode = string.Empty;
                bool isError = false;
                //Encode form string to bytes
                data = _reviewsClient.SubmitBazaarVoiceForms(endPoint, formString);
                //JSONNode data1 = JSON.Parse(data);
                data = GetError(errorCode, isError, data);
                return data;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                return data;
            }
        }

        //Get the error if any in submit form.
        public string GetError(string errorCode, bool isError, string data)
        {

            JObject jobject = JObject.Parse(data);
            string[] formError = jobject.SelectTokens("FormErrors").Select(x => x.ToString()).ToArray();
            if (formError.Count() > 0)
            {
                string error = formError[0];
                if (error == "{}")
                    isError = false;
                else
                    isError = true;
            }
            if (isError)
            {
                errorCode = TryGetValue(jobject, "FormErrors", "FieldErrors");
                errorCode = errorCode.Split(',')[2].Split(':')[1];
            }
            if (errorCode.Contains(ZnodeCustom_Resources.ERROR_FORM_DUPLICATE))
                data = "Nickname is duplicate";
            else if (errorCode.Contains(ZnodeCustom_Resources.ERROR_))
                data = ZnodeCustom_Resources.BVFormErrorMessage;
            else
                data = "false";
            return data;
        }

        //Submit the vote postive or negative.
        public string SubmitHelpfulVote(string helpfullnesstype, string contentId)
        {
            string result = "";
            try
            {
                string restUrl = "/submitfeedback.json?";
                string bvUrl = GetApiPassKeyUrl(restUrl, string.Empty);
                string enpoint = GetFeebackAndReportUrls();
                List<KeyValuePair<string, object>> postParams = new List<KeyValuePair<string, object>>();
                postParams.Add(new KeyValuePair<string, object>(ContentId, contentId));
                postParams.Add(new KeyValuePair<string, object>("Vote", helpfullnesstype));
                postParams.Add(new KeyValuePair<string, object>(FeedbackType, "helpfulness"));
                postParams.Add(new KeyValuePair<string, object>(ContentType, Review));
                var formString = string.Join("&", postParams.Select(x => string.Format("{0}={1}", x.Key, x.Value)));
                string data = _reviewsClient.SubmitBazaarVoiceForms(enpoint, formString);
                dynamic dynamicData = JsonConvert.DeserializeObject(data);
                result = dynamicData?.HasErrors;
                if (string.IsNullOrEmpty(result))
                    result = "false";
                return result;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                return result;
            }

        }

        public string SubmitReportInappropriate(string reportType, string contentId)
        {
            string result = "";
            try
            {
                string enpoint = GetFeebackAndReportUrls();
                List<KeyValuePair<string, object>> postParams = new List<KeyValuePair<string, object>>();
                postParams.Add(new KeyValuePair<string, object>(ContentId, contentId));
                postParams.Add(new KeyValuePair<string, object>(FeedbackType, reportType));
                postParams.Add(new KeyValuePair<string, object>(ContentType, Review));
                var formString = string.Join("&", postParams.Select(x => string.Format("{0}={1}", x.Key, x.Value)));
                string data = _reviewsClient.SubmitBazaarVoiceForms(enpoint, formString);
                dynamic dynamicData = JsonConvert.DeserializeObject(data);
                result = dynamicData?.HasErrors;
                if (string.IsNullOrEmpty(result))
                    result = "false";
                return result;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                return result;
            }

        }

        //Get decoration price details
        public DecorationPriceDetailsViewModel GetDecorationPrice(PriceParameterViewModel viewModel)
        {
            List<LocationDetails> json = JsonConvert.DeserializeObject<List<LocationDetails>>(viewModel.CustomizedJson);

            PriceParameterModel priceParameterModel = MapPriceRequestParameter(viewModel);

            DecorationPriceModel decorationPriceModel = _decorationPriceClient.GetDecorationPrice(priceParameterModel);

            DecorationPriceDetailsViewModel decorationPriceDetailsViewModel = MapPriceDetails(viewModel, decorationPriceModel, json);

            return decorationPriceDetailsViewModel;
        }

        //map price details
        private DecorationPriceDetailsViewModel MapPriceDetails(PriceParameterViewModel viewModel, DecorationPriceModel decorationPriceModel, List<LocationDetails> locationDetails)
        {
            string currencyCode = PortalAgent.CurrentPortal.CultureCode;

            DecorationPriceDetailsViewModel decorationPriceDetailsViewModel = new DecorationPriceDetailsViewModel { LocationDetails = new List<LocationPriceDetailViewModel>() };

            decorationPriceDetailsViewModel.Name = decorationPriceModel.Name;
            decorationPriceDetailsViewModel.ProductTotalPrice = FormatPriceWithCurrency(decorationPriceModel.UnitProductPrice * viewModel.Quantity, currencyCode);

            foreach (var item in locationDetails)
            {
                LocationPriceDetailViewModel decorationLocationPriceDetailViewModel = new LocationPriceDetailViewModel();
                decorationLocationPriceDetailViewModel.DecorationLocation = $"Customization Location: {item.ViewName}";

                decimal locationUnitPrice = 0;
                if (item.Text?.Count > 0)
                {
                    decimal textUnitPrice = decorationPriceModel.UnitTextPrice * item.Text.FirstOrDefault().NoOfLines;
                    decorationLocationPriceDetailViewModel.Text = item.Text.FirstOrDefault().Text;
                    locationUnitPrice = locationUnitPrice + textUnitPrice;
                    decorationLocationPriceDetailViewModel.TextUnitPriceWithCurrency = FormatPriceWithCurrency(textUnitPrice, currencyCode);
                    decorationLocationPriceDetailViewModel.TextTotalPriceWithCurrency = FormatPriceWithCurrency(textUnitPrice * viewModel.Quantity, currencyCode);
                }

                if (item.Image?.Count > 0)
                {
                    decimal logoUnitPrice = decorationPriceModel.UnitRegularPrice;
                    if (item.IsLargeLocation)
                        logoUnitPrice = decorationPriceModel.UnitLargeLogoPrice;

                    decorationLocationPriceDetailViewModel.Logo = item.Image.FirstOrDefault().ImageName;

                    locationUnitPrice = locationUnitPrice + logoUnitPrice;

                    decorationLocationPriceDetailViewModel.LogoUnitPriceWithCurrency = FormatPriceWithCurrency(logoUnitPrice, currencyCode);
                    decorationLocationPriceDetailViewModel.LogoTotalPriceWithCurrency = FormatPriceWithCurrency(logoUnitPrice * viewModel.Quantity, currencyCode);
                }

                decorationLocationPriceDetailViewModel.LocationUnitPrice = locationUnitPrice;
                decorationPriceDetailsViewModel.LocationDetails.Add(decorationLocationPriceDetailViewModel);
            }

            decimal productTotalPrice = (decorationPriceDetailsViewModel.LocationDetails.Sum(x => x.LocationUnitPrice) + decorationPriceModel.UnitProductPrice) * viewModel.Quantity;


            decorationPriceDetailsViewModel.TotalPrice = FormatPriceWithCurrency(productTotalPrice, currencyCode);
            return decorationPriceDetailsViewModel;
        }

        //Map decoration price request parameter
        private PriceParameterModel MapPriceRequestParameter(PriceParameterViewModel viewModel)
        {
            PortalViewModel currentPortal = PortalAgent.CurrentPortal;

            PriceParameterModel priceParameterModel = new PriceParameterModel();
            priceParameterModel.SKU = viewModel.SKU;
            priceParameterModel.Quantity = viewModel.Quantity;
            priceParameterModel.LogoQuantity = (viewModel.RegularLogoCount * viewModel.Quantity);
            priceParameterModel.LargeLogoQuantity = (viewModel.LargeLogoCount * viewModel.Quantity);
            priceParameterModel.TextLineQuantity = (viewModel.TotalTextLineCount * viewModel.Quantity);
            priceParameterModel.LargeLogoSKU = viewModel.LargeLogoSKU;
            priceParameterModel.LogoSKU = viewModel.LogoSKU;
            priceParameterModel.TextLineSKU = viewModel.TextLineSKU;
            priceParameterModel.PublishCatalogId = currentPortal.PublishCatalogId;
            priceParameterModel.PortalId = currentPortal.PortalId;
            priceParameterModel.LocaleId = currentPortal.LocaleId;
            return priceParameterModel;
        }

        //Get znode sku from artifi sku
        public ZnodeSKURequestViewModel GetZnodeSKU(string artifiSKU, Dictionary<string, string> attributes = null)
        {
            PortalViewModel portalViewModel = PortalAgent.CurrentPortal;
            ZnodeSKURequestModel model = new ZnodeSKURequestModel();
            model.SKU = artifiSKU;
            model.PortalId = portalViewModel.PortalId;
            model.LocaleId = portalViewModel.LocaleId;
            model.PublishCatalogId = portalViewModel.PublishCatalogId;
            model.Attributes = attributes;

            var publishProductSku = _ariatproductClient.GetZnodeSKU(model);

            ZnodeSKURequestViewModel znodeSKURequestViewModel = new ZnodeSKURequestViewModel();
            if (HelperUtility.IsNotNull(publishProductSku))
            {
                znodeSKURequestViewModel.SKU = publishProductSku.SKU;
                znodeSKURequestViewModel.ArtifiSKU = publishProductSku.ArtifiSKU;
                znodeSKURequestViewModel.StyleNumber = publishProductSku.StyleNumber;
            }
            return znodeSKURequestViewModel;
        }

        public override ProductViewModel GetConfigurableProduct(ParameterProductModel model)
        {
            if (model.ParentProductId > 0)
            {
                Dictionary<string, string> SelectedAttributes = GetAttributeValues(model.Codes, model.Values);

                model.LocaleId = PortalAgent.LocaleId;
                model.PublishCatalogId = GetCatalogId().GetValueOrDefault();
                model.PortalId = PortalAgent.CurrentPortal.PortalId;
                model.SelectedAttributes = SelectedAttributes;

                _productClient.SetProfileIdExplicitly(Helper.GetProfileId().GetValueOrDefault());
                PublishProductModel publishProductModel = _productClient.GetConfigurableProduct(model, GetProductExpands());

                if (HelperUtility.IsNotNull(model))
                    if (HelperUtility.IsNotNull(publishProductModel))
                    {
                        ProductViewModel viewModel = publishProductModel.ToViewModel<ProductViewModel>();
                        ParameterProductModel productAttribute = null;
                        ConfigurableAttributeViewModel configurableData = null;

                        //If Product is default configurable product.
                        if (publishProductModel.IsDefaultConfigurableProduct)
                        {
                            //Default product attribute.
                            AttributesViewModel defaultAttribute = viewModel.Attributes?.FirstOrDefault(x => x.IsConfigurable);
                            //Get parameter model.
                            productAttribute = GetConfigurableParameterModel(model.ParentProductId, model.SelectedCode, model.SelectedValue, SelectedAttributes);
                            productAttribute.PublishCatalogId = model.PublishCatalogId;
                            //Get product aatribute values.
                            configurableData = GetProductAttribute(model.ParentProductId, productAttribute,
                                                  viewModel.Attributes.Where(x => x.IsConfigurable && x.ConfigurableAttribute.Count > 0).ToList(), publishProductModel.IsDefaultConfigurableProduct);
                            //Set message id combination does not exist.
                            configurableData.CombinationErrorMessage = WebStore_Resources.ProductCombinationErrorMessage;
                            viewModel.IsDefaultConfigurableProduct = publishProductModel.IsDefaultConfigurableProduct;
                        }
                        else
                        {
                            //Get parameter model.
                            productAttribute = GetConfigurableParameterModel(model.ParentProductId, model.SelectedCode, model.SelectedValue, SelectedAttributes);
                            //Get product aatribute values.
                            productAttribute.PublishCatalogId = model.PublishCatalogId;
                            configurableData = GetProductAttribute(model.ParentProductId, productAttribute,
                            viewModel.Attributes.Where(x => x.IsConfigurable && x.ConfigurableAttribute.Count > 0).ToList(), publishProductModel.IsDefaultConfigurableProduct);
                        }

                        //Map Product configurable product data.
                        MapConfigurableProductDatas(model.ParentProductId, model.SKU, viewModel, configurableData);

                        Helper.SetProductCartParameter(viewModel);
                        return viewModel;
                    }
            }
            return new ProductViewModel { Attributes = new List<AttributesViewModel>() };
        }

        public void MapConfigurableProductDatas(int productId, string sku, ProductViewModel viewModel, ConfigurableAttributeViewModel configurableData)
        {
            viewModel.ConfigurableData = new ConfigurableAttributeViewModel();
            //Assign list of configurable attribute
            viewModel.ConfigurableData.ConfigurableAttributes = configurableData.ConfigurableAttributes;
            //List od swatch images
            viewModel.ConfigurableData.SwatchImages = configurableData.SwatchImages;
            viewModel.ConfigurableData.CombinationErrorMessage = configurableData.CombinationErrorMessage;

            string minQuantity = viewModel?.Attributes?.Value(ZnodeConstant.MinimumQuantity);
            decimal quantity = Convert.ToDecimal(string.IsNullOrEmpty(minQuantity) ? "0" : minQuantity);

            //Check any default addon product is selected or not.
            string addOnSKU = string.Empty;
            List<string> addOnProductSKU = viewModel.AddOns?.Where(x => x.IsRequired)?.Select(y => y.AddOnValues?.FirstOrDefault(x => x.IsDefault)?.SKU)?.ToList();

            if (addOnProductSKU?.Count > 0)
            {
                addOnSKU = string.Join(",", addOnProductSKU.Where(x => !string.IsNullOrEmpty(x)));
            }

            viewModel.IsCallForPricing = Convert.ToBoolean(viewModel.Attributes?.Value(ZnodeConstant.CallForPricing)) || (viewModel.Promotions?.Any(x => x.PromotionType?.Replace(" ", "") == ZnodeConstant.CallForPricing)).GetValueOrDefault();

            //Check Product Inventory
            CheckInventory(viewModel, quantity);

            if (!string.IsNullOrEmpty(addOnSKU))
                //Check Associated addon inventory.
                CheckAddOnInvenTory(viewModel, addOnSKU, quantity);

            GetProductFinalPrice(viewModel, viewModel.AddOns, quantity, addOnSKU);

            if (HelperUtility.IsNull(viewModel.ProductPrice))
            {
                viewModel.ShowAddToCart = false;
                viewModel.InventoryMessage = WebStore_Resources.ErrorPriceNotAssociate;
            }
            SetInventoryMessage(viewModel, quantity);
            viewModel.ParentProductId = productId;
            viewModel.IsConfigurable = true;
            if (viewModel.IsDefaultConfigurableProduct)
                viewModel.ShowAddToCart = false;
        }


        public virtual  void SetInventoryMessage(ProductViewModel viewModel, decimal? quantity)
        {
            List<AttributesSelectValuesViewModel> inventorySetting = GetOutOfStockOptionsAttributeList(viewModel);
            string inventorySettingCode = inventorySetting?.FirstOrDefault()?.Code;
            decimal combinedQuantity;

            if (!ValidateMinMaxQuantity(viewModel, quantity, out combinedQuantity))
            {
                viewModel.InventoryMessage = string.Format(WebStore_Resources.WarningSelectedQuantity, viewModel.Attributes?.Value(ZnodeConstant.MinimumQuantity), viewModel.Attributes?.Value(ZnodeConstant.MaximumQuantity));
                viewModel.ShowAddToCart = false;
                return;
            }
            
            if (HelperUtility.IsNotNull(viewModel) && HelperUtility.IsNotNull(viewModel.Quantity))
            {
                bool AllowBackOrder = false;
                bool TrackInventory = false;


                if (inventorySetting?.Count > 0)
                {
                    TrackInventoryData(ref AllowBackOrder, ref TrackInventory, inventorySettingCode);
                    if (viewModel.Quantity < combinedQuantity && AllowBackOrder && TrackInventory)
                    {
                        viewModel.InventoryMessage = !string.IsNullOrEmpty(viewModel.BackOrderMessage) ? viewModel.BackOrderMessage : WebStore_Resources.TextBackOrderMessage;
                        viewModel.ShowAddToCart = true;

                        if (viewModel.InventoryMessage == viewModel.Custom2 && viewModel.Quantity <= 0 && viewModel.Custom1!=null && viewModel.Quantity < combinedQuantity)
                        {
                            viewModel.InventoryMessage = string.Empty;
                        }

                        if(viewModel.InventoryMessage == viewModel.Custom2 && viewModel.Quantity >0  && viewModel.Custom1 != null && viewModel.Quantity < combinedQuantity)
                        {
                            viewModel.Custom1 = null;
                        }
                        return;
                    }
                    viewModel.InventoryMessage = !string.IsNullOrEmpty(viewModel.InStockMessage) ? viewModel.InStockMessage : WebStore_Resources.TextInstock;                  
                    viewModel.ShowAddToCart = true;
                }
            }                  
        }

        public override ProductViewModel GetProduct(int productID)
        {
            if (productID > 0)
            {
                //set user id for profile base pricing.
                _productClient.UserId = (GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey)?.UserId).GetValueOrDefault();

                _productClient.SetPublishStateExplicitly(PortalAgent.CurrentPortal.PublishState);
                _productClient.SetLocaleExplicitly(PortalAgent.CurrentPortal.LocaleId);
                _productClient.SetDomainHeaderExplicitly(GetCurrentWebstoreDomain());
                _productClient.SetProfileIdExplicitly(Helper.GetProfileId().GetValueOrDefault());
                PublishProductModel model = _productClient.GetPublishProduct(productID, GetRequiredFilters(), GetProductExpands());
                if (HelperUtility.IsNotNull(model))
                {
                    ProductViewModel viewModel = model.ToViewModel<ProductViewModel>();

                    string minQuantity = viewModel.Attributes?.Value(ZnodeConstant.MinimumQuantity);

                    viewModel.MinQuantity = minQuantity;

                    decimal quantity = Convert.ToDecimal(string.IsNullOrEmpty(minQuantity) ? "0" : minQuantity);

                    viewModel.IsCallForPricing = Convert.ToBoolean(viewModel.Attributes?.Value(ZnodeConstant.CallForPricing)) || (model.Promotions?.Any(x => x.PromotionType?.Replace(" ", "") == ZnodeConstant.CallForPricing)).GetValueOrDefault();

                    viewModel.ProductType = viewModel.Attributes.CodeFromSelectValue(ZnodeConstant.ProductType);

                    //Check Main Product inventory
                    CheckInventory(viewModel, quantity);

                    //Check any default addon product is selected or not.
                    string addOnSKU = string.Empty;
                    List<string> addOnProductSKU = viewModel.AddOns?.Where(x => x.IsRequired)?.Select(y => y.AddOnValues?.FirstOrDefault(x => x.IsDefault)?.SKU)?.ToList();

                    if (addOnProductSKU?.Count > 0)
                    {
                        addOnSKU = string.Join(",", addOnProductSKU.Where(x => !string.IsNullOrEmpty(x)));
                    }

                    if ((!string.IsNullOrEmpty(addOnSKU) && (HelperUtility.IsNotNull(viewModel.Quantity) && viewModel.Quantity > 0)) || (!string.IsNullOrEmpty(addOnSKU) && (Equals(viewModel.ProductType, ZnodeConstant.GroupedProduct))))
                        //Check Associated addon inventory.
                        CheckAddOnInvenTory(viewModel, addOnSKU, quantity);

                    if (!viewModel.IsCallForPricing)
                        GetProductFinalPrice(viewModel, viewModel.AddOns, quantity, addOnSKU);

                    viewModel.ParentProductId = productID;
                    viewModel.IsConfigurable = HelperUtility.IsNotNull(viewModel.Attributes?.Find(x => x.ConfigurableAttribute?.Count > 0));

                    viewModel.ParentProductImageSmallPath = model?.ParentProductImageSmallPath;

                    UpdateRecentViewedProducts(viewModel);
                    AddToRecentlyViewProduct(viewModel.ConfigurableProductId > 0 ? viewModel.ConfigurableProductId : viewModel.PublishProductId);

                    Helper.SetProductCartParameter(viewModel);

                    if (viewModel.IsConfigurable) 
                       GetConfigurableValues(model, viewModel);

                    //check if the product is added in wishlist for the logged in user. If so, binds its wishlistId
                    BindProductWishListDetails(viewModel);
                    SetInventoryMessage(viewModel, quantity);

                    return viewModel;
                }
            }
            throw new ZnodeException(ErrorCodes.NotFound, WebStore_Resources.ErrorProductNotFound);
        }
     
        public override ProductViewModel GetProductPriceAndInventory(string productSKU, string quantity, string addOnIds, string parentproductSKU = "", int parentProductId = 0)
        {
            decimal selectedQuantity = 0;
            decimal.TryParse(quantity, out selectedQuantity);

            //Get Product By product sku
            _productClient.SetProfileIdExplicitly(Helper.GetProfileId().GetValueOrDefault());
            ProductViewModel productModel = _productClient.GetPublishProductBySKU(new ParameterProductModel { SKU = productSKU, ParentProductSKU = parentproductSKU, ParentProductId = parentProductId }, GetProductExpands(), GetRequiredFilters())?.ToViewModel<ProductViewModel>();

            if (!string.IsNullOrEmpty(parentproductSKU) && !string.Equals(productSKU, parentproductSKU, StringComparison.InvariantCultureIgnoreCase) && HelperUtility.IsNotNull(productModel))
            {
                string sku = productModel.SKU;
                productModel.SKU = parentproductSKU;
                productModel.ConfigurableProductSKU = productSKU;
            }

            if (HelperUtility.IsNotNull(productModel))
            {
                Helper.SetProductCartParameter(productModel);

                productModel.IsCallForPricing = Convert.ToBoolean(productModel.Attributes?.Value(ZnodeConstant.CallForPricing)) || (productModel.Promotions?.Any(x => x.PromotionType?.Replace(" ", "") == ZnodeConstant.CallForPricing)).GetValueOrDefault();

                //Check product inventory.
                CheckInventory(productModel, selectedQuantity);

                //Check Add on inventory.
                if (productModel.ShowAddToCart)
                {
                    CheckAddOnInvenTory(productModel, addOnIds, selectedQuantity);
                }

                GetProductFinalPrice(productModel, productModel.AddOns, selectedQuantity, addOnIds);
                SetInventoryMessage(productModel, selectedQuantity);
                return productModel;
            }
            else
                return new ProductViewModel();
        }

  
        #endregion

        #region Private Methods
        private void GetRatingFromNode(BVRatingCount bVRatingCount, XmlNode node)
        {
            bVRatingCount.AverageRating = node["AverageOverallRating"] == null ? 0 : Convert.ToDecimal(node["AverageOverallRating"].InnerText);
            bVRatingCount.AverageRatingPercentage = bVRatingCount.AverageRating == 0 ? 0 : Convert.ToInt32(bVRatingCount.AverageRating * 100 / 5);
            bVRatingCount.TotalReviewCount = node["TotalReviewCount"] == null ? 0 : Convert.ToInt32(node["TotalReviewCount"].InnerText);
            XmlNodeList xmlss = node["RatingDistribution"].ChildNodes;
            foreach (XmlNode nodes in xmlss)
            {
                int count = nodes["Count"] == null ? 0 : Convert.ToInt32(nodes["Count"].InnerText);
                int ratingValue = nodes["RatingValue"] == null ? 0 : Convert.ToInt32(nodes["RatingValue"].InnerText);
                int ratingValuePercentage = ((count * 100) / bVRatingCount.TotalReviewCount);
                string ratingCountPercentage = string.Join(",", Convert.ToString(count), Convert.ToString(ratingValuePercentage));
                bVRatingCount.keyValuePairs.Add(ratingValue, ratingCountPercentage);
            }
            int[] ratings = { 1, 2, 3, 4, 5 };
            foreach (int rate in ratings)
            {
                if (!bVRatingCount.keyValuePairs.ContainsKey(rate))
                    bVRatingCount.keyValuePairs.Add(rate, string.Join(",", "0", "0"));
            }
        }
        private List<AttributesSelectValuesViewModel> GetOutOfStockOptionsAttributeList(ShortProductViewModel viewModel)
     => viewModel.Attributes?.SelectAttributeList(ZnodeConstant.OutOfStockOptions);

        private void AddToRecentlyViewProduct(int productId)
        {
            if (productId > 0)
            {
                List<string> productIds = GetFromSession<List<string>>(ZnodeConstant.RecentlyViewProducts);
                if (!IsProductExistInList(productIds, Convert.ToString(productId)))
                    SetMaxRecentProductInSession(Convert.ToString(productId));
            }
        }

        private void GetConfigurableValues(PublishProductModel model, ProductViewModel viewModel)
        {
            viewModel.ConfigurableData = new ConfigurableAttributeViewModel();
            //Select Is Configurable Attributes list
            viewModel.ConfigurableData.ConfigurableAttributes = viewModel.Attributes.Where(x => x.IsConfigurable && x.ConfigurableAttribute?.Count > 0).ToList();
            //Assign select attribute values.
            viewModel.ConfigurableData.ConfigurableAttributes.ForEach(x => x.SelectedAttributeValue = new[] { x.ConfigurableAttribute?.FirstOrDefault()?.AttributeValue });
        }

        private bool IsProductExistInList(List<string> productIds, string productId)
        {
            if (productIds?.Count > 0)
                return productIds.Contains(productId) ? true : false;

            return false;
        }
        private void SetMaxRecentProductInSession(string productId)
        {
            //List of product ids from cookies
            List<string> productIds = GetFromSession<List<string>>(ZnodeConstant.RecentlyViewProducts);

            if (HelperUtility.IsNull(productIds))
                productIds = new List<string>();

            productIds.Add(productId);

            int maxItemToDisplay = /*MvcDemoConstants.MaxRecentViewItemToDisplay*/15;

            //If exceed of max limit of recently view product remove last product from list.
            if (productIds.Count > maxItemToDisplay)
                for (int count = 0; count < productIds.Count - maxItemToDisplay; count++)
                    productIds.RemoveAt(0);

            if (productIds.Count > 0)
                SaveInSession(ZnodeConstant.RecentlyViewProducts, productIds);
        }

        private void UpdateRecentViewedProducts(ProductViewModel viewModel)
        {
            RecentViewModel recentViewModel = new RecentViewModel()
            {
                ImageSmallPath = viewModel.ConfigurableProductId > 0 ? HttpUtility.UrlEncode(viewModel.ParentProductImageSmallPath) : HttpUtility.UrlEncode(viewModel.ImageSmallPath),
                PublishProductId = viewModel.ConfigurableProductId > 0 ? viewModel.ConfigurableProductId : viewModel.PublishProductId,
                SalesPrice = viewModel.SalesPrice,
                Name = viewModel.ConfigurableProductId > 0 ? viewModel.ParentConfiguarableProductName : viewModel.Name,
                SEOUrl = viewModel.SEOUrl,
                SKU = viewModel.SKU,
                ProductType = viewModel.ProductType,
                CultureCode = viewModel.CultureCode,
                PromotionalPrice = viewModel.PromotionalPrice,
                UOM = Attributes.ValueFromSelectValue(viewModel?.Attributes, ZnodeConstant.UOM),
                RetailPrice = viewModel.RetailPrice,
                Rating = viewModel.Rating,
                Attributes = viewModel.Attributes,
                Promotions = viewModel.Promotions

            };

            int maxItemToDisplay = 15;
            List<RecentViewModel> storedValues = new List<RecentViewModel>();
            try
            {
                List<RecentViewModel> recentViewProductCookie = GetFromSession<List<RecentViewModel>>("RecentViewProduct");

                if (recentViewProductCookie == null || recentViewProductCookie?.Count == 0)
                {
                    storedValues.Add(recentViewModel);
                    SaveInSession(ZnodeConstant.RecentViewProduct, storedValues);
                }
                else
                {
                    int publishProductId = viewModel.ConfigurableProductId > 0 ? viewModel.ConfigurableProductId : viewModel.PublishProductId;
                    storedValues = recentViewProductCookie;

                    if (!storedValues.Where(x => x.PublishProductId == publishProductId).Any())
                    {
                        storedValues.Insert(0, recentViewModel);
                        if (storedValues.Count() > maxItemToDisplay)
                        {
                            storedValues.RemoveAt(storedValues.Count - 1);
                        }
                        SaveInSession(ZnodeConstant.RecentViewProduct, storedValues);
                    }
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Inventory.ToString(), TraceLevel.Error);
            }
        }
        //Get Customization from session.
        public List<ShoppingCartItemModel> GetCustomizationDeatilsInEdit(string sku, string customizationId)
        {
            ShoppingCartModel cartModel = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey) ??
                      _cartAgent.GetCartFromCookie();

            List<ShoppingCartItemModel> shoppingCartItemModels = cartModel.ShoppingCartItems.Where(x => x.PersonaliseValuesDetail.Any(y => y.PersonalizeCode == "ArtifiDesignId" && y.PersonalizeValue == customizationId)).ToList();
            return shoppingCartItemModels;
        }

       

        //Validate min and max quantity of product
        private bool ValidateMinMaxQuantity(ProductViewModel viewModel, decimal? quantity, out decimal combinedQuantity)
        {
            bool result = false;
            if (HelperUtility.IsNotNull(viewModel))
            {
                decimal selectedQuantity = quantity.GetValueOrDefault();
                string sku = string.IsNullOrEmpty(viewModel.ConfigurableProductSKU) ? viewModel.SKU : viewModel.ConfigurableProductSKU;
                decimal cartQuantity = GetOrderedItemQuantity(sku);
                combinedQuantity = selectedQuantity + cartQuantity;
                if (!Equals(viewModel.ProductType, ZnodeConstant.GroupedProduct) && !HelperUtility.Between(combinedQuantity, Convert.ToDecimal(viewModel.Attributes?.Value(ZnodeConstant.MinimumQuantity)), Convert.ToDecimal(viewModel.Attributes?.Value(ZnodeConstant.MaximumQuantity)), true))
                    result = false;
                else
                    result = true;
            }
            else
            {
                combinedQuantity = quantity.GetValueOrDefault();
                result = true;
            }
            return result;
        }

        private List<AttributesSelectValuesViewModel> GetOutOfStockOptionsAttributeList(ProductViewModel viewModel)
           => viewModel.Attributes?.SelectAttributeList(ZnodeConstant.OutOfStockOptions);

        //
        private void GetProductRating(List<BVRatingCount> bVRatingCounts, string xmlstr)
        {
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(xmlstr);
            XmlNodeList xNodeList = xml.GetElementsByTagName("Statistics");
            foreach (XmlNode node in xNodeList)
            {
                BVRatingCount bVRatingCount = new BVRatingCount();
                string productid = node[ProductStatistics].ChildNodes[0].LocalName;
                string totalReviewCount = node[ProductStatistics].ChildNodes[1].ChildNodes[0].LocalName;
                string averageOverallRating = node[ProductStatistics].ChildNodes[1].ChildNodes[1].LocalName;
                if (productid == "ProductId")
                    bVRatingCount.ProductSku = node[ProductStatistics].ChildNodes[0].InnerText;
                if (totalReviewCount == "TotalReviewCount")
                    bVRatingCount.TotalReviewCount = Convert.ToInt32(node[ProductStatistics].ChildNodes[1].ChildNodes[0].InnerText);
                if (averageOverallRating == "AverageOverallRating")
                    bVRatingCount.AverageRating = Convert.ToDecimal(node[ProductStatistics].ChildNodes[1].ChildNodes[1].InnerText);
                bVRatingCount.AverageRatingPercentage = bVRatingCount.AverageRating == 0 ? 0 : Convert.ToInt32(bVRatingCount.AverageRating * 100 / 5);
                bVRatingCounts.Add(bVRatingCount);

            }
        }

        //Get error as string from JsonObject.
        private string TryGetValue(JObject user, string propertyName, string subProperty)
        {
            JToken jtoken;
            if (user.TryGetValue(propertyName, out jtoken))
            {
                JObject jobject = JObject.Parse(jtoken.ToString());
                if (jobject != null)
                {
                    jobject.TryGetValue(subProperty, out jtoken);
                }
                return jtoken.ToString();
            }
            return (string)null;
        }

        //Convert Json to model.
        private BVModel JsonToModel(string response)
        {
            BVModel bvmodel = JsonConvert.DeserializeObject<BVModel>(response);
            List<string> keysToExclude = new List<string> { "userlocationgeocode_country", "userlocationgeocode_latitude", "userlocationgeocode_longitude",
                "userlocationgeocode_region,", "isratingsonly","contextdatavalue_Familiarity","contextdatavalue_Size","sendemailalertwhenpublished","sendemailalertwhencommented" };
            foreach (var key in keysToExclude)
                bvmodel.Data.Fields.Remove(key);
            return bvmodel;
        }

        private string GetFeebackAndReportUrls()
        {
            string bvUrl = PortalAgent.CurrentPortal.GlobalAttributes?.Attributes?.FirstOrDefault(o => o.AttributeCode == "BVUrl")?.AttributeValue;
            string apiVersion = PortalAgent.CurrentPortal.GlobalAttributes?.Attributes?.FirstOrDefault(o => o.AttributeCode == "ApiVersion")?.AttributeValue;
            string passKey = PortalAgent.CurrentPortal.GlobalAttributes?.Attributes?.FirstOrDefault(o => o.AttributeCode == "PassKey")?.AttributeValue;

            string restUrl = "/submitfeedback.json?";
            List<string> parameters = new List<string>
            {
                "ApiVersion",
                "PassKey"
            };
            bvUrl = GetUrl(string.Empty, bvUrl, apiVersion, passKey, restUrl, parameters);
            return bvUrl;
        }

        //Format price with currency code
        private static string FormatPriceWithCurrency(decimal? price, string culture)
        {
            return HelperMethods.FormatPriceWithCurrency(price, culture);
        }

        private ParameterProductModel GetConfigurableParameterModel(int productId, string selectedCode, string selectedValue, Dictionary<string, string> SelectedAttributes)
        {
            ParameterProductModel productAttribute = new ParameterProductModel();
            productAttribute.ParentProductId = productId;
            productAttribute.LocaleId = PortalAgent.LocaleId;
            productAttribute.SelectedAttributes = SelectedAttributes;
            productAttribute.PortalId = PortalAgent.CurrentPortal.PortalId;
            productAttribute.SelectedCode = selectedCode;
            productAttribute.SelectedValue = selectedValue;
            return productAttribute;
        }


        #endregion
    }
}

