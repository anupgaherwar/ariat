﻿using System;
using System.Collections.Generic;
using System.Linq;

using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models;
using Znode.Engine.Core.ViewModels;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.Agents;
using Znode.Libraries.ECommerce.Utilities;

using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
namespace Znode.WebStore.Custom.Agents
{
    public class CustomCartAgent : CartAgent
    {
        #region Constructor

        public CustomCartAgent(IShoppingCartClient shoppingCartsClient, IPublishProductClient publishProductClient,
           IAccountQuoteClient accountQuoteClient, IUserClient userClient) :
           base(shoppingCartsClient, publishProductClient, accountQuoteClient, userClient)
        {

        }
        #endregion

        #region Public Method

        public override void GetSelectedAddOnProductsForAddToCart(AddToCartViewModel cartItem, string addOnSKUS = null)
        {
            string[] addOnProducts = !string.IsNullOrEmpty(addOnSKUS) ? addOnSKUS.Split(',') : cartItem.AddOnProductSKUs?.Split(',');

            List<string> newAddonSKUs = new List<string>();

            if (IsNull(cartItem.AssociatedAddOnProducts) && IsNotNull(addOnProducts))
            {
                cartItem.AssociatedAddOnProducts = new List<AssociatedProductModel>();
                for (int index = 0; index < addOnProducts.Length; index++)
                {
                    string[] addonDetails = addOnProducts[index].Split('~');
                    decimal addonQuantity = addonDetails.Length == 1 ? cartItem.Quantity : Convert.ToDecimal(addonDetails[1]) * cartItem.Quantity;
                    cartItem.AssociatedAddOnProducts.Add(new AssociatedProductModel { Sku = addonDetails[0], AddOnQuantity = addonQuantity, Quantity = cartItem.Quantity, OrderLineItemRelationshipTypeId = 1 });
                    newAddonSKUs.Add(addonDetails[0]);
                }
            }

            if (IsNotNull(cartItem.AssociatedAddOnProducts) && cartItem.AssociatedAddOnProducts.Count > 0)
            {
                for (int index = 0; index < cartItem.AssociatedAddOnProducts.Count(); index++)
                {
                    bool isNewExtIdRequired = !Equals(index, 0);
                    cartItem.ExternalId = isNewExtIdRequired ? Guid.NewGuid().ToString() : cartItem.ExternalId;
                    cartItem.AddOnProductSKUs = cartItem.AssociatedAddOnProducts[index].Sku;
                    cartItem.Quantity = cartItem.Quantity;
                    cartItem.SKU = cartItem.SKU;
                    cartItem.AddOnQuantity = cartItem.AssociatedAddOnProducts[index].AddOnQuantity;

                    if (IsNotNull(cartItem))
                        cartItem.ShoppingCartItems.Add(cartItem.ToModel<ShoppingCartItemModel>());
                }
            }
            else
                cartItem.ShoppingCartItems.Add(cartItem.ToModel<ShoppingCartItemModel>());
        }
           
        //Update quantity of cart item.
        public override AddToCartViewModel UpdateQuantityOfCartItem(string guid, string quantity, int productId)
        {
            // Get shopping cart from session.
            ShoppingCartModel shoppingCart = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey);
            decimal newQuantity = ModifyQuantityValue(quantity);
            decimal? orginalQuantity = shoppingCart.ShoppingCartItems?.FirstOrDefault(x => x.ExternalId == guid)?.Quantity;

            if (IsNotNull(shoppingCart))
            {
                //Update quantity and update the cart.
                if (productId > 0)
                {
                    shoppingCart.ShoppingCartItems?.Where(w => w.ExternalId == guid)?.Select(x => x.GroupProducts?.Where(y => y.ProductId == productId)?.Select(z => { z.Quantity = newQuantity; return z; })?.FirstOrDefault()).ToList();
                }
                else
                {
                    shoppingCart.ShoppingCartItems?.Where(w => w.ExternalId == guid)?.Select(y => { y.Quantity = Convert.ToDecimal(newQuantity.ToInventoryRoundOff()); return y; }).ToList();
                }

                ShoppingCartItemModel shoppingCartItemModel = shoppingCart.ShoppingCartItems?.FirstOrDefault(w => w.ExternalId == guid);
                foreach (AssociatedProductModel item in shoppingCartItemModel?.AssociatedAddOnProducts)
                {
                    item.Quantity = Convert.ToDecimal(newQuantity.ToInventoryRoundOff());
                    if (IsNotNull(orginalQuantity) && orginalQuantity > 0 && item.AddOnQuantity >= orginalQuantity)
                    {
                        decimal? orginalAddonQty = item.AddOnQuantity / orginalQuantity;
                        item.AddOnQuantity = Convert.ToDecimal(item.Quantity * Convert.ToDecimal(orginalAddonQty.ToInventoryRoundOff()));
                    }


                }
                //shoppingCartItemModel?.AssociatedAddOnProducts?.ForEach(x => x.Quantity = Convert.ToDecimal(newQuantity.ToInventoryRoundOff() && x=>x.AddOnQuantity = ));
                if (IsNotNull(shoppingCartItemModel))
                {
                    AddToCartViewModel addToCartViewModel = new AddToCartViewModel();
                    addToCartViewModel.CookieMappingId = GetFromCookie(WebStoreConstants.CartCookieKey);
                    addToCartViewModel.GroupId = shoppingCartItemModel.GroupId;
                    addToCartViewModel.AddOnProductSKUs = shoppingCartItemModel.AddOnProductSKUs;
                    addToCartViewModel.AutoAddonSKUs = shoppingCartItemModel.AutoAddonSKUs;
                    addToCartViewModel.SKU = shoppingCartItemModel.SKU;
                    addToCartViewModel.ParentOmsSavedcartLineItemId = shoppingCartItemModel.ParentOmsSavedcartLineItemId;
                    addToCartViewModel.AssociatedAddOnProducts = shoppingCartItemModel.AssociatedAddOnProducts;
                    if (!string.IsNullOrEmpty(shoppingCartItemModel.BundleProductSKUs))
                    {
                        addToCartViewModel.OmsSavedCartLineItemId = shoppingCartItemModel.OmsSavedcartLineItemId;
                        addToCartViewModel.Quantity = shoppingCartItemModel.Quantity;
                        GetSelectedBundleProductsForAddToCart(addToCartViewModel, shoppingCartItemModel.BundleProductSKUs);
                    }
                    else if (shoppingCartItemModel.GroupProducts?.Count > 0)
                    {
                        shoppingCartItemModel.Quantity = shoppingCartItemModel.GroupProducts.FirstOrDefault().Quantity;
                        addToCartViewModel.ShoppingCartItems.Add(shoppingCartItemModel);
                    }

                    else if (!string.IsNullOrEmpty(shoppingCartItemModel.ConfigurableProductSKUs))
                    {
                        addToCartViewModel.OmsSavedCartLineItemId = shoppingCartItemModel.OmsSavedcartLineItemId;
                        addToCartViewModel.Quantity = shoppingCartItemModel.Quantity;
                        GetSelectedConfigurableProductsForAddToCart(addToCartViewModel, shoppingCartItemModel.ConfigurableProductSKUs);
                    }
                    else if (!string.IsNullOrEmpty(shoppingCartItemModel?.AddOnProductSKUs))
                    {
                        addToCartViewModel.OmsSavedCartLineItemId = shoppingCartItemModel.OmsSavedcartLineItemId;
                        addToCartViewModel.Quantity = shoppingCartItemModel.Quantity;
                        GetSelectedAddOnProductsForAddToCart(addToCartViewModel, shoppingCartItemModel.AddOnProductSKUs);
                    }
                    else
                        addToCartViewModel.ShoppingCartItems.Add(shoppingCartItemModel);

                    AddToCartModel addToCartModel = addToCartViewModel.ToModel<AddToCartModel>();
                    addToCartModel.PublishedCatalogId = shoppingCart.PublishedCatalogId;
                    addToCartModel.Coupons = shoppingCart.Coupons;
                    addToCartModel.ZipCode = shoppingCart?.ShippingAddress?.PostalCode;
                    addToCartModel.UserId = shoppingCart.UserId;
                    addToCartModel.PortalId = shoppingCart.PortalId;
                    addToCartModel.LocaleId = shoppingCart.LocaleId;
                    addToCartModel = _shoppingCartsClient.AddToCartProduct(addToCartModel);
                    addToCartModel.ShippingId = shoppingCart.ShippingId;
                    //Save items updated quantities with sku in list.
                    UpdateQuantityItem(shoppingCartItemModel);

                    // ShoppingCartItems set null to get from cart from database in GetCart
                    shoppingCart.ShoppingCartItems = null;

                    SaveInSession(WebStoreConstants.CartModelSessionKey, shoppingCart);

                    SaveInCookie(WebStoreConstants.CartCookieKey, addToCartModel.CookieMappingId, ZnodeConstant.MinutesInAYear);

                    //To clear the session for CartCount session key.
                    ClearCartCountFromSession();
                }
            }
            return new AddToCartViewModel();
        }
        #endregion
    }
}
