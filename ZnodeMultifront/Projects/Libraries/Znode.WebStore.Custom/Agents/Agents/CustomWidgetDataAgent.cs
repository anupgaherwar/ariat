﻿using Microsoft.Ajax.Utilities;

using System.Linq;

using Znode.Engine.Api.Client;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Resources;

namespace Znode.WebStore.Custom.Agents.Agents
{
    public class CustomWidgetDataAgent : WidgetDataAgent
    {
        public CustomWidgetDataAgent(IWebStoreWidgetClient widgetClient, IPublishProductClient productClient, IPublishCategoryClient publishCategoryClient, IBlogNewsClient blogNewsClient, IContentPageClient contentPageClient, ISearchClient searchClient, ICMSPageSearchClient cmsPageSearchClient) :
            base(widgetClient, productClient, publishCategoryClient, blogNewsClient, contentPageClient, searchClient, cmsPageSearchClient)
        {
        }

        //Get product list 
        public override ProductListViewModel GetCategoryProducts(WidgetParameter widgetparameter, int pageNum = 1, int pageSize = 16, int sortValue = 0)
        {
            ProductListViewModel productList = base.GetCategoryProducts(widgetparameter, pageNum, pageSize, sortValue);

            if (HelperUtility.IsNotNull(productList) && productList.Products?.Count > 0)
            {
                foreach (var item in productList.Products)
                {
                    if (item.Attributes.FirstOrDefault(x => x.AttributeCode == "ColorSpecific")?.SelectValues.Count > 0)
                    {
                        int noOfColorAvailable = item.Attributes.FirstOrDefault(x => x.AttributeCode == "ColorSpecific").SelectValues.DistinctBy(values => values.Code).Count();

                        if (noOfColorAvailable == 1)
                        {
                            item.Custom1 = string.Format(Custom_Resources.TextColorOptions, noOfColorAvailable);
                        }
                        else if (noOfColorAvailable > 1)
                        {
                            item.Custom1 = string.Format(Custom_Resources.TextNoOfColorOptionsForMoreThanOneValue, noOfColorAvailable);
                        }
                    }

                }
            }
            return productList;
        }
    }
}
