﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;

using Znode.Engine.Api.Models;
using Znode.Engine.WebStore.Agents;
using Znode.WebStore.Custom.ViewModel;

namespace Znode.Custom.Engine.WebStore
{
    public interface IAriatProductAgent : IProductAgent
    {
        /// <summary>
        /// Get the data for the repesective sku
        /// </summary>
        /// <param name="sku">Product sku </param>
        BVModel GetBazarVoiceData(string sku);

        /// <summary>
        /// convert string to model 
        /// </summary>
        /// <param name="videoString">Video string</param>
        /// <returns></returns>
        BVVideoModel ToModel(string videoString);

        /// <summary>
        /// Get review list.
        /// </summary>
        /// <param name="sku">sku</param>
        /// <returns></returns>
        BVReviewListModel GetReviewList(string sku, int offset = 0, string sort = "");

        /// <summary>
        /// Get the rating count.
        /// </summary>
        /// <param name="sku">sku</param>
        /// <returns></returns>
        BVRatingCount GetRatingCount(string sku);

        /// <summary>
        /// Get all product ratings.
        /// </summary>
        /// <param name="skus">skus</param>
        /// <returns></returns>
        List<BVRatingCount> GetAllProductRating(string skus);

        /// <summary>
        /// Post image url
        /// </summary>
        /// <returns></returns>
        Task<string> GetPostUrl();

        /// <summary>
        /// Get the image Url from BV.
        /// </summary>
        /// <param name="imageUrls">imageUrls</param>
        /// <param name="files">files</param>
        /// <param name="photoUploadUrl">photoUploadUrl</param>
        /// <returns></returns>
        Task<string> GetImageUrl(string imageUrls, HttpPostedFileBase files, string photoUploadUrl);

        /// <summary>
        /// Submit the review form.
        /// </summary>
        /// <param name="submitBVModel">submitBVModel</param>
        /// <returns></returns>
        string SubmitForm(SubmitBVModel submitBVModel);

        /// <summary>
        /// Submit the helpful vote.
        /// </summary>
        /// <param name="helpfullnesstype">helpfullnesstype</param>
        /// <param name="contentId">contentId</param>
        /// <returns></returns>
        string SubmitHelpfulVote(string helpfullnesstype, string contentId);

        /// <summary>
        /// Submit report.
        /// </summary>
        /// <param name="reportType">reportType</param>
        /// <param name="contentId">contentId</param>
        /// <returns></returns>
        string SubmitReportInappropriate(string reportType, string contentId);
    
        /// <summary>
        /// Get Decoration price details.
        /// </summary>
        /// <param name="viewModel">Request parameter decoration.</param>
        /// <returns>Price Details</returns>
        DecorationPriceDetailsViewModel GetDecorationPrice(PriceParameterViewModel viewModel);

        /// <summary>
        /// get znode sku from artifi
        /// </summary>
        /// <param name="artifiSKU">artifi sku</param>
        /// <returns>znode sku.</returns>
        ZnodeSKURequestViewModel GetZnodeSKU(string artifiSKU, Dictionary<string, string> customization = null);
       
        /// <summary>
        /// Get Existing Customization from Session for edit mode.
        /// </summary>
        /// <param name="sku">SKU</param>
        /// <param name="customizationId">Artifi design id.</param>
        /// <returns>List of line items.</returns>
        List<ShoppingCartItemModel> GetCustomizationDeatilsInEdit(string sku, string customizationId);
    }
}
