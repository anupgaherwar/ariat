﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;

using Znode.Engine.Api.Models;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.ViewModels;
using Znode.WebStore.Custom.ViewModel;

namespace Znode.Custom.Engine.WebStore
{
    public interface IAriatUserAgent : IUserAgent
    {
        AddressViewModel GetAdyenShippingAddress();
    }
}