﻿using Znode.WebStore.Custom.ViewModel;

namespace Znode.WebStore.Custom.Agents
{
    public interface IAriatAdyenAgent
    {
        /// <summary>
        /// Get Dropin component
        /// </summary>
        /// <param name="paymentCode">Payment Code</param>
        /// <returns>Dropin veiw model</returns>
        AriatDropinViewModel GetDropin(string paymentCode);
    }
}
