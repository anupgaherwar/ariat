﻿using Microsoft.AspNet.Identity.Owin;

using System;
using System.IO;
using System.Web.Mvc;
using System.Xml.Serialization;

using Znode.Custom.Engine.WebStore;
using Znode.Engine.Api.Models;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.Models;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.SAML;
using Znode.Libraries.SAMLWeb;
using Znode.WebStore.Core.Agents;

namespace Znode.Engine.WebStore.Controllers
{
    public class CustomUserController : UserController
    {
        #region Private Read-only members
        private readonly IAuthenticationHelper _authenticationHelper;
        private readonly IUserAgent _userAgent;
        private readonly ICartAgent _cartAgent;
        private readonly IAriatUserAgent _ariatUserAgent;
             
        #endregion

        #region Public Constructor     
        public CustomUserController(IUserAgent userAgent, ICartAgent cartAgent, IAuthenticationHelper authenticationHelper, IPaymentAgent paymentAgent, IImportAgent importAgent, IFormBuilderAgent formBuilderAgent, IPowerBIAgent powerBIAgent, IAriatUserAgent ariatUserAgent) : base(userAgent, cartAgent, authenticationHelper, paymentAgent, importAgent, formBuilderAgent, powerBIAgent)
        {
            _authenticationHelper = authenticationHelper;
            _userAgent = userAgent;
            _cartAgent = cartAgent;
            _ariatUserAgent = ariatUserAgent;
        }
        #endregion

        #region Public Methods
        [AllowAnonymous]
        [HttpGet]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public override ActionResult Login(string returnUrl)
        {
            return base.Login(returnUrl);
        }

        //SAML call back 
        public ActionResult SAMLCallback(string returnUrl = "")
        {
            SessionObj sessionObj = SessionHelper.GetDataFromSession<SessionObj>(Request.Params["state"]);

            string samlResponseXML = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(sessionObj.SAMLToken));

            XmlSerializer serializer = new XmlSerializer(typeof(ResponseType));

            ResponseType response = (ResponseType)serializer.Deserialize(new StringReader(samlResponseXML));

            string username = string.Empty;
            //Find assertion
            AssertionType assertion = null;
            foreach (var item in response.Items)
            {
                if (item is AssertionType)
                {
                    assertion = item as AssertionType;
                }
            }

            //Find the username
            if (assertion != null)
            {
                foreach (var item in assertion.Subject.Items)
                {
                    if (item is NameIDType)
                    {

                        if (!string.IsNullOrEmpty(((NameIDType)item).Value))
                        {
                            username = ((NameIDType)item).Value;
                        }
                    }
                }
            }
            string id = Request.Params["id"];

            //Set the Authentication Cookie.  
            SetAuthCookie(username, id);

            return RedirectToAction<HomeController>(x => x.Index());
        }



        public override ActionResult Logout()
        {
            ImpersonationModel impersonationModel = SessionHelper.GetDataFromSession<ImpersonationModel>(WebStoreConstants.ImpersonationSessionKey);
            if (User.Identity.IsAuthenticated)
            {
                ExternalLoginInfo loginInfo = SessionHelper.GetDataFromSession<ExternalLoginInfo>(WebStoreConstants.SocialLoginDetails);

                if (HelperUtility.IsNotNull(loginInfo))
                    return Redirect(_userAgent.Logout(loginInfo));
                _userAgent.Logout();
            }
            if (Request.IsAjaxRequest())
                return Json(null, JsonRequestBehavior.AllowGet);
            if (impersonationModel != null)
            {
                return RedirectToAction<HomeController>(x => x.Index());
            }
            return RedirectToAction<UserController>(x => x.Login("/"));
        }
        [HttpGet]
        public JsonResult GetAdyenShippingAddress()
        {
            AddressViewModel addressViewModel = _ariatUserAgent.GetAdyenShippingAddress();
            if (HelperUtility.IsNotNull(addressViewModel))
            {
                return Json(new
                {
                    hasError = false,
                    streetAddress1 = addressViewModel.Address1,
                    streetAddress2 = addressViewModel.Address2,
                    streetAddress3 = addressViewModel.Address3,
                    city = addressViewModel.CityName,
                    stateCode = addressViewModel.StateCode,
                    stateName = addressViewModel.StateName,
                    postalCode = addressViewModel.PostalCode,
                    country = addressViewModel.CountryName,
                    email = addressViewModel.EmailAddress
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { hasError = true }, JsonRequestBehavior.AllowGet);
            }
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public override ActionResult Login(LoginViewModel model, string returnUrl, bool isSinglePageCheckout = false)
        {
            if (ModelState.IsValid)
            {
                model.IsWebStoreUser = true;
                //Authenticate the User Credentials.
                LoginViewModel loginViewModel = _userAgent.Login(model);
                if (!loginViewModel.HasError)
                {
                    SetDataAfterLogin(model);
                    loginViewModel.IsFromSocialMedia = false;

                    //Redirection to the Login Url.
                    _authenticationHelper.RedirectFromLoginPage(model.Username, true);

                    string url = GetReturnUrlAfterLogin(loginViewModel, returnUrl);

                    if (Request.IsAjaxRequest() && !string.IsNullOrEmpty(url))
                    {
                        if (!string.IsNullOrEmpty(loginViewModel.Custom1))
                        {
                            SetNotificationMessage(GetErrorNotificationMessage(loginViewModel.Custom1));
                        }
                        return RedirectToAction<UserController>(x => x.ReturnRedirectUrl(url));
                    }
                    if (!string.IsNullOrEmpty(url))
                        return Redirect(url);
                }
                if (Request.IsAjaxRequest())
                {
                    if (HelperUtility.IsNotNull(loginViewModel) && loginViewModel.IsResetPassword)
                    {
                        TempData[WebStoreConstants.UserName] = model.Username;
                        TempData[WebStoreConstants.CheckoutReturnUrl] = WebStoreConstants.CheckoutReturnUrl;
                        return Json(new { status = true, error = "", isresetpassword = true }, JsonRequestBehavior.AllowGet);
                    }
                    else return Json(new { status = false, error = loginViewModel.ErrorMessage }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (HelperUtility.IsNotNull(loginViewModel) && loginViewModel.IsResetPassword)
                    {
                        TempData[WebStoreConstants.UserName] = model.Username;
                        return RedirectToAction<UserController>(x => x.ResetWebstorePassword());
                    }
                    else
                        SetNotificationMessage(GetErrorNotificationMessage(loginViewModel.ErrorMessage));
                }
            }
            return View(model);
        }
        #endregion

        #region Private Methods
        private void SetDataAfterLogin(LoginViewModel loginViewModel)
        {
            //Set the Authentication Cookie.           
            _authenticationHelper.SetAuthCookie(loginViewModel.Username, loginViewModel.RememberMe);

            //Remember me.
            if (loginViewModel.RememberMe)
                SaveLoginRememberMeCookie(loginViewModel.Username);

            ShoppingCartModel cart = SessionHelper.GetDataFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey);
            TempData["CartCount"] = cart?.ShoppingCartItems?.Count;

            _cartAgent.MergeGuestUserCart();
        }
        private void SetAuthCookie(string userName, string id)
        {
            Session.Add(WebStoreConstants.UserAccountKey, new UserViewModel { UserName = userName, UserId = Convert.ToInt32(id), RoleName = "admin" });

            //Set the Authentication Cookie.           
            _authenticationHelper.SetAuthCookie(userName, true);
        } 
        #endregion
    }
}