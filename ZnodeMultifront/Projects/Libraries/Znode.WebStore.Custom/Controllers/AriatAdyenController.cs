﻿using System.Web.Mvc;

using Znode.Engine.WebStore.Controllers;
using Znode.Libraries.Resources;
using Znode.WebStore.Custom.Agents;
using Znode.WebStore.Custom.ViewModel;

namespace Znode.WebStore.Custom.Controllers
{
    public class AriatAdyenController : BaseController
    {
        #region private variables
        private readonly IAriatAdyenAgent _ariatAdyenAgent;
        #endregion

        #region constructor
        public AriatAdyenController(IAriatAdyenAgent ariatAdyenAgent)
        {
            _ariatAdyenAgent = ariatAdyenAgent;
        }
        #endregion

        #region public methods
        [HttpGet]
        public ActionResult GetDropin(string paymentCode)
        {
            AriatDropinViewModel adyenResponse = _ariatAdyenAgent.GetDropin(paymentCode);

            return Json(new
            {
                html = !adyenResponse.HasError? RenderRazorViewToString("~/Views/Themes/AriatCrew/Views/Checkout/_AriatAdyenPayment.cshtml", adyenResponse):"",
                message =adyenResponse.HasError ? "Unable to connect payment application." :"",
                status= !adyenResponse.HasError
            }, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}
