﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

using Znode.Custom.Engine.WebStore;
using Znode.Engine.Api.Models;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.ViewModels;
using Znode.WebStore.Custom.ViewModel;

namespace Znode.Engine.WebStore.Controllers
{
    public class AriatProductController : ProductController
    {
        #region Private variables
        private readonly IAriatProductAgent _ariatProductAgent;
        private readonly IProductAgent _productAgent;
        #endregion

        #region Constructor

        public AriatProductController(IProductAgent productAgent, IUserAgent userAgent, ICartAgent cartAgent, IWidgetDataAgent widgetDataAgent, IAttributeAgent attributeAgent, IRecommendationAgent recommendationAgent, IAriatProductAgent ariatProductAgent) : base(productAgent, userAgent, cartAgent, widgetDataAgent, attributeAgent, recommendationAgent)
        {
            _productAgent = productAgent;
            _ariatProductAgent = ariatProductAgent;
        }
        #endregion

        const string errorStatus = "false";

        #region Public Methods
        [HttpGet]
        //Generate Review Form
        public ActionResult WriteReviewBV(string name)
        {
            BVModel responses = _ariatProductAgent.GetBazarVoiceData(name);
            string htmlContent = string.Empty;
            htmlContent = RenderRazorViewToString("~/Views/Themes/AriatCrew/Views/BazaarVoice/WriteReviewBV.cshtml", responses);
            return Json(new
            {
                html = htmlContent,
            }, JsonRequestBehavior.AllowGet);
        }

        //Get the photo data to generate the photo popup
        public ActionResult GetPhotoDataFromBV(string name)
        {
            BVModel responses = _ariatProductAgent.GetBazarVoiceData(name);
            string htmlContent = string.Empty;
            htmlContent = RenderRazorViewToString("~/Views/Themes/AriatCrew/Views/BazaarVoice/GetPhotoDataBV.cshtml", responses);
            return Json(new
            {
                html = htmlContent,
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        // Submit Bazaar Voice Form
        public JsonResult SubmitBVForm(SubmitBVModel submitBVModel)
        {
            string result = _ariatProductAgent.SubmitForm(submitBVModel);
            if (result == errorStatus)
                return Json(new { sucess = true, HasError = false });
            else
                return Json(new { sucess = false, error = true, HasError = true, responseText = result });
        }

        [HttpPost]
        //Submit Helpful Vote
        public JsonResult SubmitHelpfulVote(string helpfullnesstype, string contentId)
        {
            string error = _ariatProductAgent.SubmitHelpfulVote(helpfullnesstype, contentId);
            if (error == errorStatus)
                return Json(new { sucess = true });
            else
                return Json(new { error = "Request Not complete" });
        }

        [HttpPost]
        //Submit the Report
        public JsonResult ReportInappropriate(string reportType, string contentId)
        {

            string error = _ariatProductAgent.SubmitReportInappropriate(reportType, contentId);
            if (error == errorStatus)
                return Json(new { sucess = true });
            else
                return Json(new { error = "Request Not complete" });
        }

        //Get the video data
        public ActionResult GetVideoDataFromBV(string videoString)
        {
            string htmlContent = string.Empty;
            BVVideoModel bVVideoModel = _ariatProductAgent.ToModel(videoString);
            htmlContent = RenderRazorViewToString("~/Views/Themes/AriatCrew/Views/BazaarVoice/GetVideoDataBV.cshtml", bVVideoModel);
            return Json(new
            {
                html = htmlContent,
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        //Get the Image Url from Bazaar voice
        public async Task<string> GetImageUrlFromBV()
        {
            string imageUrls = string.Empty;
            HttpPostedFileBase files = Request.Files["UploadedImage"];
            string bvUrl = PortalAgent.CurrentPortal.GlobalAttributes?.Attributes?.FirstOrDefault(o => o.AttributeCode == "BVUrl")?.AttributeValue;
            string photoUploadUrl = await _ariatProductAgent.GetPostUrl();
            imageUrls = await _ariatProductAgent.GetImageUrl(imageUrls, files, photoUploadUrl);
            return imageUrls;
        }

        //Get the review list from Bazaar voice
        public ActionResult GetReviewList(string sku, int offset, bool isAjax, string sort)
        {
            BVReviewListModel bVReviewListModel = _ariatProductAgent.GetReviewList(sku, offset, sort);
            string htmlContent = string.Empty;
            if (offset == 0 && isAjax)
                return PartialView("~/Views/Themes/AriatCrew/Views/BazaarVoice/_GetReviewList.cshtml", bVReviewListModel);
            else
            {
                htmlContent = RenderRazorViewToString("~/Views/Themes/AriatCrew/Views/BazaarVoice/_GetReviewList.cshtml", bVReviewListModel);
                return Json(new
                {
                    html = htmlContent,
                    listCount = bVReviewListModel.Results.Count(),
                    currentoffsetBV = bVReviewListModel.Offset,
                }, JsonRequestBehavior.AllowGet);
            }
        }

        //Get review count from sku
        public virtual ActionResult GetReviewCount(string sku)
        {
            BVRatingCount bVRatingCount = _ariatProductAgent.GetRatingCount(sku);
            return PartialView("~/Views/Themes/AriatCrew/Views/BazaarVoice/_GetRatingCount.cshtml", bVRatingCount);
        }

        [HttpPost]
        //Get the rating for comma separated skus
        public virtual JsonResult GetRatings(string skus)
        {
            List<BVRatingCount> bVRatingCounts = _ariatProductAgent.GetAllProductRating(skus.ToString());
            return Json(new { status = true, data = bVRatingCounts });
        }

        //Get decoration price details
        [HttpPost]
        public virtual JsonResult GetDecorationPrice(PriceParameterViewModel viewModel)
        {
            var priceDetails = _ariatProductAgent.GetDecorationPrice(viewModel);

            var htmlContent = RenderRazorViewToString("~/Views/Themes/AriatCrew/Views/Product/_DecorationPriceDetails.cshtml", priceDetails);
            return Json(new
            {
                html = htmlContent

            }, JsonRequestBehavior.AllowGet);
        }

        //get znode sku
        [HttpPost]
        public virtual JsonResult GetZnodeSKU(string artifiSKU, Dictionary<string, string> selectedAttributes = null)
        {
            ZnodeSKURequestViewModel znodesku = _ariatProductAgent.GetZnodeSKU(artifiSKU, selectedAttributes);

            return Json(new
            {
                sku = znodesku.SKU,
                artifiSKU = znodesku.ArtifiSKU,
                styleNumber = znodesku.StyleNumber

            }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public override ActionResult GetProductPrice(string productSKU = "", string parentProductSKU = "", string quantity = "", string addOnIds = "", int parentProductId = 0)
        {
            ProductViewModel viewModel = _productAgent.GetProductPriceAndInventory(productSKU, quantity, addOnIds, parentProductSKU, parentProductId);

            return Json(new
            {
                success = viewModel.ShowAddToCart,
                message = viewModel.InventoryMessage,
                Quantity = viewModel.Quantity,
                data = new
                {
                    style = viewModel.ShowAddToCart ? "success" : "error",
                    price = Helper.FormatPriceWithCurrency(viewModel.ProductPrice, viewModel.CultureCode),
                    totalprice = Helper.FormatPriceWithCurrency(viewModel.ProductPrice * Convert.ToInt32(quantity), viewModel.CultureCode),
                    sku = viewModel?.SKU,
                    productId = parentProductId > 0 ? parentProductId : viewModel.PublishProductId,
                    addOnMessage = viewModel?.AddOns?.Count > 0 ? viewModel.AddOns?.Select(x => x.InventoryMessage)?.FirstOrDefault() : null,
                    isOutOfStock = viewModel?.AddOns?.Count > 0 ? viewModel.AddOns?.Select(x => x.IsOutOfStock)?.FirstOrDefault() : null,
                }
            }, JsonRequestBehavior.AllowGet);
        }

        //Get Artifi Customization Details in Edit mode.
        [HttpGet]
        public virtual JsonResult GetCustomizationDeatilsInEdit(string sku, string customizeproductid)
        {
            List<ShoppingCartItemModel> lineItems = _ariatProductAgent.GetCustomizationDeatilsInEdit(sku, customizeproductid);

            return Json(new
            {
                lineItems = lineItems

            }, JsonRequestBehavior.AllowGet);
        }
        #endregion

    }
}
