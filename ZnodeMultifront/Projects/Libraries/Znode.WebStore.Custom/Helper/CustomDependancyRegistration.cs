﻿using Autofac;
using Znode.Api.Client.Custom;
using Znode.Api.Client.Custom.Clients;
using Znode.Api.Client.Custom.Clients.IClients;
using Znode.Custom.Engine.WebStore;
using Znode.Engine.Api.Client;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.Controllers;
using Znode.Libraries.Framework.Business;
using Znode.WebStore.Core.Agents.IAgent;
using Znode.WebStore.Custom;
using Znode.WebStore.Custom.Agents;
using Znode.WebStore.Custom.Agents.Agents;
using Znode.WebStore.Custom.Agents.IAgents;

namespace Znode.Engine.WebStore
{
    public class CustomDependancyRegistration : IDependencyRegistration
    {
        public virtual void Register(ContainerBuilder builder)
        {
            builder.RegisterType<CustomUserController>().As<UserController>().InstancePerDependency();
            builder.RegisterType<AriatProductController>().As<ProductController>().InstancePerDependency();
            builder.RegisterType<AriatProductAgent>().As<IAriatProductAgent>().InstancePerDependency();
            builder.RegisterType<AriatProductAgent>().As<IProductAgent>().InstancePerDependency();
            builder.RegisterType<AriatReviewClient>().As<IAriatReviewClient>().InstancePerDependency();

            builder.RegisterType<CustomWidgetDataAgent>().As<IWidgetDataAgent>().InstancePerDependency();

            builder.RegisterType<DecorationPriceClient>().As<IDecorationPriceClient>().InstancePerDependency();
            /*Adyen pay start*/
            builder.RegisterType<AriatAdyenAgent>().As<IAriatAdyenAgent>().InstancePerDependency();
            builder.RegisterType<AriatAdyenClient>().As<IAriatAdyenClient>().InstancePerDependency();
            builder.RegisterType<AriatUserClient>().As<IAriatUserClient>().InstancePerDependency();

            /*Adyen pay end*/
            /*Adyen to save custom1 field in DB start*/
            builder.RegisterType<AriatCheckoutAgent>().As<ICheckoutAgent>().InstancePerDependency();
            /*Adyen to save custom1 field in DB End*/
            builder.RegisterType<AriatProductClient>().As<IAriatProductClient>().InstancePerDependency();
            /*Generate OrderNumber Start*/
            builder.RegisterType<AriatOrderClient>().As<IAriatOrderClient>().InstancePerDependency();
            /*Generate OrderNumber End*/
            builder.RegisterType<CustomCartAgent>().As<ICartAgent>().InstancePerDependency();
            builder.RegisterType<AriatUserAgent>().As<IAriatUserAgent>().InstancePerDependency();
            builder.RegisterType<AriatUserAgent>().As<IUserAgent>().InstancePerDependency();
            builder.RegisterType<CustomHeadersHelper>().As<ICustomHeaders>().InstancePerDependency();
        }
        public int Order
        {
            get { return 1; }
        }
    }
}