﻿using System;
using System.Collections.Generic;
using System.Web;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.WebStore.Core.Agents.IAgent;

using static Znode.Libraries.ECommerce.Utilities.HelperUtility;

namespace Znode.WebStore.Custom
{
    public class CustomHeadersHelper : ICustomHeaders
    {
        #region Public Method
        public Dictionary<string, string> SetCustomHeaderOfClient()
        {
            Dictionary<string, string> headers = new Dictionary<string, string>();
            UserViewModel usermodel = new UserViewModel();
            bool isUserAutheticated = IsUserAutheticated();
            if (isUserAutheticated)
            {
                usermodel = GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey);
            }
            if (HelperUtility.IsNotNull(usermodel?.Custom3))
            {
                headers.Add("AccountExternalId", Convert.ToString(usermodel.Custom3));
            }
            return headers;
        }
        #endregion

        #region Protected Methods
        protected virtual bool IsUserAutheticated() => IsNotNull(GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey)) && HttpContext.Current.User.Identity.IsAuthenticated ? true : false;
        protected T GetFromSession<T>(string key) => SessionHelper.GetDataFromSession<T>(key); 
        #endregion
    }
}
