﻿using System.Collections.Generic;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Libraries.Admin;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Api.Custom
{
    public class CustomOrderHelper : ZnodeOrderHelper, IZnodeOrderHelper
    {
        #region Private Variable
        private readonly IZnodeRepository<ZnodePublishProductEntity> _publishProductEntity; 
        #endregion

        #region Constructor 
        public CustomOrderHelper()
        {
            _publishProductEntity = new ZnodeRepository<ZnodePublishProductEntity>(HelperMethods.Context);
        }
        #endregion

        #region Public Methods
        public override decimal GetAddonQuantity(ShoppingCartItemModel shoppingCartItem)
        {
            return shoppingCartItem.GroupProducts?.Count > 0 ? shoppingCartItem.GroupProducts[0].Quantity : (shoppingCartItem.AssociatedAddOnProducts?.FirstOrDefault(x => x.Sku == shoppingCartItem.AddOnProductSKUs) != null ? shoppingCartItem.AssociatedAddOnProducts.FirstOrDefault(x => x.Sku == shoppingCartItem.AddOnProductSKUs).AddOnQuantity : shoppingCartItem.Quantity);
        }

        public override List<OrderLineItemModel> FormalizeOrderLineItems(OrderModel orderModel)
        {
            List<OrderLineItemModel> orderLineItemListModel = new List<OrderLineItemModel>();

            List<OrderLineItemModel> lineItems = orderModel.OrderLineItems.ToList();

            foreach (OrderLineItemModel _lineItem in lineItems)
            {
                OrderLineItemModel parentOrderLineItem = orderModel.OrderLineItems.FirstOrDefault(oli => oli.OmsOrderLineItemsId == _lineItem.ParentOmsOrderLineItemsId);

                int? parentOmsOrderLineItemsId = orderModel.OrderLineItems.Where(oli => oli.ParentOmsOrderLineItemsId == _lineItem.OmsOrderLineItemsId && oli.OrderLineItemRelationshipTypeId == (int)ZnodeCartItemRelationshipTypeEnum.Bundles)?.FirstOrDefault()?.ParentOmsOrderLineItemsId;
                if (parentOmsOrderLineItemsId.HasValue)
                {
                    _lineItem.ParentOmsOrderLineItemsId = parentOmsOrderLineItemsId;
                    _lineItem.Total = (_lineItem.Price * _lineItem.Quantity) + lineItems.Where(x => x.ParentOmsOrderLineItemsId == parentOmsOrderLineItemsId && x.OrderLineItemRelationshipTypeId == (int)ZnodeCartItemRelationshipTypeEnum.AddOns).Sum(y => y.Quantity * y.Price);
                    parentOrderLineItem = orderModel.OrderLineItems.Where(oli => oli.OmsOrderLineItemsId == parentOmsOrderLineItemsId).FirstOrDefault();
                }
                if (parentOrderLineItem != null && _lineItem.OrderLineItemRelationshipTypeId != (int)ZnodeCartItemRelationshipTypeEnum.Bundles && _lineItem.OrderLineItemRelationshipTypeId != null)
                {
                    if (_lineItem.OrderLineItemRelationshipTypeId == (int)ZnodeCartItemRelationshipTypeEnum.Group)
                    {
                        _lineItem.Description = $"{_lineItem.ProductName}{"<br/>"}{ _lineItem.Description}";
                        _lineItem.ProductName = $"{parentOrderLineItem.ProductName}";
                    }

                    _lineItem.Total = (_lineItem.Price * _lineItem.Quantity) + lineItems.Where(x => x.ParentOmsOrderLineItemsId == _lineItem.OmsOrderLineItemsId).Sum(y => y.Quantity * y.Price);
                    _lineItem.PersonaliseValueList = parentOrderLineItem?.PersonaliseValueList;
                    _lineItem.PersonaliseValuesDetail = parentOrderLineItem?.PersonaliseValuesDetail;
                    _lineItem.GroupId = parentOrderLineItem.GroupId;

                    if (HelperUtility.IsNotNull(parentOrderLineItem.PersonaliseValuesDetail))
                    {
                        _lineItem.ProductName = $"<span style='vertical-align:top'>{parentOrderLineItem.ProductName}</span>";
                    }
                    if (_lineItem.OrderLineItemRelationshipTypeId != (int)ZnodeCartItemRelationshipTypeEnum.AddOns)
                    {
                        if (!string.IsNullOrEmpty(_lineItem.Sku))
                        {
                            string styleNumber = GetStyleNumberFromSku(_lineItem.Sku);
                            if (!string.IsNullOrEmpty(styleNumber))
                            {
                                _lineItem.Custom1 = styleNumber;
                                _lineItem.Custom3 = _lineItem.Price.ToString();
                            }
                        }
                        orderLineItemListModel.Add(_lineItem);
                    }
                }
                else
                {
                    if (orderModel.OrderLineItems.Any(oli => oli.OmsOrderLineItemsId == _lineItem.OmsOrderLineItemsId) && _lineItem.OrderLineItemRelationshipTypeId != (int)ZnodeCartItemRelationshipTypeEnum.Bundles && _lineItem.OrderLineItemRelationshipTypeId != (int)ZnodeCartItemRelationshipTypeEnum.AddOns)
                    {
                        if (!string.IsNullOrEmpty(_lineItem.Sku))
                        {
                            string styleNumber = GetStyleNumberFromSku(_lineItem.Sku);
                            if (!string.IsNullOrEmpty(styleNumber))
                            {
                                _lineItem.Custom1 = styleNumber;
                                _lineItem.Custom3 = _lineItem.Price.ToString();
                            }
                            orderLineItemListModel.Add(_lineItem);
                        }
                    }
                }


            }
            return orderLineItemListModel;
        }

        public virtual string GetStyleNumberFromSku(string sku)
        {
            string styleNumber = string.Empty;
            ZnodePublishProductEntity znodePublishProduct = _publishProductEntity.Table.FirstOrDefault(x => x.SKU == sku);
            PublishProductModel productDetails = znodePublishProduct?.ToModel<PublishProductModel>();
            if (productDetails?.Attributes?.Count > 0)
            {
                styleNumber = productDetails.Attributes.FirstOrDefault(x => x.AttributeCode == "StyleNumber").AttributeValues;
            }
            return styleNumber;
        } 
        #endregion

    }
}
