﻿
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Web;

using Znode.Custom.Api.Model;
using Znode.Custom.Data;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;

using static Znode.Libraries.ECommerce.Utilities.HelperUtility;

namespace Znode.Api.Custom.Service.Service
{
    public class AriatAccountService : BaseService, IAriatAccountService
    {
        #region Private variables
        public const string AccountExternalID = "AccountExternalID";      
        IZnodeRepository<AriatProgramCatalogProduct> _ariatProgramCatalogProduct;
        public const string LoggingComponent = "CustomAccount";
        public const string ErrorMessage = "AriatProgramCatalogProductId cannot be null or empty.";
        public const string AriatProgramCatalogProductId = "AriatProgramCatalogProductId";
        #endregion
        
        #region Public Constructor

        public AriatAccountService()
        {
            _ariatProgramCatalogProduct = new ZnodeRepository<AriatProgramCatalogProduct>(new Custom_Entities());
        }

        #endregion

        #region Public Method
        //Get the full catalog product list.
        public AriatFullCatalogProductListModel GetFullCatalogProductList(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            int? publishCatalogId = GetPublishedCatalogId(filters);
            PageListModel pageListModel = new PageListModel(filters, sorts, page);
            int localeId = GetDefaultLocaleId();
            ExecuteSpHelper objStoredProc = new ExecuteSpHelper();
            objStoredProc.GetParameter("@LocaleId", localeId, ParameterDirection.Input, SqlDbType.Int);
            objStoredProc.GetParameter("@PublishCatalogId", publishCatalogId, ParameterDirection.Input, SqlDbType.Int);
            objStoredProc.GetParameter("@PageNo", pageListModel.PagingStart, ParameterDirection.Input, SqlDbType.Int);
            objStoredProc.GetParameter("@Rows", pageListModel.PagingLength, ParameterDirection.Input, SqlDbType.Int);
            DataSet ds = objStoredProc.GetSPResultInDataSet("Znode_ExportCatalogProducts");
            ConvertDataTableToList dt = new ConvertDataTableToList();
            AriatFullCatalogProductListModel ariatFullCatalogProductListModel = new AriatFullCatalogProductListModel();
            ariatFullCatalogProductListModel.CatalogProductsDetailList = ds?.Tables.Count > 0 ? dt.ConvertDataTable<AriatProgramCatalogProductModel>(ds?.Tables[0]) : new List<AriatProgramCatalogProductModel>();

            if (IsNotNull(ds) && ds?.Tables.Count >= 2)
            {
                if (!string.IsNullOrEmpty(ds?.Tables[1].Rows[0]["RowsCount"].ToString()))
                    pageListModel.TotalRowCount = Convert.ToInt32(ds?.Tables[1].Rows[0]["RowsCount"].ToString());
            }

            ariatFullCatalogProductListModel.BindPageListModel(pageListModel);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            return ariatFullCatalogProductListModel;
        }

        //Get Published Catalog Id
        public virtual int? GetPublishedCatalogId(FilterCollection filters)
        {
            int accountId = 0;
            int? publishCatalogId = null;
            if (filters.Exists(x => x.Item1.Equals("AccountId", StringComparison.InvariantCultureIgnoreCase)))
            {
                int.TryParse(filters.Where(filterTuple => filterTuple.Item1 == ZnodePortalAccountEnum.AccountId.ToString().ToLower())?.FirstOrDefault()?.Item3, out accountId);
            }
            if (accountId > 0)
            {
                IZnodeRepository<ZnodePortalAccount> _portalAccountRepository = new ZnodeRepository<ZnodePortalAccount>();
                int? portalId = _portalAccountRepository.Table.FirstOrDefault(xx => xx.AccountId == accountId)?.PortalId;
                if (HelperUtility.IsNotNull(portalId))
                {
                    IZnodeRepository<ZnodePortalCatalog> _portalCatalogRepository = new ZnodeRepository<ZnodePortalCatalog>();
                    publishCatalogId = _portalCatalogRepository.Table.FirstOrDefault(xx => xx.PortalId == portalId)?.PublishCatalogId;
                }
            }
            return publishCatalogId;
        }

        //Get program catalog product list.
        public AriatProgramCatalogProductListModel ProgramCatalogProductList(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            string accountExternalId = "";
            if (filters.Exists(x => x.Item1.Equals(AccountExternalID, StringComparison.InvariantCultureIgnoreCase)))
            {
                accountExternalId = filters.Where(filterTuple => filterTuple.Item1 == AccountExternalID.ToLower())?.FirstOrDefault()?.Item3;
                filters.RemoveAll(x => x.Item1.Equals(AccountExternalID, StringComparison.InvariantCultureIgnoreCase));
                filters.Add(new FilterTuple(AccountExternalID, FilterOperators.Is, accountExternalId));
                filters.RemoveAll(x => x.Item1.Equals("accountid", StringComparison.InvariantCultureIgnoreCase));
            }
            if (!string.IsNullOrEmpty(accountExternalId))
            {
                PageListModel pageListModel = new PageListModel(filters, sorts, page);
                List<AriatProgramCatalogProduct> ariatProgramCatalogProductEntity = _ariatProgramCatalogProduct.GetPagedList(pageListModel.EntityWhereClause.WhereClause, pageListModel.OrderBy, pageListModel.EntityWhereClause.FilterValues, pageListModel.PagingStart, pageListModel.PagingLength, out pageListModel.TotalRowCount)?.ToList();
                AriatProgramCatalogProductListModel ariatProgramCatalogProductListModel = ariatProgramCatalogProductEntity?.Count > 0 ? new AriatProgramCatalogProductListModel { ProductList = ariatProgramCatalogProductEntity.ToModel<AriatProgramCatalogProductModel>()?.ToList() } : new AriatProgramCatalogProductListModel { ProductList = new List<AriatProgramCatalogProductModel>() };
                ariatProgramCatalogProductListModel.BindPageListModel(pageListModel);
                return ariatProgramCatalogProductListModel;
            }
            return new AriatProgramCatalogProductListModel { ProductList = new List<AriatProgramCatalogProductModel>() };
        }

        //Publish program catalog.
        public PublishedModel Publish(ParameterModel ids)
        {
            ZnodeLogging.LogMessage("Execution started.", LoggingComponent, TraceLevel.Info);
            ZnodeLogging.LogMessage("ariatProgramCatalogProductId to be program.", LoggingComponent, TraceLevel.Verbose, ids?.Ids);
            try
            {
                HttpContext httpContext = HttpContext.Current;
                Thread thread = new Thread(new ThreadStart(() =>
                {
                    HttpContext.Current = httpContext;
                }));
                thread.Start();

                return new PublishedModel { IsPublished = true, ErrorMessage = Admin_Resources.SuccessPublish };
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, "Admin", TraceLevel.Error);
                return new PublishedModel { IsPublished = false, ErrorMessage = Admin_Resources.ErrorPublished };
            }
        }

        //Delete the Product.
        public virtual bool DeleteProduct(ParameterModel ariatProgramCatalogProductId)
        {
            ZnodeLogging.LogMessage("Execution started.", LoggingComponent, TraceLevel.Info);
            ZnodeLogging.LogMessage("ariatProgramCatalogProductId to be deleted.", LoggingComponent, TraceLevel.Verbose, ariatProgramCatalogProductId?.Ids);

            //Checks ariatProgramCatalogProductId id.
            if (string.IsNullOrEmpty(ariatProgramCatalogProductId.Ids))
                throw new Exception(ErrorMessage);

            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(AriatProgramCatalogProductId, ProcedureFilterOperators.In, ariatProgramCatalogProductId.Ids));
            EntityWhereClauseModel whereClauseModel = DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filters.ToFilterDataCollection());
            return _ariatProgramCatalogProduct.Delete(whereClauseModel.WhereClause);
        }
        #endregion


    }
}
