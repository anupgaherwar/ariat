﻿using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using ApplicationUser = Znode.Engine.Services.ApplicationUser;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data;
using Znode.Engine.Exceptions;
using System.Net;
using Znode.Libraries.Resources;
using Microsoft.AspNet.Identity;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;
using Znode.Libraries.Data.Helpers;
using Znode.Engine.Services.Maps;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Web;
using Znode.Api.Custom.Service.IService;
using Znode.Custom.Data;

namespace Znode.Api.Custom.Service.Service
{
    public class AriatUserService : UserService, IAriatUserService
    {
        #region Private Variable and Constant
        private readonly IZnodeRepository<ZnodeUser> _userRepository;
        private readonly IUserLoginHelper _loginHelper;
        private readonly IZnodeRepository<ZnodePortalCatalog> _portalCatalogRepository;
        private readonly IZnodeRepository<ZnodeUserProfile> _userProfileRepository;
        private readonly IZnodeRepository<ZnodeProfile> _profileRepository;
        private readonly IZnodeRepository<ZnodeOmsOrderDetail> _orderRepository;
        private readonly IZnodeRepository<ZnodeUserAddress> _userAddressRepository;
        private readonly IZnodeRepository<ZnodeUserPortal> _userPortalRepository;
        private readonly IZnodeRepository<ZnodeDomain> _domainRepository;
        private readonly IZnodeRepository<ZnodeGiftCard> _giftCardRepository;
        private readonly IZnodeRepository<ZnodeAccount> _accountRepository;
        const string AccountActivationWithVoucher = "AccountActivationWithVoucher";
        const string AccountActivationWithoutVoucher = "AccountActivationWithoutVoucher";
        IZnodeRepository<AriatProgramCatalogProduct> _ariatProgramCatalogProduct;
        #endregion

        #region Constructor
        public AriatUserService()
        {
            _userRepository = new ZnodeRepository<ZnodeUser>();
            _loginHelper = GetService<IUserLoginHelper>();
            _portalCatalogRepository = new ZnodeRepository<ZnodePortalCatalog>();
            _userProfileRepository = new ZnodeRepository<ZnodeUserProfile>();
            _profileRepository = new ZnodeRepository<ZnodeProfile>();
            _orderRepository = new ZnodeRepository<ZnodeOmsOrderDetail>();
            _userAddressRepository = new ZnodeRepository<ZnodeUserAddress>();
            _userPortalRepository = new ZnodeRepository<ZnodeUserPortal>();
            _domainRepository = new ZnodeRepository<ZnodeDomain>();
            _giftCardRepository = new ZnodeRepository<ZnodeGiftCard>();
            _accountRepository = new ZnodeRepository<ZnodeAccount>();
            _ariatProgramCatalogProduct = new ZnodeRepository<AriatProgramCatalogProduct>(new Custom_Entities());
        }
        #endregion

        #region Public Methods
        public override UserModel Login(int portalId, UserModel model, out int? errorCode, NameValueCollection expand = null)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { portalId = portalId });

            errorCode = null;
            //Validate the Login user, and return the user details.
            ApplicationUser user = GetAspNetUserDetails(ref model, portalId);
            UserVerificationTypeEnum userVerificationTypeEnum;

            UserModel userDetails = GetUserDetailsForLogin(model, user.Id);

            if (IsNotNull(userDetails) && userDetails.IsVerified)
            {
                //This method is used to login the User having valid username and password.
                SignInStatus result = SignInManager.PasswordSignIn(user.UserName, model.User.Password, model.User.RememberMe, true);

                if (Equals(result, SignInStatus.Success))
                {
                    //Check Password is expired or not.
                    ZnodeUserAccountBase.CheckLastPasswordChangeDate(user.Id, out errorCode);

                    CheckUserPortalAccessForLogin(model.PortalId.GetValueOrDefault(), userDetails.UserId);

                    //Bind user details required after login.
                    model = BindDetails(model, user, expand);
                    return model;
                }
            }
            userVerificationTypeEnum = IsNotNull(userDetails?.UserVerificationType) ? (UserVerificationTypeEnum)Enum.Parse(typeof(UserVerificationTypeEnum), userDetails.UserVerificationType) : UserVerificationTypeEnum.None;
            //Throw the Exceptions for Invalid Login.
            InvalidLoginError(user, model.IsVerified, userVerificationTypeEnum);
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return model;
        }


        //Single reset password functionality.
        public override UserModel ForgotPassword(int portalId, UserModel model, bool isUserCreateFromAdmin = false, bool isAdminUser = false)
        {
            ZnodeLogging.LogMessage("Execution Started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { portalId = portalId });

            if (IsNull(model))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.UserModelNotNull);

            if (IsNull(model.User))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.LoginUserModelNotNull);

            portalId = (model.PortalId > 0) ? model.PortalId.Value : PortalId;
            ZnodeLogging.LogMessage("Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { portalId = portalId });

            //Get the AspNet Znode user.
            AspNetZnodeUser znodeUser = _loginHelper.GetUser(model, portalId);

            if (IsNull(znodeUser))
                throw new ZnodeException(ErrorCodes.InvalidData, string.Format(Admin_Resources.UserNameUnavailable, model.User));

            //This method is used to find the User having valid username.
            ApplicationUser user = UserManager.FindByNameAsync(znodeUser.AspNetZnodeUserId).Result;

            if (IsNull(user))
                throw new ZnodeException(ErrorCodes.InvalidData, string.Format(Admin_Resources.UserNotFound, model.User));


            ZnodeUser userStatusCode = _userRepository.Table?.FirstOrDefault(x => x.AspNetUserId == user.Id);

            if (!userStatusCode.IsVerified && IsNotNull(userStatusCode?.UserVerificationType)
                && IsAdminApprovalType((UserVerificationTypeEnum)Enum.Parse(typeof(UserVerificationTypeEnum), userStatusCode.UserVerificationType)))
                throw new ZnodeException(ErrorCodes.AdminApprovalLoginFail, WebStore_Resources.LoginFailAdminApproval);

            //This method is used to verify the userwith password stored in database.
            if (UserManager.IsLockedOut(user.Id))
                throw new ZnodeException(ErrorCodes.AccountLocked, Admin_Resources.AccountLocked);

            if (!string.IsNullOrEmpty(userStatusCode?.FirstName))
            {
                model.FirstName = userStatusCode.FirstName;
            }

            if (portalId < 1)
            {
                // Get the portal id.
                SetAssignedPortal(model, true);
                if (model.PortalId < 1)
                    model.PortalId = PortalId;
            }
            else
                model.PortalId = portalId;

            //Reset Password Link in Email - Start
            SendResetPassword(model, user, isUserCreateFromAdmin, isAdminUser);
            ZnodeLogging.LogMessage("Execution Done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);

            return model;
        }

        public override bool BulkResetPassword(ParameterModel accountIds)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            bool isPasswordReset = true;
            StringBuilder failedUserNames = new StringBuilder();
            List<ZnodeUser> accountList = GetUsers(accountIds);
            ZnodeLogging.LogMessage("accountList list count:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, accountList?.Count());
            if (accountList?.Count > 0)
            {
                foreach (var account in accountList)
                {
                    try
                    {
                        if (IsNull(account))
                            throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.UserModelNotNull);

                        ApplicationUser userDetails = UserManager.FindById(account.AspNetUserId);
                        if (IsNull(userDetails))
                            throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ApplicationUserNotNull);
                        //This method is used to find the User having valid user name.
                        ApplicationUser user = UserManager.FindByNameAsync(userDetails.UserName).Result;

                        if (IsNull(user))
                            throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.UserNotFound);

                        //This method is used to verify the user with password stored in database.
                        if (UserManager.IsLockedOut(user.Id))
                            throw new ZnodeException(ErrorCodes.AccountLocked, Admin_Resources.AccountLocked);

                        //This method is used to generate password reset token.
                        string passwordResetToken = UserManager.GeneratePasswordResetToken(user.Id);
                        //Reset Password Link in Email - Start

                        //Get the User name for Znode user.
                        string userName = _loginHelper.GetUserById(userDetails.UserName)?.UserName;
                        string firstName = !string.IsNullOrEmpty(account?.FirstName) ? account?.FirstName : userName;
                        int portalId = _userPortalRepository.Table.FirstOrDefault(x => x.UserId == account.UserId)?.PortalId ?? PortalId;

                        //Send the Reset Password Email
                        SendResetPasswordMail(userDetails, passwordResetToken, userName, portalId, firstName);
                    }
                    catch (ZnodeException)
                    {
                        throw;
                    }
                    catch (Exception ex)
                    {
                        isPasswordReset = false;
                        failedUserNames.Append($"{account.MiddleName} ");
                        ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Admin.ToString(), TraceLevel.Error);
                    }
                }
                if (!string.IsNullOrEmpty(Convert.ToString(failedUserNames)))
                    throw new ZnodeException(ErrorCodes.ErrorResetPassword, Convert.ToString(failedUserNames));
            }
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return isPasswordReset;
        }

        public override bool EnableDisableUser(ParameterModel userIds, bool lockUser)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            bool isAllowedToLock = false;
            List<ZnodeUser> list = GetUsers(userIds);
            if (lockUser)
            {
                int failedOperation = 0;
                foreach (ZnodeUser accountDetails in list)
                {
                    bool isErrors = base.EnableDisableUser(accountDetails, lockUser, out isAllowedToLock);
                    if (isErrors)
                        failedOperation++;
                    if (failedOperation > 0)
                    {
                        throw new ZnodeException(ErrorCodes.LockOutEnabled, Admin_Resources.LockedOutDisableForAccount);
                    }
                }
                ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
                return isAllowedToLock;
            }
            else
            {

                if (list?.Count > 0)
                {
                    ZnodeLogging.LogMessage("ZnodeUser list count:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, list?.Count());
                    int failedOperations = 0;
                    foreach (ZnodeUser accountDetails in list)
                    {
                        bool isError = EnableDisableUserWithPassword(accountDetails, lockUser, out isAllowedToLock);

                        if (isError)
                            failedOperations++;
                    }

                    if (failedOperations > 0)
                    {
                        throw new ZnodeException(ErrorCodes.LockOutEnabled, Admin_Resources.LockedOutDisableForAccount);
                    }
                }
            }
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return isAllowedToLock;
        }

        public bool EnableDisableUserWithPassword(ZnodeUser accountDetails, bool lockUser, out bool isAllowedToLock)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);

            isAllowedToLock = true;
            bool isVerifiedUpdated = false;
            int portalIdValue = 0;
            string accountName = string.Empty;
            bool isSuccessFullTrigged = false;

            //Get current user details.
            //ApplicationUser user = UserManager.FindById(accountDetails.AspNetUserId.ToString());

            ApplicationUser userDetails = UserManager.FindById(accountDetails.AspNetUserId);
            if (IsNull(userDetails))
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ApplicationUserNotNull);
            ApplicationUser user = UserManager.FindByNameAsync(userDetails.UserName).Result;

            if (IsNull(user))
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.UserNotFound);


            if (IsNotNull(userDetails))
            {
                int? portalId = _userPortalRepository.Table.FirstOrDefault(x => x.UserId == accountDetails.UserId)?.PortalId;
                // Gets the portal ids 
                portalIdValue = ((portalId < 1) || IsNull(portalId)) ? PortalId : portalId.Value;
                ZnodeLogging.LogMessage("Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { portalId = portalId });

                IdentityResult result = null;
                //Code for unlocking user account.
                if (UserManager.IsLockedOut(userDetails.Id) && !lockUser)
                {
                    userDetails.LockoutEndDateUtc = null;
                    result = UserManager.Update(userDetails);
                    if (UserManager.IsLockedOut(user.Id))
                        throw new ZnodeException(ErrorCodes.AccountLocked, Admin_Resources.AccountLocked);
                    if (!accountDetails.IsVerified)
                    {
                        if (IsAdminApprovalType(accountDetails.UserVerificationType))
                        {
                            isVerifiedUpdated = VerifyAccountAndSendMail(accountDetails, portalIdValue);
                        }
                    }
                }
                else if (lockUser)
                    //Code for locking user account.
                    result = UserManager.SetLockoutEndDate(userDetails.Id, DateTimeOffset.MaxValue);
                if (IsNotNull(portalId) && !isVerifiedUpdated)
                {
                    if (UserManager.IsLockedOut(userDetails.Id))
                        throw new ZnodeException(ErrorCodes.AccountLocked, Admin_Resources.AccountLocked);

                    int voucherCount = _giftCardRepository.Table.Count(x => x.UserId == accountDetails.UserId);
                    bool isVoucherAssociatedToUser = voucherCount > 0 ? true : false;

                    if (HelperUtility.IsNotNull(accountDetails.AccountId) && accountDetails.AccountId > 0)
                        accountName = _accountRepository.Table.FirstOrDefault(x => x.AccountId == accountDetails.AccountId)?.Name;


                    bool isAdminUser = IsAdminUser(userDetails);
                    PortalModel portalModel = GetCustomPortalDetails(portalIdValue);

                    string baseUrl = !isAdminUser ? GetDomains(portalIdValue) : ZnodeApiSettings.AdminWebsiteUrl;
                    string passwordResetToken = UserManager.GeneratePasswordResetToken(user.Id);
                    baseUrl = !isAdminUser ? (IsNotNull(portalModel) && portalModel.IsEnableSSL) ? $"https://{baseUrl}" : $"http://{baseUrl}" : baseUrl;

                    string userName = _loginHelper.GetUserById(userDetails.UserName)?.UserName;

                    string passwordResetUrl = $"{baseUrl}/User/ResetPassword?passwordToken={WebUtility.UrlEncode(passwordResetToken)}&userName={WebUtility.UrlEncode(userName)}";
                    string passwordResetLink = $"<a href=\"{passwordResetUrl}\"> here</a>";
                    isSuccessFullTrigged = SendAccountActivationEmail(accountDetails.FirstName, userDetails.Email, portalId.Value, isVoucherAssociatedToUser, accountName, passwordResetUrl, passwordResetLink, portalModel, accountDetails.UserId);
                }

                if (IsNotNull(result))
                {
                    if (result.Succeeded)
                        isAllowedToLock = true;
                }
                else if (isSuccessFullTrigged)
                {
                    isAllowedToLock = true;
                }
            }
            ZnodeLogging.LogMessage("Execution Done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);

            return false;
        }
        #endregion

        #region Protected Methods
        protected override UserModel BindDetails(UserModel model, ApplicationUser user, NameValueCollection expand)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            //Gets the User Details by User Id.
            model = GetUserByUserId(user.Id, expand);
            //Get role id.
            model.User.RoleId = user.Roles.Select(s => s.RoleId).FirstOrDefault();
            //check whether the current login user has Customer user and B2B role or not.
            model.IsAdminUser = IsAdminUser(user);
            //Get the role name.

            IRoleService role = GetService<IRoleService>();
            model.RoleName = role.GetRole(model.User.RoleId)?.Name;

            //Get user address list.
            model.Addresses = GetAddressList(model);
            //Get user access permissiom.
            model.PermissionCode = GetUserAccessPermission(model.UserId)?.PermissionCode;
            model.ProfileId = Convert.ToInt32(model.Profiles?.Select(x => x.ProfileId).FirstOrDefault());

            model = GetUserAccountCatalogDetails(model);

            ZnodeLogging.LogMessage("ExternalId:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, model.ExternalId);
            ZnodeLogging.LogMessage("Execution Done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return GetUsersAdditionalAttributes(model);
        }

        protected override bool VerifyAccountAndSendMail(ZnodeUser accountDetails, int portalId)
        {
            //Execute only when request is for unlocking and non verified.
            accountDetails.IsVerified = true;
            accountDetails.UserVerificationType = null;
            bool updateStatus = _userRepository.Update(accountDetails);
            GenerateAccountActivationStatusEmail(accountDetails.ToModel<UserModel>(), ZnodeConstant.AccountVerificationSuccessful, portalId);
            return updateStatus;
        }
        #endregion

        #region Private Methods
        private bool SendAccountActivationEmail(string firstName, string email, int portalId, bool isVoucherAssociatedToUser, string accountName, string passwordResetUrl, string passwordResetLink, PortalModel portalModel, int userId)
        {
            bool isSuccessFullTrigged = false;
            string salesRepFirstName = string.Empty;
            string salesRepLastName = string.Empty;
            string salesRepEmail = string.Empty;
            string salesRepContactNumber = string.Empty;
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { firstName = firstName, email = email, portalId = portalId });

            EmailTemplateMapperModel emailTemplateMapperModel = isVoucherAssociatedToUser ? GetEmailTemplateByCode(AccountActivationWithVoucher, portalId)
                                                                        : GetEmailTemplateByCode(AccountActivationWithoutVoucher, portalId);

            if (isVoucherAssociatedToUser && IsNotNull(emailTemplateMapperModel))
            {
                GiftCardModel giftCardModel = new GiftCardModel();
                List<DefaultGlobalConfigModel> defaultGlobalSettingData = GetDefaultGlobalSettingData();
                giftCardModel = _giftCardRepository.Table.FirstOrDefault(x => x.UserId == userId).ToModel<GiftCardModel>();

                EmailTemplateHelper.SetParameter("#VoucherName#", giftCardModel?.Name);
                EmailTemplateHelper.SetParameter("#StartDate#", giftCardModel?.StartDate?.ToString(GetDefaultDateFormat(defaultGlobalSettingData)));
                EmailTemplateHelper.SetParameter("#ExpirationDate#", giftCardModel?.ExpirationDate?.ToString(GetDefaultDateFormat(defaultGlobalSettingData)));
                EmailTemplateHelper.SetParameter("#Amount#", FormatPriceWithCurrency(giftCardModel?.Amount, string.IsNullOrEmpty(giftCardModel.CultureCode) ? GetDefaultCulture(defaultGlobalSettingData) : giftCardModel.CultureCode, GetDefaultPriceRoundOff(defaultGlobalSettingData)));

            }

            if (userId > 0)
            {
                IZnodeRepository<ZnodeSalesRepCustomerUserPortal> _salesRepCustomerUserPortalRepository = new ZnodeRepository<ZnodeSalesRepCustomerUserPortal>();
                int? salesRepId = _salesRepCustomerUserPortalRepository.Table.FirstOrDefault(xx => xx.CustomerUserid == userId)?.SalesRepUserId;
                if (IsNotNull(salesRepId) && salesRepId > 0)
                {
                    UserModel userModel = _userRepository.Table.FirstOrDefault(xx => xx.UserId == salesRepId)?.ToModel<UserModel>();
                    salesRepFirstName = userModel?.FirstName;
                    salesRepLastName = userModel?.LastName;
                    salesRepEmail = userModel?.Email;
                    salesRepContactNumber = userModel?.PhoneNumber;
                }
            }
            if (IsNotNull(emailTemplateMapperModel))
            {
                firstName = string.IsNullOrEmpty(firstName) ? email : firstName;
                EmailTemplateHelper.SetParameter("#FirstName#", firstName);
                EmailTemplateHelper.SetParameter("#AccountName#", accountName);
                EmailTemplateHelper.SetParameter("#Link#", passwordResetLink);
                EmailTemplateHelper.SetParameter("#Url#", passwordResetUrl);
                EmailTemplateHelper.SetParameter("#StoreLogo#", portalModel?.StoreLogo);
                EmailTemplateHelper.SetParameter("#SalesRepFirstName#", salesRepFirstName);
                EmailTemplateHelper.SetParameter("#SalesRepLastName#", salesRepLastName);
                EmailTemplateHelper.SetParameter("#SalesRepEmail#", salesRepEmail);
                EmailTemplateHelper.SetParameter("#SalesRepContactNumber#", salesRepContactNumber);
                EmailTemplateHelper.SetParameter("#StoreName#", portalModel?.StoreName);

                string messageText = EmailTemplateHelper.ReplaceTemplateTokens(emailTemplateMapperModel.Descriptions);
                //These method is used to set null to unwanted keys.
                messageText = SetNullToUnWantedKeys(messageText);
                ZnodeLogging.LogMessage("Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { messageText = messageText });

                string bcc = string.Empty;
                try
                {
                    if (IsNotNull(portalModel) && portalId > 0)
                        ZnodeEmail.SendEmail(portalId, email, portalModel?.AdministratorEmail, ZnodeEmail.GetBccEmail(emailTemplateMapperModel.IsEnableBcc, portalId, bcc), $"{portalModel?.StoreName} - {emailTemplateMapperModel?.Subject}", messageText, true);
                    else
                        ZnodeEmail.SendEmail(email, ZnodeConfigManager.SiteConfig.AdminEmail, ZnodeEmail.GetBccEmail(emailTemplateMapperModel.IsEnableBcc, portalId, bcc), $"{ZnodeConfigManager.SiteConfig.StoreName} - {emailTemplateMapperModel?.Subject}", messageText, true);
                    isSuccessFullTrigged = true;
                }
                catch (Exception ex)
                {
                    ZnodeLogging.LogActivity((int)ZnodeLogging.ErrorNum.LoginCreateSuccess, firstName, string.Empty, null, ex.Message, null);
                }
            }
            return isSuccessFullTrigged;
        }
        private List<DefaultGlobalConfigModel> GetDefaultGlobalSettingData()
        {
            IDefaultGlobalConfigService defaultGlobalConfigService = ZnodeDependencyResolver.GetService<IDefaultGlobalConfigService>();
            return defaultGlobalConfigService.GetDefaultGlobalConfigList()?.DefaultGlobalConfigs;
        }

        private string GetDefaultPriceRoundOff(List<DefaultGlobalConfigModel> defaultGlobalSettingData)
           => defaultGlobalSettingData?.Where(x => string.Equals(x.FeatureName, GlobalSettingEnum.PriceRoundOff.ToString(), StringComparison.CurrentCultureIgnoreCase))?.Select(x => x.FeatureValues)?.FirstOrDefault();

        //Get default currency code.
        private string GetDefaultCulture(List<DefaultGlobalConfigModel> defaultGlobalSettingData)
           => defaultGlobalSettingData?.Where(x => string.Equals(x.FeatureName, GlobalSettingEnum.Culture.ToString(), StringComparison.CurrentCultureIgnoreCase))?.Select(x => x.FeatureValues)?.FirstOrDefault();
        //For Price according to currency.
        private static string FormatPriceWithCurrency(decimal? price, string CultureName, string defaultPriceRoundOff)
        {
            string currencyValue;
            if (IsNotNull(CultureName))
            {
                CultureInfo info = new CultureInfo(CultureName);
                info.NumberFormat.CurrencyDecimalDigits = Convert.ToInt32(defaultPriceRoundOff);
                currencyValue = $"{price.GetValueOrDefault().ToString("c", info.NumberFormat)}";
            }
            else
                currencyValue = Convert.ToString(price);

            return currencyValue;
        }

        private void SendResetPassword(UserModel model, ApplicationUser user, bool isUserCreateFromAdmin = false, bool isAdminUser = false)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);

            //This method is used to update the security stamp to make invalid previous token which is required in case of reset password. 
            UserManager.UpdateSecurityStamp(user.Id);
            //This method is used to generate password reset token.
            string passwordResetToken = UserManager.GeneratePasswordResetToken(user.Id);
            string baseUrl = string.IsNullOrEmpty(model.BaseUrl) ? ZnodeApiSettings.AdminWebsiteUrl : model.BaseUrl;
            string name = string.Empty;

            if (string.IsNullOrEmpty(model?.FirstName))
            {
                name = model?.User?.Username;
            }
            else
            {
                name = model?.FirstName;
            }

            if (string.IsNullOrEmpty(name))
            {
                name = model.UserName;
            }
            PortalModel portalModel = GetCustomPortalDetails(model.PortalId.Value);
            if (isUserCreateFromAdmin && isAdminUser != true)
            {
                //Get Portal details as well as the Domain Url based on the WEbStore Application Type.
                baseUrl = (IsNotNull(portalModel)) ? $"{HttpContext.Current.Request.Url.Scheme}://{portalModel.DomainUrl}" : ZnodeApiSettings.AdminWebsiteUrl;
            }
            //Generate the Password Reset Url
            string passwordResetUrl = $"{baseUrl}/User/ResetPassword?passwordToken={WebUtility.UrlEncode(passwordResetToken)}&userName={WebUtility.UrlEncode(model.User.Username)}";
            string subject = string.Empty;
            bool isHtml = false;
            bool isEnableBcc = false;
            //Generate Reset Password Email Content.

            try
            {
                string messageText = GenerateResetPasswordEmail(name, passwordResetUrl, $"<a href=\"{passwordResetUrl}\"> here</a>", model.PortalId.Value, out subject, out isHtml, out isEnableBcc, model.LocaleId, isUserCreateFromAdmin);
                ZnodeLogging.LogMessage("Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info, new { messageText = messageText });

                if (!string.IsNullOrEmpty(messageText))
                    //This method is used to send an email.                    
                    if (IsNotNull(portalModel) && model.PortalId.Value > 0)
                        ZnodeEmail.SendEmail(model.PortalId.Value, model.User.Email, portalModel?.AdministratorEmail, ZnodeEmail.GetBccEmail(isEnableBcc, model.PortalId.Value, string.Empty), subject, messageText, isHtml);
                    else
                        ZnodeEmail.SendEmail(model.User.Email, ZnodeConfigManager.SiteConfig.AdminEmail, ZnodeEmail.GetBccEmail(isEnableBcc, model.PortalId.Value, string.Empty), subject, messageText, isHtml);
                else
                    throw new ZnodeException(ErrorCodes.EmailTemplateDoesNotExists, Admin_Resources.ErrorResetPasswordLinkReset);
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Admin.ToString(), TraceLevel.Warning);
                model.IsEmailSentFailed = true;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Admin.ToString(), TraceLevel.Error);
                model.IsEmailSentFailed = true;
                throw new ZnodeException(ErrorCodes.ErrorSendResetPasswordLink, Admin_Resources.ErrorSendResetPasswordLink);
            }
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
        }
        private void SetAssignedPortal(UserModel userModel, bool isCustomerEditMode)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            //Check for userId greater than 0 and sent form customer edit mode.
            if (userModel?.UserId > 0 && isCustomerEditMode)
                SetPortal(userModel);

            if (userModel?.UserId < 1 && !isCustomerEditMode && userModel.PortalIds.Any())
                userModel.PortalId = Convert.ToInt32(userModel.PortalIds.FirstOrDefault());
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
        }

        //Sets the portal ids.
        private void SetPortal(UserModel userModel)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            //If the user belongs to the account then fetch the account's portal Id otherwise user portal id.
            if (userModel.AccountId > 0)
                userModel.PortalId = GetAccountPortalId(Convert.ToInt32(userModel.AccountId));
            else
            {
                List<ZnodeUserPortal> userPortalList = _userPortalRepository.Table.Where(x => x.UserId == userModel.UserId)?.ToList();

                if ((userPortalList?.Any(x => x.PortalId > 0)).GetValueOrDefault())
                    userModel.PortalIds = userPortalList.Select(x => x.PortalId.ToString()).ToArray();
                userModel.PortalId = string.IsNullOrEmpty(userModel.PortalIds?.FirstOrDefault()) ? (int?)null : Convert.ToInt32(userModel.PortalIds?.FirstOrDefault());
            }
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
        }

        //Get the account's portal Id.
        private int? GetAccountPortalId(int accountId)
        {
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { accountId = accountId });

            IZnodeRepository<ZnodePortalAccount> _portalAccountRepository = new ZnodeRepository<ZnodePortalAccount>();
            return _portalAccountRepository.Table.FirstOrDefault(x => x.AccountId == accountId)?.PortalId;
        }

        //Get default date format.
        private string GetDefaultDateFormat(List<DefaultGlobalConfigModel> defaultGlobalSettingData)
            => defaultGlobalSettingData?.Where(x => string.Equals(x.FeatureName, GlobalSettingEnum.DateFormat.ToString(), StringComparison.CurrentCultureIgnoreCase))?.Select(x => x.FeatureValues)?.FirstOrDefault();

        //These method is used to set null to unwanted keys.
        private static string SetNullToUnWantedKeys(string messageText)
        {
            //It is used to find keys in between special characters.
            List<string> list = Regex.Matches(messageText, @"#\w*#")
                                    .Cast<Match>()
                                     .Select(m => m.Value)
                                        .ToList();

            ZnodeLogging.LogMessage("list count:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, list?.Count());
            foreach (string key in list)
                messageText = ReplaceTokenWithMessageText(key, null, messageText);

            ZnodeLogging.LogMessage("Output Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { messageText = messageText });

            return messageText;
        }

        private List<ZnodeUser> GetUsers(ParameterModel accountIds)
        {
            FilterCollection filterList = new FilterCollection();
            filterList.Add(new FilterTuple(ZnodeUserEnum.UserId.ToString(), ProcedureFilterOperators.In, accountIds.Ids));

            return _userRepository.GetEntityList(DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filterList.ToFilterDataCollection()).WhereClause).ToList();
        }

        private string GetDomains(int portalId)
          => _domainRepository.Table.Where(x => x.PortalId == portalId && x.ApplicationType == ZnodeConstant.WebStore && x.IsActive && x.IsDefault)?.Select(x => x.DomainName)?.FirstOrDefault().ToString();


        //Method to verify admin approval type.
        private bool IsAdminApprovalType(string userVerificationType) => IsAdminApprovalType((UserVerificationTypeEnum)Enum.Parse(typeof(UserVerificationTypeEnum), userVerificationType));

        //Method to verify admin approval type.
        private bool IsAdminApprovalType(UserVerificationTypeEnum userVerificationType)
        {
            if (userVerificationType == UserVerificationTypeEnum.AdminApprovalCode)
            {
                return true;
            }
            return false;
        }

        private UserModel GetUsersAdditionalAttributes(UserModel model)
        {
            model.PortalId = PortalId;          
            GetGlobalDetails(model);
            string ids = string.Empty;
            decimal amount = 0;
            string culterCode = string.Empty;
            //Get the allowance amount.
            List<GiftCardModel> giftCardRestrictVocher = _giftCardRepository.Table.Where(x => x.RestrictToCustomerAccount == false && x.IsActive == true)?.ToModel<GiftCardModel>().ToList();
            List<GiftCardModel> giftCardModels = _giftCardRepository.Table.Where(x => x.UserId == model.UserId && x.ExpirationDate >= DateTime.Today && x.RemainingAmount > 0)?.ToModel<GiftCardModel>().ToList();
            if (giftCardModels.Count > 0)
            {
                foreach (GiftCardModel item in giftCardModels)
                {
                    if (HelperUtility.IsNotNull(item.RemainingAmount) && item.RemainingAmount > 0 && item.IsActive == true)
                    {
                        amount += item.RemainingAmount.HasValue ? item.RemainingAmount.Value : 0;
                    }
                }
                ids = string.Join(",", giftCardModels.Select(x => x.GiftCardId));
            }
            if (giftCardRestrictVocher.Count > 0)
            {
                foreach (GiftCardModel item in giftCardRestrictVocher)
                {
                    if (!ids.Contains(item.GiftCardId.ToString()) && HelperUtility.IsNotNull(item.RemainingAmount) && item.RemainingAmount > 0 && item.IsActive == true)
                    {
                        amount += item.RemainingAmount.HasValue ? item.RemainingAmount.Value : 0;
                    }
                }
            }
            if (HelperUtility.IsNotNull(amount) && amount > 0)
            {
                List<DefaultGlobalConfigModel> defaultGlobalSettingData = GetDefaultGlobalSettingData();
                model.Custom5 = Convert.ToString(amount);
                model.Custom5 = FormatPriceWithCurrency(amount, string.IsNullOrEmpty(culterCode) ? GetDefaultCulture(defaultGlobalSettingData) : culterCode, GetDefaultPriceRoundOff(defaultGlobalSettingData));
            }
            return model;
        }
       
        //gets the budget amount for the user
        private void GetGlobalDetails(UserModel model)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { AccountId = model.AccountId });
            int accountId = model.AccountId.HasValue ? model.AccountId.Value : 0;
            List<GlobalAttributeValuesModel> allUserAttributes = GetGlobalLevelAttributeList(model.UserId, ZnodeConstant.User);
            List<GlobalAttributeValuesModel> allStoreAttributes = GetGlobalLevelAttributeList(model.PortalId.GetValueOrDefault(), ZnodeConstant.Store);
            if (accountId > 0)
            {
                List<GlobalAttributeValuesModel> allAccountAttributes = GetGlobalLevelAttributeList(accountId, ZnodeConstant.Account);
                GlobalAttributeValuesModel globalAttributeValuesModel = allAccountAttributes.FirstOrDefault(xx => xx.AttributeCode == "EnableShippingAddressOfOwn");
                if (HelperUtility.IsNotNull(globalAttributeValuesModel) && !string.IsNullOrEmpty(globalAttributeValuesModel.AttributeValue))
                {
                    allUserAttributes.Add(globalAttributeValuesModel);
                }
                allUserAttributes = AddAttributeForProgramCatalog(model, allUserAttributes, allStoreAttributes);
            }
            ZnodeLogging.LogMessage("List Count:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { allUserAttributes = allUserAttributes?.Count(), allStoreAttributesCount = allStoreAttributes?.Count() });

            model.BillingAccountNumber = allUserAttributes?.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.BillingAccountNumber)?.AttributeValue;
            model.PerOrderLimit = Convert.ToDecimal(allUserAttributes?.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.PerOrderLimit)?.AttributeValue);
            model.AnnualOrderLimit = Convert.ToDecimal(allUserAttributes?.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.PerOrderAnnualLimit)?.AttributeValue);
            int annualOrderLimitStartMonth = model.PortalId.GetValueOrDefault() > 0 ? Convert.ToInt32(allStoreAttributes?.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.PerOrderAnnualLimitStartMonth && x.AttributeValue != null)?.AttributeValue) : 1;
            ZnodeLogging.LogMessage("Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { annualOrderLimitStartMonth = annualOrderLimitStartMonth });

            model.AnnualBalanceOrderAmount = CalculateUserBalanceAmount(annualOrderLimitStartMonth, model.UserId, model.AnnualOrderLimit);
            model.UserGlobalAttributes = allUserAttributes;
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
        }

        private List<GlobalAttributeValuesModel> AddAttributeForProgramCatalog(UserModel model, List<GlobalAttributeValuesModel> allUserAttributes, List<GlobalAttributeValuesModel> allStoreAttributes)
        {
            if (!string.IsNullOrEmpty(model?.Custom3) && model?.PublishCatalogId == 0)
            {
                FilterCollection filter = new FilterCollection();
                filter.Add(new FilterTuple("AccountExternalID", FilterOperators.Is, model?.Custom3));
                EntityWhereClauseModel whereClauseModel = DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filter.ToFilterDataCollection());
                List<AriatProgramCatalogProduct> ariatProgramCatalogProductEntity = _ariatProgramCatalogProduct.GetEntityListWithoutOrderBy(whereClauseModel.WhereClause, whereClauseModel.FilterValues)?.ToList();
                if (ariatProgramCatalogProductEntity?.Count == 0)
                {
                    if (allStoreAttributes?.Count() > 0)
                    {
                        GlobalAttributeValuesModel globalAttributeErrorValues = allStoreAttributes.FirstOrDefault(xx => xx.AttributeCode == "ShowNoProgramCatalogMessage");
                        if (HelperUtility.IsNotNull(globalAttributeErrorValues) && globalAttributeErrorValues.AttributeValue == "true")
                        {
                            allUserAttributes.Add(globalAttributeErrorValues);
                            return allUserAttributes;
                        }
                    }
                }
            }
            return allUserAttributes;
        }

        //Calculate the annual balance amount for the user
        private decimal CalculateUserBalanceAmount(int annualOrderLimitStartMonth, int userId, decimal annualOrderLimit)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { annualOrderLimitStartMonth = annualOrderLimitStartMonth, userId = userId, annualOrderLimit = annualOrderLimit });

            if (annualOrderLimitStartMonth > 0)
            {
                var startDate = new DateTime(DateTime.Now.Year, annualOrderLimitStartMonth, 1);
                var endDate = startDate.AddMonths(12).AddSeconds(-1);

                if (startDate > DateTime.Now)
                    return 0;

                decimal? orderTotalforYear = _orderRepository.Table.Where(x => x.UserId == userId && x.OrderDate >= startDate && x.OrderDate <= endDate)?.Sum(y => y.Total);
                decimal limit = annualOrderLimit - orderTotalforYear.GetValueOrDefault();
                ZnodeLogging.LogMessage("Order limit:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, limit);
                ZnodeLogging.LogMessage("Execution Done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);

                return limit <= 0 ? 0 : limit;

            }
            else
                return 0;
        }

        //Get address list.
        private List<AddressModel> GetAddressList(UserModel model)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { AccountId = model?.AccountId });

            FilterCollection filters = new FilterCollection();

            List<string> navigationProperties = new List<string>();
            navigationProperties.Add(ZnodeAccountAddressEnum.ZnodeAddress.ToString());

            //If login user is B2B get account address list else get customer address list.
            if (_loginHelper.IsAccountCustomer(model) && model.AccountId > 0)
            {
                filters.Add(ZnodeAccountEnum.AccountId.ToString(), FilterOperators.Equals, model.AccountId.ToString());

                IZnodeRepository<ZnodeAccountAddress> _accountAddressRepository = new ZnodeRepository<ZnodeAccountAddress>();
                return AccountMap.ToListModel(_accountAddressRepository.GetEntityList(DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filters.ToFilterDataCollection()).WhereClause, navigationProperties))?.AddressList;
            }
            else
            {
                filters.Add(ZnodeUserEnum.UserId.ToString(), FilterOperators.Equals, model.UserId.ToString());
                ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
                return UserMap.ToAddressListModel(_userAddressRepository.GetEntityList(DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filters.ToFilterDataCollection()).WhereClause, navigationProperties), null)?.AddressList;
            }

        }


        private UserModel GetUserByUserId(string userId, NameValueCollection expand)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { userId = userId });

            if (!string.IsNullOrEmpty(userId))
            {
                //Create tupple to generate where clause.
                FilterCollection filters = new FilterCollection();
                filters.Add(new FilterTuple(ZnodeUserEnum.AspNetUserId.ToString(), ProcedureFilterOperators.Is, userId));

                //Generating where clause to get account details.
                EntityWhereClauseModel whereClauseModel = DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filters.ToFilterDataCollection());
                List<string> navigationProperties = GetExpands(expand);

                //This method is used to get the user details.
                ZnodeUser account = _userRepository.GetEntity(whereClauseModel.WhereClause, navigationProperties, whereClauseModel.FilterValues);

                if (PortalId > 0 && HelperUtility.IsNotNull(account.ZnodeUserWishLists))
                    account.ZnodeUserWishLists = account.ZnodeUserWishLists.Where(x => x.PortalId == PortalId).ToList();

                if (IsNotNull(account))
                    //Bind the User details based on the expand collection
                    return BindUserDetails(UserMap.ToModel(account, string.Empty), expand);
            }
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return new UserModel();
        }

        //Get expands and add them to navigation properties
        private List<string> GetExpands(NameValueCollection expands)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            List<string> navigationProperties = new List<string>();
            if (expands?.HasKeys() ?? false)
            {
                foreach (string key in expands.Keys)
                {
                    //Add expand keys
                    if (key.Equals(ZnodeAccountUserOrderApprovalEnum.ZnodeUser.ToString(), StringComparison.InvariantCultureIgnoreCase)) SetExpands(ZnodeAccountUserOrderApprovalEnum.ZnodeUser.ToString(), navigationProperties);
                    if (Equals(key, FilterKeys.WishLists)) SetExpands(ZnodeUserEnum.ZnodeUserWishLists.ToString(), navigationProperties);
                    if (Equals(key, FilterKeys.Profiles)) SetExpands(ZnodeUserEnum.ZnodeUserProfiles.ToString(), navigationProperties);
                }
            }
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return navigationProperties;
        }


        private void SendResetPasswordMail(ApplicationUser userDetails, string passwordResetToken, string userName, int portalId, string firstName = "")
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { userName = userName, portalId = portalId });

            //check whether the current login user has B2B role or not.
            bool isAdminUser = IsAdminUser(userDetails);

            string baseUrl = !isAdminUser ? GetDomains(portalId) : ZnodeApiSettings.AdminWebsiteUrl;

            //Get Portal Details.
            PortalModel portalModel = GetCustomPortalDetails(portalId);

            //Set the Http protocol based on the portal setting.
            baseUrl = !isAdminUser ? (IsNotNull(portalModel) && portalModel.IsEnableSSL) ? $"https://{baseUrl}" : $"http://{baseUrl}" : baseUrl;

            string passwordResetUrl = $"{baseUrl}/User/ResetPassword?passwordToken={WebUtility.UrlEncode(passwordResetToken)}&userName={WebUtility.UrlEncode(userName)}";
            string passwordResetLink = $"<a href=\"{passwordResetUrl}\"> here</a>";

            // Genrate Reset Password Email Content.
            string subject = string.Empty;
            //Set to True to send this email in HTML format.
            bool isHtml = false;
            bool isEnableBcc = false;
            try
            {
                string messageText = GenerateResetPasswordEmail(firstName, passwordResetUrl, passwordResetLink, portalId, out subject, out isHtml, out isEnableBcc);
                ZnodeLogging.LogMessage("Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { messageText = messageText });

                if (!string.IsNullOrEmpty(messageText))
                    //This method is used to send an email.          
                    if (IsNotNull(portalModel) && portalId > 0)
                        ZnodeEmail.SendEmail(portalId, userDetails.Email, portalModel?.AdministratorEmail, ZnodeEmail.GetBccEmail(isEnableBcc, portalId, string.Empty), subject, messageText, isHtml);
                    else
                        ZnodeEmail.SendEmail(userDetails.Email, ZnodeConfigManager.SiteConfig.AdminEmail, ZnodeEmail.GetBccEmail(isEnableBcc, portalId, string.Empty), subject, messageText, isHtml);
                else
                    throw new ZnodeException(ErrorCodes.EmailTemplateDoesNotExists, Admin_Resources.ErrorResetPasswordLinkReset);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Admin.ToString(), TraceLevel.Error);
                throw new ZnodeException(ErrorCodes.ErrorSendResetPasswordLink, Admin_Resources.ErrorSendResetPasswordLink);
            }
        }

        //Generate Reset Password Email Content based on the Email Template
        private string GenerateResetPasswordEmail(string firstName, string resetUrl, string resetLink, int portalId, out string subject, out bool isHtml, out bool isEnableBcc, int localeId = 0, bool isUserCreateFromAdmin = false)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { firstName = firstName, portalId = portalId });

            // Send email to the user
            string messageText = string.Empty;
            subject = "Reset Password Notification";
            isHtml = false;
            isEnableBcc = false;
            //Method to get Email Template Details.
            EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode((isUserCreateFromAdmin ? ZnodeConstant.NewCustomerAccountFromAdmin : ZnodeConstant.ResetPassword), portalId, localeId);
            if (IsNotNull(emailTemplateMapperModel))
            {
                string storeLogo = GetCustomPortalDetails(portalId)?.StoreLogo;
                ZnodeLogging.LogMessage("Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info, new { storeLogo = storeLogo });

                //Set Parameters for the Email Templates to be get replaced.
                SetParameterOfEmailTemplate(firstName, resetUrl, resetLink, storeLogo);

                //Replace the Email Template Keys, based on the passed email template parameters.
                messageText = EmailTemplateHelper.ReplaceTemplateTokens(emailTemplateMapperModel.Descriptions);
                subject = emailTemplateMapperModel.Subject;
                isHtml = true;
                isEnableBcc = emailTemplateMapperModel.IsEnableBcc;
            }
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return messageText;
        }

        //Set Parameters for the Email Templates to be get replaced.
        private static void SetParameterOfEmailTemplate(string firstName, string resetUrl, string resetLink, string storeLogo)
        {
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { firstName = firstName });

            EmailTemplateHelper.SetParameter("#FirstName#", firstName);
            EmailTemplateHelper.SetParameter("#UserName#", firstName);
            EmailTemplateHelper.SetParameter("#Link#", resetLink);
            EmailTemplateHelper.SetParameter("#Url#", resetUrl);
            EmailTemplateHelper.SetParameter("#StoreLogo#", storeLogo);
        }

        //private void ExpandProfiles(NameValueCollection expands, UserModel user)
        //{
        //    IZnodeRepository<ZnodePublishCatalog> _publishCatalogRepository = new ZnodeRepository<ZnodePublishCatalog>();
        //    ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
        //    if (!string.IsNullOrEmpty(expands.Get(FilterKeys.Profiles)))
        //    {
        //        //Get the List of Profile associated with the user.
        //        user.Profiles = (from userProfile in _userProfileRepository.Table
        //                         join profile in _profileRepository.Table on userProfile.ProfileId equals profile.ProfileId
        //                         join publishCatalog in _publishCatalogRepository.Table on profile.PimCatalogId equals publishCatalog.PimCatalogId into _childPublishCatalog
        //                         from childPublishCatalog in _childPublishCatalog.DefaultIfEmpty()
        //                         where userProfile.UserId == user.UserId
        //                         select new ProfileModel
        //                         {
        //                             IsDefault = userProfile.IsDefault,
        //                             ProfileId = profile.ProfileId,
        //                             ProfileName = profile.ProfileName,
        //                             Weighting = profile.Weighting,
        //                             ShowOnPartnerSignup = profile.ShowOnPartnerSignup,
        //                             TaxExempt = profile.TaxExempt,
        //                             DefaultExternalAccountNo = profile.DefaultExternalAccountNo,
        //                             PimCatalogId = profile.PimCatalogId,
        //                             PublishCatalogId = childPublishCatalog.PublishCatalogId
        //                         }).ToList();


        //    }
        //}

        private void InvalidLoginError(ApplicationUser user, bool isVerified, UserVerificationTypeEnum UserVerificaionTypeCode)
        {
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, user?.Id);
            if (!Equals(user, null))
            {
                if (!isVerified && UserVerificaionTypeCode != UserVerificationTypeEnum.None)
                {
                    if (UserVerificationTypeEnum.AdminApprovalCode == UserVerificaionTypeCode)
                    {
                        ZnodeLogging.LogActivity((int)ZnodeLogging.ErrorNum.LoginFailed, string.Empty, string.Empty, null, Admin_Resources.LogInFailed, null);
                        throw new ZnodeUnauthorizedException(ErrorCodes.AdminApproval, string.Empty, HttpStatusCode.Unauthorized);
                    }
                    else
                    {
                        ZnodeLogging.LogActivity((int)ZnodeLogging.ErrorNum.LoginFailed, string.Empty, string.Empty, null, Admin_Resources.LogInFailed, null);
                        throw new ZnodeUnauthorizedException(ErrorCodes.LoginFailed, string.Empty, HttpStatusCode.Unauthorized);
                    }

                }
                else if (UserManager.IsLockedOut(user.Id))
                {
                    ZnodeLogging.LogActivity((int)ZnodeLogging.ErrorNum.LoginFailed, string.Empty, string.Empty, null, "Login failed", null);
                    throw new ZnodeUnauthorizedException(ErrorCodes.AccountLocked, WebStore_Resources.AccountLockedErrorMessage, HttpStatusCode.Unauthorized);
                }
                else
                {
                    //Gets current failed password atttemt count for setting the message.
                    int inValidAttemtCount = user.AccessFailedCount;

                    //Gets Maximum failed password atttemt count from web.config
                    int maxInvalidPasswordAttemptCount = UserManager.MaxFailedAccessAttemptsBeforeLockout;
                    ZnodeLogging.LogMessage("Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { inValidAttemtCount = inValidAttemtCount, maxInvalidPasswordAttemptCount = maxInvalidPasswordAttemptCount });

                    if (inValidAttemtCount > 0 && maxInvalidPasswordAttemptCount > 0)
                    {
                        if (maxInvalidPasswordAttemptCount <= inValidAttemtCount)
                        {
                            ZnodeLogging.LogActivity((int)ZnodeLogging.ErrorNum.LoginFailed, string.Empty, string.Empty, null, Admin_Resources.ErrorLogin, null);
                            throw new ZnodeUnauthorizedException(ErrorCodes.AccountLocked, string.Empty, HttpStatusCode.Unauthorized);
                        }

                        //Set warning error at (MaxFailureAttemptCount - 2) & (MaxFailureAttemptCount - 1) attempt.
                        if (Equals((maxInvalidPasswordAttemptCount - inValidAttemtCount), 2))
                        {
                            ZnodeLogging.LogActivity((int)ZnodeLogging.ErrorNum.LoginFailed, string.Empty, string.Empty, null, Admin_Resources.ErrorLogin, null);
                            throw new ZnodeUnauthorizedException(ErrorCodes.TwoAttemptsToAccountLocked, string.Empty, HttpStatusCode.Unauthorized);
                        }
                        else if (Equals((maxInvalidPasswordAttemptCount - inValidAttemtCount), 1))
                        {
                            ZnodeLogging.LogActivity((int)ZnodeLogging.ErrorNum.LoginFailed, string.Empty, string.Empty, null, Admin_Resources.ErrorLogin, null);
                            throw new ZnodeUnauthorizedException(ErrorCodes.OneAttemptToAccountLocked, string.Empty, HttpStatusCode.Unauthorized);
                        }
                        else
                        {
                            ZnodeLogging.LogActivity((int)ZnodeLogging.ErrorNum.LoginFailed, string.Empty, string.Empty, null, Admin_Resources.ErrorUsernameAndPassword, null);
                            throw new ZnodeUnauthorizedException(ErrorCodes.LoginFailed, string.Empty, HttpStatusCode.Unauthorized);
                        }
                    }
                    else
                    {
                        ZnodeLogging.LogActivity((int)ZnodeLogging.ErrorNum.LoginFailed, string.Empty, string.Empty, null, Admin_Resources.ErrorUsernameAndPassword, null);
                        throw new ZnodeUnauthorizedException(ErrorCodes.LoginFailed, string.Empty, HttpStatusCode.Unauthorized);
                    }
                }
            }
            else
            {
                ZnodeLogging.LogActivity((int)ZnodeLogging.ErrorNum.LoginFailed, string.Empty, string.Empty, null, Admin_Resources.ErrorUsernameAndPassword, null);
                throw new ZnodeUnauthorizedException(ErrorCodes.LoginFailed, string.Empty, HttpStatusCode.Unauthorized);
            }
        }

        //Get the Login User Details
        private ApplicationUser GetAspNetUserDetails(ref UserModel model, int portalId)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { portalId = portalId });

            if (IsNull(model))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.UserModelNotNull);

            if (IsNull(model.User))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.LoginUserModelNotNull);

            //Get the User Details from Znode Mapper
            AspNetZnodeUser znodeUser = _loginHelper.GetUser(model, (model?.PortalId > 0) ? Convert.ToInt32(model.PortalId) : portalId);
            if (IsNull(znodeUser))
                throw new ZnodeException(ErrorCodes.InvalidData, string.Format(Admin_Resources.ErrorUserNotExist, model.User.Username));

            //Bind the Aspnet Znode User Id.
            model.AspNetZnodeUserId = znodeUser.AspNetZnodeUserId;

            //Get the Login User Details from Owin Mapper.
            var user = UserManager.FindByName(znodeUser.AspNetZnodeUserId);
            if (IsNull(user))
                throw new ZnodeException(ErrorCodes.InvalidData, string.Format(Admin_Resources.ErrorUserExist, model.User.Username));
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return user;
        }

        private UserModel GetUserAccountCatalogDetails(UserModel model)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { AccountId = model?.AccountId });

            GetPublishCatalogId(model);

            IZnodeRepository<ZnodeAccount> _accountRepository = new ZnodeRepository<ZnodeAccount>();

            if (model.AccountId > 0)
            {
                ZnodeAccount accountDetails = _accountRepository.GetById(model.AccountId.GetValueOrDefault());
                model.Custom3 = accountDetails?.ExternalId;
                model.Custom4 = accountDetails?.Name;
                if (accountDetails?.PublishCatalogId >= 0)
                    model.PublishCatalogId = accountDetails?.PublishCatalogId;
                else if (accountDetails?.ParentAccountId > 0)
                {
                    ZnodeAccount parentAccountDetails = _accountRepository.GetById(accountDetails.ParentAccountId.GetValueOrDefault());
                    if (parentAccountDetails?.PublishCatalogId > 0)
                        model.PublishCatalogId = parentAccountDetails.PublishCatalogId;
                }
            }
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return model;
        }

        private void GetPublishCatalogId(UserModel model)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { AccountId = model?.AccountId });

            int? portalCatalogId = _portalCatalogRepository.Table.Where(x => x.PortalId == model.PortalId)?.FirstOrDefault()?.PublishCatalogId;

            ZnodeLogging.LogMessage("PublishCatalogId:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, portalCatalogId);

            if (portalCatalogId > 0)
                model.PublishCatalogId = portalCatalogId;
        }

        public AddressModel GetAdyenShippingAddress(AddressModel addressModel)
        {
            IZnodeRepository<ZnodeUserAddress> znodeAddressRepository = new ZnodeRepository<ZnodeUserAddress>();

            AddressModel userAddressModel = znodeAddressRepository.Table.FirstOrDefault(xx => xx.AddressId == addressModel.AddressId).ToModel<AddressModel>();

            if (HelperUtility.IsNotNull(userAddressModel) && userAddressModel.AddressId == addressModel.AddressId)
            {
                return addressModel;
            }
            return userAddressModel;
        }
        #endregion

    }
}
