﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;

namespace Znode.Api.Custom.Service.Service
{
    public class AriatWebStoreWidgetService : WebStoreWidgetService
    {

        protected override WebStoreWidgetProductListModel ToWebstoreWidgetProductListModel(List<PublishProductModel> model, List<ZnodePublishWidgetProductEntity> productListWidgetEntity)
        {
            WebStoreWidgetProductListModel webStoreWidgetProductListModel = new WebStoreWidgetProductListModel();
            webStoreWidgetProductListModel.Products = new List<WebStoreWidgetProductModel>();

            if (HelperUtility.IsNotNull(model) && model?.Count > 0)
            {
                foreach (PublishProductModel data in model)
                {
                    WebStoreWidgetProductModel webStoreWidgetProductModel = new WebStoreWidgetProductModel();
                    webStoreWidgetProductModel.WebStoreProductModel = MapData(data, productListWidgetEntity);
                    webStoreWidgetProductListModel.Products.Add(webStoreWidgetProductModel);

                }
            }
            ZnodeLogging.LogMessage("webStoreWidgetProductListModel list count: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, webStoreWidgetProductListModel?.Products?.Count);
            webStoreWidgetProductListModel.Products = webStoreWidgetProductListModel.Products?.OrderBy(x => x.WebStoreProductModel.DisplyOrder).ToList();
            return webStoreWidgetProductListModel;
        }

        //Map PublishProductModel to WebStoreProductModel.
        protected override WebStoreProductModel MapData(PublishProductModel model, List<ZnodePublishWidgetProductEntity> productListWidgetEntity)
        {
            return new WebStoreProductModel
            {
                Attributes = model.Attributes,
                LocaleId = model.LocaleId,
                CatalogId = model.PublishedCatalogId,
                PublishProductId = model.PublishProductId,
                Name = model.Name,
                RetailPrice = model.RetailPrice,
                SalesPrice = model.SalesPrice,
                PromotionalPrice = model.PromotionalPrice,
                SKU = model.SKU,
                ProductReviews = model.ProductReviews,
                CurrencyCode = model.CurrencyCode,
                CultureCode = model.CultureCode,
                ImageMediumPath = model.ImageMediumPath,
                ImageSmallPath = model.ImageSmallPath,
                ImageSmallThumbnailPath = model.ImageSmallThumbnailPath,
                ImageThumbNailPath = model.ImageThumbNailPath,
                ImageLargePath = model.ImageLargePath,
                SEODescription = model.SEODescription,
                SEOTitle = model.SEOTitle,
                Rating = model.Rating,
                TotalReviews = model.TotalReviews,
                SEOUrl = model.SEOUrl,
                SEOKeywords = model.SEOKeywords,
                GroupProductPriceMessage = model.GroupProductPriceMessage,
                Promotions = model.Promotions,
                DisplyOrder = productListWidgetEntity?.FirstOrDefault(x => x?.SKU == model?.SKU)?.DisplayOrder.GetValueOrDefault(),
                ProductType = model.ProductType,
                Custom2 = model.Custom2,
                Custom3 = model.Custom3
            };
        }
    }
}
