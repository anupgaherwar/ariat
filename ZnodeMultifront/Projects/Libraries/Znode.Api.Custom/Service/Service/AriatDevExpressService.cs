﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Diagnostics;
using System.Linq;

using Znode.Custom.Api.Model;
using Znode.Engine.Services;
using Znode.Libraries.Data;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;

namespace Znode.Api.Custom.Service
{
    public class AriatDevExpressService : BaseService, IAriatDevExpressService
    {

        //Get voucher report details.
        public AriatVoucherReportListModel GetVoucherReport(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Reports.ToString(), TraceLevel.Info);

            object beginDate, endDate, storesNames, accountName;
            RemoveParametersGetValue(filters, FilterKeys.BeginDate, out beginDate);
            RemoveParametersGetValue(filters, FilterKeys.EndDate, out endDate);
            RemoveParametersGetValue(filters, FilterKeys.StoresName, out storesNames);
            RemoveParametersGetValue(filters, "accountnames", out accountName);
            ZnodeLogging.LogMessage("beginDate, endDate, storesNames, discountType values to set SP parameters to get coupon list with currency: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, new object[] { beginDate, endDate, storesNames });
            PageListModel pageListModel = new PageListModel(filters, sorts, page);

            string[] accountnames = accountName?.ToString().Split('|');

            DataTable table = new DataTable("AccountNames");
            table.Columns.Add("AccountName", typeof(string));

            if (accountnames?.Count() > 0)
            {
                foreach (string item in accountnames)
                {
                    table.Rows.Add(item);
                }
            }

            string cmdString = "ZnodeDevExpressReport_GetVoucher @BeginDate, @EndDate, @StoreName, @AccountNames";

            IZnodeViewRepository<AriatVoucherReportModel> objStoredProc = new ZnodeViewRepository<AriatVoucherReportModel>();
            objStoredProc.SetParameter("@BeginDate", beginDate, ParameterDirection.Input, DbType.DateTime);
            objStoredProc.SetParameter("@EndDate", endDate, ParameterDirection.Input, DbType.DateTime);
            objStoredProc.SetParameter("@StoreName", GetWithoutCurrencyStore(Convert.ToString(storesNames)), ParameterDirection.Input, DbType.String);
            objStoredProc.SetTableValueParameter("@AccountNames", table, ParameterDirection.Input, SqlDbType.Structured, "dbo.SelectColumnList");
            List<AriatVoucherReportModel> voucherReportModels = objStoredProc.ExecuteStoredProcedureList(cmdString).ToList();

            AriatVoucherReportListModel listModel = new AriatVoucherReportListModel
            {
                VoucherList = voucherReportModels
            };
            return listModel;
        }

        private void RemoveParametersGetValue(FilterCollection filters, string paramName, out object paramValue)
        {
            //Remove begin date from filters collection
            paramValue = filters?.Find(x => string.Equals(x.FilterName, paramName, StringComparison.CurrentCultureIgnoreCase))?.Item3;
            filters?.RemoveAll(x => string.Equals(x.FilterName, paramName, StringComparison.CurrentCultureIgnoreCase));
        }

        private object GetWithoutCurrencyStore(string storeNames)
        {
            string[] stores = storeNames?.Split('|');
            if (stores?.FirstOrDefault()?.Contains(" (") ?? false)
            {
                List<string> updatedStores = new List<string>();
                foreach (string store in stores)
                {
                    if (store.LastIndexOf(" (") >= 0)
                    {
                        updatedStores.Add(store.Substring(0, store.LastIndexOf(" (")));
                    }
                    else
                    {
                        updatedStores.Add(store);
                    }
                }
                return string.Join("|", updatedStores);
            }
            return storeNames;
        }
    }
}
