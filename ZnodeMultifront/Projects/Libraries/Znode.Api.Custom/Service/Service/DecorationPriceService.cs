﻿using MongoDB.Driver;

using System.Collections.Generic;
using System.Linq;

using Znode.Custom.Api.Model;
using Znode.Custom.Api.Model.ParameterModel;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Libraries.Admin;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Api.Custom.Service.Service
{
    public class DecorationPriceService : BaseService, IDecorationPriceService
    {
        #region Private variable
        private readonly IPublishProductHelper _publishProductHelper;
        private readonly IPublishedProductDataService publishProductData;
        #endregion

        #region Constructor

        public DecorationPriceService(IPublishProductHelper publishProductHelper)
        {
            _publishProductHelper = publishProductHelper;
            publishProductData = ZnodeDependencyResolver.GetService<IPublishedProductDataService>();
        }
        #endregion

        //Get decoration price 
        public DecorationPriceModel GetDecorationPrice(PriceParameterModel model)
        {
            var cataLogVersionId = GetCatalogVersionId(model.PublishCatalogId) ?? 0;
            string productName = publishProductData.GetProductNameBySKU(model.SKU, model.LocaleId, cataLogVersionId);

            List<PriceSKUModel> priceList = _publishProductHelper.GetPricingBySKUs(new List<string> { model.SKU, model.LogoSKU, model.LargeLogoSKU, model.TextLineSKU }, model.PortalId, GetLoginUserId(), model.ProfileId);

            DecorationPriceModel decorationPriceModel = new DecorationPriceModel();
            decorationPriceModel.Name = productName;
            if (priceList.Count > 0)
            {
                //Get Product unit price
                decorationPriceModel.UnitProductPrice = GetUnitPrice(priceList, model.SKU, model.Quantity);
                //Get Regular logos unit price
                decorationPriceModel.UnitRegularPrice = GetUnitPrice(priceList, model.LogoSKU, model.LogoQuantity);
                //Get large logos unit price
                decorationPriceModel.UnitLargeLogoPrice = GetUnitPrice(priceList, model.LargeLogoSKU, model.LargeLogoQuantity);
                //Get text line unit price
                decorationPriceModel.UnitTextPrice = GetUnitPrice(priceList, model.TextLineSKU, model.TextLineQuantity);
            }
            return decorationPriceModel;
        }

        //Get unit price of sku
        public decimal GetUnitPrice(List<PriceSKUModel> priceSKUModels, string sku, int quantity)
        {
            var priceSKU = priceSKUModels.Where(x => x.SKU == sku).ToList();
            var TierPriceList = new List<PriceTierModel>();
            int Count = 1;

            if (priceSKU?.Count() > 0)
            {
                foreach (PriceSKUModel tierPriceSKU in priceSKU)
                {
                    if (HelperUtility.IsNotNull(tierPriceSKU.TierPrice) && HelperUtility.IsNotNull(tierPriceSKU.TierQuantity))
                    {
                        PriceTierModel tierPrice = new PriceTierModel();
                        tierPrice.Price = tierPriceSKU.TierPrice;
                        tierPrice.Quantity = tierPriceSKU.TierQuantity;
                        tierPrice.MinQuantity = tierPriceSKU.TierQuantity;
                        tierPrice.Custom1 = tierPriceSKU.Custom1;
                        tierPrice.Custom2 = tierPriceSKU.Custom2;
                        tierPrice.Custom3 = tierPriceSKU.Custom3;
                        if (HelperUtility.IsNotNull(priceSKU.ElementAtOrDefault(Count)))
                            tierPrice.MaxQuantity = priceSKU.ElementAt(Count).TierQuantity;
                        else
                            tierPrice.MaxQuantity = decimal.MaxValue;

                        TierPriceList.Add(tierPrice);
                        Count++;
                    }
                }
                decimal price = 0;

                PriceSKUModel priceDetails = priceSKU.FirstOrDefault();

                if (HelperUtility.IsNotNull(priceDetails))
                    price = (priceDetails.SalesPrice > 0 ? priceDetails.SalesPrice : priceDetails.RetailPrice).GetValueOrDefault();

                if (TierPriceList.Count > 0)
                {
                    foreach (PriceTierModel productTieredPrice in TierPriceList)
                    {
                        //check if tier quantity is valid or not.
                        if (quantity >= productTieredPrice.MinQuantity && quantity < productTieredPrice.MaxQuantity)
                        {
                            price = productTieredPrice.Price.GetValueOrDefault();
                            break;
                        }
                    }
                }
                return price;
            }

            return 0;
        }
    }
}
