﻿using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;

using Znode.Api.Custom.Service.IService;
using Znode.Custom.Api.Model;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Libraries.Data;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;

namespace Znode.Api.Custom.Service
{
    public class AriatProductService : PublishProductServiceV2, IAriatProductService
    {

        #region Private Variables
        private readonly IPublishedProductDataService publishProductData;
        #endregion

        #region Constructor
        public AriatProductService()
        {
            publishProductData = ZnodeDependencyResolver.GetService<IPublishedProductDataService>();
        }
        #endregion

        #region public methods
        //get znode sku from artifi sku.
        public ZnodeSKURequestModel GetZnodeSKU(ZnodeSKURequestModel znodeSKURequestModel)
        {
            Dictionary<string, string> keyValuePairs = new Dictionary<string, string>();
            keyValuePairs.Add("ArtifiSKU", znodeSKURequestModel.SKU);

            foreach (var item in znodeSKURequestModel.Attributes)
            {
                keyValuePairs.Add(item.Key, item.Value);
            }
            string json = JsonConvert.SerializeObject(keyValuePairs);

            var versionId = GetCatalogVersionId(znodeSKURequestModel.PublishCatalogId, znodeSKURequestModel.LocaleId);
            IZnodeViewRepository<ZnodeSKURequestModel> skuPrice = new ZnodeViewRepository<ZnodeSKURequestModel>();
            skuPrice.SetParameter("@ZnodeCatalogId", znodeSKURequestModel.PublishCatalogId, ParameterDirection.Input, DbType.Int32);
            skuPrice.SetParameter("@LocaleId", znodeSKURequestModel.LocaleId, ParameterDirection.Input, DbType.Int32);
            skuPrice.SetParameter("@VersionId", versionId, ParameterDirection.Input, DbType.Int32);
            skuPrice.SetParameter("@jsonfile ", json, ParameterDirection.Input, DbType.String);

            List<ZnodeSKURequestModel> model = skuPrice.ExecuteStoredProcedureList("ZnodeGetZnodePublishProductEntityData @ZnodeCatalogId,@LocaleId,@VersionId,@jsonfile")?.ToList();

            if (model?.Count > 0)
            {
                ZnodeSKURequestModel skuDetails = new ZnodeSKURequestModel();
                var data = model.FirstOrDefault();
                skuDetails.SKU = data.SKU;
                var product = publishProductData.GetPublishProductBySKU(data.SKU, znodeSKURequestModel.PublishCatalogId, znodeSKURequestModel.LocaleId, versionId);
                if (HelperUtility.IsNotNull(product))
                {
                    var publishModel = JsonConvert.DeserializeObject<List<PublishAttributeModel>>(product.Attributes);
                    skuDetails.ArtifiSKU = publishModel.FirstOrDefault(x => x.AttributeCode == "ArtifiSKU")?.AttributeValues;
                    skuDetails.StyleNumber = publishModel.FirstOrDefault(x => x.AttributeCode == "StyleNumber")?.AttributeValues;
                }
                return skuDetails;
            }
            return null;
        }

        //Get publish product from artifi sku.
        public virtual PublishProductListModel GetPublishProductsByAttribute(FilterCollection filters)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            int catalogId, portalId, localeId, userId;
            GetParametersValueForFilters(filters, out catalogId, out portalId, out localeId);
            ZnodeLogging.LogMessage("catalogId, portalId and localeId: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new object[] { catalogId, portalId, localeId });

            int.TryParse(filters.FirstOrDefault(x => x.FilterName.Equals(FilterKeys.UserId, StringComparison.InvariantCultureIgnoreCase))?.FilterValue, out userId);

            filters.RemoveAll(x => x.FilterName == FilterKeys.PortalId);
            filters.RemoveAll(x => x.FilterName == FilterKeys.UserId);

            //get catalog current version id by catalog id.
            if (catalogId > 0)
                filters.Add(WebStoreEnum.VersionId.ToString(), FilterOperators.Equals, Convert.ToString(GetCatalogVersionId(catalogId)));
            else
                filters.Add(WebStoreEnum.VersionId.ToString(), FilterOperators.In, GetCatalogAllVersionIds());

            if (userId > 0 && EnableProfileBasedSearch(portalId))
            {
                int? userProfileId = GetUserProfileId(userId);
                filters.Add(WebStoreEnum.ProfileIds.ToString(), FilterOperators.In, Convert.ToString(userProfileId));
            }

            //Replace filter keys with published filter keys
            ReplaceFilterKeys(ref filters);


            ModifyFiltersForAttributes(ref filters);

            PageListModel pageListModel = new PageListModel(filters, null, null);
            //get publish products 

            List<PublishProductModel> publishProducts = publishProductData.GetPublishProducts(new PageListModel(filters, null, null))?.ToModel<PublishProductModel>()?.ToList();

            PublishProductListModel publishProductListModel = new PublishProductListModel
            {
                PublishProducts = FilterProductsByAttribute(publishProducts, filters.Where(x => x.FilterName.Contains(ZnodeConstant.PublishedAttribute)).Select(y => y).ToList())
            };

            publishProductListModel.PublishProducts = Equals(pageListModel.PagingLength, int.MaxValue) ? publishProductListModel.PublishProducts.ToList()
                                                                                                       : publishProductListModel.PublishProducts.Take(pageListModel.PagingLength).ToList();

            ZnodeLogging.LogMessage("PublishProducts and locale list count: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new object[] { publishProductListModel?.PublishProducts, publishProductListModel?.Locale });

            // publishProductListModel.BindPageListModel(pageListModel);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            return publishProductListModel;
        }

        private List<PublishProductModel> FilterProductsByAttribute(List<PublishProductModel> publishedProducts, IList<FilterTuple> attribFilters)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            if ((attribFilters.Count % 2).Equals(0))
            {
                for (int i = 0, j = 1; i < attribFilters.Count(); i += 2, j += 2)
                {
                    if (publishedProducts?.Count > 0)
                    {
                        publishedProducts = publishedProducts.Where(x => x.Attributes
                                                                          .Any(y => y.AttributeCode.IndexOf(attribFilters[i].FilterValue, StringComparison.InvariantCultureIgnoreCase) >= 0
                                                                                    && ValueFilter(y, attribFilters[j].FilterValue, attribFilters[j].FilterOperator)))
                                                                          .Select(z => z).ToList();
                    }
                }
            }

            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            return publishedProducts;
        }

        private bool ValueFilter(PublishAttributeModel model, string filterValue, string filterOperator)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            bool result = false;

            if (model.SelectValues.Count > 0)
            {
                switch (model.AttributeTypeName)
                {
                    case "Simple Select":
                        result = model.SelectValues.Any(x => string.Equals(x.Code, filterValue, StringComparison.InvariantCultureIgnoreCase));
                        break;
                    case "Multi Select":
                        string[] filterValues = filterValue.Split(new char[] { ';' });
                        foreach (string filter in filterValues)
                        {
                            if ((filterOperator == FilterOperators.Contains && model.SelectValues.Any(x => string.Equals(x.Code, filter, StringComparison.InvariantCultureIgnoreCase))
                                 || (filterOperator == FilterOperators.NotContains && !model.SelectValues.Any(x => string.Equals(x.Code, filter, StringComparison.InvariantCultureIgnoreCase)))))
                                return true;
                        }

                        result = false;
                        break;
                    default:
                        result = false;
                        break;
                }
            }
            else
                result = model.AttributeValues.IndexOf(filterValue, StringComparison.InvariantCultureIgnoreCase) >= 0;

            return result;
        }
        #endregion
    }
}
