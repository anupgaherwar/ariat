﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Engine.Services;

namespace Znode.Api.Custom.Service
{
    public class AriatFormBuilderService : FormBuilderService
    {
        #region Private Variable   
       private readonly IZnodeRepository<ZnodeGlobalAttribute> _globalAttributeRepository;  
        private readonly IZnodeRepository<ZnodeGlobalAttributeGroupMapper> _groupMapperRepository;
        private readonly IZnodeRepository<ZnodeFormBuilderAttributeMapper> _formMapperRepository;
        private readonly IZnodeRepository<ZnodeAttributeType> _attributeTypeRepository;     
        private readonly IZnodeRepository<ZnodeGlobalAttributeLocale> _attributeLocaleRepository;
       
        #endregion

        #region Public Constructor
        public AriatFormBuilderService()
        {        
            _globalAttributeRepository = new ZnodeRepository<ZnodeGlobalAttribute>();        
            _groupMapperRepository = new ZnodeRepository<ZnodeGlobalAttributeGroupMapper>();
            _formMapperRepository = new ZnodeRepository<ZnodeFormBuilderAttributeMapper>();
            _attributeTypeRepository = new ZnodeRepository<ZnodeAttributeType>();             
            _attributeLocaleRepository = new ZnodeRepository<ZnodeGlobalAttributeLocale>();
           
        }
        #endregion

        #region Public Methods
        public override List<GlobalAttributeModel> AttributesAssignedToFormForRMA(int formBuilderId, int localeId, string attributeIds)
        {
            List<int> listattributeId = new List<int>(Array.ConvertAll(attributeIds.Split(','), int.Parse));
            List<GlobalAttributeModel> attributes = new List<GlobalAttributeModel>();
            attributes = (from grpMap in _attributeLocaleRepository.Table
                          join formMap in _formMapperRepository.Table
                          on grpMap.GlobalAttributeId equals formMap.GlobalAttributeId
                          join glbAttribute in _globalAttributeRepository.Table
                          on grpMap.GlobalAttributeId equals glbAttribute.GlobalAttributeId
                          join attributetype in _attributeTypeRepository.Table
                          on glbAttribute.AttributeTypeId equals attributetype.AttributeTypeId
                          where listattributeId.Contains(grpMap.GlobalAttributeId.Value)
                          && formMap.FormBuilderId == formBuilderId
                          && grpMap.LocaleId == localeId
                          select new GlobalAttributeModel
                          {
                              AttributeName = grpMap.AttributeName,
                              GlobalAttributeId = formMap.GlobalAttributeId.HasValue ? formMap.GlobalAttributeId.Value : 0,
                              AttributeTypeId = glbAttribute.AttributeTypeId,
                              AttributeTypeName = attributetype.AttributeTypeName
                          }).ToList();

            if (attributes.Count <= 0)
            {
                attributes = (from grpMap in _attributeLocaleRepository.Table
                              join gagm in _groupMapperRepository.Table
                              on grpMap.GlobalAttributeId equals gagm.GlobalAttributeId
                              from formMap in _formMapperRepository.Table.Where(x => x.GlobalAttributeId == gagm.GlobalAttributeId || x.GlobalAttributeGroupId == gagm.GlobalAttributeGroupId)
                              join glbAttribute in _globalAttributeRepository.Table
                              on grpMap.GlobalAttributeId equals glbAttribute.GlobalAttributeId
                              join attributetype in _attributeTypeRepository.Table
                              on glbAttribute.AttributeTypeId equals attributetype.AttributeTypeId
                              where listattributeId.Contains(grpMap.GlobalAttributeId.Value)
                              && formMap.FormBuilderId == formBuilderId
                              && grpMap.LocaleId == localeId
                              select new GlobalAttributeModel
                              {
                                  AttributeName = grpMap.AttributeName,
                                  GlobalAttributeId = formMap.GlobalAttributeId.HasValue ? formMap.GlobalAttributeId.Value : grpMap.GlobalAttributeId.HasValue ? grpMap.GlobalAttributeId.Value : 0,
                                  AttributeTypeId = glbAttribute.AttributeTypeId,
                                  AttributeTypeName = attributetype.AttributeTypeName
                              }).ToList();
            }

            attributes?.RemoveAll(x => x.AttributeTypeName.ToLower() == ZnodeConstant.File);
            return attributes;
        } 
        #endregion
    }
}
