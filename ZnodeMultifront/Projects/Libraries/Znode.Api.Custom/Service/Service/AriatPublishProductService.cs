﻿using MongoDB.Driver;

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;

using Znode.Custom.Data;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Admin;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;

using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;

namespace Znode.Engine.Services
{
    public class AriatPublishProductService : PublishProductService
    {
        #region Private Varaibles
        //private readonly IMongoRepository<CategoryEntity> _categoryMongoRepository;
        private readonly IPublishProductHelper publishProductHelper;
        IZnodeRepository<AriatProgramCatalogProduct> _ariatProgramCatalogProduct;
        #endregion

        #region Constants
        const string AriatColorAttribute = "ColorSpecific";
        const string AriatProductImageAttribute = "ProductImage";
        #endregion

        public AriatPublishProductService()
        {
            //_categoryMongoRepository = new MongoRepository<CategoryEntity>(GetCatalogVersionId());
            publishProductHelper = ZnodeDependencyResolver.GetService<IPublishProductHelper>();
            _ariatProgramCatalogProduct = new ZnodeRepository<AriatProgramCatalogProduct>(new Custom_Entities());
        }

        #region Proctected method
        public override PublishProductModel GetPublishProduct(int publishProductId, FilterCollection filters, NameValueCollection expands)
        {
            ZnodeLogging.LogMessage("Input Parameters publishProductId:", string.Empty, TraceLevel.Info, publishProductId);

            bool isChildPersonalizableAttribute = false;
            List<PublishAttributeModel> parentPersonalizableAttributes = null;
            int portalId, localeId;
            int? catalogVersionId;
            PublishProductModel publishProduct = null;
            List<PublishProductModel> products;
            string parentProductImageName = null;

            //Get publish product 
            products = GetPublishProductFromPublishedData(publishProductId, filters, out portalId, out localeId, out catalogVersionId, publishProduct, out products);

            List<int> associatedCategoryIds = new List<int>();

            if (HelperUtility.IsNotNull(products) && products.Count > 0)
            {
                List<int> categoryIds = products.Select(x => x.ZnodeCategoryIds)?.ToList();

                FilterCollection filter = new FilterCollection();
                if (categoryIds?.Count > 0)
                    filter.Add("ZnodeCategoryId", FilterOperators.In, string.Join(",", categoryIds));
                filter.Add("IsActive", FilterOperators.Equals, ZnodeConstant.TrueValue);
                filter.Add("VersionId", FilterOperators.Equals, catalogVersionId.ToString());

                List<PublishedCategoryEntityModel> categoryEntities = GetService<IPublishedCategoryDataService>().GetPublishedCategoryList(new PageListModel(filter, null, null))?.ToModel<PublishedCategoryEntityModel>()?.ToList();

                associatedCategoryIds.AddRange(categoryEntities.Select(x => x.ZnodeCategoryId));

                //If no category associated to product then perform else part
                if (associatedCategoryIds?.Count > 0)
                    publishProduct = products.FirstOrDefault(x => associatedCategoryIds.Contains(x.ZnodeCategoryIds));
                else
                    publishProduct = products?.FirstOrDefault();

                parentProductImageName = publishProduct?.Attributes.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.ProductImage)?.AttributeValues;
            }

            if (HelperUtility.IsNotNull(publishProduct))
            {
                List<PublishProductModel> associatedProducts = GetService<IPublishedProductDataService>().GetAssociatedConfigurableProducts(publishProductId, localeId, catalogVersionId).ToModel<PublishProductModel>()?.ToList();
                List<PublishProductModel> associatedProductsForCatalog = new List<PublishProductModel>();
                if (associatedProducts?.Count > 0)
                {
                    string accountExternalId = GetHeaderValue("AccountExternalId");
                    if (!string.IsNullOrEmpty(accountExternalId))
                    {
                        FilterCollection filter = new FilterCollection();
                        List<string> styleNumbers = new List<string>();
                        filter.Add(new FilterTuple("AccountExternalID", FilterOperators.Is, accountExternalId));
                        EntityWhereClauseModel whereClauseModel = DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filter.ToFilterDataCollection());
                        styleNumbers = _ariatProgramCatalogProduct.GetEntityListWithoutOrderBy(whereClauseModel.WhereClause, whereClauseModel.FilterValues)?.Select(x => x.StyleNumber)?.ToList();                                                                      
                        if (styleNumbers.Count > 0)
                        {
                            associatedProductsForCatalog =  GetProgramCatalogAssociatedProducts(associatedProducts, styleNumbers).ToList<PublishProductModel>();
                        }
                    }
                    if (associatedProductsForCatalog.Count > 0)
                    {
                        associatedProducts = null;
                        associatedProducts = associatedProductsForCatalog;
                    }
                }
                if (associatedProducts?.Count > 0)
                {
                    associatedProducts = MapParentAttributesToChild(publishProduct, associatedProducts);
                    parentPersonalizableAttributes = publishProduct.Attributes?.Where(x => x.IsPersonalizable).ToList();
                    publishProduct = GetDefaultConfigurableProduct(expands, portalId, localeId, publishProduct, associatedProducts, null, catalogVersionId);
                }
                else
                    //Get expands associated to Product
                    publishProductHelper.GetDataFromExpands(portalId, GetExpands(expands), publishProduct, localeId, WhereClauseForPortalId(portalId), GetLoginUserId(), catalogVersionId.GetValueOrDefault(), WebstoreVersionId, GetProfileId());

                isChildPersonalizableAttribute = publishProduct.Attributes.Where(x => x.IsPersonalizable).Count() > 0;

                GetProductImagePath(portalId, publishProduct, true, parentProductImageName);

                //set stored based In Stock, Out Of Stock, Back Order Message.
                SetPortalBasedDetails(portalId, publishProduct);

                publishProduct.ZnodeProductCategoryIds = associatedCategoryIds;


            }
            return AddPersonalizeAttributeInChildProduct(publishProduct, parentPersonalizableAttributes, isChildPersonalizableAttribute);
        }

        //Get the style number assocaited prosucts.
        private IEnumerable<PublishProductModel> GetProgramCatalogAssociatedProducts(List<PublishProductModel> associatedProducts,List<string> styleNumbers)
        {
            foreach (PublishProductModel associatedProduct in associatedProducts)
            {
                if (associatedProduct?.Attributes?.Count > 0)
                {
                    string styleNumber = associatedProduct?.Attributes?.FirstOrDefault(x => x.AttributeCode == "StyleNumber")?.AttributeValues;
                    //10016256
                    bool isContains = styleNumbers.Contains(styleNumber);
                    if (isContains)
                    {
                        yield return associatedProduct;                      
                    }
                }
            }
        }

        //Map Attributes values.
        protected override List<PublishAttributeModel> MapWebStoreConfigurableAttributeData(List<List<PublishAttributeModel>> attributeList, string selectedCode, string selectedValue, Dictionary<string, string> SelectedAttributes, List<PublishProductModel> products, List<string> ConfigurableAttributeCodes, int portalId)
        {
            List<PublishAttributeModel> configurableAttributeList = new List<PublishAttributeModel>();
            List<ConfigurableAttributeModel> attributesList = new List<ConfigurableAttributeModel>();
            IImageHelper image = GetService<IImageHelper>(new ZnodeNamedParameter("PortalId", portalId));

            if (SelectedAttributes.Count <= 0 && !string.IsNullOrEmpty(attributeList?.FirstOrDefault()?.FirstOrDefault()?.AttributeCode) && !string.IsNullOrEmpty(attributeList?.FirstOrDefault()?.FirstOrDefault()?.AttributeValues))
                SelectedAttributes.Add(attributeList.FirstOrDefault().FirstOrDefault().AttributeCode, attributeList.FirstOrDefault().FirstOrDefault().AttributeValues);

            IEnumerable<PublishAttributeModel> attributes = ExitingAttributeList(selectedCode, selectedValue, products, SelectedAttributes);

            var matchAttributeList = attributes.GroupBy(w => w.AttributeCode).Select(g => new
            {
                AttributeCode = g.Key,
                AttributeValues = g.Select(c => c.AttributeValues)
            });

            foreach (List<PublishAttributeModel> attributeEntityList in attributeList)
            {
                attributesList.Clear();
                PublishAttributeModel attributesModel = new PublishAttributeModel();
                foreach (PublishAttributeModel attributeValue in attributeEntityList)
                {
                    //Check if attribute already exist in list.
                    if (!AlreadyExist(attributesList, attributeValue.AttributeValues).GetValueOrDefault())
                    {
                        IEnumerable<string> matchAttribute = matchAttributeList?.FirstOrDefault(x => x.AttributeCode == attributeValue.AttributeCode)?.AttributeValues;

                        ConfigurableAttributeModel attribute = new ConfigurableAttributeModel();

                        if (HelperUtility.IsNotNull(matchAttribute) && !matchAttribute.Contains(attributeValue.AttributeValues) && ConfigurableAttributeCodes?.Count != 1)
                            attribute.IsDisabled = true;

                        attribute.AttributeValue = attributeValue.AttributeValues;

                        //Assign the display order value of configurable type attributes.
                        attribute.SelectValues = attributeValue.SelectValues?.ToList();

                        if (attribute.SelectValues?.Count > 0)
                        {
                            AttributesSelectValuesModel selectValues = attribute.SelectValues.FirstOrDefault();

                            if (attributeValue.AttributeCode == AriatColorAttribute)
                                attribute.ImagePath = image.GetImageHttpPathSmallThumbnail(UpdateSwatchImage(attributeValue, products));
                            if (string.IsNullOrEmpty(attribute.ImagePath))
                            {
                                if (string.Equals(attributeValue.IsSwatch, ZnodeConstant.TrueValue, StringComparison.InvariantCultureIgnoreCase))
                                    attribute.ImagePath = image.GetImageHttpPathSmallThumbnail(selectValues.Path);
                            }

                            attribute.SwatchText = selectValues.SwatchText;
                            attribute.DisplayOrder = selectValues.DisplayOrder;
                        }
                        attributesList.Add(attribute);
                    }
                }

                PublishAttributeModel attributeEntity = attributeEntityList.FirstOrDefault();
                attributesModel.AttributeName = attributeEntity.AttributeName;
                attributesModel.AttributeCode = attributeEntity.AttributeCode;
                attributesModel.AttributeValues = attributeEntity.AttributeValues;
                attributesModel.IsConfigurable = attributeEntity.IsConfigurable;
                attributesModel.IsSwatch = attributeEntity.IsSwatch;
                attributesModel.SelectValues = attributeEntity.SelectValues?.ToList();
                attributesModel.DisplayOrder = attributeEntity.DisplayOrder;
                attributesModel.ConfigurableAttribute.AddRange(attributesList);
                configurableAttributeList.Add(attributesModel);
            }
            return configurableAttributeList;
        }

        public override PublishProductModel GetConfigurableProduct(ParameterProductModel productAttributes, NameValueCollection expands)
        {
            PublishProductModel product = null;
            int? versionId = GetCatalogVersionId(productAttributes.PublishCatalogId);

            FilterCollection filters = new FilterCollection();
            filters.Add("ZnodeProductId", FilterOperators.Equals, productAttributes.ParentProductId.ToString());
            filters.Add("IsActive", FilterOperators.Equals, ZnodeConstant.TrueValue);
            filters.Add("VersionId", FilterOperators.Equals, versionId.ToString());

            PublishProductModel parentProduct = GetService<IPublishedProductDataService>().GetPublishProductByFilters(filters)?.ToModel<PublishProductModel>();

            //Selecting child SKU
            IEnumerable<string> childSKU = parentProduct.Attributes.Where(x => x.IsConfigurable).SelectMany(x => x.SelectValues.OrderBy(z => z.VariantDisplayOrder).Select(y => y.VariantSKU)).Distinct();

            //Creating new query
            List<PublishProductModel> productList = GetService<IPublishedProductDataService>().GetAssociatedConfigurableProducts(parentProduct.PublishProductId, productAttributes.LocaleId, versionId).ToModel<PublishProductModel>()?.ToList();


            foreach (var item in productAttributes.SelectedAttributes)
            {

                productList = (from productentity in productList
                               from attribute in productentity.Attributes
                               where attribute.AttributeCode == item.Key && attribute.SelectValues.FirstOrDefault()?.Value == item.Value
                               select productentity).ToList();
            }

            if (productList.Count > 0)
            {
                productList = MapParentAttributesToChild(parentProduct, productList);
            }

            PublishProductModel productEntity = productList.FirstOrDefault();

            //If Combination does not exist.
            if (HelperUtility.IsNull(productEntity))
            {
                //Creating new query
                List<PublishProductModel> newproductList = GetAssociatedConfigurableProducts(productAttributes.LocaleId, versionId, childSKU);
                product = newproductList?.FirstOrDefault();
                if (HelperUtility.IsNotNull(product)) { product.IsDefaultConfigurableProduct = true; }
            }
            else
                product = productEntity;


            if (HelperUtility.IsNotNull(product))
            {
                bool isChildPersonalizableAttribute = product.Attributes.Where(x => x.IsPersonalizable).Count() > 0;

                var parentPersonalizableAttributes = parentProduct.Attributes?.Where(x => x.IsPersonalizable);
                product.AssociatedGroupProducts = MapParametersForProduct(productList);

                product.ConfigurableProductId = productAttributes.ParentProductId;
                product.IsConfigurableProduct = true;

                product.ProductType = ZnodeConstant.ConfigurableProduct;
                product.ConfigurableProductSKU = product.SKU;
                product.SKU = productAttributes.ParentProductSKU;

                publishProductHelper.GetDataFromExpands(productAttributes.PortalId, GetExpands(expands), product, productAttributes.LocaleId, string.Empty, GetLoginUserId(), null, WebstoreVersionId, GetProfileId());

                GetProductImagePath(productAttributes.PortalId, product);

                //set stored based In Stock, Out Of Stock, Back Order Message.
                SetPortalBasedDetails(productAttributes.PortalId, product);

                if (!isChildPersonalizableAttribute && parentPersonalizableAttributes?.Count() > 0)
                    product.Attributes.AddRange(parentPersonalizableAttributes);
            }
            return product;
        }

        protected override PublishProductModel GetDefaultConfigurableProduct(NameValueCollection expands, int portalId, int localeId, PublishProductModel publishProduct, List<PublishProductModel> associatedProducts, List<string> ConfigurableAttributeCodes = null, int? catalogVersionId = 0)
        {
            //Parent Product Properties
            string sku = publishProduct.SKU;
            string parentSEOCode = publishProduct.Attributes?.FirstOrDefault(x => x.AttributeCode == "SKU")?.AttributeValues;
            int categoryIds = publishProduct.ZnodeCategoryIds;
            List<PublishCategoryModel> categoryHierarchyIds = publishProduct.CategoryHierarchy;
            string parentProductImageSmallPath = publishProduct.ParentProductImageSmallPath;
            List<PublishAttributeModel> parentPersonalizableAttributes = publishProduct.Attributes?.Where(x => x.IsPersonalizable).ToList();
            string parentConfigurableProductName = publishProduct.Name;
            int configurableProductId = publishProduct.PublishProductId;

            //assign values from select values into attribute values
            associatedProducts.ForEach(x => x.Attributes.Where(y => y.IsConfigurable).ToList()
            .ForEach(z =>
            {
                z.AttributeValues = z.SelectValues.FirstOrDefault()?.Value;
            }));

            //Get first product from list of associated products 
            publishProduct = associatedProducts.FirstOrDefault();

            publishProduct.ConfigurableProductId = configurableProductId;
            publishProduct.ParentSEOCode = parentSEOCode;
            publishProduct.ConfigurableProductSKU = publishProduct.SKU;
            publishProduct.CategoryHierarchy = categoryHierarchyIds;
            publishProduct.ParentProductImageSmallPath = parentProductImageSmallPath;
            publishProduct.SKU = sku;
            publishProduct.ZnodeCategoryIds = categoryIds;
            publishProduct.IsConfigurableProduct = true;
            publishProduct.ParentConfiguarableProductName = parentConfigurableProductName;

            catalogVersionId = catalogVersionId > 0 ? catalogVersionId : GetCatalogVersionId();

            //Get expands associated to Product.
            publishProductHelper.GetDataFromExpands(portalId, GetExpands(expands), publishProduct, localeId, WhereClauseForPortalId(portalId), GetLoginUserId(), catalogVersionId, WebstoreVersionId, GetProfileId());

            PublishAttributeModel defaultAttribute = publishProduct.Attributes.FirstOrDefault(x => x.IsConfigurable);

            List<PublishAttributeModel> variants = publishProduct.Attributes?.Where(x => x.IsConfigurable).ToList();

            Dictionary<string, string> selectedAttribute = GetSelectedAttributes(variants);

            ConfigurableAttributeCodes = ConfigurableAttributeCodes ?? variants.Select(x => x.AttributeCode).ToList();

            List<PublishAttributeModel> attributeList = MapWebStoreConfigurableAttributeData(publishProductHelper.GetConfigurableAttributes(associatedProducts, ConfigurableAttributeCodes), defaultAttribute?.AttributeCode, defaultAttribute?.AttributeValues, selectedAttribute, associatedProducts, ConfigurableAttributeCodes, portalId);


            if (HelperUtility.IsNotNull(attributeList) && attributeList.Count > 0)
            {
                publishProduct.Attributes.RemoveAll(x => attributeList.Select(y => y.AttributeCode).Contains(x.AttributeCode));
                publishProduct.Attributes.AddRange(attributeList);
            }

            return publishProduct;
        }
        public virtual List<PublishProductModel> MapParentAttributesToChild(PublishProductModel parentProduct, List<PublishProductModel> productList)
        {
            PublishAttributeModel longDescription = parentProduct?.Attributes?.FirstOrDefault(x => x.AttributeCode == "LongDescription");
            PublishAttributeModel shortDescription = parentProduct?.Attributes?.FirstOrDefault(x => x.AttributeCode == "ShortDescription");
            PublishAttributeModel productSpecification = parentProduct?.Attributes?.FirstOrDefault(x => x.AttributeCode == "ProductSpecification");
            PublishAttributeModel featureDescription = parentProduct?.Attributes?.FirstOrDefault(x => x.AttributeCode == "FeatureDescription");

            foreach (PublishProductModel products in productList)
            {

                products.Attributes.RemoveAll(x => x.AttributeCode == "LongDescription" || x.AttributeCode == "ShortDescription" || x.AttributeCode == "ProductSpecification" || x.AttributeCode == "FeatureDescription");

                if (HelperUtility.IsNotNull(longDescription))
                {
                    products.Attributes.Add(longDescription);
                }
                if (HelperUtility.IsNotNull(shortDescription))
                {
                    products.Attributes.Add(shortDescription);
                }
                if (HelperUtility.IsNotNull(productSpecification))
                {
                    products.Attributes.Add(productSpecification);
                }
                if (HelperUtility.IsNotNull(featureDescription))
                {
                    products.Attributes.Add(featureDescription);
                }
            }

            return productList;
        }
        #endregion

        #region Private Method
        //Update the Swatch Image from attribute image to product image.
        private string UpdateSwatchImage(PublishAttributeModel attributeEntity, List<PublishProductModel> products)
        {
            string imageValue = string.Empty;
            if (IsNotNull(attributeEntity) && products.Count > 0)
            {
                PublishProductModel productEntity = products.FirstOrDefault(x => x.Attributes.Contains(attributeEntity));
                imageValue = productEntity?.Attributes?.FirstOrDefault(x => x.AttributeCode == AriatProductImageAttribute)?.AttributeValues;
            }
            return imageValue;
        }
        private PublishProductModel AddPersonalizeAttributeInChildProduct(PublishProductModel publishProduct, List<PublishAttributeModel> parentPersonalizableAttributes, bool isChildPersonalizableAttribute)
        {
            if (!isChildPersonalizableAttribute)
            {
                if (parentPersonalizableAttributes?.Count > 0)
                    publishProduct.Attributes.AddRange(parentPersonalizableAttributes);
            }
            return publishProduct;
        }

        private List<PublishProductModel> GetPublishProductFromPublishedData(int publishProductId, FilterCollection filters, out int portalId, out int localeId, out int? catalogVersionId, PublishProductModel publishProduct, out List<PublishProductModel> products)
        {
            //Get parameter values from filters.
            int catalogId;
            GetParametersValueForFilters(filters, out catalogId, out portalId, out localeId);

            //Remove portal id filter.
            filters.RemoveAll(x => x.FilterName == FilterKeys.PortalId);

            //Replace filter keys.
            ReplaceFilterKeys(ref filters);

            //get catalog current version id by catalog id.
            catalogVersionId = GetCatalogVersionId(catalogId);

            filters.Add(WebStoreEnum.ZnodeProductId.ToString(), FilterOperators.Equals, Convert.ToString(publishProductId));

            if (catalogVersionId > 0)
                filters.Add(FilterKeys.VersionId, FilterOperators.Equals, catalogVersionId.HasValue ? catalogVersionId.Value.ToString() : "0");

            publishProduct = null;

            //Get publish product
            products = GetService<IPublishedProductDataService>().GetPublishProducts(new PageListModel(filters, null, null)).ToModel<PublishProductModel>().ToList();

            return products;
        }
        #endregion

        protected override void BindProductDetails(PublishProductListModel publishProductListModel, int portalId, IList<PublishCategoryProductDetailModel> productDetails)
        {
            ZnodeLogging.LogMessage("Input Parameter portalId:", string.Empty, TraceLevel.Info, new { portalId = portalId });
            IImageHelper imageHelper = GetService<IImageHelper>(new ZnodeNamedParameter("PortalId", portalId));

            ZnodePortal portalDetails = GetPortalDetailsById(portalId);

            publishProductListModel?.PublishProducts?.ForEach(product =>
            {
                PublishCategoryProductDetailModel productSKU = productDetails?
                            .FirstOrDefault(productdata => productdata.SKU == product.SKU);

                if (HelperUtility.IsNotNull(productSKU))
                {
                    product.SalesPrice = productSKU.SalesPrice;
                    product.RetailPrice = productSKU.RetailPrice;
                    product.CurrencyCode = productSKU.CurrencyCode;
                    product.CultureCode = productSKU.CultureCode;
                    product.CurrencySuffix = productSKU.CurrencySuffix;
                    product.Quantity = productSKU.Quantity;
                    product.ReOrderLevel = productSKU.ReOrderLevel;
                    product.Rating = productSKU.Rating;
                    product.TotalReviews = productSKU.TotalReviews;
                    string ImageName = product.Attributes.FirstOrDefault(y => y.AttributeCode == ZnodeConstant.ProductImage)?.AttributeValues;
                    product.ImageSmallPath = imageHelper.GetImageHttpPathSmall(ImageName);
                    product.ImageMediumPath = imageHelper.GetImageHttpPathMedium(ImageName);
                    product.ImageThumbNailPath = imageHelper.GetImageHttpPathSmall(ImageName);
                    product.ImageSmallThumbnailPath = imageHelper.GetImageHttpPathSmall(ImageName);
                    product.InStockMessage = portalDetails?.InStockMsg;
                    product.OutOfStockMessage = portalDetails?.OutOfStockMsg;
                    product.BackOrderMessage = portalDetails?.BackOrderMsg;
                    product.ProductType = product.Attributes.FirstOrDefault(y => y.AttributeCode == ZnodeConstant.ProductType)?.SelectValues.FirstOrDefault()?.Value;
                    product.IsActive = Convert.ToBoolean(product.Attributes.FirstOrDefault(y => y.AttributeCode == ZnodeConstant.IsActive)?.AttributeValues);
                    product.Custom2 = productSKU.Custom2;
                    product.Custom3 = productSKU.Custom3;
                }
            });
        }
    }
}
