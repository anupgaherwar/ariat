﻿using System.Data;
using System.Diagnostics;
using System.Linq;

using Znode.Engine.Api.Models;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Admin;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.ShoppingCart;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;

using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;

namespace Znode.Engine.Services
{
    public class AriatShoppingCartService : ShoppingCartService
    {
        #region Private Variables
       private readonly IZnodeRepository<ZnodePortalCatalog> _portalCatalogRepository;
       private readonly IZnodeRepository<ZnodeUser> _userRepository;
       private readonly IZnodeRepository<ZnodeAccount> _accountRepository;      
        private readonly IShoppingCartMap _shoppingCartMap;       
        #endregion

        #region Constructor
        public AriatShoppingCartService()
        {   
           _portalCatalogRepository = new ZnodeRepository<ZnodePortalCatalog>();
           _userRepository = new ZnodeRepository<ZnodeUser>();
           _accountRepository = new ZnodeRepository<ZnodeAccount>();
           _shoppingCartMap = GetService<IShoppingCartMap>();
        }
        #endregion

        #region Public Methods


        //To get ShoppingCart by cookieId 
        public override ShoppingCartModel GetShoppingCartDetails(CartParameterModel cartParameterModel, ShoppingCartModel cartModel = null)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            cartParameterModel.ProfileId = cartModel?.ProfileId ?? GetProfileId();
            ZnodeLogging.LogMessage("Profile Id:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, cartParameterModel?.ProfileId);
            IZnodeRepository<ZnodePimDownloadableProduct> _pimDownloadableProduct = new ZnodeRepository<ZnodePimDownloadableProduct>();
            IZnodeRepository<ZnodePortalProfile> _znodePortalProfile = new ZnodeRepository<ZnodePortalProfile>();

            ZnodePortalProfile znodePortalProfile = _znodePortalProfile.Table.FirstOrDefault(x => x.IsDefaultAnonymousProfile == true && x.PortalId == cartParameterModel.PortalId);

            if (IsNotNull(znodePortalProfile))
            {
                if (znodePortalProfile?.ProfileId == cartParameterModel?.ProfileId && cartParameterModel?.UserId > 0)
                {
                    IZnodeRepository<ZnodeUserProfile> znodeUserProfilefRepository = new ZnodeRepository<ZnodeUserProfile>();
                    int? profileId = znodeUserProfilefRepository.Table.FirstOrDefault(x => x.UserId == cartParameterModel.UserId).ProfileId;
                    if (IsNotNull(profileId) && profileId > 0)
                        cartParameterModel.ProfileId = profileId.HasValue ? profileId.Value : cartParameterModel.ProfileId;
                }
            }

            SetPublishCatalogId(cartParameterModel);

            ZnodeShoppingCart znodeShoppingCart = GetService<IZnodeShoppingCart>().LoadFromDatabase(cartParameterModel);
            znodeShoppingCart.IsAllowWithOtherPromotionsAndCoupons = DefaultGlobalConfigSettingHelper.IsAllowWithOtherPromotionsAndCoupons;
            //to map cart model to znode shopping cart  
            BindCartModel(cartModel, znodeShoppingCart);

            IImageHelper imageHelper = GetService<IImageHelper>(new ZnodeNamedParameter("PortalId", znodeShoppingCart.PortalId.GetValueOrDefault()));

            //Map Libraries.ECommerce.ShoppingCart to ShoppingCartModel.
            ShoppingCartModel shoppingCartModel = _shoppingCartMap.ToModel(znodeShoppingCart, imageHelper);

            //Map null data with requesting cart model
            MapNullDataWithRequestingCartModel(cartModel, shoppingCartModel);

            ZnodeLogging.LogMessage("Coupon count:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, cartModel?.Coupons?.Count);
            //Get coupons if already applied.
            if (cartModel?.Coupons?.Count > 0)
                shoppingCartModel.Coupons = cartModel.Coupons;

            ZnodeLogging.LogMessage("Shipping Id:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, cartParameterModel?.ShippingId);
            if (HelperUtility.IsNotNull(cartParameterModel.ShippingId))
            {
                IZnodeRepository<ZnodeShipping> _shippingRepository = new ZnodeRepository<ZnodeShipping>();

                //Check if Shipping is null or not,If null then get the shipping 
                //on the basis of ShippingId form ShoppingCartModel.
                ZnodeShipping shipping = _shippingRepository.Table.FirstOrDefault(x => x.ShippingId == cartParameterModel.ShippingId);

                if (HelperUtility.IsNotNull(shipping))
                {
                    shoppingCartModel.Shipping = new OrderShippingModel
                    {
                        ShippingId = shipping.ShippingId,
                        ShippingDiscountDescription = shipping.Description,
                        ShippingCountryCode = string.IsNullOrEmpty(cartParameterModel.ShippingCountryCode) ? string.Empty : cartParameterModel.ShippingCountryCode
                    };
                }
            }

            ZnodeLogging.LogMessage("ShoppingCartItem:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, shoppingCartModel?.ShoppingCartItems);
            //Check product inventory of the product for all type of product in cart-line item.
            if (HelperUtility.IsNotNull(shoppingCartModel?.ShoppingCartItems))
            {
                if (cartModel?.ShoppingCartItems?.Count > 0)
                {
                    shoppingCartModel?.ShoppingCartItems.ForEach(cartItem =>
                    {
                        var lineItem = cartModel.ShoppingCartItems
                                    .Where(cartLineItem => cartLineItem.SKU == cartItem.SKU && cartLineItem.AddOnProductSKUs == cartItem.AddOnProductSKUs)?.FirstOrDefault();
                        cartItem.OmsOrderLineItemsId = (lineItem?.OmsOrderLineItemsId).GetValueOrDefault();
                        cartItem.CartDescription = string.IsNullOrEmpty(cartItem.CartDescription) ? lineItem?.CartDescription : cartItem.CartDescription;

                    });
                }

                //Single call instead multiple inventory check.
                CheckBaglineItemInventory(shoppingCartModel, cartParameterModel);
            }

            //Bind cookieMappingId, PortalId, LocaleId, CatalogId, UserId.
            BindCartData(shoppingCartModel, cartParameterModel);

            if (IsNotNull(shoppingCartModel) && cartModel?.OmsOrderId > 0)
                shoppingCartModel.BillingAddress = cartModel.BillingAddress;

            IPIMAttributeService _pimAttributeService = GetService<IPIMAttributeService>();
            //Binding Shoppingcart items' personalize attribute names.
            //TODO: _pimAttributeService.GetAttributeLocale will be called for each personalized attr of each shopping cart item.
            // So this code may perform slow if cart items are more quantity.
            shoppingCartModel.ShoppingCartItems.Where(s => s.PersonaliseValuesDetail != null && s.PersonaliseValuesDetail.Count > 0).ToList()
                .ForEach(x => x.PersonaliseValuesDetail
                    .ForEach(p => p.PersonalizeName = _pimAttributeService.GetAttributeLocale(p.PersonalizeCode, shoppingCartModel.LocaleId)));

            if (cartModel?.Vouchers?.Count > 0)
                shoppingCartModel.Vouchers = cartModel.Vouchers;

            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return shoppingCartModel;
        }
        #endregion

        #region Private Methods
        private void SetPublishCatalogId(CartParameterModel cartParameterModel)
        {
            if (cartParameterModel.PublishedCatalogId == 0 && cartParameterModel.PortalId > 0)
            {
                int userAccountId = _userRepository.Table.FirstOrDefault(o => o.UserId == cartParameterModel.UserId).AccountId.GetValueOrDefault();
                if (userAccountId != 0)
                {
                    int accountCatalogId = GetAccountCatalogId(userAccountId);
                    cartParameterModel.PublishedCatalogId = accountCatalogId == 0 ? GetPublishedCatalogId(cartParameterModel.PortalId) : accountCatalogId;
                }
                else
                {
                    cartParameterModel.PublishedCatalogId = GetPublishedCatalogId(cartParameterModel.PortalId);
                }
            }
        }

        //Get the Publish Catalog Id on the basis of PortalId
        private int GetPublishedCatalogId(int portalId)
        {
            return portalId > 0 ? _portalCatalogRepository.Table.FirstOrDefault(o => o.PortalId == portalId).PublishCatalogId : 0;
        }

        //Get Catalog Id on the basis of UserAccount
        private int GetAccountCatalogId(int userAccountId)
        {
            return _accountRepository.Table.FirstOrDefault(o => o.AccountId == userAccountId).PublishCatalogId.GetValueOrDefault();
        } 
        #endregion
    }
}
