﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Engine.Services.Maps;
using Znode.Libraries.ECommerce.Entities;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Sample.Api.Model;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;

namespace Znode.Api.Custom.Service.Service
{
    public class AriatShoppingCartItemMap : ShoppingCartItemMap
    {
        public AriatShoppingCartItemMap()
        {

        }

        #region Public Methods
        public override void BindProductDetails(ShoppingCartItemModel cartItem, ZnodeProductBaseEntity znodeProduct)
        {
            if (HelperUtility.IsNotNull(znodeProduct))
            {
                cartItem.ProductId = znodeProduct.ProductID;
                cartItem.SKU = znodeProduct.SKU;
                cartItem.IsActive = znodeProduct.IsActive;
                cartItem.QuantityOnHand = znodeProduct.QuantityOnHand;
                string finalCartDescription = string.Empty;


                List<DefaultGlobalConfigModel> defaultGlobalSettingData = GetDefaultGlobalSettingData();
                cartItem.CultureCode = string.IsNullOrEmpty(cartItem.CultureCode) ? GetDefaultCulture(defaultGlobalSettingData) : cartItem.CultureCode;

                finalCartDescription = GetArtifiCartDescription(cartItem, finalCartDescription, znodeProduct.ZNodeAddonsProductCollection, znodeProduct);

                if (string.IsNullOrEmpty(znodeProduct.ShoppingCartDescription) || string.IsNullOrWhiteSpace(znodeProduct.ShoppingCartDescription))
                {
                    if (string.IsNullOrEmpty(cartItem.Description) || string.IsNullOrWhiteSpace(cartItem.Description))
                    {
                        cartItem.CartDescription = znodeProduct.Description;
                    }
                    else
                    {
                        cartItem.CartDescription = cartItem.Description;
                    }
                }
                else
                {
                    cartItem.CartDescription = znodeProduct.ShoppingCartDescription;
                }
                if (!string.IsNullOrEmpty(finalCartDescription))
                    cartItem.CartDescription = finalCartDescription;

                cartItem.ImageMediumPath = znodeProduct.ImageFile;
                cartItem.ProductName = znodeProduct.Name;
                cartItem.ProductDiscountAmount = znodeProduct.DiscountAmount;
                cartItem.SeoPageName = znodeProduct.SEOURL;
                cartItem.DownloadableProductKey = znodeProduct.DownloadableProductKey;
                cartItem.AddOnProductSKUs = GetProductTypeSKUs(znodeProduct, ZnodeCartItemRelationshipTypeEnum.AddOns);
                cartItem.BundleProductSKUs = GetProductTypeSKUs(znodeProduct, ZnodeCartItemRelationshipTypeEnum.Bundles);
                cartItem.ConfigurableProductSKUs = GetProductTypeSKUs(znodeProduct, ZnodeCartItemRelationshipTypeEnum.Configurable);
                cartItem.GroupProducts = GetGroupProducts(znodeProduct, ZnodeCartItemRelationshipTypeEnum.Group);
                cartItem.AssociatedAddOnProducts = GetAssociateProducts(znodeProduct, ZnodeCartItemRelationshipTypeEnum.AddOns);
                SetProductAttributes(cartItem, znodeProduct.Attributes);

            }
        }

        public override List<AssociatedProductModel> GetAssociateProducts(ZnodeProductBaseEntity product, ZnodeCartItemRelationshipTypeEnum productType)
        {
            List<AssociatedProductModel> products = new List<AssociatedProductModel>();
            switch (productType)
            {
                case ZnodeCartItemRelationshipTypeEnum.AddOns:
                    foreach (ZnodeProductBaseEntity item in product.ZNodeAddonsProductCollection)
                    {
                        products.Add(new AssociatedProductModel
                        {
                            Sku = item.SKU,
                            ProductId = item.ProductID,
                            Quantity = item.SelectedQuantity,
                            AddOnQuantity = item.SelectedQuantity,
                        });
                    }

                    return products;
                case ZnodeCartItemRelationshipTypeEnum.Bundles:
                    foreach (ZnodeProductBaseEntity item in product.ZNodeBundleProductCollection)
                    {
                        products.Add(new AssociatedProductModel
                        {
                            Sku = item.SKU,
                            ProductId = item.ProductID
                        });
                    }

                    return products;
                case ZnodeCartItemRelationshipTypeEnum.Configurable:
                    foreach (ZnodeProductBaseEntity item in product.ZNodeConfigurableProductCollection)
                    {
                        products.Add(new AssociatedProductModel
                        {
                            Sku = item.SKU,
                            ProductId = item.ProductID
                        });
                    }

                    return products;
                case ZnodeCartItemRelationshipTypeEnum.Group:
                    foreach (ZnodeProductBaseEntity item in product.ZNodeGroupProductCollection)
                    {
                        products.Add(new AssociatedProductModel
                        {
                            Sku = item.SKU,
                            ProductId = item.ProductID,
                            Quantity = item.SelectedQuantity,
                            ProductName = item.Name,
                            UnitPrice = GetUnitPriceForGroupProduct(item),
                            MinimumQuantity = item.MinQty,
                            MaximumQuantity = item.MaxQty,
                        });
                    }

                    return products;
                default:
                    return products;
            }
        }
        #endregion

        #region Private Methods

        private List<DefaultGlobalConfigModel> GetDefaultGlobalSettingData()
        {
            IDefaultGlobalConfigService defaultGlobalConfigService = ZnodeDependencyResolver.GetService<IDefaultGlobalConfigService>();
            return defaultGlobalConfigService.GetDefaultGlobalConfigList()?.DefaultGlobalConfigs;
        }

        private string GetDefaultCulture(List<DefaultGlobalConfigModel> defaultGlobalSettingData)
          => defaultGlobalSettingData?.Where(x => string.Equals(x.FeatureName, GlobalSettingEnum.Culture.ToString(), StringComparison.CurrentCultureIgnoreCase))?.Select(x => x.FeatureValues)?.FirstOrDefault();

        private string GetArtifiCartDescription(ShoppingCartItemModel cartItem, string finalCartDescription, ZnodeGenericCollection<ZnodeProductBaseEntity> znodeAddonsProductCollection, ZnodeProductBaseEntity znodeProduct)
        {
            decimal textUnitPrice = 0;
            decimal logoUnitPrice = 0;
            decimal largeLogoUnitPrice = 0;
            decimal productUnitPrice = 0;
            decimal productTotalPrice = 0;
            string name = string.Empty;
            string configProductCartDescription = string.Empty;
            if (znodeProduct?.ZNodeConfigurableProductCollection?.Count > 0)
            {
                foreach (ZnodeProductBaseEntity productBaseEntity in znodeProduct?.ZNodeConfigurableProductCollection)
                {
                    if (znodeProduct?.ProductID == productBaseEntity?.ProductID)
                    {
                        configProductCartDescription = productBaseEntity?.Description;
                        if(!string.IsNullOrEmpty(configProductCartDescription) && configProductCartDescription.Contains("-"))
                        {
                            configProductCartDescription = configProductCartDescription.Replace("-", ":");
                        }
                    }

                }
            }
            string productDescription = "<div class='d-flex flex-column mt-3'><div class='d-flex flex-row justify-content-between cart-width'><div><p>{0}</p></div><div><p>{1}</p></div></div><div><p>{2}</p></div></div>";
            if (znodeProduct.ConfigurableProductPrice == 0)
            {
                productUnitPrice = znodeProduct.FinalPrice;
            }
            else
            {
                productUnitPrice = znodeProduct.ConfigurableProductPrice;
            }
            if (productUnitPrice > 0)
            {
                productTotalPrice = productUnitPrice * cartItem.Quantity;
                name = string.Join("", "<b>" + znodeProduct.Name + "</b>" , "(+" + " " + ServiceHelper.FormatPriceWithCurrency(productUnitPrice, cartItem.CultureCode) + ")");
            }
            productDescription = string.Format(productDescription, name, ServiceHelper.FormatPriceWithCurrency(productTotalPrice, cartItem.CultureCode), configProductCartDescription);
            foreach (ZnodeProductBaseEntity item in znodeAddonsProductCollection)
            {
                if (item.SKU == "TextLines")
                {
                    textUnitPrice = item.FinalPrice;
                }

                if (item.SKU == "Logo")
                {
                    logoUnitPrice = item.FinalPrice;
                }

                if (item.SKU == "LargeLogo")
                {
                    largeLogoUnitPrice = item.FinalPrice;
                }
            }
            if (IsNotNull(cartItem.PersonaliseValuesDetail) && cartItem.PersonaliseValuesDetail.Count > 0)
            {
                foreach (PersonaliseValueModel item in cartItem.PersonaliseValuesDetail)
                {
                    string locationDescription = "<div class='d-flex flex-column mt-3'><div class='d-flex'> <div><p><b>Customization Location</b>: {0}</p></div><div><p></p></div></div><div class='d-flex justify-content-between cart-width'><div><p><b>Text</b>: {1}{2}</p></div><div><p>{3}</p></div></div><div class='d-flex justify-content-between'><div><p><b>Text Color</b>: {4}</p></div><div><p></p></div></div></div>";
                    string logoDescription = "<div class='d-flex  justify-content-between cart-width'><div><p><b>Logo</b>: {0}<b> {1}</b></p></div><div><p><b>{2}</b></p></div></div>";
                    string onlyLogoDescription = "<div class='d-flex mt-3 flex-column justify-content-between'><div class='d-flex'><div><p><b>Customization Location: {0}</b></p></div><div><p></p></div></div><div class='d-flex justify-content-between cart-width'><div><p><b>Logo</b>: {1}{2}</p></div><div><p>{3}</p></div></div></div>";
                    string finalLogoDescription = string.Empty;
                    bool personalizedText = false;
                    bool image = false;
                    decimal textPrice = 0;
                    decimal textTotalPrice = 0;
                    decimal logoTotalPrice = 0;
                    if (item.PersonalizeCode != "ArtifiDesignId" && item.PersonalizeCode != "ArtifiImagePath" && item.PersonalizeCode != "ArtifiSKU")
                    {
                        ArtifiPersonalizeModel artifiPersonalizeModel = JsonConvert.DeserializeObject<ArtifiPersonalizeModel>(item.PersonalizeValue);
                        if (artifiPersonalizeModel?.text?.Count >= 1)
                        {
                            string textPriceInString = string.Empty;
                            if (!string.IsNullOrEmpty(artifiPersonalizeModel.text[0].noOfLines) && !string.IsNullOrEmpty(artifiPersonalizeModel.text[0].text))
                            {
                                textPrice = Convert.ToDecimal(artifiPersonalizeModel.text[0].noOfLines) * textUnitPrice;
                                textTotalPrice = textPrice * cartItem.Quantity;
                                textPriceInString = "(+" + " " + ServiceHelper.FormatPriceWithCurrency(textPrice, cartItem.CultureCode) + ")";
                                locationDescription = string.Format(locationDescription, item.PersonalizeCode, artifiPersonalizeModel.text[0].text, textPriceInString, ServiceHelper.FormatPriceWithCurrency(textTotalPrice, cartItem.CultureCode), artifiPersonalizeModel.text[0].colorName);
                                personalizedText = true;
                            }
                        }

                        if (artifiPersonalizeModel?.image?.Count >= 1)
                        {
                            string logoUnitPriceInString = string.Empty;
                            if (!string.IsNullOrEmpty(artifiPersonalizeModel.image[0].imageName))
                            {
                                logoTotalPrice = logoUnitPrice * cartItem.Quantity;
                                logoUnitPriceInString = "(+" + " " + ServiceHelper.FormatPriceWithCurrency(logoUnitPrice, cartItem.CultureCode) + ")";
                                if(item.PersonalizeCode == "Back" && !string.IsNullOrEmpty(artifiPersonalizeModel.image[0].imageName))
                                {
                                    logoUnitPriceInString = "(+" + " " + ServiceHelper.FormatPriceWithCurrency(largeLogoUnitPrice, cartItem.CultureCode) + ")";
                                    logoTotalPrice = largeLogoUnitPrice * cartItem.Quantity;
                                }
                                if (personalizedText)
                                {
                                    finalLogoDescription = string.Format(logoDescription, artifiPersonalizeModel.image[0].imageName, logoUnitPriceInString, ServiceHelper.FormatPriceWithCurrency(logoTotalPrice, cartItem.CultureCode));
                                }
                                else
                                {
                                    finalLogoDescription = string.Format(onlyLogoDescription, item.PersonalizeCode, artifiPersonalizeModel.image[0].imageName, logoUnitPriceInString, ServiceHelper.FormatPriceWithCurrency(logoTotalPrice, cartItem.CultureCode));
                                }
                                image = true;
                            }
                        }
                        if (personalizedText && image)
                        {
                            if (string.IsNullOrEmpty(finalCartDescription))
                            {
                                finalCartDescription = string.Join("", finalCartDescription, productDescription);
                            }
                            finalCartDescription = string.Join("", finalCartDescription, locationDescription, finalLogoDescription);
                        }
                        else if (personalizedText)
                        {
                            if (string.IsNullOrEmpty(finalCartDescription))
                            {
                                finalCartDescription = string.Join("", finalCartDescription, productDescription);
                            }
                            finalCartDescription = string.Join("", finalCartDescription, locationDescription);
                        }
                        else if (image)
                        {
                            if (string.IsNullOrEmpty(finalCartDescription))
                            {
                                finalCartDescription = string.Join(" ", finalCartDescription, productDescription);
                            }
                            finalCartDescription = string.Join("", finalCartDescription, finalLogoDescription);
                        }

                    }
                }
            }
            return finalCartDescription;
        }
        #endregion
    }
}
