﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Libraries.ECommerce.Utilities;

using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;
namespace Znode.Api.Custom.Service.Service
{
    public class AriatSearchService : SearchService
    {

        public override void BindProductDetails(KeywordSearchModel searchResult, int portalId, IList<PublishCategoryProductDetailModel> productDetails, bool isSendAllLocations = false)
        {

            IImageHelper imageHelper = GetService<IImageHelper>(new ZnodeNamedParameter("PortalId", portalId));

            searchResult?.Products?.ForEach(product =>
            {
                PublishCategoryProductDetailModel productSKU = productDetails?
                            .FirstOrDefault(productdata => productdata.SKU == product.SKU);

                if (IsNotNull(productSKU))
                {
                    product.SalesPrice = productSKU.SalesPrice;
                    product.RetailPrice = productSKU.RetailPrice;
                    product.CurrencyCode = productSKU.CurrencyCode;
                    product.CultureCode = productSKU.CultureCode;
                    product.CurrencySuffix = productSKU.CurrencySuffix;
                    product.Quantity = productSKU.Quantity;
                    product.ReOrderLevel = productSKU.ReOrderLevel;
                    product.Rating = productSKU.Rating;
                    product.TotalReviews = productSKU.TotalReviews;
                    product.ImageSmallPath = imageHelper.GetImageHttpPathSmall(product.Attributes.FirstOrDefault(y => y.AttributeCode == ZnodeConstant.ProductImage)?.AttributeValues);
                    product.AllLocationQuantity = productSKU.AllLocationQuantity;
                    product.Custom2 = productSKU.Custom2;
                    product.Custom3 = productSKU.Custom3;
                }
            });

        }
    }
}
