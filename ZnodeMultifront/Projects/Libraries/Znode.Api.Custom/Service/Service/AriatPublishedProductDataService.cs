﻿using System.Collections.Generic;
using System.Linq;
using Znode.Engine.Services;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;

namespace Znode.Api.Custom.Service.Service
{
    public class AriatPublishedProductDataService : PublishedProductDataService
    {
        #region Private Variable
        private readonly IZnodeRepository<ZnodePublishConfigurableProductEntity> _publishConfigurableProductEntity;
        private readonly IZnodeRepository<ZnodePublishProductEntity> _publishProductEntity;
        #endregion

        #region Constructor
        public AriatPublishedProductDataService()
        {
            _publishProductEntity = new ZnodeRepository<ZnodePublishProductEntity>(HelperMethods.Context);
            _publishConfigurableProductEntity = new ZnodeRepository<ZnodePublishConfigurableProductEntity>(HelperMethods.Context);
        }
        #endregion

        #region Public Method
        public override List<ZnodePublishProductEntity> GetAssociatedConfigurableProducts(int publishProductId, int localeId, int? catalogVersionId)
        {
            List<ZnodePublishProductEntity> publishProductEntity = new List<ZnodePublishProductEntity>();

            publishProductEntity = (from configEntity in _publishConfigurableProductEntity.Table
                                    join productEntity in _publishProductEntity.Table
                                    on configEntity.AssociatedZnodeProductId equals productEntity.ZnodeProductId
                                    where configEntity.ZnodeProductId == publishProductId &&
                                    configEntity.VersionId == catalogVersionId &&
                                    productEntity.VersionId == catalogVersionId
                                    orderby configEntity.AssociatedProductDisplayOrder ascending
                                    select productEntity)?.ToList();

            return publishProductEntity;
        }
        #endregion

    }
}
