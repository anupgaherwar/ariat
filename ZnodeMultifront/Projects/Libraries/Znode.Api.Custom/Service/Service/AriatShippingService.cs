﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Custom.Data;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.Services;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;
using Znode.Sample.Api.Model;

namespace Znode.Api.Custom.Service.Service
{
    public class AriatShippingService : ShippingService, IAriatShippingService
    {
        #region Private Variables
        IZnodeRepository<ZnodeAriatCustomShippingCharge> _ariatCustomShippingCharge;
        IZnodeRepository<ZnodeShipping> _shippingRepository;
        #endregion

        #region Controller

        public AriatShippingService()
        {
            _ariatCustomShippingCharge = new ZnodeRepository<ZnodeAriatCustomShippingCharge>(new Custom_Entities());
            _shippingRepository = new ZnodeRepository<ZnodeShipping>();
        }

        #endregion

        #region Public Methods

        //Create new shipping rate
        public AriatShippingModel Create(AriatShippingModel model)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Customers.ToString(), TraceLevel.Info);

            if (HelperUtility.IsNull(model))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ModelNotNull);

            ZnodeAriatCustomShippingCharge result = _ariatCustomShippingCharge.Insert(model.ToEntity<ZnodeAriatCustomShippingCharge>());

            ZnodeLogging.LogMessage("New Record inserted with id ", ZnodeLogging.Components.Shipping.ToString(), TraceLevel.Verbose, result?.AriatCustomShippingChargesId);

            if (HelperUtility.IsNotNull(result))
                return result.ToModel<AriatShippingModel>();

            return model;
        }

        //Delete existing shipping rate details
        public bool DeleteShippingRate(ParameterModel shippingRateIds)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Shipping.ToString(), TraceLevel.Info);

            if (HelperUtility.IsNull(shippingRateIds) || string.IsNullOrEmpty(shippingRateIds.Ids))
            {
                throw new ZnodeException(ErrorCodes.IdLessThanOne, Admin_Resources.ErrorTemplateIdLessThanOne);
            }

            //Generates filter clause for multiple omsTemplateIds.
            FilterCollection filter = new FilterCollection();
            filter.Add(new FilterTuple("AriatCustomShippingChargesId", ProcedureFilterOperators.In, shippingRateIds.Ids));

            //Returns true if cart item deleted successfully else return false.
            bool IsDeleted = _ariatCustomShippingCharge.Delete(DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filter.ToFilterDataCollection()).WhereClause);

            return IsDeleted;
        }

        //Update an existing shipping rate details
        public bool EditShippingRate(AriatShippingModel model)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Shipping.ToString(), TraceLevel.Info);

            if (HelperUtility.IsNull(model))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ModelNotNull);

            bool status = false;
            if (model.AriatCustomShippingChargesId > 0)
            {
                ZnodeAriatCustomShippingCharge existingData = _ariatCustomShippingCharge.GetById(model.AriatCustomShippingChargesId);
                model.ShippingCode = existingData.ShippingCode;

                status = _ariatCustomShippingCharge.Update(model.ToEntity<ZnodeAriatCustomShippingCharge>());

                return status;
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Shipping.ToString(), TraceLevel.Info);

            return status;
        }

        //Get Existing shipping rate details
        public AriatShippingModel GetShippingRateDetails(int ariatCustomShippingChargesId)
        {
            ZnodeAriatCustomShippingCharge znodeAriatCustomShipping = _ariatCustomShippingCharge.GetById(ariatCustomShippingChargesId);

            if (HelperUtility.IsNotNull(znodeAriatCustomShipping))
                return znodeAriatCustomShipping.ToModel<AriatShippingModel>();

            return null;
        }

        //Get shipping rate list
        public AriatShippingListModel ShippingRateList(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            int shippingId;
            int.TryParse(filters.FirstOrDefault(x => x.FilterName.Equals(ZnodeShippingEnum.ShippingId.ToString(), StringComparison.InvariantCultureIgnoreCase))?.FilterValue, out shippingId);

            filters.RemoveAll(x => x.FilterName == FilterKeys.ShippingId.ToLower());

            ZnodeShipping ShippingInfo = _shippingRepository.GetById(shippingId);

            filters.Add(ZnodeShippingEnum.ShippingCode.ToString(), FilterOperators.Is, ShippingInfo.ShippingCode);

            PageListModel pageListModel = new PageListModel(filters, sorts, page);

            List<ZnodeAriatCustomShippingCharge> shippingCharges = _ariatCustomShippingCharge.GetPagedList(pageListModel.EntityWhereClause.WhereClause, pageListModel.OrderBy, pageListModel.EntityWhereClause.FilterValues, pageListModel.PagingStart, pageListModel.PagingLength, out pageListModel.TotalRowCount)?.ToList();

            AriatShippingListModel shippingListModel = shippingCharges?.Count > 0 ? new AriatShippingListModel { ShippingList = shippingCharges.ToModel<AriatShippingModel>()?.ToList() } : new AriatShippingListModel { ShippingList = new List<AriatShippingModel>() };

            shippingListModel.BindPageListModel(pageListModel);

            return shippingListModel;
        }
        #endregion
    }
}
