﻿using System.Collections.Specialized;

using Znode.Custom.Api.Model;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Api.Custom.Service
{
    public interface IAriatDevExpressService
    { 
        /// <summary>
        /// Get vouchers report
        /// </summary>
        /// <param name="expands">Expand for vouchers report.</param>
        /// <param name="filters">filters for vouchers report.</param>
        /// <param name="sorts">Sort for vouchers report.</param>
        /// <param name="page">Paging for vouchers report.</param>
        /// <returns>Report list for vouchers.</returns>
        AriatVoucherReportListModel GetVoucherReport(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page);
    }
}
