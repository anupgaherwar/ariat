﻿using System.Collections.Specialized;

using Znode.Custom.Api.Model;
using Znode.Engine.Api.Models;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Api.Custom.Service
{
    public interface IAriatAccountService
    {
        /// <summary>
        /// Program Catalog Product List
        /// </summary>
        /// <param name="expands">Expand for program catalog products.</param>
        /// <param name="filters">filters for program catalog products.</param>
        /// <param name="sorts">Sort for program catalog products.</param>
        /// <param name="page">Paging for program catalog products.</param>
        /// <returns>Program Catalog Product List.</returns>
        AriatProgramCatalogProductListModel ProgramCatalogProductList(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Get the full catalog product list on the basis of filters.
        /// </summary>
        /// <param name="expands">Expand for full catalog products.</param>
        /// <param name="filters">filters for full catalog products.</param>
        /// <param name="sorts">Sort for program full products.</param>
        /// <param name="page">Paging for program full products.</param>
        /// <returns>Full catalog product list</returns>
        AriatFullCatalogProductListModel GetFullCatalogProductList(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page);
        
        /// <summary>
        /// Publlish program catalog
        /// </summary>
        /// <param name="ids">Account external id</param>
        /// <returns>Publish Return Model With Status and Message.</returns>
        PublishedModel Publish(ParameterModel ids);

        /// <summary>
        /// Delete product as per ariatProgramCatalogProductIds.
        /// </summary>
        /// <param name="ariatProgramCatalogProductId">AriatProgramCatalogProductId to be deleted.</param>
        /// <returns>Returns true if deleted sucessfully else return false.</returns>
        bool DeleteProduct(ParameterModel ariatProgramCatalogProductId);
    }
}
