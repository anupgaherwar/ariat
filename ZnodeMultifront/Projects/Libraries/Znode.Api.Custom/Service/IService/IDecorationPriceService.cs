﻿using Znode.Custom.Api.Model;
using Znode.Custom.Api.Model.ParameterModel;

namespace Znode.Api.Custom.Service
{
    public interface IDecorationPriceService
    {
        /// <summary>
        /// Get decoration price details
        /// </summary>
        /// <param name="model">Parameter model to get the decoration price</param>
        /// <returns>Price details</returns>
        DecorationPriceModel GetDecorationPrice(PriceParameterModel model);
    }
}
