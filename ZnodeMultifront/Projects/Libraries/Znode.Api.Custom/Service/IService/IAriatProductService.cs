﻿
using Znode.Custom.Api.Model;

namespace Znode.Api.Custom.Service.IService
{
    public interface IAriatProductService
    {
        /// <summary>
        /// Get get znode sku
        /// </summary>
        /// <param name="znodeSKURequestModel">Request model to get znode sku</param>
        /// <returns>Znode sku</returns>
        ZnodeSKURequestModel GetZnodeSKU(ZnodeSKURequestModel znodeSKURequestModel);
    }
}
