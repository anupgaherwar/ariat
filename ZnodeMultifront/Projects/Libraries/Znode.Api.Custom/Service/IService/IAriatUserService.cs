﻿
using Znode.Custom.Api.Model;
using Znode.Engine.Api.Models;

namespace Znode.Api.Custom.Service.IService
{
    public interface IAriatUserService
    {
        /// <summary>
        /// Get get znode sku
        /// </summary>
        /// <param name="znodeSKURequestModel">Request model to get znode sku</param>
        /// <returns>Znode sku</returns>
        AddressModel GetAdyenShippingAddress(AddressModel addressModel);
    }
}
