﻿using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Sample.Api.Model;

namespace Znode.Api.Custom.Service
{
    public interface IAriatShippingService
    {
        /// <summary>
        /// Get Shipping rate list
        /// </summary>
        /// <param name="expands">expands for shipping rate list</param>
        /// <param name="filters">filters for shipping rate</param>
        /// <param name="sorts">sorts for shipping rate list</param>
        /// <param name="page">page details for shipping rate list</param>
        /// <returns>Shipping Rate List</returns>
        AriatShippingListModel ShippingRateList(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Create new shipping rate details
        /// </summary>
        /// <param name="model">model with shipping rate data</param>
        /// <returns>Created model with shipping rate data</returns>
        AriatShippingModel Create(AriatShippingModel model);

        /// <summary>
        /// Delete an existing record
        /// </summary>
        /// <param name="parameterModel">model with shippings ids</param>
        /// <returns>true or false</returns>
        bool DeleteShippingRate(ParameterModel parameterModel);

        /// <summary>
        /// Update an existing record
        /// </summary>
        /// <param name="model">model with shipping rate data</param>
        /// <returns>true or false</returns>
        bool EditShippingRate(AriatShippingModel model);

        /// <summary>
        /// Get shipping rate details
        /// </summary>
        /// <param name="ariatCustomShippingChargesId">Shipping rate id</param>
        /// <returns>Shipping Rate details</returns>
        AriatShippingModel GetShippingRateDetails(int ariatCustomShippingChargesId);
    }
}
