﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Znode.Api.Custom.Cache;
using Znode.Api.Custom.Service;
using Znode.Engine.Api.Controllers;
using Znode.Engine.Api.Helper;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
using Znode.Libraries.Framework.Business;
using Znode.Sample.Api.Model;
using Znode.Sample.Api.Model.Responses;

namespace Znode.Api.Custom.Controller
{
    public class AriatShippingController : BaseController
    {
        #region Private Variables
        private readonly IAriatShippingCache _customCache;
        private readonly IAriatShippingService _customService;
        #endregion
      
        #region Constructor

        public AriatShippingController(IAriatShippingService portalService)
        {
            _customService = portalService;
            _customCache = new AriatShippingCache(_customService);
        }

        #endregion

        #region Public Methods
        /// <summary>
        /// Get Shipping Rate List
        /// </summary>
        /// <returns> Rate List</returns>
        [ResponseType(typeof(AriatShippingRateListResponse))]
        [HttpGet]
        public virtual HttpResponseMessage ShippingRateList()
        {
            HttpResponseMessage response;
            try
            {
                Func<string> method = () => _customCache.ShippingRateList(RouteUri, RouteTemplate);
                return CreateResponse<AriatShippingRateListResponse>(method, ZnodeLogging.Components.Shipping.ToString());
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                response = CreateInternalServerErrorResponse(new AriatShippingRateListResponse { HasError = true, ErrorMessage = ex.Message });
            }
            return response;
        }

        /// <summary>
        /// Create new shipping rate
        /// </summary>
        /// <param name="model">model with shipping rate details</param>
        /// <returns>Shipping Rate Created Model</returns>
        [HttpPost, ValidateModel]
        [ResponseType(typeof(AriatShippingRateResponse))]
        public virtual HttpResponseMessage Create([FromBody] AriatShippingModel model)
        {
            HttpResponseMessage response;

            try
            {
                AriatShippingModel ariatShippingModel = _customService.Create(model);

                if (!Equals(ariatShippingModel, null))
                {
                    response = CreateCreatedResponse(new AriatShippingRateResponse { Shipping = ariatShippingModel });
                    response.Headers.Add("Location", GetUriLocation(Convert.ToString(ariatShippingModel.AriatCustomShippingChargesId)));
                }
                else
                    response = CreateInternalServerErrorResponse();
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Shipping.ToString(), TraceLevel.Warning);
                response = CreateInternalServerErrorResponse(new AriatShippingRateResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode });
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Shipping.ToString(), TraceLevel.Error);
                response = CreateInternalServerErrorResponse(new AriatShippingRateResponse { HasError = true, ErrorMessage = ex.Message });
            }
            return response;
        }


        /// <summary>
        /// Get Shipping Rate details by id
        /// </summary>
        /// <param name="ariatCustomShippingChargesId">Id to get details</param>
        /// <returns></returns>
        [ResponseType(typeof(AriatShippingRateResponse))]
        [HttpGet]
        public virtual HttpResponseMessage GetShippingRateDetails(int ariatCustomShippingChargesId)
        {
            HttpResponseMessage response;

            try
            {
                string data = _customCache.GetShippingRateDetails(ariatCustomShippingChargesId, RouteUri, RouteTemplate);
                response = !string.IsNullOrEmpty(data) ? CreateOKResponse<AriatShippingRateResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Shipping.ToString(), TraceLevel.Error);
                AriatShippingRateResponse data = new AriatShippingRateResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }


        /// <summary>
        /// Delete existing records
        /// </summary>
        /// <param name="ids">Multiple ids to delete</param>
        /// <returns>True or False depends on the result</returns>
        [ResponseType(typeof(TrueFalseResponse))]
        [HttpPost]
        public virtual HttpResponseMessage DeleteShippingRate(ParameterModel ids)
        {
            HttpResponseMessage response;

            try
            {
                bool updated = _customService.DeleteShippingRate(ids);
                response = CreateOKResponse(new TrueFalseResponse { IsSuccess = updated });
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Shipping.ToString(), TraceLevel.Warning);
                TrueFalseResponse data = new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode };
                response = CreateInternalServerErrorResponse(data);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Shipping.ToString(), TraceLevel.Error);
                TrueFalseResponse data = new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }


        /// <summary>
        /// Update Exiting Details of shipping rate
        /// </summary>
        /// <param name="model">Model with updated data</param>
        /// <returns>True or False depends on the result</returns>
        [ResponseType(typeof(TrueFalseResponse))]
        [HttpPut]
        public virtual HttpResponseMessage EditShippingRate([FromBody] AriatShippingModel model)
        {
            HttpResponseMessage response;

            try
            {
                bool deleted = _customService.EditShippingRate(model);
                response = CreateOKResponse(new TrueFalseResponse { IsSuccess = deleted });
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Warning);
                TrueFalseResponse data = new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode };
                response = CreateInternalServerErrorResponse(data);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Error);
                TrueFalseResponse data = new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        } 
        #endregion
    }
}
