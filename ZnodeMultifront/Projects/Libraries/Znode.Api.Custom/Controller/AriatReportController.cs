﻿
using System;
using System.Diagnostics;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

using Znode.Api.Custom.Cache;
using Znode.Api.Custom.Service;
using Znode.Custom.Api.Model.Responses;
using Znode.Engine.Api.Controllers;
using Znode.Libraries.Framework.Business;

namespace Znode.Api.Custom.Controller
{
    public class AriatReportController : BaseController
    {
        #region Private variables
        private readonly IAriatDevExpressCache _customCache;
        private readonly IAriatDevExpressService _customService;
        #endregion

        #region constructor
        public AriatReportController(IAriatDevExpressService reportService)
        {
            _customService = reportService;
            _customCache = new AriatDevExpressCache(_customService);
        }

        #endregion
        #region Public method.
        /// <summary>
        /// Get vouchers report.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(VoucherReportListResponse))]
        [HttpGet]
        public HttpResponseMessage GetVouchersReport()
        {
            HttpResponseMessage response;
            try
            {
                string data = _customCache.GetVouchersReport(RouteUri, RouteTemplate);
                response = string.IsNullOrEmpty(data) ? CreateNoContentResponse() : CreateOKResponse<VoucherReportListResponse>(data);
            }
            catch (Exception ex)
            {
                response = CreateInternalServerErrorResponse(new VoucherReportListResponse { HasError = true, ErrorMessage = ex.Message });
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
            }
            return response;
        } 
        #endregion
    }
}
