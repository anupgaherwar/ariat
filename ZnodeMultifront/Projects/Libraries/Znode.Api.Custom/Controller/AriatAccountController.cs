﻿
using System;
using System.Diagnostics;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

using Znode.Api.Custom.Cache;
using Znode.Api.Custom.Service;
using Znode.Custom.Api.Model.Responses;
using Znode.Engine.Api.Controllers;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
using Znode.Engine.Api.Helper;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Libraries.Framework.Business;

namespace Znode.Api.Custom.Controller
{
    public class AriatAccountController : BaseController
    {
        #region Private Variables
        private readonly IAriatAccountCache _customCache;
        private readonly IAriatAccountService _customService;
        public const string LoggingComponent = "CustomAccount";
        #endregion

        #region Constructor

        public AriatAccountController(IAriatAccountService accountService)
        {
            _customService = accountService;
            _customCache = new AriatAccountCache(_customService);
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Get Program catalog product list
        /// </summary>
        /// <returns>Program catalog product list</returns>
        [ResponseType(typeof(AriatProgramCatalogProductListResponse))]
        [HttpGet]
        public virtual HttpResponseMessage ProgramCatalogProductList()
        {
            HttpResponseMessage response;
            try
            {
                Func<string> method = () => _customCache.ProgramCatalogProductList(RouteUri, RouteTemplate);
                return CreateResponse<AriatProgramCatalogProductListResponse>(method, ZnodeLogging.Components.Shipping.ToString());
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                response = CreateInternalServerErrorResponse(new AriatProgramCatalogProductListResponse { HasError = true, ErrorMessage = ex.Message });
            }
            return response;
        }

        /// <summary>
        /// Get the full catalog product list
        /// </summary>
        /// <returns>Full catalog product list.</returns>
        [ResponseType(typeof(AriatFullCatalogProductListResponse))]
        [HttpGet]
        public virtual HttpResponseMessage FullCatalogProductList()
        {
            HttpResponseMessage response;
            try
            {
                string data = _customCache.FullCatalogProductList(RouteUri, RouteTemplate);
                response = !string.IsNullOrEmpty(data) ? CreateOKResponse<AriatFullCatalogProductListResponse>(data) : CreateNoContentResponse();
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                AriatFullCatalogProductListResponse data = new AriatFullCatalogProductListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }


        /// <summary>
        /// Publish The Program Catalog
        /// </summary>
        /// <param name="ids">Parameter model for program catalog</param>
        /// <returns>Publish Status</returns>
        [ResponseType(typeof(PublishedResponse))]
        [HttpPost]
        public virtual HttpResponseMessage Publish(ParameterModel ids)
        {
            HttpResponseMessage response;
            try
            {
                PublishedModel published = _customService.Publish(ids);
                response = !Equals(published, null) ? CreateOKResponse(new PublishedResponse { PublishedModel = published }) : CreateInternalServerErrorResponse();
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Warning);
                response = CreateInternalServerErrorResponse(new PublishedResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode });
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                response = CreateInternalServerErrorResponse(new PublishedResponse { HasError = true, ErrorMessage = ex.Message });
            }
            return response;
        }
        /// <summary>
        /// Delete product by ariatProgramCatalogProductId
        /// </summary>
        /// <param name="ariatProgramCatalogProductId">Id of ariatProgramCatalogProductId</param>
        /// <returns>Returns true/false as per delete operation.</returns>
        [ResponseType(typeof(TrueFalseResponse))]
        [HttpPost, ValidateModel]
        public HttpResponseMessage Delete([FromBody] ParameterModel ariatProgramCatalogProductId)
        {
            HttpResponseMessage response;
            try
            {
                //Delete promotion.
                bool deleted = _customService.DeleteProduct(ariatProgramCatalogProductId);
                response = CreateOKResponse(new TrueFalseResponse { IsSuccess = deleted });
            }          
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, LoggingComponent, TraceLevel.Error);
                TrueFalseResponse data = new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        #endregion

    }
}
