﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Web.Http.Description;
using System.Web.Mvc;
using Znode.Api.Custom.Service.IService;
using Znode.Custom.Api.Model.Responses;
using Znode.Engine.Api.Controllers;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Libraries.Framework.Business;

namespace Znode.Api.Custom.Controller
{
    public class AriatUserController : BaseController
    {
        #region private variabe
        private readonly IAriatUserService _service;
        #endregion

        #region Constructor
        public AriatUserController(IAriatUserService service)
        {
            _service = service;
        }
        #endregion

        #region public methods

        [ResponseType(typeof(AriatUserResponse))]
        [HttpPost]
        public HttpResponseMessage AdyenShippingAddress(AddressModel addressModel)
        {
            HttpResponseMessage response;
            try
            {
                AddressModel data = _service.GetAdyenShippingAddress(addressModel);
                response = CreateOKResponse(new AriatUserResponse { ShippingAddress = data });
            }
            catch (ZnodeException ex)
            {
                response = CreateInternalServerErrorResponse(new AriatZnodeSKUResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode });
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
            }
            catch (Exception ex)
            {
                response = CreateInternalServerErrorResponse(new AriatZnodeSKUResponse { HasError = true, ErrorMessage = ex.Message });
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
            }

            return response;
        }
        #endregion
    }
}
