﻿using System;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Znode.Engine.Api.Controllers;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Service;
using Znode.Engine.Exceptions;
using Znode.Libraries.Framework.Business;
using Znode.Sample.Api.Model.Responses;

namespace Znode.Api.Custom.Controller
{
    public class AriatOrderController: BaseController
    { 
        
        #region Private Variables
        private readonly IArtifiOrderService _artifiOrderService;
        #endregion

        public AriatOrderController(IArtifiOrderService artifiOrderService)
        {
            _artifiOrderService = artifiOrderService;
        }
        /*Generate OrderNumber Start*/
        [HttpGet]
        [ResponseType(typeof(GenerateOrderResponse))]
        public HttpResponseMessage GenerateOrderNumber( int portalId )
        {
            HttpResponseMessage response;
            ParameterModel parameterPortalId = new ParameterModel();
            parameterPortalId.PortalId = portalId;
            try
            {
                SubmitOrderModel submitOrderModel = new SubmitOrderModel();
                string OrderNumber = _artifiOrderService.GenerateOrderNumber(submitOrderModel, parameterPortalId);
                response = CreateCreatedResponse(new GenerateOrderResponse { OrderNumber = OrderNumber });
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex.ToString());
                response = CreateInternalServerErrorResponse(new GenerateOrderResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode });
            }
            catch (Exception ex)
            {
                response = CreateInternalServerErrorResponse(new GenerateOrderResponse { HasError = true, ErrorMessage = ex.Message });
                ZnodeLogging.LogMessage(ex.ToString(), ZnodeLogging.Components.OMS.ToString());
            }

            return response;
        }
        /*Generate OrderNumber End*/
    }
}
