﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Web.Http.Description;
using System.Web.Mvc;

using Znode.Api.Custom.Service.IService;
using Znode.Custom.Api.Model;
using Znode.Custom.Api.Model.Responses;
using Znode.Engine.Api.Controllers;
using Znode.Engine.Exceptions;
using Znode.Libraries.Framework.Business;

namespace Znode.Api.Custom.Controller
{
    public class AriatProductController : BaseController
    {
        #region private variabe
        private readonly IAriatProductService _service;
        #endregion

        #region Constructor
        public AriatProductController(IAriatProductService service)
        {
            _service = service;
        }
        #endregion

        #region public methods
        //Get znode sku from artifi sku
        [ResponseType(typeof(AriatZnodeSKUResponse))]
        [HttpPost]
        public HttpResponseMessage ZnodeSKU(ZnodeSKURequestModel znodeSKURequestModel)
        {
            HttpResponseMessage response;
            try
            {
                ZnodeSKURequestModel data = _service.GetZnodeSKU(znodeSKURequestModel);
                response = CreateOKResponse(new AriatZnodeSKUResponse { ProductData = data });
            }
            catch (ZnodeException ex)
            {
                response = CreateInternalServerErrorResponse(new AriatZnodeSKUResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode });
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
            }
            catch (Exception ex)
            {
                response = CreateInternalServerErrorResponse(new AriatZnodeSKUResponse { HasError = true, ErrorMessage = ex.Message });
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
            }

            return response;
        }
        #endregion
    }
}
