﻿
using System;
using System.Diagnostics;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

using Znode.Api.Custom.Service;
using Znode.Custom.Api.Model;
using Znode.Custom.Api.Model.ParameterModel;
using Znode.Custom.Api.Model.Responses;
using Znode.Engine.Api.Controllers;
using Znode.Engine.Api.Helper;
using Znode.Engine.Exceptions;
using Znode.Libraries.Framework.Business;

namespace Znode.Api.Custom.Controller
{
    public class DecorationPriceController : BaseController
    {
        #region Private Variables

        private readonly IDecorationPriceService _decorationPriceService;
        #endregion

        #region Constructor

        public DecorationPriceController(IDecorationPriceService decorationPriceService)
        {
            _decorationPriceService = decorationPriceService;
        }

        #endregion

        /// <summary>
        /// Get Decoration price details
        /// </summary>
        /// <param name="model">Model for pricinf details</param>
        /// <returns>Pricing details</returns>
        [HttpPost, ValidateModel]
        [ResponseType(typeof(DecorationPriceResponse))]
        public virtual HttpResponseMessage GetDecorationPrice([FromBody] PriceParameterModel model)
        {
            HttpResponseMessage response;

            try
            {
                DecorationPriceModel decorationPrice = _decorationPriceService.GetDecorationPrice(model);

                if (!Equals(decorationPrice, null))
                {
                    response = CreateOKResponse(new DecorationPriceResponse { Price = decorationPrice });
                }
                else
                    response = CreateInternalServerErrorResponse();
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Shipping.ToString(), TraceLevel.Warning);
                response = CreateInternalServerErrorResponse(new DecorationPriceResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode });
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Shipping.ToString(), TraceLevel.Error);
                response = CreateInternalServerErrorResponse(new DecorationPriceResponse { HasError = true, ErrorMessage = ex.Message });
            }
            return response;
        }
    }
}
