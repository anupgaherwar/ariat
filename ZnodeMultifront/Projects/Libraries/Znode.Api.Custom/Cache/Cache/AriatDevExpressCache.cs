﻿using Znode.Api.Custom.Service;
using Znode.Custom.Api.Model;
using Znode.Custom.Api.Model.Responses;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models.Extensions;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Api.Custom.Cache
{
    public class AriatDevExpressCache : BaseCache, IAriatDevExpressCache
    {
        #region Private Variables
        private IAriatDevExpressService _ariatReportService;
        #endregion

        #region Constructor

        public AriatDevExpressCache(IAriatDevExpressService customService)
        {
            _ariatReportService = customService;
        }
        #endregion


        #region Public method
        public string GetVouchersReport(string routeUri, string routeTemplate)
        {
            string data = GetFromCache(routeUri);
            if (string.IsNullOrEmpty(data))
            {
                AriatVoucherReportListModel listModel = _ariatReportService.GetVoucherReport(Expands, Filters, Sorts, Page);
                if (HelperUtility.IsNotNull(listModel))
                {
                    VoucherReportListResponse response = new VoucherReportListResponse { VoucherList = listModel.VoucherList };
                    response.MapPagingDataFromModel(listModel);
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }

        #endregion
    }
}
