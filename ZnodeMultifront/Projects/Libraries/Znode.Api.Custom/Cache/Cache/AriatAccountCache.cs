﻿
using Znode.Api.Custom.Service;
using Znode.Custom.Api.Model;
using Znode.Custom.Api.Model.Responses;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models.Extensions;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Api.Custom.Cache
{
    public class AriatAccountCache : BaseCache, IAriatAccountCache
    {
        #region Private variables
        private IAriatAccountService _customAccountService;
        #endregion

        #region Public constructor

        public AriatAccountCache(IAriatAccountService customService)
        {
            _customAccountService = customService;
        }
        #endregion

        #region Public method
        //Get program catalog product list.
        public string ProgramCatalogProductList(string routeUri, string routeTemplate)
        {
            string data = GetFromCache(routeUri);
            if (string.IsNullOrEmpty(data))
            {
                AriatProgramCatalogProductListModel listModel = _customAccountService.ProgramCatalogProductList(Expands, Filters, Sorts, Page);
                if (HelperUtility.IsNotNull(listModel))
                {
                    AriatProgramCatalogProductListResponse response = new AriatProgramCatalogProductListResponse { ProductList = listModel.ProductList };
                    response.MapPagingDataFromModel(listModel);
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }
        //Get the full catalog product list.
        public string FullCatalogProductList(string routeUri, string routeTemplate)
        {
            string data = GetFromCache(routeUri);
            if (string.IsNullOrEmpty(data))
            {
                AriatFullCatalogProductListModel productList = _customAccountService.GetFullCatalogProductList(Expands, Filters, Sorts, Page);
                if (HelperUtility.IsNotNull(productList) && productList.CatalogProductsDetailList?.Count > 0)
                {
                    AriatFullCatalogProductListResponse response = new AriatFullCatalogProductListResponse { CatalogProductList = productList.CatalogProductsDetailList };
                    response.MapPagingDataFromModel(productList);
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }
        #endregion
    }
}
