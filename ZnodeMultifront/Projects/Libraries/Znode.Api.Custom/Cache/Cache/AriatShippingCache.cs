﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Api.Custom.Service;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models.Extensions;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Sample.Api.Model;
using Znode.Sample.Api.Model.Responses;

namespace Znode.Api.Custom.Cache
{
    public class AriatShippingCache : BaseCache, IAriatShippingCache
    {
        #region Private Variables
        private IAriatShippingService _ariatShippingService;
        #endregion

        #region Constructor

        public AriatShippingCache(IAriatShippingService customService)
        {
            _ariatShippingService = customService;
        }
        #endregion

        #region Public Methods
        public string GetShippingRateDetails(int ariatCustomShippingChargesId, string routeUri, string routeTemplate)
        {
            //Get data from cache.
            string data = GetFromCache(routeUri);
            if (string.IsNullOrEmpty(data))
            {
                //get store details by id.
                AriatShippingModel model = _ariatShippingService.GetShippingRateDetails(ariatCustomShippingChargesId);
                if (HelperUtility.IsNotNull(model))
                {
                    AriatShippingRateResponse response = new AriatShippingRateResponse { Shipping = model };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }

        public string ShippingRateList(string routeUri, string routeTemplate)
        {
            string data = GetFromCache(routeUri);
            if (string.IsNullOrEmpty(data))
            {
                AriatShippingListModel listModel = _ariatShippingService.ShippingRateList(Expands, Filters, Sorts, Page);
                if (HelperUtility.IsNotNull(listModel))
                {
                    AriatShippingRateListResponse response = new AriatShippingRateListResponse { ShippingRateList = listModel.ShippingList };
                    response.MapPagingDataFromModel(listModel);
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        } 
        #endregion
    }
}
