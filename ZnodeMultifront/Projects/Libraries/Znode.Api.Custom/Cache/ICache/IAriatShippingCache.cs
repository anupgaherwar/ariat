﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Api.Custom.Cache
{
    public interface IAriatShippingCache
    {
        /// <summary>
        /// Get shipping rate list
        /// </summary>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>Shipping rate list</returns>
        string ShippingRateList(string routeUri, string routeTemplate);

        /// <summary>
        /// Get Shipping Rate details by id.
        /// </summary>
        /// <param name="ariatCustomShippingChargesId">id to get shipping rate details</param>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>Get shipping rate details</returns>
        string GetShippingRateDetails(int ariatCustomShippingChargesId, string routeUri, string routeTemplate);
    }
}
