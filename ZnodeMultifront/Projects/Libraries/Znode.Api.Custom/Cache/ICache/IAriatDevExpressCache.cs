﻿namespace Znode.Api.Custom.Cache
{
    public interface IAriatDevExpressCache
    {
        /// <summary>
        /// Get voucher report.
        /// </summary>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>voucher report list.</returns>
        string GetVouchersReport(string routeUri, string routeTemplate);
    }
}
