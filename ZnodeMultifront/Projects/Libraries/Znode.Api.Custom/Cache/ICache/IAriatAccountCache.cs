﻿namespace Znode.Api.Custom.Cache
{
    public interface IAriatAccountCache
    {
        string ProgramCatalogProductList(string routeUri, string routeTemplate);

        /// <summary>
        /// Get Full catalog product list
        /// </summary>
        /// <param name="routeUri">Route URI</param>
        /// <param name="routeTemplate">Route template</param>
        /// <returns>Full catalog product list</returns>
        string FullCatalogProductList(string routeUri, string routeTemplate);
    }
}
