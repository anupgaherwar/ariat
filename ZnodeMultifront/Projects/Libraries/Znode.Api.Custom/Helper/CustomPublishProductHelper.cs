﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Libraries.Admin;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Api.Custom.Helper
{
    public class CustomPublishProductHelper : PublishProductHelper
    {
        #region Public Methods
        public override void GetProductPageTemplate(PublishProductModel publishProduct, int portalId, int? webstoreVersionId = null)
        {
            // base.GetProductPageTemplate(publishProduct, portalId, webstoreVersionId);
            string cacheKey = $"ProductPageEntity_{portalId}_{webstoreVersionId}";
            List<ZnodePublishProductPageEntity> pageEntities = Equals(HttpRuntime.Cache[cacheKey], null)
               ? GetProductPageTemplateAndInsertIntoDB(portalId, webstoreVersionId, cacheKey)
               : ((List<ZnodePublishProductPageEntity>)HttpRuntime.Cache.Get(cacheKey));
            string productType = publishProduct.IsConfigurableProduct ? ZnodeConstant.ConfigurableProduct : publishProduct.Attributes?.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.ProductType)?.SelectValues?.FirstOrDefault()?.Code;
            publishProduct.ProductTemplateName = pageEntities.FirstOrDefault(x => x.ProductType == productType && x.PortalId == portalId)?.TemplateName;
        }

        public override void GetProductInventory(PublishProductModel publishProduct, int portalId)
        {
            List<InventorySKUModel> inventory = GetInventoryBySKUs(new List<string> { string.IsNullOrEmpty(publishProduct.ConfigurableProductSKU) ? publishProduct.SKU : publishProduct.ConfigurableProductSKU }, portalId);
            if (inventory.Count > 0)
            {
                publishProduct.Quantity = inventory.FirstOrDefault().Quantity;
                publishProduct.ReOrderLevel = inventory.FirstOrDefault().ReOrderLevel;
                publishProduct.DefaultWarehouseName = inventory.FirstOrDefault().WarehouseName;
                publishProduct.DefaultInventoryCount = inventory.FirstOrDefault().DefaultInventoryCount;
                if (HelperUtility.IsNotNull(inventory.FirstOrDefault().BackOrderExpectedDate))
                {
                    List<DefaultGlobalConfigModel> defaultGlobalSettingData = GetDefaultGlobalSettingData();
                    publishProduct.Custom1 = inventory.FirstOrDefault().BackOrderExpectedDate?.ToString(GetDefaultDateFormat(defaultGlobalSettingData));                   
                }
                ZnodePortal portalDetails = GetPortalDetailsById(portalId);
                if (!string.IsNullOrEmpty(portalDetails.BackOrderMsg))
                {
                    publishProduct.Custom2 = portalDetails.BackOrderMsg;
                }
                int inventoryCount = 0;
                publishProduct.DefaultInventoryCount = Int32.TryParse(publishProduct.DefaultInventoryCount?.Split('.').First(), out inventoryCount) ? inventoryCount.ToString() : publishProduct.DefaultInventoryCount;
            }
        }
        #endregion

        #region Private Methods
        private List<ZnodePublishProductPageEntity> GetProductPageTemplateAndInsertIntoDB(int portalId, int? webstoreVersionId, string cacheKey)
        {

            IZnodeRepository<ZnodePublishProductPageEntity> _publishProductPageEntity = new ZnodeRepository<ZnodePublishProductPageEntity>(HelperMethods.Context);
            List<ZnodePublishProductPageEntity> pageEntities = _publishProductPageEntity.Table.Where(x => x.PortalId == portalId && x.VersionId == webstoreVersionId)?.ToList();
            if (pageEntities.Count > 0)
                HttpRuntime.Cache.Insert(cacheKey, pageEntities);
            return pageEntities;
        }

        private string GetDefaultDateFormat(List<DefaultGlobalConfigModel> defaultGlobalSettingData)
           => defaultGlobalSettingData?.Where(x => string.Equals(x.FeatureName, GlobalSettingEnum.DateFormat.ToString(), StringComparison.CurrentCultureIgnoreCase))?.Select(x => x.FeatureValues)?.FirstOrDefault();
        private List<DefaultGlobalConfigModel> GetDefaultGlobalSettingData()
        {
            IDefaultGlobalConfigService defaultGlobalConfigService = ZnodeDependencyResolver.GetService<IDefaultGlobalConfigService>();
            return defaultGlobalConfigService.GetDefaultGlobalConfigList()?.DefaultGlobalConfigs;
        }
        #endregion
    }
}
