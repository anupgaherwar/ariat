﻿using Autofac;

using Znode.Api.Custom.Cache;
using Znode.Api.Custom.Service;
using Znode.Api.Custom.Service.IService;
using Znode.Api.Custom.Service.Service;
using Znode.Engine.Services;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Admin;
using Znode.Libraries.Admin.Import;
using Znode.Libraries.ECommerce.ShoppingCart;
using Znode.Libraries.Framework.Business;

namespace Znode.Api.Custom.Helper
{
    public class DependencyRegistration : IDependencyRegistration
    {
        /// <summary>
        /// Register the Dependency Injection types.
        /// </summary>
        /// <param name="builder">Autofac Container Builder</param>
        public virtual void Register(ContainerBuilder builder)
        {
            builder.RegisterType<CustomPortalService>().As<ICustomPortalService>().InstancePerRequest();
            builder.RegisterType<AriatPublishProductService>().As<IPublishProductService>().InstancePerRequest();
            //Here override znode base code method by injecting dependancy mention as below.
            //"In CustomPortalService.cs we have override 'DeletePortal()' of znode base code".
            //builder.RegisterType<CustomPortalService>().As<IPortalService>().InstancePerRequest();
            builder.RegisterType<AriatShippingCache>().As<IAriatShippingCache>().InstancePerRequest();
            builder.RegisterType<AriatShippingService>().As<IAriatShippingService>().InstancePerRequest();
            builder.RegisterType<DecorationPriceService>().As<IDecorationPriceService>().InstancePerRequest();
            builder.RegisterType<AriatUserService>().As<IUserService>().InstancePerRequest();
            builder.RegisterType<CustomOrderHelper>().As<IZnodeOrderHelper>().InstancePerRequest();
            builder.RegisterType<AriatProductService>().As<IAriatProductService>().InstancePerRequest();
            builder.RegisterType<CustomPublishProductHelper>().As<IPublishProductHelper>().InstancePerRequest();
            builder.RegisterType<AriatShoppingCartItemMap>().As<IShoppingCartItemMap>().InstancePerRequest();
            builder.RegisterType<AriatZnodeCheckout>().As<IZnodeCheckout>().InstancePerRequest();
            builder.RegisterType<AriatPublishedProductDataService>().As<IPublishedProductDataService>().InstancePerRequest();
            builder.RegisterType<AriatSearchService>().As<SearchService>().InstancePerRequest();
            builder.RegisterType<AriatSearchService>().As<ISearchService>().InstancePerRequest();
            builder.RegisterType<AriatWebStoreWidgetService>().As<IWebStoreWidgetService>().InstancePerRequest();
            builder.RegisterType<AriatWebStoreWidgetService>().As<WebStoreWidgetService>().InstancePerRequest();
            builder.RegisterType<AriatShoppingCartService>().As<IShoppingCartService>().InstancePerRequest();
            builder.RegisterType<AriatDevExpressService>().As<IAriatDevExpressService>().InstancePerRequest();
            builder.RegisterType<AriatDevExpressCache>().As<IAriatDevExpressCache>().InstancePerRequest();
            builder.RegisterType<AriatUserService>().As<IAriatUserService>().InstancePerRequest();
            builder.RegisterType<AriatFormBuilderService>().As<IFormBuilderService>().InstancePerRequest();
            builder.RegisterType<AriatAccountService>().As<IAriatAccountService>().InstancePerRequest();
            builder.RegisterType<AriatImportHelper>().As<IImportHelper>().InstancePerRequest();
        }

        /// <summary>
        /// Order method represents Dependency Injection Registration Order.
        /// For znode base code Library the DI registration order set to 0.
        /// For custom code library the DI registration order should be incremental.
        /// </summary>
        public int Order
        {
            get { return 1; }
        }
    }
}
