﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;

using Znode.Engine.Api.Models;
using Znode.Libraries.Admin;
using Znode.Libraries.Framework.Business;

namespace Znode.Api.Custom.Helper
{
    public class AriatImportHelper : ImportHelper
    {
        protected override int ProcessImportData(ImportModel model, string tableName, int templateId, string uniqueIdentifier, int userId)
        {
            try
            {
                string errorFileName = string.Empty;
                SqlConnection conn = GetSqlConnection();
                SqlCommand cmd = new SqlCommand("Znode_ImportData", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@TableName", tableName);
                cmd.Parameters.AddWithValue("@TemplateId", templateId);
                cmd.Parameters.AddWithValue("@NewGUID", uniqueIdentifier);
                cmd.Parameters.AddWithValue("@UserId", userId);
                cmd.Parameters.AddWithValue("@LocaleId", model.LocaleId);
                cmd.Parameters.AddWithValue("@DefaultFamilyId", model.FamilyId);
                cmd.Parameters.AddWithValue("@PriceListId", model.PriceListId);
                cmd.Parameters.AddWithValue("@CountryCode", model.CountryCode);
                cmd.Parameters.AddWithValue("@PortalId", model.PortalId);
                cmd.Parameters.AddWithValue("@IsAccountAddress", model.IsAccountAddress);
                cmd.Parameters.AddWithValue("@AccountExternalID", model.Custom1);
                if (conn.State.Equals(ConnectionState.Closed))
                    conn.Open();
                cmd.ExecuteReader();
                conn.Close();
                return 1;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex.Message, ZnodeLogging.Components.Import.ToString(), TraceLevel.Error, ex);
                return 0;
            }
        }
    }
}
