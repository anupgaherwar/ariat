﻿using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.Linq;

using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Libraries.Admin;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.ECommerce.Entities;
using Znode.Libraries.ECommerce.Fulfillment;
using Znode.Libraries.ECommerce.ShoppingCart;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Sample.Api.Model;

using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;

namespace Znode.Api.Custom.Helper
{
    public class AriatZnodeCheckout : ZnodeCheckout
    {

        #region Member Variables
        private readonly int _ShippingID = 0;
        private readonly IZnodeOrderHelper orderHelper;
        private readonly IPublishProductHelper publishProductHelper;
        private readonly Dictionary<int, string> publishCategory = new Dictionary<int, string>();
        #endregion

        #region Constructor
        public AriatZnodeCheckout()
        {
            orderHelper = GetService<IZnodeOrderHelper>();
            publishProductHelper = GetService<IPublishProductHelper>();
        }


        // Initializes a new instance of the ZNodeCheckout class.
        public AriatZnodeCheckout(UserAddressModel userAccount, ZnodePortalCart shoppingCart)
        {

            this.UserAccount = userAccount;
            this.ShoppingCart = shoppingCart;
            orderHelper = GetService<IZnodeOrderHelper>();
            publishProductHelper = GetService<IPublishProductHelper>();
        }
        #endregion

        public override ZnodeOrderFulfillment GetOrderFullfillment(UserAddressModel userAccount, ZnodePortalCart shoppingCart, int portalId)
        {
            ZnodeOrderFulfillment order = new ZnodeOrderFulfillment(shoppingCart);

            order.PortalId = portalId;

            SetOrderDetails(order, shoppingCart, userAccount);

            foreach (ZnodeMultipleAddressCart addressCart in shoppingCart.AddressCarts)
            {
                int? shippingId = addressCart.Shipping.ShippingID != 0 ? addressCart.Shipping.ShippingID : this._ShippingID;
                AddressModel address = userAccount?.ShippingAddress;
                address = IsNull(address) || Equals(address.AddressId, 0) ? shoppingCart.Payment?.ShippingAddress : address;
                addressCart.OrderShipmentID = CreateOrderShipment(address, shippingId, userAccount?.Email);

                SetOrderLineItems(order, addressCart);
            }
            return order;
        }

        public override void SetOrderLineItems(ZnodeOrderFulfillment order, ZnodeMultipleAddressCart addressCart)
        {
            GetDistinctCategoryIdsforCartItem(addressCart);
            List<Znode.Libraries.Data.DataModel.ZnodeOmsOrderLineItem> lineItemShippingDateList = orderHelper.GetLineItemShippingDate(order.Order.OmsOrderDetailsId);

            DateTime? ShipDate = DateTime.Now;
            // loop through cart and add line items
            foreach (Libraries.ECommerce.ShoppingCart.ZnodeShoppingCartItem shoppingCartItem in addressCart.ShoppingCartItems)
            {
                OrderLineItemModel orderLineItem;

                if (string.IsNullOrEmpty(shoppingCartItem.GroupId))
                {
                    orderLineItem = order.OrderLineItems.FirstOrDefault(oli => oli.GroupId == shoppingCartItem.GroupId && oli.Sku == shoppingCartItem.SKU && shoppingCartItem.OrderLineItemRelationshipTypeId != (int)ZnodeCartItemRelationshipTypeEnum.Simple);
                }
                else
                {
                    orderLineItem = order.OrderLineItems.FirstOrDefault(oli => oli.GroupId == shoppingCartItem.GroupId && oli.Sku == shoppingCartItem.Product.SKU && shoppingCartItem.OrderLineItemRelationshipTypeId != (int)ZnodeCartItemRelationshipTypeEnum.Simple);
                }

                bool addNewOrderItem = false;
                if (IsNull(orderLineItem))
                {
                    addNewOrderItem = true;
                    orderLineItem = new OrderLineItemModel();
                    orderLineItem.OmsOrderShipmentId = addressCart.OrderShipmentID;
                    string finalCartDescription = string.Empty;

                    List<DefaultGlobalConfigModel> defaultGlobalSettingData = GetDefaultGlobalSettingData();
                    string culterCode = GetDefaultCulture(defaultGlobalSettingData);
                    if (string.IsNullOrEmpty(shoppingCartItem.Product.ShoppingCartDescription) || string.IsNullOrWhiteSpace(shoppingCartItem.Product.ShoppingCartDescription))
                        if (string.IsNullOrEmpty(shoppingCartItem.Product.Description) || string.IsNullOrWhiteSpace(shoppingCartItem.Product.Description))
                            orderLineItem.Description = shoppingCartItem.Description;
                        else
                            orderLineItem.Description = shoppingCartItem.Product.Description;
                    else
                        orderLineItem.Description = shoppingCartItem.Product.ShoppingCartDescription;
                    orderLineItem.ProductName = shoppingCartItem.Product.Name;
                    orderLineItem.Sku = shoppingCartItem.Product.SKU;
                    orderLineItem.Quantity = ((shoppingCartItem?.Product?.ZNodeConfigurableProductCollection.Count > 0) || (shoppingCartItem?.Product?.ZNodeGroupProductCollection.Count > 0)) ? 0 : shoppingCartItem.Quantity;
                    orderLineItem.Price = (shoppingCartItem?.Product?.ZNodeConfigurableProductCollection.Count > 0) ? 0 : GetParentProductPrice(shoppingCartItem);
                    orderLineItem.DiscountAmount = GetLineItemDiscountAmount(shoppingCartItem.Product.DiscountAmount, shoppingCartItem.Quantity);
                    orderLineItem.ShipSeparately = shoppingCartItem.Product.ShipSeparately;
                    orderLineItem.ParentOmsOrderLineItemsId = null;
                    orderLineItem.DownloadLink = shoppingCartItem.Product.DownloadLink;
                    orderLineItem.GroupId = shoppingCartItem.GroupId;
                    orderLineItem.IsActive = true;
                    orderLineItem.Vendor = shoppingCartItem.Product.VendorCode;
                    orderLineItem.OrderLineItemStateId = shoppingCartItem.OrderStatusId;
                    orderLineItem.IsItemStateChanged = shoppingCartItem.IsItemStateChanged;
                    if (string.Equals(shoppingCartItem.OrderStatus, ZnodeOrderStatusEnum.SHIPPED.ToString(), StringComparison.OrdinalIgnoreCase) && shoppingCartItem.IsItemStateChanged)
                        orderLineItem.ShipDate = ShipDate;
                    else if (string.Equals(shoppingCartItem.OrderStatus, ZnodeOrderStatusEnum.SHIPPED.ToString(), StringComparison.OrdinalIgnoreCase))
                        orderLineItem.ShipDate = lineItemShippingDateList.Count > 0 ? lineItemShippingDateList.Find(x => x.OmsOrderLineItemsId == shoppingCartItem.OmsOrderLineItemId).ShipDate : null;
                    if (!string.IsNullOrEmpty(shoppingCartItem.TrackingNumber))
                        orderLineItem.TrackingNumber = shoppingCartItem.TrackingNumber;
                    //to apply custom tax/shipping cost
                    orderLineItem.IsLineItemShippingCostEdited = order.IsShippingCostEdited;
                    orderLineItem.IsLineItemTaxCostEdited = order.IsTaxCostEdited;
                    orderLineItem.PartialRefundAmount = shoppingCartItem.PartialRefundAmount;
                    //Assign Auto-add-on SKUs.
                    orderLineItem.AutoAddonSku = string.IsNullOrEmpty(shoppingCartItem.AutoAddonSKUs) ? null : shoppingCartItem.AutoAddonSKUs;

                    //to set order line item attributes
                    orderLineItem.Attributes = SetLineItemAttributes(shoppingCartItem?.Product?.Attributes, shoppingCartItem?.Product.ProductCategoryIds);

                    // then make a shipping cost entry in orderlineItem table.             
                    orderLineItem.ShippingCost = orderLineItem.IsLineItemShippingCostEdited ? 0 : shoppingCartItem.ShippingCost;

                    //Set order tax to order line item.
                    orderLineItem.HST = orderLineItem.IsLineItemTaxCostEdited ? 0 : shoppingCartItem.Product.HST;
                    orderLineItem.PST = orderLineItem.IsLineItemTaxCostEdited ? 0 : shoppingCartItem.Product.PST;
                    orderLineItem.GST = orderLineItem.IsLineItemTaxCostEdited ? 0 : shoppingCartItem.Product.GST;
                    orderLineItem.VAT = orderLineItem.IsLineItemTaxCostEdited ? 0 : shoppingCartItem.Product.VAT;
                    orderLineItem.SalesTax = orderLineItem.IsLineItemTaxCostEdited ? 0 : shoppingCartItem.Product.SalesTax;
                    orderLineItem.TaxTransactionNumber = shoppingCartItem.TaxTransactionNumber;
                    orderLineItem.TaxRuleId = shoppingCartItem.TaxRuleId;
                    orderLineItem.IsConfigurableProduct = shoppingCartItem.Product.ZNodeConfigurableProductCollection.Count > 0 ? true : false;
                    orderLineItem.Custom1 = shoppingCartItem.Custom1;
                    orderLineItem.Custom2 = shoppingCartItem.Custom2;
                    orderLineItem.Custom3 = shoppingCartItem.Custom3;
                    orderLineItem.Custom4 = shoppingCartItem.Custom4;
                    orderLineItem.Custom5 = shoppingCartItem.Custom5;

                    if (shoppingCartItem.Product.RecurringBillingInd)
                    {
                        orderLineItem.RecurringBillingAmount = shoppingCartItem.Product.RecurringBillingInitialAmount;
                        orderLineItem.RecurringBillingCycles = shoppingCartItem.Product.RecurringBillingTotalCycles;
                        orderLineItem.RecurringBillingFrequency = shoppingCartItem.Product.RecurringBillingFrequency;
                        orderLineItem.RecurringBillingPeriod = shoppingCartItem.Product.RecurringBillingPeriod;
                        orderLineItem.IsRecurringBilling = true;
                    }

                    orderLineItem.OrdersDiscount = shoppingCartItem.Product.OrdersDiscount;
                    //To add personalize attribute list
                    orderLineItem.PersonaliseValueList = shoppingCartItem.PersonaliseValuesList;
                    orderLineItem.PersonaliseValuesDetail = shoppingCartItem.PersonaliseValuesDetail;
                    orderLineItem.AdditionalCost = shoppingCartItem.AdditionalCost;
                    orderLineItem.OmsOrderLineItemsId = shoppingCartItem.OmsOrderLineItemId;

                    finalCartDescription = GetArtifiCartDescription(shoppingCartItem, finalCartDescription, shoppingCartItem.Product.ZNodeAddonsProductCollection, culterCode, shoppingCartItem.Product);
                    if (!string.IsNullOrEmpty(finalCartDescription))
                        orderLineItem.Description = finalCartDescription;
                }

                AddSimpleItemInOrderLineItem(orderLineItem, shoppingCartItem);

                //to add add-on items in order line item
                AddAddOnsItemsInOrderLineItem(orderLineItem, shoppingCartItem);

                //to add bundle items in order line item
                AddBundleItemsInOrderLineItem(orderLineItem, shoppingCartItem);

                //to add Configurable item in order line item                
                AddConfigurableItemsInOrderLineItem(orderLineItem, shoppingCartItem);

                //to add Group item in order line item
                AddGroupItemsInOrderLineItem(orderLineItem, shoppingCartItem);

                //To add personalise attribute list
                orderLineItem.PersonaliseValueList = shoppingCartItem.PersonaliseValuesList;

                if (addNewOrderItem)
                    order.OrderLineItems.Add(orderLineItem);
            }


        }

        public override decimal GetLineItemQuantity(ZnodeCartItemRelationshipTypeEnum productType, decimal groupProductQuantity, decimal cartQuantity)
        {

            if (productType.Equals(ZnodeCartItemRelationshipTypeEnum.Group))
            {
                return groupProductQuantity;
            }
            else if (productType.Equals(ZnodeCartItemRelationshipTypeEnum.AddOns) && groupProductQuantity >= cartQuantity)
            {
                return groupProductQuantity;
            }
            else
            {
                return cartQuantity;
            }

        }

        public override decimal GetParentProductPrice(Libraries.ECommerce.ShoppingCart.ZnodeShoppingCartItem shoppingCartItem)
        {
            decimal price = 0;

            if (!IsAllowAddOnQuantity)
                price = shoppingCartItem.UnitPrice - (shoppingCartItem?.Product?.AddOnPrice ?? 0);
            else
                price = shoppingCartItem.UnitPrice;

            if (price < 0 && shoppingCartItem?.UnitPrice < shoppingCartItem?.Product?.AddOnPrice)
            {
                price = shoppingCartItem.UnitPrice;
            }

            //if parent product price set to null or zero then associate configurable product price else  parent product price 
            if (GetProductPrice(shoppingCartItem.Product).Equals(0))
                price = price - (shoppingCartItem?.Product?.ConfigurableProductPrice ?? 0);

            //if product type is group then price will be zero 
            if (shoppingCartItem?.Product?.GroupProductPrice > 0)
                return 0;

            return price;
        }

        public virtual bool IsAllowAddOnQuantity { get => Convert.ToBoolean(ZnodeApiSettings.IsAllowAddOnQuantity); }

        private string GetDescriptionWithPrice(Libraries.ECommerce.ShoppingCart.ZnodeShoppingCartItem shoppingCartItem, OrderLineItemModel orderLineItem, string finalCartDescription, string culterCode, string colorDescription = "")
        {
            decimal productUnitPrice = 0;
            decimal productTotalPrice = 0;
            string space = string.Empty;
            string name = string.Empty;
            if (shoppingCartItem.Product.ConfigurableProductPrice == 0)
            {
                productUnitPrice = shoppingCartItem.Product.FinalPrice;
            }
            else
            {
                productUnitPrice = shoppingCartItem.Product.ConfigurableProductPrice;
            }

            if (productUnitPrice > 0)
            {
                productTotalPrice = productUnitPrice * shoppingCartItem.Quantity;
                name = shoppingCartItem.Product.Name + space.PadRight(4) + "(+{0})" + space.PadRight(30) + "{1}";
                name = string.Format(name, ServiceHelper.FormatPriceWithCurrency(productUnitPrice, culterCode), ServiceHelper.FormatPriceWithCurrency(productTotalPrice, culterCode));
            }
            if (!string.IsNullOrEmpty(colorDescription) && colorDescription.Contains("<br/>") && !string.IsNullOrEmpty(finalCartDescription))
            {
                orderLineItem.Description = string.Concat(colorDescription.Substring(0, colorDescription.IndexOf("<br/>")), finalCartDescription);
            }
            else if (!string.IsNullOrEmpty(orderLineItem.Description) && orderLineItem.Description.Contains("<br/>") && !string.IsNullOrEmpty(finalCartDescription) && string.IsNullOrEmpty(colorDescription))
            {
                orderLineItem.Description = string.Concat(orderLineItem.Description.Substring(0, orderLineItem.Description.IndexOf("<br/>")), finalCartDescription);
            }
            else if (!string.IsNullOrEmpty(finalCartDescription))
            {
                orderLineItem.Description = finalCartDescription;
            }

            if (!string.IsNullOrEmpty(name))
            {
                orderLineItem.Description = string.Join("<br>", name, orderLineItem.Description);
            }

            return orderLineItem.Description;
        }

        //to add child line items in order line item model as per product type
        public override void AddLineItemsInOrderLineItem(OrderLineItemModel orderLineItem, ZnodeProductBaseEntity product, Libraries.ECommerce.ShoppingCart.ZnodeShoppingCartItem shoppingCartItem, ZnodeCartItemRelationshipTypeEnum cartItemProductType)
        {
            string finalCartDescription = string.Empty;
            List<DefaultGlobalConfigModel> defaultGlobalSettingData = GetDefaultGlobalSettingData();
            string culterCode = GetDefaultCulture(defaultGlobalSettingData);
            //finalCartDescription = GetArtifiCartDescription(shoppingCartItem, finalCartDescription, shoppingCartItem.Product.ZNodeAddonsProductCollection, culterCode);

            OrderLineItemModel childLineItem = new OrderLineItemModel();
            childLineItem.OmsOrderShipmentId = orderLineItem.OmsOrderShipmentId;
            childLineItem.Description = shoppingCartItem.Product.ShoppingCartDescription;
            finalCartDescription = GetArtifiCartDescription(shoppingCartItem, finalCartDescription, shoppingCartItem.Product.ZNodeAddonsProductCollection, culterCode, shoppingCartItem.Product);
            if (!string.IsNullOrEmpty(finalCartDescription))
            {
                childLineItem.Description = finalCartDescription;
            }
            childLineItem.ProductName = product.Name;
            childLineItem.Sku = product.SKU;
            childLineItem.Quantity = GetLineItemQuantity(cartItemProductType, product.SelectedQuantity, shoppingCartItem.Quantity);
            childLineItem.Price = IsNotNull(shoppingCartItem?.CustomUnitPrice) && !Equals(cartItemProductType, ZnodeCartItemRelationshipTypeEnum.Bundles) && !Equals(cartItemProductType, ZnodeCartItemRelationshipTypeEnum.AddOns) && !Equals(cartItemProductType, ZnodeCartItemRelationshipTypeEnum.Group) ? shoppingCartItem.CustomUnitPrice.GetValueOrDefault() : GetOrderLineItemPrice(cartItemProductType, product, shoppingCartItem);
            childLineItem.DiscountAmount = GetLineItemDiscountAmount(product.DiscountAmount, childLineItem.Quantity);
            childLineItem.ShipSeparately = product.ShipSeparately;
            childLineItem.ParentOmsOrderLineItemsId = orderLineItem.OmsOrderLineItemsId;
            childLineItem.OrderLineItemRelationshipTypeId = (int)cartItemProductType;
            childLineItem.DownloadLink = product.DownloadLink;
            childLineItem.IsActive = true;
            childLineItem.OrdersDiscount = product.OrdersDiscount;
            childLineItem.OrderLineItemStateId = orderLineItem.OrderLineItemStateId;
            childLineItem.TrackingNumber = orderLineItem.TrackingNumber;
            //Set order tax to order line item.
            childLineItem.HST = orderLineItem.IsLineItemTaxCostEdited ? 0 : product.HST;
            childLineItem.PST = orderLineItem.IsLineItemTaxCostEdited ? 0 : product.PST;
            childLineItem.GST = orderLineItem.IsLineItemTaxCostEdited ? 0 : product.GST;
            childLineItem.VAT = orderLineItem.IsLineItemTaxCostEdited ? 0 : product.VAT;
            childLineItem.SalesTax = orderLineItem.IsLineItemTaxCostEdited ? 0 : product.SalesTax;
            childLineItem.TaxTransactionNumber = orderLineItem.TaxTransactionNumber;
            childLineItem.TaxRuleId = orderLineItem.TaxRuleId;
            childLineItem.PartialRefundAmount = orderLineItem.PartialRefundAmount;
            childLineItem.Custom1 = shoppingCartItem.Custom1;
            childLineItem.Custom2 = shoppingCartItem.Custom2;
            childLineItem.Custom3 = shoppingCartItem.Custom3;
            childLineItem.Custom4 = shoppingCartItem.Custom4;
            childLineItem.Custom5 = shoppingCartItem.Custom5;
            childLineItem.ParentProductSKU = shoppingCartItem.ParentProductSKU;

            if (!string.IsNullOrEmpty(product.SKU))
            {
                IZnodeRepository<ZnodeInventory> _inventoryRepository = new ZnodeRepository<ZnodeInventory>();

                InventorySKUModel inventorySKUModel = _inventoryRepository.Table.FirstOrDefault(x => x.SKU == product.SKU)?.ToModel<InventorySKUModel>();
                List<Dictionary<string, string>> inventoryDictonary = new List<Dictionary<string, string>>();
                Dictionary<string, string> keyValuePairs = new Dictionary<string, string>();
                if (IsNotNull(inventorySKUModel) && IsNotNull(inventorySKUModel.BackOrderExpectedDate) && inventorySKUModel.Quantity <= 0.0M)
                {
                    string backOrderDate = inventorySKUModel.BackOrderExpectedDate.ToString();
                    if (!string.IsNullOrEmpty(backOrderDate))
                    {
                        keyValuePairs.Add("BackorderDate", backOrderDate.ToString());
                    }

                }
                if (product.SKU == childLineItem.Sku && HelperUtility.IsNotNull(product.RetailPrice))
                {
                    string msrp = product.RetailPrice.ToString();
                    if (!string.IsNullOrEmpty(msrp))
                    {
                        keyValuePairs.Add("MSRP", msrp.ToString());

                    }
                }

                if (keyValuePairs.Count > 0)
                {
                    inventoryDictonary.Add(keyValuePairs);
                    string backOrderJson = JsonConvert.SerializeObject(inventoryDictonary);
                    if (!string.IsNullOrEmpty(backOrderJson))
                    {
                        childLineItem.Custom4 = backOrderJson;
                    }
                }

            }
            childLineItem.Attributes = SetLineItemAttributes(product?.Attributes, product?.ProductCategoryIds);

            if (product.ShipSeparately)
                childLineItem.ShippingCost = orderLineItem.IsLineItemShippingCostEdited ? 0 : product.ShippingCost;

            if (product.RecurringBillingInd)
            {
                childLineItem.RecurringBillingAmount = product.RecurringBillingInitialAmount;
                childLineItem.RecurringBillingCycles = product.RecurringBillingTotalCycles;
                childLineItem.RecurringBillingFrequency = product.RecurringBillingFrequency;
                childLineItem.RecurringBillingPeriod = product.RecurringBillingPeriod;
                childLineItem.IsRecurringBilling = true;
            }
            orderLineItem.OrderLineItemCollection.Add(childLineItem);
        }

        public override void AddSimpleProductLineItemsInOrderLineItem(OrderLineItemModel orderLineItem, ZnodeCartItemRelationshipTypeEnum cartItemProductType)
        {
            string finalCartDescription = string.Empty;
            List<DefaultGlobalConfigModel> defaultGlobalSettingData = GetDefaultGlobalSettingData();
            string culterCode = GetDefaultCulture(defaultGlobalSettingData);
            OrderLineItemModel childLineItem = new OrderLineItemModel();
            childLineItem.OmsOrderShipmentId = orderLineItem.OmsOrderShipmentId;
            childLineItem.Description = orderLineItem.Description;
            //finalCartDescription = GetArtifiCartDescription(orderLineItem, finalCartDescription, shoppingCartItem.Product.ZNodeAddonsProductCollection, culterCode);
            //childLineItem.Description = GetDescriptionWithPrice(orderLineItem, orderLineItem, finalCartDescription, culterCode, shoppingCartItem.Product.ShoppingCartDescription);
            childLineItem.ProductName = orderLineItem.ProductName;
            childLineItem.Sku = orderLineItem.Sku;
            childLineItem.Quantity = orderLineItem.Quantity;
            childLineItem.Price = orderLineItem.Price;
            childLineItem.DiscountAmount = orderLineItem.DiscountAmount;
            childLineItem.ShipSeparately = orderLineItem.ShipSeparately;
            childLineItem.ParentOmsOrderLineItemsId = orderLineItem.OmsOrderLineItemsId;
            childLineItem.OrderLineItemRelationshipTypeId = (int)cartItemProductType;
            childLineItem.DownloadLink = orderLineItem.DownloadLink;
            childLineItem.IsActive = true;
            childLineItem.OrdersDiscount = orderLineItem.OrdersDiscount;
            childLineItem.OrderLineItemStateId = orderLineItem.OrderLineItemStateId;
            childLineItem.TrackingNumber = orderLineItem.TrackingNumber;
            //Set order tax to order line item.
            childLineItem.HST = orderLineItem.IsLineItemTaxCostEdited ? 0 : orderLineItem.HST;
            childLineItem.PST = orderLineItem.IsLineItemTaxCostEdited ? 0 : orderLineItem.PST;
            childLineItem.GST = orderLineItem.IsLineItemTaxCostEdited ? 0 : orderLineItem.GST;
            childLineItem.VAT = orderLineItem.IsLineItemTaxCostEdited ? 0 : orderLineItem.VAT;
            childLineItem.SalesTax = orderLineItem.IsLineItemTaxCostEdited ? 0 : orderLineItem.SalesTax;
            childLineItem.TaxTransactionNumber = orderLineItem.TaxTransactionNumber;
            childLineItem.TaxRuleId = orderLineItem.TaxRuleId;
            childLineItem.PartialRefundAmount = orderLineItem.PartialRefundAmount;
            childLineItem.Custom1 = orderLineItem.Custom1;
            childLineItem.Custom2 = orderLineItem.Custom2;
            childLineItem.Custom3 = orderLineItem.Custom3;
            childLineItem.Custom4 = orderLineItem.Custom4;
            childLineItem.Custom5 = orderLineItem.Custom5;

            childLineItem.Attributes = orderLineItem.Attributes;

            childLineItem.ShippingCost = orderLineItem.ShippingCost;


            childLineItem.RecurringBillingAmount = orderLineItem.RecurringBillingAmount;
            childLineItem.RecurringBillingCycles = orderLineItem.RecurringBillingCycles;
            childLineItem.RecurringBillingFrequency = orderLineItem.RecurringBillingFrequency;
            childLineItem.RecurringBillingPeriod = orderLineItem.RecurringBillingPeriod;
            childLineItem.IsRecurringBilling = orderLineItem.IsRecurringBilling;

            childLineItem.PersonaliseValueList = orderLineItem.PersonaliseValueList;
            childLineItem.PersonaliseValuesDetail = orderLineItem.PersonaliseValuesDetail;
            childLineItem.ProductImagePath = orderLineItem.ProductImagePath;

            orderLineItem.OrderLineItemCollection.Add(childLineItem);
        }
        private string GetArtifiCartDescription(Libraries.ECommerce.Entities.ZnodeShoppingCartItem cartItem, string finalCartDescription, ZnodeGenericCollection<ZnodeProductBaseEntity> znodeAddonsProductCollection, string culterCode, ZnodeProductBaseEntity znodeProduct)
        {
            decimal textUnitPrice = 0;
            decimal logoUnitPrice = 0;
            decimal productUnitPrice = 0;
            decimal productTotalPrice = 0;
            decimal largeLogoUnitPrice = 0;
            string name = string.Empty;
            string configProductCartDescription = string.Empty;
            if (znodeProduct?.ZNodeConfigurableProductCollection?.Count > 0)
            {
                foreach (ZnodeProductBaseEntity productBaseEntity in znodeProduct?.ZNodeConfigurableProductCollection)
                {
                    if (znodeProduct?.ProductID == productBaseEntity?.ProductID)
                    {
                        configProductCartDescription = productBaseEntity?.Description;
                        if (!string.IsNullOrEmpty(configProductCartDescription) && configProductCartDescription.Contains("-"))
                        {
                            configProductCartDescription = configProductCartDescription.Replace("-", ":");
                        }
                    }

                }
            }
            string productDescription = "<div class='d-flex flex-column mt-3'><div class='d-flex flex-row justify-content-between cart-width'><div><p>{0}</p></div><div><p>{1}</p></div></div><div><p>{2}</p></div></div>";
            if (znodeProduct.ConfigurableProductPrice == 0)
            {
                productUnitPrice = znodeProduct.FinalPrice;
            }
            else
            {
                productUnitPrice = znodeProduct.ConfigurableProductPrice;
            }
            if (productUnitPrice > 0)
            {
                productTotalPrice = productUnitPrice * cartItem.Quantity;
                 name = string.Join("", "<b>" + znodeProduct.Name + "</b>" , "(+" + " " + ServiceHelper.FormatPriceWithCurrency(productUnitPrice, culterCode) + ")");
            }
            productDescription = string.Format(productDescription, name, ServiceHelper.FormatPriceWithCurrency(productTotalPrice, culterCode), configProductCartDescription);
            foreach (ZnodeProductBaseEntity item in znodeAddonsProductCollection)
            {
                if (item.SKU == "TextLines")
                {
                    textUnitPrice = item.FinalPrice;
                }

                if (item.SKU == "Logo")
                {
                    logoUnitPrice = item.FinalPrice;
                }

                if (item.SKU == "LargeLogo")
                {
                    largeLogoUnitPrice = item.FinalPrice;
                }
            }
            if (IsNotNull(cartItem.PersonaliseValuesDetail) && cartItem.PersonaliseValuesDetail.Count > 0)
            {
                foreach (PersonaliseValueModel item in cartItem.PersonaliseValuesDetail)
                {
                    string locationDescription = "<div class='d-flex flex-column mt-3'><div class='d-flex'> <div><p><b>Customization Location</b>: {0}</p></div><div><p></p></div></div><div class='d-flex justify-content-between cart-width'><div><p><b>Text</b>: {1}{2}</p></div><div><p>{3}</p></div></div><div class='d-flex justify-content-between'><div><p><b>Text Color</b>: {4}</p></div><div><p></p></div></div></div>";
                    string logoDescription = "<div class='d-flex  justify-content-between cart-width'><div><p><b>Logo</b>: {0}<b> {1}</b></p></div><div><p><b>{2}</b></p></div></div>";
                    string onlyLogoDescription = "<div class='d-flex mt-3 flex-column justify-content-between'><div class='d-flex'><div><p><b>Customization Location: {0}</b></p></div><div><p></p></div></div><div class='d-flex justify-content-between cart-width'><div><p><b>Logo</b>: {1}{2}</p></div><div><p>{3}</p></div></div></div>";
                    string finalLogoDescription = string.Empty;
                    bool personalizedText = false;
                    bool image = false;
                    decimal textPrice = 0;
                    decimal textTotalPrice = 0;
                    decimal logoTotalPrice = 0;
                    if (item.PersonalizeCode != "ArtifiDesignId" && item.PersonalizeCode != "ArtifiImagePath" && item.PersonalizeCode != "ArtifiSKU")
                    {
                        ArtifiPersonalizeModel artifiPersonalizeModel = JsonConvert.DeserializeObject<ArtifiPersonalizeModel>(item.PersonalizeValue);
                        if (artifiPersonalizeModel?.text?.Count >= 1)
                        {
                            string textPriceInString = string.Empty;
                            if (!string.IsNullOrEmpty(artifiPersonalizeModel.text[0].noOfLines) && !string.IsNullOrEmpty(artifiPersonalizeModel.text[0].text))
                            {
                                textPrice = Convert.ToDecimal(artifiPersonalizeModel.text[0].noOfLines) * textUnitPrice;
                                textTotalPrice = textPrice * cartItem.Quantity;
                                textPriceInString = "(+" + " " + ServiceHelper.FormatPriceWithCurrency(textPrice, culterCode) + ")";
                                locationDescription = string.Format(locationDescription, item.PersonalizeCode, artifiPersonalizeModel.text[0].text, textPriceInString, ServiceHelper.FormatPriceWithCurrency(textTotalPrice, culterCode), artifiPersonalizeModel.text[0].colorName);
                                personalizedText = true;
                            }
                        }

                        if (artifiPersonalizeModel?.image?.Count >= 1)
                        {
                            string logoUnitPriceInString = string.Empty;
                            if (!string.IsNullOrEmpty(artifiPersonalizeModel.image[0].imageName))
                            {
                                logoTotalPrice = logoUnitPrice * cartItem.Quantity;
                                logoUnitPriceInString = "(+" + " " + ServiceHelper.FormatPriceWithCurrency(logoUnitPrice, culterCode) + ")";
                                if (item.PersonalizeCode == "Back" && !string.IsNullOrEmpty(artifiPersonalizeModel.image[0].imageName))
                                {
                                    logoUnitPriceInString = "(+" + " " + ServiceHelper.FormatPriceWithCurrency(largeLogoUnitPrice, culterCode) + ")";
                                    logoTotalPrice = largeLogoUnitPrice * cartItem.Quantity;
                                }
                                if (personalizedText)
                                {
                                    finalLogoDescription = string.Format(logoDescription, artifiPersonalizeModel.image[0].imageName, logoUnitPriceInString, ServiceHelper.FormatPriceWithCurrency(logoTotalPrice, culterCode));
                                }
                                else
                                {
                                    finalLogoDescription = string.Format(onlyLogoDescription, item.PersonalizeCode, artifiPersonalizeModel.image[0].imageName, logoUnitPriceInString, ServiceHelper.FormatPriceWithCurrency(logoTotalPrice, culterCode));
                                }
                                image = true;
                            }
                        }
                        if (personalizedText && image)
                        {
                            if (string.IsNullOrEmpty(finalCartDescription))
                            {
                                finalCartDescription = string.Join("", finalCartDescription, productDescription);
                            }
                            finalCartDescription = string.Join("", finalCartDescription, locationDescription, finalLogoDescription);
                        }
                        else if (personalizedText)
                        {
                            if (string.IsNullOrEmpty(finalCartDescription))
                            {
                                finalCartDescription = string.Join("", finalCartDescription, productDescription);
                            }
                            finalCartDescription = string.Join("", finalCartDescription, locationDescription);
                        }
                        else if (image)
                        {
                            if (string.IsNullOrEmpty(finalCartDescription))
                            {
                                finalCartDescription = string.Join(" ", finalCartDescription, productDescription);
                            }
                            finalCartDescription = string.Join("", finalCartDescription, finalLogoDescription);
                        }

                    }
                }
            }
            return finalCartDescription;
        }

        private List<DefaultGlobalConfigModel> GetDefaultGlobalSettingData()
        {
            IDefaultGlobalConfigService defaultGlobalConfigService = ZnodeDependencyResolver.GetService<IDefaultGlobalConfigService>();
            return defaultGlobalConfigService.GetDefaultGlobalConfigList()?.DefaultGlobalConfigs;
        }

        private string GetDefaultCulture(List<DefaultGlobalConfigModel> defaultGlobalSettingData)
          => defaultGlobalSettingData?.Where(x => string.Equals(x.FeatureName, GlobalSettingEnum.Culture.ToString(), StringComparison.CurrentCultureIgnoreCase))?.Select(x => x.FeatureValues)?.FirstOrDefault();

        // to set order additional details like shippingid,purchaseordernumber & referraluserid etc
        public override void SetOrderAdditionalDetails(ZnodeOrderFulfillment order, SubmitOrderModel model)
        {
            if (order?.ShippingDiscount > 0)
                order.Order.Custom4 = order.ShippingDiscount.ToString();

            base.SetOrderAdditionalDetails(order, model);
        }
    }
}