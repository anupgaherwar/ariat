﻿using System;

namespace Znode.Libraries.DevExpress.Report.ReportSchemaModels.Vouchers
{
    public class VoucherInfo : BaseInfo
    {
        public DateTime? BeginDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Name { get; set; }
        public string CardNumber { get; set; }
        public decimal? Amount { get; set; }
        public decimal? RemainingAmount { get; set; }

        public string StoreName { get; set; }
        public string CustomerName { get; set; }
        public string CustomerEmail { get; set; }
        public string CurrencyCode { get; set; }
        public string CultureCode { get; set; }
        public string AccountName { get; set; }
        public string ExternalId { get; set; }
        public string Symbol { get; set; }
        public string SPWhereClause { get; set; }
        public string CreatedByName { get; set; }

        public string CreatedDateString { get; set; }
        public string ExpirationDateString { get; set; }
        public string IsActive { get; set; }
    }
}
