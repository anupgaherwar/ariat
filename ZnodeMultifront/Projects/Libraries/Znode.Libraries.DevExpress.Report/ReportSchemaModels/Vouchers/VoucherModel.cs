﻿using DevExpress.DataAccess.ObjectBinding;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Libraries.DevExpress.Report.ReportSchemaModels.Vouchers
{
    [DisplayName("Vouchers")]
    [HighlightedClass]
    public class VoucherModel : List<VoucherInfo>
    {

        [HighlightedMember]
        public VoucherModel()
        {

        }
    }
}
