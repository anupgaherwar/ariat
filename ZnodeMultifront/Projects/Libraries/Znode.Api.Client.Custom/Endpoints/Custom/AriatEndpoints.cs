﻿using System;

using Znode.Engine.Api.Client.Endpoints;

namespace Znode.Api.Client.Custom.Endpoints
{
    public class AriatEndpoints : BaseEndpoint
    {
        //Get shipping rate list
        public static string ShippingRateList() => $"{ApiRoot}/ariatshipping/shippingratelist";

        //Create shipping rate details
        public static string CreateShippingRate() => $"{ApiRoot}/ariatshipping/createshippingrate";

        //Update shipping rate details
        public static string EditShippingRate() => $"{ApiRoot}/ariatshipping/editshippingrate";

        //get customization pricing details
        public static string GetDecorationPrice() => $"{ApiRoot}/decorationprice/getdecorationprice";


        //Delete and existing record.
        public static string DeleteShippingRate() => $"{ApiRoot}/ariatshipping/deleteshippingrate";

        //Get shipping rate details
        public static string GetShippingRateDetails(int ariatCustomShippingChargesId) => $"{ApiRoot}/ariatshipping/getshippingratedetails/{ariatCustomShippingChargesId}";

        //Get znode sku from artifi sku
        public static string GetPublishProductSkuByArtifiSku(string artifySku) => $"{ApiRoot}/ariatproduct/getpublishproductskubyartifisku/{artifySku}";

        //get znode sku from artifi sku
        public static string GetZnodeSKU() => $"{ApiRoot}/ariatproduct/getznodesku";
        /*Generate OrderNumber Start*/
        public static string GenerateOrderNumber(int portalId) => $"{ApiRoot}/ariatorder/generateordernumber/{portalId}";

        public static string GetAdyenShippingAddress()=> $"{ApiRoot}/ariatuser/getadyenshippingaddress";
        /*Generate OrderNumber End*/

    }
}
