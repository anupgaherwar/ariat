﻿
using Znode.Engine.Api.Client.Endpoints;

namespace Znode.Api.Client.Custom.Endpoints
{
    public class AriatAccountEndpoint : BaseEndpoint
    {
        //Get program catalog product list
        public static string ProgramCatalogProductList() => $"{ApiRoot}/ariataccount/programcatalogproductlist";

        //Get full catalog product list.
        public static string FullCatalogProductList() => $"{ApiRoot}/ariataccount/fullcatalogproductlist";

        //Delete the products.
        public static string Delete() => $"{ApiRoot}/ariataccount/delete";

        //Publish The program catalog.
        public static string Publish() => $"{ApiRoot}/ariataccount/publish";
     
    }
}
