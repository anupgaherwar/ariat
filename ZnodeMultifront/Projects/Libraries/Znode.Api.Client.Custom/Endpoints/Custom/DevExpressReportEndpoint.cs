﻿
using Znode.Engine.Api.Client.Endpoints;

namespace Znode.Api.Client.Custom.Endpoints
{
    public class AriatDevExpressReportEndpoint : BaseEndpoint
    {
        public static string GetVouchersReport() => $"{ApiRoot}/ariatreport/getvouchersreport";   
    }
}
