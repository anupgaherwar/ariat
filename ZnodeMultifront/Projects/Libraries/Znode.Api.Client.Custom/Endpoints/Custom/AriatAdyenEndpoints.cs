﻿using Znode.Engine.Api.Client.Endpoints;

namespace Znode.Api.Client.Custom.Endpoints
{
    public class AriatAdyenEndpoints : BaseEndpoint
    {
        public static string GetDropin() => $"{PaymentApiRoot}/payment/getdropin";
    }
}
