﻿
using Znode.Custom.Api.Model;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Api.Client.Custom.Response
{
    public class AriatAdyenResponse : BaseResponse
    {
        public AriatAdyenDropInModel ResponseModel { get; set; }
    }
}
