﻿using Znode.Custom.Api.Model;
using Znode.Engine.Api.Client;

namespace Znode.Api.Client.Custom.Clients
{
    public interface IAriatAdyenClient : IBaseClient
    {
        /// <summary>
        /// Get Dropin Response
        /// </summary>
        /// <param name="adyenDropinRequestModel">Request model</param>
        /// <returns>dropin response</returns>
        AriatAdyenDropInModel GetDropin(AdyenDropinRequestModel adyenDropinRequestModel);
    }
}
