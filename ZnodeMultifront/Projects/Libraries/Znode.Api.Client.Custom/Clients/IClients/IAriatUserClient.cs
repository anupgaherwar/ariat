﻿using Znode.Custom.Api.Model;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models;
using Znode.Sample.Api.Model;

namespace Znode.Api.Client.Custom.Clients
{
    public interface IAriatUserClient : IBaseClient
    {
        /// <summary>
        /// Get Adyen Shipping Address
        /// </summary>
        /// <param name="addressViewModel">Request model</param>
        /// <returns>address</returns>
        AddressModel GetAdyenShippingAddress(AddressModel addressViewModel);
    }
}
