﻿using System.Collections.Generic;

using Znode.Custom.Api.Model;
using Znode.Engine.Api.Client;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Api.Client.Custom.Clients
{
    public interface IAriatDevExpressClient : IBaseClient
    {
        /// <summary>
        /// Get vouchers report
        /// </summary>
        /// <param name="filters">filter for reports.</param>
        /// <returns>report data.</returns>
        List<AriatVoucherReportModel> GetVouchersReport(FilterCollection filters = null);
    }
}
