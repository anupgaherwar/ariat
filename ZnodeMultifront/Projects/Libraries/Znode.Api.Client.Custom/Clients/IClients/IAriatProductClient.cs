﻿
using Znode.Custom.Api.Model;
using Znode.Engine.Api.Client;

namespace Znode.WebStore.Custom.Agents.IAgents
{
    public interface IAriatProductClient : IBaseClient
    {
        /// <summary>
        /// Get znode sku from artfi ski
        /// </summary>
        /// <param name="znodeSKURequestModel">request model</param>
        /// <returns>znode sku</returns>
        ZnodeSKURequestModel GetZnodeSKU(ZnodeSKURequestModel znodeSKURequestModel);
    }
}
