﻿
using Znode.Custom.Api.Model;
using Znode.Custom.Api.Model.ParameterModel;
using Znode.Engine.Api.Client;

namespace Znode.Api.Client.Custom.Clients
{
    public interface IDecorationPriceClient : IBaseClient
    {
        /// <summary>
        /// Get customization pricing details
        /// </summary>
        /// <param name="priceParameterModel">model to get the pricing details</param>
        /// <returns>Price model</returns>
        DecorationPriceModel GetDecorationPrice(PriceParameterModel priceParameterModel);
    }
}
