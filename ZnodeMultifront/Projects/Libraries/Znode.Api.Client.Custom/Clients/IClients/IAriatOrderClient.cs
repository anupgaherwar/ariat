﻿using Znode.Engine.Api.Client;

namespace Znode.Api.Client.Custom.Clients.IClients
{
    public interface IAriatOrderClient : IOrderClient
    {
        /// <summary>
        /// Get Order Number.
        /// </summary>
        /// <param name="portalId">Id of portal.</param>
        /// <returns></returns>
        string GenerateOrderNumber(int portalId);
    }
}
