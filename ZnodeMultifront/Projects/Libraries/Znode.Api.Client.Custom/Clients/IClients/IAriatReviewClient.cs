﻿using System.Threading.Tasks;
using System.Web;
namespace Znode.Engine.Api.Client
{
    public interface IAriatReviewClient : IBaseClient
    {
        /// <summary>
        /// Get the Bazaar Voice Data.
        /// </summary>
        /// <param name="getDataUrl">Url to get the data</param>
        /// <param name="apiVersion">Api Version</param>
        /// <param name="sku">SKU</param>
        /// <param name="passKey">PassKey</param>
        /// <returns></returns>
        string GetBazaarVoiceData(string url);

        /// <summary>
        /// Get the image url
        /// </summary>
        /// <param name="imageUrls">imageUrls</param>
        /// <param name="files">files</param>
        /// <param name="photoUploadUrl">photoUploadUrl</param>
        /// <returns></returns>
        Task<string> GetImageURL(string imageUrls, HttpPostedFileBase files, string photoUploadUrl);

        /// <summary>
        /// Submit form data
        /// </summary>
        /// <param name="endPoint">endPoint</param>
        /// <param name="formString">formString</param>
        /// <returns></returns>
        string SubmitBazaarVoiceForms(string endPoint, string formString);

    }
}
