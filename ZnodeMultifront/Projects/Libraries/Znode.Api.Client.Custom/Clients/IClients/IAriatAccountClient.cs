﻿
using Znode.Custom.Api.Model;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Api.Client.Custom.Clients
{
    public interface IAriatAccountClient : IBaseClient
    {
        /// <summary>
        /// Get program catalog product list.
        /// </summary>
        /// <param name="expands">Expand for program catalog product list</param>
        /// <param name="filters">Filters for program catalog product list</param>
        /// <param name="sortCollection">Sorts for program catalog product list</param>
        /// <param name="page">Page number</param>
        /// <param name="recordPerPage">Page size</param>
        /// <returns>Program catalog product list.</returns>
        AriatProgramCatalogProductListModel ProgramCatalogProductList(ExpandCollection expands, FilterCollection filters, SortCollection sortCollection, int page, int recordPerPage);

        /// <summary>
        /// Get full catalog product list
        /// </summary>
        /// <param name="expands">Expand for program catalog product list</param>
        /// <param name="filters">Filters for program catalog product list</param>
        /// <param name="sorts">Sorts for program catalog product list</param>
        /// <param name="pageIndex">Page number</param>
        /// <param name="pageSize">Page size</param>
        /// <returns></returns>
        AriatFullCatalogProductListModel FullCatalogProductListModel (ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Delete product by ariatProgramCatalogProductIds.
        /// </summary>
        /// <param name="ariatProgramCatalogProductId">AriatProgramCatalogProductId Ids to be deleted.</param>
        /// <returns>True if product is deleted successfully; false if product fails to delete.</returns>
        bool DeleteProducts(ParameterModel ariatProgramCatalogProductId);
     
        /// <summary>
        /// Publish program catalog
        /// </summary>
        /// <param name="parameterModel">Account External Id as parmeter</param>
        /// <returns></returns>
        PublishedModel Publish(ParameterModel parameterModel);
    }
}
