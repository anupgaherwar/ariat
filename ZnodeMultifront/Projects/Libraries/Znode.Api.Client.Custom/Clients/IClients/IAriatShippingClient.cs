﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Sample.Api.Model;

namespace Znode.Api.Client.Custom.Clients
{
    public interface IAriatShippingClient : IBaseClient
    {
        /// <summary>
        /// Get Shipping rate List
        /// </summary>
        /// <param name="expands">Expands for shipping rate list</param>
        /// <param name="filters">filters for shipping rate list</param>
        /// <param name="sortCollection">Sort for shipping rate list</param>
        /// <param name="page">Page number</param>
        /// <param name="recordPerPage">Record per page</param>
        /// <returns>Shipping rate list</returns>
        AriatShippingListModel ShippingRateList(ExpandCollection expands, FilterCollection filters, SortCollection sortCollection, int page, int recordPerPage);

        /// <summary>
        /// Create Shipping rate details
        /// </summary>
        /// <param name="model"> model with shipping rate details</param>
        /// <returns>Model with created data.</returns>
        AriatShippingModel CreateShippingRate(AriatShippingModel model);
      
        /// <summary>
        /// Update exiting shipping rate details
        /// </summary>
        /// <param name="ariatShippingModel">Model with shipping rate details</param>
        /// <returns>True or False</returns>
        bool EditShippingRate(AriatShippingModel ariatShippingModel);

        /// <summary>
        /// Delete and existing record.
        /// </summary>
        /// <param name="parameterModel">Model with the record id to delete</param>
        /// <returns>True or False</returns>
        bool DeleteShippingRate(ParameterModel parameterModel);

        /// <summary>
        /// Get Shipping rate details
        /// </summary>
        /// <param name="ariatCustomShippingChargesId">shipping rate id</param>
        /// <returns>Model with shipping rate details</returns>
        AriatShippingModel GetShippingRateDetails(int ariatCustomShippingChargesId);

    }
}
