﻿using System;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Znode.Engine.Api.Client
{
    public class AriatReviewClient : BaseClient, IAriatReviewClient
    {
        #region Public Methods
        //Get the Bazaar Voice Data accoeding to URLs
        public string GetBazaarVoiceData(string urls)
        {
            string data = GetDataFromEndpoint(urls);
            return data;
        }

        //Get the data from the Endpoint
        public string GetDataFromEndpoint(string endpoint)
        {
            string baseEndPoint = endpoint;

            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(endpoint);
            req.KeepAlive = false; // Prevents "server committed a protocol violation" error
            req.Method = "GET";
            req.Timeout = RequestTimeout;
            StreamReader streamIn =
                       new StreamReader(req.GetResponse().GetResponseStream());
            string response = streamIn.ReadToEnd();
            streamIn.Close();
            return response;
        }

        //Get the BV Image
        public async Task<string> GetImageURL(string imageUrls, HttpPostedFileBase files, string photoUploadUrl)
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    using (MultipartFormDataContent content =
                        new MultipartFormDataContent("Upload----" + DateTime.Now.ToString(CultureInfo.InvariantCulture)))
                    {
                        content.Add(new StreamContent(files.InputStream), "photo", files.FileName);

                        using (
                            HttpResponseMessage message =
                               await client.PostAsync(photoUploadUrl, content))
                        {
                            imageUrls = await message.Content.ReadAsStringAsync();
                            if (imageUrls.Contains("{"))
                            {
                                string[] imageArray = imageUrls.Split('{');
                                foreach (string item in imageArray)
                                {
                                    if (item.Contains("Thumb."))
                                    {
                                        imageUrls = item.Split(',')[1].Split('"')[3];
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return imageUrls;
        }

        //Sumbit data to BV
        public string SubmitBazaarVoiceForms(string endPoint, string formString)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(formString);

            //Create a POST webrequest
            HttpWebRequest request = WebRequest.CreateHttp(endPoint);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";

            //Open request stream
            using (Stream reqStream = request.GetRequestStream())
            {
                //Write bytes to the request stream
                reqStream.Write(bytes, 0, bytes.Length);
            }

            //Try to get response
            WebResponse response = null;
            try
            {
                response = request.GetResponse();
            }
            catch (WebException e)
            {
                //Something went wrong with our request. Return the exception message.
                return e.Message;
            }
            string errorCode = string.Empty;
            //Get response stream
            using (Stream respStream = response.GetResponseStream())
            {
                //Create a streamreader to read the website's response, then return it as a string
                using (StreamReader reader = new StreamReader(respStream))
                {

                    string data = reader.ReadToEnd();
                    return data;
                }
            }
        } 
        #endregion

    }

}
