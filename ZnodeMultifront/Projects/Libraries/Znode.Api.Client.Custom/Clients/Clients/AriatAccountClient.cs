﻿
using Newtonsoft.Json;

using System.Collections.ObjectModel;
using System.Net;

using Znode.Api.Client.Custom.Endpoints;
using Znode.Custom.Api.Model;
using Znode.Custom.Api.Model.Responses;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Api.Client.Custom.Clients
{
    public class AriatAccountClient : BaseClient, IAriatAccountClient
    {
        //Get the full catalog product list.
        public AriatFullCatalogProductListModel FullCatalogProductListModel(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            string endpoint = AriatAccountEndpoint.FullCatalogProductList();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);
            ApiStatus status = new ApiStatus();

            AriatFullCatalogProductListResponse response = GetResourceFromEndpoint<AriatFullCatalogProductListResponse>(endpoint, status);

            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NoContent };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            AriatFullCatalogProductListModel list = new AriatFullCatalogProductListModel { CatalogProductsDetailList = response?.CatalogProductList };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        //Get program catalog product list.
        public AriatProgramCatalogProductListModel ProgramCatalogProductList(ExpandCollection expands, FilterCollection filters, SortCollection sortCollection, int page, int recordPerPage)
        {
            string endpoint = AriatAccountEndpoint.ProgramCatalogProductList();
            endpoint += BuildEndpointQueryString(expands, filters, sortCollection, page, recordPerPage);

            ApiStatus status = new ApiStatus();

            AriatProgramCatalogProductListResponse response = GetResourceFromEndpoint<AriatProgramCatalogProductListResponse>(endpoint, status);

            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NoContent };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            AriatProgramCatalogProductListModel list = new AriatProgramCatalogProductListModel { ProductList = response?.ProductList };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        //publish program catalog.
        public virtual PublishedModel Publish(ParameterModel parameterModel)
        {
            string endpoint = AriatAccountEndpoint.Publish();
            ApiStatus status = new ApiStatus();
            PublishedResponse response = PostResourceToEndpoint<PublishedResponse>(endpoint, JsonConvert.SerializeObject(parameterModel), status);

            //check the status of response.
            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);
            return response?.PublishedModel;
        }

        //Delete the product according to ariatProgramCatalogProductId.
        public virtual bool DeleteProducts(ParameterModel ariatProgramCatalogProductId)
        {
            //Get Endpoint.
            string endpoint = AriatAccountEndpoint.Delete();

            ApiStatus status = new ApiStatus();
            TrueFalseResponse response = PostResourceToEndpoint<TrueFalseResponse>(endpoint, JsonConvert.SerializeObject(ariatProgramCatalogProductId), status);

            //check the status of response.
            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);
            return response.IsSuccess;
        }
    }
}
