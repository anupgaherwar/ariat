﻿

using Newtonsoft.Json;

using System.Net;

using Znode.Api.Client.Custom.Endpoints;
using Znode.Api.Client.Custom.Response;
using Znode.Custom.Api.Model;
using Znode.Engine.Api.Client;
using Znode.Engine.Exceptions;

namespace Znode.Api.Client.Custom.Clients
{
    public class AriatAdyenClient : BaseClient, IAriatAdyenClient
    {
        #region public methods
       
        //Get Dropin component
        public AriatAdyenDropInModel GetDropin(AdyenDropinRequestModel adyenDropinRequestModel)
        {
            string endpoint = AriatAdyenEndpoints.GetDropin();

            ApiStatus status = new ApiStatus();
            AriatAdyenResponse response = PostResourceToEndpoint<AriatAdyenResponse>(endpoint, JsonConvert.SerializeObject(adyenDropinRequestModel), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return response?.ResponseModel;
        }
        #endregion
    }
}
