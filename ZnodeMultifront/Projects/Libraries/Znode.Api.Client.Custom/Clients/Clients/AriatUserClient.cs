﻿using Newtonsoft.Json;
using System.Net;
using Znode.Api.Client.Custom.Endpoints;
using Znode.Custom.Api.Model.Responses;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;

namespace Znode.Api.Client.Custom.Clients
{
    public class AriatUserClient : BaseClient, IAriatUserClient
    {
        public AddressModel GetAdyenShippingAddress(AddressModel addressViewModel)
        {
            string endpoint = AriatEndpoints.GetAdyenShippingAddress();
            ApiStatus status = new ApiStatus();

            AriatUserResponse response = PostResourceToEndpoint<AriatUserResponse>(endpoint, JsonConvert.SerializeObject(addressViewModel), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return response?.ShippingAddress;

            //ApiStatus status = new ApiStatus();
            throw new System.NotImplementedException();
        }
        #region public methods        
       
        #endregion
    }
}
