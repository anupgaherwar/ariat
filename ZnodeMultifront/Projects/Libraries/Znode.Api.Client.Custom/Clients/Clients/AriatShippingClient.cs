﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Znode.Api.Client.Custom.Endpoints;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Sample.Api.Model;
using Znode.Sample.Api.Model.Responses;

namespace Znode.Api.Client.Custom.Clients.Clients
{
    public class AriatShippingClient : BaseClient, IAriatShippingClient
    {
        //Create shipping rate details
        public AriatShippingModel CreateShippingRate(AriatShippingModel model)
        {
            string endpoint = AriatEndpoints.CreateShippingRate();

            ApiStatus status = new ApiStatus();
            AriatShippingRateResponse response = PostResourceToEndpoint<AriatShippingRateResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return response?.Shipping;
        }

        //Delete shipping rate
        public bool DeleteShippingRate(ParameterModel parameterModel)
        {
            string endpoint = AriatEndpoints.DeleteShippingRate();

            ApiStatus status = new ApiStatus();
            TrueFalseResponse response = PostResourceToEndpoint<TrueFalseResponse>(endpoint, JsonConvert.SerializeObject(parameterModel), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return response.IsSuccess;
        }

        //Update shipping rate details
        public bool EditShippingRate(AriatShippingModel ariatShippingModel)
        {
            string endpoint = AriatEndpoints.EditShippingRate();

            ApiStatus status = new ApiStatus();
            TrueFalseResponse response = PutResourceToEndpoint<TrueFalseResponse>(endpoint, JsonConvert.SerializeObject(ariatShippingModel), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);
            return response.IsSuccess;
        }

        //Get Shipping rate details
        public AriatShippingModel GetShippingRateDetails(int ariatCustomShippingChargesId)
        {
            string endpoint = AriatEndpoints.GetShippingRateDetails(ariatCustomShippingChargesId);

            ApiStatus status = new ApiStatus();

            AriatShippingRateResponse response = GetResourceFromEndpoint<AriatShippingRateResponse>(endpoint, status);
            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NoContent };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return response?.Shipping;
        }

        //Get shipping rate list
        public AriatShippingListModel ShippingRateList(ExpandCollection expands, FilterCollection filters, SortCollection sortCollection, int page, int recordPerPage)
        {
            string endpoint = AriatEndpoints.ShippingRateList();
            endpoint += BuildEndpointQueryString(expands, filters, sortCollection, page, recordPerPage);

            ApiStatus status = new ApiStatus();

            AriatShippingRateListResponse response = GetResourceFromEndpoint<AriatShippingRateListResponse>(endpoint, status);

            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NoContent };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            AriatShippingListModel list = new AriatShippingListModel { ShippingList = response?.ShippingRateList };
            list.MapPagingDataFromResponse(response);

            return list;
        }
    }
}
