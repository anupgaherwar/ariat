﻿using Newtonsoft.Json;

using System.Net;

using Znode.Api.Client.Custom.Endpoints;
using Znode.Custom.Api.Model;
using Znode.Custom.Api.Model.Responses;
using Znode.Engine.Api.Client;
using Znode.Engine.Exceptions;
using Znode.WebStore.Custom.Agents.IAgents;

namespace Znode.Api.Client.Custom.Clients
{
    public class AriatProductClient : BaseClient, IAriatProductClient
    {
        #region public methods

        //Get znode sku from artifi sku
        public ZnodeSKURequestModel GetZnodeSKU(ZnodeSKURequestModel znodeSKURequestModel)
        {
            string endpoint = AriatEndpoints.GetZnodeSKU();

            ApiStatus status = new ApiStatus();

            AriatZnodeSKUResponse response = PostResourceToEndpoint<AriatZnodeSKUResponse>(endpoint, JsonConvert.SerializeObject(znodeSKURequestModel), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return response?.ProductData;
        }
        #endregion
    }
}
