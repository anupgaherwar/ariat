﻿using System.Collections.ObjectModel;
using System.Net;
using Znode.Api.Client.Custom.Clients.IClients;
using Znode.Api.Client.Custom.Endpoints;
using Znode.Engine.Api.Client;
using Znode.Sample.Api.Model.Responses;

namespace Znode.Api.Client.Custom
{
    public class AriatOrderClient : OrderClient, IAriatOrderClient
    {
        /*Generate OrderNumber Start*/
        public string GenerateOrderNumber(int portalId)
        {
            string endpoint = AriatEndpoints.GenerateOrderNumber(portalId);
            endpoint += BuildEndpointQueryString(null, null, null, null, null);
            ApiStatus status = new ApiStatus();
            GenerateOrderResponse response = GetResourceFromEndpoint<GenerateOrderResponse>(endpoint, status);
            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NoContent };
            GenerateOrderResponse generateOrderNumber = new GenerateOrderResponse { OrderNumber = response?.OrderNumber };
            return response?.OrderNumber;
        }
        /*Generate OrderNumber End*/
    }
}
