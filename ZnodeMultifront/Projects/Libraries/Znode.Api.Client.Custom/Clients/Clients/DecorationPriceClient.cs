﻿using Newtonsoft.Json;

using System.Net;

using Znode.Api.Client.Custom.Endpoints;
using Znode.Custom.Api.Model;
using Znode.Custom.Api.Model.ParameterModel;
using Znode.Custom.Api.Model.Responses;
using Znode.Engine.Api.Client;
using Znode.Engine.Exceptions;

namespace Znode.Api.Client.Custom.Clients
{
    public class DecorationPriceClient : BaseClient, IDecorationPriceClient
    {
        //Get Customization pricing details
        public DecorationPriceModel GetDecorationPrice(PriceParameterModel priceParameterModel)
        {
            string endpoint = AriatEndpoints.GetDecorationPrice();

            ApiStatus status = new ApiStatus();
            DecorationPriceResponse response = PostResourceToEndpoint<DecorationPriceResponse>(endpoint, JsonConvert.SerializeObject(priceParameterModel), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return response?.Price;
        }
    }
}
