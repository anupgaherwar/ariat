﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;

using Znode.Api.Client.Custom.Endpoints;
using Znode.Custom.Api.Model;
using Znode.Custom.Api.Model.Responses;
using Znode.Engine.Api.Client;
using Znode.Engine.Exceptions;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Api.Client.Custom.Clients
{
    public class AriatDevExpressClient : BaseClient, IAriatDevExpressClient
    {
        //Get vouchers reports.
        public List<AriatVoucherReportModel> GetVouchersReport(FilterCollection filters = null)
        {
            string endpoint = AriatDevExpressReportEndpoint.GetVouchersReport();
            endpoint += BuildEndpointQueryString(null, filters, null, null, null);
            
            ApiStatus status = new ApiStatus();
            
            VoucherReportListResponse response = GetResourceFromEndpoint<VoucherReportListResponse>(endpoint, status);
            
            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NoContent };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);
           
            return response.VoucherList;
        }
    }
}
