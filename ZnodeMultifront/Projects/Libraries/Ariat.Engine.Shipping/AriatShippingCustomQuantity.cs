﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Znode.Custom.Data;
using Znode.Engine.Api.Models;
using Znode.Engine.Shipping.Helper;
using Znode.Libraries.Data;
using Znode.Libraries.ECommerce.Entities;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;

namespace Znode.Engine.Shipping
{
    public class AriatShippingCustomQuantity : ZnodeBusinessBase
    {
        #region Private variables
        //shippingcodes
        private readonly string UPSNextDayAir = "UPSNextDayAir";
        private readonly string UPS2DayAir = "UPS2DayAir";
        private readonly string UPS3DayAir = "UPS3DayAir";
        #endregion

        #region Public Methods
        /// <summary>
        /// Calculates the custom shipping rate based on weight.
        /// </summary>
        /// <param name="shoppingCart">The current shopping cart.</param>
        /// <param name="shippingBag">The shipping data for the custom shipping type.</param>

        public void Calculate(ZnodeShoppingCart shoppingCart, ZnodeShippingBag shippingBag)
        {
            ShippingRuleListModel shippingRules = new ShippingRuleListModel();
            ZnodeShippingHelper shippingHelper = new ZnodeShippingHelper();

            shippingRules.ShippingRuleList = GetRateBasedOnQuantityRuleList(shoppingCart);
            decimal totalShoppingCartQuantity = 0;
            int? totalShoppingCartWeightQuantityCount = shippingBag?.ShoppingCart?.ShoppingCartItems.Where(x => x.Product.ShippingRuleTypeCode == "QuantityBasedRate" && x.Product.FreeShippingInd == false).Count();
            int counter = 0; //These counter is used to increment that much time when the line item is weight based.

            // Determine quantity-based shipping rate for each item
            foreach (ZnodeShoppingCartItem cartItem in shippingBag?.ShoppingCart?.ShoppingCartItems ?? new List<ZnodeShoppingCartItem>())
            {
                decimal itemShippingCost = 0;
                decimal totalItemQuantity = 0;
                bool isRuleApplied = false;
                bool isAriatRuleApplied = false;
                decimal handlingCharge = 0;

                string shippingRuleTypeCode = Convert.ToString(cartItem.Product.ShippingRuleTypeCode);
                bool getsFreeShipping = cartItem.Product.FreeShippingInd;
                decimal quantity = cartItem.Quantity;

                if (Equals(shippingRuleTypeCode?.ToLower(), shippingHelper.GetShippingRuleTypesEnumValue(Convert.ToString(ZnodeShippingRuleTypes.RateBasedOnQuantity)).ToLower()) && (!getsFreeShipping))
                    totalItemQuantity += quantity;

                if (Equals(shippingRuleTypeCode?.ToLower(), shippingHelper.GetShippingRuleTypesEnumValue(Convert.ToString(ZnodeShippingRuleTypes.RateBasedOnQuantity)).ToLower()) && (!getsFreeShipping))
                {
                    totalShoppingCartQuantity += totalItemQuantity;
                    counter++; //This because we need to find the total weight of shopping cart hence used counter to 
                               //increment and get the weight of all line item when the counter and totalShoppingCartWeightQuantityCount value is same then we will find the shipping charges.
                }


                if (HelperUtility.IsNotNull(totalShoppingCartWeightQuantityCount) && counter == totalShoppingCartWeightQuantityCount && totalItemQuantity > 0)
                {
                    isRuleApplied = ApplyRule(shippingRules.ShippingRuleList, totalShoppingCartQuantity, out itemShippingCost, shippingBag.ShippingCode);

                    if (isRuleApplied && cartItem.Product.ShipSeparately)
                        cartItem.Product.ShippingCost = shippingHelper.GetItemHandlingCharge(itemShippingCost, shippingBag);

                }

                // Reset if rule is applied
                isRuleApplied = true;

                bool applyHandlingCharge = false;

                // Get shipping rate on quantity based for group product
                cartItem.Product.ShippingCost = cartItem?.Product?.ZNodeGroupProductCollection?.Count > 0 ? CalculateProductShipping(shippingBag, cartItem?.Product?.ZNodeGroupProductCollection, shoppingCart, out itemShippingCost) : cartItem.Product.ShippingCost;
                List<string> shippingCodes = new List<string>(new string[] { UPSNextDayAir, UPS2DayAir, UPS3DayAir });

                // Calculate handling charge.
                if (shippingCodes.Contains(shippingBag.ShippingCode))
                {
                    if (applyHandlingCharge && cartItem.Product.ShipSeparately)
                        handlingCharge += shippingHelper.GetOrderLevelShippingHandlingCharge(shippingBag, shoppingCart);
                }
                else if (applyHandlingCharge && cartItem.Product.ShipSeparately)
                    shoppingCart.OrderLevelShipping += shippingHelper.GetOrderLevelShippingHandlingCharge(shippingBag, shoppingCart);

                if (shippingCodes.Contains(shippingBag.ShippingCode))
                {
                    shoppingCart.OrderLevelShipping = itemShippingCost + handlingCharge;
                    isAriatRuleApplied = true;
                }
                if (isAriatRuleApplied && Equals(shippingRuleTypeCode?.ToLower(), shippingHelper.GetShippingRuleTypesEnumValue(Convert.ToString(ZnodeShippingRuleTypes.RateBasedOnWeight)).ToLower())
                    && (!getsFreeShipping) && HelperUtility.IsNotNull(totalShoppingCartWeightQuantityCount)
                    && counter == totalShoppingCartWeightQuantityCount)
                    break;

            }
        }
        #endregion

        #region Private Methods
        // ShippingRule dependency on data access
        private bool ApplyRule(List<ShippingRuleModel> shippingRules, decimal totalShoppingCartWeightQuantity, out decimal itemShippingCost, string shippingCode = "")
        {
            List<ZnodeAriatCustomShippingCharge> znodeAriatCustomShippingChargeList = new List<ZnodeAriatCustomShippingCharge>();
            ZnodeAriatCustomShippingCharge znodeAriatCustomShippingCharge = new ZnodeAriatCustomShippingCharge();
            bool isRuleApplied = false;
            itemShippingCost = 0;

            znodeAriatCustomShippingChargeList = Equals(HttpRuntime.Cache["DefaultShippingChargesCache"], null)
              ? GetShippingCharges() : (List<ZnodeAriatCustomShippingCharge>)HttpRuntime.Cache.Get("DefaultShippingChargesCache");

            znodeAriatCustomShippingCharge = znodeAriatCustomShippingChargeList.FirstOrDefault(x => x.ShippingCode == shippingCode && totalShoppingCartWeightQuantity >= x.LowerLimit && totalShoppingCartWeightQuantity <= x.UpperLimit);
            foreach (ShippingRuleModel rule in shippingRules)
            {
                if (totalShoppingCartWeightQuantity >= rule.LowerLimit && totalShoppingCartWeightQuantity <= rule.UpperLimit)
                {
                    itemShippingCost += rule.BaseCost + (rule.PerItemCost * totalShoppingCartWeightQuantity);
                }
            }
            if (HelperUtility.IsNotNull(znodeAriatCustomShippingCharge) && HelperUtility.IsNotNull(znodeAriatCustomShippingCharge))
            {
                itemShippingCost += znodeAriatCustomShippingCharge.ShippingAmount.HasValue ? Convert.ToDecimal(znodeAriatCustomShippingCharge.ShippingAmount) : 0;
                isRuleApplied = true;
            }
            else
            {
                if (totalShoppingCartWeightQuantity >= znodeAriatCustomShippingChargeList.Where(x => x.ShippingCode == shippingCode).Max(x => x.UpperLimit))
                {
                    znodeAriatCustomShippingCharge = znodeAriatCustomShippingChargeList.Where(x => x.ShippingCode == shippingCode).OrderByDescending(x => x.UpperLimit).FirstOrDefault();
                    itemShippingCost += znodeAriatCustomShippingCharge.ShippingAmount.HasValue ? Convert.ToDecimal(znodeAriatCustomShippingCharge.ShippingAmount) : 0;
                    isRuleApplied = true;
                }
            }
            return isRuleApplied;
        }

        //Get the shipping charges and insert in cache.
        private List<ZnodeAriatCustomShippingCharge> GetShippingCharges()
        {
            IZnodeRepository<ZnodeAriatCustomShippingCharge> _ariatCustomShippingCharge = new ZnodeRepository<ZnodeAriatCustomShippingCharge>(new Custom_Entities());
            List<ZnodeAriatCustomShippingCharge> znodeAriatCustomShippingChargeModel = new List<ZnodeAriatCustomShippingCharge>();

            znodeAriatCustomShippingChargeModel = _ariatCustomShippingCharge.Table.Where(x => x.ShippingCode != string.Empty).ToList();
            if (HelperUtility.IsNotNull(znodeAriatCustomShippingChargeModel) && znodeAriatCustomShippingChargeModel.Count() > 0)
            {
                HttpRuntime.Cache.Insert("DefaultShippingChargesCache", znodeAriatCustomShippingChargeModel);
            }
            return znodeAriatCustomShippingChargeModel;
        }

        // Get Get shipping rule list  for rate based on quantity.
        private List<ShippingRuleModel> GetRateBasedOnQuantityRuleList(ZnodeShoppingCart shoppingCart)
        {
            ZnodeShippingHelper shippingHelper = new ZnodeShippingHelper();
            return shippingHelper.GetShippingRuleList(shippingHelper.GetShippingRuleTypesEnumValue(Convert.ToString(ZnodeShippingRuleTypes.RateBasedOnQuantity)), string.IsNullOrEmpty(shoppingCart.Shipping.ShippingCountryCode) ? string.Empty : shoppingCart.Shipping.ShippingCountryCode, Convert.ToInt32(shoppingCart.PortalId), shoppingCart.Shipping.ShippingID);
        }

        // Calculate shipping rate for group product.
        private decimal CalculateProductShipping(ZnodeShippingBag shippingBag, ZnodeGenericCollection<ZnodeProductBaseEntity> productCollection, ZnodeShoppingCart shoppingCart, out decimal itemShippingCost)
        {
            itemShippingCost = 0;

            ShippingRuleListModel shippingRules = new ShippingRuleListModel();
            ZnodeShippingHelper shippingHelper = new ZnodeShippingHelper();

            shippingRules.ShippingRuleList = GetRateBasedOnQuantityRuleList(shoppingCart);

            foreach (ZnodeProductBaseEntity productItem in productCollection ?? new ZnodeGenericCollection<ZnodeProductBaseEntity>())
            {
                decimal productShippingCost = 0;
                decimal totalItemQuantity = 0;
                bool isRuleApplied = false;

                string shippingRuleTypeCode = Convert.ToString(productItem.ShippingRuleTypeCode);
                bool getsFreeShipping = productItem.FreeShippingInd;
                decimal quantity = productItem.SelectedQuantity;

                if (Equals(shippingRuleTypeCode?.ToLower(), shippingHelper.GetShippingRuleTypesEnumValue(Convert.ToString(ZnodeShippingRuleTypes.RateBasedOnQuantity)).ToLower()) && (!getsFreeShipping))
                    totalItemQuantity += quantity;

                if (totalItemQuantity > 0)
                {
                    isRuleApplied = ApplyRule(shippingRules.ShippingRuleList, totalItemQuantity, out productShippingCost);

                    if (isRuleApplied && productItem.ShipSeparately)
                        productItem.ShippingCost = shippingHelper.GetItemHandlingCharge(productShippingCost, shippingBag);

                    else if (isRuleApplied)
                        productItem.ShippingCost = productShippingCost;

                    itemShippingCost += productShippingCost;
                }
            }
            return itemShippingCost;
        }
        #endregion
    }

}

