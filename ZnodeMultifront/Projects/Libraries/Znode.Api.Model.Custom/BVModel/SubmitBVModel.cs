﻿namespace Znode.Engine.Api.Models
{
    public class SubmitBVModel
    {
        public int Rating { get; set; }

        public string ReviewTitle { get; set; }

        public string Review { get; set; }

        public string IsRecommedToFriend { get; set; }

        public string UserNickname { get; set; }

        public string UserLocation { get; set; }

        public string Email { get; set; }

        public string PhotoUrl_1 { get; set; }

        public string PhotoCaption_1 { get; set; }

        public string PhotoUrl_2 { get; set; }

        public string PhotoCaption_2 { get; set; }

        public string PhotoUrl_3 { get; set; }

        public string PhotoCaption_3 { get; set; }

        public string PhotoUrl_4 { get; set; }

        public string PhotoCaption_4 { get; set; }

        public string PhotoUrl_5 { get; set; }

        public string PhotoCaption_5 { get; set; }

        public string PhotoUrl_6 { get; set; }

        public string PhotoCaption_6 { get; set; }

        public string VideoUrl_1 { get; set; }

        public string VideoId { get; set; }

        public string VideoCaption_1 { get; set; }

        public string ProductSKU { get; set; }

        public string SubmissionTime { get; set; }

        public int ContentId { get; set; }

        public int TotalInappropriateFeedbackCount { get; set; }

        public int TotalNegativeFeedbackCount { get; set; }

        public int TotalPositiveFeedbackCount { get; set; }

    }

}

