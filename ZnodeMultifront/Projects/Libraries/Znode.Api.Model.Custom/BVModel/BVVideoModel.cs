﻿namespace Znode.Engine.Api.Models
{
    public class BVVideoModel
    {
        public string Video { get; set; }

        public string VideoName { get; set; }

        public string VideoURL { get; set; }

        public string VideoCaption { get; set; }
    }
}
