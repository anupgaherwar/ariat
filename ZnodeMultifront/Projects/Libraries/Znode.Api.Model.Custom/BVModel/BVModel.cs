﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
namespace Znode.Engine.Api.Models
{
    public class BVModel
    {
        [JsonProperty("Locale")]
        public string Locale { get; set; }

        [JsonProperty("Errors")]
        public object[] Errors { get; set; }

        [JsonProperty("Form")]
        public Form[] Form { get; set; }

        [JsonProperty("FormErrors")]
        public FormErrors FormErrors { get; set; }

        [JsonProperty("SubmissionId")]
        public object SubmissionId { get; set; }

        [JsonProperty("Data")]
        public Data Data { get; set; }

        [JsonProperty("TypicalHoursToPost")]
        public long TypicalHoursToPost { get; set; }

        [JsonProperty("Review")]
        public Review Review { get; set; }

        [JsonProperty("AuthorSubmissionToken")]
        public object AuthorSubmissionToken { get; set; }

        [JsonProperty("HasErrors")]
        public bool HasErrors { get; set; }


    }
    public class Data
    {
        [JsonProperty("Fields")]
        public Dictionary<string, Field> Fields { get; set; }

        [JsonProperty("Groups")]
        public Dictionary<string, Group> Groups { get; set; }

        [JsonProperty("FieldsOrder")]
        public string[] FieldsOrder { get; set; }

        [JsonProperty("GroupsOrder")]
        public string[] GroupsOrder { get; set; }
    }

    public class Field
    {
        [JsonProperty("Options")]
        public Option[] Options { get; set; }

        [JsonProperty("Type")]
        public FieldType Type { get; set; }

        [JsonProperty("Required")]
        public bool FieldRequired { get; set; }

        [JsonProperty("Label")]
        public string Label { get; set; }

        [JsonProperty("Value")]
        public bool? Value { get; set; }

        [JsonProperty("MinLength")]
        public long? MinLength { get; set; }

        [JsonProperty("Id")]
        public string Id { get; set; }

        [JsonProperty("MaxLength")]
        public long? MaxLength { get; set; }

        [JsonProperty("Default")]
        public object Default { get; set; }
    }
    public class Option
    {
        [JsonProperty("Label")]
        public string Label { get; set; }

        [JsonProperty("Value")]
        public string Value { get; set; }

        [JsonProperty("Selected")]
        public bool Selected { get; set; }
    }

    public class Group
    {
        [JsonProperty("Type")]
        public GroupType Type { get; set; }

        [JsonProperty("SubElements")]
        public Form[] SubElements { get; set; }

        [JsonProperty("Required")]
        public bool GroupRequired { get; set; }

        [JsonProperty("Label")]
        public object Label { get; set; }

        [JsonProperty("Id")]
        public string Id { get; set; }
    }

    public class Form
    {
        [JsonProperty("Type")]
        public FormType Type { get; set; }

        [JsonProperty("Id")]
        public string Id { get; set; }
    }

    public class FormErrors
    {
    }

    public class Review
    {
        [JsonProperty("SendEmailAlertWhenCommented")]
        public bool SendEmailAlertWhenCommented { get; set; }

        [JsonProperty("Rating")]
        public long Rating { get; set; }

        [JsonProperty("SubmissionTime")]
        public DateTimeOffset SubmissionTime { get; set; }

        [JsonProperty("ReviewText")]
        public object ReviewText { get; set; }

        [JsonProperty("SubmissionId")]
        public object SubmissionId { get; set; }

        [JsonProperty("Title")]
        public object Title { get; set; }

        [JsonProperty("IsRecommended")]
        public object IsRecommended { get; set; }

        [JsonProperty("TypicalHoursToPost")]
        public object TypicalHoursToPost { get; set; }

        [JsonProperty("Id")]
        public object Id { get; set; }

        [JsonProperty("SendEmailAlertWhenPublished")]
        public bool SendEmailAlertWhenPublished { get; set; }
    }
    public enum FieldType { BooleanInput, IntegerInput, SelectInput, TextAreaInput, TextInput };

    public enum FormType { Field, Group };

    public enum GroupType { InputGroup };
}

