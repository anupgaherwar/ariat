﻿using System.Collections.Generic;
namespace Znode.Engine.Api.Models
{
    public class BVRatingCount
    {
        public int TotalReviewCount { get; set; }

        public decimal AverageRating { get; set; }

        public string ProductSku { get; set; }

        public int AverageRatingPercentage { get; set; }

        public Dictionary<int, string> keyValuePairs = new Dictionary<int, string>();
    }
}
