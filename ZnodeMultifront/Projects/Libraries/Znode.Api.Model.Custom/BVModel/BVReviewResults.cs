﻿using System;
using System.Collections.Generic;
namespace Znode.Engine.Api.Models
{
    public class Result
    {
        public string Id { get; set; }

        public string CID { get; set; }

        public string SourceClient { get; set; }

        public DateTime LastModeratedTime { get; set; }

        public DateTime LastModificationTime { get; set; }

        public string ProductId { get; set; }

        public string AuthorId { get; set; }

        public string ContentLocale { get; set; }

        public bool IsFeatured { get; set; }

        public int TotalInappropriateFeedbackCount { get; set; }

        public int TotalClientResponseCount { get; set; }

        public int TotalCommentCount { get; set; }

        public int Rating { get; set; }

        public bool IsRatingsOnly { get; set; }

        public bool? IsRecommended { get; set; }

        public int TotalFeedbackCount { get; set; }

        public int TotalNegativeFeedbackCount { get; set; }

        public int TotalPositiveFeedbackCount { get; set; }

        public string ModerationStatus { get; set; }

        public string SubmissionId { get; set; }

        public DateTime SubmissionTime { get; set; }

        public string UserNickname { get; set; }

        public object Pros { get; set; }

        public int RatingRange { get; set; }

        public double? Helpfulness { get; set; }

        public List<Photo> Photos { get; set; }

        public string ReviewText { get; set; }

        public object Title { get; set; }

        public object UserLocation { get; set; }

        public List<Videos> Videos { get; set; }

        public string SubmissionTimeAsString { get; set; }
    }

    public class Videos
    {
        public string VideoId { get; set; }

        public string VideoHost { get; set; }

        public string VideoThumbnailUrl { get; set; }

        public string VideoIframeUrl { get; set; }

        public string Caption { get; set; }

        public string VideoUrl { get; set; }

        public string EmbededUrl { get; set; }
    }

    public class Thumbnail
    {
        public string Id { get; set; }

        public string Url { get; set; }
    }

    public class Sizes
    {
        public Thumbnail thumbnail { get; set; }
    }

    public class Photo
    {
        public Sizes Sizes { get; set; }

        public string Id { get; set; }

        public List<string> SizesOrder { get; set; }

        public object Caption { get; set; }
    }
}



