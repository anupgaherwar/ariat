﻿using System.Collections.Generic;
namespace Znode.Engine.Api.Models
{
    public class BVReviewListModel
    {
        public int Limit { get; set; }

        public int Offset { get; set; }

        public int TotalResults { get; set; }

        public string Locale { get; set; }

        public List<Result> Results { get; set; }

        public bool HasErrors { get; set; }

        public List<object> Errors { get; set; }
    }
}
