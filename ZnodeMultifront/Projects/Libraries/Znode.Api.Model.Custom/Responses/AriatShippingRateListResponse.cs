﻿using System.Collections.Generic;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Sample.Api.Model.Responses
{
    public class AriatShippingRateListResponse : BaseListResponse
    {
        public List<AriatShippingModel> ShippingRateList { get; set; }
    }
}
