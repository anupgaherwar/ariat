﻿using Znode.Engine.Api.Models.Responses;

namespace Znode.Sample.Api.Model.Responses
{
    public class AriatShippingRateResponse : BaseResponse
    {
        public AriatShippingModel Shipping { get; set; }
    }
}
