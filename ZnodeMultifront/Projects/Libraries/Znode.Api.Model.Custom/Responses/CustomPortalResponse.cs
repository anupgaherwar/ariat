﻿using Znode.Engine.Api.Models.Responses;

namespace Znode.Sample.Api.Model.Responses
{
    public class CustomPortalResponse : BaseResponse
    {
        public CustomPortalDetailModel PortalDetail { get; set; }
        public CustomPortalModel Portal { get; set; }
    }
}
