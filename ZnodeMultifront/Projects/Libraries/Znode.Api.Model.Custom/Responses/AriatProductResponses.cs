﻿using Znode.Engine.Api.Models.Responses;

namespace Znode.Custom.Api.Model.Responses
{
    public class AriatProductResponse : PublishProductResponse
    {
        public string ZnodeSku { get; set; }
    }
}
