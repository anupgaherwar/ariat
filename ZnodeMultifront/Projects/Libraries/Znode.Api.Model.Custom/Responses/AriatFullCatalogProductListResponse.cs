﻿using System.Collections.Generic;

using Znode.Engine.Api.Models.Responses;

namespace Znode.Custom.Api.Model.Responses
{
    public class AriatFullCatalogProductListResponse : BaseListResponse
    {
        public List<AriatProgramCatalogProductModel> CatalogProductList { get; set; }             
       

    }
}
