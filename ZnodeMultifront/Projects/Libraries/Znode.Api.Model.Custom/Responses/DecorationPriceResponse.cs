﻿using Znode.Custom.Api.Model;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Custom.Api.Model.Responses
{
    public class DecorationPriceResponse : BaseResponse
    {
        public DecorationPriceModel Price { get; set; }
    }
}
