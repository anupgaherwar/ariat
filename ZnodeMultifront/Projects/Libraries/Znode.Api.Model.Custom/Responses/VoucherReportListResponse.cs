﻿using System.Collections.Generic;

using Znode.Engine.Api.Models.Responses;

namespace Znode.Custom.Api.Model.Responses
{
    public class VoucherReportListResponse : BaseListResponse
    {
        public List<AriatVoucherReportModel> VoucherList { get; set; }
    }
}
