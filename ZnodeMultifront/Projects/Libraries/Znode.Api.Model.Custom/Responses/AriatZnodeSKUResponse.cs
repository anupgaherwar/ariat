﻿using Znode.Engine.Api.Models.Responses;

namespace Znode.Custom.Api.Model.Responses
{
    public class AriatZnodeSKUResponse : BaseResponse
    {
        public ZnodeSKURequestModel ProductData { get; set; }
    }
}
