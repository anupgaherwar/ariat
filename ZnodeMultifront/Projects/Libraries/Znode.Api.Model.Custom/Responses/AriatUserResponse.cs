﻿using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Custom.Api.Model.Responses
{
    public class AriatUserResponse : BaseResponse
    {
        public AddressModel ShippingAddress { get; set; }
    }
}
