﻿using Znode.Engine.Api.Models.Responses;

namespace Znode.Sample.Api.Model.Responses
{
    public class GenerateOrderResponse : BaseResponse
    {
        public string OrderNumber { get; set; }
    }
}
