﻿using System.Collections.Generic;

using Znode.Engine.Api.Models.Responses;

namespace Znode.Custom.Api.Model.Responses
{
    public class AriatProgramCatalogProductListResponse : BaseListResponse
    {
        public List<AriatProgramCatalogProductModel> ProductList { get; set; }
    }
}
