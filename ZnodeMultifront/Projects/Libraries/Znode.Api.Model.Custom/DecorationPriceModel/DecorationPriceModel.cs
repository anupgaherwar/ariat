﻿
using Znode.Engine.Api.Models;

namespace Znode.Custom.Api.Model
{
    public class DecorationPriceModel : BaseModel
    {
        public decimal TotalPrice { get; set; }

        public decimal UnitProductPrice { get; set; }
        public decimal UnitRegularPrice { get; set; }
        public decimal UnitLargeLogoPrice { get; set; }
        public decimal UnitTextPrice { get; set; }
        public string Name { get; set; }
    }
}
