﻿namespace Znode.Sample.Api.Model
{
    public class AriatOrderLineItemsDetailModel
    {
        public string InvoiceDate { get; set; }

        public string BackorderDate { get; set; }

        public string InvoiceNumber { get; set; }

        public string ShipDate { get; set; }

        public string MSRP { get; set; }

        public string ExpectedShipDate { get; set; }
    }
}
