﻿using System.Collections.Generic;

using Znode.Engine.Api.Models;

namespace Znode.Custom.Api.Model
{
    public class AriatVoucherReportListModel : BaseListModel
    {
        public List<AriatVoucherReportModel> VoucherList { get; set; }
    }
}
