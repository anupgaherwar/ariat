﻿using System.Collections.Generic;

namespace Znode.Sample.Api.Model
{
    public class ArtifiPersonalizeModel
    {
        public List<Text> text { get; set; }
        public List<Image> image { get; set; }
        public int viewId { get; set; }
        public string viewCode { get; set; }
    }
    public class Text
    {
        public string widgetKey { get; set; }
        public string text { get; set; }
        public string colorCode { get; set; }
        public string colorName { get; set; }
        public string color { get; set; }
        public string noOfLines { get; set; }
    }

    public class Image
    {
        public string widgetKey { get; set; }
        public string src { get; set; }
        public string clipArtCode { get; set; }
        public string customClipartId { get; set; }
        public string imageName { get; set; }
        public string colorCode { get; set; }
        public string color { get; set; }
    }


}
