﻿namespace Znode.Custom.Api.Model.ParameterModel
{
    public class PriceParameterModel
    {
        public string SKU { get; set; }
        public int Quantity { get; set; }
        public string LogoSKU { get; set; }
        public int LogoQuantity { get; set; }
        public string LargeLogoSKU { get; set; }
        public int LargeLogoQuantity { get; set; }
        public string TextLineSKU { get; set; }
        public int TextLineQuantity { get; set; }
        public int PortalId { get; set; }
        public int ProfileId { get; set; }

        public int PublishCatalogId { get; set; }
        public int LocaleId { get; set; }
    }
}
