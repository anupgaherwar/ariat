﻿using System.Collections.Generic;

using Znode.Engine.Api.Models;

namespace Znode.Custom.Api.Model
{
    public class ZnodeSKURequestModel : BaseModel
    {
        public string SKU { get; set; }
        public string ArtifiSKU { get; set; }
        public string StyleNumber { get; set; }
        public int PublishCatalogId { get; set; }
        public int PortalId { get; set; }
        public int LocaleId { get; set; }

        public Dictionary<string, string> Attributes { get; set; }
    }
}
