﻿using System.Collections.Generic;
using Znode.Engine.Api.Models;

namespace Znode.Sample.Api.Model
{
    public class AriatShippingListModel : BaseListModel
    {
        public List<AriatShippingModel> ShippingList { get; set; }
    }
}
