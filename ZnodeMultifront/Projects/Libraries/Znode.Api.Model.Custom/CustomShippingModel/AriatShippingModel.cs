﻿using Znode.Engine.Api.Models;

namespace Znode.Sample.Api.Model
{
    public class AriatShippingModel:BaseModel
    {
        public int AriatCustomShippingChargesId { get; set; }
        public string ShippingCode { get; set; }
        public decimal? LowerLimit { get; set; }
        public decimal? UpperLimit { get; set; }
        public decimal? ShippingAmount { get; set; }
    }
}
