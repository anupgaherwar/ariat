﻿using Znode.Engine.Api.Models;

namespace Znode.Custom.Api.Model
{
    public class AdyenDropinRequestModel : BaseModel
    {
        public string PaymentCode { get; set; }
        public bool IsTestMode { get; set; }
        public string RequestDomain { get; set; }
    }
}
