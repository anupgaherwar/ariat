﻿
using Znode.Engine.Api.Models;

namespace Znode.Custom.Api.Model
{
    public class AriatAdyenDropInModel : BaseModel
    {
        public bool IsSuccess { get; set; }
        public string PaymentMethods { get; set; }
        public string Environment { get; set; }
        public string OriginKey { get; set; }
    }
}
