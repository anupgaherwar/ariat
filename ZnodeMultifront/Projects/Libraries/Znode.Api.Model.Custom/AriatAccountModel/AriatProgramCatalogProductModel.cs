﻿
using Znode.Engine.Api.Models;

namespace Znode.Custom.Api.Model
{
    public class AriatProgramCatalogProductModel : BaseModel
    {
        public int AriatProgramCatalogProductId { get; set; }
        public string ProductName { get; set; }
        public string StyleNumber { get; set; }
        public string ColorSpecific { get; set; }
        public string ProductFamily { get; set; }
        public string SAPCategory { get; set; }
        public string SAPProductGroup { get; set; }
        public string SAPSubGroup { get; set; }
        public string Gender { get; set; }
        public string IsCustomizable { get; set; }
        public string Waterproof { get; set; }
        public string ExtendedSizes { get; set; }
        public string SafetyToe { get; set; }
        public string SafetySpecification { get; set; }
        public string ToeShape { get; set; }
        public string Technology { get; set; }
        public string AccountExternalID { get; set; }
    }
}
