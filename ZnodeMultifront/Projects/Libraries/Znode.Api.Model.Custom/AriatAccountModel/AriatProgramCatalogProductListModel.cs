﻿
using System.Collections.Generic;

using Znode.Engine.Api.Models;

namespace Znode.Custom.Api.Model
{
    public class AriatProgramCatalogProductListModel : BaseListModel
    {
        public List<AriatProgramCatalogProductModel> ProductList { get; set; }
    }
}
