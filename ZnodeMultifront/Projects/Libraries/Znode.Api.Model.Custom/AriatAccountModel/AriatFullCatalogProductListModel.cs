﻿using System.Collections.Generic;

using Znode.Engine.Api.Models;

namespace Znode.Custom.Api.Model
{
    public class AriatFullCatalogProductListModel : BaseListModel
    {
        public List<AriatProgramCatalogProductModel> CatalogProductsDetailList { get; set; }      
        
    }
}
