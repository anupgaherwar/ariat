﻿CREATE TYPE [dbo].[SKUPriceForQuote] AS TABLE (
    [SKU]                    VARCHAR (600)   NULL,
    [OmsSavedCartLineItemId] INT             NULL,
    [Price]                  NUMERIC (28, 6) NULL,
    [ShippingCost]           NUMERIC (28, 6) NULL);

