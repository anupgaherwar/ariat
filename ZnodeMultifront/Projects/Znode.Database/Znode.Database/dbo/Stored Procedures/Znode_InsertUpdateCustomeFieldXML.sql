﻿----DROP TABLE ZnodePimCustomeFieldXML

--CREATE TABLE ZnodePimCustomeFieldXML 
--(
--PimCustomeFieldXMLId INT IDENTITY(1,1) CONSTRAINT PK_ZnodePimCustomeFieldXML  PRIMARY KEY 
--,PimProductId INT 
--,CustomCode VARCHAR(300)
--,CustomeFiledXML NVARCHAR(max) 
--,LocaleId     INT 
--,CreatedBy    INT NOT NULL 
--,CreatedDate  DATETIME NOT NULL 
--,ModifiedBy   INT NOT NULL 
--,ModifiedDate DATETIME NOT NULL 
--)

-- EXEC [Znode_InsertUpdateCustomeFieldXML] 1

CREATE  Procedure [dbo].[Znode_InsertUpdateCustomeFieldXML] 
(
 @PimProductId VARCHAR(2000)
)
AS
BEGIN 
  BEGIN TRY 
    
	;With Cte_CustomeAttributeValue AS
	(

			SELECT PimProductId ,ZPCF.CustomCode,'<AttributeCode>'+ISNULL((SELECT ''+ZPCF.CustomCode FOR XML PATH('')),'') +'</AttributeCode>'+'<AttributeName>'+ISNULL((SELECT ''+ZPCFL.CustomKey FOR XML PATH('')),'')+'</AttributeName>'
			+'<AttributeValues>'+ISNULL((SELECT ''+ZPCFL.CustomKeyValue FOR XML PATH('')),'')+'</AttributeValues>'+'<IsUseInSearch>0</IsUseInSearch>
			<IsHtmlTags>0</IsHtmlTags>
			<IsComparable>0</IsComparable>
			<IsFacets>0</IsFacets>
			<AttributeTypeName>Text Area</AttributeTypeName>
			<IsPersonalizable>0</IsPersonalizable>
			<IsCustomeField>1</IsCustomeField>
			<IsConfigurable>0</IsConfigurable>
			<IsSwatch>0</IsSwatch>
			<DisplayOrder>'+Convert(nvarchar(100),Isnull(DisplayOrder,0))+'</DisplayOrder>
			' AttributeValue,ZPCFL.LocaleId 
			FROM ZnodePimCustomField ZPCF
			INNER JOIN ZnodePimCustomFieldLocale ZPCFL ON (ZPCFL.PimCustomFieldId = ZPCF.PimCustomFieldId) 
	)

  MERGE INTO ZnodePimCustomeFieldXML TARGET
  USING Cte_CustomeAttributeValue SOURCE 
  ON (TARGET.PimProductId = SOURCE.PimProductId
    AND  TARGET.LocaleId = SOURCE.LocaleId
	AND TARGET.CustomCode = SOURCE.CustomCode
  )
  WHEN MATCHED THEN 
  UPDATE 
   SET TARGET.CustomeFiledXML = SOURCE.AttributeValue
       ,TARGET.ModifiedBy      = 2 
	   ,TARGET.ModifiedDAte   = GETDATE()

  WHEN NOT MATCHED THEN 
  INSERT (PimProductId
				,CustomCode
				,CustomeFiledXML
				,LocaleId
				,CreatedBy
				,CreatedDate
				,ModifiedBy
				,ModifiedDate)
				  VALUES (SOURCE.PimProductId
				  ,SOURCE.CustomCode
				,Source.AttributeValue
				,SOURCE.LocaleId
				,2
				,GETDATE()
				,2
				,GETDATE())
				WHEN NOT MATCHED BY SOURCE THEN 
	DELETE;

  END TRY 
  BEGIN CATCH 
  SELECT ERROR_MESSAGE()
  END CATCH 
END