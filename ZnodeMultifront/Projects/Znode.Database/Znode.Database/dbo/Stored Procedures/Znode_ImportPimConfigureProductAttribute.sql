﻿

CREATE PROCEDURE [dbo].[Znode_ImportPimConfigureProductAttribute]
	-- Add the parameters for the stored procedure here
	
	@ImportProcessLogId INT,
    @UserId             INT
AS
BEGIN
	SET NOCOUNT ON
	Begin Try

		DECLARE @GetDate DATETIME = dbo.Fn_GetDate();
		if object_id('tempdb..#main') is not null
						drop table #main
						
					

		if object_id('tempdb..##config_import') is not null
		select PimProductId,SAPCategory,	SAPProductGroup,	SAPSubGroup into #main from (
		SELECT a.PimProductId ,b.AttributeValue AttributeValue  ,c.AttributeCode 
		FROM ZnodePimAttributeValue a 
		INNER JOIN  ZnodePimAttributeValueLocale b ON ( b.PimAttributeValueId = a.PimAttributeValueId )
		INNER JOIN ZnodePimAttribute c ON ( c.PimAttributeId=a.PimAttributeId )
		inner join ##config_import CI on CI.[PimProductId] =PimProductId
		where c.AttributeCode in ('SAPCategory', 'SAPProductGroup','SAPSubGroup')
		)abc
		PIVOT  
		(  
		max(AttributeValue)
		FOR AttributeCode IN (SAPCategory, SAPProductGroup,SAPSubGroup)  
		) AS PivotTable;  
	

		INSERT INTO [dbo].[ZnodePimConfigureProductAttribute]
           ([PimProductId] ,[PimFamilyId] ,[PimAttributeId] ,[CreatedBy] ,[CreatedDate] ,[ModifiedBy] ,[ModifiedDate])
			select M.PimProductId,ZPAF.PimAttributeFamilyId PimFamilyId,	ZA.PimAttributeId ,@UserId,@GetDate,@UserId,@GetDate
				from #main m inner join ZnodePimAttributeFamily ZPAF on zpaf.FamilyCode = M.SAPCategory
				inner join ZnodePimAttribute ZA ON ZA.AttributeCode IN ('Color', 'WidthSize', 'Size')
				where m.SAPCategory = 'Accessories' and ZA.IsConfigurable =1
					and NOT EXISTS (select top 1 1 from ZnodePimConfigureProductAttribute ZPCPA where ZPCPA.PimProductId =M.PimProductId
						and ZPCPA.PimFamilyId = ZPAF.PimAttributeFamilyId and ZPCPA.PimAttributeId = ZA.PimAttributeId)

			
		INSERT INTO [dbo].[ZnodePimConfigureProductAttribute]
           ([PimProductId] ,[PimFamilyId] ,[PimAttributeId] ,[CreatedBy] ,[CreatedDate] ,[ModifiedBy] ,[ModifiedDate])
			select M.PimProductId,ZPAF.PimAttributeFamilyId PimFamilyId,	ZA.PimAttributeId ,@UserId,@GetDate,@UserId,@GetDate
				from #main m inner join ZnodePimAttributeFamily ZPAF on zpaf.FamilyCode = M.SAPCategory
				inner join ZnodePimAttribute ZA ON ZA.AttributeCode IN ('Color', 'WidthSize', 'Size')
				where m.SAPCategory = 'Footwear' and ZA.IsConfigurable =1
					and NOT EXISTS (select top 1 1 from ZnodePimConfigureProductAttribute ZPCPA where ZPCPA.PimProductId =M.PimProductId
						and ZPCPA.PimFamilyId = ZPAF.PimAttributeFamilyId and ZPCPA.PimAttributeId = ZA.PimAttributeId)

			
		INSERT INTO [dbo].[ZnodePimConfigureProductAttribute]
           ([PimProductId] ,[PimFamilyId] ,[PimAttributeId] ,[CreatedBy] ,[CreatedDate] ,[ModifiedBy] ,[ModifiedDate])
			select M.PimProductId,ZPAF.PimAttributeFamilyId PimFamilyId,	ZA.PimAttributeId ,@UserId,@GetDate,@UserId,@GetDate
				from #main m inner join ZnodePimAttributeFamily ZPAF on zpaf.FamilyCode = M.SAPCategory
				inner join ZnodePimAttribute ZA ON ZA.AttributeCode IN ('Color')
				where m.SAPCategory = 'Apparel'  and M.SAPProductGroup ='WORK' and m.SAPSubGroup in ('Caps','Outerwear') and ZA.IsConfigurable =1
					and NOT EXISTS (select top 1 1 from ZnodePimConfigureProductAttribute ZPCPA where ZPCPA.PimProductId =M.PimProductId
						and ZPCPA.PimFamilyId = ZPAF.PimAttributeFamilyId and ZPCPA.PimAttributeId = ZA.PimAttributeId)


			
		INSERT INTO [dbo].[ZnodePimConfigureProductAttribute]
           ([PimProductId] ,[PimFamilyId] ,[PimAttributeId] ,[CreatedBy] ,[CreatedDate] ,[ModifiedBy] ,[ModifiedDate])
			select M.PimProductId,ZPAF.PimAttributeFamilyId PimFamilyId,	ZA.PimAttributeId ,@UserId,@GetDate,@UserId,@GetDate
				from #main m inner join ZnodePimAttributeFamily ZPAF on zpaf.FamilyCode = M.SAPCategory
				inner join ZnodePimAttribute ZA ON ZA.AttributeCode IN ('Color', 'LengthSize', 'Waist')
				where m.SAPCategory = 'Apparel'  and M.SAPProductGroup ='Western' and m.SAPSubGroup ='DENIM' and ZA.IsConfigurable =1
					and NOT EXISTS (select top 1 1 from ZnodePimConfigureProductAttribute ZPCPA where ZPCPA.PimProductId =M.PimProductId
						and ZPCPA.PimFamilyId = ZPAF.PimAttributeFamilyId and ZPCPA.PimAttributeId = ZA.PimAttributeId)

			
		INSERT INTO [dbo].[ZnodePimConfigureProductAttribute]
           ([PimProductId] ,[PimFamilyId] ,[PimAttributeId] ,[CreatedBy] ,[CreatedDate] ,[ModifiedBy] ,[ModifiedDate])
			select M.PimProductId,ZPAF.PimAttributeFamilyId PimFamilyId,	ZA.PimAttributeId ,@UserId,@GetDate,@UserId,@GetDate
				from #main m inner join ZnodePimAttributeFamily ZPAF on zpaf.FamilyCode = M.SAPCategory
				inner join ZnodePimAttribute ZA ON ZA.AttributeCode IN ('Color', 'LengthSize', 'Waist')
				where m.SAPCategory = 'Apparel'  and M.SAPProductGroup ='WORK' and m.SAPSubGroup in ('Bottoms','Denim','FR Bottoms','FR Denim')
					and ZA.IsConfigurable =1 and NOT EXISTS (select top 1 1 from ZnodePimConfigureProductAttribute ZPCPA where ZPCPA.PimProductId =M.PimProductId
						and ZPCPA.PimFamilyId = ZPAF.PimAttributeFamilyId and ZPCPA.PimAttributeId = ZA.PimAttributeId)

			
		INSERT INTO [dbo].[ZnodePimConfigureProductAttribute]
           ([PimProductId] ,[PimFamilyId] ,[PimAttributeId] ,[CreatedBy] ,[CreatedDate] ,[ModifiedBy] ,[ModifiedDate])
			select M.PimProductId,ZPAF.PimAttributeFamilyId PimFamilyId,	ZA.PimAttributeId ,@UserId,@GetDate,@UserId,@GetDate
				from #main m inner join ZnodePimAttributeFamily ZPAF on zpaf.FamilyCode = M.SAPCategory
				inner join ZnodePimAttribute ZA ON ZA.AttributeCode IN ('Color', 'LengthSize', 'Size')
				where m.SAPCategory = 'Apparel'  and M.SAPProductGroup ='English' and m.SAPSubGroup in ('Breeches','Show Collection')
					and ZA.IsConfigurable =1 and NOT EXISTS (select top 1 1 from ZnodePimConfigureProductAttribute ZPCPA where ZPCPA.PimProductId =M.PimProductId
						and ZPCPA.PimFamilyId = ZPAF.PimAttributeFamilyId and ZPCPA.PimAttributeId = ZA.PimAttributeId)

			
		INSERT INTO [dbo].[ZnodePimConfigureProductAttribute]
           ([PimProductId] ,[PimFamilyId] ,[PimAttributeId] ,[CreatedBy] ,[CreatedDate] ,[ModifiedBy] ,[ModifiedDate])
			select M.PimProductId,ZPAF.PimAttributeFamilyId PimFamilyId,	ZA.PimAttributeId ,@UserId,@GetDate,@UserId,@GetDate
				from #main m inner join ZnodePimAttributeFamily ZPAF on zpaf.FamilyCode = 'Default'
				inner join ZnodePimAttribute ZA ON ZA.AttributeCode IN ('Color', 'Size' )
				where ZA.IsConfigurable =1
					and NOT EXISTS (select top 1 1 from ZnodePimConfigureProductAttribute ZPCPA where ZPCPA.PimProductId =M.PimProductId)

	

	END TRY 
	BEGIN CATCH
		 SELECT ERROR_MESSAGE(),ERROR_LINE(),ERROR_PROCEDURE();
            -- UPDATE ZnodeImportProcessLog SET Status = dbo.Fn_GetImportStatus(3), ProcessCompletedDate = @GetDate WHERE ImportProcessLogId = @ImportProcessLogId;
            -- ROLLBACK TRAN ImportProducts;
	END CATCH;

END