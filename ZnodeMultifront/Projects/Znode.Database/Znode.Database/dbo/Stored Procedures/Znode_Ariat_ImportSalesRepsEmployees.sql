﻿


CREATE  PROCEDURE [dbo].[Znode_Ariat_ImportSalesRepsEmployees](
	  @TableName nvarchar(200), @Status bit OUT, @UserId int, @ImportProcessLogId int, @NewGUId nvarchar(200), @LocaleId int= 0,@PortalId int ,@CsvColumnString nvarchar(max))
AS
	--------------------------------------------------------------------------------------
	-- Summary :  Import to Create user and assign the customer to account 
	
	-- Unit Testing : 
	--------------------------------------------------------------------------------------

BEGIN
	BEGIN TRAN A;
	BEGIN TRY
	 
	 
		DECLARE @MessageDisplay nvarchar(100), @SSQL nvarchar(max),@AspNetZnodeUserId nvarchar(256),@ASPNetUsersId nvarchar(256),
		@PasswordHash nvarchar(max),@SecurityStamp nvarchar(max),@RoleId nvarchar(256),@IsAllowGlobalLevelUserCreation nvarchar(10)
		Declare @ProfileId  int
		DECLARE @FailedRecordCount BIGINT
		DECLARE @SuccessRecordCount BIGINT
		 
		SET @SecurityStamp = '0wVYOZNK4g4kKz9wNs-UHw2'
		SET @PasswordHash = 'APy4Tm1KbRG6oy7h3r85UDh/lCW4JeOi2O2Mfsb3OjkpWTp1YfucMAvvcmUqNaSOlA==';
		SELECT  @RoleId  = Id from AspNetRoles where   NAME = 'Sales Rep'  

		Select @IsAllowGlobalLevelUserCreation = FeatureValues from ZnodeGlobalsetting where FeatureName = 'AllowGlobalLevelUserCreation'

		DECLARE @GetDate datetime= dbo.Fn_GetDate();
		
		
		IF OBJECT_ID(N'tempdb.##insertEmployees') IS NOT NULL
			BEGIN
				DROP TABLE ##insertEmployees
			END
		SET @SSQL = 'Select  * into ##InsertEmployees from ' + @TableName +''; 
		-- select @CsvColumnString
		-- select * from #InsertCustomer
		print @SSQL;
		
		EXEC sys.sp_sqlexec @SSQL;
		

		

-------------------------- Validation of data

			--Declare @FailedRecordCount int =0, @SuccessRecordCount int = 0;
				INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
				SELECT '62', 'Role Unavailable', ZIC.Role, @NewGUId,  RowNumber,  @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
					from ##insertEmployees ZIC where not exists (select top 1 1 from AspNetRoles ZR where ZR.Name = ZIC.Role);
				
			
				INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
					   SELECT '10', 'PortalId', ZIC.PortalId , @NewGUId,   RowNumber , @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
					   FROM ##insertEmployees ZIC where not exists (select top 1 1 from ZnodePortal ZP where ZP.StoreCode = ZIC.PortalId);

					  --- User validation
				--If @IsAllowGlobalLevelUserCreation = 'false'
				--	INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
				--		   SELECT '10', 'UserName', Name, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
				--		   FROM ##insertEmployees AS ii
				--			WHERE ltrim(rtrim(ii.Name)) in 
				--		   (
				--			   SELECT UserName FROM AspNetZnodeUser   where PortalId = @PortalId
				--		   );
				--Else 
				--	INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
				--		   SELECT '10', 'UserName', Name, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
				--		   FROM ##insertEmployees AS ii
				--		   WHERE ltrim(rtrim(ii.Name)) in 
				--		   (
				--			   SELECT UserName FROM AspNetZnodeUser   
				--		   );
		
				INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
					   SELECT '35', 'UserName', Name, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
					   FROM ##insertEmployees AS ii
					   WHERE ii.Name not like '%_@_%_.__%' 
				
				INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
					   SELECT '30', 'UserName', Name, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
					   FROM ##insertEmployees AS ii
					   WHERE ltrim(rtrim(ii.Name)) in 
					   (SELECT ltrim(rtrim(Name))  FROM ##insertEmployees group by ltrim(rtrim(Name))  having count(*) > 1 )

				 UPDATE ZIL
					   SET ZIL.ColumnName =   ZIL.ColumnName + ' [ Name - ' + ISNULL(Name,'') + ' ] '
					   FROM ZnodeImportLog ZIL 
					   INNER JOIN ##insertEmployees IPA ON (ZIL.RowNumber = IPA.RowNumber)
					   WHERE  ZIL.ImportProcessLogId = @ImportProcessLogId AND ZIL.RowNumber IS NOT NULL



------------- End of Validation 	
------------------------------------------------------------------------
------------ Delete Invalid Data after validation  
				DELETE FROM ##insertEmployees
					WHERE RowNumber IN
					(
						SELECT DISTINCT 
							   RowNumber
						FROM ZnodeImportLog
						WHERE ImportProcessLogId = @ImportProcessLogId  and RowNumber is not null 
						--AND GUID = @NewGUID
					);		
-- Update Record count in log 
        
		SELECT @FailedRecordCount = COUNT(DISTINCT RowNumber) FROM ZnodeImportLog WHERE RowNumber IS NOT NULL AND  ImportProcessLogId = @ImportProcessLogId;
		Select @SuccessRecordCount = count(DISTINCT RowNumber) FROM ##insertEmployees
		UPDATE ZnodeImportProcessLog SET FailedRecordcount = @FailedRecordCount , SuccessRecordCount = @SuccessRecordCount , 
		TotalProcessedRecords = (ISNULL(@FailedRecordCount,0) + ISNULL(@SuccessRecordCount,0))
		WHERE ImportProcessLogId = @ImportProcessLogId;
		-- End

		-- Insert Product Data 
				
				
				DECLARE @InsertedAspNetZnodeUser TABLE (AspNetZnodeUserId nvarchar(256) ,UserName nvarchar(512),PortalId int )
				DECLARE @InsertedASPNetUsers TABLE (Id nvarchar(256) ,UserName nvarchar(512))
				DECLARE @InsertZnodeUser TABLE (UserId int,AspNetUserId nvarchar(256),CreatedDate Datetime, Email varchar(256) )

				UPDATE ANU SET 
				ANU.PhoneNumber	= IC.PhoneNumber
				from AspNetZnodeUser ANZU INNER JOIN ASPNetUsers ANU ON ANZU.AspNetZnodeUserId = ANU.UserName 
				INNER JOIN ZnodeUser ZU ON ANU.ID = ZU.AspNetUserId	
				INNER JOIN ##insertEmployees IC ON ANZU.UserName = IC.Name 
				where Isnull(ANZU.PortalId,0) = Isnull(@PortalId ,0)
				

				--select * from AspNetZnodeUser
				 /*CustomerAccount Code:Start*/
				  
				UPDATE ZU SET 
				--ZU.Custom1		= IC.AccountStoreId, /*CustomerAccount customization to put AccountStoreId in custom1 field*/
				--ZU.Custom2		= IC.SalesRepId, /*CustomerAccount customization to put AccountStoreId in custom1 field*/
				ZU.FirstName	= IC.FirstName,
				ZU.LastName		= IC.LastName,
				--ZU.MiddleName	= IC.MiddleName,
				--ZU.BudgetAmount = IC.BudgetAmount,
				--ZU.Email		= IC.Email,
				ZU.PhoneNumber	= IC.PhoneNumber,
				--ZU.EmailOptIn	= Isnull(IC.EmailOptIn,0),
				ZU.IsActive		= IC.IsActive,
				ZU.ExternalId = IC.ExternalId
				from AspNetZnodeUser ANZU INNER JOIN ASPNetUsers ANU ON ANZU.AspNetZnodeUserId = ANU.UserName 
				INNER JOIN ZnodeUser ZU ON ANU.ID = ZU.AspNetUserId	
				INNER JOIN ##insertEmployees IC ON ANZU.UserName = IC.Name 
				--where Isnull(ANZU.PortalId,0) = Isnull(@PortalId ,0)
				 /*CustomerAccount Code:End*/

			--select * from ZnodePortal
	
				Insert into AspNetZnodeUser (AspNetZnodeUserId, UserName, PortalId)		
				OUTPUT INSERTED.AspNetZnodeUserId, INSERTED.UserName, INSERTED.PortalId	INTO  @InsertedAspNetZnodeUser 			 
				Select NEWID(),IC.Name, ZP.PortalId FROM ##insertEmployees IC 
				inner join ZnodePortal ZP on ZP.StoreCode = IC.PortalId
				where Not Exists (Select TOP 1 1  from AspNetZnodeUser ANZ where Isnull(ANZ.PortalId,0) = Isnull(ZP.PortalId ,0) AND ANZ.UserName = IC.Name)

				INSERT INTO ASPNetUsers (Id,Email,EmailConfirmed,PasswordHash,SecurityStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,
				LockoutEndDateUtc,LockOutEnabled,AccessFailedCount,PasswordChangedDate,UserName)
				output inserted.Id, inserted.UserName into @InsertedASPNetUsers
				SELECT NewId(), A.Name,0 ,@PasswordHash,@SecurityStamp,PhoneNumber,0,0,case when A.isactive ='0' or A.isactive ='False' then '9999-12-31 23:59:59.997' else null end  LockoutEndDateUtc,1 LockoutEnabled,
				0,@GetDate,AspNetZnodeUserId from ##insertEmployees A INNER JOIN @InsertedAspNetZnodeUser  B 
				ON A.Name = B.UserName
				where not exists (select top 1 1 from ASPNetUsers ANU where ANU.Email = a.name) 
				
				

				INSERT INTO  ZnodeUser(AspNetUserId,FirstName,LastName,CustomerPaymentGUID,Email,PhoneNumber,EmailOptIn,
				IsActive,ExternalId, CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
				OUTPUT Inserted.UserId, Inserted.AspNetUserId,Inserted.CreatedDate, Inserted.Email into @InsertZnodeUser
				SELECT IANU.Id AspNetUserId ,IC.FirstName,IC.LastName,null CustomerPaymentGUID, IC.Name
				,IC.PhoneNumber,Isnull(null,0),IC.IsActive,IC.ExternalId, @UserId,
				  @Getdate ,@UserId,@Getdate
				from ##insertEmployees IC Inner join 
				@InsertedAspNetZnodeUser IANZU ON IC.Name = IANZU.UserName  INNER JOIN 
				ASPNetUsers IANU ON IANZU.UserName = IANU.Email 
				  where Not Exists (select top 1 1 from ZnodeUser ZU where zu.Email =IANU.email)

				

				
				INSERT INTO AspNetUserRoles (UserId,RoleId)  
					Select AspNetUserId, AR.Id from @InsertZnodeUser  IZU
					inner join ##insertEmployees IC on ic.name= IZU.Email 
					inner join AspNetRoles AR on AR.Name = IC.Role
				    --select * from AspNetUserRoles 
				INSERT INTO ZnodeUserPortal (UserId,PortalId,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate) 
				SELECT IZU.UserId, ANZU.PortalId , @UserId, IZU.CreatedDate,@UserId,@Getdate 
				from @InsertZnodeUser IZU inner join ZnodeUser ZU  on ZU.userid = IZU.userid
				inner join AspNetZnodeUser ANZU on ZU.Email = ANZU.username
				--where Not Exists (select top 1 1 from ZnodeUserPortal ZUP where  IZU.UserId = ZUP.UserId)
			
				UPDATE ZU SET 
				ZU.AccountId		= ac.AccountId  /*CustomerAccount customization to put AccountStoreId in custom1 field*/
				from AspNetZnodeUser ANZU INNER JOIN ASPNetUsers ANU ON ANZU.AspNetZnodeUserId = ANU.UserName 
				INNER JOIN ZnodeUser ZU ON ANU.ID = ZU.AspNetUserId	
				INNER JOIN ##insertEmployees IC ON ANZU.UserName = IC.Name 
				INNER JOIN ZnodeAccount ac on ac.ExternalId=IC.ExternalId
				where Isnull(ANZU.PortalId,0) = Isnull(@PortalId ,0)


				update ZnodeAddress
				set  [ExternalId] = IA.ExternalId ,
				[FirstName] = IA.FirstName, 
				[LastName] = IA.LastName, 
				[CompanyName] = IA.CompanyName,
				[Address1] = IA.Address1,
				[Address2] = IA.Address2,
				[Address3] = IA.Address3,
				[CountryName]  = IA.CountryName,
				[StateName] = IA.StateName,
				[CityName] = IA.CityName,
				[PostalCode] = IA.PostalCode,
				[PhoneNumber] = IA.PhoneNumber,
				[FaxNumber] = IA.FaxNumber,
				[IsActive] = 1
				from ##insertEmployees IA
				where ZnodeAddress.[DisplayName] = IA.DisplayName  

			
-------------------------------Inserting Data in the table.
					INSERT INTO [dbo].[ZnodeAddress]([FirstName],[LastName],[DisplayName],[CompanyName],[Address1],[Address2]
						,[Address3],[CountryName],[StateName],[CityName],[PostalCode],[PhoneNumber],[FaxNumber],[IsDefaultBilling],[IsDefaultShipping]
						,[IsActive],[ExternalId],[CreatedBy],[CreatedDate],[ModifiedBy],[ModifiedDate],[IsShipping],[IsBilling],Custom1 )
					SELECT DISTINCT FirstName,LastName,DisplayName,CompanyName,Address1,Address2,Address3,CountryName,StateName,CityName,
						PostalCode,PhoneNumber,FaxNumber,1,1,1,ExternalId,@UserId,@GetDate,@UserId,@GetDate,1,1,null
						FROM ##insertEmployees IA
						WHERE NOT EXISTS (SELECT TOP 1  1  FROM ZnodeAddress ZA WHERE ZA.DisplayName = IA.DisplayName AND ZA.Address1 = IA.Address1 AND ZA.ExternalId = ia.ExternalId);

					INSERT INTO [dbo].[ZnodeUserAddress] ([UserId] ,[AddressId] ,[CreatedBy],[CreatedDate] ,[ModifiedBy] ,[ModifiedDate])
					Select UserId, AddressId,@UserId,@GetDate,@UserId,@GetDate
						from ##insertEmployees IE Inner Join ZnodeUser ZU on ZU.FirstName = IE.FirstName and ZU.LastName = IE.LastName
						inner join Znodeaddress ZA on  ZA.FirstName = IE.FirstName and ZA.LastName = IE.LastName and ZA.Displayname = IE.DisplayName
						where NOT EXISTS (SELECT TOP 1 1 FROM  ZnodeUserAddress ZUA WHERE ZUA.AddressId = ZA.AddressId and ZUA.UserId = ZU.UserId);
						

		UPDATE ZnodeImportProcessLog
		  SET Status = dbo.Fn_GetImportStatus( 2 ), ProcessCompletedDate = @GetDate
		WHERE ImportProcessLogId = @ImportProcessLogId;

		if @FailedRecordCount >1 
		begin 
			UPDATE ZnodeImportProcessLog
		  SET Status = dbo.Fn_GetImportStatus( 3 ), ProcessCompletedDate = @GetDate
		WHERE ImportProcessLogId = @ImportProcessLogId;
		End 

		COMMIT TRAN A;
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN A;
		UPDATE ZnodeImportProcessLog
		  SET Status = dbo.Fn_GetImportStatus( 3 ), ProcessCompletedDate = @GetDate
		WHERE ImportProcessLogId = @ImportProcessLogId;

		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
				SELECT '62', ERROR_MESSAGE(), ERROR_PROCEDURE(), @NewGUId,  ERROR_LINE(),  @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
					--from ##insertEmployees ZIC ;
				

		SET @Status = 0;
		SELECT ERROR_LINE(), ERROR_MESSAGE(), ERROR_PROCEDURE();
		
	END CATCH;
END;
