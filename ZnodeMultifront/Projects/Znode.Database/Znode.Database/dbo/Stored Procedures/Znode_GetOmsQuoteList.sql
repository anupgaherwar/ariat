﻿
CREATE PROCEDURE [dbo].[Znode_GetOmsQuoteList]    
(     
  @WhereClause NVARCHAR(MAX),    
  @Rows        INT            = 100,    
  @PageNo      INT            = 1  ,    
  @Order_BY    VARCHAR(1000)  = '' ,    
  @RowsCount   INT OUT             ,    
  @AccountId   INT,    
  @UserId      INT            = 0,     
  @IsPendingPayment BIT = 0  ,     
  @IsParentPendingOrder  BIT = 1     
  )    
AS     
   /*    
  Summary :- This procedure is used to get the Quote list of account and Users    
    Fn_GetRecurciveAccounts is used to fetch AccountId and Its recursive ParentId      
    @InnerWhereClause contains AccountId fetched from the Function Fn_GetRecurciveAccounts     
    OrderDetails are fetched from the tables filtered by AccountId Present in @InnerWhereClause    
    OrderDetails are fetched in Descending order of OmsQuoteId    
     Unit Testing     
     
     EXEC Znode_GetOmsQuoteList '(PortalId in(''13'',''2'',''3'',''6'')) ' ,@RowsCount = 0 ,@AccountId = 0,@UserId = 0      
    
*/    
     BEGIN    
         BEGIN TRY    
			SET NOCOUNT ON;    
			DECLARE @SQL NVARCHAR(MAX)= '', @InnerWhereClause VARCHAR(MAX)= '', @ProcessType  varchar(50)='Quote',@QuoteFilter NVARCHAr(max)='';    
    
            DECLARE @TBL_QuoteDetails TABLE (OmsQuoteId INT,UserName NVARCHAR(300),AccountName NVARCHAR(400),QuoteOrderTotal NUMERIC(28, 6),[OrderStatus] VARCHAR(300),    
            CreatedDate DATETIME,StoreName NVARCHAR(Max),CurrencyCode VARCHAR(100),CultureCode VARCHAR(100),PublishState nvarchar(600),RowId INT,CountNo INT,CreatedByName NVARCHAr(max) ,ModifiedByName NVARCHAR(max),IsConvertedToOrder bit,OrderType varchar(50), UserId INT);    
           
             IF @UserId <> 0  AND @IsParentPendingOrder   = 1           
                 BEGIN    
                     SET @InnerWhereClause = ' AND '''+CAST(@UserId AS VARCHAR(max))+''' = ZU.UserId ';    
                    -- SET @AccountId = (SELECT TOP 1 AccountID FROM ZnodeUser WHERE UserId = @UserId);    
                 END    
             ELSE IF @IsParentPendingOrder   = 0     
                BEGIN    
				SET @InnerWhereClause = ' AND  EXISTS (SELECT TOP 1 1 FROM [dbo].[Fn_GetRecurciveUserId] ('+CAST(@UserId AS VARCHAR(50))+','''+@ProcessType+''') SP WHERE (SP.UserId = ZU.UserId OR SP.UserId IS NULL)  )';   
   
				SET @QuoteFilter =' AND EXISTS (SELECT TOP 1 1 FROM ZnodeOMSQuoteApproval WR WHERE WR.OmsQuoteId = ZOQ.OmsQuoteId AND ( Wr.ApproverUserId ='+CAST(@UserId AS VARCHAR(50))+' OR Wr.UserId = '+CAST(@UserId AS VARCHAR(50))+'  ) ) ';        
				END    
    ELSE     
    BEGIN     
      SET @InnerWhereClause = ''    
    END       
          
    IF @IsPendingPayment =1     
    BEGIN     
       
     SET @InnerWhereClause = @InnerWhereClause+' AND NOT EXISTS ( SELECT TOP 1 1 FROM ZnodeUserGlobalAttributeValue a     
    INNER JOIN ZnodeUserGlobalAttributeValueLocale b  on (b.UserGlobalAttributeValueId = a.UserGlobalAttributeValueId)    
    INNER JOIN ZnodeGlobalAttribute c ON (c.GlobalAttributeid = a.GlobalAttributeId )    
    WHERE c.AttributeCOde = ''BillingAccountNumber'' AND a.UserId =  ZU.UserId AND b.AttributeValue = '''' ) AND ZOQ.IsPendingPayment =  1    '    
         
    END     
    ELSE     
    BEGIN    
       SET @InnerWhereClause = @InnerWhereClause+' AND ZOQ.IsPendingPayment = 0   '    
    END     
    
    SET @InnerWhereClause = @InnerWhereClause + CASE WHEN @AccountId > 0 THEN ' AND ZA.AccountId ='+CAST(@AccountId AS VARCHAR(200)) ELSE '' END     
    
    SET @SQL = '       
		;With Cte_GetQuoteDetail AS     
		(    
		SELECT Zu.UserId ,ZOQ.OmsQuoteId,ZU.FirstName + CASE WHEN ZU.LastName IS NULL THEN '''' ELSE '' ''+Zu.LastName END UserName , QuoteOrderTotal , ZOOS.Description [OrderStatus]    
		,ZOQ.CreatedDate,ZA.Name AccountName,ZP.PortalId,Zp.StoreName , ZCC.CurrencyCode AS CurrencyCode, ZC.CultureCode AS CultureCode ,ZVGD.UserName CreatedByName , ZVGDI.UserName ModifiedByName,    
		case When ZOQ.IsConvertedToOrder IS NULL THEN 0 ELSE ZOQ.IsConvertedToOrder END IsConvertedToOrder,ISNULL(DT.QuoteTypeCode,'''') QuoteTypeCode,ZODPS.DisplayName as PublishState,
		'+case when cast(@IsParentPendingOrder as varchar(10)) = 0  then +'Case  When TYUI.ApproverUserId ='+CAST(@UserId AS VARCHAR(50))  + ' then ''Approval Requested'' 
		else ''Pending For Approval'' END' else '''''' end +' OrderType    
		FROM ZnodeOmsQuote ZOQ    
		INNER JOIN ZnodeUser ZU ON (ZU.UserId = ZOQ.UserId)    
		LEFT JOIN ZnodePublishState ZODPS ON (ZODPS.PublishStateId = ZOQ.PublishStateId)  
		LEFT JOIN ZnodeUserPortal ZUP ON ZU.UserId = ZUP.UserId    
		inner JOIN ZnodePortal ZP ON ZP.PortalId = Zoq.PortalId    
		'+CASE WHEN @IsParentPendingOrder = 0  THEN ' LEFT JOIN ZnodeOMSQuoteApproval TYUI ON (TYUI.OmsQuoteId = ZOQ.OmsQuoteId AND TYUI.ApproverUserId ='+CAST(@UserId AS VARCHAR(50))+' ) ' ELSE '' END +'    
		LEFT JOIN ZnodePortalUnit ZPU ON (ZPU.PortalId = Zp.PortalId)    
		LEFT JOIN ZnodeCulture ZC ON (ZPU.CultureId = ZC.CultureId)    --- Changed join condition from CurrencyId to CultureId    
		LEFT JOIN ZnodeCurrency ZCC ON (ZC.CurrencyId = ZCC.CurrencyId)    --- Joined ZnodeCulture and ZnodeCurrency   
		LEFT JOIN ZnodeOmsOrderState ZOOS ON (ZOOS.OmsOrderStateId = '+CASE WHEN @IsParentPendingOrder = 0 AND EXISTS (SELECT TOP 1 1 FROM ZnodeOMSQuoteApproval OQ WHERE OQ.ApproverUserId = @UserId) THEN 'TYUI.OmsOrderStateId ' ELSE 'ZOQ.OmsOrderStateId' END  +' )     
		LEFT JOIN ZnodeAccount ZA ON (ZA.AccountId = ZU.AccountId )    
		LEFT JOIN [dbo].[View_GetUserDetails]  ZVGD ON (ZVGD.UserId = ZOQ.CreatedBy )    
		LEFT JOIN [dbo].[View_GetUserDetails]  ZVGDI ON (ZVGDI.UserId = ZOQ.ModifiedBy)    
		INNER JOIN ZnodeOmsQuoteType DT ON (DT.OmsQuoteTypeId = ZOQ.OmsQuoteTypeId)    
		WHERE DT.OmsQuoteTypeId <> (select top 1 OmsQuoteTypeId from ZnodeOmsQuoteType where QuoteTypeCode = ''QUOTE'')'+' '+@InnerWhereClause+@QuoteFilter+'    
    
		)    
		, Cte_GetQuote AS     
		(    
		SELECT OmsQuoteId,UserName ,AccountName , QuoteOrderTotal QuoteAmount, [OrderStatus]  ,CreatedDate ,StoreName,CurrencyCode, CultureCode,PublishState,CreatedByName , ModifiedByName ,IsConvertedToOrder,OrderType,'+dbo.Fn_GetPagingRowId(@Order_BY,'CreatedDate DESC
		,OmsQuoteId DESC')+',Count(*)Over() CountNo ,UserId      
		FROM Cte_GetQuoteDetail    
		WHERE 1=1     
		'+dbo.Fn_GetFilterWhereClause(@WhereClause)+'    
		)    
		SELECT OmsQuoteId,UserName ,AccountName ,  QuoteAmount, [OrderStatus]  ,CreatedDate ,StoreName,CurrencyCode, CultureCode,PublishState,RowId,CountNo,CreatedByName , ModifiedByName,IsConvertedToOrder,OrderType, UserId    
		FROM Cte_GetQuote     
		'+dbo.Fn_GetPaginationWhereClause(@PageNo,@Rows)    
      print @SQL
        INSERT INTO @TBL_QuoteDetails (OmsQuoteId, UserName, AccountName, QuoteOrderTotal ,OrderStatus, CreatedDate, StoreName,CurrencyCode, CultureCode,PublishState, RowId ,CountNo,CreatedByName , ModifiedByName,IsConvertedToOrder,OrderType, UserId)          
        EXEC (@SQL);    
        SET @RowsCount = ISNULL((SELECT TOP 1 CountNo FROM @TBL_QuoteDetails), 0);    
    
        SELECT  OmsQuoteId,UserName,AccountName,QuoteOrderTotal,[OrderStatus],CreatedDate,StoreName,CurrencyCode, CultureCode,PublishState,CreatedByName , ModifiedByName,IsConvertedToOrder  ,OrderType , UserId 
        FROM @TBL_QuoteDetails;    
        
         END TRY    
         BEGIN CATCH    
		DECLARE @Status BIT ;    
		SET @Status = 0;    
		DECLARE @Error_procedure VARCHAR(1000)= ERROR_PROCEDURE(), @ErrorMessage NVARCHAR(MAX)= ERROR_MESSAGE(), @ErrorLine VARCHAR(100)= ERROR_LINE(), @ErrorCall NVARCHAR(MAX)= 'EXEC Znode_GetOmsQuoteList @WhereClause = '+CAST(@WhereClause AS VARCHAR(max)  
		)+',@Rows='+CAST(@Rows AS VARCHAR(50))+',@PageNo='+CAST(@PageNo AS VARCHAR(50))+',@Order_BY='+@Order_BY+',@RowsCount='+CAST(@RowsCount AS VARCHAR(50))+',@AccountId='+CAST(@AccountId AS VARCHAR(50))+',@UserId='+CAST(@UserId AS VARCHAR(50))+',@PortalId='+''  
		+',@Status='+CAST(@Status AS VARCHAR(10));    
                      
		SELECT 0 AS ID,CAST(0 AS BIT) AS Status;                        
        
		EXEC Znode_InsertProcedureErrorLog    
		@ProcedureName = 'Znode_GetOmsQuoteList',    
		@ErrorInProcedure = @Error_procedure,    
		@ErrorMessage = @ErrorMessage,    
		@ErrorLine = @ErrorLine,    
		@ErrorCall = @ErrorCall;    
         END CATCH;    
     END