﻿


CREATE  PROCEDURE [dbo].[Znode_Ariat_ImportCustomer](
	  @TableName nvarchar(100), @Status bit OUT, @UserId int, @ImportProcessLogId int, @NewGUId nvarchar(200), @LocaleId int= 0,@PortalId int ,@CsvColumnString nvarchar(max))
AS
	--------------------------------------------------------------------------------------
	-- Summary :  Import to Create user and assign the customer to account 
	
	-- Unit Testing : 
	--------------------------------------------------------------------------------------

BEGIN
		
	BEGIN TRAN A;
	BEGIN TRY
	 
	 
		DECLARE @MessageDisplay nvarchar(100), @SSQL nvarchar(max),@AspNetZnodeUserId nvarchar(256),@ASPNetUsersId nvarchar(256),
		@PasswordHash nvarchar(max),@SecurityStamp nvarchar(max),@RoleId nvarchar(256),@IsAllowGlobalLevelUserCreation nvarchar(10)
		Declare @ProfileId  int
		DECLARE @FailedRecordCount BIGINT
		DECLARE @SuccessRecordCount BIGINT
		 
		SET @SecurityStamp = '0wVYOZNK4g4kKz9wNs-UHw2'
		SET @PasswordHash = 'APy4Tm1KbRG6oy7h3r85UDh/lCW4JeOi2O2Mfsb3OjkpWTp1YfucMAvvcmUqNaSOlA==';
		SELECT  @RoleId  = Id from AspNetRoles where   NAME = 'User'  

		Select @IsAllowGlobalLevelUserCreation = FeatureValues from ZnodeGlobalsetting where FeatureName = 'AllowGlobalLevelUserCreation'

		DECLARE @GetDate datetime= dbo.Fn_GetDate();
		-- Retrive RoundOff Value from global setting 

		-- Three type of import required three table varible for product , category and brand
		--DECLARE @InsertCustomer TABLE
		--( 
		--	RowId int IDENTITY(1, 1) PRIMARY KEY, RowNumber int, UserName nvarchar(512) ,FirstName	nvarchar(200),
		--	LastName nvarchar(200), BudgetAmount	numeric,Email	nvarchar(100),PhoneNumber	nvarchar(100),
		--    EmailOptIn	bit	,ReferralStatus	nvarchar(40),IsActive	bit	,ExternalId	nvarchar(max),CreatedDate Datetime,
		--	ProfileName varchar(200), GUID NVARCHAR(400)
		--);

		--	--SET @SSQL = 'SELECT RowNumber,UserName,FirstName,LastName,BudgetAmount,Email,PhoneNumber,EmailOptIn,IsActive,ExternalId,GUID FROM '+ @TableName;
		--SET @SSQL = 'SELECT RowNumber,' + @CsvColumnString + ',GUID FROM '+ @TableName;
		--INSERT INTO @InsertCustomer( RowNumber,UserName,FirstName,LastName,Email,PhoneNumber,       EmailOptIn,IsActive,ExternalId,CreatedDate,ProfileName,GUID )
		--EXEC sys.sp_sqlexec @SSQL;

		DECLARE @InsertCustomer TABLE
		( 
			RowId int IDENTITY(1, 1) PRIMARY KEY, RowNumber int, UserName nvarchar(512) ,FirstName	nvarchar(200),
			LastName nvarchar(200), BudgetAmount	numeric,Email	nvarchar(100),PhoneNumber	nvarchar(100),
		    EmailOptIn	bit	,ReferralStatus	nvarchar(40),IsActive	bit	,ExternalId	nvarchar(max),CreatedDate Datetime,
			ProfileName varchar(200), GUID NVARCHAR(400),AccountNumber varchar(200),AccountStoreId varchar(200),SalesRepId varchar(200)
		);

		 

			--SET @SSQL = 'SELECT RowNumber,UserName,FirstName,LastName,BudgetAmount,Email,PhoneNumber,EmailOptIn,IsActive,ExternalId,GUID FROM '+ @TableName;
		SET @SSQL = 'SELECT RowNumber,' + @CsvColumnString + ',GUID FROM '+ @TableName;
		INSERT INTO @InsertCustomer(RowNumber,UserName,FirstName,LastName,Email,PhoneNumber,EmailOptIn,IsActive,ExternalId,CreatedDate,ProfileName,AccountNumber,AccountStoreId,SalesRepId,guid)
		EXEC sys.sp_sqlexec @SSQL;
		
		
	 
		select TOP 1 @ProfileId   =  ProfileId from ZnodePortalprofile where Portalid = @Portalid and IsDefaultRegistedProfile=1
		If( Isnull(@ProfileId ,0) = 0 ) 
		Begin
		
			INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
				SELECT '62','Portalid' ,@ProfileId, @NewGUId, 1 , @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
				from @InsertCustomer

			
				UPDATE ZnodeImportProcessLog
				SET Status = dbo.Fn_GetImportStatus( 3 ), ProcessCompletedDate = @GetDate
				WHERE ImportProcessLogId = @ImportProcessLogId;
			

				SELECT @FailedRecordCount = COUNT(DISTINCT RowNumber) FROM ZnodeImportLog 
				WHERE RowNumber IS NOT NULL AND  ImportProcessLogId = @ImportProcessLogId;
				Select @SuccessRecordCount = 0

				UPDATE ZnodeImportProcessLog SET FailedRecordcount = @FailedRecordCount , SuccessRecordCount = @SuccessRecordCount , 
				TotalProcessedRecords = (ISNULL(@FailedRecordCount,0) + ISNULL(@SuccessRecordCount,0))
				WHERE ImportProcessLogId = @ImportProcessLogId;

				DELETE FROM @InsertCustomer 
				SET @Status = 0;

				COMMIT TRAN A;
				Return 0 
		End

		
		--UserName,FirstName,LastName,Email,PhoneNumber,EmailOptIn,IsActive,ExternalId
	
	    -- start Functional Validation 

		-----------------------------------------------
		--If @IsAllowGlobalLevelUserCreation = 'false'
		--		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
		--			   SELECT '10', 'UserName', UserName, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
		--			   FROM @InsertCustomer AS ii
		--			    WHERE ltrim(rtrim(ii.UserName)) in 
		--			   (
		--				   SELECT UserName FROM AspNetZnodeUser   where PortalId = @PortalId
		--			   );
		--Else 
		--		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
		--			   SELECT '10', 'UserName', UserName, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
		--			   FROM @InsertCustomer AS ii
		--			   WHERE ltrim(rtrim(ii.UserName)) in 
		--			   (
		--				   SELECT UserName FROM AspNetZnodeUser   
		--			   );
		
				INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
					   SELECT '35', 'UserName', UserName, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
					   FROM @InsertCustomer AS ii
					   WHERE ii.UserName not like '%_@_%_.__%' 
				
				INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
					   SELECT '30', 'UserName', UserName, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
					   FROM @InsertCustomer AS ii
					   WHERE ltrim(rtrim(ii.UserName)) in 
					   (SELECT ltrim(rtrim(UserName))  FROM @InsertCustomer group by ltrim(rtrim(UserName))  having count(*) > 1 )


				INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
					   SELECT '54', 'Account mismatch', II.AccountNumber, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
					   FROM @InsertCustomer AS ii
					  where NOT EXISTS (select top 1 1 from  ZnodeAccount ac where  ac.ExternalId=II.AccountNumber)

			INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
				SELECT '54','SalesRepid' ,ic.SalesRepId, @NewGUId, 1 , @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
				from @InsertCustomer ic
				where not exists (select top 1 1 from ZnodeUser ZU
					inner join AspNetUserRoles ANUR on ANUR.UserId = ZU.AspNetUserId
					inner join AspNetRoles ANR on ANR.id = ANUR.roleid and Name = 'Sales Rep'
					where ZU.Email=ic.SalesRepId)
				
		 UPDATE ZIL
			   SET ZIL.ColumnName =   ZIL.ColumnName + ' [ UserName - ' + ISNULL(UserName,'') + ' ] '
			   FROM ZnodeImportLog ZIL 
			   INNER JOIN @InsertCustomer IPA ON (ZIL.RowNumber = IPA.RowNumber)
			   WHERE  ZIL.ImportProcessLogId = @ImportProcessLogId AND ZIL.RowNumber IS NOT NULL

		--Note : Content page import is not required 
		
		-- End Function Validation 	
		-----------------------------------------------
		--- Delete Invalid Data after functional validatin  

		DELETE FROM @InsertCustomer
		WHERE RowNumber IN
		(
			SELECT DISTINCT 
				   RowNumber
			FROM ZnodeImportLog
			WHERE ImportProcessLogId = @ImportProcessLogId  and RowNumber is not null 
			--AND GUID = @NewGUID
		);


		-- Update Record count in log 
        
		SELECT @FailedRecordCount = COUNT(DISTINCT RowNumber) FROM ZnodeImportLog WHERE RowNumber IS NOT NULL AND  ImportProcessLogId = @ImportProcessLogId;
		Select @SuccessRecordCount = count(DISTINCT RowNumber) FROM @InsertCustomer
		UPDATE ZnodeImportProcessLog SET FailedRecordcount = @FailedRecordCount , SuccessRecordCount = @SuccessRecordCount , 
		TotalProcessedRecords = (ISNULL(@FailedRecordCount,0) + ISNULL(@SuccessRecordCount,0))
		WHERE ImportProcessLogId = @ImportProcessLogId;
		-- End

		-- Insert Product Data 
				
				
				DECLARE @InsertedAspNetZnodeUser TABLE (AspNetZnodeUserId nvarchar(256) ,UserName nvarchar(512),PortalId int )
				DECLARE @InsertedASPNetUsers TABLE (Id nvarchar(256) ,UserName nvarchar(512))
				DECLARE @InsertZnodeUser TABLE (UserId int,AspNetUserId nvarchar(256),CreatedDate Datetime )

				UPDATE ANU SET 
				ANU.PhoneNumber	= IC.PhoneNumber
				from AspNetZnodeUser ANZU INNER JOIN ASPNetUsers ANU ON ANZU.AspNetZnodeUserId = ANU.UserName 
				INNER JOIN ZnodeUser ZU ON ANU.ID = ZU.AspNetUserId	
				INNER JOIN @InsertCustomer IC ON ANZU.UserName = IC.UserName 
				where Isnull(ANZU.PortalId,0) = Isnull(@PortalId ,0)
				


				 /*CustomerAccount Code:Start*/
				  
				UPDATE ZU SET 
				ZU.Custom1		= IC.AccountStoreId, /*CustomerAccount customization to put AccountStoreId in custom1 field*/
				ZU.Custom2		= IC.SalesRepId, /*CustomerAccount customization to put AccountStoreId in custom1 field*/
				ZU.FirstName	= IC.FirstName,
				ZU.LastName		= IC.LastName,
				--ZU.MiddleName	= IC.MiddleName,
				ZU.BudgetAmount = IC.BudgetAmount,
				ZU.Email		= IC.Email,
				ZU.PhoneNumber	= IC.PhoneNumber,
				ZU.EmailOptIn	= Isnull(IC.EmailOptIn,0),
				ZU.IsActive		= IC.IsActive
				--ZU.ExternalId = ExternalId
				from AspNetZnodeUser ANZU INNER JOIN ASPNetUsers ANU ON ANZU.AspNetZnodeUserId = ANU.UserName 
				INNER JOIN ZnodeUser ZU ON ANU.ID = ZU.AspNetUserId	
				INNER JOIN @InsertCustomer IC ON ANZU.UserName = IC.UserName 
				where Isnull(ANZU.PortalId,0) = Isnull(@PortalId ,0)
				 /*CustomerAccount Code:End*/

				 
	
				Insert into AspNetZnodeUser (AspNetZnodeUserId, UserName, PortalId)		
				OUTPUT INSERTED.AspNetZnodeUserId, INSERTED.UserName, INSERTED.PortalId	INTO  @InsertedAspNetZnodeUser 			 
				Select NEWID(),IC.UserName, @PortalId FROM @InsertCustomer IC 
				where Not Exists (Select TOP 1 1  from AspNetZnodeUser ANZ where Isnull(ANZ.PortalId,0) = Isnull(@PortalId,0) AND ANZ.UserName = IC.UserName)

				INSERT INTO ASPNetUsers (Id,Email,EmailConfirmed,PasswordHash,SecurityStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,
				LockoutEndDateUtc,LockOutEnabled,AccessFailedCount,PasswordChangedDate,UserName)
				output inserted.Id, inserted.UserName into @InsertedASPNetUsers
				SELECT NewId(), Email,0 ,@PasswordHash,@SecurityStamp,PhoneNumber,0,0,case when IsActive =0 then '9999-12-31 23:59:59.997' else NULL end LockoutEndDateUtc,1 LockoutEnabled,
				0,@GetDate,AspNetZnodeUserId from @InsertCustomer A INNER JOIN @InsertedAspNetZnodeUser  B 
				ON A.UserName = B.UserName
				
				
				INSERT INTO  ZnodeUser(AspNetUserId,FirstName,LastName,CustomerPaymentGUID,Email,PhoneNumber,EmailOptIn,
				IsActive,ExternalId, CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
				OUTPUT Inserted.UserId, Inserted.AspNetUserId,Inserted.CreatedDate into @InsertZnodeUser
				SELECT IANU.Id AspNetUserId ,IC.FirstName,IC.LastName,null CustomerPaymentGUID,IC.UserName
				,IC.PhoneNumber,Isnull(IC.EmailOptIn,0),IC.IsActive,IC.ExternalId, @UserId,
				CASE WHEN IC.CreatedDate IS NULL OR IC.CreatedDate = '' THEN  @Getdate ELSE IC.CreatedDate END,@UserId,@Getdate
				from @InsertCustomer IC Inner join 
				@InsertedAspNetZnodeUser IANZU ON IC.UserName = IANZU.UserName  INNER JOIN 
				@InsertedASPNetUsers IANU ON IANZU.AspNetZnodeUserId = IANU.UserName 
				where not exists (select top 1 1 from znodeuser z where z.email = ic.UserName)
				  	     
				INSERT INTO AspNetUserRoles (UserId,RoleId)  Select AspNetUserId, @RoleID from @InsertZnodeUser 
				INSERT INTO ZnodeUserPortal (UserId,PortalId,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate) 
				SELECT UserId, @PortalId , @UserId, IZU.CreatedDate,@UserId,@Getdate 
				from @InsertZnodeUser IZU
				--Declare @ProfileId  int 
				--select TOP 1 @ProfileId   =  ProfileId from ZnodePortalprofile where Portalid = @Portalid and IsDefaultRegistedProfile=1

				--insert into ZnodeUserProfile (ProfileId,UserId,IsDefault,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
				--SELECT @ProfileId  , UserId, 1 , @UserId,CreatedDate,@UserId,@Getdate from @InsertZnodeUser

				--update aspnetusers
				--	set LockoutEndDateUtc  = getdate()
				--	from aspnetusers ANU inner join znodeuser ZU on ANU.id = ANU.AspNetUserId
				--	inner join 
				--	where Id ='3D4E455A-0D56-430C-99F0-EA747D37190B'
				 

				/*CustomerAccount customization to put AccountStoreId in custom1 field :start*/
				UPDATE ZU SET 
				ZU.Custom1		= IC.AccountStoreId,  /*CustomerAccount customization to put AccountStoreId in custom1 field*/
				ZU.Custom2		= IC.SalesRepId  /*CustomerAccount customization to put SalesRepId in custom2 field*/
				from AspNetZnodeUser ANZU INNER JOIN ASPNetUsers ANU ON ANZU.AspNetZnodeUserId = ANU.UserName 
				INNER JOIN ZnodeUser ZU ON ANU.ID = ZU.AspNetUserId	
				INNER JOIN @InsertCustomer IC ON ANZU.UserName = IC.UserName 
				where Isnull(ANZU.PortalId,0) = Isnull(@PortalId ,0)

				UPDATE ZU SET 
				ZU.AccountId		= ac.AccountId  /*CustomerAccount customization to put AccountStoreId in custom1 field*/
				
				from AspNetZnodeUser ANZU 
				INNER JOIN ASPNetUsers ANU ON ANZU.AspNetZnodeUserId = ANU.UserName 
				INNER JOIN ZnodeUser ZU ON ANU.ID = ZU.AspNetUserId	
				INNER JOIN @InsertCustomer IC ON ANZU.UserName = IC.UserName 
				INNER JOIN ZnodeAccount ac on ac.ExternalId=IC.AccountNumber
				where Isnull(ANZU.PortalId,0) = Isnull(@PortalId ,0)

				IF OBJECT_ID('tempdb..#testt') IS NOT NULL DROP TABLE #testt;
				
					create table #testt
					(SalesRepUserId int, 	CustomerUserid int, cu_username varchar(100), sal_username varchar(100))

					insert into #testt (CustomerUserid,cu_username)
					select zu.UserId, zu.Email from Znodeuser  ZU
					inner join @InsertCustomer t on Zu.Email = t.UserName

					update #testt
					set SalesRepUserId = zu.UserId,
					sal_username =zu.Email
					from Znodeuser  ZU
					inner join @InsertCustomer t on Zu.Email = t.SalesRepId
					where t.UserName= cu_username

					INSERT INTO [dbo].[ZnodeSalesRepCustomerUserPortal]
						([UserPortalId],[SalesRepUserId],[CustomerUserid],[CustomerPortalId],[CreatedBy],[CreatedDate],[ModifiedBy],[ModifiedDate])
							select  UserPortalId,SalesRepUserId,CustomerUserid, @PortalId,@UserId,@GetDate,@UserId,@GetDate  from #testt t
							inner join [ZnodeUserPortal] ZUP on ZUP.UserId = SalesRepUserId

				 INSERT INTO dbo.ZnodeAccountUserPermission
					(UserId,AccountPermissionAccessId,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
						 select ZU.UserId, ZAPA.AccountPermissionAccessId, 2,getdate(),2,getdate()  from
						    znodeuser ZU inner join @InsertCustomer t on Zu.Email = t.UserName
							inner join  [ZnodeAccountPermission] ZAP on ZAP.AccountPermissionName = 'Does not require approval'
							inner join [ZnodeAccountPermissionAccess] ZAPA on ZAPA.AccountPermissionId = ZAP.AccountPermissionId
							where not exists (select top 1 1 from ZnodeAccountUserPermission ZAUP where ZAUP.UserId =ZU.UserId and ZAUP.AccountPermissionAccessId =ZAPA.AccountPermissionAccessId)



				/*CustomerAccount customization to put AccountStoreId in custom1 field :End*/

---------------------------------------------------------------------------------

				declare @Profile table (ProfileId int)

				INSERT INTO ZnodeProfile (ProfileName,ShowOnPartnerSignup,Weighting,TaxExempt,DefaultExternalAccountNo,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,ParentProfileId)
				OUTPUT inserted.ProfileId INTO @Profile(ProfileId)
				SELECT Distinct ProfileName, 0, null,0, replace(ltrim(rtrim(ProfileName)),' ','') as DefaultExternalAccountNo, @UserId,@Getdate, @UserId,@Getdate, null as ParentProfileId				
				from @InsertCustomer IC
				where not exists(select * from ZnodeProfile ZP where IC.ProfileName = ZP.ProfileName )
				AND ISNULL(ic.ProfileName,'') <> ''

				INSERT INTO ZnodePortalProfile (PortalId,	ProfileId,	IsDefaultAnonymousProfile,	IsDefaultRegistedProfile,	CreatedBy,	CreatedDate,	ModifiedBy,	ModifiedDate)
				SELECT @PortalId, ProfileId, 0 AS IsDefaultAnonymousProfile, 0 AS IsDefaultRegistedProfile, @UserId,@Getdate, @UserId,@Getdate
				from @Profile

				UPDATE ZnodeUserProfile 
				SET ProfileId = Zap.ProfileId
				FROM ZnodeUser a
				inner join ASPNetUsers b on (b.Id = a.AspNetUserId)
				inner join AspNetZnodeUser c on (c.AspNetZnodeUserId = b.UserName)
				inner join @InsertCustomer IC on (IC.UserName = c.UserName)
				inner join ZnodeUserProfile u ON u.UserId = a.UserId
				inner join ZnodeAccount ac on ac.ExternalId=IC.AccountNumber
				inner join ZnodeAccountProfile ZAP on ac.AccountId =ZAP.AccountId and ZAP.IsDefault = 1
				--inner join ZnodeProfile ZP on IC.ProfileName = ZP.ProfileName
				--where IC.ProfileName <> ''
				
				INSERT INTO ZnodeUserProfile (ProfileId,UserId,IsDefault,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
				SELECT Zap.ProfileId  , a.UserId, 1 , @UserId,a.CreatedDate,@UserId,@Getdate 
				from ZnodeUser a
				INNER JOIN ASPNetUsers b on (b.Id = a.AspNetUserId)
				inner join AspNetZnodeUser c on (c.AspNetZnodeUserId = b.UserName)
				inner join @InsertCustomer IC on (IC.UserName = c.UserName)
				inner join ZnodeAccount ac on ac.ExternalId=IC.AccountNumber
				inner join ZnodeAccountProfile ZAP on ac.AccountId =ZAP.AccountId and ZAP.IsDefault = 1
				where NOT EXISTS (SELECT TOP  1 1 FROM ZnodeUserProfile u WHERE u.UserId = a.UserId )
				AND EXISTS(SELECT * FROM @InsertZnodeUser IZU WHERE A.UserId = IZU.UserId)

		
		UPDATE ZnodeImportProcessLog
		  SET Status = dbo.Fn_GetImportStatus( 2 ), ProcessCompletedDate = @GetDate
		WHERE ImportProcessLogId = @ImportProcessLogId;

		COMMIT TRAN A;
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN A;
		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
				SELECT '62',ERROR_PROCEDURE() ,ERROR_MESSAGE(), @NewGUId, ERROR_LINE() , @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
						
		UPDATE ZnodeImportProcessLog
		  SET Status = dbo.Fn_GetImportStatus( 3 ), ProcessCompletedDate = @GetDate
		WHERE ImportProcessLogId = @ImportProcessLogId;

		SET @Status = 0;
		SELECT ERROR_LINE(), ERROR_MESSAGE(), ERROR_PROCEDURE();
		
	END CATCH;
END;

