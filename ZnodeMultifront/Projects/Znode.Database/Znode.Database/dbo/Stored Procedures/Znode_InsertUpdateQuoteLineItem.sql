﻿
CREATE PROCEDURE [dbo].[Znode_InsertUpdateQuoteLineItem]
(   @OmsQuoteId      INT ,
    @UserId          INT = 0,
	@OmsSavedCartId  INT = 0,
    @Status          BIT OUT ,
	@SKUPriceForQuote [dbo].[SKUPriceForQuote] ReadOnly
	
)
AS 
   /* 
    Summary: This Procedure is used to save and edit the quote line item      
    Unit Testing   
    Exec Znode_InsertUpdateQuoteLineItem 1071,1527,0
   
	*/
     BEGIN
         BEGIN TRAN A;
         BEGIN TRY
             SET NOCOUNT ON;
			 DECLARE @GetDate DATETIME = dbo.Fn_GetDate();
			 DECLARE  @OmsQuoteLineItemId INT;
			 DECLARE @TempOmsQuoteLineItem TABLE (OmsQuoteLineItemId INT , OmsSavedCartLineItemId INT , ParentOmsSavedCartLineItemId INT  )
			
			CREATE TABLE #ZnodeOmsSavedCartLineItem
			(OmsSavedCartLineItemId	int,
			ParentOmsSavedCartLineItemId	int,
			OmsSavedCartId	int,
			SKU	nvarchar(200),
			Quantity	numeric(28,	13) ,
			OrderLineItemRelationshipTypeId	int,
			CustomText	nvarchar(MAX),
			CartAddOnDetails	nvarchar(MAX),
			Sequence	int,
			CreatedBy	int,
			CreatedDate	datetime,
			ModifiedBy	int,
			ModifiedDate	datetime,
			AutoAddon	nvarchar(MAX),
			OmsOrderId	int,
			Custom1	nvarchar(MAX),
			Custom2	nvarchar(MAX),
			Custom3	nvarchar(MAX),
			Custom4	nvarchar(MAX),
			Custom5	nvarchar(MAX),
			GroupId	nvarchar(MAX),
			ProductName	nvarchar(2000),
			Description	nvarchar(MAX),
			Price numeric(28,6),
			ShippingCost numeric(28,6))

			IF (isnull(@OmsSavedCartId,0) <> 0)
			BEGIN
				INSERT INTO #ZnodeOmsSavedCartLineItem(OmsSavedCartLineItemId,ParentOmsSavedCartLineItemId,OmsSavedCartId,SKU,Quantity,OrderLineItemRelationshipTypeId,CustomText,CartAddOnDetails,
				                                       Sequence,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,AutoAddon,OmsOrderId,Custom1,Custom2,Custom3,Custom4,Custom5,GroupId,ProductName,Description)
				SELECT a.OmsSavedCartLineItemId,a.ParentOmsSavedCartLineItemId,a.OmsSavedCartId,a.SKU,a.Quantity,a.OrderLineItemRelationshipTypeId,a.CustomText,a.CartAddOnDetails,
				       a.Sequence,a.CreatedBy,a.CreatedDate,a.ModifiedBy,a.ModifiedDate,a.AutoAddon,a.OmsOrderId,a.Custom1,a.Custom2,a.Custom3,a.Custom4,a.Custom5,a.GroupId,a.ProductName,a.Description 				
				FROM ZnodeOmsSavedCartLineItem  a 
				WHERE NOT EXISTS (SELECT TOP 1 1 FROM ZnodeOmsQuoteLineItem ty WHERE ty.OmsQuoteId = @OmsQuoteId) and a.OmsSavedCartId=@OmsSavedCartId
				
			END
			ELSE
			BEGIN
				INSERT INTO #ZnodeOmsSavedCartLineItem(OmsSavedCartLineItemId,ParentOmsSavedCartLineItemId,OmsSavedCartId,SKU,Quantity,OrderLineItemRelationshipTypeId,CustomText,CartAddOnDetails,
				                                       Sequence,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,AutoAddon,OmsOrderId,Custom1,Custom2,Custom3,Custom4,Custom5,GroupId,ProductName,Description)
				SELECT a.OmsSavedCartLineItemId,a.ParentOmsSavedCartLineItemId,a.OmsSavedCartId,a.SKU,a.Quantity,a.OrderLineItemRelationshipTypeId,a.CustomText,a.CartAddOnDetails,
				       a.Sequence,a.CreatedBy,a.CreatedDate,a.ModifiedBy,a.ModifiedDate,a.AutoAddon,a.OmsOrderId,a.Custom1,a.Custom2,a.Custom3,a.Custom4,a.Custom5,a.GroupId,a.ProductName,a.Description 
				FROM ZnodeOmsSavedCartLineItem  a 
				INNER JOIN ZnodeOmsSavedCart b ON (b.OmsSavedCartId = a.OmsSavedCartId)
				INNER JOIN ZnodeOmsCookieMapping c ON (c.OmsCookieMappingId = b.OmsCookieMappingId)
				WHERE NOT EXISTS (SELECT TOP 1 1 FROM ZnodeOmsQuoteLineItem ty WHERE ty.OmsQuoteId = @OmsQuoteId) 
				AND c.UserId = @UserId
			END

			UPDATE ZOSLI set ZOSLI.Price = SPQ.Price, ZOSLI.ShippingCost = SPQ.ShippingCost
			FROM #ZnodeOmsSavedCartLineItem ZOSLI
			INNER JOIN @SKUPriceForQuote SPQ ON ZOSLI.OmsSavedCartLineItemId = SPQ.OmsSavedCartLineItemId
			
			MERGE INTO ZnodeOmsQuoteLineItem TARGET 
			USING #ZnodeOmsSavedCartLineItem  SOURCE 
			ON (1=0 )
			WHEN NOT MATCHED THEN 
			INSERT   (ParentOmsQuoteLineItemId,OmsQuoteId,SKU,Quantity,OrderLineItemRelationshipTypeId
						,CustomText,CartAddOnDetails,Sequence,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,GroupId,ProductName,Description, Price, ShippingCost)
			
			 
			VALUES ( NULL,@OmsQuoteId,SOURCE.SKU,SOURCE.Quantity,SOURCE.OrderLineItemRelationshipTypeId
						,SOURCE.CustomText,SOURCE.CartAddOnDetails,SOURCE.Sequence,SOURCE.CreatedBy,SOURCE.CreatedDate,SOURCE.ModifiedBy,SOURCE.ModifiedDate,SOURCE.GroupId,SOURCE.ProductName,SOURCE.Description, SOURCE.Price, SOURCE.ShippingCost ) 
			
			OUTPUT Inserted.OmsQuoteLineItemId , SOURCE.OmsSavedCartLineItemId,Source.ParentOmsSavedCartLineItemId  INTO @TempOmsQuoteLineItem ;


			UPDATE a
			SET a.ParentOmsQuoteLineItemId =( SELECT TOP 1 b1.OmsQuoteLineItemId FROM  @TempOmsQuoteLineItem b1 WHERE b.ParentOmsSavedCartLineItemId  = b1.OmsSavedCartLineItemId)  
			FROM ZnodeOmsQuoteLineItem  a 
			INNER JOIN @TempOmsQuoteLineItem b ON (b.OmsQuoteLineItemId = a.OmsQuoteLineItemId)
			WHERE b.ParentOmsSavedCartLineItemId IS NOT NULL 



			INSERT INTO ZnodeOmsQuotePersonalizeItem (OmsQuoteLineItemId,PersonalizeCode,PersonalizeValue,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate
														,DesignId,ThumbnailURL)
			SELECT b.OmsQuoteLineItemId,PersonalizeCode,PersonalizeValue,a.CreatedBy,a.CreatedDate,a.ModifiedBy,a.ModifiedDate
														,DesignId,ThumbnailURL
			FROM ZnodeOmsPersonalizeCartItem a  
			INNER JOIN @TempOmsQuoteLineItem b ON (b.OmsSavedCartLineItemId =  a.OmsSavedCartLineItemId )         


             SET @Status = 1;
             COMMIT TRAN A;
         END TRY
         BEGIN CATCH
        
		     SET @Status = 0;
		     DECLARE @Error_procedure VARCHAR(1000)= ERROR_PROCEDURE(), @ErrorMessage NVARCHAR(MAX)= ERROR_MESSAGE(), @ErrorLine VARCHAR(100)= ERROR_LINE(),
			 @ErrorCall NVARCHAR(MAX)= 'EXEC Znode_InsertUpdateQuoteLineItem @CartLineItemXML = '+CAST('' AS VARCHAR(max))+',@UserId = '+CAST(@UserId AS VARCHAR(50))+',@Status='+CAST(@Status AS VARCHAR(10));
              			 
             SELECT 0 AS ID,CAST(0 AS BIT) AS Status;                    
			 ROLLBACK TRAN A;
             EXEC Znode_InsertProcedureErrorLog
				@ProcedureName = 'Znode_InsertUpdateQuoteLineItem',
				@ErrorInProcedure = @Error_procedure,
				@ErrorMessage = @ErrorMessage,
				@ErrorLine = @ErrorLine,
				@ErrorCall = @ErrorCall;
         END CATCH;
     END;