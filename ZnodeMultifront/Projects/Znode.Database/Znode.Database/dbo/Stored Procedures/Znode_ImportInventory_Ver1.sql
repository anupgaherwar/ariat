﻿

CREATE PROCEDURE [dbo].[Znode_ImportInventory_Ver1](
	  @TableName nvarchar(100), @Status bit OUT, @UserId int, @ImportProcessLogId int, @NewGUId nvarchar(200))
AS
	--------------------------------------------------------------------------------------
	-- Summary :  Import Inventory data 
	--		   Input data in XML format Validate data with all scenario 
	-- Unit Testing : 
	--BEGIN TRANSACTION;
	--update ZnodeGlobalSetting set FeatureValues = '5' WHERE FeatureName = 'InventoryRoundOff' 
	--    DECLARE @status INT;
	--    EXEC [Znode_ImportInventory] @InventoryXML = '<ArrayOfImportInventoryModel>
	-- <ImportInventoryModel>
	--   <SKU>S1002</SKU>
	--   <Quantity>999998.33</Quantity>
	--   <ReOrderLevel>10</ReOrderLevel>
	--   <RowNumber>1</RowNumber>
	--   <ListCode>TestInventory</ListCode>
	--   <ListName>TestInventory</ListName>
	-- </ImportInventoryModel>
	--</ArrayOfImportInventoryModel>' , @status = @status OUT , @UserId = 2;
	--    SELECT @status;
	--    ROLLBACK TRANSACTION;
	--------------------------------------------------------------------------------------

BEGIN
	--BEGIN TRAN A;
	BEGIN TRY
		DECLARE @RoundOffValue int, @MessageDisplay nvarchar(100), @MessageDisplayForFloat nvarchar(100);
		DECLARE @GetDate datetime= dbo.Fn_GetDate();
		-- Retrive RoundOff Value from global setting 
		SELECT @RoundOffValue = FeatureValues
		FROM ZnodeGlobalSetting
		WHERE FeatureName = 'InventoryRoundOff';
		
		IF OBJECT_ID('tempdb.dbo.#InserInventoryForValidation', 'U') IS NOT NULL 
		DROP TABLE tempdb.dbo.#InserInventoryForValidation
		
		IF OBJECT_ID('tempdb.dbo.#InsertInventory ', 'U') IS NOT NULL 
		DROP TABLE tempdb.dbo.#InsertInventory 

		--@MessageDisplay will use to display validate message for input inventory value  

		DECLARE @sSql nvarchar(max);
		SET @sSql = ' Select @MessageDisplay_new = Convert(Numeric(28, '+CONVERT(nvarchar(200), @RoundOffValue)+'), 123.12345699 ) ';
		EXEC SP_EXecutesql @sSql, N'@MessageDisplay_new NVARCHAR(100) OUT', @MessageDisplay_new = @MessageDisplay OUT;
		SET @sSql = ' Select @MessageDisplay_new = Convert(Numeric(28, '+CONVERT(nvarchar(200), @RoundOffValue)+'), 0.999999 ) ';
		EXEC SP_EXecutesql @sSql, N'@MessageDisplay_new NVARCHAR(100) OUT', @MessageDisplay_new = @MessageDisplayForFloat OUT;
		Create TABLE tempdb..#InserInventoryForValidation 
		( 
				RowNumber int, SKU varchar(max), Quantity varchar(max), ReOrderLevel varchar(max), WarehouseCode varchar(max)
				
				, BackOrderQuantity varchar(max), 
			  BackOrderExpectedDate varchar(max)
				, GUID nvarchar(400)
		);
		CREATE TABLE tempdb..#InsertInventory  
		( 
				InsertInventoryId int IDENTITY(1, 1) PRIMARY KEY, RowNumber int, SKU varchar(300) INDEX Ix CLUSTERED (SKU), Quantity numeric(28, 6), ReOrderLevel numeric(28, 6), WarehouseCode varchar(200)
				
				, BackOrderQuantity NUMERIC(28, 6), 
			  BackOrderExpectedDate DateTime
				, GUID nvarchar(400) 
		);
		--DECLARE tempdb..#InsertInventory  TABLE
		--( 
		--		InsertInventoryId int IDENTITY(1, 1) PRIMARY KEY, RowNumber int, SKU varchar(300) INDEX Ix CLUSTERED (SKU), Quantity numeric(28, 6), ReOrderLevel numeric(28, 6), WarehouseCode varchar(200), GUID nvarchar(400) 
		--);
	

	     
		DECLARE @SKU TABLE
		( 
				SKU nvarchar(300)
		);
		
	-- 	SELECT * INTO _temp FROM [dbo].[##Inventory_164ac2dd-4607-451d-9ed7-ca8b312e369b]
			
		INSERT INTO @SKU
			   SELECT b.AttributeValue
			   FROM ZnodePimAttributeValue AS a
					INNER JOIN
					ZnodePimAttributeValueLocale AS b
					ON a.PimAttributeId = dbo.Fn_GetProductSKUAttributeId() AND 
					   a.PimAttributeValueId = b.PimAttributeValueId;

		DECLARE @InventoryListId int;
		SET @SSQL = 'Select RowNumber,SKU,Quantity,ReOrderLevel,WarehouseCode , BackOrderQuantity , 
			  BackOrderExpectedDate  ,GUID FROM '+@TableName;
		INSERT INTO tempdb..#InserInventoryForValidation( RowNumber, SKU, Quantity, ReOrderLevel, WarehouseCode,BackOrderQuantity , 
			  BackOrderExpectedDate, GUID )
		EXEC sys.sp_sqlexec @SSQL;
		
		
		

		--Required Validation 
		--UomName should not be null 
		--Data for this Inventory list is already available  
		-- 
		-- 1)  Validation for SKU is pending Proper data not found and 
		--Discussion still open for Publish version where we create SKU and use thi SKU code for validation 
		--Select * from ZnodePimAttributeValue  where PimAttributeId =248
		--select * from View_ZnodePimAttributeValue Vzpa Inner join ZnodePimAttribute Zpa on Vzpa.PimAttributeId=Zpa.PimAttributeId where Zpa.AttributeCode = 'SKU'
		--Select * from ZnodePimAttribute where AttributeCode = 'SKU'
		--2)  Start Data Type Validation for XML Data  
		--SELECT * FROM ZnodeInventory
		--SELECT * FROM ZNodeInventoryList
		UPDATE tempdb..#InserInventoryForValidation
		  SET ReOrderLevel = 0
		WHERE ReOrderLevel = '';

		UPDATE tempdb..#InserInventoryForValidation
		  SET BackOrderQuantity = 0
		WHERE BackOrderQuantity = '';

		UPDATE tempdb..#InserInventoryForValidation
		  SET BackOrderExpectedDate = null
		WHERE BackOrderExpectedDate = '';

		DELETE FROM tempdb..#InserInventoryForValidation
		WHERE RowNumber IN
		(

			SELECT DISTINCT 
				   RowNumber
			FROM ZnodeImportLog
			WHERE ImportProcessLogId = @ImportProcessLogId AND 
				  GUID = @NewGUID
		);
	
		INSERT INTO tempdb..#InsertInventory ( RowNumber, SKU, Quantity, ReOrderLevel, WarehouseCode,BackOrderQuantity , 
			  BackOrderExpectedDate )
			   SELECT RowNumber, SKU, Quantity, ReOrderLevel, WarehouseCode,BackOrderQuantity , 
			  BackOrderExpectedDate
			   FROM tempdb..#InserInventoryForValidation;
					 
		-- start Functional Validation 
		-----------------------------------------------
		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
			   SELECT '19', 'SKU', SKU, @NewGUId, RowNumber, 2, @GetDate, 2, @GetDate, @ImportProcessLogId
			   FROM tempdb..#InsertInventory  AS ii
			   WHERE ii.SKU NOT IN
			   (
				   SELECT SKU
				   FROM @SKU
			   );
		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
			   SELECT '19', 'WarehouseCode', WarehouseCode, @NewGUId, RowNumber, 2, @GetDate, 2, @GetDate, @ImportProcessLogId
			   FROM tempdb..#InsertInventory  AS ii
			   WHERE NOT EXISTS
			   (
				   SELECT TOP 1 1
				   FROM ZnodeWarehouse AS zw
				   WHERE zw.WarehouseCode = ii.WarehouseCode
			   );

		UPDATE ZIL
			   SET ZIL.ColumnName =   ZIL.ColumnName + ' [ SKU - ' + ISNULL(SKU,'') + ' ] '
			   FROM ZnodeImportLog ZIL 
			   INNER JOIN #InsertInventory IPA ON (ZIL.RowNumber = IPA.RowNumber)
			   WHERE  ZIL.ImportProcessLogId = @ImportProcessLogId AND ZIL.RowNumber IS NOT NULL


		-- End Function Validation 	
		-----------------------------------------------
		--- Delete Invalid Data after functional validatin  
		DELETE FROM tempdb..#InsertInventory 
		WHERE RowNumber IN
		(
			SELECT DISTINCT 
				   RowNumber
			FROM ZnodeImportLog
			WHERE ImportProcessLogId = @ImportProcessLogId AND 
				  GUID = @NewGUID
		);
		
		DECLARE @TBL_ReadyToInsertInventory TABLE
		( 
			RowNumber int, SKU varchar(300), Quantity numeric(28, 6), ReOrderLevel numeric(28, 6), WarehouseId int
			, BackOrderQuantity NUMERIC(28, 6), 
			  BackOrderExpectedDate DateTime
		);

		INSERT INTO @TBL_ReadyToInsertInventory( RowNumber, SKU, Quantity, ReOrderLevel, WarehouseId,BackOrderQuantity , 
			  BackOrderExpectedDate )
			   SELECT ii.RowNumber, ii.SKU, ii.Quantity, ISNULL(ii.ReOrderLevel, 0), zw.WarehouseId
			  , BackOrderQuantity , 
			  BackOrderExpectedDate
			   FROM tempdb..#InsertInventory  AS ii
					INNER JOIN
					ZnodeWarehouse AS zw
					ON ii.WarehouseCode = zw.WarehouseCode AND 
					   ii.RowNumber IN
			   (
				   SELECT MAX(ii1.RowNumber)
				   FROM tempdb..#InsertInventory  AS ii1
				   WHERE ii1.WarehouseCode = ii.WarehouseCode AND 
						 ii1.SKU = ii.SKU
			   );
				-- Update Record count in log 
        DECLARE @FailedRecordCount BIGINT
		DECLARE @SuccessRecordCount BIGINT
		SELECT @FailedRecordCount = COUNT(DISTINCT RowNumber) FROM ZnodeImportLog WHERE RowNumber IS NOT NULL AND  ImportProcessLogId = @ImportProcessLogId;
		Select @SuccessRecordCount = count(DISTINCT RowNumber) FROM @TBL_ReadyToInsertInventory
		UPDATE ZnodeImportProcessLog SET FailedRecordcount = @FailedRecordCount , SuccessRecordCount = @SuccessRecordCount,
		TotalProcessedRecords = (ISNULL(@FailedRecordCount,0) + ISNULL(@SuccessRecordCount,0)) 
		WHERE ImportProcessLogId = @ImportProcessLogId;
		-- End
		   	
		
		--select 'update started'  
		UPDATE zi
		  SET Quantity = rtii.Quantity, ReOrderLevel = ISNULL(rtii.ReOrderLevel, 0), ModifiedBy = @UserId, ModifiedDate = @GetDate
		  ,BackOrderQuantity = rtii.BackOrderQuantity, BackOrderExpectedDate = rtii.BackOrderExpectedDate
		FROM ZNodeInventory zi
			 INNER JOIN	 @TBL_ReadyToInsertInventory rtii
			 ON( zi.WarehouseId = rtii.WarehouseId AND 
				 zi.SKU = rtii.SKU
			   );
			   
		--select 'update End'                
		INSERT INTO ZnodeInventory( WarehouseId, SKU, Quantity, ReOrderLevel, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate,BackOrderQuantity , 
			  BackOrderExpectedDate )
			   SELECT WarehouseId, SKU, Quantity, ISNULL(ReOrderLevel, 0), @UserId, @GetDate, @UserId, @GetDate,ISNULL(BackOrderQuantity,0) , 
			  BackOrderExpectedDate
			   FROM @TBL_ReadyToInsertInventory AS rtii
			   WHERE NOT EXISTS
			   (
				   SELECT TOP 1 1
				   FROM ZnodeInventory AS zi
				   WHERE zi.WarehouseId = rtii.WarehouseId AND 
						 zi.SKU = rtii.SKU
			   ); 
		--select 'End'
		--      SET @Status = 1;
		UPDATE ZnodeImportProcessLog
		  SET Status = dbo.Fn_GetImportStatus( 2 ), ProcessCompletedDate = GETDATE()
		WHERE ImportProcessLogId = @ImportProcessLogId;

		--COMMIT TRAN A;
	END TRY
	BEGIN CATCH

		SET @Status = 0;
		SELECT ERROR_LINE(), ERROR_MESSAGE(), ERROR_PROCEDURE();
		--ROLLBACK TRAN A;
	END CATCH;
END;