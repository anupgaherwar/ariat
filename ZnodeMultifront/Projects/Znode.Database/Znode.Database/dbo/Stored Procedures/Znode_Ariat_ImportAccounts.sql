﻿





CREATE PROCEDURE [dbo].[Znode_Ariat_ImportAccounts] 
	
	(
	 @TableName nvarchar(100), @Status bit OUT, @UserId int, @ImportProcessLogId int, @NewGUId nvarchar(200), @LocaleId int= 0,@PortalId int ,@CsvColumnString nvarchar(max)

	  )
AS
BEGIN
	BEGIN TRAN A;
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @GetDate DATETIME= dbo.Fn_GetDate();
	BEGIN TRY
		
		Declare @profileid int =0;--, @PortalId int =0;
		set @portalid =0;
		DECLARE @ssql VARCHAR(1000);
		IF OBJECT_ID(N'tempdb.##insertAccount') IS NOT NULL
			BEGIN
				DROP TABLE ##insertAccount
			END
		SET @SSQL = 'Select  * into ##InsertAccount from ' + @TableName +''; 
		EXEC sys.sp_sqlexec @SSQL;
		
		Declare @FailedRecordCount int =0, @SuccessRecordCount int = 0;
			--INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
			--	SELECT '62', 'Default Account Profile', ZIC.ProfileId, null,  RowNumber,  @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
			--		from ##insertAccount ZIC where not exists (select top 1 1 from ZnodeProfile ZP where ZP.ProfileName = ZIC.ProfileId);
				
			
			INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
				SELECT '10', 'PortalId', ZIC.PortalId , null,   RowNumber , @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId
					FROM ##insertAccount ZIC where not exists (select top 1 1 from ZnodePortal ZP where ZP.StoreCode = ZIC.PortalId);
			   
			DELETE FROM ##insertAccount
				WHERE RowNumber IN
				(
					SELECT DISTINCT 
						   RowNumber
					FROM ZnodeImportLog
					WHERE ImportProcessLogId = @ImportProcessLogId  and RowNumber is not null 
					
				);
			
			 
		SELECT @FailedRecordCount = COUNT(DISTINCT RowNumber) FROM ZnodeImportLog WHERE   ImportProcessLogId = @ImportProcessLogId;

		Select @SuccessRecordCount = count(DISTINCT RowNumber) FROM ##insertAccount;
 
		UPDATE ZnodeImportProcessLog SET FailedRecordcount = @FailedRecordCount , SuccessRecordCount = @SuccessRecordCount , 
			TotalProcessedRecords = (ISNULL(@FailedRecordCount,0) + ISNULL(@SuccessRecordCount,0))
				WHERE ImportProcessLogId = @ImportProcessLogId;

		
		
		
		INSERT INTO [dbo].[ZnodeAddress]([FirstName],[LastName],[DisplayName],[CompanyName],[Address1],[Address2]
						,[Address3],[CountryName],[StateName],[CityName],[PostalCode],[PhoneNumber],[FaxNumber],[IsDefaultBilling],[IsDefaultShipping]
						,[IsActive],[ExternalId],[CreatedBy],[CreatedDate],[ModifiedBy],[ModifiedDate],[IsShipping],[IsBilling],Custom1 )
		SELECT DISTINCT 
		case when firstname ='' or firstname is null then  left(Name,case when CHARINDEX(' ', Name) =0 then len(Name) else CHARINDEX(' ', Name) end ) else FirstName end  AS FirstName,
		case when lastname ='' or lastname is null then  case when CHARINDEX(' ',Name) >0 then
				case when CHARINDEX(' ',Name,CHARINDEX(' ',Name)+1) =0
					then   substring(Name,CHARINDEX(' ',Name)+1,len(Name)-CHARINDEX(' ',Name))
					else  SUBSTRING(Name,CHARINDEX(' ',Name)+1,  CHARINDEX(' ',Name,CHARINDEX(' ',Name)+1)-CHARINDEX(' ',Name)) end
					else Name end else LastName end  AS LastName,
		DisplayName,CompanyName,Address1,Address2,Address3,CountryName,StateName,CityName,
		PostalCode,case when PhoneNumber ='' then '000-000-0000' else  isnull(PhoneNumber,'000-000-0000') end ,FaxNumber,0,0,1,Null,@UserId,@GetDate,@UserId,@GetDate,1,1,AccountStoreId
		FROM ##insertAccount IA 
		WHERE NOT EXISTS (SELECT TOP 1  1  FROM ZnodeAddress ZA WHERE ZA.DisplayName = IA.DisplayName AND ZA.Address1 = IA.Address1 
		AND ZA.custom1= ia.AccountStoreId		)		;

		INSERT INTO [dbo].[ZnodeAccount] ([ParentAccountId],[Name],[Desription],[BudgetAmount],[IsActive],[ExternalId],[PublishCatalogId],[CreatedBy],[CreatedDate],[ModifiedBy],[ModifiedDate])
		SELECT DISTINCT NULL,NAME,NULL,NULL,NULL,ExternalId,NULL,@UserId,@GetDate,@UserId,@GetDate 
		FROM ##insertAccount a
		WHERE NOT EXISTS (SELECT TOP 1 1 FROM [dbo].[ZnodeAccount] RT WHERE RT.[Name] =  a.[Name] AND ISNULL(RT.ExternalId,'') = ISNULL(a.ExternalId,''));


		INSERT INTO [dbo].[ZnodeAccountAddress]([AccountId],[AddressId],[CreatedBy],[CreatedDate],[ModifiedBy],[ModifiedDate])
		SELECT distinct AccountId, AddressId ,@UserId,@GetDate,@UserId,@GetDate  
		FROM ##insertAccount ZIC
		INNER JOIN Znodeaddress ZADD ON ZIC.DisplayName = ZADD.DisplayName  and ZIC.Address1 = ZADD.Address1 and ZIC.AccountStoreId = ZADD.Custom1
		INNER JOIN  Znodeaccount ZA ON ZIC.Name = ZA.Name 
		where NOT EXISTS (SELECT TOP 1 1 FROM [dbo].[ZnodeAccountAddress] RT WHERE RT.AccountId =  ZA.AccountId AND RT.AddressId = ZADD.AddressId);

		update ZAP
			set IsDefaultBilling = case when row_num =1 then 1 else 0 end
				from ZnodeAddress ZAP inner join (
				select ROW_NUMBER() OVER (PARTITION BY  accountid
				ORDER BY IsDefaultBilling desc, ZAA.AddressId
				) row_num  , AccountId,	ZAA.AddressId, IsDefaultBilling,DisplayName,Address1,ExternalId,custom1
				from ZnodeAccountAddress ZAA inner join ZnodeAddress ZA on ZA.AddressId =ZAA.AddressId) za on  ZA.DisplayName = ZAP.DisplayName AND ZA.Address1 = ZAP.Address1 AND isnull(ZA.ExternalId,0) = isnull(ZAP.ExternalId,0) AND ZA.custom1= ZAP.custom1	;


		update ZAP
			set IsDefaultShipping = case when row_num =1 then 1 else 0 end
				from ZnodeAddress ZAP inner join (
				select ROW_NUMBER() OVER (PARTITION BY  accountid
				ORDER BY IsDefaultShipping desc, ZAA.AddressId
				) row_num  , AccountId,	ZAA.AddressId, IsDefaultShipping,DisplayName,Address1,ExternalId,custom1
				from ZnodeAccountAddress ZAA inner join ZnodeAddress ZA on ZA.AddressId =ZAA.AddressId) za on  ZA.DisplayName = ZAP.DisplayName AND ZA.Address1 = ZAP.Address1 AND isnull(ZA.ExternalId,0) = isnull(ZAP.ExternalId,0) AND ZA.custom1= ZAP.custom1	;



		DECLARE @GlobalAttributeId INT = (SELECT TOP 1 GlobalAttributeId FROM ZnodeGlobalAttribute WHERE AttributeCode = 'SalesPersonID' )
		,@GlobalAttributeId1 INT = (SELECT TOP 1 GlobalAttributeId FROM ZnodeGlobalAttribute WHERE AttributeCode = 'BillTerm' )
		,@defaultLocaleId INT =  dbo.fn_getDefaultLocaleId()

		INSERT INTO ZnodeAccountGlobalAttributeValue(AccountId,GlobalAttributeId,GlobalAttributeDefaultValueId,AttributeValue,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
		SELECT ZA.AccountId,@GlobalAttributeId, NULL,NULL,@UserId,@GetDate,@UserId,@GetDate
		FROM Znodeaccount za 
		INNER JOIN ##insertAccount ZIC ON ( ZIC.Name = ZA.Name )
		WHERE NOT EXISTS (SELECT TOP 1 1 FROM ZnodeAccountGlobalAttributeValue RT WHERE RT.AccountId = ZA.AccountId AND RT.GlobalAttributeId = @GlobalAttributeId)
		
		INSERT INTO ZnodeAccountGlobalAttributeValueLocale(AccountGlobalAttributeValueId,LocaleId,AttributeValue,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate
								,GlobalAttributeDefaultValueId,MediaId,MediaPath) 
		SELECT DISTINCT  AccountGlobalAttributeValueId,@defaultLocaleId,ZIC.SalesPersonid,@UserId,@GetDate,@UserId,@GetDate,null,null,null
		FROM ZnodeAccountGlobalAttributeValue a 
		INNER JOIN Znodeaccount za ON (ZA.AccountId = a.AccountId AND a.GlobalAttributeId = @GlobalAttributeId )
		INNER JOIN ##insertAccount ZIC ON ( ZIC.Name = ZA.Name )
		WHERE NOT EXISTS (SELECT TOP 1 1 FROM ZnodeAccountGlobalAttributeValueLocale RT WHERE RT.AccountGlobalAttributeValueId = a.AccountGlobalAttributeValueId AND RT.LocaleId = @GlobalAttributeId)

		INSERT INTO ZnodeAccountGlobalAttributeValue(AccountId,GlobalAttributeId,GlobalAttributeDefaultValueId,AttributeValue,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
		SELECT DISTINCT ZA.AccountId,@GlobalAttributeId1, NULL,NULL,@UserId,@GetDate,@UserId,@GetDate
		FROM Znodeaccount za 
		INNER JOIN ##insertAccount ZIC ON ( ZIC.Name = ZA.Name)
		WHERE NOT EXISTS (SELECT TOP 1 1 FROM ZnodeAccountGlobalAttributeValue RT WHERE RT.AccountId = ZA.AccountId AND RT.GlobalAttributeId = @GlobalAttributeId1)
		
		INSERT INTO ZnodeAccountGlobalAttributeValueLocale(AccountGlobalAttributeValueId,LocaleId,AttributeValue,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate
								,GlobalAttributeDefaultValueId,MediaId,MediaPath) 
		SELECT DISTINCT  AccountGlobalAttributeValueId,@defaultLocaleId,BillTerm,@UserId,@GetDate,@UserId,@GetDate,null,null,null
		FROM ZnodeAccountGlobalAttributeValue a 
		INNER JOIN Znodeaccount za ON (ZA.AccountId = a.AccountId AND a.GlobalAttributeId = @GlobalAttributeId1 )
		INNER JOIN ##insertAccount ZIC ON ( ZIC.Name = ZA.Name )
		WHERE NOT EXISTS (SELECT TOP 1 1 FROM ZnodeAccountGlobalAttributeValueLocale RT WHERE RT.AccountGlobalAttributeValueId = a.AccountGlobalAttributeValueId AND RT.LocaleId = @GlobalAttributeId1);

		INSERT INTO [dbo].[ZnodeAccountProfile]
           ([AccountId] ,[ProfileId],[CreatedBy],[CreatedDate],[ModifiedBy] ,[ModifiedDate],[IsDefault])
			select Distinct AccountId ,ZP.ProfileId ,@UserId,@GetDate,@UserId,@GetDate  ,0
				from  Znodeaccount ZA 
				INNER JOIN ##insertAccount ZIC ON ZIC.Name = ZA.Name  
				inner join ZnodeProfile ZP on ZP.ProfileName = ZIC.ProfileId
				where    ISNULL(ZIC.ProfileID,'') != '' and NOT EXISTS (SELECT TOP 1 1 FROM [ZnodeAccountProfile] RT where RT.ProfileId = ZP.ProfileID and RT.AccountId = ZA.AccountId);

		update ZAP
		set isdefault = case when row_num =1 then 1 else 0 end
		from [ZnodeAccountProfile] ZAP inner join (select ROW_NUMBER() OVER (PARTITION BY  accountid
	ORDER BY isdefault desc, profileid
   ) row_num,* from [ZnodeAccountProfile]) za on za.accountid = ZAP.accountid and ZA.profileid= zap.profileid


		INSERT INTO [dbo].[ZnodePortalAccount]
           ([PortalId] ,[AccountId] ,[CreatedBy] ,[CreatedDate] ,[ModifiedBy] ,[ModifiedDate])
		   select distinct ZP.PortalId,AccountId ,@UserId,@GetDate,@UserId,@GetDate  
				from  Znodeaccount ZA 
				INNER JOIN ##insertAccount ZIC ON ZIC.Name = ZA.Name 
				inner join ZnodePortal ZP on ZP.StoreCode = ZIC.PortalId
				where  NOT EXISTS (SELECT TOP 1 1 FROM [ZnodePortalAccount] RT where RT.PortalId = ZP.PortalId and RT.AccountId = ZA.AccountId);
		
		
		


		
				UPDATE ZnodeImportProcessLog
				SET Status = dbo.Fn_GetImportStatus( 2 ), ProcessCompletedDate = @GetDate
				WHERE ImportProcessLogId = @ImportProcessLogId;
				
				SELECT @FailedRecordCount = COUNT(DISTINCT RowNumber) FROM ZnodeImportLog 
				WHERE RowNumber IS NOT NULL AND  ImportProcessLogId = @ImportProcessLogId;
				Select @SuccessRecordCount = 0
			
			if @FailedRecordCount >0
					UPDATE ZnodeImportProcessLog
				SET Status = dbo.Fn_GetImportStatus( 3 ), ProcessCompletedDate = @GetDate
				WHERE ImportProcessLogId = @ImportProcessLogId;
			

				

			COMMIT TRAN A;

	END TRY
	BEGIN CATCH
		ROLLBACK TRAN A;
		UPDATE ZnodeImportProcessLog
		  SET Status = dbo.Fn_GetImportStatus( 3 ), ProcessCompletedDate = @GetDate
		WHERE ImportProcessLogId = @ImportProcessLogId;

		SELECT @FailedRecordCount = COUNT(DISTINCT RowNumber) FROM ZnodeImportLog 
				WHERE RowNumber IS NOT NULL AND  ImportProcessLogId = @ImportProcessLogId;
				Select @SuccessRecordCount = 0

				UPDATE ZnodeImportProcessLog SET FailedRecordcount = @FailedRecordCount , SuccessRecordCount = @SuccessRecordCount , 
				TotalProcessedRecords = (ISNULL(@FailedRecordCount,0) + ISNULL(@SuccessRecordCount,0))
				WHERE ImportProcessLogId = @ImportProcessLogId;

		
		SET @Status = 0;
		SELECT ERROR_LINE(), ERROR_MESSAGE(), ERROR_PROCEDURE();
		
	END CATCH;
	
END
