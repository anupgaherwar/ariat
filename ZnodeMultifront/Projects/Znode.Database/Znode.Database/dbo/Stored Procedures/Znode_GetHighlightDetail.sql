﻿

CREATE PROCEDURE [dbo].[Znode_GetHighlightDetail]
	( @WhereClause nvarchar(max),
	  @Rows int= 10,
	  @PageNo int= 1,
	  @Order_BY varchar(1000)= '', 
	  @RowsCount int= 0 OUT,
	  @LocaleId int= 1,
      @IsAssociated bit= 0,
      @Isdebug bit= 0)
AS
/*
	 Summary :- This Procedure is used to get the highlights details 
	 Unit Testing 
	 begin tran
	 EXEC Znode_GetHighlightDetail '',10,1,'',0,1,0
	 rollback tran
	 
*/
BEGIN
	BEGIN TRY
		SET NOCOUNT ON;

		DECLARE @DefaultLocaleId int= dbo.Fn_GetDefaultLocaleId();
		DECLARE @SeoId varchar(max)= '', @SQL nvarchar(max);
		DECLARE @TBL_HighlightsDetails TABLE
		( 
			Description nvarchar(max), 
			HighlightId int,
			HighlightCode varchar(600),
			HighlightType NVARCHAR(400),
			DisplayOrder int,
			IsActive bit, 
			HighlightLocaleId int,
			MediaPath nvarchar(max), 
			MediaId int,
			Hyperlink nvarchar(max),
			ImageAltTag NVARCHAR(4000),DisplayPopup BIT
		);
		--Get default attributeid for ProductHighlights
		DECLARE @AttributeId int= [dbo].[Fn_GetProductHighlightsAttributeId]();
		DECLARE @TBL_AttributeDefault TABLE
		( 
			PimAttributeId int,
			AttributeDefaultValueCode varchar(600),
			IsEditable bit, 
			AttributeDefaultValue nvarchar(max),
			DisplayOrder INT
		);
	    DECLARE @TBL_HighlightsDetail TABLE
		( 
			 Description nvarchar(max),
			 HighlightId int, 
			 HighlightCode varchar(600),
			 HighlightType NVARCHAR(400),
			 DisplayOrder int,
			 IsActive bit,
			 HighlightLocaleId int, 
			 MediaPath nvarchar(max), 
			 MediaId int, 
			 Hyperlink nvarchar(max),
			 ImageAltTag NVARCHAR(4000),DisplayPopup BIT,
			 HighlightName nvarchar(max), 
			 RowId int, 
			 CountId int
		);

		
		INSERT INTO @TBL_AttributeDefault
		EXEC Znode_GetAttributeDefaultValueLocale @AttributeId, @LocaleId;

		SET @WhereClause = ' '+@WhereClause+CASE
											WHEN @IsAssociated = 1 THEN CASE
																		WHEN @WhereClause = '' THEN ' '
																		ELSE ' AND '
																		END+' EXISTS ( SELECT TOP 1 1 
																						FROM ZnodePimAttributeValue ZAV 
																						INNER JOIN ZnodePimAttribute ZA ON (ZA.PimAttributeId = ZAV.PimAttributeId AND ZA.AttributeCode = ''Highlights'') 
																				        INNER JOIN ZnodePimProductAttributeDefaultValue ZAVL ON (ZAV.PimAttributeValueId= ZAVL.PimAttributeValueId ) 
																		WHERE ( ZAVL.AttributeValue = TMADV.AttributeDefaultValueCode))'
											ELSE CASE
												 WHEN @WhereClause = '' THEN ' 1 = 1  '
												 ELSE ''
												 END
											END;


		WITH Cte_GetHighlightsBothLocale
			 AS (SELECT ZHL.Description, ZH.HighlightId, LocaleId, ZH.HighlightCode,ZPHT.Name HighlightType , ZH.DisplayOrder, ZH.IsActive, ZHL.HighlightLocaleId, [dbo].[Fn_GetMediaThumbnailMediaPath]( Zm.path ) AS MediaPath
							, ZH.MediaId, Hyperlink,ImageAltTag,DisplayPopup
				 FROM ZnodeHighlight AS ZH
					  LEFT JOIN  ZnodeHighlightLocale AS ZHL ON(ZHL.HighlightId = ZH.HighlightId)					 					  
					  LEFT JOIN ZnodeMedia AS ZM ON(ZM.MediaId = ZH.MediaId)
					  LEFT JOIN ZnodeHighLightType ZPHT ON (ZPHT.HighlightTypeId = ZH.HighlightTypeId)					  
				 WHERE LocaleId IN( @LocaleId, @DefaultLocaleId )					 
			    ),

			 Cte_HighlightsFirstLocale
			 AS (SELECT Description, HighlightId, LocaleId, HighlightCode,HighlightType, DisplayOrder, IsActive, HighlightLocaleId, MediaPath
			           , MediaId, Hyperlink,ImageAltTag,DisplayPopup
				 FROM Cte_GetHighlightsBothLocale AS CTGBBL
				 WHERE LocaleId = @LocaleId),

			 Cte_HighlightsDefaultLocale
			 AS (
			 SELECT Description, HighlightId, HighlightCode,HighlightType, DisplayOrder, IsActive, HighlightLocaleId, MediaPath
			            , MediaId, Hyperlink,ImageAltTag,DisplayPopup
			 FROM Cte_HighlightsFirstLocale
			 UNION ALL
			 SELECT Description, HighlightId, HighlightCode,HighlightType, DisplayOrder, IsActive, HighlightLocaleId, MediaPath
			             , MediaId, Hyperlink,ImageAltTag,DisplayPopup
			 FROM Cte_GetHighlightsBothLocale AS CTBBL
			 WHERE LocaleId = @DefaultLocaleId AND 
				   NOT EXISTS
			 (
				 SELECT TOP 1 1
				 FROM Cte_HighlightsFirstLocale AS CTBFL
				 WHERE CTBBL.HighlightId = CTBFL.HighlightId
			 ))


			 INSERT INTO @TBL_HighlightsDetails( Description, HighlightId, HighlightCode,HighlightType, DisplayOrder, IsActive, HighlightLocaleId, MediaPath, MediaId, Hyperlink,ImageAltTag,DisplayPopup )
					SELECT Description, HighlightId, HighlightCode,HighlightType, DisplayOrder, IsActive, HighlightLocaleId, MediaPath, MediaId, Hyperlink,ImageAltTag,DisplayPopup
					FROM Cte_HighlightsDefaultLocale AS CTEBD; 

		
		SELECT TBBD.*, TBAD.AttributeDefaultValue AS HighlightName, TBAD.AttributeDefaultValueCode
		INTO #TM_HighlightsLocale
		FROM @TBL_HighlightsDetails AS TBBD 
			 INNER JOIN @TBL_AttributeDefault AS TBAD  ON(TBAD.AttributeDefaultValueCode = TBBD.HighlightCode);
			 		
		SET @SQL = ' 
	            ;With Cte_HighlightsDetails AS 
				(
					SELECT * ,'+[dbo].[Fn_GetPagingRowId]( @Order_BY, 'HighlightId DESC' )+',Count(*)Over() CountId
					FROM #TM_HighlightsLocale TMADV
					WHERE 1=1
					'+[dbo].[Fn_GetFilterWhereClause]( @WhereClause )+'

			    )
				SELECT Description ,HighlightId , HighlightCode,HighlightType , DisplayOrder  ,IsActive   ,HighlightLocaleId 
										,MediaPath ,MediaId ,Hyperlink,ImageAltTag,DisplayPopup
									    ,HighlightName ,RowId  ,CountId 
				FROM Cte_HighlightsDetails
				'+[dbo].[Fn_GetOrderByClause]( @Order_BY, 'HighlightId DESC' )+' ';

		INSERT INTO @TBL_HighlightsDetail( Description, HighlightId, HighlightCode,HighlightType, DisplayOrder, IsActive, HighlightLocaleId, MediaPath, MediaId, Hyperlink,ImageAltTag,DisplayPopup, HighlightName, RowId, CountId )
		EXEC (@SQL);

		SET @RowsCount = ISNULL(( SELECT TOP 1 CountId FROM @TBL_HighlightsDetail), 0);

		SELECT HighlightId, Description, HighlightCode,HighlightType, DisplayOrder, IsActive, HighlightLocaleId, MediaPath, MediaId, Hyperlink,ImageAltTag,DisplayPopup, HighlightName
		FROM @TBL_HighlightsDetail;
	END TRY
	BEGIN CATCH
		 DECLARE @Status BIT ;
		     SET @Status = 0;
		     DECLARE @Error_procedure VARCHAR(1000)= ERROR_PROCEDURE(), @ErrorMessage NVARCHAR(MAX)= ERROR_MESSAGE(), @ErrorLine VARCHAR(100)= ERROR_LINE(), @ErrorCall NVARCHAR(MAX)= 'EXEC Znode_GetHighlightDetail @WhereClause = '+CAST(@WhereClause AS VARCHAR(max))+',@Rows='+CAST(@Rows AS VARCHAR(50))+',@PageNo='+CAST(@PageNo AS VARCHAR(50))+',@Order_BY='+@Order_BY+',@LocaleId = '+CAST(@LocaleId AS VARCHAR(50))+',@IsAssociated='+CAST(@IsAssociated AS VARCHAR(50))+',@RowsCount='+CAST(@RowsCount AS VARCHAR(50))+',@Status='+CAST(@Status AS VARCHAR(10));
              			 
             SELECT 0 AS ID,CAST(0 AS BIT) AS Status;                    
		  
             EXEC Znode_InsertProcedureErrorLog
				@ProcedureName = 'Znode_GetHighlightDetail',
				@ErrorInProcedure = @Error_procedure,
				@ErrorMessage = @ErrorMessage,
				@ErrorLine = @ErrorLine,
				@ErrorCall = @ErrorCall;
	END CATCH;
END;