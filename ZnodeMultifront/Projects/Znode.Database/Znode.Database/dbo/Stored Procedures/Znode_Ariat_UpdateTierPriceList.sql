﻿

CREATE PROCEDURE [dbo].[Znode_Ariat_UpdateTierPriceList]
	
AS
BEGIN
	BEGIN TRY  
	 SET NOCOUNT ON;
	-- Storing SKU, Listcode, price in a temp table of Master as it is been used later
		SELECT ZPL.[PriceListId]
      ,[ListCode]
      ,[SKU]
	  ,[RetailPrice]
	  ,[SalesPrice]
	  ,zp.[ActivationDate]
           ,ZP.[ExpirationDate]
	  into #TMPZP
  FROM [dbo].[ZnodePriceList] ZPL
  left join [dbo].[ZnodePrice] ZP 
  on Zp.PriceListId = ZPL.PriceListId
  where ListCode in ('MasterPrice');
 
 
  -- Upating the Tier retail price which already exits select * from #TMPZP

  Update Main
	set [RetailPrice] = ZM.RetailPrice,
	--case when ZT.ListCode ='Tier1' then ZM.RetailPrice *95/100  when ZT.ListCode ='Tier2' then ZM.RetailPrice *90/100 
	--when ZT.ListCode ='Tier3' then ZM.RetailPrice *85/100  when ZT.ListCode ='TierPreferred' then ZM.RetailPrice *80/100 end ,
	 [SalesPrice] = case when ZT.ListCode ='Tier1' then ZM.RetailPrice *95/100  when ZT.ListCode ='Tier2' then ZM.RetailPrice *90/100 
	when ZT.ListCode ='Tier3' then ZM.RetailPrice *85/100  when ZT.ListCode ='TierPreferred' then ZM.RetailPrice *80/100 end 
	, [ActivationDate] = ZM.[ActivationDate]
	, [ExpirationDate]=  ZM.[ExpirationDate]
	from [dbo].[ZnodePrice] Main 
	inner join [dbo].[ZnodePriceList] ZT
	on  Main.PriceListId = ZT.PriceListId
    inner Join  #TMPZP ZM on  ZM.SKU = Main.SKU 
	where ZT.ListCode in ('Tier1','Tier2','Tier3','TierPreferred')
    
  -- Inserting the Tier records which does not exits
  
	INSERT INTO [dbo].[ZnodePrice]
           ([PriceListId]
           ,[SKU]
           ,[SalesPrice]
           ,[RetailPrice]
           ,[UomId]
           ,[UnitSize]
           ,[ActivationDate]
           ,[ExpirationDate]
           ,[ExternalId]
           ,[CreatedBy]
           ,[CreatedDate]
           ,[ModifiedBy]
           ,[ModifiedDate]
           ,[CostPrice])
				(
					select Main.[PriceListId]
				   ,ZM.[SKU]
				   ,case when Main.ListCode ='Tier1' then ZM.RetailPrice *95/100  when Main.ListCode ='Tier2' then ZM.RetailPrice *90/100 
				   when Main.ListCode ='Tier3' then ZM.SalesPrice *85/100  when Main.ListCode ='TierPreferred' then ZM.RetailPrice *80/100 end  [SalesPrice]
				   --,case when Main.ListCode ='Tier1' then ZM.RetailPrice *95/100  when Main.ListCode ='Tier2' then ZM.RetailPrice *90/100 
				   --when Main.ListCode ='Tier3' then ZM.RetailPrice *85/100  when Main.ListCode ='TierPreferred' then ZM.RetailPrice *80/100 end  [RetailPrice]
				  ,ZM.RetailPrice 
				  ,1 as [UomId]
				  ,NULL [UnitSize]
				  ,ZM.[ActivationDate]
				  ,ZM.[ExpirationDate]
				  ,NULL [ExternalId]
				  ,MAIN.[CreatedBy]
				  ,getdate()
                  ,MAIN.[ModifiedBy]
                  ,getdate()
                  ,Null [CostPrice]

					from [dbo].[ZnodePriceList] Main 
					left Join  #TMPZP ZM on ZM.[ListCode] ='MasterPrice'
					 where Main.ListCode in ('Tier1','Tier2','Tier3','TierPreferred') and
					Main.PriceListId not in (select distinct PriceListId from [dbo].[ZnodePrice] zp where ZM.SKU = ZP.SKU ));
		-- Drop the Temp Table select * from [ZnodePriceList]  select * from [ZnodePrice] select * from #TMPZP
			DROP TABLE IF EXISTS #TMPZP ;

	END TRY 
	BEGIN CATCH  
		 -- statements that handle exception
		 Declare @status int;
		  ROLLBACK TRAN
       SET @Status = 0;  
       DECLARE @Error_procedure VARCHAR(1000)= ERROR_PROCEDURE(), @ErrorMessage NVARCHAR(MAX)= ERROR_MESSAGE(), @ErrorLine VARCHAR(100)= ERROR_LINE(),@ErrorCall NVARCHAR(MAX)= 'EXEC Znode_Ariat_UpdateTierPriceList';  
                    
		  SELECT 0 AS ID,CAST(0 AS BIT) AS Status;                      
      
		  EXEC Znode_InsertProcedureErrorLog  
		  @ProcedureName = 'Znode_Ariat_UpdateTierPriceList',  
		  @ErrorInProcedure = @Error_procedure,  
		  @ErrorMessage = @ErrorMessage,  
		  @ErrorLine = @ErrorLine,  
		  @ErrorCall = @ErrorCall;  
	END CATCH  
END
