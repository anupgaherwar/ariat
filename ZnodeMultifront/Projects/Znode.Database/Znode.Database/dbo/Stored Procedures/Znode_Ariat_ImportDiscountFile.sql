﻿
CREATE PROCEDURE [dbo].[Znode_Ariat_ImportDiscountFile](
	@TableName nvarchar(100), @Status bit OUT, @UserId int, @ImportProcessLogId int, @NewGUId nvarchar(200))
AS

BEGIN
	BEGIN TRAN A;
	BEGIN TRY
		DECLARE @MessageDisplay nvarchar(100), @MessageDisplayForFloat nvarchar(100);
		DECLARE @GetDate datetime= dbo.Fn_GetDate();
		
		IF OBJECT_ID('tempdb.dbo.#InsertDiscountFileForValidation', 'U') IS NOT NULL 
		DROP TABLE tempdb.dbo.#InsertDiscountFileForValidation
		
		IF OBJECT_ID('tempdb.dbo.#InsertDiscountFile', 'U') IS NOT NULL 
		DROP TABLE tempdb.dbo.#InsertDiscountFile

		--@MessageDisplay will use to display validate message for Discount File value  

		DECLARE @sSql nvarchar(max);
		Create TABLE tempdb..#InsertDiscountFileForValidation 
		( 
			RowNumber int, ExternalId varchar(max), PriceListCode varchar(200), GUID nvarchar(400)
		);
		CREATE TABLE tempdb..#InsertDiscountFile  
		( 
			InsertPriceListAccountId int IDENTITY(1, 1) PRIMARY KEY, RowNumber int, ExternalId varchar(max), PriceListCode varchar(200), GUID nvarchar(400)
		);
	
		SET @SSQL = 'Select RowNumber,ExternalId,PriceListCode,GUID FROM '+@TableName;
		INSERT INTO tempdb..#InsertDiscountFileForValidation( RowNumber, ExternalId, PriceListCode,GUID )
		EXEC sys.sp_sqlexec @SSQL;
		
		DELETE FROM tempdb..#InsertDiscountFileForValidation
		WHERE RowNumber IN
		(
			SELECT DISTINCT 
				   RowNumber
			FROM ZnodeImportLog
			WHERE ImportProcessLogId = @ImportProcessLogId AND 
				  GUID = @NewGUID
		);
	
		INSERT INTO tempdb..#InsertDiscountFile ( RowNumber, ExternalId, PriceListCode)
		SELECT RowNumber, ExternalId, PriceListCode
		FROM tempdb..#InsertDiscountFileForValidation;
		
		select * from tempdb..#InsertDiscountFile 			 
		-- start Functional Validation 
		-----------------------------------------------
		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
		SELECT '19', 'PriceListCode', PriceListCode, @NewGUId, RowNumber, 2, @GetDate, 2, @GetDate, @ImportProcessLogId
		FROM tempdb..#InsertDiscountFile  AS ii
		WHERE ii.PriceListCode NOT IN
		(
			SELECT ListCode FROM ZnodePriceList zpl
			WHERE zpl.ListCode = ii.PriceListCode
		);

		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
		SELECT '19', 'ExternalId', ExternalId, @NewGUId, RowNumber, 2, @GetDate, 2, @GetDate, @ImportProcessLogId
		FROM tempdb..#InsertDiscountFile  AS ii
		WHERE NOT EXISTS
		(
			SELECT TOP 1 1
			FROM ZnodeAccount AS za
			WHERE za.ExternalId = ii.ExternalId
		);

		UPDATE ZIL
		SET ZIL.ColumnName =   ZIL.ColumnName + ' [ ExternalId - ' + ISNULL(ExternalId,'') + ' ] '
		FROM ZnodeImportLog ZIL 
		INNER JOIN #InsertDiscountFile IPA ON (ZIL.RowNumber = IPA.RowNumber)
		WHERE  ZIL.ImportProcessLogId = @ImportProcessLogId AND ZIL.RowNumber IS NOT NULL


		-- End Function Validation 	
		-----------------------------------------------
		--- Delete Invalid Data after functional validatin  
		DELETE FROM tempdb..#InsertDiscountFile 
		WHERE RowNumber IN
		(
			SELECT DISTINCT 
				   RowNumber
			FROM ZnodeImportLog
			WHERE ImportProcessLogId = @ImportProcessLogId AND 
				  GUID = @NewGUID
		);
		
		DECLARE @TBL_ReadyToInsertDiscountFile TABLE
		( 
			RowNumber int,ExternalId varchar(max), PriceListCode varchar(200), PriceListId int,AccountId int,PriceListAccountId int
		);

		INSERT INTO @TBL_ReadyToInsertDiscountFile( RowNumber, ExternalId, PriceListCode,PriceListId,AccountId,PriceListAccountId)
		SELECT a.RowNumber, a.ExternalId, a.PriceListCode,c.PriceListId ,b.AccountId,Isnull( d.PriceListAccountId,0)
		FROM tempdb..#InsertDiscountFile  AS a
		inner join ZnodeAccount b on(a.ExternalId = b.ExternalId)
		inner join ZnodePriceList c on a.PriceListCode = c.ListCode
		left outer join ZnodePriceListAccount d on(d.PriceListId = c.PriceListId and d.AccountId= b.AccountId)

		-- Update Record count in log 
        DECLARE @FailedRecordCount BIGINT
		DECLARE @SuccessRecordCount BIGINT
		SELECT @FailedRecordCount = COUNT(DISTINCT RowNumber) FROM ZnodeImportLog WHERE RowNumber IS NOT NULL AND  ImportProcessLogId = @ImportProcessLogId;
		Select @SuccessRecordCount = count(DISTINCT RowNumber) FROM @TBL_ReadyToInsertDiscountFile
		UPDATE ZnodeImportProcessLog SET FailedRecordcount = @FailedRecordCount , SuccessRecordCount = @SuccessRecordCount,
		TotalProcessedRecords = (ISNULL(@FailedRecordCount,0) + ISNULL(@SuccessRecordCount,0)) 
		WHERE ImportProcessLogId = @ImportProcessLogId;
		-- End
		   	
		
		--select 'update started'  
		UPDATE zpla
		  SET PriceListId = rtii.PriceListId, AccountId = rtii.AccountId, ModifiedBy = @UserId, ModifiedDate = @GetDate
		FROM ZnodePriceListAccount zpla
			 INNER JOIN	 @TBL_ReadyToInsertDiscountFile rtii
			 ON(rtii.PriceListAccountId != 0 and zpla.PriceListAccountId =rtii.PriceListAccountId );
		--select 'update End'   
		             
		INSERT INTO ZnodePriceListAccount( PriceListId, AccountId,Precedence,CreatedBy, CreatedDate, ModifiedBy, ModifiedDate)
		SELECT PriceListId, AccountId,999, @UserId, @GetDate, @UserId, @GetDate
		FROM @TBL_ReadyToInsertDiscountFile AS rtii
		WHERE PriceListAccountId = 0

		--select 'End'
		--      SET @Status = 1;
		UPDATE ZnodeImportProcessLog
		  SET Status = dbo.Fn_GetImportStatus( 2 ), ProcessCompletedDate = GETDATE()
		WHERE ImportProcessLogId = @ImportProcessLogId;

		COMMIT TRAN A;
	END TRY
	BEGIN CATCH

		SET @Status = 0;
		SELECT ERROR_LINE(), ERROR_MESSAGE(), ERROR_PROCEDURE();
		ROLLBACK TRAN A;
	END CATCH;
END;