﻿
CREATE PROCEDURE [dbo].[Znode_GetPublishProductJson]
(
	 @PublishCatalogId INT = 0 
	,@PimProductId     TransferId Readonly
	,@UserId		   INT = 0	
	,@PimCatalogId     INT = 0 
	,@VersionIdString  VARCHAR(100)= ''
	,@Status		   Bit  OutPut
	,@RevisionState   Varchar(50) = ''
)
With RECOMPILE
AS
/*
DECLARE @rrte transferId 
INSERT INTO @rrte
select 1

EXEC [_POC_Znode_GetPublishProductbulk] @PublishCatalogId=9,@UserId= 2 ,@localeIDs = @rrte,@PublishStateId = 3 

*/
BEGIN
Begin Try 
	SET NOCOUNT ON;
	Set @Status = 0  
	Declare  @RevisionType VARCHAR(50) = '' 
	Declare @VersionId int = 0 
	DECLARE @PortalId INT = (SELECT TOP 1 POrtalId FROM ZnodePortalCatalog WHERE PublishCatalogId = @PublishCatalogId)
	DECLARE @PriceListId INT = (SELECT TOP 1 PriceListId FROM ZnodePriceListPortal WHERE PortalId = @PortalId )
	DECLARE @DomainUrl varchar(max) = (select TOp 1 URL FROM ZnodeMediaConfiguration WHERE IsActive =1)
	DECLARE @MaxSmallWidth INT  = (SELECT TOP 1  MAX(MaxSmallWidth) FROM ZnodePortalDisplaySetting WHERE PortalId = @PortalId)
	DECLARE @PimMediaAttributeId INT = dbo.Fn_GetProductImageAttributeId()

	DECLARE @TBL_LocaleId  TABLE (RowId INT IDENTITY(1,1) PRIMARY KEY  , LocaleId INT , VersionId int,RevisionType varchar(50)  )
	DECLARE @LocaleIds transferId 

    INSERT INTO @TBL_LocaleId (LocaleId,VersionId,RevisionType)
	SELECT PV.LocaleId , PV.VersionId , PV.RevisionType  FROM ZnodePublishVersionEntity PV Inner join Split(@VersionIdString,',') S ON PV.VersionId = S.Item
	
		


	Insert into @LocaleIds  
	SELECT  LocaleId
	FROM ZnodeLocale MT 
	WHERE IsActive = 1
	
	DECLARE --@ProductNamePimAttributeId INT = dbo.Fn_GetProductNameAttributeId(),
	@DefaultLocaleId INT= Dbo.Fn_GetDefaultLocaleId(),@LocaleId INT = 0 
		--,@SkuPimAttributeId  INT =  dbo.Fn_GetProductSKUAttributeId() , @IsActivePimAttributeId INT =  dbo.Fn_GetProductIsActiveAttributeId()
    DECLARE @GetDate DATETIME =dbo.Fn_GetDate()

    DECLARE @DefaultPortal int, @IsAllowIndexing int
    select @DefaultPortal = ZPC.PortalId, @IsAllowIndexing = 1 from ZnodePimCatalog ZPC Inner Join ZnodePublishCatalog ZPC1 ON ZPC.PimCatalogId = ZPC1.PimCatalogId where ZPC1.PublishCatalogId =  @PublishCatalogId and isnull(ZPC.IsAllowIndexing,0) = 1 
   
   -----delete unwanted publish data
	delete ZPC from ZnodePublishCategoryProduct ZPC
	where not exists(select * from ZnodePublishCategory ZC where ZPC.PublishCategoryId = ZC.PublishCategoryId )

	delete ZPP from ZnodePublishCategoryProduct ZPP
	where not exists(select * from ZnodePublishProduct ZP where ZPP.PublishProductId = ZP.PublishProductId )

	delete ZPP from ZnodePublishCatalogProductDetail ZPP
	where not exists(select * from ZnodePublishProduct ZP where ZPP.PublishProductId = ZP.PublishProductId )

	delete ZPCP from ZnodePublishCatalogProductDetail ZPCP
	inner join ZnodePublishProduct b on ZPCP.PublishProductId = b.PublishProductId 
	where not exists(select * from ZnodePimCategoryProduct a
	inner join ZnodePimCategoryHierarchy ZPCH on ZPCH.PimCategoryID = a.PimCategoryId 
	where b.PimProductId = A.PimProductId and ZPCP.PimCategoryHierarchyId = ZPCH.PimCategoryHierarchyId)
	and isnull(ZPCP.PimCategoryHierarchyId,0) <> 0 and b.PublishCatalogId = @PublishCatalogId
	---------


   DECLARE @Counter INT =1 ,@maxCountId INT = (SELECT max(RowId) FROM @TBL_LocaleId ) 

   CREATE TABLE #ZnodePrice (RetailPrice numeric(28,13),SalesPrice numeric(28,13),CurrencyCode varchar(100), CultureCode varchar(100), CurrencySuffix varchar(100), PublishProductId int)
	
   CREATE TABLE #ProductSKU (SEOUrl nvarchar(max), SEODescription  nvarchar(max),SEOKeywords  nvarchar(max),SEOTitle  nvarchar(max), PublishProductId int)

	create table #ProductImages (PublishProductId int, ImageSmallPath  varchar(max))

	EXEC Znode_InsertUpdateAttributeDefaultValueJson 1 
	EXEC Znode_InsertUpdateCustomeFieldJson 1 
	EXEC Znode_InsertUpdatePimAttributeJson 1 

	EXEC [Znode_InsertUpdatePimCatalogProductDetailJson] @PublishCatalogId=@PublishCatalogId,@LocaleId=@LocaleIds ,@UserId=@UserId
	
	--truncate table ZnodePublishedXml
	
	if (@IsAllowIndexing=1)
	begin 
		insert into #ZnodePrice
		SELECT RetailPrice,SalesPrice,ZC.CurrencyCode,ZCC.CultureCode ,ZCC.Symbol CurrencySuffix,TYU.PublishProductId
		FROM ZnodePrice ZP 
		INNER JOIN ZnodePriceList ZPL ON (ZPL.PriceListId = ZP.PriceListId)
		INNER JOIN ZnodeCurrency ZC oN (ZC.CurrencyId = ZPL.CurrencyId )
		INNER JOIN ZnodeCulture ZCC ON (ZCC.CultureId = ZPL.CultureId)
		INNER JOIN ZnodePublishProductDetail TY ON (TY.SKU = ZP.SKU ) 
		INNER JOIN ZnodePublishProduct TYU ON (TYU.PublishProductId = TY.PublishProductId) 
		WHERE ZP.PriceListId = @PriceListId 
		AND TY.LocaleId = @DefaultLocaleId
		AND TYU.PublishCatalogId = @PublishCatalogId
		AND EXISTS (SELECT TOP 1 1 FROM ZnodePriceListPortal ZPLP 
		INNER JOIN ZnodePimCatalog ZPC on ZPC.PortalId=ZPLP.PortalId WHERE ZPLP.PriceListId=ZP.PriceListId )
		AND EXISTS(select * from ZnodePimProduct ZPP1 where TYU.PimProductId = ZPP1.PimProductId )
	
		insert INTO #ProductImages
		SELECT  TYU.PublishProductId , @DomainUrl +'Catalog/'  + CAST(@DefaultPortal AS VARCHAr(100)) + '/'+ CAST(@MaxSmallWidth AS VARCHAR(100)) + '/' + RT.MediaPath AS ImageSmallPath   
		FROM ZnodePimAttributeValue ZPAV 
		INNER JOIN ZnodePublishProduct TYU ON (TYU.PimProductId  = ZPAV.PimProductId)
		INNER JOIN ZnodePimProductAttributeMedia  RT ON ( RT.PimAttributeValueId = ZPAV.PimAttributeValueId )
		WHERE  TYU.PublishCatalogId = @PublishCatalogId
		AND RT.LocaleId = @DefaultLocaleId
		AND ZPAV.PimAttributeId = (SELECT TOp 1 PimAttributeId FROM ZnodePimAttribute WHERE AttributeCode = 'ProductImage')
		AND EXISTS(select * from ZnodePimProduct ZPP1 where TYU.PimProductId = ZPP1.PimProductId )
	
		insert INTO #ProductSKU 
		SELECT ZCSD.SEOUrl , ZCDL.SEODescription,ZCDL.SEOKeywords ,ZCDL.SEOTitle, TYU.PublishProductId
		FROM ZnodeCMSSEODetail ZCSD 
		INNER JOIN ZnodeCMSSEODetailLocale ZCDL ON (ZCDL.CMSSEODetailId = ZCSD.CMSSEODetailId)
		INNER JOIN ZnodePublishProductDetail TY ON (TY.SKU = ZCSD.SEOCode AND ZCDL.LocaleId = TY.LocaleId) 
		INNER JOIN ZnodePublishProduct TYU ON (TYU.PublishProductId = TY.PublishProductId)
		WHERE CMSSEOTypeId = (SELECT TOP 1 CMSSEOTypeId FROM ZnodeCMSSEOType WHERE Name = 'Product') 
		AND ZCDL.LocaleId = @DefaultLocaleId
		AND TYU.PublishCatalogId = @PublishCatalogId
		--AND ZCSD.PublishStateId = @PublishStateId
		AND ZCSD.PortalId = @DefaultPortal
		AND EXISTS(select * from ZnodePimProduct ZPP1 where TYU.PimProductId = ZPP1.PimProductId )

	end
	
	CREATE NONCLUSTERED INDEX Idx_#ProductSKU_PublishProductId
	ON [dbo].[#ProductSKU] ([PublishProductId])
	CREATE NONCLUSTERED INDEX Idx_#ProductImages_PublishProductId
	ON [dbo].#ProductImages ([PublishProductId])
	CREATE NONCLUSTERED INDEX Idx_#ZnodePrice_PublishProductId
	ON [dbo].#ZnodePrice ([PublishProductId])

	SELECT ZPP.Pimproductid,ZPCPD.LocaleId,
	--'{"Attributes":[' +
	  '[' +
	(Select STUFF((SELECT ','+ Attributes from ZnodePublishProductAttributeJson a 
	where a.pimproductid = ZPP.pimproductid and a.LocaleId = ZPCPD.LocaleId 
	FOR XML Path ('')) ,1,1,'')  ) 
	+ ']'
	--']}' 
	ProductXML
	into #ProductAttributeXML
	FROM [ZnodePublishCatalogProductDetail] ZPCPD 
	INNER JOIN ZnodePublishProduct ZPP ON ZPCPD.PublishProductId = ZPP.PublishProductId and ZPCPD.PublishCatalogId = ZPP.PublishCatalogId --where TY.PimProductId = ZPP.PimProductId  AND TY.LocaleId = ZPCPD.LocaleId 
	WHERE ZPCPD.PublishCatalogId = @PublishCatalogId
	group by pimproductid,ZPCPD.LocaleId


	CREATE NONCLUSTERED INDEX Idx_#ProductAttributeXML_PimProductId_LocaleId
	ON [dbo].#ProductAttributeXML (PimProductId,LocaleId)

	DECLARE @MaxCount INT, @MinRow INT, @MaxRow INT, @Rows numeric(10,2);
			SELECT @MaxCount = COUNT(*) FROM [ZnodePublishCatalogProductDetail] WHERE PublishCatalogId = @PublishCatalogId;

			SELECT @Rows = 5000
        
			SELECT @MaxCount = CEILING(@MaxCount / @Rows);

			select PimCatalogProductDetailId, PublishProductId,Row_Number() over(Order by PublishProductId) ID into #ZnodePublishCatalogProductDetail from [ZnodePublishCatalogProductDetail] WHERE PublishCatalogId = @PublishCatalogId


		--CREATE NONCLUSTERED INDEX #ZnodePublishCatalogProductDetail

		 IF OBJECT_ID('tempdb..#Temp_ImportLoop') IS NOT NULL
             DROP TABLE #Temp_ImportLoop;
        
		 ---- To get the min and max rows for import in loop
		 ;WITH cte AS 
		 (
			SELECT RowId = 1, 
				   MinRow = 1, 
                   MaxRow = cast(@Rows as int)
            UNION ALL
            SELECT RowId + 1, 
                   MinRow + cast(@Rows as int), 
                   MaxRow + cast(@Rows as int)
            FROM cte
            WHERE RowId + 1 <= @MaxCount
		)
        SELECT RowId, MinRow, MaxRow
        INTO #Temp_ImportLoop
        FROM cte
		option (maxrecursion 0);
		



		DECLARE cur_BulkData CURSOR LOCAL FAST_FORWARD
        FOR SELECT MinRow, MaxRow, B.LocaleId ,B.VersionId, B.RevisionType
		FROM #Temp_ImportLoop L
		CROSS APPLY @TBL_LocaleId B;

        OPEN cur_BulkData;
        FETCH NEXT FROM cur_BulkData INTO  @MinRow, @MaxRow,@LocaleId,@VersionId, @RevisionType
		WHILE @@FETCH_STATUS = 0
        BEGIN
				INSERT INTO ZnodePublishProductEntity (
					VersionId, --1
					IndexId, --2 
					ZnodeProductId,ZnodeCatalogId, --3
					SKU,LocaleId, --4 
					Name,ZnodeCategoryIds, --5
					IsActive,Attributes,Brands,CategoryName, --6 
					CatalogName,DisplayOrder, --7 
					RevisionType,AssociatedProductDisplayOrder, --8
					ProductIndex,--9
					SalesPrice,RetailPrice,CultureCode,CurrencySuffix,CurrencyCode,SeoDescription,SeoKeywords,SeoTitle,SeoUrl,ImageSmallPath,SKULower)
		
				Select 
					CAST(@VersionId AS VARCHAR(50)) VersionId --1 
					,CAST(ZPCPD.ProductIndex AS VARCHAr(100)) + CAST(ISNULL(ZPCP.PublishCategoryId,'')  AS VARCHAR(50))  + CAST(Isnull(ZPCPD.PublishCatalogId ,'')  AS VARCHAR(50)) + CAST( Isnull(ZPCPD.LocaleId,'') AS VARCHAR(50)) IndexId  --2
					,CAST(ZPCPD.PublishProductId AS VARCHAR(50)) PublishProductId,CAST(ZPCPD.PublishCatalogId  AS VARCHAR(50)) PublishCatalogId  --3 
					,CAST(ISNULL(ZPCPD.SKU ,'') AS NVARCHAR(2000)) SKU,CAST( Isnull(ZPCPD.LocaleId,'') AS VARCHAR(50)) LocaleId -- 4 
					,CAST(isnull(ZPCPD.ProductName,'') AS NVARCHAR(2000) )  ProductName ,CAST(ISNULL(ZPCP.PublishCategoryId,'')  AS VARCHAR(50)) PublishCategoryId  -- 5 
					--'{"Attributes":[' + PAX.ProductXML + ']'
					,CAST(ISNULL(ZPCPD.IsActive ,'0') AS VARCHAR(50)) IsActive ,PAX.ProductXML,'[]' Brands,CAST(isnull(ZPCPD.CategoryName,'') AS NVARCHAR(2000)) CategoryName  --6
					,CAST(Isnull(ZPCPD.CatalogName,'')  AS NVARCHAR(2000)) CatalogName,CAST(ISNULL(ZPCCF.DisplayOrder,'') AS VARCHAR(50)) DisplayOrder  -- 7 
					,@RevisionType , 0 AssociatedProductDisplayOrder,-- pending  -- 8 
					ZPCPD.ProductIndex,  -- 9
					Case When @IsAllowIndexing = 1 then  ISNULL(CAST(TBZP.SalesPrice  AS VARCHAr(500)),'') else '' end SalesPrice , 
					Case When @IsAllowIndexing = 1 then  ISNULL(CAST(TBZP.RetailPrice  AS VARCHAr(500)),'') else '' end RetailPrice , 
					Case When @IsAllowIndexing = 1 then  ISNULL(TBZP.CultureCode ,'') else '' end CultureCode , 
					Case When @IsAllowIndexing = 1 then  ISNULL(TBZP.CurrencySuffix ,'') else '' end CurrencySuffix , 
					Case When @IsAllowIndexing = 1 then  ISNULL(TBZP.CurrencyCode ,'') else '' end CurrencyCode , 
					Case When @IsAllowIndexing = 1 then  ISNULL(TBPS.SEODescription,'') else '' end SEODescriptionForIndex,
					Case When @IsAllowIndexing = 1 then  ISNULL(TBPS.SEOKeywords,'') else '' end SEOKeywords,
					Case When @IsAllowIndexing = 1 then  ISNULL(SEOTitle,'') else '' end SEOTitle,
					Case When @IsAllowIndexing = 1 then  ISNULL(TBPS.SEOUrl ,'') else '' end SEOUrl,
					Case When @IsAllowIndexing = 1 then  ISNULL(TBPI.ImageSmallPath,'') else '' end ImageSmallPath,
					CAST(ISNULL(LOWER(ZPCPD.SKU) ,'') AS NVARCHAR(2000)) Lower_SKU
			
					FROM [ZnodePublishCatalogProductDetail] ZPCPD
					INNER JOIN ZnodePublishCatalog ZPCV ON (ZPCV.PublishCatalogId = ZPCPD.PublishCatalogId)
					INNER JOIN ZnodePublishProduct ZPP ON ZPCPD.PublishProductId = ZPP.PublishProductId and ZPCPD.PublishCatalogId = ZPP.PublishCatalogId
					INNER JOIN #ProductAttributeXML PAX ON PAX.PimProductId = ZPP.PimProductId  AND PAX.LocaleId = ZPCPD.LocaleId 
					INNER JOIN #ZnodePublishCatalogProductDetail z on ZPCPD.PimCatalogProductDetailId = z.PimCatalogProductDetailId
					LEFT JOIN  #ZnodePrice TBZP ON (TBZP.PublishProductId = ZPCPD.PublishProductId)
					LEFT JOIN  #ProductSKU TBPS ON (TBPS.PublishProductId = ZPCPD.PublishProductId)
					LEFT JOIN  #ProductImages TBPI ON (TBPI.PublishProductId = ZPCPD.PublishProductId)
					LEFT JOIN  ZnodePublishCategoryProduct ZPCP ON (ZPCP.PublishProductId = ZPCPD.PublishProductId AND ZPCP.PublishCatalogId = ZPCPD.PublishCatalogId AND ZPCP.PimCategoryHierarchyId = ZPCPD.PimCategoryHierarchyId)
					LEFT JOIN  ZnodePublishCategory ZPC ON (ZPCP.PublishCatalogId = ZPC.PublishCatalogId AND   ZPC.PublishCategoryId = ZPCP.PublishCategoryId AND ZPCP.PimCategoryHierarchyId = ZPC.PimCategoryHierarchyId)
					LEFT JOIN  ZnodePimCategoryProduct ZPCCF ON (ZPCCF.PimCategoryId = ZPC.PimCategoryId  AND ZPCCF.PimProductId = ZPP.PimProductId )
					LEFT JOIN  ZnodePimCategoryHierarchy ZPCH ON (ZPCH.PimCatalogId = ZPCV.PimCatalogId AND  ZPCH.PimCategoryHierarchyId =  ZPC.PimCategoryHierarchyId)
					WHERE      ZPCPD.LocaleId = @LocaleId and z.Id BETWEEN @MinRow AND @MaxRow
					AND ZPCPD.PublishCatalogId = @PublishCatalogId
					SET @VersionId = 0
			FETCH NEXT FROM cur_BulkData INTO  @MinRow, @MaxRow,@LocaleId,@VersionId, @RevisionType
        END;
		CLOSE cur_BulkData;
		DEALLOCATE cur_BulkData;


		If @RevisionState = 'PREVIEW' 
			UPDATE ZnodePimProduct SET IsProductPublish = 1,PublishStateId =  DBO.Fn_GetPublishStateIdForPreview()  
			WHERE EXISTS (SELECT TOP 1 1 FROM ZnodePublishProduct ZPP WHERE ZPP.PimProductId = ZnodePimProduct.PimProductId 
			AND ZPP.PublishCatalogId = @PublishCatalogId)
		Else
			UPDATE ZnodePimProduct SET IsProductPublish = 1,PublishStateId =  DBO.Fn_GetPublishStateIdForPublish()  
			WHERE EXISTS (SELECT TOP 1 1 FROM ZnodePublishProduct ZPP WHERE ZPP.PimProductId = ZnodePimProduct.PimProductId 
			AND ZPP.PublishCatalogId = @PublishCatalogId)
		UPDATE ZnodePublishCatalogLog 
		SET IsProductPublished = 1 
		,PublishProductId = (SELECT  COUNT(DISTINCT PublishProductId) FROM ZnodePublishCategoryProduct ZPP 
		WHERE ZPP.PublishCatalogId = ZnodePublishCatalogLog.PublishCatalogId AND ZPP.PublishCategoryId IS NOT NULL) 
		WHERE EXISTS (SELECT TOP 1 1 FROM @TBL_LocaleId TY  WHERE  TY.VersionId =ZnodePublishCatalogLog.PublishCatalogLogId )  


		SET @Status = 1 
END TRY 
BEGIN CATCH 
	SET @Status =0  
	DECLARE @Error_procedure VARCHAR(1000)= ERROR_PROCEDURE(), @ErrorMessage NVARCHAR(MAX)= ERROR_MESSAGE(), 
		@ErrorLine VARCHAR(100)= ERROR_LINE(),
		@ErrorCall NVARCHAR(MAX)= 'EXEC Znode_GetPublishProductJson 
		   @PublishCatalogId = '+CAST(@PublishCatalogId AS VARCHAR	(max))+',@UserId='+CAST(@UserId AS VARCHAR(50))
		+',@PimCatalogId = ' + CAST(@PimCatalogId AS varchar(20))
		+',@VersionIdString= ' + CAST(@VersionIdString AS varchar(20))
		  	
	EXEC Znode_InsertProcedureErrorLog
		@ProcedureName = 'Znode_GetPublishProductJson',
		@ErrorInProcedure = @Error_procedure,
		@ErrorMessage = @ErrorMessage,
		@ErrorLine = @ErrorLine,
		@ErrorCall = @ErrorCall;
END CATCH

END