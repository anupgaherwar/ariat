﻿CREATE PROCEDURE [dbo].[Znode_GetQuoteList]
( 
	
	@WhereClause NVARCHAR(MAx) = '',
	@Rows        INT            = 100,
    @PageNo      INT            = 1,
    @Order_BY    VARCHAR(1000)  = '',
    @RowsCount   INT OUT			,
    @UserId	   INT = 0,
	@OmsQuoteTypeId int
)
AS
  /*
     Summary : This procedure is used to get the oms order detils
			   Records are fetched for those users who placed the order i.e UserId is Present in ZnodeUser and  ZnodeOmsOrderDetails tables
	 Unit Testing:

     EXEC Znode_GetQuoteList @Order_BY = '',@RowsCount= 0, @UserId = 0 ,@Rows = 80, @PageNo = 1, @OmsQuoteTypeId=1

*/
BEGIN
SET NOCOUNT ON;
BEGIN TRY
	DECLARE @SQL nvarchar(max) = ''

	Create Table #QuoteInfo(OmsQuoteId int, QuoteNumber varchar(200),UserID int, CustomerName varchar(300), EmailID varchar(50), PhoneNumber varchar(50), PortalID int, StoreName varchar(500), QuoteStatus varchar(500),  TotalAmount numeric(28,6),QuoteDate datetime,QuoteExpirationDate  DateTime,CultureCode varchar(100),RowId int,CountNo int)

	SELECT ZU.UserId, ZU.FirstName, ZU.MiddleName, ZU.LastName, ZU.Email , ZU.PhoneNumber
	INTO #User
	FROM ZnodeUser ZU 
	WHERE EXISTS(SELECT * FROM ZnodeOmsQuote ZOQ where ZU.UserId = ZOQ.UserID )

	Update ZOQ set OmsOrderStateId = (select top 1 OmsOrderStateId from ZnodeOMSOrderState where OrderStateName = 'EXPIRED')
	from ZnodeOmsQuote ZOQ
	Inner Join ZnodeOmsQuoteType ZOQT ON ZOQ.OmsQuoteTypeId = ZOQT.OmsQuoteTypeId
	INNER JOIN #User U ON ZOQ.UserId = U.UserId 
	INNER JOIN ZnodePortal ZP ON ZOQ.PortalID = ZP.PortalID
	INNER JOIN ZnodeOMSOrderState ZOOS ON ZOOS.OmsOrderStateId = ZOQ.OmsOrderStateId
	where ZOQ.OmsQuoteTypeId = @OmsQuoteTypeId AND (ZOQ.UserId = @UserId OR @UserId = 0 )
	and cast(ZOQ.QuoteExpirationDate as date) < cast(GETDATE() as date)
	and ZOQ.OmsOrderStateId <> (select top 1 OmsOrderStateId from ZnodeOMSOrderState where OrderStateName = 'EXPIRED')

	Select ZOQ.OmsQuoteId, ZOQ.OmsQuoteTypeId,U.UserID, ZOQ.QuoteNumber as QuoteNumber,isnull(U.FirstName,'')+case when U.MiddleName is not null then ' ' else '' end+ isnull(U.MiddleName,'')+' '+isnull(U.LastName,'') as CustomerName,
	U.Email as EmailID ,U.PhoneNumber,ZP.PortalID,ZP.StoreName ,ZOQ.CreatedDate as QuoteDate,ZOOS.Description as QuoteStatus,ZOQ.QuoteOrderTotal as TotalAmount, ZOQ.QuoteExpirationDate , ZOQ.CultureCode
	into #QuoteDetail
	from ZnodeOmsQuote ZOQ
	Inner Join ZnodeOmsQuoteType ZOQT ON ZOQ.OmsQuoteTypeId = ZOQT.OmsQuoteTypeId
	INNER JOIN #User U ON ZOQ.UserId = U.UserId 
	INNER JOIN ZnodePortal ZP ON ZOQ.PortalID = ZP.PortalID
	INNER JOIN ZnodeOMSOrderState ZOOS ON ZOOS.OmsOrderStateId = ZOQ.OmsOrderStateId
	where ZOQ.OmsQuoteTypeId = @OmsQuoteTypeId AND (ZOQ.UserId = @UserId OR @UserId = 0 )

	set @SQL = '
	;with cte_Quote_Detail as
	(
		select OmsQuoteId, QuoteNumber, UserID, CustomerName, EmailID, PhoneNumber, PortalID, StoreName, QuoteStatus, QuoteDate,
			   TotalAmount,QuoteExpirationDate, CultureCode , '+dbo.Fn_GetPagingRowId(@Order_BY,'OmsQuoteId DESC')+',Count(*)Over() CountNo
		from #QuoteDetail
		WHERE 1= 1 '+dbo.Fn_GetFilterWhereClause(@WhereClause)+'
	)
	select OmsQuoteId, QuoteNumber, CustomerName, EmailID, PhoneNumber, PortalID, StoreName, QuoteStatus, QuoteDate,
			   TotalAmount,QuoteExpirationDate, CultureCode , RowId,CountNo
	from cte_Quote_Detail
	'+dbo.Fn_GetPaginationWhereClause(@PageNo,@Rows)

	--print @SQL
	insert into #QuoteInfo(OmsQuoteId, QuoteNumber,  CustomerName, EmailID, PhoneNumber, PortalID, StoreName, QuoteStatus, QuoteDate, TotalAmount, QuoteExpirationDate, CultureCode , RowId,CountNo)
	EXEC(@SQL)

	select OmsQuoteId, QuoteNumber, CustomerName, EmailID, PhoneNumber, StoreName, QuoteStatus, QuoteDate, TotalAmount, QuoteExpirationDate, CultureCode
	from #QuoteInfo
	Order by RowId

	SET @RowsCount = ISNULL((SELECT TOP 1 CountNo FROM #QuoteInfo),0)

	IF OBJECT_ID('tempdb..#QuoteDetail') is not null
		DROP TABLE #QuoteDetail
	IF OBJECT_ID('tempdb..#QuoteInfo') is not null
		DROP TABLE #QuoteInfo
	IF OBJECT_ID('tempdb..#User') is not null
		DROP TABLE #User

END TRY
BEGIN CATCH
    DECLARE @Status BIT ;
	SET @Status = 0;
	DECLARE @Error_procedure VARCHAR(1000)= ERROR_PROCEDURE(), @ErrorMessage NVARCHAR(MAX)= ERROR_MESSAGE(), @ErrorLine VARCHAR(100)= ERROR_LINE(),
	@ErrorCall NVARCHAR(MAX)= 'EXEC Znode_GetQuoteList @WhereClause = '''+ISNULL(CAST(@WhereClause AS VARCHAR(max)),'''''')+''',@Rows='''+ISNULL(CAST(@Rows AS VARCHAR(50)),'''''')+''',@PageNo='+ISNULL(CAST(@PageNo AS VARCHAR(50)),'''')+',
	@Order_BY='+ISNULL(@Order_BY,'''''')+',@UserId = '+ISNULL(CAST(@UserId AS VARCHAR(50)),'''')+',@RowsCount='+ISNULL(CAST(@RowsCount AS VARCHAR(50)),'''');
              			 
    SELECT 0 AS ID,CAST(0 AS BIT) AS Status;                    
		  
    EXEC Znode_InsertProcedureErrorLog
	@ProcedureName = 'Znode_GetQuoteList',
	@ErrorInProcedure = 'Znode_GetQuoteList',
	@ErrorMessage = @ErrorMessage,
	@ErrorLine = @ErrorLine,
	@ErrorCall = @ErrorCall;
END CATCH;
END