﻿CREATE PROCEDURE [dbo].[Znode_GetGiftCardHistoryList]  
(   @WhereClause NVARCHAR(max),  
    @Rows        INT            = 100,  
    @PageNo      INT            = 1,  
    @Order_BY    VARCHAR(1000)  = '',  
    @RowsCount   INT  out 
  )    
AS   
/*   
    Summary: This procedure is used to find the GiftCardhistoryList 

     EXEC Znode_GetGiftCardHistoryList @WhereClause='' ,@PortalId ='1',  @RowsCount= 0,@ExpirationDate = ''  
*/  
  
     BEGIN  
         BEGIN TRY  
             SET NOCOUNT ON;  
             DECLARE @SQL NVARCHAR(MAX);
			 
			 DECLARE @TBL_GiftCardHistoryList TABLE (OmsOrderId INT,OrderNumber NVARCHAR(600),TransactionAmount NUMERIC(28,6),TransactionDate DATETIME,UserName NVARCHAR(512),CustomerName NVARCHAR(512),PortalId INT,UserId INT,OmsUserId INT,GiftCardId INT,CultureCode VARCHAR(100), RowId INT, CountNo INT )  
			   
    SET @SQL ='  
 
      ;WITH CTE_GetGiftHistoryCard AS  
      (  
      SELECT  ZOD.OmsOrderId AS OmsOrderId ,ZO.OrderNumber,ZGH.TransactionAmount,ZGH.TransactionDate,ZU.Email As UserName, 
         CASE WHEN ZU.FirstName IS NULL THEN '''' ELSE ZU.FirstName END + CASE WHEN ZU.LastName IS NULL  THEN '''' ELSE '' ''+ZU.LastName END as CustomerName,ZGC.PortalId,ZGC.UserId,ZOD.UserId as OmsUserId,ZGC.GiftCardId,zc.CultureCode,ZGC.IsActive,ZGC.RemainingAmount
      FROM ZnodeGiftCard ZGC   
      INNER JOIN ZnodeGiftCardHistory ZGH ON (ZGC.GiftCardId = ZGH.GiftCardId)  
      INNER JOIN ZnodeOmsOrderDetails ZOD on (ZGH.OmsOrderDetailsId = ZOD.OmsOrderDetailsId)
	  INNER JOIN ZnodePortal ZP ON (ZGC.PortalId = ZP.PortalId)  
      INNER JOIN ZnodePortalUnit zpu on (zp.PortalId = zpu.PortalId)  
      LEFT JOIN ZnodeCulture zc on (zc.CultureId = zpu.CultureId)    
      LEFT JOIN ZnodeOmsOrder ZO on (ZOD.OmsOrderId = ZO.OmsOrderId)  
      LEFT JOIN ZnodeUser ZU ON (ZU.UserId = ZOD.UserId) 	
	   		
      )  
      , CTE_GetGiftCardHistoryList AS  
      (  
      SELECT  OmsOrderId ,OrderNumber,TransactionAmount,TransactionDate,UserName,CustomerName,PortalId,UserId,OmsUserId,GiftCardId,CultureCode,IsActive,RemainingAmount,
      '+dbo.Fn_GetPagingRowId(@Order_BY,'GiftCardId DESC')+',Count(*)Over() CountNo   
      FROM CTE_GetGiftHistoryCard  
      WHERE 1=1 '+dbo.Fn_GetFilterWhereClause(@WhereClause)+'       
      )  
  
      SELECT  OmsOrderId ,OrderNumber,TransactionAmount,TransactionDate,UserName,CustomerName,PortalId,UserId,OmsUserId,GiftCardId,CultureCode,RowId,CountNo 
      FROM CTE_GetGiftCardHistoryList  
      '+dbo.Fn_GetPaginationWhereClause(@PageNo,@Rows)  
       
     
   INSERT INTO @TBL_GiftCardHistoryList  
   EXEC(@SQL)  
  
   SET @RowsCount =ISNULL((SELECT TOP 1 CountNo FROM @TBL_GiftCardHistoryList ),0)  
     
   SELECT  OmsOrderId ,OrderNumber,TransactionAmount,TransactionDate,UserName,CustomerName,PortalId,UserId,OmsUserId,GiftCardId,CultureCode
   FROM @TBL_GiftCardHistoryList  
  
      END TRY  
   BEGIN CATCH  
    DECLARE @Status BIT ;  
       SET @Status = 0;  
       DECLARE @Error_procedure VARCHAR(1000)= ERROR_PROCEDURE(), @ErrorMessage NVARCHAR(MAX)= ERROR_MESSAGE(), @ErrorLine VARCHAR(100)= ERROR_LINE(),  
    @ErrorCall NVARCHAR(MAX)= 'EXEC Znode_GetGiftCardHIstoryList @WhereClause = '+CAST(@WhereClause AS VARCHAR(MAX))+',@Rows='+CAST(@Rows AS VARCHAR(50))+',@PageNo='+CAST(@PageNo AS VARCHAR(50))+',@Order_BY='+@Order_BY+',@RowsCount='+CAST(@RowsCount AS VARCHAR(50))+'@Status='+CAST(@Status AS VARCHAR(10));  
                    
             SELECT 0 AS ID,CAST(0 AS BIT) AS Status;                      
      
             EXEC Znode_InsertProcedureErrorLog  
    @ProcedureName = 'Znode_GetGiftCardHistoryList',  
    @ErrorInProcedure = @Error_procedure,  
    @ErrorMessage = @ErrorMessage,  
    @ErrorLine = @ErrorLine,  
    @ErrorCall = @ErrorCall;  
   END CATCH  
     END