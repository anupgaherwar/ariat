﻿CREATE PROCEDURE [dbo].[Znode_ImportVoucher](
	  @TableName nvarchar(100), @Status bit OUT, @UserId int, @ImportProcessLogId int, @NewGUId nvarchar(200), @PimCatalogId int= 0)
AS
	--------------------------------------------------------------------------------------
	-- Summary :  Import Attribute Code Name and their default input validation rule other 
	--			  flag will be inserted as default we need to modify front end
	
	-- Unit Testing: 

	--------------------------------------------------------------------------------------
BEGIN
	BEGIN TRAN Voucher;
	BEGIN TRY
		DECLARE @MessageDisplay nvarchar(100), @SSQL nvarchar(max);
		DECLARE @GetDate datetime= dbo.Fn_GetDate(), @LocaleId int  ;
		SELECT @LocaleId = DBO.Fn_GetDefaultLocaleId();
		-- Retrive RoundOff Value from global setting 

		DECLARE @InsertVoucherData TABLE
		( 
			RowId int IDENTITY(1, 1) PRIMARY KEY,RowNumber int, StoreCode varchar(400),VoucherName varchar(600), VoucherNumber varchar(600),
			VoucherAmount numeric(28,6),UserName varchar(600), ExpirationDate Datetime,IsActive varchar(100),RemainingAmount numeric(28,6),
			RestrictVoucherToACustomer varchar(100), StartDate datetime, GUID nvarchar(400)
		);
		
		SET @SSQL = 'Select RowNumber,StoreCode, VoucherName,VoucherNumber,VoucherAmount,UserName,ExpirationDate,
						IsActive,RemainingAmount,RestrictVoucherToACustomer,StartDate,GUID FROM '+@TableName;
		INSERT INTO @InsertVoucherData( RowNumber,StoreCode, VoucherName,VoucherNumber,VoucherAmount,UserName,ExpirationDate,
										IsActive,RemainingAmount,RestrictVoucherToACustomer,StartDate,GUID)
		EXEC sys.sp_sqlexec @SSQL;

		select ANZU.UserName, ANU.Id, ZU.UserId
		into #TempUserData
		from AspNetZnodeUser ANZU 
		inner join AspNetUsers ANU ON ANZU.AspNetZnodeUserId = ANU.UserName
		inner join ZnodeUser ZU ON ANU.Id = ZU.AspNetUserId

		update @InsertVoucherData set VoucherNumber = [dbo].[Fn_RandomString](10)
		where isnull(ltrim(rtrim(VoucherNumber)),'') = ''

		-- Start Functional Validation 
		-----------------------------------------------
		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
			   SELECT '70', 'StoreCode', StoreCode, @NewGUId, RowNumber, 2, @GetDate, 2, @GetDate, @ImportProcessLogId
			   FROM @InsertVoucherData AS ii
			   WHERE ii.StoreCode not in 
			   (
				   SELECT StoreCode FROM ZnodePortal 
			   );

		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
			   SELECT '37', 'ExpirationDate', ExpirationDate, @NewGUId, RowNumber, 2, @GetDate, 2, @GetDate, @ImportProcessLogId
			   FROM @InsertVoucherData AS ii
			   WHERE ii.ExpirationDate < ii.StartDate

		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
			   SELECT '65', 'ExpirationDate', ExpirationDate, @NewGUId, RowNumber, 2, @GetDate, 2, @GetDate, @ImportProcessLogId
			   FROM @InsertVoucherData AS ii
			   WHERE ii.ExpirationDate < @GetDate

		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
			   SELECT '37', 'StartDate', StartDate, @NewGUId, RowNumber, 2, @GetDate, 2, @GetDate, @ImportProcessLogId
			   FROM @InsertVoucherData AS ii
			   WHERE (ii.StartDate > ii.ExpirationDate )


		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
			   SELECT '8', 'StartDate', StartDate, @NewGUId, RowNumber, 2, @GetDate, 2, @GetDate, @ImportProcessLogId
			   FROM @InsertVoucherData AS ii
			   WHERE isnull(ii.StartDate,'') = ''

		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
			   SELECT '8', 'ExpirationDate', StartDate, @NewGUId, RowNumber, 2, @GetDate, 2, @GetDate, @ImportProcessLogId
			   FROM @InsertVoucherData AS ii
			   WHERE isnull(ii.ExpirationDate,'') = ''
		
		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
			   SELECT '67', 'UserName', UserName, @NewGUId, RowNumber, 2, @GetDate, 2, @GetDate, @ImportProcessLogId
			   FROM @InsertVoucherData AS ii
			   WHERE isnull(ii.UserName,'') <> '' and not exists ( SELECT VoucherNumber FROM #TempUserData U where ii.UserName = U.UserName);

		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
			   SELECT '71', 'VoucherNumber', VoucherNumber, @NewGUId, RowNumber, 2, @GetDate, 2, @GetDate, @ImportProcessLogId
			   FROM @InsertVoucherData AS ii
			   WHERE len(ltrim(rtrim(ii.VoucherNumber))) <> 10 
	
		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )  
			SELECT '68', 'IsActive', IsActive, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId  
			FROM @InsertVoucherData AS ii  
			WHERE ii.IsActive not in ('True','1','Yes','FALSE','0','No','')
		
		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )  
			SELECT '5', 'ExpirationDate', ExpirationDate, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId  
			FROM @InsertVoucherData AS ii  
			WHERE isdate(ii.ExpirationDate) = 0

		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )  
			SELECT '5', 'StartDate', StartDate, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId  
			FROM @InsertVoucherData AS ii  
			WHERE isdate(ii.StartDate) = 0

		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )  
			SELECT '69', 'RemainingAmount', RemainingAmount, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId  
			FROM @InsertVoucherData AS ii  
			WHERE ii.VoucherAmount <> ii.RemainingAmount

		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )  
			SELECT '68', 'RestrictVoucherToACustomer', RestrictVoucherToACustomer, @NewGUId, RowNumber, @UserId, @GetDate, @UserId, @GetDate, @ImportProcessLogId  
			FROM @InsertVoucherData AS ii  
			WHERE ii.RestrictVoucherToACustomer not in ('True','1','Yes','FALSE','0','No','')

		UPDATE ZIL
			   SET ZIL.ColumnName =   ZIL.ColumnName + ' [ VoucherName - ' + ISNULL(VoucherName,'') + ' ] '
			   FROM ZnodeImportLog ZIL 
			   INNER JOIN @InsertVoucherData IPA ON (ZIL.RowNumber = IPA.RowNumber)
			   WHERE  ZIL.ImportProcessLogId = @ImportProcessLogId AND ZIL.RowNumber IS NOT NULL

		-- End Function Validation 	
		-----------------------------------------------
		-- Delete Invalid Data after functional validatin  
		DELETE FROM @InsertVoucherData
		WHERE RowNumber IN
		(
			SELECT DISTINCT 
				   RowNumber
			FROM ZnodeImportLog
			WHERE ImportProcessLogId = @ImportProcessLogId  and RowNumber is not null 
		);
		
		-- Update Record count in log 
        DECLARE @FailedRecordCount BIGINT
		DECLARE @SuccessRecordCount BIGINT
		SELECT @FailedRecordCount = COUNT(DISTINCT RowNumber) FROM ZnodeImportLog WHERE RowNumber IS NOT NULL AND  ImportProcessLogId = @ImportProcessLogId;
		Select @SuccessRecordCount = count(DISTINCT RowNumber) FROM @InsertVoucherData
		UPDATE ZnodeImportProcessLog SET FailedRecordcount = @FailedRecordCount , SuccessRecordCount = @SuccessRecordCount ,
		TotalProcessedRecords = (ISNULL(@FailedRecordCount,0) + ISNULL(@SuccessRecordCount,0)) 
		WHERE ImportProcessLogId = @ImportProcessLogId;
	
		UPDATE ZGC set ExpirationDate = ICD.ExpirationDate, UserId = @UserId, ModifiedBy = @UserId, ModifiedDate = @GetDate, IsActive = ICD.IsActive,
				RemainingAmount = ICD.RemainingAmount, RestrictToCustomerAccount = ICD.RestrictVoucherToACustomer, Name = ICD.VoucherName, StartDate = ICD.StartDate
		from ZnodeGiftCard ZGC
		inner join @InsertVoucherData ICD ON ICD.VoucherNumber = ZGC.CardNumber
		inner join ZnodePortal ZP ON ICD.StoreCode = ZP.StoreCode and ZGC.PortalId = ZP.PortalId
		left join #TempUserData ZU ON ICD.UserName = ZU.UserName

		insert into ZnodeGiftCard(PortalId,Name,CardNumber,Amount,UserId,ExpirationDate,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,IsActive,RemainingAmount,RestrictToCustomerAccount,StartDate)
		select ZP.PortalId, ICD.VoucherName, ICD.VoucherNumber, ICD.VoucherAmount, ZU.UserId, ICD.ExpirationDate, @UserId, @Getdate, @UserId, @Getdate, ICD.IsActive, ICD.RemainingAmount, ICD.RestrictVoucherToACustomer, ICD.StartDate
		From @InsertVoucherData ICD 
		inner join ZnodePortal ZP ON ICD.StoreCode = ZP.StoreCode 
		left join #TempUserData ZU ON ICD.UserName = ZU.UserName
		where not exists(select * from ZnodeGiftCard ZGC where ICD.VoucherNumber = ZGC.CardNumber )


		UPDATE ZnodeImportProcessLog
		  SET STATUS = dbo.Fn_GetImportStatus( 2 ), ProcessCompletedDate = Getdate()
		WHERE ImportProcessLogId = @ImportProcessLogId;
		COMMIT TRAN Voucher;
	END TRY
	BEGIN CATCH

		UPDATE ZnodeImportProcessLog
		  SET Status = dbo.Fn_GetImportStatus( 3 ), ProcessCompletedDate = Getdate()
		WHERE ImportProcessLogId = @ImportProcessLogId;

		SET @Status = 0;
		SELECT ERROR_LINE(), ERROR_MESSAGE(), ERROR_PROCEDURE();
		ROLLBACK TRAN Voucher;
	END CATCH;
END;