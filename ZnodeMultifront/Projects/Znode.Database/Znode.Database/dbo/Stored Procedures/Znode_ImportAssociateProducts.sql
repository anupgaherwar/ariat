﻿





CREATE PROCEDURE [dbo].[Znode_ImportAssociateProducts](
	  @TableName nvarchar(100), @Status bit OUT, @UserId int, @ImportProcessLogId int, @NewGUId nvarchar(200), @PimCatalogId int= 0)
AS
	--------------------------------------------------------------------------------------
	-- Summary :  Import Product Association 
	
	-- Unit Testing : 
	--BEGIN TRANSACTION;
	--update ZnodeGlobalSetting set FeatureValues = '5' WHERE FeatureName = 'InventoryRoundOff' 
	--    DECLARE @status INT;
	--    EXEC [Znode_ImportInventory] @InventoryXML = '<ArrayOfImportInventoryModel>
	-- <ImportInventoryModel>
	--   <SKU>S1002</SKU>
	--   <Quantity>999998.33</Quantity>
	--   <ReOrderLevel>10</ReOrderLevel>
	--   <RowNumber>1</RowNumber>
	--   <ListCode>TestInventory</ListCode>
	--   <ListName>TestInventory</ListName>
	-- </ImportInventoryModel>
	--</ArrayOfImportInventoryModel>' , @status = @status OUT , @UserId = 2;
	--    SELECT @status;
	--    ROLLBACK TRANSACTION;
	--------------------------------------------------------------------------------------

BEGIN
	BEGIN TRAN A;
	BEGIN TRY
		DECLARE @MessageDisplay nvarchar(100), @SSQL nvarchar(max);
		DECLARE @GetDate datetime= dbo.Fn_GetDate();

		IF OBJECT_ID('TEMPDB..#InsertProductAssociation') IS NOT NULL 
			DROP TABLE #InsertProductAssociation

		IF OBJECT_ID('TEMPDB..#InsertProduct') IS NOT NULL 
			DROP TABLE #InsertProduct

		IF OBJECT_ID('TEMPDB..#SKU') IS NOT NULL 
			DROP TABLE #SKU
		-- Retrive RoundOff Value from global setting 

		CREATE TABLE #InsertProductAssociation 
		( 
			RowId int IDENTITY(1, 1) PRIMARY KEY, RowNumber int, ParentSKU varchar(300), ChildSKU varchar(200), DisplayOrder int, GUID nvarchar(400)
		);
		
		CREATE TABLE #InsertProduct 
		( 
			RowId int IDENTITY(1, 1) PRIMARY KEY, RowNumber int, ParentProductId varchar(300), ChildProductId varchar(200), DisplayOrder int, GUID nvarchar(400)
		);


		DECLARE @CategoryAttributId int;

		DECLARE @InventoryListId int;

		SET @SSQL = 'Select RowNumber,ParentSKU,ChildSKU,DisplayOrder,GUID FROM '+@TableName;
		INSERT INTO #InsertProductAssociation( RowNumber, ParentSKU,ChildSKU,DisplayOrder, GUID )
		EXEC sys.sp_sqlexec @SSQL;


		--@MessageDisplay will use to display validate message for input inventory value  
		CREATE TABLE #SKU 
		( 
						   SKU nvarchar(300), PimProductId int
		);
		INSERT INTO #SKU
			   SELECT b.AttributeValue, a.PimProductId
			   FROM ZnodePimAttributeValue AS a
					INNER JOIN
					ZnodePimAttributeValueLocale AS b
					ON a.PimAttributeId = dbo.Fn_GetProductSKUAttributeId() AND 
					   a.PimAttributeValueId = b.PimAttributeValueId;

		DECLARE @ProductType TABLE
		( 
			ProductType nvarchar(100) ,PimProductId int
		);
		INSERT INTO @ProductType
			   SELECT  ZPADV.AttributeDefaultValueCode, a.PimProductId
			   FROM ZnodePimAttributeValue AS a
					INNER JOIN
					ZnodePimProductAttributeDefaultValue AS b
					ON a.PimAttributeId = dbo.Fn_GetProductTypeAttributeId() AND 
					   a.PimAttributeValueId = b.PimAttributeValueId
					   Inner join ZnodePimAttributeDefaultValue ZPADV On b.PimAttributeDefaultValueId = ZPADV.PimAttributeDefaultValueId
					   where  ZPADV.AttributeDefaultValueCode in ('GroupedProduct','BundleProduct','ConfigurableProduct');
		-- start Functional Validation 
		-----------------------------------------------
		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
			   SELECT '19', 'ParentSKU', ParentSKU, @NewGUId, RowNumber, 2, @GetDate, 2, @GetDate, @ImportProcessLogId
			   FROM #InsertProductAssociation AS ii
			   WHERE NOT EXISTS( SELECT SKU FROM #SKU SKU WHERE ii.ParentSKU = SKU.SKU)

			INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
			   SELECT '19', 'ChildSKU', ChildSKU, @NewGUId, RowNumber, 2, @GetDate, 2, @GetDate, @ImportProcessLogId
			   FROM #InsertProductAssociation AS ii
			   WHERE NOT EXISTS( SELECT SKU FROM #SKU SKU WHERE ii.ChildSKU = SKU.SKU)

			INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
			   SELECT '53', 'ParentSKU / ChildSKU', ParentSKU+' / '+ChildSKU, @NewGUId, RowNumber, 2, @GetDate, 2, @GetDate, @ImportProcessLogId
			   FROM #InsertProductAssociation AS ii
			   WHERE ii.ParentSKU IN
			   (
				   select ParentSKU from #InsertProductAssociation
					group by ParentSKU,ChildSKU
					having count(1)>1
			   );

			INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
			   SELECT '49', 'ParentSKU',   ParentSKU , @NewGUId, RowNumber, 2, @GetDate, 2, @GetDate, @ImportProcessLogId
			   FROM #InsertProductAssociation AS ii
			   WHERE not exists
			   (
				   SELECT SKU  FROM #SKU SKU inner join @ProductType  PT ON SKU.PimProductId = PT.PimProductId and ii.ParentSKU = SKU.SKU
	
			   );

			INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
			   SELECT '51', 'ChildSKU',   ChildSKU, @NewGUId, RowNumber, 2, @GetDate, 2, @GetDate, @ImportProcessLogId
			   FROM #InsertProductAssociation AS ii
			   WHERE exists 
			   (
				   SELECT SKU  FROM #SKU SKU inner join @ProductType  PT ON SKU.PimProductId = PT.PimProductId and ii.ChildSKU = SKU.SKU
	
			   );

			INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
			   SELECT '35', 'ParentSKU',  'Configure Attribute Missing: '+ Convert(nvarchar(400),isnull(ParentSKU,'')), @NewGUId, RowNumber, 2, @GetDate, 2, @GetDate, @ImportProcessLogId
			   FROM #InsertProductAssociation AS ii Inner join #SKU PS ON 
			   ii.ParentSKU = PS.SKU 
			   Inner join @ProductType  PT ON PS.PimProductId = PT.PimProductId  AND PT.ProductType  in ('ConfigurableProduct')
			   where  NOT exists 
			   (select PimProductId  from ZnodePimConfigureProductAttribute d where PS.PimProductId = d.PimProductId)
			   -- End Function Validation 	

			INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
			SELECT '17', 'DisplayOrder', DisplayOrder, @NewGUId, RowNumber, 2, @GetDate, 2, @GetDate, @ImportProcessLogId
			FROM #InsertProductAssociation AS ii
			WHERE (ii.DisplayOrder <> '' OR ii.DisplayOrder IS NOT NULL )AND  ii.DisplayOrder = 0

		INSERT INTO ZnodeImportLog( ErrorDescription, ColumnName, Data, GUID, RowNumber, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ImportProcessLogId )
			SELECT '64', 'DisplayOrder', DisplayOrder, @NewGUId, RowNumber, 2, @GetDate, 2, @GetDate, @ImportProcessLogId
			FROM #InsertProductAssociation AS ii
			WHERE (ii.DisplayOrder <> '' OR ii.DisplayOrder IS NOT NULL )AND  ii.DisplayOrder > 999

			   UPDATE ZIL
			   SET ZIL.ColumnName =   ZIL.ColumnName + ' [ SKU - ' + isnull(ParentSKU,'') + ' ] '
			   FROM ZnodeImportLog ZIL 
			   INNER JOIN #InsertProductAssociation IPA ON (ZIL.RowNumber = IPA.RowNumber)
			   WHERE  ZIL.ImportProcessLogId = @ImportProcessLogId AND ZIL.RowNumber IS NOT NULL

		-----------------------------------------------
		--- Delete Invalid Data after functional validatin  
		DELETE FROM #InsertProductAssociation
		WHERE RowNumber IN
		(
			SELECT DISTINCT 
				   RowNumber
			FROM ZnodeImportLog
			WHERE ImportProcessLogId = @ImportProcessLogId AND 
				  GUID = @NewGUID
		);

		insert into #InsertProduct (RowNumber,  ParentProductId , ChildProductId , DisplayOrder)
			SELECT RowNumber , SKUParent.PimProductId SKUParentId , 
				   ( Select TOP 1 SKUChild.PimProductId from #SKU AS SKUChild where  SKUChild.SKU = IPAC.ChildSKU ) SKUChildId,
				    DisplayOrder
					FROM #InsertProductAssociation AS IPAC INNER JOIN #SKU AS SKUParent ON IPAC.ParentSKU = SKUParent.SKU 

	-- Update Record count in log 
        DECLARE @FailedRecordCount BIGINT
		DECLARE @SuccessRecordCount BIGINT
		SELECT @FailedRecordCount = COUNT(DISTINCT RowNumber) FROM ZnodeImportLog WHERE RowNumber IS NOT NULL AND  ImportProcessLogId = @ImportProcessLogId;
		Select @SuccessRecordCount = count(DISTINCT RowNumber) FROM #InsertProduct
		UPDATE ZnodeImportProcessLog SET FailedRecordcount = @FailedRecordCount , SuccessRecordCount = @SuccessRecordCount, TotalProcessedRecords = (ISNULL(@FailedRecordCount,0) + ISNULL(@SuccessRecordCount,0))
		WHERE ImportProcessLogId = @ImportProcessLogId;
		-- End
	
		UPDATE B set b.ModifiedDate = @GetDate, b.ModifiedBy = @UserId, b.DisplayOrder = case when a.DisplayOrder is not null then a.DisplayOrder else b.DisplayOrder end
		from #InsertProduct A
		INNER JOIN ZnodePimProductTypeAssociation B ON a.ParentProductId = b.PimParentProductId and a.ChildProductId = b.PimProductId

		INSERT INTO ZnodePimProductTypeAssociation (PimParentProductId, PimProductId, DisplayOrder, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate) 
		select  ParentProductId , ChildProductId , DisplayOrder, @UserId, @GetDate, @UserId, @GetDate  from #InsertProduct 
		where  NOT Exists (Select TOP 1 1 from ZnodePimProductTypeAssociation where PimParentProductId =  #InsertProduct.ParentProductId
		AND PimProductId = #InsertProduct.ChildProductId )

		--DELETE FROM ZnodePimConfigureProductAttribute 
		--WHERE EXISTS (SELECT TOP 1  1 FROM #InsertProduct RT WHERE RT.ParentProductId = ZnodePimConfigureProductAttribute.PimProductId )

		-- INSERT INTO ZnodePimConfigureProductAttribute (PimProductId,PimFamilyId,PimAttributeId,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
	 --    SELECT DISTINCT c.ParentProductId, d.PimAttributeFamilyId , a.PimAttributeId , @UserId , @GetDate, @UserId , @GetDate
		-- FROM ZnodePimAttributeValue a 
		-- INNER JOIN ZnodePimProductTypeAssociation b ON (b.PimProductId = a.PimProductId)
		-- INNER JOIN #InsertProduct c ON (c.ParentProductId = b.PimParentProductId)
		-- INNER JOIN ZnodePimProduct d ON (d.PimProductId = c.ParentProductId)
		-- WHERE a.PimAttributeId IN (SELECT PimAttributeId FROM ZnodePimAttribute WHERE IsConfigurable = 1 AND IsCategory = 0 )
		-- AND NOT EXISTS (SELECT TOP 1 1 FROM ZnodePimConfigureProductAttribute t WHERE t.PimAttributeId = a.PimAttributeId AND t.PimProductId = d.PimProductId )
				


		--select 'End'
		--      SET @Status = 1;
		UPDATE ZnodeImportProcessLog
		  SET Status = dbo.Fn_GetImportStatus( 2 ), ProcessCompletedDate = @GetDate
		WHERE ImportProcessLogId = @ImportProcessLogId;

		COMMIT TRAN A;
	END TRY
	BEGIN CATCH

		UPDATE ZnodeImportProcessLog
		  SET Status = dbo.Fn_GetImportStatus( 3 ), ProcessCompletedDate = @GetDate
		WHERE ImportProcessLogId = @ImportProcessLogId;

		SET @Status = 0;
		SELECT ERROR_LINE(), ERROR_MESSAGE(), ERROR_PROCEDURE();
		ROLLBACK TRAN A;
	END CATCH;
END;