﻿CREATE TABLE [dbo].[Customer3] (
    [Username]    NVARCHAR (50) NOT NULL,
    [FirstName]   NVARCHAR (50) NOT NULL,
    [LastName]    NVARCHAR (50) NOT NULL,
    [Email]       NVARCHAR (50) NOT NULL,
    [PhoneNumber] FLOAT (53)    NOT NULL,
    [EmailOptIn]  NVARCHAR (1)  NULL,
    [IsActive]    NVARCHAR (50) NOT NULL,
    [ExternalId]  NVARCHAR (1)  NULL,
    [CreatedDate] NVARCHAR (1)  NULL,
    [ProfileName] NVARCHAR (50) NOT NULL
);

