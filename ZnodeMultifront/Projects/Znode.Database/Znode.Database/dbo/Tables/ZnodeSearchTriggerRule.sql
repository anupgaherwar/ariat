﻿CREATE TABLE [dbo].[ZnodeSearchTriggerRule] (
    [SearchTriggerRuleId]    INT           IDENTITY (1, 1) NOT NULL,
    [SearchCatalogRuleId]    INT           NOT NULL,
    [SearchTriggerKeyword]   VARCHAR (100) NULL,
    [SearchTriggerCondition] VARCHAR (50)  NULL,
    [SearchTriggerValue]     VARCHAR (600) NULL,
    [CreatedBy]              INT           NOT NULL,
    [CreatedDate]            DATETIME      NOT NULL,
    [ModifiedBy]             INT           NOT NULL,
    [ModifiedDate]           DATETIME      NOT NULL,
    CONSTRAINT [PK_ZnodeSearchTriggerRule] PRIMARY KEY CLUSTERED ([SearchTriggerRuleId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_ZnodeSearchTriggerRule_ZnodeSearchCatalogRule] FOREIGN KEY ([SearchCatalogRuleId]) REFERENCES [dbo].[ZnodeSearchCatalogRule] ([SearchCatalogRuleId])
);

