﻿CREATE TABLE [dbo].[ZnodePortalCatalog] (
    [PortalCatalogId]  INT      IDENTITY (1, 1) NOT NULL,
    [PortalId]         INT      NOT NULL,
    [PublishCatalogId] INT      NOT NULL,
    [CreatedBy]        INT      NOT NULL,
    [CreatedDate]      DATETIME NOT NULL,
    [ModifiedBy]       INT      NOT NULL,
    [ModifiedDate]     DATETIME NOT NULL,
    CONSTRAINT [PK_ZnodePortalCatalog] PRIMARY KEY CLUSTERED ([PortalCatalogId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_ZNodePortalCatalog_ZnodePortal] FOREIGN KEY ([PortalId]) REFERENCES [dbo].[ZnodePortal] ([PortalId]),
    CONSTRAINT [FK_ZnodePortalCatalog_ZnodePublishCatalog] FOREIGN KEY ([PublishCatalogId]) REFERENCES [dbo].[ZnodePublishCatalog] ([PublishCatalogId])
);

