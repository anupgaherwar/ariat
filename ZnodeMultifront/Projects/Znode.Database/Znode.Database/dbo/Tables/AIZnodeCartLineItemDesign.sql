﻿CREATE TABLE [dbo].[AIZnodeCartLineItemDesign] (
    [OmsOrderLineItemsDesignId] INT            IDENTITY (1, 1) NOT NULL,
    [OmsOrderLineItemsId]       INT            NULL,
    [OrderLineItemsDesignId]    INT            NULL,
    [OrderLineItemsImagePath]   NVARCHAR (MAX) NULL,
    [CreatedBy]                 INT            NULL,
    [CreatedDate]               DATETIME       NULL,
    [ModifiedBy]                INT            NULL,
    [ModifiedDate]              DATETIME       NULL,
    CONSTRAINT [PK_AIZnodeLineItemDesign] PRIMARY KEY CLUSTERED ([OmsOrderLineItemsDesignId] ASC) WITH (FILLFACTOR = 90)
);

