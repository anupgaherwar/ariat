﻿CREATE TABLE [dbo].[ZnodeSearchCatalogRule] (
    [SearchCatalogRuleId] INT           IDENTITY (1, 1) NOT NULL,
    [PublishCatalogId]    INT           NOT NULL,
    [RuleName]            VARCHAR (600) NULL,
    [StartDate]           DATETIME      NULL,
    [EndDate]             DATETIME      NULL,
    [IsTriggerForAll]     BIT           NOT NULL,
    [IsItemForAll]        BIT           NOT NULL,
    [IsGlobalRule]        BIT           CONSTRAINT [DF_ZnodeSearchCatalogRule_IsGlobalRule] DEFAULT ((0)) NOT NULL,
    [IsPause]             BIT           CONSTRAINT [DF_ZnodeSearchCatalogRule_IsPause] DEFAULT ((0)) NOT NULL,
    [CreatedBy]           INT           NOT NULL,
    [CreatedDate]         DATETIME      NOT NULL,
    [ModifiedBy]          INT           NOT NULL,
    [ModifiedDate]        DATETIME      NOT NULL,
    CONSTRAINT [PK_ZnodeSearchCatalogRule] PRIMARY KEY CLUSTERED ([SearchCatalogRuleId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [UK_ZnodeSearchCatalogRule_RuleName] UNIQUE NONCLUSTERED ([RuleName] ASC) WITH (FILLFACTOR = 90)
);

