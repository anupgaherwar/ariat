﻿CREATE TABLE [dbo].[ZnodeSearchGlobalProductCategoryBoost] (
    [SearchGlobalProductCategoryBoostId] INT             IDENTITY (1, 1) NOT NULL,
    [PublishCatalogId]                   INT             NOT NULL,
    [PublishProductId]                   INT             NOT NULL,
    [PublishCategoryId]                  INT             NOT NULL,
    [Boost]                              NUMERIC (28, 6) NOT NULL,
    [CreatedBy]                          INT             NOT NULL,
    [CreatedDate]                        DATETIME        NOT NULL,
    [ModifiedBy]                         INT             NOT NULL,
    [ModifiedDate]                       DATETIME        NOT NULL,
    CONSTRAINT [PK_ZNodeProductCategoryBoost] PRIMARY KEY CLUSTERED ([SearchGlobalProductCategoryBoostId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_ZnodeSearchGlobalProductCategoryBoost_ZnodePublishCatalog] FOREIGN KEY ([PublishCatalogId]) REFERENCES [dbo].[ZnodePublishCatalog] ([PublishCatalogId]),
    CONSTRAINT [FK_ZnodeSearchGlobalProductCategoryBoost_ZnodePublishCategory] FOREIGN KEY ([PublishCategoryId]) REFERENCES [dbo].[ZnodePublishCategory] ([PublishCategoryId]),
    CONSTRAINT [FK_ZnodeSearchGlobalProductCategoryBoost_ZnodePublishProduct] FOREIGN KEY ([PublishProductId]) REFERENCES [dbo].[ZnodePublishProduct] ([PublishProductId]),
    CONSTRAINT [IX_ZNodeSearchGlobalProductCategoryUnique] UNIQUE NONCLUSTERED ([PublishProductId] ASC, [PublishCategoryId] ASC) WITH (FILLFACTOR = 90)
);

