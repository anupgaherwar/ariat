﻿CREATE TABLE [dbo].[ZnodePromotionAttributeValidation] (
    [PramotionAttributeValidationId] INT           IDENTITY (1, 1) NOT NULL,
    [PramotionAttributeId]           INT           NULL,
    [InputValidationId]              INT           NULL,
    [InputValidationRuleId]          INT           NULL,
    [Name]                           VARCHAR (300) NULL,
    [CreatedBy]                      INT           NOT NULL,
    [CreatedDate]                    DATETIME      NOT NULL,
    [ModifiedBy]                     INT           NOT NULL,
    [ModifiedDate]                   DATETIME      NOT NULL,
    CONSTRAINT [PK_ZnodePramotionAttributeValidation] PRIMARY KEY CLUSTERED ([PramotionAttributeValidationId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_ZnodePramotionAttributeValidation_ZnodePramotionAttributeValidation] FOREIGN KEY ([PramotionAttributeValidationId]) REFERENCES [dbo].[ZnodePromotionAttributeValidation] ([PramotionAttributeValidationId]),
    CONSTRAINT [FK_ZnodePromotionAttributeValidation_ZnodeAttributeInputValidation] FOREIGN KEY ([InputValidationId]) REFERENCES [dbo].[ZnodeAttributeInputValidation] ([InputValidationId]),
    CONSTRAINT [FK_ZnodePromotionAttributeValidation_ZnodeAttributeInputValidationRule] FOREIGN KEY ([InputValidationRuleId]) REFERENCES [dbo].[ZnodeAttributeInputValidationRule] ([InputValidationRuleId]),
    CONSTRAINT [FK_ZnodePromotionAttributeValidation_ZnodePromotionAttribute] FOREIGN KEY ([PramotionAttributeId]) REFERENCES [dbo].[ZnodePromotionAttribute] ([PramotionAttributeId])
);

