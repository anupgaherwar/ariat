﻿CREATE TABLE [dbo].[ZnodePortalPixelTracking] (
    [PortalPixelTrackingId]   INT            IDENTITY (1, 1) NOT NULL,
    [PortalId]                INT            NOT NULL,
    [CreatedBy]               INT            NULL,
    [CreatedDate]             DATETIME       NULL,
    [ModifiedBy]              INT            NULL,
    [ModifiedDate]            DATETIME       NULL,
    [TrackingPixelScriptCode] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_ZnodePortalPixelTracking] PRIMARY KEY CLUSTERED ([PortalPixelTrackingId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_ZnodePortalPixelTracking_ZnodePortal] FOREIGN KEY ([PortalId]) REFERENCES [dbo].[ZnodePortal] ([PortalId])
);



