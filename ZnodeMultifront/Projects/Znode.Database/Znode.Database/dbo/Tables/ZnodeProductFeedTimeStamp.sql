﻿CREATE TABLE [dbo].[ZnodeProductFeedTimeStamp] (
    [ProductFeedTimeStampId]   INT            IDENTITY (1, 1) NOT NULL,
    [ProductFeedTimeStampCode] NVARCHAR (50)  NOT NULL,
    [ProductFeedTimeStampName] NVARCHAR (100) NULL,
    [CreatedBy]                INT            NOT NULL,
    [CreatedDate]              DATETIME       NOT NULL,
    [ModifiedBy]               INT            NOT NULL,
    [ModifiedDate]             DATETIME       NOT NULL,
    CONSTRAINT [PK_ProductFeedTimeStamp] PRIMARY KEY CLUSTERED ([ProductFeedTimeStampId] ASC) WITH (FILLFACTOR = 90)
);

