﻿CREATE TABLE [dbo].[ZnodeOmsQuoteHistory] (
    [OmsQuoteHistoryId] INT             IDENTITY (1, 1) NOT NULL,
    [OMSQuoteId]        INT             NULL,
    [OrderAmount]       NUMERIC (28, 6) NULL,
    [Message]           NVARCHAR (MAX)  NULL,
    [CreatedBy]         INT             NULL,
    [CreatedDate]       DATETIME        NULL,
    [ModifiedBy]        INT             NULL,
    [ModifiedDate]      DATETIME        NULL,
    CONSTRAINT [PK_ZnodeOmsQuoteHistory] PRIMARY KEY CLUSTERED ([OmsQuoteHistoryId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_ZnodeOmsQuoteHistory_ZnodeOmsQuote] FOREIGN KEY ([OMSQuoteId]) REFERENCES [dbo].[ZnodeOmsQuote] ([OmsQuoteId])
);

