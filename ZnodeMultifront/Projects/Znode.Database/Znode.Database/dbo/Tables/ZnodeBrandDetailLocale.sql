﻿CREATE TABLE [dbo].[ZnodeBrandDetailLocale] (
    [BrandDetailLocaleId] INT            IDENTITY (1, 1) NOT NULL,
    [BrandId]             INT            NULL,
    [Description]         NVARCHAR (MAX) NULL,
    [SEOFriendlyPageName] NVARCHAR (600) NULL,
    [LocaleId]            INT            NULL,
    [CreatedBy]           INT            NOT NULL,
    [CreatedDate]         DATETIME       NOT NULL,
    [ModifiedBy]          INT            NOT NULL,
    [ModifiedDate]        DATETIME       NOT NULL,
    [BrandName]           NVARCHAR (100) NULL,
    CONSTRAINT [PK_ZnodeBrandDetailLocale] PRIMARY KEY CLUSTERED ([BrandDetailLocaleId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_ZnodeBrandDetailLocale_ZnodeBrandDetails] FOREIGN KEY ([BrandId]) REFERENCES [dbo].[ZnodeBrandDetails] ([BrandId])
);

