﻿CREATE TABLE [dbo].[ZnodeOmsOrderDiscount] (
    [OmsOrderDiscountId] INT             IDENTITY (1, 1) NOT NULL,
    [OmsOrderDetailsId]  INT             NULL,
    [OmsOrderLineItemId] INT             NULL,
    [OmsDiscountTypeId]  INT             NULL,
    [DiscountCode]       VARCHAR (300)   NULL,
    [DiscountAmount]     NUMERIC (28, 6) NULL,
    [Description]        NVARCHAR (MAX)  NULL,
    [CreatedBy]          INT             NOT NULL,
    [CreatedDate]        DATETIME        NOT NULL,
    [ModifiedBy]         INT             NOT NULL,
    [ModifiedDate]       DATETIME        NOT NULL,
    CONSTRAINT [PK_ZnodeOmsOrderDiscount] PRIMARY KEY CLUSTERED ([OmsOrderDiscountId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_ZnodeOmsOrderDiscount_ZnodeOmsDiscountType] FOREIGN KEY ([OmsDiscountTypeId]) REFERENCES [dbo].[ZnodeOmsDiscountType] ([OmsDiscountTypeId]),
    CONSTRAINT [FK_ZnodeOmsOrderDiscount_ZnodeOmsOrderDetails] FOREIGN KEY ([OmsOrderDetailsId]) REFERENCES [dbo].[ZnodeOmsOrderDetails] ([OmsOrderDetailsId]),
    CONSTRAINT [FK_ZnodeOmsOrderDiscount_ZnodeOmsOrderLineItem] FOREIGN KEY ([OmsOrderLineItemId]) REFERENCES [dbo].[ZnodeOmsOrderLineItems] ([OmsOrderLineItemsId])
);

