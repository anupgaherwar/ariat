﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/
:r .\dbo\scripts\ZnodeApplicationSetting.sql
:r .\dbo\scripts\AspNetRoles.sql
-- :r .\dbo\scripts\DefaultDataFirstLoad.sql
:r .\dbo\scripts\ZnodeAccessPermission.sql
:r .\dbo\scripts\ZnodeMenu.sql
:r .\dbo\scripts\ZnodeActionMenu.sql
:r .\dbo\scripts\ZnodeActions.sql
:r .\dbo\scripts\ZnodeApproverLevel.sql
:r .\dbo\scripts\ZnodeAriatCustomShippingCharges.sql
:r .\dbo\scripts\ZnodeAttributeInputValidation.sql
:r .\dbo\scripts\ZnodeAttributeInputValidationRule.sql
:r .\dbo\scripts\ZnodeAttributeType.sql
:r .\dbo\scripts\ZnodeCasePriority.sql
:r .\dbo\scripts\ZnodeCaseStatus.sql
:r .\dbo\scripts\ZnodeCaseType.sql
:r .\dbo\scripts\ZnodeCMSArea.sql
:r .\dbo\scripts\ZnodeCMSContentPageGroup.sql
:r .\dbo\scripts\ZnodeCMSContentPageGroupLocale.sql
:r .\dbo\scripts\ZnodeCMSContentPageGroupMapping.sql
:r .\dbo\scripts\ZnodeCMSContentPages.sql
:r .\dbo\scripts\ZnodeCMSContentPagesLocale.sql
:r .\dbo\scripts\ZnodeCMSMessage.sql
:r .\dbo\scripts\ZnodeCMSMessageKey.sql

:r .\dbo\scripts\ZnodeCMSSEOType.sql
:r .\dbo\scripts\ZnodeCMSTemplate.sql
:r .\dbo\scripts\ZnodeCMSTextWidgetConfiguration.sql
:r .\dbo\scripts\ZnodeCMSWidgets.sql
:r .\dbo\scripts\ZnodeCMSWidgetSliderBanner.sql
:r .\dbo\scripts\ZnodeCMSWidgetTitleConfiguration.sql
:r .\dbo\scripts\ZnodeCMSWidgetTitleConfigurationLocale.sql
:r .\dbo\scripts\ZnodeDateFormat.sql
:r .\dbo\scripts\ZnodeDepartment.sql
:r .\dbo\scripts\ZnodeDisplayUnit.sql
:r .\dbo\scripts\ZnodeEmailTemplate.sql
:r .\dbo\scripts\ZnodeEmailTemplateAreas.sql
:r .\dbo\scripts\ZnodeEmailTemplateLocale.sql
:r .\dbo\scripts\ZnodeERPTaskScheduler.sql
:r .\dbo\scripts\ZnodeERPTaskSchedulerSetting.sql
:r .\dbo\scripts\ZnodeGlobalAttribute.sql
:r .\dbo\scripts\ZnodeGlobalAttributeDefaultValue.sql
:r .\dbo\scripts\ZnodeGlobalAttributeDefaultValueLocale.sql
:r .\dbo\scripts\ZnodeGlobalAttributeGroup.sql
:r .\dbo\scripts\ZnodeGlobalAttributeGroupLocale.sql
:r .\dbo\scripts\ZnodeGlobalAttributeGroupMapper.sql
:r .\dbo\scripts\ZnodeGlobalAttributeLocale.sql
:r .\dbo\scripts\ZnodeGlobalAttributeValidation.sql
:r .\dbo\scripts\ZnodeGlobalEntity.sql
:r .\dbo\scripts\ZnodeGlobalGroupEntityMapper.sql
:r .\dbo\scripts\ZnodeGlobalSetting.sql
:r .\dbo\scripts\ZnodeImportAttributeValidation.sql
:r .\dbo\scripts\ZnodeImportHead.sql
:r .\dbo\scripts\ZnodeImportTemplate.sql
:r .\dbo\scripts\ZnodeImportTemplateMapping.sql
:r .\dbo\scripts\ZnodeImportUpdatableColumns.sql
:r .\dbo\scripts\ZnodeListView.sql
:r .\dbo\scripts\ZnodeListViewFilter.sql
:r .\dbo\scripts\ZnodeLocale.sql
:r .\dbo\scripts\ZnodeMediaConfiguration.sql
:r .\dbo\scripts\ZnodeMediaServerMaster.sql
:r .\dbo\scripts\ZnodeMenuActionsPermission.sql
:r .\dbo\scripts\ZnodeMessage.sql
:r .\dbo\scripts\ZnodeOmsDiscountType.sql
:r .\dbo\scripts\ZnodeOmsOrderLineItemRelationshipType.sql
:r .\dbo\scripts\ZnodeOmsOrderState.sql
:r .\dbo\scripts\ZnodeOmsOrderStateShowToCustomer.sql
:r .\dbo\scripts\ZnodeOmsPaymentState.sql
:r .\dbo\scripts\ZnodeOmsQuoteType.sql
:r .\dbo\scripts\ZnodeOmsRefundType.sql
:r .\dbo\scripts\ZnodePimAttribute.sql
:r .\dbo\scripts\ZnodePimAttributeDefaultValue.sql
:r .\dbo\scripts\ZnodePimAttributeDefaultValueLocale.sql
:r .\dbo\scripts\ZnodePimAttributeFamily.sql
:r .\dbo\scripts\ZnodePimAttributeGroup.sql
:r .\dbo\scripts\ZnodePimAttributeGroupLocale.sql
:r .\dbo\scripts\ZnodePimAttributeGroupMapper.sql
:r .\dbo\scripts\ZnodePimAttributeValidation.sql
:r .\dbo\scripts\ZnodePimFamilyLocale.sql
:r .\dbo\scripts\ZnodePimFrontendProperties.sql
:r .\dbo\scripts\ZnodePortalFeature.sql
:r .\dbo\scripts\ZnodeProductFeedSiteMapType.sql
:r .\dbo\scripts\ZnodeProductFeedType.sql
:r .\dbo\scripts\ZnodeProductReviewState.sql
:r .\dbo\scripts\ZnodePromotionType.sql
:r .\dbo\scripts\ZnodePublishState.sql
:r .\dbo\scripts\ZnodeRmaRequestStatus.sql
:r .\dbo\scripts\ZnodeRobotsTxt.sql
:r .\dbo\scripts\ZnodeRoleMenu.sql
:r .\dbo\scripts\ZnodeRoleMenuAccessMapper.sql
:r .\dbo\scripts\ZnodeSearchActivity.sql
:r .\dbo\scripts\ZnodeSearchFeature.sql
:r .\dbo\scripts\ZnodeSearchQueryType.sql
:r .\dbo\scripts\ZnodeSearchQueryTypeFeature.sql
:r .\dbo\scripts\ZnodeShippingServiceCode.sql
:r .\dbo\scripts\ZnodeShippingTypes.sql
:r .\dbo\scripts\ZnodeSortSetting.sql
:r .\dbo\scripts\ZnodeSupplierTypes.sql
:r .\dbo\scripts\ZnodeTaxClass.sql
:r .\dbo\scripts\ZnodeTaxRuleTypes.sql
:r .\dbo\scripts\ZnodeTimeFormat.sql
:r .\dbo\scripts\ZnodeUom.sql