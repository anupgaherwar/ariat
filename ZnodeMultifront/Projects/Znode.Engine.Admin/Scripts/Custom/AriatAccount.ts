﻿class AriatAccount extends ZnodeBase {
    _endPoint: Endpoint;

    constructor() {
        super();
        this._endPoint = new Endpoint();

    }

    Init() {
    }    

    ExportFullCatalog(): any {
       // e.preventDefault();
        //var currentTarget = e.currentTarget;
        var controller: string = "AriatAccount";
        var action: string = "ExportFullCatalog";
        var accountId: string = $("#hdnAccountId").val();
        var url = this.getExportUrl(controller, action);
        var param = this.getExportParam(accountId);
        var catalogId = $('#hdnFilterCatalogId').val();
        param = catalogId != undefined ? (param += "&pimCatalogId=" + catalogId + "&catalogName=" + $('#hdnFilterCatalogName').val()) : param;
        var exportBase = this;
        ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("ExportPleaseWaitMsg"), "success", true, 5000);
        ZnodeBase.prototype.ajaxRequest(url, "GET", param, function (response) {
            if (response.status)
                exportBase.downloadFile(response);
            else
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(response.message, "error", true, 5000);
        }, null);
    }

    ExportProgramCatalog(): any {       
        var controller: string = "AriatAccount";
        var action: string = "ExportProgramCatalog";
        var accountId: string = $("#hdnAccountId").val();
        var url = this.getExportUrl(controller, action);
        var param = this.getExportParam(accountId);
        var catalogId = $('#hdnFilterCatalogId').val();
        param = catalogId != undefined ? (param += "&pimCatalogId=" + catalogId + "&catalogName=" + $('#hdnFilterCatalogName').val()) : param;
        var exportBase = this;
        ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("ExportPleaseWaitMsg"), "success", true, 5000);
        ZnodeBase.prototype.ajaxRequest(url, "GET", param, function (response) {
            if (response.status)
                exportBase.downloadFile(response);
            else
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(response.message, "error", true, 5000);
        }, null);
    }

    getExportParam(exportTypeId: string) {
        return ("accountId=".concat(exportTypeId));
    }

    //Returns export url
    getExportUrl(controller, action) {
        return ("/".concat(controller, "/", action));
    }

    //Download Data into file to user
    downloadFile(response: any) {
        /*Byte Order Mark - used in encoding that allow reader to identify a file as being encoded in UTF-8.*/
        var BOM = "\uFEFF";
        var blob = new Blob([BOM.concat(response.content)], { type: "text/csv;charset=utf-8;" });

        if (ZnodeBase.prototype.getBrowser() == "IE")
            window.navigator.msSaveBlob(blob, response.fileName);
        else {
            var url = window.URL.createObjectURL(blob);
            var a = document.createElement("a");
            document.body.appendChild(a);
            a.href = url;
            a.download = response.fileName;
            a.click();
            window.URL.revokeObjectURL(url); /* Not to keep the reference to the file any longer.*/
        }
    }

    DeleteProducts(control): any {
        var ariatProgramCatalogProductIds = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (ariatProgramCatalogProductIds.length > 0) {
            CustomEndpoint.prototype.DeleteProducts(ariatProgramCatalogProductIds, function (res) {        
                DynamicGrid.prototype.RefreshGridOndelete(control, res);
            });
        }
    }
}