﻿
Import.prototype.PostData = function (importModel): any {
    var accountNumber = CustomImport.prototype.GetAccountCode('externalId');
    if (accountNumber) {
        importModel.IsPartialPage = true;
        importModel.Custom1 = accountNumber;
    }
    ZnodeBase.prototype.ShowLoader();
    Endpoint.prototype.ImportPost(importModel, function (res) {
        var ReturnURL = $("#importReturnURl").val();
        setTimeout(function () {
            ZnodeBase.prototype.HideLoader();
            window.location.href = window.location.protocol + "//" + window.location.host + ReturnURL;
        }, 900);
    });
}

class CustomImport extends ZnodeBase {

    GetAccountCode(param: any) {
        var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < url.length; i++) {
            var urlparam = url[i].split('=');
            if (urlparam[0] == param) {
                return urlparam[1];
            }
        }
    }

    ValidateModelAndPost(): any {
        if (Import.prototype.ValidateModel()) {
            Import.prototype.CreateAndPostModel();
            return false;
        } else {
            return false;
        }
    }
}

