﻿
$(document).off("click", "#artifi-previewproduct");
$(document).on("click", "#artifi-previewproduct", function (e) {
    e.preventDefault();
    var _this = this;
    var portalId = $("#hdnPortalId").val();
    portalId = portalId == '0' || typeof portalId == "undefined" ? $("#PortalId").val() : portalId;
    $.ajax({
        url: "/artifiorder/getartificredentials",
        method: "POST",
        data: { portalId: portalId, "userId": $("#hdnUserId").val() },
        dataType: "json",
        async: false,
        success: function (data) {
            var artifiCredentials = {
                websiteId: data.WebsiteCode,
                webApiclientKey: data.WebApiClientKey,
                jsUrl: data.JsUrl,
                userId: data.userId
            }
            var configurablesku = $(_this).data('configurablesku');
            var groupsku = $(_this).data('groupsku')
            var sku = $(_this).data("groupsku");
            sku = sku != "" && sku != null && sku != 'undefined' ? sku : $(_this).data("sku");
            var _param = "";
            if (configurablesku != "" && configurablesku != null && configurablesku != 'undefined') {
                _param = {
                    websiteId: artifiCredentials.websiteId,
                    webApiclientKey: artifiCredentials.webApiclientKey,
                    sku: sku,
                    designId: $(_this).data("custmizeproductid"),
                    isGuest: true,
                    userId: artifiCredentials.userId,
                    height: "100%",
                    width: "100%",
                }
            }
            else {
                _param = {
                    websiteId: artifiCredentials.websiteId,
                    webApiclientKey: artifiCredentials.webApiclientKey,
                    productCode: sku,
                    designId: $(_this).data("custmizeproductid"),
                    isGuest: true,
                    userId: artifiCredentials.userId,
                    height: "100%",
                    width: "100%",
                }
            }

            var _url = Artifi.getPreviewUrl(_param);
            ZnodeBase.prototype.BrowseAsidePoupPanel('/ArtifiOrder/PreviewArtifiDesign?url=' + _url, 'previewArtifiProduct');
        },
        error: function (data) {
            console.log(data);
        }

    });
});

$(document).off("click", "#artifi-design-call");
$(document).on("click", "#artifi-design-call", function (e) {
    e.preventDefault();
    $("#getProductsList").hide();
    ZnodeBase.prototype.BrowseAsidePoupPanel('/ArtifiOrder/AddEditArtifiDesign', 'getArtifiProduct');

});

function AddArtifiDesign() {
    var item = CartItemModel();
    item.PortalId = $("#PortalId").val();
    item.CatalogId = $("#PortalCatalogId").val();
    item.LocaleId = $("#LocaleId").val();
    item.UserId = $("#hdnUserId").val();
    item.Quantity = $("#Quantity").val();
    var cartitem = JSON.stringify(item);
    sessionStorage.removeItem("cartitem");
    sessionStorage.setItem("cartitem", cartitem);

    var groupSku = $("#artifi-design-call").attr("data-artifigroupsku");
    var configurableSku = $("#dynamic-configurableProductSKUs").val();
    var productName = $("#artifi-design-call").attr("data-groupproductname");

    if (productName != "" && typeof (productName) != 'undefined') {
        localStorage.removeItem("productname");
        localStorage.setItem("productname", productName);
    }
    else {
        localStorage.removeItem("productname");
        localStorage.setItem("productname", $("#artifi-design-call").attr("data-simpleproductname"));
    }
    var sku = (groupSku != "" && groupSku != null && groupSku != 'undefined') ? groupSku : configurableSku;
    var status = false;
    if (sku != "" && sku != null && sku != 'undefined') {
        localStorage.removeItem("sku");
        localStorage.setItem("sku", sku);
        localStorage.setItem("configurablesku", configurableSku);
        var publishProductId = $("#artifi-design-call").attr("data-productId");
        var quantity = $("#Quantity").val();
        if (quantity == null || quantity == "") {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("RequiredProductQuantity"), "error", true, fadeOutTime);
            return false;
        }
        if (Order.prototype.OnAssociatedProductQuantityChange() && Order.prototype.CheckGroupProductQuantity(Order.prototype.BindProductParameterModel(), sku, quantity)) {
            $('#getArtifiProduct').modal('show');
        }
    }
    $('#Artifi-Designer').empty();
    if ($("#button-addtocart").attr("disabled") != "disabled") {
        sku = $("#artifi-design-call").attr("data-artifisku");
        localStorage.removeItem("sku");
        localStorage.setItem("sku", sku);
    }
    LaunchArtifi();
}


function CartItemModel() {
    var bindCartItemModel = {
        PublishProductId: $("#dynamic-productId").val(),
        //"parentproductId": $("#dynamic-parentproductId").val(),
        ProductName: $("#dynamic-productName").val(),
        SKU: $("#dynamic-sku").val(),
        PortalId: $("#dynamic-portalId").val() == "" ? 0 : $("#dynamic-portalId").val(),
        CatalogId: $("#dynamic-catalogId").val(),
        UserId: $("#dynamic-userId").val(),
        LocaleId: $("#dynamic-localeId").val(),
        ProductType: $("#dynamic-producttype").val(),
        AddOnProductSKUs: $("#dynamic-addonProductSKU").val(),
        BundleProductSKUs: $("#dynamic-bundleProductSKU").val(),
        Quantity: $("#dynamic-quantity").val(),
        GroupProductsQuantity: $("#dynamic-groupProductsQuantity").val(),
        GroupProductSKUs: $("#dynamic-groupProductSKUs").val(),
        ShippingId: $("#dynamic-shippingId").val() == "" ? 0 : $("#dynamic-shippingId").val(),
        OmsOrderId: $("#hdnManageOmsOrderId").val(),
        ConfigurableProductSKUs: $("#dynamic-configurableProductSKUs").val(),
        PersonalisedCodes: $("#dynamic-personalisedcodes").val(),
        PersonalisedValues: $("#dynamic-personalisedvalues").val(),
        GroupProductNames: $("#dynamic-groupProductNames").val(),
        AutoAddonSKUs: $("#dynamic-autoaddonskus").val(),
        AISavedDesigns: "",
        IsConfigurableProduct: $("#dynamic-configurableProductSKUs").val() != "" ? true : false,
        IsGroupProduct: $("#dynamic-groupProductsQuantity").val() != "" ? true : false,
        OrderLineItemsDesignId: 0
    }
    return bindCartItemModel;
}

if (window.addEventListener) {
    addEventListener("message", receiveArtifiMessage, false)
} else {
    attachEvent("onmessage", receiveArtifiMessage);
}

function loadArtifiIntegrationJS() {
    var url = "https://designer.artifi.net/script/sasintegration/artifiintegration.js";

    if (typeof Artifi === 'undefined') {
        $.getScript(url);
    }
}

function receiveArtifiMessage(event) {
    loadArtifiIntegrationJS();
    var origin = event.origin || event.originalEvent.origin;
    if (Artifi && !Artifi.isValidArtifiDomain(origin)) return;
    var eventObj = JSON.parse(event.data);
    var action = eventObj.action;
    var omsOrderId = $("#hdnManageOmsOrderId").val();
    if (omsOrderId == "" || typeof omsOrderId == 'undefined') {
        omsOrderId = 0;
    }
    //action = 'add-to-cart'
    switch (action) {
        case Artifi.Constant.addToCart:
            var data = eventObj.data;
            var isEdit = localStorage.getItem("isedit");
            var quantity = 0;
            if (data.quantity > 0)
                quantity = data.quantity;
            if (isEdit != "" && isEdit != null && isEdit != 'undefined') {
                $.ajax({
                    url: "/artifiorder/updateartificartitem?OrderLineItemsDesignId=" + data.custmizeProductId + "&AIImagePath=" + data.savedDesigns[0] + "&OmsOrderId=" + omsOrderId + "&sku=" + data.sku + "&quantity=" + quantity,
                    method: "GET",
                    dataType: "json",
                    async: false,
                    success: function (data) {
                        if (isEdit != "" && isEdit != null && isEdit != 'undefined') {
                            localStorage.removeItem("isedit");
                        }
                        Order.prototype.HideAsidePopUpPanel();
                        var omsOrderId = $("#hdnManageOmsOrderId").val();
                        if (omsOrderId == "" || typeof omsOrderId == 'undefined') {
                            omsOrderId = 0;
                        }
                        //Endpoint.prototype.GetShoppingCart(omsOrderId, function (res) {
                        //    $("#divShoppingCart").html(res);
                        //})
                    },
                    error: function (data) {
                        localStorage.removeItem("isedit");
                        console.log(data);
                    }
                });
            }
            else {
                var cartItem = sessionStorage.getItem("cartitem");
                cartItem = $.parseJSON(cartItem);
                var personalisedCodes = [];
                var personalisedValues = [];
                personalisedCodes.push('ArtifiDesignId');
                personalisedValues.push(data.custmizeProductId);
                personalisedCodes.push('ArtifiImagePath');
                if (data != 'undefined' && data.savedDesigns != null && data.savedDesigns != 'undefined')
                    personalisedValues.push(data.savedDesigns[0]);

                if (personalisedCodes == 'undefined' || personalisedCodes == null) {
                    personalisedCodes = "";
                }

                if (cartItem != null || typeof cartItem != 'undefined') {
                    cartItem.PersonalisedCodes = personalisedCodes.toString();
                    cartItem.PersonalisedValues = personalisedValues.toString();
                    cartItem.OmsOrderId = omsOrderId;
                }

                if (typeof data.sku != 'undefined' && data.sku != '' && data.productCode != data.sku) {
                    cartItem.ConfigurableProductSKUs = data.sku;
                }
                //if (typeof data.configurablesku != 'undefined' && data.configurablesku != '' && data.productCode != data.configurablesku) {
                //    cartItem.ConfigurableProductSKUs = data.configurablesku;
                //}

                if (cartItem != "" && cartItem != null && cartItem != 'undefined') {
                    $.ajax({
                        url: "/Order/AddToCartProduct",
                        data: JSON.stringify(cartItem),// cartItem,
                        method: "POST",
                        dataType: "json",
                        contentType: 'application/json; charset=utf-8',
                        async: false,
                        success: function (data) {
                            sessionStorage.removeItem("cartitem");
                            var orderId = $("#OmsOrderId").val();
                            orderId = orderId != "" && orderId != null ? orderId : 0;
                            ShowLoader();
                            Order.prototype.HideAsidePopUpPanel();
                            Order.prototype.AddToCartSuccessCallBack(data);
                        },
                        error: function (data) {
                            console.log(data);
                        }
                    });
                }
            }
            break;
        case "close-pop-up":
            if (window.artifiAddToCartSuccess == false) {
                closeArtifiWindow();
            }
            break;
        case "error":
            $(".personalizeDivCover").addClass("na");
            break;
    }
}

function ShowLoader() {
    $("#Single-loader-content-backdrop").show();
}


function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function ArtifiDesigner(custmizeproductid, userId, portalId, sku, productname) {
    var postData = { "portalId": portalId, "userId": userId };
    $.ajax({
        url: "/artifiorder/getartificredentials",
        method: "POST",
        data: JSON.stringify(postData),
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: false,
        success: function (data) {
            var _param = "";
            var productDetails = "";

            var editedItem = localStorage.getItem("edititem");
            if (editedItem != "" && editedItem != null && editedItem != 'undefined') {
                productDetails = JSON.parse(editedItem);
                sku = productDetails.sku;
                custmizeproductid = productDetails.custmizeproductid;
                productname = productDetails.productname;
                localStorage.removeItem("edititem");
                localStorage.setItem("isedit", "true");
            }


            var artifiCredentials = {
                websiteId: data.WebsiteCode,
                webApiclientKey: data.WebApiClientKey,
                jsUrl: data.JsUrl,
                userId: data.userId,
            }

            var configurableSKU = localStorage.getItem("configurablesku");
            if (configurableSKU != "" && configurableSKU != null && configurableSKU != 'undefined') {
                _param = {
                    divId: "Artifi-Designer",
                    websiteId: artifiCredentials.websiteId,
                    webApiclientKey: artifiCredentials.webApiclientKey,
                    sku: sku,
                    designId: custmizeproductid,
                    isGuest: true,
                    userId: artifiCredentials.userId,
                    height: "450px",
                    width: "100%",
                }
            }
            else {
                _param = {
                    divId: "Artifi-Designer",
                    websiteId: artifiCredentials.websiteId,
                    webApiclientKey: artifiCredentials.webApiclientKey,
                    productCode: sku,
                    designId: custmizeproductid,
                    isGuest: true,
                    userId: artifiCredentials.userId,
                    height: "450px",
                    width: "100%",
                }
            }
            var previewdata = $.parseJSON(localStorage.getItem("previewdata"));
            localStorage.removeItem("previewdata");
            if (previewdata != "" && previewdata != null && previewdata != 'undefined' && previewdata.ispreview == true) {

                _param.divId = "";
                _param.userId = _param.userId;
                _param.productCode = previewdata.sku;
                _param.designId = previewdata.custmisedproductid;
                var _url = Artifi.getPreviewUrl(_param);
                $('#getArtifiProduct').find('.modal-body').load(_url);
            }
            else {
                $("#designersku").empty();
                $("#getArtifiProduct .modal-header").html(" ");
                $("#Artifi-Designer-parent> h1").remove();
                var isEdit = localStorage.getItem("isedit");
                $("#Artifi-Designer-parent").prepend("<h1 class='product-name padding-left' style='color:#000;'>" + productname + "</h1>");
                $(".product-meta").find("#main-content").hide();
                Artifi.Initialize(_param);
            }
            return _param;
        },
        error: function (error) {
            console.log(error);
        }
    });
}

function LaunchArtifi() {
    $(".add-to-cart-popover").popover('hide');
    var portalId = $("#dynamic-portalId").val();

    var productname = localStorage.getItem("productname");

    productname = productname != "" && productname != 'undefined' ? productname : $("#dynamic-productName").val();
    portalId = portalId == "" || portalId == null || portalId == "undefined" ? $("#txtPortalName").data("portalid") : portalId;
    portalId = portalId == "" || portalId == null || portalId == "undefined" ? $("#PortalId").val() : portalId;
    ArtifiDesigner("", $("#hdnUserId").val(), portalId, localStorage.getItem("sku"), productname);
}

function EditDesign(orderlineitemsdesignid, userid, portalid, sku, configurableSku, productname, omsorderid, omsorderlineitemsid) {

    $('#Artifi-Designer').empty();
    //var productname = $("#" + orderlineitemsdesignid + "").val();
    var productDetails = {
        sku: sku,
        custmizeproductid: orderlineitemsdesignid,
        productname: productname
    }
    localStorage.setItem("sku", sku);
    localStorage.setItem("configurablesku", configurableSku);
    localStorage.setItem("edititem", JSON.stringify(productDetails));
    ZnodeBase.prototype.BrowseAsidePoupPanel('/ArtifiOrder/AddEditArtifiDesign?isEdit=true', 'getArtifiProduct');
}

function OpenXML(orderlineitemsdesignid) {
    var postData = { "portalId": $("#PortalId").val(), "userId": $("#hdnUserId").val() };
    $.ajax({
        url: "/artifiorder/getartificredentials",
        method: "POST",
        data: JSON.stringify(postData),
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: false,
        success: function (data) {

            var artifiCredentials = {
                websiteId: data.WebsiteCode,
                webApiclientKey: data.WebApiClientKey,
                jsUrl: data.JsUrl,
                userId: data.userId,
            }
            var xmlURL = "" + artifiCredentials.jsUrl + "/Designer/Services/RegenerateXMLFile?customizedProductId=" + orderlineitemsdesignid + "&websiteId=" + artifiCredentials.websiteId + "&webApiClientKey=" + artifiCredentials.webApiclientKey + "";
            window.open(xmlURL);
        },
        error: function (error) {
        }
    });
}

function DownloadArtifiPDF(event, designedId) {
    var postData = { "portalId": $("#PortalId").val(), "userId": $("#hdnUserId").val() };
    $.ajax({
        url: "/artifiorder/getartificredentials",
        method: "POST",
        data: JSON.stringify(postData),
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: false,
        success: function (data) {

            var artifiCredentials = {
                websiteId: data.WebsiteCode,
                webApiclientKey: data.WebApiClientKey,
                jsUrl: data.JsUrl,
                userId: data.userId,
            }
            var url = "" + artifiCredentials.jsUrl + "/Designer/Services/DownloadOutputFiles?customizedProductId=" + designedId + "&websiteId=" + artifiCredentials.websiteId + "&webApiClientKey=" + artifiCredentials.webApiclientKey + "";

            window.open(url);
            setTimeout(function () { ZnodeBase.prototype.HideLoader() }, 1000);

        },
        error: function (error) {
        }
    });
}

function GetArtifiCredentials(portalId) {
    if (portalId > 0 && portalId != NaN) {
        $.ajax({
            url: "/artifiorder/getartificredentials",
            method: "POST",
            data: { portalId: portalId },
            dataType: "json",
            async: false,
            success: function (data) {
                $.getScript("" + data.JsUrl + "/script/sasintegration/artifiintegration.js");
            },
            error: function (data) {
                console.log(data);
            }
        });
    }
}

function HideDesignID() {
    var cartCount = $("#hdnCartCount").val();
    var isdesignAvailable = $("#hdnDesignAvailable").val();
    if (typeof cartCount != 'undefined' && cartCount != null && cartCount != '') {
        if (isdesignAvailable == "True") {
            $("#artifi-design").show();
            $(".skudesigneId").show();
        }
    }
}
