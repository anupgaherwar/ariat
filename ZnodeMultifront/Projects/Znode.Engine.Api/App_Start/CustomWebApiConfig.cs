﻿using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Routing;

namespace Znode.Engine.Api
{
    public static class CustomWebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            //Custom Portal Detail Routes
            config.Routes.MapHttpRoute("custom-portal-list", "customportal/list", new { controller = "customportal", action = "getportallist" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Get) });
            config.Routes.MapHttpRoute("custom-portal-detail-list", "customportaldetail/list", new { controller = "customportal", action = "getcustomportaldetaillist" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Get) });
            config.Routes.MapHttpRoute("custom-portal-get", "customportal/getcustomportaldetail/{customPortalDetailId}", new { controller = "customportal", action = "getcustomportaldetail" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Get), customPortalDetailId = @"^\d+$" });
            config.Routes.MapHttpRoute("custom-portal-create", "customportal/create", new { controller = "customportal", action = "insertcustomportaldetail" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) });
            config.Routes.MapHttpRoute("custom-portal-update", "customportal/update", new { controller = "customportal", action = "updatecustomportaldetail" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Put) });
            config.Routes.MapHttpRoute("custom-portal-delete", "customportal/delete", new { controller = "customportal", action = "deletecustomportaldetail" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) });

            config.Routes.MapHttpRoute("custom-ariat-shippinglist", "ariatshipping/shippingratelist", new { controller = "ariatshipping", action = "shippingratelist" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Get) });
            config.Routes.MapHttpRoute("custom-ariat-getshippingratedetails", "ariatshipping/getshippingratedetails/{ariatCustomShippingChargesId}", new { controller = "ariatshipping", action = "getshippingratedetails" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Get), ariatCustomShippingChargesId = @"^\d+$" });
            config.Routes.MapHttpRoute("custom-ariat-createshippingdetails", "ariatshipping/createshippingrate", new { controller = "ariatshipping", action = "Create" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) });
            config.Routes.MapHttpRoute("custom-ariat-updateshippingdetails", "ariatshipping/editshippingrate", new { controller = "ariatshipping", action = "editshippingrate" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Put) });
            config.Routes.MapHttpRoute("custom-ariat-delete", "ariatshipping/deleteshippingrate", new { controller = "ariatshipping", action = "deleteshippingrate" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) });

            config.Routes.MapHttpRoute("custom-decopricing", "decorationprice/getdecorationprice", new { controller = "decorationprice", action = "getdecorationprice" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) });
            //Get znode sku from artifi sku
            config.Routes.MapHttpRoute("ariatproduct-getznodesku", "ariatproduct/getznodesku", new { controller = "ariatproduct", action = "ZnodeSKU" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) });
            config.Routes.MapHttpRoute("ariatuser-getadyenshippingaddress", "ariatuser/getadyenshippingaddress", new { controller = "ariatuser", action = "adyenshippingaddress" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) });
            config.Routes.MapHttpRoute("ariatorder-generateordernumber", "ariatorder/generateordernumber/{portalId}", new { controller = "ariatorder", action = "generateordernumber" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Get), portalId = @"^\d+$" } );

            config.Routes.MapHttpRoute("ariatreport-vouchers", "ariatreport/getvouchersreport", new { controller = "ariatreport", action = "getvouchersreport" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Get) });
            config.Routes.MapHttpRoute("ariataccount-fullcatalogproductlist", "ariataccount/fullcatalogproductlist", new { controller = "ariataccount", action = "fullcatalogproductlist" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Get) });
            config.Routes.MapHttpRoute("ariataccount-programcatalogproductlist", "ariataccount/programcatalogproductlist", new { controller = "ariataccount", action = "programcatalogproductlist" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Get) });
            config.Routes.MapHttpRoute("ariataccount-delete", "ariataccount/delete", new { controller = "ariataccount", action = "delete" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) });
            config.Routes.MapHttpRoute("ariataccount-publish", "ariataccount/publish", new { controller = "ariataccount", action = "publish" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) });
        }
    }
}