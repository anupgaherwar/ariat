﻿using AutoMapper;
using Znode.Custom.Api.Model;
using Znode.Custom.Data;
using Znode.Sample.Api.Model;

namespace Znode.Engine.Api
{
    public static class CustomAutoMapperConfig
    {
        public static void Execute()
        {
            Mapper.CreateMap<ZnodeCustomPortalDetail, CustomPortalDetailModel>().ReverseMap();
            Mapper.CreateMap<AriatShippingModel, ZnodeAriatCustomShippingCharge>().ReverseMap();
            Mapper.CreateMap<AriatProgramCatalogProduct, AriatProgramCatalogProductModel>()
                .ForMember(d => d.IsCustomizable, opt => opt.MapFrom(src => src.IsCustomizableFlag))
                .ForMember(d => d.ColorSpecific, opt => opt.MapFrom(src => src.SpecificColor))
                .ForMember(d => d.Waterproof, opt => opt.MapFrom(src => src.WaterproofFlag))
                .ForMember(d => d.SafetySpecification, opt => opt.MapFrom(src => src.SafetySpecifications));
        }
    }
}