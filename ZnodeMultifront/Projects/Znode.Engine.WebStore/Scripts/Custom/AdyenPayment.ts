﻿class AdyenPayment extends ZnodeBase {

    Init() {

    }

    GetDropin(paymentCode) {
        Endpoint.prototype.GetDropin(paymentCode, function (res) {
            if (res.status) {
                $("#adyenPaymentReponse").html(res.html);
                User.prototype.AriatBindState();
                $("#ccAddressDetails").show();
                $("#div-CreditCard").hide();
            } else {
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(res.message, "error", isFadeOut, fadeOutTime);
            }
        });
    }

}