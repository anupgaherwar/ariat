﻿var counter = 0;
var isValidVideo: boolean = true;
var videoCounter = 0;
var isVideoRemoved: boolean = false;

interface FileReaderEventTarget extends EventTarget {
    result: string
}
interface FileReaderEvent extends ProgressEvent {
    target: FileReaderEventTarget;
    getMessage(): string;
}

class CustomProduct extends ZnodeBase {   
    constructor() {
        super();      
    }

    Init() {
    }

    BuildForm(): any {
        var sku: string = $("#hdnSku").val();
        CustomEndpoint.prototype.WriteReviewBV(sku, function (response) {
            var htmlString = response.html;
            if (htmlString != "" && typeof htmlString != "undefined" && htmlString != null) {
                $('#custom-modal1').find('#custom-content1').empty();
                $('#custom-modal1').find('#custom-content1').append(htmlString);
                $('#custom-modal1').modal('show');
            }
        });
    }

    BuildPhotoForm(): any {
        var sku: string = $("#hdnSku").val();
        CustomEndpoint.prototype.GetPhotoDataFromBV(sku, function (response) {
            var htmlString = response.html;

            if (htmlString != "" && typeof htmlString != "undefined" && htmlString != null) {
                $('#myModal').find('#myModal-content').empty();
                $('#myModal').find('#myModal-content').append(htmlString);
                $('#custom-modal1').css('display', 'none'); // hide bv-form
                $('#myModal').modal('show');          
            }

        });
    }

    BuildVideoForm(): any {
        var videoString: string = $("#videokeyValue").val();
        CustomEndpoint.prototype.GetVideoDataFromBV(videoString, function (response) {
            var htmlString = response.html;
            if (htmlString != "" && typeof htmlString != "undefined" && htmlString != null) {
                $('#myModalVideo').find('#myModalVideo-content').empty();
                $('#myModalVideo').find('#myModalVideo-content').append(htmlString);
                $('#custom-modal1').css('display', 'none'); // hide bv-form
                $('#myModalVideo').modal('show');
                $(".videocaptionclass").css('display', 'none');       
            }
        });

    }

    AppendVideo(control): any {
        if (videoCounter == 0) {
            CustomProduct.prototype.AddVideo();
        } else {
            CustomProduct.prototype.AppendVideoImage();
        }
    }

    AddVideo(): any {
        var url = $('#videourl').val();
        if (url != undefined || url != '' && videoCounter == 0) {
            var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=|\?v=)([^#\&\?]*).*/;
            var match = url.match(regExp);
            if (match && match[2].length == 11) {
                $(".iframeclass").css('display', 'block');
                $(".BvVideoImageclass").css('display', 'none');
                $('#iframeVideo').attr('src', 'https://www.youtube.com/embed/' + match[2] + '?autoplay=1&enablejsapi=1');
                $(".videocaptionclass").css('display', 'block');
                isValidVideo = true;
                videoCounter = 1
            } else {
                $(".iframeclass").css('display', 'block');
                $(".BvVideoImageclass").css('display', 'none');
                $('#iframeVideo').attr('src', 'https://www.youtube.com/embed/' + url + '?autoplay=1&enablejsapi=1');
                $("#addVideoButton").click(function (event) {
                    event.preventDefault();
                });
                isValidVideo = false;
                videoCounter = 1
            }
        }
        $("#removeVideo").css('display', 'block');
        console.log('addvido');


    }

    AppendVideoImage() {
        if (videoCounter == 1 && isValidVideo == true) {
            $('#myModalVideo').modal('hide');
            var video_thumbnail;
            var videoValue = $('#videourl').val();
            var videoCaptionBV = $('#VideoCaption').val();
            var videoUrlBV = $('#VideoURL').val();
            var videocaptionValue = $('#videocaption').val();
            var p = document.getElementById('video');
            var videoImageUrl = videoValue.match(/youtube\.com.*(\?v=|\/embed\/)(.{11})/).pop();
            video_thumbnail = "http://img.youtube.com/vi/" + videoImageUrl + "/0.jpg"
            var img = document.createElement('img');
            img.setAttribute("height", "64px");
            img.setAttribute("width", "64px");
            img.src = video_thumbnail;
            var element = document.createElement("input");
            element.setAttribute("type", "hidden");
            element.setAttribute("value", videoValue);
            element.setAttribute("name", videoUrlBV);
            var element1 = document.createElement("input");
            element1.setAttribute("type", "hidden");
            element1.setAttribute("value", videocaptionValue);
            element1.setAttribute("name", videoCaptionBV);
            var d = document.createElement('div');
            d.className = "newvideodiv";
            d.id = "newvideoid";
            p.appendChild(d);
            d.append(img);
            d.append(element);
            d.append(element1);
            videoCounter = 0;
        }

    }

    RemoveVideoFromModal() {
        $("#iframeVideo").attr("src", "");
        $(".iframeclass").css('display', 'none');
        $(".BvVideoImageclass").css('display', 'block');
        $(".videocaptionclass").css('display', 'none');
        console.log("removevideo");
        $("#removeVideo").css('display', 'none');
        videoCounter = 0;
        isValidVideo = true;
    }

    readURL(input): any {
        var fd = new FormData();
        var files = (<HTMLInputElement>$("#chooseButton").get(0)).files[0]
        fd.append('UploadedImage', files);
        $.ajax({
            url: "/CustomProduct/GetImageUrlFromBV?form=" + fd,
            type: 'post',
            data: fd,
            contentType: false,
            processData: false,
            success: function (response) {
                if (response != 0) {
                    $("#BvImage").attr("src", response).width(150).height(200);
                    $(".captionTextBox").show();
                    $("#chooseButton").hide();
                    $("#addphotobutton").show();
                    //$('.artist-collection-photo').show();
                } else {
                    alert('file not uploaded');
                }
            },
        });
    }

    AppendImage(control): any {
        $('#myModal').modal('hide');
        counter = counter + 1;
        var photoKey: string = $('#photokeyValue').val();
        var photoUrl = photoKey.split('[')[counter].split(',')[1]
        var photoCaption = photoKey.split('[')[counter].split(',')[2].replace(']', "")
        console.log("The count value is" + counter);
        var imageValue = $('#BvImage').attr('src');
        var captionValue = $("#photocaption").val();
        var element = document.createElement("input");

        //Assign different attributes to the element.
        element.setAttribute("type", "hidden");
        element.setAttribute("value", imageValue);
        element.setAttribute("name", photoUrl);
        element.setAttribute("id", photoUrl);

        var element1 = document.createElement("input");

        //Assign different attributes to the element.
        element1.setAttribute("type", "hidden");
        element1.setAttribute("value", captionValue);
        element1.setAttribute("name", photoCaption);
        element1.setAttribute("id", photoCaption);
        var d = document.createElement('div');
        d.className = "newDiv";
        var img = document.createElement('img');
        img.setAttribute("height", "64px");
        img.setAttribute("width", "64px");
        img.src = imageValue;
        img.className = "top_right_corner";
        d.id = "new1";
        var p = document.getElementById('new');
        var aTag = document.createElement('a');
        aTag.setAttribute('href', "yourlink.htm");
        aTag.innerText = "link text";
        p.appendChild(d);
        d.append(img);
        var div = $("div[id=new1]");
        div.append("<a class=\"upload-images-close dirtyignore\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Remove\" onclick='Product.prototype.RemoveImage(\"" + photoUrl + "\",(\"" + photoCaption + "\")'><i class='z-close-circle'></i></a>");
        d.append(element);
        d.append(element1);
    }

    RemoveImage(photoUrl, photoCaption): any {
        var a = photoCaption;
        var b = photoUrl
    }

    StarRating(control): any {
        var id = $(control).attr("id");
        var star: number = parseInt($(control).attr("id"));
        var className: string = $(control).attr("class");
        if (className.match("unchecked")) {
            for (var a = 1; a <= star; a += 1) {
                $(".star" + a).removeClass('fa fa-star unchecked').addClass('fa fa-star checked');
            }
            //$(control).removeClass('fa fa-star unchecked');
            //$(control).addClass('fa fa-star checked');
            $("#rating").val(id);
        }
        if (className.match("checked")) {
            $(control).removeClass('fa fa-star checked');
            $(control).addClass('fa fa-star unchecked');
            $("#rating").val(id);
        }
    }

    SubmitBVForm(): any {
        var starRate = $('.full').length;
        //var photoUrlCaption = new Object();
        //photoUrlCaption = { Key: "Hello", Value: "World" };
        //photoUrlCaption = { Key: "Hello1", Value: "World1"}
        var data = {};
        data["PhotoUrlCaption[" + 1 + "].Key"] = "textBoxTitle";
        data["PhotoUrlCaption[" + 1 + "].Value"] = "textboxValue";
        var nickName = $("#usernickname").length;
        if (starRate > 0) {
            $('span#starRating').removeClass('field-validation-valid');
        } else {
            $('span#starRating').text("Click here to Rate!!")
        }
        if (starRate > 0) {
            var submitBVModel = {
                rating: $('.full').length,
                ReviewTitle: $("#title").val(),
                Review: $("#reviewtext").val(),
                IsRecommedToFriend: $("input[name='isrecommended']:checked").val(),
                usernickname: $("#usernickname").val(),
                UserLocation: $("#userlocation").val(),
                Email: $("#useremail").val(),
                PhotoUrl_1: $("#photourl_1").val() ,
                PhotoCaption_1: $("#photocaption_1").val(),
                PhotoUrl_2: $("#photourl_2").val(),
                PhotoCaption_2: $("#photocaption_2").val(),
                PhotoUrl_3: $("#photourl_3").val(),
                PhotoCaption_3: $("#photocaption_3").val(),
                PhotoUrl_4: $("#photourl_4").val(),
                PhotoCaption_4: $("#photocaption_4").val(),
                PhotoUrl_5: $("#photourl_5").val(),
                PhotoCaption_5: $("#photocaption_5").val(),
                PhotoUrl_6: $("#photourl_6").val(),
                PhotoCaption_6: $("#photocaption_6").val(),
                VideoUrl_1: $("#videourl_1").val(),
                VideoCaption_1: $("#videocaption_1").val()
                //ProductId: $("#hdnSku").val()
            }
            $.ajax({
                type: "POST",
                url: "/CustomProduct/SubmitBVForm",
                data: submitBVModel,
                async: false,
                success: function (response) {
                }
            });
        }
     }
    
    changeImage(): any {
        $(".captionTextBox").hide();
        $("#BvImage").attr("src", "/Content/Images/no-image.png");
        $("#chooseButton").show();
    }

    closePhotoModal(): any {
        $('#custom-modal1').css('display', 'block'); // show bv-form
    }

    closeVideoModal(): any {
        console.log('close modal');
        $('#custom-modal1').css('display', 'block'); // show bv-form
    }

}
    $(document).on("click", "#mainModal", function() {
        videoCounter = 0;
    });



