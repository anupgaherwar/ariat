﻿class AriatCheckout extends ZnodeBase {
    constructor() {
        super();
    }

    Init() {
    }
}
Checkout.prototype.CalculateShipping = function (ShippingclassName): any {
    var form = $("#form0")
    if (form.length > 0) {
        var shippingOptionId = $("input[name='ShippingOptions']:checked").val();
        var shippingAddressId = $("#shipping-content").find("#AddressId").val();
        var shippingCode = $("input[name='ShippingOptions']:checked").attr("data-shippingCode");
        var additionalInstruction = $('#AdditionalInstruction').val();
        var isQuoteRequest = $('#IsQuoteRequest').val();
        var poNumber = $("#JobName").val();
        $("#hndShippingclassName").val(ShippingclassName);
        $("#messageBoxContainerId").hide();
        if (ShippingclassName.toLowerCase() == (Constant.ZnodeCustomerShipping).toLowerCase()) {
            $("#customerShippingDiv").show();
        }
        else {
            $("#customerShippingDiv").hide();
        }
        if (shippingOptionId != null || shippingOptionId != "") {
            if (form.attr('action').match("shippingOptionId")) {
                var url = form.attr('action').split('?')[0];
                form.attr('action', "")
                form.attr('action', url);
            }
            //CustomEndpoint.prototype.WriteReviewBV(sku, function (response) {
            //    Cust
            form.attr('action', form.attr('action') + "?shippingOptionId=" + shippingOptionId + "&shippingAddressId=" + shippingAddressId + "&shippingCode=" + shippingCode + "&additionalInstruction=" + additionalInstruction + "" + "&isQuoteRequest=" + isQuoteRequest + "&poNumber=" + poNumber);
            form.submit();
        }
    }
}
