﻿var counter = 0;
var isValidVideo: boolean = true;
var videoCounter = 0;
var isVideoRemoved: boolean = false;
var nextOffeset: number = 0;
var inventoryQtyMessage: string = "99+";
var existingJson: any;
var backOrderDateHeader: string = "Estimated Availability Date : ";

declare interface Math {
    trunc(x: number): number;
}

interface Array<T> {
    [x: string]: any;
}

interface HTMLInputElement extends HTMLElement {
    options: any;
    selectedIndex: any;

}
interface FileReaderEventTarget extends EventTarget {
    result: string
}
interface FileReaderEvent extends ProgressEvent {
    target: FileReaderEventTarget;
    getMessage(): string;
}

declare interface String {
    includes(x: string): boolean;
}

class AriatProduct extends ZnodeBase {
    constructor() {
        super();
    }

    Init() {
    }

    BuildForm(): any {
        var sku: string = $("#hdnSku").val();
        CustomEndpoint.prototype.WriteReviewBV(sku, function (response) {
            var htmlString = response.html;
            if (htmlString != "" && typeof htmlString != "undefined" && htmlString != null) {
                $('#custom-modal1').find('#custom-content1').empty();
                $('#custom-modal1').find('#custom-content1').append(htmlString);
                $('#custom-modal1').modal('show');
            }
        });
    }

    BuildPhotoForm(): any {
        var sku: string = $("#hdnSku").val();
        CustomEndpoint.prototype.GetPhotoDataFromBV(sku, function (response) {
            var htmlString = response.html;
            if (htmlString != "" && typeof htmlString != "undefined" && htmlString != null) {
                $('#myModal').find('#myModal-content').empty();
                $('#myModal').find('#myModal-content').append(htmlString);
                $('#myModal').modal('show');

            }

        });
    }

    BuildVideoForm(): any {
        var videoString: string = $("#videokeyValue").val();
        CustomEndpoint.prototype.GetVideoDataFromBV(videoString, function (response) {
            var htmlString = response.html;
            if (htmlString != "" && typeof htmlString != "undefined" && htmlString != null) {
                $('#myModalVideo').find('#myModalVideo-content').empty();
                $('#myModalVideo').find('#myModalVideo-content').append(htmlString);
                $('#myModalVideo').modal('show');
                $(".videocaptionclass").css('display', 'none');

            }
        });

    }
    AppendVideo(control): any {
        if (videoCounter == 0) {
            AriatProduct.prototype.AddVideo();
        } else {
            AriatProduct.prototype.AppendVideoImage();
        }
    }

    AddVideo(): any {
        var url = $('#videourl').val();
        if (url != undefined && url != '' && videoCounter == 0) {
            var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=|\?v=)([^#\&\?]*).*/;
            var match = url.match(regExp);
            if (match && match[2].length == 11) {
                $(".iframeclass").css('display', 'block');
                $(".BvVideoImageclass").css('display', 'none');
                $('#iframeVideo').attr('src', 'https://www.youtube.com/embed/' + match[2] + '?autoplay=1&enablejsapi=1');
                $(".videocaptionclass").css('display', 'block');
                $("#removeVideo").css('display', 'block');
                isValidVideo = true;
                videoCounter = 1
            } else {
                $(".iframeclass").css('display', 'block');
                $(".BvVideoImageclass").css('display', 'none');
                $('#iframeVideo').attr('src', 'https://www.youtube.com/embed/' + url + '?autoplay=1&enablejsapi=1');
                $("#addVideoButton").click(function (event) {
                    event.preventDefault();
                });
                isValidVideo = false;
                videoCounter = 1
            }
        }
    }


    AppendVideoImage() {
        if (videoCounter == 1 && isValidVideo == true) {
            $('#myModalVideo').modal('hide');
            var video_thumbnail;
            var videoValue = $('#videourl').val();
            var videoCaptionBV = $('#VideoCaption').val();
            var videoUrlBV = $('#VideoURL').val().trim();
            var videocaptionValue = $('#videocaption').val();
            var p = document.getElementById('video');
            var videoImageUrl;
            if (videoImageUrl = videoValue.match(/(\?|&)v=([^&#]+)/)) {
                videoImageUrl = videoImageUrl.pop();
            } else if (videoImageUrl = videoValue.match(/(\.be\/)+([^\/]+)/)) {
                videoImageUrl = videoImageUrl.pop();
            } else if (videoImageUrl = videoValue.match(/(\embed\/)+([^\/]+)/)) {

                videoImageUrl = videoImageUrl.pop().replace('?rel=0', '');
            }

            video_thumbnail = "http://img.youtube.com/vi/" + videoImageUrl + "/0.jpg"
            var img = document.createElement('img');
            img.setAttribute("height", "64px");
            img.setAttribute("width", "64px");
            img.src = video_thumbnail;
            var element = document.createElement("input");
            element.setAttribute("type", "hidden");
            element.setAttribute("value", videoValue);
            element.setAttribute("name", videoUrlBV);
            element.setAttribute("id", videoUrlBV);
            var element1 = document.createElement("input");
            element1.setAttribute("type", "hidden");
            element1.setAttribute("value", videocaptionValue);
            element1.setAttribute("name", videoCaptionBV);
            element1.setAttribute("id", videoCaptionBV);
            var d = document.createElement('div');
            d.className = "newvideodiv";
            d.id = "newvideoid";
            p.appendChild(d);
            d.append(img);
            d.append(element);
            d.append(element1);
            videoCounter = 0;
        }

    }

    RemoveVideoFromModal() {
        $("#iframeVideo").attr("src", "");
        $(".iframeclass").css('display', 'none');
        $(".BvVideoImageclass").css('display', 'block');
        $(".videocaptionclass").css('display', 'none');
        videoCounter = 0;
        isValidVideo = true;
    }

    NextClick(): any {
        var currentTotalCount = $("#CurrentTotalCount").val();
        var totalResult = $("#totalResult").val();
        var curentOffset: number = $("#CuurentOffset").val();
        var limit = $("#hdnLimit").val()
        var offset: number = Number(curentOffset) + Number(limit);
        var sku: string = $("#hdnSku").val();
        var isAjax: boolean = false;
        var e = document.getElementById("bv-dropdown") as HTMLInputElement;
        var attribute: string = e.options[e.selectedIndex].id;
        var value: string = e.options[e.selectedIndex].value;
        var attributeForSelectToAppend: string = e.options[e.selectedIndex].id;
        if (attribute == "ratinghigh" || attribute == "ratinglow") {
            attribute = "rating";
        }
        var sort: string = attribute + ',' + value;
        if (Number(currentTotalCount) <= Number(totalResult))
            CustomEndpoint.prototype.GetReviewLists(sku, offset, isAjax, sort, function (response) {
                var htmlString = response.html;
                var listCount: number = response.listCount;
                var currentoffsetBV: number = response.currentoffsetBV + 1;
                var reviewOnpage: number = Number(listCount) + Number(offset);
                if (htmlString != "" && typeof htmlString != "undefined" && htmlString != null) {
                    $(".reviews").empty();
                    $(".reviews").append(htmlString);
                    if (currentoffsetBV != 0) {
                        document.getElementById('reviewNameTop').innerHTML = "";
                        document.getElementById('reviewNameTop').innerHTML = currentoffsetBV + '-' + reviewOnpage + ' ' + 'of' + ' ' + totalResult + ' ' + 'Reviews';
                    }
                    $("#bv-dropdown option[id=" + attributeForSelectToAppend + "]").prop("selected", "selected");
                    if (listCount < Number(limit)) {
                        $('#nextButton').prop('disabled', true)
                        $('#previousButton').prop('disabled', false)
                    }
                    else {
                        $('#nextButton').prop('disabled', false)
                        $('#previousButton').prop('disabled', false)
                    }
                    if (currentoffsetBV != 0) {
                        document.getElementById('reviewName').innerHTML = "";
                        document.getElementById('reviewName').innerHTML = currentoffsetBV + '-' + reviewOnpage + ' ' + 'of' + ' ' + totalResult + ' ' + 'Reviews';
                    }
                }
            });
    }

    PreviousClick(): any {
        var currentTotalCount = $("#CurrentTotalCount").val();
        var totalResult = $("#totalResult").val();
        var curentOffset: number = $("#CuurentOffset").val();
        var limit: number = $("#hdnLimit").val()
        var offset: number = Number(curentOffset) - Number(limit);
        var sku: string = $("#hdnSku").val();
        var isAjax: boolean = false;
        var e = document.getElementById("bv-dropdown") as HTMLInputElement;
        var attribute: string = e.options[e.selectedIndex].id;
        var value: string = e.options[e.selectedIndex].value;
        var attributeForSelectToAppend: string = e.options[e.selectedIndex].id;
        if (attribute == "ratinghigh" || attribute == "ratinglow") {
            attribute = "rating";
        }
        var sort: string = attribute + ',' + value;
        if (Number(currentTotalCount) <= Number(totalResult))
            CustomEndpoint.prototype.GetReviewLists(sku, offset, isAjax, sort, function (response) {
                var htmlString = response.html;
                var listCount = response.listCount;
                var currentoffsetBV: number = response.currentoffsetBV + 1;
                var reviewOnpage: number = Number(listCount) + Number(offset);
                if (htmlString != "" && typeof htmlString != "undefined" && htmlString != null) {
                    $(".reviews").empty();
                    $(".reviews").append(htmlString);
                    $("#bv-dropdown option[id=" + attributeForSelectToAppend + "]").prop("selected", "selected")
                    if (currentoffsetBV != 0) {
                        document.getElementById('reviewNameTop').innerHTML = "";
                        document.getElementById('reviewNameTop').innerHTML = currentoffsetBV + '-' + reviewOnpage + ' ' + 'of' + ' ' + totalResult + ' ' + 'Reviews';
                    }
                    if (offset != 0) {
                        $('#previousButton').removeAttr('disabled');
                    }
                    if (currentoffsetBV != 0) {
                        document.getElementById('reviewName').innerHTML = "";
                        document.getElementById('reviewName').innerHTML = currentoffsetBV + '-' + reviewOnpage + ' ' + 'of' + ' ' + totalResult + ' ' + 'Reviews';
                    }
                }
            });

    }

    readURL(input): any {
        var fd = new FormData();
        var files = (<HTMLInputElement>$("#chooseButton").get(0)).files[0]
        fd.append('UploadedImage', files);
        $.ajax({
            url: "/AriatProduct/GetImageUrlFromBV?form=" + fd,
            type: 'post',
            data: fd,
            contentType: false,
            processData: false,
            success: function (response) {
                if (response != 0) {
                    $("#BvImage").attr("src", response).width(150).height(200);
                    $(".captionTextBox").show();
                    $("#chooseButton").hide();
                    $("#addphotobutton").show();
                } else {
                    alert('file not uploaded');
                }
            },
        });
    }

    AppendImage(control): any {
        $('#myModal').modal('hide');
        counter = counter + 1;
        var photoKey: string = $('#photokeyValue').val();
        var photoUrl = photoKey.split('[')[counter].split(',')[1].trim()
        var photoCaption = photoKey.split('[')[counter].split(',')[2].replace(']', "")
        console.log("The count value is" + counter);
        var imageValue = $('#BvImage').attr('src');
        var captionValue = $("#photocaption").val();
        var element = document.createElement("input");

        //Assign different attributes to the element.
        element.setAttribute("type", "hidden");
        element.setAttribute("value", imageValue);
        element.setAttribute("name", photoUrl);
        element.setAttribute("id", photoUrl);

        var element1 = document.createElement("input");

        //Assign different attributes to the element.
        element1.setAttribute("type", "hidden");
        element1.setAttribute("value", captionValue);
        element1.setAttribute("name", photoCaption);
        element1.setAttribute("id", photoCaption);
        var d = document.createElement('div');
        d.className = "newDiv";
        var img = document.createElement('img');
        img.setAttribute("height", "64px");
        img.setAttribute("width", "64px");
        img.src = imageValue;
        img.className = "top_right_corner";
        d.id = "new1";
        var p = document.getElementById('new');
        var aTag = document.createElement('a');
        aTag.setAttribute('href', "yourlink.htm");
        aTag.innerText = "link text";
        p.appendChild(d);
        d.append(img);
        var div = $("div[id=new1]");
        div.append("<a class=\"upload-images-close dirtyignore\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Remove\" onclick='Product.prototype.RemoveImage(\"" + photoUrl + "\",(\"" + photoCaption + "\")'><i class='z-close-circle'></i></a>");
        d.append(element);
        d.append(element1);
    }

    RemoveImage(photoUrl, photoCaption): any {
        var a = photoCaption;
        var b = photoUrl
    }

    IsFeatured(control): any {
        var attribute: string = control.options[control.selectedIndex].id
        var value: string = control.options[control.selectedIndex].value;
        var selectedid: string = control.options[control.selectedIndex].id;
        if (attribute == "ratinghigh" || attribute == "ratinglow") {
            attribute = "rating";
        }
        var currentTotalCount = $("#CurrentTotalCount").val();
        var totalResult = $("#totalResult").val();
        var curentOffset: number = $("#CuurentOffset").val();
        var limit = $("#CuurentLimit").val()
        var offset: number = 0;
        if (curentOffset == 0) {
            offset = 0;
        } else {
            offset = curentOffset;
        }
        var sku: string = $("#hdnSku").val();
        var isAjax: boolean = false;
        var sort: string = attribute + ',' + value

        if (Number(currentTotalCount) <= Number(totalResult))
            CustomEndpoint.prototype.GetReviewLists(sku, offset, isAjax, sort, function (response) {
                var htmlString = response.html;
                if (htmlString != "" && typeof htmlString != "undefined" && htmlString != null) {
                    $(".reviews").empty();
                    $(".reviews").append(htmlString);
                    var listCount = response.listCount;
                    var currentoffsetBV: number = response.currentoffsetBV + 1;
                    var reviewOnpage: number = Number(listCount) + Number(offset);
                    if (currentoffsetBV != 0) {
                        document.getElementById('reviewNameTop').innerHTML = "";
                        document.getElementById('reviewNameTop').innerHTML = currentoffsetBV + '-' + reviewOnpage + ' ' + 'of' + ' ' + totalResult + ' ' + 'Reviews';
                    }
                    $("#bv-dropdown > [id=" + selectedid + "]").attr("selected", "true");
                    if (offset != 0) {
                        $('#previousButton').removeAttr('disabled');
                    }
                    if (currentoffsetBV != 0) {
                        document.getElementById('reviewName').innerHTML = "";
                        document.getElementById('reviewName').innerHTML = currentoffsetBV + '-' + reviewOnpage + ' ' + 'of' + ' ' + totalResult + ' ' + 'Reviews';
                    }
                }
            });
    }

    FeedBack(control): any {
        var yesNoId = $(control).attr('id');
        var contentId;
        var spanValue;
        var spanText;
        if (yesNoId.includes("Yes")) {
            contentId = yesNoId.split('_')[1].replace("Yes", "");
            spanText = "Yes";
        }
        if (yesNoId.includes("No")) {
            contentId = yesNoId.split('_')[1].replace("No", "");
            spanText = "No";
        }
        var id: string = "helpfulness_" + contentId;
        var helpfullnesstype = $(control).attr('data-helpfullnesstype');
        $.ajax({
            url: "/AriatProduct/SubmitHelpfulVote",
            data: { "helpfullnesstype": helpfullnesstype, "contentId": contentId },
            type: 'POST',
            success: function (data) {
                if (!data.HasError) {
                    var yesNoSpanId = $(control).children().attr('id');
                    if (spanText == "Yes") {
                        spanValue = $("#ContentId_yes_" + contentId).text();
                    }
                    if (spanText == "No") {
                        spanValue = $("#ContentId_no_" + contentId).text();
                    }
                    var addCountToSpan: number = Number(spanValue) + 1;
                    $("#ContentId_yes_" + contentId).text(addCountToSpan);
                    if (addCountToSpan > 0 && yesNoId != null && yesNoId != "" && yesNoId != "undefined") {
                        $("#helpfulness_" + contentId).addClass("disabled");
                        localStorage.setItem(id, "true");
                    }
                }
                else {
                    ZnodeNotification.prototype.DisplayNotificationMessagesHelper(data.ErrorMessage, "error", isFadeOut, fadeOutTime);
                }
            }
        });
    }

    ReportInappropriate(control) {
        var inappropriateId = $(control).attr('id');
        var contentId;
        if (inappropriateId.includes("inappropriate")) {
            contentId = inappropriateId.split('_')[1].replace("inappropriate", "")
        }
        var id: string = "inappropriate_" + contentId;
        var reportType = $(control).attr('data-value');
        $.ajax({
            url: "/AriatProduct/ReportInappropriate",
            data: { "reportType": reportType, "contentId": contentId },
            type: 'POST',
            success: function (data) {
                if (!data.HasError) {
                    var yesNoSpanId = $(control).children().attr('id');
                    if (inappropriateId != null && inappropriateId != "" && inappropriateId != "undefined") {
                        $("#inappropriate_" + contentId).addClass("disabled");
                        localStorage.setItem(id, "true");
                    }
                }
                else {
                    ZnodeNotification.prototype.DisplayNotificationMessagesHelper(data.ErrorMessage, "error", isFadeOut, fadeOutTime);
                }
            }
        });
    }

    DisableFeedBack() {
        var contentIds = $("#contentIds").val();
        if (contentIds != null && contentIds != "" && contentIds != "undefined") {
            var contentArray = contentIds.split(',');
            for (var i = 0; i < contentArray.length; i++) {
                var id: string = "helpfulness_" + contentArray[i];
                var isAvaiable = localStorage.getItem(id);
                if (isAvaiable == "true") {
                    $("#helpfulness_" + contentArray[i]).addClass("disabled");
                }

                var reportId: string = "inappropriate_" + contentArray[i];
                var isreport = localStorage.getItem(reportId);
                if (isreport == "true") {
                    $("#inappropriate_" + contentArray[i]).addClass("disabled");
                }

                if (isAvaiable == "true" && isreport == "true") {
                    var id: string = "ContentId" + contentArray[i];
                    $("#ContentId" + contentArray[i]).addClass("disabled");
                }
            }
        }
    }


    public GetRating(sku): any {
        var skus = $("#skus").val();
        if (skus.length > 0) {
            CustomEndpoint.prototype.GetRating(skus, function (responce) {
                $.each(responce.data, function (index, value) {
                    if ($(".productRatingsku[data-sku='" + responce.data[index] + "']")) {
                        var item;
                        var item1;
                        var a = "productRating-" + responce.data[index].ProductSku;
                        var div = document.getElementById(a);
                        if (typeof (div) != 'undefined' && div != null) {
                            var aa = "spanproductRating-" + responce.data[index].ProductSku;
                            var rating: number = responce.data[index].AverageRating;
                            var spans = document.getElementById(aa)
                            var totalStar: number = 5;
                            var integral: number = Math.trunc(rating);
                            var fraction: number = Math.trunc((rating - parseInt(responce.data[index].AverageRating)) * 10);
                            var starRating = document.createElement('span')
                            starRating.setAttribute("class", "star-rating");
                            div.append(starRating);
                            for (item = 1; item <= integral; item++) {
                                var starFullSpan = document.createElement('span')
                                starFullSpan.setAttribute("class", "star-full-icon");
                                starRating.appendChild(starFullSpan)
                                totalStar = totalStar - 1;
                            }
                            if (fraction >= 5) {
                                var starHalfSpan = document.createElement('span')
                                starHalfSpan.setAttribute("class", "star-half-icon");
                                starRating.appendChild(starHalfSpan)
                                totalStar = totalStar - 1;
                            }
                            for (item1 = 1; item1 <= totalStar; item1++) {
                                var starEmptySpan = document.createElement('span')
                                starEmptySpan.setAttribute("class", "star-empty-icon");
                                starRating.appendChild(starEmptySpan)
                            }
                            var totalCount = (+ responce.data[index].TotalReviewCount)
                            var totalCountnode = document.createTextNode("(" + totalCount + ")");
                            var spaceNode = document.createTextNode(" ")
                            starRating.append(spaceNode)
                            starRating.append(totalCountnode)
                        }

                    }
                })
            });
        }
    }

    ReviewImagePopup(control): any {
        var src = $(control).attr("src");
        $('#reviewImage').attr("src", src);
        $('#reviewModal').modal('show');
    }

    ReviewStarRating(): any {
        var avgRating = $("#hdnAverage").val();
        var count = $("#hdnTotalCount").val();
        var countstring: string = (count);
        var avgRatingAsNumber: number = $("#hdnAverage").val();
        var item;
        var item1;
        if (avgRating.length > 0 && avgRating != 0) {
            var spans = document.getElementById("reviewStarRating")
            var totalStar: number = 5;
            var integral: number = Math.trunc(avgRatingAsNumber);
            var fraction: number = Math.trunc((avgRatingAsNumber - parseInt(avgRating)) * 10);
            var starRating = document.createElement('span')
            starRating.setAttribute("class", "star-rating");
            spans.append(starRating);
            for (item = 1; item <= integral; item++) {
                var starFullSpan = document.createElement('span')
                starFullSpan.setAttribute("class", "star-full-icon");
                starRating.appendChild(starFullSpan)
                totalStar = totalStar - 1;
            }
            if (fraction >= 5) {
                var starHalfSpan = document.createElement('span')
                starHalfSpan.setAttribute("class", "star-half-icon");
                starRating.appendChild(starHalfSpan)
                totalStar = totalStar - 1;
            }
            for (item1 = 1; item1 <= totalStar; item1++) {
                var starEmptySpan = document.createElement('span')
                starEmptySpan.setAttribute("class", "star-empty-icon");
                starRating.appendChild(starEmptySpan)
            }
            starRating.append("(" + countstring + ")");
            $("#reviewAchor").css('display', 'block');
        }

    }

    SubmitBVForm(): any {
        var status = $("#BVForm").valid();
        var starRate = $('.full').length;
        if (starRate > 0) {
            $('span#starRating').removeClass('field-validation-valid');
            $('span#starRating').text("");
        }
        else {
            $('span#starRating').text("Click here to Rate!!");
            return false;
        }
        if (status && starRate > 0) {
            var starRate = $('.full').length;
            var url = window.location.href;
            var data = {};
            data["PhotoUrlCaption[" + 1 + "].Key"] = "textBoxTitle";
            data["PhotoUrlCaption[" + 1 + "].Value"] = "textboxValue";
            var nickName = $("#usernickname").val();
            if (starRate > 0 && (nickName != "undefined" && nickName != "" && nickName != null)) {
                var submitBVModel = {
                    rating: $('.full').length,
                    ReviewTitle: $("#title").val(),
                    Review: $("#reviewtext").val(),
                    IsRecommedToFriend: $("input[name='isrecommended']:checked").val(),
                    usernickname: $("#usernickname").val(),
                    UserLocation: $("#userlocation").val(),
                    Email: $("#useremail").val(),
                    PhotoUrl_1: $("#photourl_1").val(),
                    PhotoCaption_1: $("#photocaption_1").val(),
                    PhotoUrl_2: $("#photourl_2").val(),
                    PhotoCaption_2: $("#photocaption_2").val(),
                    PhotoUrl_3: $("#photourl_3").val(),
                    PhotoCaption_3: $("#photocaption_3").val(),
                    PhotoUrl_4: $("#photourl_4").val(),
                    PhotoCaption_4: $("#photocaption_4").val(),
                    PhotoUrl_5: $("#photourl_5").val(),
                    PhotoCaption_5: $("#photocaption_5").val(),
                    PhotoUrl_6: $("#photourl_6").val(),
                    PhotoCaption_6: $("#photocaption_6").val(),
                    VideoUrl_1: $("#videourl_1").val(),
                    VideoCaption_1: $("#videocaption_1").val(),
                    ProductSKU: $("#hdnSku").val()
                }
                $.ajax({
                    type: "POST",
                    url: "/AriatProduct/SubmitBVForm",
                    data: submitBVModel,
                    async: false,
                    success: function (response) {
                        if (response.HasError) {
                            $("#usernicknamevalidation").html(response.responseText);
                            $('#custom-modal1').modal({ backdrop: 'static' });
                        }
                        else {
                            $('#custom-modal1').modal("hide");
                            window.location.href = url;
                        }

                    },

                    error: function (error) {
                        $("#usernicknamevalidation").html(error.responseText);
                        return false;
                    }
                });
            }
        }
    }

    OnAttributeChange(control): any {
        var productId = $("#scrollReview form").children("#dynamic-parentproductid").val();
        var Codes = [];
        var Values = [];
        var sku = $("#dynamic-configurableproductskus").val();
        var ParentProductSKU = $("#dynamic-sku").val();
        var selectedCode = $(control).attr('code');
        var selectedValue = $(control).attr('value')
        var existingBreadCrumbHtml = "";
        Codes.push($(control).attr('code'));
        Values.push($(control).attr('value'));
        AriatProduct.prototype.BindingAttributes(productId, Codes, Values, sku, ParentProductSKU, selectedCode, selectedValue, existingBreadCrumbHtml);
    }

    BindingAttributes(productId, Codes, Values, sku, ParentProductSKU, selectedCode, selectedValue, existingBreadCrumbHtml) {

        $("select.ConfigurableAttribute").each(function () {
            var selectAttributeCode: boolean = Codes.includes($(this).attr('id'));
            //var selectAttributeValues = Values.includes($(this).val());
            if (!selectAttributeCode) {
                Values.push($(this).val());
                Codes.push($(this).attr('id'));
            }

        });

        $(" input.ConfigurableAttribute:checked").each(function () {
            var inputAttributeCode: boolean = Codes.includes($(this).attr('code'));
            //var inputAttributeValues = Values.includes($(this).val());
            if (!inputAttributeCode) {
                Values.push($(this).val());
                Codes.push($(this).attr('code'));
            }
        });

        $("a.ConfigurableAttribute.active").each(function () {
            var activeValue = $(this).attr('value');
            var activeCode = $(this).attr('code');
            var isActiveAttributeCode: boolean = Codes.includes(activeCode);
           // var isActiveAttributeValues = Values.includes(activeValue);
            if (!isActiveAttributeCode) {
                Codes.push(activeCode);
                Values.push(activeValue);
            }
        });

        var catgoryIds: string = $("#categoryIds").val();
        var parameters = {
            "SelectedCode": selectedCode,
            "SelectedValue": selectedValue,
            "SKU": sku,
            "Codes": Codes.join(),
            "Values": Values.join(),
            "ParentProductId": productId,
            "ParentProductSKU": ParentProductSKU,
            "IsQuickView": $("#isQuickView").val(),
            "IsProductEdit": $('#isProductEdit').val(),
            "ParentOmsSavedCartLineItemId": $('#ParentOmsSavedCartLineItemId').val()
        }

        if ($("#breadCrumb") != undefined && $("#breadCrumb").length > 0 && $("#breadCrumb").html().length > 0) {
            existingBreadCrumbHtml = $("#breadCrumb").html();
        }

        Endpoint.prototype.GetProduct(parameters, function (res) {
            $("#layout-product").replaceWith(res);
            if (existingBreadCrumbHtml.length > 0) {
                $("#breadCrumb").html(existingBreadCrumbHtml)
            } else {
                $("#categoryIds").val(catgoryIds);
                isFromCategoryPage = localStorage.getItem("isFromCategoryPage");
                Product.prototype.GetProductBreadCrumb(parseInt(window.sessionStorage.getItem("lastCategoryId"), 10), $("#isQuickView").val());
            }
            $("#breadcrumb-productname").html($(".product-name").html())
        });
    }

    OnAttributeSelect(control): any {
        var productId = $("#scrollReview form").children("#dynamic-parentproductid").val();
        var Codes = [];
        var Values = [];
        var sku = $("#dynamic-configurableproductskus").val();
        var ParentProductSKU = $("#dynamic-sku").val();
        var selectedCode = $(control).attr('code');
        var selectedValue = $(control).val();
        var existingBreadCrumbHtml = "";
        AriatProduct.prototype.BindingAttributes(productId, Codes, Values, sku, ParentProductSKU, selectedCode, selectedValue, existingBreadCrumbHtml);
    }


    changeImage(): any {
        $(".captionTextBox").hide();
        $("#BvImage").attr("src", "/Content/Images/no-image.png");
        $("#chooseButton").show();
    }


    UpdatePrice(priceData: any): any {
        existingJson = priceData;

        if ($(".edit-design-content").find("#Artifi-Designer").children().length > 0) {
            var quantity = $("#headerQuantity-edit").val();
        } else if ($("#Artifi-Designer").children().length > 0 && $("#hdnShowPopup").length && $("#hdnShowPopup").val().toLowerCase() == 'false') {
            quantity = $("#headerQuantity").val();
        } else {
            quantity = $("#Quantity").val();
        }
        var viewModel = {
            SKU: priceData.sku,
            Name: priceData.productName,
            CustomizedJson: JSON.stringify(priceData.customizedJson),
            RegularLogoCount: priceData.regularLogoCount,
            LargeLogoCount: priceData.largeLogoCount,
            TotalTextLineCount: priceData.totalTextLineCount,
            Quantity: quantity,
            LogoSKU: "Logo",
            LargeLogoSKU: "LargeLogo",
            TextLineSKU: "TextLines",
        };
        $.ajax({
            type: "POST",
            url: "/AriatProduct/GetDecorationPrice",
            data: viewModel,
            async: false,
            success: function (response) {
                if (priceData != "" || priceData != null || priceData != 'undefined') {
                    if (document.getElementById('seeDetails').style.display == 'block' && priceData.regularLogoCount == 0 && priceData.largeLogoCount == 0 && priceData.totalTextLineCount == 0) {
                        $("#seeDetails").css('display', 'none');
                    }
                    if (priceData.regularLogoCount > 0 || priceData.largeLogoCount > 0 || priceData.totalTextLineCount > 0) {
                        $("#seeDetails").css('display', 'block');
                    }
                }
                $("#decorationPriceDetails").html(response.html)
            },
            error: function () {
            }
        });
    }

    UpdatePriceFromZnode(): any {
        var url_string = location.href;
        var url = new URL(url_string);
        var customizeproductid = url.searchParams.get('custmizeproductid');
        if (customizeproductid) {
            var sku = url.searchParams.get('configurablesku');
            Endpoint.prototype.UpdatePriceFromZnode(sku, customizeproductid, function (response) {
                if (response) {
                    var customizedJson = [];
                    var regularLogoCount = 0;
                    var largeLogoCount = 0;
                    var totalTextLineCount = 0;
                    var name;
                    response.lineItems.forEach(function (item) {
                        name = "";
                        item.PersonaliseValuesDetail.forEach(function (personaliseValuesDetail) {
                            if (personaliseValuesDetail.PersonalizeCode !== "ArtifiSKU" && personaliseValuesDetail.PersonalizeCode !== "ArtifiImagePath" && personaliseValuesDetail.PersonalizeCode !== "ArtifiDesignId") {
                                var json = JSON.parse(personaliseValuesDetail.PersonalizeValue);
                                if (json.image) {
                                    if (json.isLargeLocation) {
                                        largeLogoCount = largeLogoCount + 1;
                                    } else {
                                        regularLogoCount = regularLogoCount + 1;
                                    }

                                }

                                if (json.text) {
                                    totalTextLineCount = totalTextLineCount + json.text[0].noOfLines;
                                }
                                customizedJson.push(JSON.parse(personaliseValuesDetail.PersonalizeValue));
                            }
                        });

                    });
                    console.log(customizedJson);
                    var viewModel = {
                        sku: sku,
                        productName: name,
                        customizedJson: customizedJson,
                        regularLogoCount: regularLogoCount,
                        largeLogoCount: largeLogoCount,
                        totalTextLineCount: totalTextLineCount,
                    };
                    AriatProduct.prototype.UpdatePrice(viewModel);
                }
            });
        }
    }

    GetZnodeSKU(artifiSKU: any, callbackMethod): string {
        var attributes = new Object();

        $("a.ConfigurableAttribute.active").each(function () {
            attributes[$(this).attr('code')] = $(this).attr('value');
        });
        $("select.ConfigurableAttribute").each(function () {
            attributes[$(this).attr('id')] = $(this).val();
        });
        var url_string = location.href;
        var url = new URL(url_string);
        var val = url.searchParams.get('custmizeproductid');
        if (val) {
            var configurableAttributes = $("#configurableAttributes").val();
            var result = $.parseJSON(configurableAttributes);
            $.each(result, function (k, v) {
                attributes[k] = v;
            });
        }

        Endpoint.prototype.GetZnodeSKU(artifiSKU, attributes, function (res) {
            if (callbackMethod) {
                callbackMethod(res);
            }
        });

        return "";
    }
}

Product.prototype.InventoryStatus = function (response): boolean {
    // In stock
    var invetoryMessge: string;
    var resMessage = response.message;
    var qtyText = '('+ response.Quantity +')' ;
    var backdorderDate = $("#BackOrderDate").val();
    var backorderMessage = $("#BackOrderMessage").val();
   // var inventoryCount = $("#InventoryCount").val(); 
    if (response.Quantity && response.Quantity > 0 && response.Quantity < 99) {
        if (backorderMessage == response.message) {
            response.message = "";
            invetoryMessge = Array(0).fill('\xa0').join('') + '<span class="product-count padding-left-5  backorder-text">' + backorderMessage + Array(1).fill('\xa0').join('') + qtyText +'</span>';
        } else { 
            invetoryMessge = response.message + "   " + '<span class="product-count padding-left-5">(' + response.Quantity + ')</span>';
        }
    }  
    else if (response.Quantity >= 99) {
        invetoryMessge = response.message + "   " + '<span class="product-count padding-left-5">(' + inventoryQtyMessage + ')</span>';
    }
    else {
        invetoryMessge = response.message;
    }
    if (backdorderDate != "undefined" && backdorderDate != "" && backdorderDate != null) {       
        if (backdorderDate != null && response.message == "" && response.Quantity<=0) {
            invetoryMessge = "";
            invetoryMessge = invetoryMessge + Array(0).fill('\xa0').join('') + '<span class="product-count  backorder-text">' + backOrderDateHeader + Array(0).fill('\xa0').join('') + backdorderDate + '</span>';
        }       
    }
    if (backorderMessage != "undefined" && backorderMessage != "" && backorderMessage != null && (backdorderDate == "undefined" ||  backdorderDate == "" || backdorderDate == null )) {
        if (backorderMessage == invetoryMessge) {
            invetoryMessge = "";
            invetoryMessge = Array(0).fill('\xa0').join('') + '<span class="product-count  backorder-text">' + backorderMessage + Array(0).fill('\xa0').join('') + '</span>';
           
        }
    }
    if (backorderMessage == resMessage) {
        var productId: number = parseInt($("#scrollReview form").children("#dynamic-productid").val());
        var addTocartButtonId = 'button-addtocart_' + productId;
        if (productId != null && productId > 0) {
            document.getElementById(addTocartButtonId).innerHTML = 'Backorder'
        }
    }

    if (response.success) {
        $("#dynamic-inventory").removeClass("error-msg");
        $("#dynamic-inventory").addClass("success-msg");
        // In stock
        $("#dynamic-inventory").show().html(invetoryMessge);
        $("#button-addtocart_" + response.data.productId).prop("disabled", false);
        $("#product-details-quantity input[name='Quantity']").attr('data-change', 'false');
        return true;
    } else {
        $("#dynamic-inventory").removeClass("success-msg");
        $("#dynamic-inventory").addClass("error-msg");
        // Out of stock
        $("#dynamic-inventory").show().html(invetoryMessge);
        return false;
    }
}

Product.prototype.DisplayAddToCartNotification = function (product): any {
    if (product != null && product != undefined) {
        var styleNumber = $("#hdnStyleNumber").val();
        if (styleNumber != "" && styleNumber != undefined && styleNumber != null) {
            product.SKU = styleNumber;
        }
        Endpoint.prototype.DisplayAddToCartNotification(JSON.stringify(product), function (response) {
            var element: any = $("#addToCartNotification");
            $("#addToCartNotification").removeAttr("style");
            $(window).scrollTop(0);
            $(document).scrollTop(0);
            if (element.length) {
                if (response !== "" && response != null) {
                    element.html(response);
                    setTimeout(function () {
                        element.fadeOut().empty();
                    }, fadeOutTime);
                }
            }
        });
    }
}
$(document).on("click", "#mainModal", function () {
    videoCounter = 0;
});

$(document).on("click", "#videoModal", function () {
    videoCounter = 0;
    isValidVideo = true;
});

$(document).on("click", "#layout-writereview .setrating label", function () {
    $("#layout-writereview .setrating label").removeClass("full").addClass("empty"); // Reset all to empty
    var title;
    var one = document.getElementById("1").classList;
    var two = document.getElementById("2").classList;
    var three = document.getElementById("3").classList;
    var four = document.getElementById("4").classList;
    var five = document.getElementById("5").classList;
    var stars = $(this).data("stars");
    $("#Rating").val(stars);
    for (var a = 1; a <= stars; a += 1) {
        $(".star" + a).removeClass("empty").addClass("full");
        title = $('.full').length
    }
    if (title == 1) {
        document.getElementById('starTile').innerHTML = "Poor";
        $('span#starRating').text("");
        one.add("red");
    }
    if (title == 2) {
        document.getElementById('starTile').innerHTML = "Fair";
        $('span#starRating').text("");
        if (two.contains("yellow") && one.contains("yellow")) {
            one.remove("yellow")
            two.remove("yellow")
        }
        one.remove("red");
        one.add("orange");
        two.add("orange");
    }
    if (title == 3) {
        document.getElementById('starTile').innerHTML = "Average";
        $('span#starRating').text("");
        if (three.contains("green") && two.contains("green") && one.contains("green")) {
            three.remove("green")
            two.remove("green")
            one.remove("green")
        }
        one.remove("orange");
        one.add("yellow");
        two.remove("orange");
        two.add("yellow");
        three.add("yellow");
    }
    if (title == 4) {
        document.getElementById('starTile').innerHTML = "Good";
        $('span#starRating').text("");
        if (one.contains("dark-green") && two.contains("dark-green") && three.contains("dark-green") && four.contains("dark-green")) {
            one.remove("dark-green")
            two.remove("dark-green")
            three.remove("dark-green")
            four.remove("dark-green")
        }
        one.remove("yellow");
        one.add("green");
        two.remove("yellow");
        two.add("green");
        three.remove("yellow");
        three.add("green");
        four.add("green");
    }
    if (title == 5) {
        $('span#starRating').text("");
        document.getElementById('starTile').innerHTML = "Excellent";
        one.remove("green");
        one.add("dark-green");
        two.remove("green");
        two.add("dark-green");
        three.remove("green");
        three.add("dark-green");
        four.remove("green");
        four.add("dark-green");
        five.add("dark-green");
    }
});

$(document).on("click", "#launch-modal", function () {
    var status = $("#BVForm").valid();
    var starRate = $('.full').length;
    if (starRate > 0) {
        $('span#starRating').removeClass('field-validation-valid');
        $('span#starRating').text("");
    }
    else {
        $('span#starRating').text("Click here to Rate!!");
        return false;
    }
    if (status && starRate > 0) {
        var starRate = $('.full').length;
        var url = window.location.href;
        var data = {};
        data["PhotoUrlCaption[" + 1 + "].Key"] = "textBoxTitle";
        data["PhotoUrlCaption[" + 1 + "].Value"] = "textboxValue";
        var nickName = $("#usernickname").val();
        var nickNamelength = $("#usernickname").val().length;
        if (starRate > 0 && (nickName != "undefined" && nickName != "" && nickName != null && nickNamelength >= 4)) {
            var submitBVModel = {
                rating: $('.full').length,
                ReviewTitle: $("#title").val(),
                Review: $("#reviewtext").val(),
                IsRecommedToFriend: $("input[name='isrecommended']:checked").val(),
                usernickname: $("#usernickname").val(),
                UserLocation: $("#userlocation").val(),
                Email: $("#useremail").val(),
                PhotoUrl_1: $("#photourl_1").val(),
                PhotoCaption_1: $("#photocaption_1").val(),
                PhotoUrl_2: $("#photourl_2").val(),
                PhotoCaption_2: $("#photocaption_2").val(),
                PhotoUrl_3: $("#photourl_3").val(),
                PhotoCaption_3: $("#photocaption_3").val(),
                PhotoUrl_4: $("#photourl_4").val(),
                PhotoCaption_4: $("#photocaption_4").val(),
                PhotoUrl_5: $("#photourl_5").val(),
                PhotoCaption_5: $("#photocaption_5").val(),
                PhotoUrl_6: $("#photourl_6").val(),
                PhotoCaption_6: $("#photocaption_6").val(),
                VideoUrl_1: $("#videourl_1").val(),
                VideoCaption_1: $("#videocaption_1").val(),
                ProductSKU: $("#hdnSku").val()
            }
            $.ajax({
                type: "POST",
                url: "/AriatProduct/SubmitBVForm",
                data: submitBVModel,
                async: false,
                success: function (response) {
                    if (response.HasError) {
                        $("#usernicknamevalidation").html(response.responseText);
                        $('#custom-modal1').modal({ backdrop: 'static' });
                    } else {
                        //window.location.href = url;
                        window.location.assign(url);
                    }

                }
            });
        }
    }
});







