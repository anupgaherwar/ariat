﻿var typingTimer;
$(window).on("load", function () {

    if (location.href.indexOf("EditArtifiProduct") > -1) {
        var _custmizeproductid = $("#artifi-design-call").data("custmizeproductid");

        if (_custmizeproductid == "0" || _custmizeproductid == "" || _custmizeproductid == 0 || _custmizeproductid == null)
            _custmizeproductid = getParameterByName("custmizeproductid");

        var sku = "";
        if (getParameterByName("groupsku") != "" && getParameterByName("groupsku") != null && getParameterByName("groupsku") != "undefined" && getParameterByName("quantity") != "" && getParameterByName("quantity") != null && getParameterByName("quantity") != "undefined") {
            $("#dynamic-groupproductskus").val(getParameterByName("sku"));
            $("#dynamic-groupproductsquantity").val(getParameterByName("quantity"));
            sku = getParameterByName("groupsku");
        }
        else if (getParameterByName("configurablesku") != "" && getParameterByName("configurablesku") != null && getParameterByName("configurablesku") != "undefined") {
            sku = getParameterByName("configurablesku");
        }
        else
            sku = getParameterByName("sku");

        var artifisku = getParameterByName("artifiSku");
        var styleNumber = getParameterByName("styleNumber");
        if (artifisku != "" && artifisku != null && artifisku != 'undefined') {
            $("#artifisku").val(artifisku);
        }
        if (styleNumber != "" && styleNumber != null && styleNumber != 'undefined') {
            $("#styleNumber").val(styleNumber)
        }       
        var userId = getParameterByName("userId");
        if (userId == undefined || userId == null || userId == "") {
            userId = localStorage.getItem("UserId");
        }
        var artifiCredentials = "";
        GetArtifiCredentials($("#hdnPortalId").val(), userId, false, function (response) {
            artifiCredentials =
                {
                    websiteId: response.websiteId,
                    webApiclientKey: response.webApiclientKey,
                    jsUrl: response.jsUrl,
                    userId: response.userId,
                }
        });
        var artifijs = "" + artifiCredentials.jsUrl + "//script/sasintegration/artifiintegration.js";
        var len = $('script').filter(function () {
            return ($(this).attr('src') == artifijs);
        }).length;

        if (_custmizeproductid !== "0" && _custmizeproductid !== "" && _custmizeproductid !== 0 && _custmizeproductid !== null)
            $.getScript(artifijs, function () { LaunchArtifi(artifiCredentials, _custmizeproductid, sku) });
    }
});

function GetUserId() {
    if (typeof $("#hdnUserId_LoggedIn").val() == 'undefined' || $("#hdnUserId_LoggedIn").val() == '') {
        return localStorage.getItem("UserId") == null ? 0 : localStorage.getItem("UserId");
    }
    return $("#hdnUserId_LoggedIn").val();
}

function GetExtraQueryString(znodeSku) {
    var clientCode = localStorage.getItem("AccountId") == null ? 0 : localStorage.getItem("AccountId");
    var styleNumber = $("#styleNumber").val();
    var queryString = "ClientCode=" + clientCode + "&EcommerceSku=" + znodeSku + "&StyleNumber=" + styleNumber;
    return queryString;
}

$(document).off("click", "#artifi-design-edit");
$(document).on("click", "#artifi-design-edit", function (e) {
    var _custmizeproductid = $(this).data('custmizeproductid');
    var configurablesku = $(this).data('configurablesku');
    var groupsku = $(this).data('groupsku');

    var sku = configurablesku != "" && configurablesku != 'undefined' ? configurablesku : groupsku;
    var queryStringKey = "sku";
    if (configurablesku != "" && typeof configurablesku != 'undefined')
        queryStringKey = "configurablesku";
    else if (groupsku != "" && typeof groupsku != 'undefined')
        queryStringKey = "groupsku";
    else
        sku = $(this).data('sku');
    var artifisku = $(this).data('artifisku');
    var styleNumber = $(this).data('stylenumber');
    var quantity = "";
    if (queryStringKey != "")
        quantity = $(this).data('quantity');

    if (_custmizeproductid != "") {
        e.preventDefault();
        var user = $("#UserId");
        var userId = "";
        if (typeof user != "undefined" && user != null && user != "") {
            userId = $("#UserId").val();
        }
        if (typeof userId == "undefined" || userId == null || userId == "" || userId == "0") {
            userId = localStorage.getItem("UserId");
        }
        var _url = "../product/EditArtifiProduct?id=" + $(this).data('productid') + "&custmizeproductid=" + $(this).data('custmizeproductid') + "&" + queryStringKey + "=" + sku + "&quantity=" + quantity + "&productname=" + $(this).data('productname') + "&userId=" + userId + "&artifiSku=" + artifisku + "&styleNumber=" + styleNumber  + "";

        window.location.href = _url;
    }
});


$(document).off("click", "#artifi-previewproduct");
$(document).on("click", "#artifi-previewproduct", function (e) {
    e.preventDefault();
    var _this = this;
    var artifiCredentials = "";
    GetArtifiCredentials($("#hdnPortalId").val(), localStorage.getItem("UserId"), false, function (response) {
        artifiCredentials =
            {
                websiteId: response.websiteId,
                webApiclientKey: response.webApiclientKey,
                jsUrl: response.jsUrl,
                userId: response.userId,
            }
    });
    var configurablesku = $(_this).data('configurablesku');
    var groupsku = $(_this).data('groupsku');

    var artifiSKU = $(_this).data('artifisku')

    var configSKU = $("#dynamic-configurableproductskus").val();
    var _param = "";
    var sku = configurablesku != "" && configurablesku != 'undefined' ? configurablesku : groupsku;
    sku = sku == "" || typeof (sku) == 'undefined' ? $(_this).data("sku") : sku;

    sku = artifiSKU || sku
    var extraQueryString = GetExtraQueryString(sku);

    if (configurablesku != "" && configurablesku != null && configurablesku != 'undefined') {
        _param = {
            websiteId: artifiCredentials.websiteId,
            webApiclientKey: artifiCredentials.webApiclientKey,
            sku: sku,
            designId: $(_this).data("custmizeproductid"),
            //isGuest: true,
            userId: artifiCredentials.userId,
            height: "750px",
            width: "100%",
            extraQueryString: extraQueryString,
        }
    }
    else {
        _param = {
            websiteId: artifiCredentials.websiteId,
            webApiclientKey: artifiCredentials.webApiclientKey,
            productCode: sku,
            designId: $(_this).data("custmizeproductid"),
            //isGuest: true,
            userId: artifiCredentials.userId,
            height: "750px",
            width: "100%",
            extraQueryString: extraQueryString,
        }
    }
    loadArtifiIntegrationJS(artifiCredentials.jsUrl, function () {
        var _url = Artifi.getPreviewUrl(_param);
        $("#preview-popup-content").html("<iframe src='" + _url + "' height='750px' width='100%' frameborder='0' style='position: relative;'></iframe>")


    })
});

$(document).off("click", "#artifi-design-call");
$(document).on("click", "#artifi-design-call", function (e) {
    $("#lblHeaderSku").html($(".product-number").html());
    $("#product-title").html($("h1.product-name").html());
    ////var price = $("div.product-price").find('span').html();
    ////if (typeof price === 'undefined')
    ////    $("#lblHeaderPrice").text('$--');
    ////else
    //    $("#lblHeaderPrice").text(price);
    $("#lblHeaderPrice").text('$--');
    $("#headerQuantity").val($("#Quantity").val())
    e.preventDefault();
    var groupSku = $(this).attr("data-artifigroupsku");
    var sku = groupSku;
    var status = false;

    if (sku != "" && sku != null && sku != 'undefined') {
        var publishProductId = $(this).attr("data-productId");
        var quantity = $("#" + publishProductId + "").val();
        if (quantity == null || quantity == "") {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("RequiredProductQuantity"), "error", true, fadeOutTime);
            return false;
        }
        Product.prototype.OnAssociatedProductQuantityChange();
        if (isAddToCartGroupProduct && Product.prototype.CheckGroupProductQuantity($("#dynamic-parentproductid").val(), sku, quantity)) {
            CallArtifiDesigner(0, sku);
        }
    }
    else if (Product.prototype.OnQuantityChange()) {
        CallArtifiDesigner(0);
    }
})

function UpdateUserId(OldUserId, newUserId, portalId) {
    if (OldUserId != null && OldUserId != 'undefined' && OldUserId != '0') {
        var integrationValues = "";
        GetArtifiCredentials(portalId, newUserId, false, function (response) {
            integrationValues =
                {
                    websiteId: response.websiteId,
                    webApiclientKey: response.webApiclientKey,
                    jsUrl: response.jsUrl,
                    newUserId: response.userId,
                    oldUserId: OldUserId
                }
        });
        var artifijs = "" + integrationValues.jsUrl + "//script/sasintegration/artifiintegration.js";
        $.getScript(artifijs, function () { UpdateUser(integrationValues); });
    }
}

function UpdateUser(integrationValues) {
    $.ajax({
        url: "" + integrationValues.jsUrl + "/Designer/Services/UpdateUserId?newUserId=" + integrationValues.newUserId + "&OldUserId=" + integrationValues.oldUserId + "&websiteId=" + integrationValues.websiteId + "&webApiClientKey=" + integrationValues.webApiclientKey,
        method: "GET",
        dataType: "json",
        async: true,
        success: function (data) {
            console.log(data);
        },
        error: function (data) {
            console.log(data);
        }
    });
}

function CallArtifiDesigner(custmizeproductid, groupProductSKU) {
    var artifiCredentials = "";
    var UserId = GetUserId();
    //(typeof localStorage.getItem("UserId") === 'undefined' || localStorage.getItem("UserId") === null) ? 0 : localStorage.getItem("UserId");
    var isGuest = false;
    GetArtifiCredentials($("#hdnPortalId").val(), UserId, false, function (response) {
        artifiCredentials =
            {
                websiteId: response.websiteId,
                webApiclientKey: response.webApiclientKey,
                jsUrl: response.jsUrl,
                userId: response.userId,
                portalId: $("#hdnPortalId").val()
            };
        isGuest = response.isGuest;
    });
    if (isGuest)
        localStorage.setItem("UserId", artifiCredentials.userId);
    var configurableSku = $("#dynamic-configurableproductskus").val();
    var sku = (groupProductSKU == "" || groupProductSKU == null || groupProductSKU == 'undefined') ? (configurableSku == "" || configurableSku == null || configurableSku == 'undefined') ? $("#dynamic-sku").val() : configurableSku : groupProductSKU;
    var len = $('script').filter(function () {
        return ($(this).attr('src') == artifiCredentials.jsUrl);
    }).length;
    ShowLoader();
    loadArtifiIntegrationJS(artifiCredentials.jsUrl, function () { LaunchArtifi(artifiCredentials, 0, sku) });
}

if (window.addEventListener) {
    addEventListener("message", receiveArtifiMessage, false)
} else {
    attachEvent("onmessage", receiveArtifiMessage)
}

//function GetArtifiCredentials(portalId, userId, isGetScript, callbackMethod) {
//    var isGuest = typeof userId != 'undefined' || userId == 0;
//    var _param = {
//        websiteId: 433,
//        webApiclientKey: 'a5b8ff28-c00c-4a98-94e4-bd188086dd29',
//        jsUrl: 'https://integrationdesigner.artifi.net',
//        userId: userId,
//        isGuest: isGuest
//    };

//    // loadArtifiIntegrationJS();
//    callbackMethod(_param);
//}

function GetArtifiCredentials(portalId, userId, isGetScript, callbackMethod) {
    var isGuest = typeof userId != 'undefined' || userId == 0;
    $.ajax({
        url: "/artificheckout/getartificredentials?portalId=" + portalId + "&userId=" + userId,
        method: "GET",
        dataType: "json",
        async: false,
        success: function (data) {
            var _param = {
                websiteId: data.WebsiteCode,
                webApiclientKey: data.WebApiClientKey,
                jsUrl: data.JsUrl,
                userId: data.userId,
                isGuest: isGuest
            }
            //if (isGetScript) {
            //    //if (_param.jsUrl && _param.jsUrl != 'null') {
            //    $.getScript("" + _param.jsUrl + "//script/sasintegration/artifiintegration.js");
            //    // }
            //    //ZnodeBase.prototype.GetScript("" + _param.jsUrl + "//script/sasintegration/artifiintegration.js", true,10000);
            //}
            callbackMethod(_param);
        },
        error: function (data) {
            console.log(data);
        }
    });
}

var isScriptInLoadingState = false
function loadArtifiIntegrationJS(domain, callback) {
    if (!domain)
        return
    var scriptPath = "/script/sasintegration/artifiintegration.js"
    domain = domain || "https://integrationdesigner.artifi.net";
    var url = domain + scriptPath;

    if (typeof Artifi == 'undefined' && !isScriptInLoadingState) {
        isScriptInLoadingState = true;
        $.getScript(url, function (e) {
            if (callback) {
                isScriptInLoadingState = true;
                callback(e)
            }
        });
    } else {
        if (callback) {
            callback()
        }

    }
}

function receiveArtifiMessage(event) {
    var origin = event.origin || event.originalEvent.origin;
    if (typeof Artifi == 'undefined') return;

    if (Artifi && !Artifi.isValidArtifiDomain(origin)) return;
    var eventObj = JSON.parse(event.data);
    var action = eventObj.action;
    var isFromDesignList = getParameterByName("isFromDesignList");
    switch (action) {
        case Artifi.Constant.addToCart:
            var custmizeproductid = getParameterByName("custmizeproductid");
            var data = eventObj.data;
            var quantity = 0;
            if (data.quantity > 0)
                quantity = data.quantity;
            if ((custmizeproductid != "" && typeof (custmizeproductid) != 'undefined' && custmizeproductid > 0)) {
                if ((isFromDesignList == "" || isFromDesignList == null || typeof (isFromDesignList) == 'undefined' || !isFromDesignList)) {
                    if ($(".edit-design-content").find("#Artifi-Designer").children().length > 0) {
                        quantity = $("#headerQuantity-edit").val();
                    } else if ($("#Artifi-Designer").children().length > 0) {
                        quantity = $("#headerQuantity").val();
                    } else {
                        quantity = $("#Quantity").val();
                    }

                    var customization = new Object();
                    var arrayDetails = Object.entries(data.customizedData);
                    //Iterate and Map decoration in personalize data
                    arrayDetails.forEach(function (item) {
                        var key = item[0];
                        var values = JSON.stringify(item[1]);
                        customization[key] = values;
                    });

                    if (data.regularLogoCount)
                        customization["Logo"] = data.regularLogoCount;
                    if (data.largeLogoCount)
                        customization["LargeLogo"] = data.largeLogoCount;
                    if (data.totalTextLineCount)
                        customization["TextLines"] = data.totalTextLineCount;
                    customization["ArtifiSKU"] = data.artifiSku;

                    var viewmodel = {
                        "orderLineItemsDesignId": data.custmizeProductId,
                        "aiImagePath": data.savedDesigns[0],
                        "sku": data.sku,
                        "quantity": quantity,
                        "customization": customization
                    }

                    $.ajax({
                        url: "/artifiproduct/updateartificartitem",
                        method: "Post",
                        data: viewmodel,
                        dataType: "json",
                        async: false,
                        success: function (data) {
                            location.href = "/cart";
                        },
                        error: function (data) {
                            localStorage.removeItem("isedit");
                            console.log(data);
                        }
                    });
                }
                else {
                    SubmitEditDesignForm(data.savedDesigns[0]);
                }
            }
            else {

                var _formId = "#Form_" + $("#dynamic-productid").val();
                var personalisedCodes = [];
                var personalisedValues = [];

                Product.prototype.BindAddOnProductSKU($("#button-addtocart_" + $("#dynamic-productid").val()));

                MapDecorationData(data, _formId, personalisedCodes, personalisedValues);

                $("input[IsPersonalizable = True]").each(function () {
                    personalisedValues.push($(this).val());
                    personalisedCodes.push($(this).attr('id'));
                });

                var customProductId = data.custmizeProductId;

                if (typeof customProductId != 'undefined' && customProductId != null && customProductId != "") {
                    personalisedCodes.push('ArtifiDesignId');
                    personalisedValues.push(customProductId);
                    personalisedCodes.push('ArtifiImagePath');
                    personalisedValues.push(data.savedDesigns[0]);
                    personalisedCodes.push('ArtifiSKU');
                    personalisedValues.push(data.artifiSku);
                    $(_formId).children("#dynamic-personalisedcodes").val(personalisedCodes);
                    $(_formId).children("#dynamic-personalisedvalues").val(personalisedValues.join("`"));
                    if (data.quantity > 0)
                        $(_formId).children("#dynamic-quantity").val(data.quantity);
                    if ($("#dynamic-configurableproductskus").length > 0 && $("#dynamic-configurableproductskus").val().length > 0) {
                        if (typeof data.sku != 'undefined' && data.sku != '' && data.productCode != data.sku) {
                            $(_formId).children("#dynamic-configurableproductskus").val(data.sku);
                        }
                    }
                    if (!($("#hdnShowPopup").val().toLowerCase() == 'true')) {
                        if (typeof data.sku != 'undefined' && data.sku != '') {
                            //$(_formId).children("#dynamic-configurableproductskus").val(data.sku);
                        }
                    }
                }
                $(_formId).submit();
            }
            break;

        case "price-updated":
            if (eventObj.data.priceData != 'undefined' && eventObj.data.priceData != null && event.data.priceData != "") {
                clearTimeout(typingTimer);
                typingTimer = setTimeout(function () { AriatProduct.prototype.UpdatePrice(eventObj.data.priceData) }, 800);
            }
            break;
        case "close-pop-up":
            if (window.artifiAddToCartSuccess == false) {
                closeArtifiWindow();
            }
            break;
        case "error":
            $(".personalizeDivCover").addClass("na");
            break;
        case "sku-changed":
            skuChanged(eventObj.data.sku);
            break;
        case "artifi-height-updated":
            iframeHeightUpdated(eventObj.data.height);
            break;
    }
}


function skuChanged(sku) {
    $("#lblHeaderSku").html("<strong>SKU:</strong> " + sku);
    AriatProduct.prototype.GetZnodeSKU(sku, ZnodeSKUCallBack);
}


function ZnodeSKUCallBack(res) {

    if ($(".edit-design-content").find("#Artifi-Designer").children().length > 0) {
        quantity = $("#headerQuantity-edit").val();
    } else if ($("#Artifi-Designer").children().length > 0 && $("#hdnShowPopup").length && $("#hdnShowPopup").val().toLowerCase() == 'false') {
        quantity = $("#headerQuantity").val();
    } else {
        quantity = $("#Quantity").val();
    }

    if (res.sku != undefined) {
        var quantity = quantity;
        var eventObject = {};
        eventObject.action = "sku-changed";
        eventObject.sku = res.sku;
        eventObject.styleNumber = res.styleNumber;

        $("#artifisku").val(res.artifiSKU);
        $("#styleNumber").val(res.styleNumber);
        sendEventToIframe(eventObject);
    } else {
        ZnodeNotification.prototype.DisplayNotificationMessagesHelper("Product does not exist.", "error", true, 10000);
    }
}

function sendEventToIframe(eventObject) {
    var artifiIframe = $("#Artifi-Designer iframe")[0];
    if (typeof artifiIframe !== 'undefined' && artifiIframe != null) {
        artifiIframe.contentWindow.postMessage(eventObject, "*");
    }
    else {
        console.log('Unable to post message: iframe not found!');
    }
}

function SkuPriceCallBack(res) {
    $("#lblHeaderPrice").html(res.data.totalprice);
    var existingQuantity = $("#headerQuantity").val();
    if (existingQuantity == "") {
        $("#headerQuantity").val($("#Quantity").val());
    }
}

function SkuPriceCallBackAfterQuantityChange(res) {
    $("#lblHeaderPrice").html(res.data.totalprice);

}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function LaunchArtifi(data, custmizeproductid, groupProductSKU) {
    var znodeSku = groupProductSKU == "" || groupProductSKU == null || groupProductSKU == 'undefined' ? $("#dynamic-sku").val() : groupProductSKU;  
    var sku = $("#artifisku").val() || znodeSku;
    var _param = "";
    var configSKU = $("#dynamic-configurableproductskus").val();
    if (configSKU == "" || configSKU == null || configSKU == 'undefined') {
        configSKU = getParameterByName("configurablesku");
    }
    var extraQueryString = GetExtraQueryString(znodeSku);
    if (configSKU != "" && configSKU != null && configSKU != 'undefined') {
        _param = {
            divId: "Artifi-Designer",
            websiteId: data.websiteId,
            webApiclientKey: data.webApiclientKey,
            sku: sku,
            designId: custmizeproductid,
            //isGuest: true,
            portalId: data.portalId,
            userId: data.userId,
            height: "750px",
            width: "100%",
            extraQueryString: extraQueryString,
        }

    }
    else {
        _param = {
            divId: "Artifi-Designer",
            websiteId: data.websiteId,
            webApiclientKey: data.webApiclientKey,
            productCode: sku,
            designId: custmizeproductid,
            //isGuest: true,
            portalId: data.portalId,
            userId: data.userId,
            height: "750px",
            width: "100%",
            extraQueryString: extraQueryString,
        }
    }

    var productName = "";
    if (custmizeproductid > 0) {
        productName = getParameterByName("productname");
    }
    else {
        productName = $('.product-name').html();
        groupProductSKU = $(".product-number-details").text().trim().split(":")[1];
    }
    //$("#Artifi-Designer-parent").prepend("<h1 class='product-name'>" + productName + "</h1>");

    if ($("#hdnShowPopup").length) {
        if ($("#hdnShowPopup").val().toLowerCase() == 'true') {
            $("product-title").html('');
            $(".product-meta").find("#pdp-body").show();
            $("#pdp-head").hide();
        } else {
            $("product-title").html(productName);
            $(".product-meta").find("#pdp-body").hide();
            $("#pdp-head").show();
        }
    }
    $("#dev-artifi-close").prepend("<button type='button' class='btn-primary btn pull - right btn - artifi - close' id='dev-button-artifi-close' onclick='CloseArtifiIFrame();'><span class='close-circle-icon'></span></button>");
    $(document).scrollTop("-600");
    Artifi.Initialize(_param);
    //AriatProduct.prototype.UpdatePriceFromZnode();
    HideLoader();
    return _param;
}

function CloseArtifiIFrame() {
    window.location.reload(true);
}


function editSavedDesign(control) {
    var _custmizeproductid = $(control).attr("data-customizedProductId");
    var sku = $(control).attr("data-sku");
    var productDetails = "";
    getProductDetailsBySKU(sku, function (res) {
        productDetails = res;
    })
    var queryStringKey = "groupsku";
    var _url = "../product/EditArtifiProduct?id=" + productDetails.productId + "&custmizeproductid=" + _custmizeproductid + "&" + queryStringKey + "=" + sku + "&quantity=1&productname=" + productDetails.productName + "&userId=" + $(control).attr("data-userId") + "&isFromDesignList=" + true + "";
    window.location.href = _url;
}

function SubmitEditDesignForm(savedDesign) {
    var _formId = "#addtocart-form";
    $(_formId + " input[name='SKU']").val(getParameterByName("groupsku"));

    var quantity = 0;
    if ($("#Artifi-Designer").children().length > 0) {
        quantity = $("#headerQuantity").val();
    } else {
        quantity = $("#Quantity").val();
    }

    $(_formId + " input[name='Quantity']").val(quantity);
    $(_formId).append("<input type='hidden' id='savedDesigns' name='AISavedDesigns' value='" + savedDesign + "'>");
    $(_formId).append("<input type='hidden' id='UserId' name='UserId' value='" + getParameterByName("userId") + "'>");
    $(_formId).append("<input type='hidden' id='custmizeProductId' name='OrderLineItemsDesignId' value='" + getParameterByName("custmizeproductid") + "'>");

    // submit form
    $(_formId).submit();

}

function getProductDetailsBySKU(sku, callBackMethod) {
    $.ajax({
        url: "/artifiproduct/getproductdetailsbysku",
        method: "GET",
        data: { sku: sku },
        dataType: "json",
        async: false,
        success: function (data) {
            callBackMethod(data);
        },
        error: function (data) {
            console.log(data);
        }
    });
}

function ShowLoader() {
    $("#Single-loader-content-backdrop").show();
}

function HideLoader() {
    $("#Single-loader-content-backdrop").hide();
}
function deleteSavedDesign(control) {
    var _custmizeproductid = $(control).attr("data-customizedProductId");

    var artifiCredentials = "";
    GetArtifiCredentials($("#hdnPortalId").val(), 0, false, function (response) {
        artifiCredentials =
            {
                WebsiteCode: response.websiteId,
                WebApiClientKey: response.webApiclientKey,
                JsUrl: response.jsUrl,
                userId: response.userId,
            }
    });

    $.ajax({
        url: "" + artifiCredentials.JsUrl + "/Designer/Services/DeleteCustomizedProduct?customizedProductId=" + _custmizeproductid + "&websiteId=" + artifiCredentials.WebsiteCode + "&webApiClientKey=" + artifiCredentials.WebApiClientKey + "",
        method: "POST",
        dataType: "json",
        async: true,
        success: function (data) {

            if (data.Response != null && data.Response == "Success") {
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SuccessArtifiDesignDelete"), "success", true, fadeOutTime);
                GetAllDesign(artifiCredentials);
            }
            else
                $("#artifiCustomisedDesign").html("No Records Found.");
        },
        error: function (data) {
            console.log(data);
        }
    });
}

function GetAllDesign(artifiCredentials) {
    $.ajax({
        url: "" + artifiCredentials.JsUrl + "/Designer/Services/GetCustomizedProductByUserId?userid=" + artifiCredentials.userId + "&websiteid=" + artifiCredentials.WebsiteCode + "&webApiClientKey=" + artifiCredentials.WebApiClientKey + "&orderstatus=draft",
        method: "GET",
        dataType: "json",
        async: true,
        success: function (data) {
            if (data.Data != null)
                $("#artifiCustomisedDesign").html(" ");
            else
                $("#artifiCustomisedDesign").html("No Records Found.");

            var newRowContent = "";
            jQuery.each(data.Data, function (i, val) {
                newRowContent = newRowContent + "<li style='display:inline-block;width:20%;'><img src=" + val.CustomDesignModelList[0].ThumbnailImagePath + "' height='auto' width='100'><br><a href='#' id='artifi-previewproduct' data-sku='" + val.ProductCode + "' data-userId='" + val.UserId + "' data-custmizeproductid='" + val.Id + "' data-toggle='modal' data-target='.preview-popup'>Preview</a>|<a href='#' onclick='deleteSavedDesign(this)' data-sku='" + val.ProductCode + "' data-userId='" + val.UserId + "' data-customizedProductId='" + val.Id + "'>Delete</a></br>Saved Design Name : " + val.Title + "</li>";
            });
            $("#artifiCustomisedDesign").append(newRowContent);

        },
        error: function (data) {
            console.log(data);
        }
    });
}

function MapDecorationData(data, _formId, personalisedCodes, personalisedValues) {
    var addOnValues = [];
    //Number of Decoration location added
    var arrayDetails = Object.entries(data.customizedData);
    //Iterate and Map decoration in personalize data
    arrayDetails.forEach(function (item) {
        var key = item[0];
        var values = item[1];
        personalisedCodes.push(key)
        personalisedValues.push(JSON.stringify(values));
    });

    var regularLogo = data.regularLogoCount;
    var largeLogo = data.largeLogoCount;
    var noOfTextLine = data.totalTextLineCount;

    if (regularLogo > 0)
        addOnValues.push("Logo~" + regularLogo);
    if (largeLogo > 0)
        addOnValues.push("LargeLogo~" + largeLogo);

    if (noOfTextLine > 0)
        addOnValues.push("TextLines~" + noOfTextLine);
    //To DO: Map Logo and Line Detail Once Get fro Arttifi

    $(_formId).children("#dynamic-personalisedcodes").val(personalisedCodes);
    $(_formId).children("#dynamic-personalisedvalues").val(personalisedValues);
    $(_formId).children("#dynamic-addonproductskus").val(addOnValues.join(","));
}


function iframeHeightUpdated(height) {
    if (!height) return;
    height = height + 30; //padding: 30
    var artifiIframe = $("#Artifi-Designer iframe")[0];
    artifiIframe.style.height = height + 'px';

}

$(document).on("change", "#headerQuantity", function (e) {
    var existingCustomization = existingJson;
    if (existingCustomization == "" || existingCustomization === undefined) {
        if ($(".edit-design-content").find("#Artifi-Designer").children().length > 0) {
            quantity = $("#headerQuantity-edit").val();
        } else if ($("#Artifi-Designer").children().length > 0 && $("#hdnShowPopup").length && $("#hdnShowPopup").val().toLowerCase() == 'false') {
            quantity = $("#headerQuantity").val();
        } else {
            quantity = $("#Quantity").val();
        }
        var configurableSku = $("#dynamic-configurableproductskus").val();
        var sku = $("#dynamic-sku").val();
        if (configurableSku)
            sku = configurableSku;
        Product.prototype.UpdateProductVariations(false, sku, "", quantity, "", SkuPriceCallBackAfterQuantityChange);
    } else { AriatProduct.prototype.UpdatePrice(existingCustomization); }
});

$(document).on("change", "#headerQuantity-edit", function (e) {
    var existingCustomization = existingJson;
    if (existingCustomization == "" || existingCustomization === undefined) {
        if ($(".edit-design-content").find("#Artifi-Designer").children().length > 0) {
            quantity = $("#headerQuantity-edit").val();
        } else if ($("#Artifi-Designer").children().length > 0 && $("#hdnShowPopup").length && $("#hdnShowPopup").val().toLowerCase() == 'false') {
            quantity = $("#headerQuantity").val();
        } else {
            quantity = $("#Quantity").val();
        }
        var configurableSku = $("#dynamic-configurableproductskus").val();
        var sku = $("#dynamic-sku").val();

        if (configurableSku)
            sku = configurableSku;

        Product.prototype.UpdateProductVariations(false, sku, "", quantity, "", SkuPriceCallBackAfterQuantityChange);
    } else { AriatProduct.prototype.UpdatePrice(existingCustomization); }
});