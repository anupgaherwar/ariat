﻿class CustomEndpoint extends ZnodeBase {

    constructor() {
        super();
    }

    GetImageUrlFromBV(fd, callbackMethod) {
        super.ajaxRequest("/AriatProduct/GetImageUrlFromBV", Constant.Post, fd, callbackMethod, "json");
    }


    WriteReviewBV(name: string, callbackMethod) {
        super.ajaxRequest("/AriatProduct/WriteReviewBV", Constant.GET, { "name": name }, callbackMethod, "json", false);
    }

    GetPhotoDataFromBV(name: string, callbackMethod) {
        super.ajaxRequest("/AriatProduct/GetPhotoDataFromBV", Constant.GET, { "name": name }, callbackMethod, "json", false);
    }

    GetVideoDataFromBV(name: string, callbackMethod) {
        super.ajaxRequest("/AriatProduct/GetVideoDataFromBV", Constant.GET, { "videoString": name }, callbackMethod, "json", false);
    }

    GetReviewLists(sku: string, offset: number, isAjax: boolean ,sort: string, callbackMethod) {
        super.ajaxRequest("/AriatProduct/GetReviewList", Constant.GET, { "sku": sku, "offset": offset, "isAjax": isAjax, "sort": sort}, callbackMethod, "json", false);
    }

    public GetRating(skus: string, callbackMethod: any): void {
        super.ajaxRequest("/AriatProduct/GetRatings", Constant.Post, { "skus": skus }, callbackMethod, "json");
    }
}