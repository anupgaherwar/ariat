﻿CREATE TABLE [dbo].[ZnodeProfileCategoryHierarchy] (
    [ProfileCategoryHierarchyId] INT      IDENTITY (1, 1) NOT NULL,
    [ProfileCatalogId]           INT      NULL,
    [PimCategoryHierarchyId]     INT      NULL,
    [CreatedBy]                  INT      NOT NULL,
    [CreatedDate]                DATETIME NOT NULL,
    [ModifiedBy]                 INT      NOT NULL,
    [ModifiedDate]               DATETIME NOT NULL,
    CONSTRAINT [PK_ZZnodeProfileCategoryHierarchy] PRIMARY KEY CLUSTERED ([ProfileCategoryHierarchyId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_ZnodeProfileCategoryHierarchy_ZnodePimCategoryHierarchy] FOREIGN KEY ([PimCategoryHierarchyId]) REFERENCES [dbo].[ZnodePimCategoryHierarchy] ([PimCategoryHierarchyId]),
    CONSTRAINT [FK_ZnodeProfileCategoryHierarchy_ZnodeProfileCatalog] FOREIGN KEY ([ProfileCatalogId]) REFERENCES [dbo].[ZnodeProfileCatalog] ([ProfileCatalogId])
);

