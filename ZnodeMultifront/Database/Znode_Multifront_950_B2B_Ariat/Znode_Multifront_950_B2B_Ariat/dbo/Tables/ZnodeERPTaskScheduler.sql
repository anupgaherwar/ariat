﻿CREATE TABLE [dbo].[ZnodeERPTaskScheduler] (
    [ERPTaskSchedulerId]     INT            IDENTITY (1, 1) NOT NULL,
    [ERPConfiguratorId]      INT            NULL,
    [TaskSchedulerSettingId] INT            NOT NULL,
    [SchedulerName]          NVARCHAR (100) NULL,
    [SchedulerType]          VARCHAR (20)   NULL,
    [TouchPointName]         NVARCHAR (100) NOT NULL,
    [SchedulerFrequency]     NVARCHAR (50)  NULL,
    [RepeatTaskEvery]        INT            NULL,
    [RepeatTaskForDuration]  VARCHAR (200)  NULL,
    [StartDate]              DATETIME       NULL,
    [ExpireDate]             DATETIME       NULL,
    [IsEnabled]              BIT            CONSTRAINT [DF_ZnodeERPTaskScheduler_IsEnabled] DEFAULT ((1)) NOT NULL,
    [SchedulerCallFor]       VARCHAR (200)  NULL,
    [CreatedBy]              INT            NOT NULL,
    [CreatedDate]            DATETIME       NOT NULL,
    [ModifiedBy]             INT            NOT NULL,
    [ModifiedDate]           DATETIME       NOT NULL,
    CONSTRAINT [PK_ZnodeERPTaskScheduler] PRIMARY KEY CLUSTERED ([ERPTaskSchedulerId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_ZnodeERPTaskScheduler_ZnodeERPConfigurator] FOREIGN KEY ([ERPConfiguratorId]) REFERENCES [dbo].[ZnodeERPConfigurator] ([ERPConfiguratorId]),
    CONSTRAINT [FK_ZnodeERPTaskScheduler_ZnodeERPTaskSchedulerSetting] FOREIGN KEY ([TaskSchedulerSettingId]) REFERENCES [dbo].[ZnodeERPTaskSchedulerSetting] ([TaskSchedulerSettingId])
);

