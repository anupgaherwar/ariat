﻿CREATE TABLE [dbo].[ZnodePimProduct] (
    [PimProductId]         INT           IDENTITY (1, 1) NOT NULL,
    [PimAttributeFamilyId] INT           NULL,
    [ExternalId]           NVARCHAR (50) NULL,
    [IsProductPublish]     TINYINT       NULL,
    [CreatedBy]            INT           NULL,
    [CreatedDate]          DATETIME      NULL,
    [ModifiedBy]           INT           NULL,
    [ModifiedDate]         DATETIME      NULL,
    [PublishStateId]       TINYINT       NULL,
    CONSTRAINT [PK_ZnodePimProduct] PRIMARY KEY CLUSTERED ([PimProductId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_ZnodePimProduct_ZnodePimAttributeFamily] FOREIGN KEY ([PimAttributeFamilyId]) REFERENCES [dbo].[ZnodePimAttributeFamily] ([PimAttributeFamilyId]),
    CONSTRAINT [FK_ZnodePimProduct_ZnodePublishState] FOREIGN KEY ([PublishStateId]) REFERENCES [dbo].[ZnodePublishState] ([PublishStateId])
);

