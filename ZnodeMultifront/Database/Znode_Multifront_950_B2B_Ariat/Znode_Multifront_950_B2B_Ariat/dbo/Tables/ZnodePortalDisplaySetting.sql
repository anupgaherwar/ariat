﻿CREATE TABLE [dbo].[ZnodePortalDisplaySetting] (
    [PortalDisplaySettingsId] INT      IDENTITY (1, 1) NOT NULL,
    [PortalId]                INT      NULL,
    [MediaId]                 INT      NULL,
    [MaxDisplayItems]         INT      NULL,
    [MaxSmallThumbnailWidth]  INT      NULL,
    [MaxSmallWidth]           INT      NULL,
    [MaxMediumWidth]          INT      NULL,
    [MaxThumbnailWidth]       INT      NULL,
    [MaxLargeWidth]           INT      NULL,
    [MaxCrossSellWidth]       INT      NULL,
    [CreatedBy]               INT      NOT NULL,
    [CreatedDate]             DATETIME NOT NULL,
    [ModifiedBy]              INT      NOT NULL,
    [ModifiedDate]            DATETIME NOT NULL,
    CONSTRAINT [PK_ZnodePimGlobalDisplaySetting] PRIMARY KEY CLUSTERED ([PortalDisplaySettingsId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_ZnodePortalDisplaySetting_ZnodePortal] FOREIGN KEY ([PortalId]) REFERENCES [dbo].[ZnodePortal] ([PortalId])
);

