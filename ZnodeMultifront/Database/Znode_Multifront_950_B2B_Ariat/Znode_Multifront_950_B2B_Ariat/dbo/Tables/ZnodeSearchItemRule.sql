﻿CREATE TABLE [dbo].[ZnodeSearchItemRule] (
    [SearchItemRuleId]     INT           IDENTITY (1, 1) NOT NULL,
    [SearchCatalogRuleId]  INT           NOT NULL,
    [SearchItemKeyword]    VARCHAR (100) NULL,
    [SearchItemCondition]  VARCHAR (50)  NULL,
    [SearchItemValue]      VARCHAR (600) NULL,
    [SearchItemBoostValue] INT           NULL,
    [CreatedBy]            INT           NOT NULL,
    [CreatedDate]          DATETIME      NOT NULL,
    [ModifiedBy]           INT           NOT NULL,
    [ModifiedDate]         DATETIME      NOT NULL,
    CONSTRAINT [PK_ZnodeSearchItemRule] PRIMARY KEY CLUSTERED ([SearchItemRuleId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_ZnodeSearchItemRule_ZnodeSearchCatalogRule] FOREIGN KEY ([SearchCatalogRuleId]) REFERENCES [dbo].[ZnodeSearchCatalogRule] ([SearchCatalogRuleId])
);

