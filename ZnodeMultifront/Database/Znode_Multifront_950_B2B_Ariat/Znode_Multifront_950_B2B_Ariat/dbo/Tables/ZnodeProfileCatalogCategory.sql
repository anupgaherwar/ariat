﻿CREATE TABLE [dbo].[ZnodeProfileCatalogCategory] (
    [ProfileCatalogCategoryId] INT      IDENTITY (1, 1) NOT NULL,
    [ProfileCatalogId]         INT      NULL,
    [PimCatalogCategoryId]     INT      NULL,
    [DisplayOrder]             INT      NULL,
    [IsActive]                 BIT      CONSTRAINT [DF_ZnodeProfileCatalogCategory_IsActive] DEFAULT ((0)) NULL,
    [CreatedBy]                INT      NOT NULL,
    [CreatedDate]              DATETIME NOT NULL,
    [ModifiedBy]               INT      NOT NULL,
    [ModifiedDate]             DATETIME NOT NULL,
    CONSTRAINT [PK_ZnodeProfileCatalogCategory] PRIMARY KEY CLUSTERED ([ProfileCatalogCategoryId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_ZnodeProfileCatalogCategory_ZnodePimCatalogCategory] FOREIGN KEY ([PimCatalogCategoryId]) REFERENCES [dbo].[ZnodePimCatalogCategory] ([PimCatalogCategoryId]),
    CONSTRAINT [FK_ZnodeProfileCatalogCategory_ZnodeProfileCatalog] FOREIGN KEY ([ProfileCatalogId]) REFERENCES [dbo].[ZnodeProfileCatalog] ([ProfileCatalogId])
);

