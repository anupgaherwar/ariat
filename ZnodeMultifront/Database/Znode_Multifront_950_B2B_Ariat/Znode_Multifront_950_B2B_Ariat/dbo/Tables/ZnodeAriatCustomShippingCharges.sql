﻿CREATE TABLE [dbo].[ZnodeAriatCustomShippingCharges] (
    [AriatCustomShippingChargesId] INT             IDENTITY (1, 1) NOT NULL,
    [ShippingCode]                 VARCHAR (300)   NULL,
    [LowerLimit]                   NUMERIC (26, 8) NULL,
    [UpperLimit]                   NUMERIC (26, 8) NULL,
    [ShippingAmount]               NUMERIC (26, 8) NULL,
    [CreatedBy]                    INT             NOT NULL,
    [CreatedDate]                  DATETIME        NOT NULL,
    [ModifiedBy]                   INT             NOT NULL,
    [ModifiedDate]                 DATETIME        NOT NULL,
    PRIMARY KEY CLUSTERED ([AriatCustomShippingChargesId] ASC)
);




GO
CREATE NONCLUSTERED INDEX [IX_ZnodeAriatCustomShippingCharges_ShippingCode]
    ON [dbo].[ZnodeAriatCustomShippingCharges]([ShippingCode] ASC);

