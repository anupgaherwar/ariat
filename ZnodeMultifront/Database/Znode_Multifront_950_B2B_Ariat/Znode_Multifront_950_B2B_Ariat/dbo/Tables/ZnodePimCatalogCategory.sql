﻿CREATE TABLE [dbo].[ZnodePimCatalogCategory] (
    [PimCatalogCategoryId]   INT      IDENTITY (1, 1) NOT NULL,
    [PimCatalogId]           INT      NULL,
    [PimCategoryId]          INT      NULL,
    [PimProductId]           INT      NULL,
    [DisplayOrder]           INT      CONSTRAINT [DF_ZnodePimCatalogCategory_DisplayOrder] DEFAULT ((999)) NULL,
    [IsActive]               BIT      CONSTRAINT [DF_ZnodePimCatalogCategory_IsActive] DEFAULT ((0)) NULL,
    [CreatedBy]              INT      NOT NULL,
    [CreatedDate]            DATETIME NOT NULL,
    [ModifiedBy]             INT      NOT NULL,
    [ModifiedDate]           DATETIME NOT NULL,
    [PimCategoryHierarchyId] INT      NULL,
    CONSTRAINT [PK_ZnodePimCatalogCategory] PRIMARY KEY CLUSTERED ([PimCatalogCategoryId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_ZnodePimCatalogCategory_ZnodePimCatagory] FOREIGN KEY ([PimCategoryId]) REFERENCES [dbo].[ZnodePimCategory] ([PimCategoryId]),
    CONSTRAINT [FK_ZnodePimCatalogCategory_ZnodePimCatalog] FOREIGN KEY ([PimCatalogId]) REFERENCES [dbo].[ZnodePimCatalog] ([PimCatalogId]),
    CONSTRAINT [FK_ZnodePimCatalogCategory_ZnodePimCategoryHierarchy] FOREIGN KEY ([PimCategoryHierarchyId]) REFERENCES [dbo].[ZnodePimCategoryHierarchy] ([PimCategoryHierarchyId]),
    CONSTRAINT [FK_ZnodePimCatalogCategory_ZnodePimProduct] FOREIGN KEY ([PimProductId]) REFERENCES [dbo].[ZnodePimProduct] ([PimProductId])
);


GO
CREATE NONCLUSTERED INDEX [IX_ZnodePimCatalogCategory_PimProductId_PimCategoryHierarchyId]
    ON [dbo].[ZnodePimCatalogCategory]([PimProductId] ASC, [PimCategoryHierarchyId] ASC)
    INCLUDE([DisplayOrder]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_ZnodePimCatalogCategory_PimCategoryHierarchyId]
    ON [dbo].[ZnodePimCatalogCategory]([PimCategoryHierarchyId] ASC)
    INCLUDE([PimProductId], [DisplayOrder]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_ZnodePimCatalogCategory_PimCatalogId_PimCategoryHierarchyId_PimProductId]
    ON [dbo].[ZnodePimCatalogCategory]([PimCatalogId] ASC, [PimCategoryHierarchyId] ASC, [PimProductId] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IND_ZnodePimCatalogCategory_PimCatalogId]
    ON [dbo].[ZnodePimCatalogCategory]([PimCatalogId] ASC)
    INCLUDE([PimCategoryId], [PimProductId], [DisplayOrder]) WITH (FILLFACTOR = 90);

