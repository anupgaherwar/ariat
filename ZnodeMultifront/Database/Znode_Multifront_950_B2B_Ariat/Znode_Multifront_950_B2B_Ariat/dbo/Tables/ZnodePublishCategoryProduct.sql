﻿CREATE TABLE [dbo].[ZnodePublishCategoryProduct] (
    [PublishCategoryProductId] INT      IDENTITY (1, 1) NOT NULL,
    [PublishProductId]         INT      NULL,
    [PublishCategoryId]        INT      NULL,
    [PublishCatalogId]         INT      NULL,
    [CreatedBy]                INT      NOT NULL,
    [CreatedDate]              DATETIME NOT NULL,
    [ModifiedBy]               INT      NOT NULL,
    [ModifiedDate]             DATETIME NOT NULL,
    [PimCategoryHierarchyId]   INT      NULL,
    [ProductIndex]             INT      NULL,
    CONSTRAINT [PK_ZnodePublishCategoryProduct] PRIMARY KEY CLUSTERED ([PublishCategoryProductId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_ZnodePublishCategoryProduct_ZnodePublishCatalog] FOREIGN KEY ([PublishCatalogId]) REFERENCES [dbo].[ZnodePublishCatalog] ([PublishCatalogId]),
    CONSTRAINT [FK_ZnodePublishCategoryProduct_ZnodePublishCategory] FOREIGN KEY ([PublishCategoryId]) REFERENCES [dbo].[ZnodePublishCategory] ([PublishCategoryId]),
    CONSTRAINT [FK_ZnodePublishCategoryProduct_ZnodePublishProduct] FOREIGN KEY ([PublishProductId]) REFERENCES [dbo].[ZnodePublishProduct] ([PublishProductId])
);


GO
CREATE NONCLUSTERED INDEX [IX_ZnodePublishCategoryProduct_PublishCategoryId]
    ON [dbo].[ZnodePublishCategoryProduct]([PublishCategoryId] ASC)
    INCLUDE([PublishProductId], [PublishCatalogId]) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [Idx_ZnodePublishCategoryProduct_PublishCatalogId]
    ON [dbo].[ZnodePublishCategoryProduct]([PublishCatalogId] ASC)
    INCLUDE([PublishProductId], [PimCategoryHierarchyId]) WITH (FILLFACTOR = 90);

