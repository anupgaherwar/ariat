﻿CREATE TABLE [dbo].[ZnodeProductFeedPriority] (
    [ProductFeedPriorityId] INT             IDENTITY (1, 1) NOT NULL,
    [ProductFeedPriority]   DECIMAL (16, 1) NULL,
    [CreatedBy]             INT             NOT NULL,
    [CreatedDate]           DATETIME        NOT NULL,
    [ModifiedBy]            INT             NOT NULL,
    [ModifiedDate]          DATETIME        NOT NULL,
    CONSTRAINT [PK_ZnodeProductFeedPriority] PRIMARY KEY CLUSTERED ([ProductFeedPriorityId] ASC) WITH (FILLFACTOR = 90)
);

