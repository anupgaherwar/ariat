﻿CREATE VIEW [dbo].[View_GetProfileCatalog]
AS
     SELECT ProfileCatalogId,
            ZPC.ProfileId,
            ZPC.PimCatalogId,
            ZP.ProfileName,
            ZC.CatalogName
     FROM ZnodeProfileCatalog ZPC
          INNER JOIN ZnodePimCatalog ZC ON(ZC.PimCatalogId = ZPC.PimCatalogId)
          INNER JOIN ZnodeProfile ZP ON(ZP.ProfileId = ZPC.ProfileId);