SET IDENTITY_INSERT [dbo].[ZnodeShippingTypes] ON 
GO
INSERT [dbo].[ZnodeShippingTypes] ([ShippingTypeId], [ClassName], [Name], [Description], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, N'ZnodeShippingFedEx', N'FedEx', N'Calculates shipping rates when using FedEx.', 1, 2, CAST(N'2016-03-14T14:53:53.027' AS DateTime), 2, CAST(N'2016-03-31T13:43:30.360' AS DateTime))
GO
INSERT [dbo].[ZnodeShippingTypes] ([ShippingTypeId], [ClassName], [Name], [Description], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, N'ZnodeShippingUps', N'UPS', N'Calculates shipping rates when using UPS.', 1, 2, CAST(N'2016-03-14T14:54:06.887' AS DateTime), 2, CAST(N'2016-03-18T10:02:54.290' AS DateTime))
GO
INSERT [dbo].[ZnodeShippingTypes] ([ShippingTypeId], [ClassName], [Name], [Description], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, N'ZnodeShippingUsps', N'USPS', N'Calculates shipping rates when using the United States Postal Service.', 1, 1, CAST(N'2016-03-21T00:00:00.000' AS DateTime), 1, CAST(N'2016-03-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[ZnodeShippingTypes] ([ShippingTypeId], [ClassName], [Name], [Description], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (4, N'ZnodeShippingCustom', N'Custom', N'Calculates custom shipping rates.', 1, 1, CAST(N'2016-03-21T00:00:00.000' AS DateTime), 2, CAST(N'2016-03-31T13:55:36.177' AS DateTime))
GO
INSERT [dbo].[ZnodeShippingTypes] ([ShippingTypeId], [ClassName], [Name], [Description], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (5, N'ZnodeCustomerShipping', N'Customer''s Shipping', N'Use for Customers Shipping', 1, 2, CAST(N'2018-01-10T11:30:43.940' AS DateTime), 2, CAST(N'2018-01-10T11:30:43.940' AS DateTime))
GO
INSERT [dbo].[ZnodeShippingTypes] ([ShippingTypeId], [ClassName], [Name], [Description], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (6, N'AriatShippingCustom', N'AriatShippingCustom', N'Calculates custom shipping rates.', 1, 1, CAST(N'2020-06-04T10:59:28.923' AS DateTime), 2, CAST(N'2020-06-04T10:59:28.923' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[ZnodeShippingTypes] OFF
GO
