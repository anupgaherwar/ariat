/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :  ApplicationSettingData.sql					
               					
--------------------------------------------------------------------------------------
------*/
--:r .\Post\ZnodePublishState.sql
:r .\Script.PostDeployment1.sql
--:r .\Script.PostDeployment2.sql
--:r .\Script.PostDeployment3.sql
:r .\Post\ZnodeAriatCustomShippingCharges.sql
-- :r .\Post\ZnodePortal.sql
-- :r .\Post\ZnodeDomain.sql
-- :r .\Post\ApplicationSetting_AdminRefresh.sql
-- :r .\Post\ApplicationSettingData.sql
-- :r .\Post\ZnodeMenu.sql
-- :r .\Post\ZnodeAction.sql
-- :r .\Post\ZnodeGlobalSetting.sql
-- :r .\Post\ZnodeMessage.sql
-- :r .\Post\ZnodePaymentGateway.sql
-- :r .\Post\ZnodeImportTemplateMapping.sql
-- :r .\Post\ZnodeImportAttributeValidation.sql
-- :r .\Post\ZnodePaymentType.sql
-- :r .\Post\ZnodePimAttributeValidation.sql
-- :r .\Post\SEODetail.sql
-- :r .\Post\ZnodeGlobalAttribute.sql
-- :r .\Post\ZnodeOmsOrderState.sql
-- :r .\Post\ZnodeOmsOrderLineItemRelationshipType.sql
-- :r .\Post\ZnodeAccountPermission.sql
-- :r .\Post\ZnodePublishStateApplicationTypeMapping.sql
-- :r .\Post\ZnodeCMSMessage.sql
-- :r .\Post\ZnodeCMSTemplate.sql
-- :r .\Post\ZnodeCMSContentPages.sql
-- :r .\Post\ZnodeCMSContentPageGroup.sql
-- :r .\Post\ZnodeCMSTheme.sql
-- :r .\Post\ZnodeCMSWidgets.sql
-- :r .\Post\ZnodeApproverLevel.sql
-- :r .\Post\ZnodePaymentSetting.sql
-- :r .\Post\ZnodeOmsPaymentState.sql
-- :r .\Post\ZnodePublishCatalogLog.sql
-- :r .\Post\ZnodeEmailTemplate.sql
-- :r .\Post\ZnodeOmsQuote.sql
-- :r .\Post\ZnodeOmsOrderStateShowToCustomer.sql
-- :r .\Post\ZnodeRobotsTxt.sql
-- :r .\Post\AspNet_SqlCacheTablesForChangeNotification.sql
-- :r .\Post\ZnodeTimeFormat.sql
-- :r .\Post\ZnodeBrandDetailLocale.sql
-- :r .\Post\ZnodeCMSCustomerReview.sql
-- :r .\Post\ZnodePublishStatus.sql












