﻿CREATE PROCEDURE [dbo].[Znode_InsertUpdateProfileCatalog]
(   @ProfileId        INT          = NULL,
	@PimCatalogId     VARCHAR(MAX) = '',
	@UserId           INT,
	@Status           BIT OUT,
	@PimCategoryId    VARCHAR(MAX) = '',
	@ProfileCatalogId INT          = NULL,
	@PimProductId     VARCHAR(MAX) = '',
	@PimCategoryHierarchyId INT =0 
	
	
	)
AS 
    /* Summary :- This Procedure is used to dump the catalog data with profile base catalog 
     Unit Testing 
     EXEC Znode_InsertUpdateProfileCatalog 
	*/
     BEGIN
         BEGIN TRAN InsertUpdateProfileCatalog;
         BEGIN TRY
             DECLARE @TBL_ProfileCatalogId TABLE
             (ProfileCatalogId INT,
              PimCatalogId     INT
             );
			 DECLARE @GetDate DATETIME = dbo.Fn_GetDate();
             DECLARE @TBL_PimCatalogId TABLE(PimCatalogId INT);
             DECLARE @TBL_PimCategoryId TABLE(PimCategoryHierarchyId INT,PimCategoryId INT);
             DECLARE @TBL_PimProductId TABLE(PimProductId INT);
             DECLARE @PimCatalogIds INT;
             INSERT INTO @TBL_PimCatalogId(PimCatalogId)
                    SELECT Item
                    FROM dbo.split(@PimCatalogId, ',') SP;
					--SELECT @PimCategoryHierarchyId
					IF @PimCategoryHierarchyId >= 1 
					BEGIN 

             INSERT INTO @TBL_PimCategoryId(PimCategoryId,PimCategoryHierarchyId)
                    SELECT PimCategoryId,PimCategoryHierarchyId
                    FROM dbo.Fn_GetRecurciveCategoryIds_ForChild(@PimCategoryHierarchyId,@PimCatalogId ) SP
					WHERE EXISTS (SELECT TOP 1 1 FROM dbo.split(@PimCategoryId,',') th WHERE th.item= Sp.PimCategoryId )
					UNION ALL
					SELECT PimCategoryId,PimCategoryHierarchyId
					FROM ZnodePimCategoryHierarchy  SP
					WHERE PimCategoryHierarchyId = @PimCategoryHierarchyId
					;
					END 
					ELSE 
					BEGIN 
					 INSERT INTO @TBL_PimCategoryId(PimCategoryId,PimCategoryHierarchyId)
					  SELECT PimCategoryId,PimCategoryHierarchyId
					  FROM ZnodePimCategoryHierarchy  SP
					  WHERE EXISTS (SELECT TOP 1 1 FROM dbo.split(@PimCategoryId,',') th WHERE th.item= Sp.PimCategoryId )
					  AND ParentPimCategoryHierarchyId IS nULL
					END 
             INSERT INTO @TBL_PimProductId(PimProductId)
                    SELECT ZPCC.PimProductId
                    FROM ZnodePimCatalogCategory ZPCC
                    WHERE(( EXISTS
                         (
                             SELECT TOP 1 1
                             FROM dbo.split(@PimProductId, ',') SP
                             WHERE SP.Item = ZPCC.PimProductId
                         )OR @PimProductId = '')
                    AND ( 

					EXISTS (SELECT TOP 1 1 FROM @TBL_PimCategoryId  ty WHERE
                             ty.PimCategoryHierarchyId = ZPCC.PimCategoryHierarchyId  )
                                  
                         ))
                    AND EXISTS
                    (
                        SELECT TOP 1 1
                        FROM @TBL_PimCatalogId TBPC
                        WHERE TBPC.PimCatalogId = ZPCC.PimCatalogId
                    );
             IF @ProfileId IS NOT NULL
                AND @ProfileCatalogId IS NULL
                 BEGIN
                     MERGE INTO ZnodeProfileCatalog TARGET
                     USING @TBL_PimCatalogId SOURCE
                     ON(TARGET.PimCatalogId = SOURCE.PimCatalogId
                        AND TARGET.ProfileId = @ProfileId)
                         WHEN NOT MATCHED
                         THEN INSERT(ProfileId,
                                     PimCatalogId,
                                     CreatedBy,
                                     CreatedDate,
                                     ModifiedBy,
                                     ModifiedDate) VALUES
                     (@ProfileId,
                      SOURCE.PimCatalogId,
                      @UserId,
                      @GetDate,
                      @UserId,
                      @GetDate
                     )
                     OUTPUT INSERTED.ProfileCatalogId,
                            Source.PimCatalogId
                            INTO @TBL_ProfileCatalogId;
                     INSERT INTO ZnodeProfileCategoryHierarchy
                     (ProfileCatalogId,
                      PimCategoryHierarchyId,
                      CreatedBy,
                      CreatedDate,
                      ModifiedBy,
                      ModifiedDate
                     )
                            SELECT TBPC.ProfileCatalogId,
                                   ZPCH.PimCategoryHierarchyId,
                                   @UserId,
                                   @GetDate,
                                   @UserId,
                                   @GetDate
                            FROM ZnodePimCategoryHierarchy ZPCH
                                 INNER JOIN @TBL_ProfileCatalogId TBPC ON(ZPCH.PimCatalogId = TBPC.PimCatalogId);
                     INSERT INTO ZnodeProfileCatalogCategory
                     (ProfileCatalogId,
                      PimCatalogCategoryId,
                      CreatedBy,
                      CreatedDate,
                      ModifiedBy,
                      ModifiedDate
                     )
                            SELECT TBPC.ProfileCatalogId,
                                   PimCatalogCategoryId,
                                   @UserId,
                                   @GetDate,
                                   @UserId,
                                   @GetDate
                            FROM ZnodePimCatalogCategory ZPCC
                                 INNER JOIN @TBL_ProfileCatalogId TBPC ON(ZPCC.PimCatalogId = TBPC.PimCatalogId);
                 END;
             ELSE
                 BEGIN
                     SET @PimCatalogIds = CAST(@PimCatalogId AS INT);
                     INSERT INTO ZnodeProfileCategoryHierarchy
                     (ProfileCatalogId,
                      PimCategoryHierarchyId,
                      CreatedBy,
                      CreatedDate,
                      ModifiedBy,
                      ModifiedDate
                     )
                            SELECT @ProfileCatalogId,
                                   ZPCH.PimCategoryHierarchyId,
                                   @UserId,
                                   @GetDate,
                                   @UserId,
                                   @GetDate
                            FROM ZnodePimCategoryHierarchy ZPCH
                            WHERE 
							EXISTS (SELECT TOP 1 1 FROM @TBL_PimCategoryId  ty WHERE
                             ty.PimCategoryHierarchyId = ZPCH.PimCategoryHierarchyId )  
                            
                                  AND 
								  ZPCH.PimCatalogId = @PimCatalogIds;
                     INSERT INTO ZnodeProfileCatalogCategory
                     (ProfileCatalogId,
                      PimCatalogCategoryId,
                      CreatedBy,
                      CreatedDate,
                      ModifiedBy,
                      ModifiedDate
                     )
                            SELECT @ProfileCatalogId,
                                   PimCatalogCategoryId,
                                   @UserId,
                                   @GetDate,
                                   @UserId,
                                   @GetDate
                            FROM ZnodePimCatalogCategory ZPCC
                            WHERE EXISTS (SELECT TOP 1 1 FROM @TBL_PimCategoryId  ty WHERE
                             ty.PimCategoryHierarchyId = ZPCC.PimCategoryHierarchyId  )  
                            
                                  AND ZPCC.PimCatalogId = @PimCatalogIds
                                  AND EXISTS
                            (
                                SELECT TOP 1 1
                                FROM @TBL_PimProductId TBPP
                                WHERE TBPP.PimProductId = ZPCC.PimProductId
                            );
                 END;
             SET @Status = 1;
             COMMIT TRAN InsertUpdateProfileCatalog;
         END TRY
         BEGIN CATCH
		     SELECT ERROR_MESSAGE()
             DECLARE @Error_procedure VARCHAR(1000)= ERROR_PROCEDURE(), @ErrorMessage NVARCHAR(MAX)= ERROR_MESSAGE(), @ErrorLine VARCHAR(100)= ERROR_LINE(),
			 @ErrorCall NVARCHAR(MAX)= 'EXEC Znode_InsertUpdateProfileCatalog @ProfileId = '+CAST(@ProfileId AS VARCHAR(200))+',@PimCatalogId='+@PimCatalogId+',@UserId='+CAST(@UserId AS VARCHAR(200))+',@PimCategoryId='+@PimCategoryId+',@ProfileCatalogId='+CAST(@ProfileCatalogId AS VARCHAR(200))+',@PimProductId='+@PimProductId+',@Status='+CAST(@Status AS VARCHAR(200));
             SET @Status = 0;
             SELECT 0 AS ID,
                    CAST(0 AS BIT) AS Status;
			 ROLLBACK TRAN InsertUpdateProfileCatalog;
             EXEC Znode_InsertProcedureErrorLog
                  @ProcedureName = 'Znode_InsertUpdateProfileCatalog',
                  @ErrorInProcedure = @Error_procedure,
                  @ErrorMessage = @ErrorMessage,
                  @ErrorLine = @ErrorLine,
                  @ErrorCall = @ErrorCall;
         END CATCH;
     END;