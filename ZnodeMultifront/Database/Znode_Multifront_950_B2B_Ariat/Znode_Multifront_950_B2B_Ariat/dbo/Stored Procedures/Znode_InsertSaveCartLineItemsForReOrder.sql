﻿
CREATE PROCEDURE [dbo].[Znode_InsertSaveCartLineItemsForReOrder] 
(
 @OmsOrderId INT, 
 @OmsSavedCartId INT ,
 @UserId INT ,
 @OmsOrderLineItemsId INT = 0,
 @Status BIT = 0 OUT
)
AS 
BEGIN 
 BEGIN TRY 
  SET NOCOUNT ON 
  DECLARE @TBL_ZnodeOmsSavedCartLineItem TABLE (OmsSavedCartLineItemId INT , RowId INT  )
  DECLARE @GetDate DATETIME = dbo.FN_getDate() 
  DECLARE @AddOnOrderLineItemRelationshipTypeId INT = (SELECT TOP 1 OrderLineItemRelationshipTypeId 
															FROM ZnodeOmsOrderLineItemRelationshipType
															WHERE Name = 'Addons')

   CREATE TABLE #ZnodeOmsOrderLineItems_temp (OmsOrderLineItemsId INT ,ParentOmsOrderLineItemsId INT,OmsOrderDetailsId INT , SKU  NVARCHAr(2000),
   OrderLineItemRelationshipTypeID INT ,AutoAddonSKU NVARCHAR(400),Custom1 NVARCHAR(MAX),Custom2 NVARCHAR(MAX),Custom3 NVARCHAR(MAX),Custom4 NVARCHAR(MAX)
   ,Custom5 NVARCHAR(MAX), GroupId nvarchar(MAX),ProductName NVARCHAr(2000), Description nvarchar(MAX),Quantity NUMERIC(28,6), ROWID INT, ParentRowID INT)

  IF EXISTS (SELECT TOP 1 OmsOrderDetailsId  FROM ZnodeOmsOrderDetails WHERE  OmsOrderId =  @OmsOrderId AND IsActive =1 )
  BEGIN 
  INSERT INTO #ZnodeOmsOrderLineItems_temp
  SELECT OmsOrderLineItemsId, ParentOmsOrderLineItemsId,OmsOrderDetailsId,SKU,OrderLineItemRelationshipTypeID,AutoAddonSKU,
  Custom1,Custom2,Custom3,Custom4,
  Custom5,GroupId,ProductName,Description,Quantity
  ,Row_number()Over(Order By OmsOrderLineItemsId )  RowId, NULL ParentRowId
  FROM ZnodeOmsOrderLineItems 
  WHERE  OmsOrderDetailsId = (SELECT TOP 1 OmsOrderDetailsId  FROM ZnodeOmsOrderDetails WHERE  OmsOrderId =  @OmsOrderId AND IsActive =1 ) 
  ORDER BY OmsOrderLineItemsId 
   
  END

  ELSE 
  BEGIN
  
   
  INSERT INTO #ZnodeOmsOrderLineItems_temp
  SELECT OmsOrderLineItemsId, ParentOmsOrderLineItemsId,ZOO.OmsOrderDetailsId,ZOO.SKU,ZOO.OrderLineItemRelationshipTypeID,ZOO.AutoAddonSKU,
  ZOO.Custom1,ZOO.Custom2,ZOO.Custom3,ZOO.Custom4,
  ZOO.Custom5,ZOO.GroupId,ZOO.ProductName,ZOO.Description,ZOO.Quantity
  ,Row_number()Over(Order By ZOO.OmsOrderLineItemsId )   RowId, NULL ParentRowId
  FROM ZnodeOmsOrderLineItems ZOO
  WHERE ( ZOO.OmsOrderLineItemsId = @OmsOrderLineItemsId OR ZOO.ParentOmsOrderLineItemsId = @OmsOrderLineItemsId ) 
  --AND EXISTS (select top 1 1 from ZnodeOmsOrderLineItems tt WHERE tt.OmsOrderLineItemsId = ZOO.ParentOmsOrderLineItemsId )

 
   
  INSERT INTO #ZnodeOmsOrderLineItems_temp 
  SELECT ZOO.OmsOrderLineItemsId,ZOO.ParentOmsOrderLineItemsId,ZOO.OmsOrderDetailsId,ZOO.SKU,ZOO.OrderLineItemRelationshipTypeID,ZOO.AutoAddonSKU,
  ZOO.Custom1,ZOO.Custom2,ZOO.Custom3,ZOO.Custom4,
  ZOO.Custom5,ZOO.GroupId,ZOO.ProductName,ZOO.Description,ZOO.Quantity
  ,0 RowId, NULL ParentRowId
  FROM ZnodeOmsOrderLineItems ZOO
  WHERE ZOO.OmsOrderLineItemsId = (SELECT TOP 1 ParentOmsOrderLineItemsId FROM #ZnodeOmsOrderLineItems_temp TY WHERE TY.ParentOmsOrderLineItemsId IS NOT NULL 
  AND TY.OrderLineItemRelationshipTypeID <> @AddOnOrderLineItemRelationshipTypeId )
  AND ZOO.ParentOmsOrderLineItemsId  IS NULL 
  


  END

     DECLARE @TBL_OmsSavedCartOld TABLE(SKU NVARCHAR(2000), OmsSavedCartLineItemId INT ,ParentSKU NVARCHAR(2000) , ParentOmsSavedCartLineItemId INT ,AddOnSKU NVARCHAR(2000), OmsSavedCartLineItemIdAddOn NVARCHAR(2000) ,PersonalizeCode NVARCHAR(1200), PersonalizeValue  NVARCHAr(max)  )

	 DECLARE @TBL_OmsSavedCartNew TABLE(SKU NVARCHAR(2000), OmsSavedCartLineItemId INT ,ParentSKU NVARCHAR(2000) , ParentOmsSavedCartLineItemId INT ,AddOnSKU NVARCHAR(2000), OmsSavedCartLineItemIdAddOn NVARCHAR(2000) ,PersonalizeCode NVARCHAR(1200), PersonalizeValue  NVARCHAr(max)  )

	 
	 SELECT * 
	 INTO #ZnodeOmsSavedCartLineItemOld
	 FROM #ZnodeOmsOrderLineItems_temp a 
	


	 SELECT * 
	 INTO #ZnodeOmsPersonalizeCartItemOld
	 FROM ZnodeOmsPersonalizeItem a 
	 WHERE EXISTS (SELECT TOP 1 1 FROM #ZnodeOmsOrderLineItems_temp t WHERE t.OmsOrderLineItemsId = a.OmsOrderLineItemsId)

	  SELECT * 
	 INTO #ZnodeOmsSavedCartLineItemNew
	 FROM ZnodeOmsSavedCartLineItem a 
	 WHERE OmsSavedCartId = @OmsSavedCartId 


	 SELECT * 
	 INTO #ZnodeOmsPersonalizeCartItemNew
	 FROM ZnodeOmsPersonalizeCartItem a 
	 WHERE EXISTS (SELECT TOP 1 1 FROM #ZnodeOmsSavedCartLineItemNew t WHERE t.OmsSavedCartLineItemId = a.OmsSavedCartLineItemId)

	 

	 INSERT INTO @TBL_OmsSavedCartOld (SKU,OmsSavedCartLineItemId,ParentSKU,ParentOmsSavedCartLineItemId)
	    SELECT SKU , OmsOrderLineItemsId,(SELECT TOP 1 SKU FROM #ZnodeOmsSavedCartLineItemOld TBL_B WHERE TBL_B.OmsOrderLineItemsId = ISNULL(TBL_A.ParentOmsOrderLineItemsId,0)  ) ParentSKU
					, ParentOmsOrderLineItemsId
		FROM #ZnodeOmsSavedCartLineItemOld TBL_A
		WHERE OrderLineItemRelationshipTypeId IS NOT NULL AND OrderLineItemRelationshipTypeId <> @AddOnOrderLineItemRelationshipTypeId
	 
	

	 ;With Cte_UpdateOld AS 
	 (
		SELECT ParentOmsOrderLineItemsId , SUBSTRING((SELECT ','+SKU FROM #ZnodeOmsSavedCartLineItemOld t WHERE t.ParentOmsOrderLineItemsId = a.ParentOmsOrderLineItemsId FOR XML PATH('') ),2,4000)  SKU
		     , SUBSTRING((SELECT ','+CAST(OmsOrderLineItemsId AS NVARCHAR(max)) FROM #ZnodeOmsSavedCartLineItemOld t WHERE t.ParentOmsOrderLineItemsId = a.ParentOmsOrderLineItemsId FOR XML PATH('') ),2,4000)  OmsSavedCartLineItemId
		FROM #ZnodeOmsSavedCartLineItemOld a 
		WHERE a.OrderLineItemRelationshipTypeId = @AddOnOrderLineItemRelationshipTypeId
	 )
	 
	 UPDATE TBL_O
	 SET TBL_O.AddOnSKU =  TBL_ON.SKU
		,TBL_O.OmsSavedCartLineItemIdAddOn =  TBL_ON.OmsSavedCartLineItemId
	 FROM @TBL_OmsSavedCartOld TBL_O 
	 INNER JOIN Cte_UpdateOld TBL_ON ON (TBL_ON.ParentOmsOrderLineItemsId  = TBL_O.OmsSavedCartLineItemId )
	   
   
	  INSERT INTO @TBL_OmsSavedCartNew (SKU,OmsSavedCartLineItemId,ParentSKU,ParentOmsSavedCartLineItemId)
	    SELECT SKU , OmsSavedCartLineItemId,(SELECT TOP 1 SKU FROM #ZnodeOmsSavedCartLineItemNew TBL_B WHERE TBL_B.OmsSavedCartLineItemId = ISNULL( TBL_A.ParentOmsSavedCartLineItemId,0)   ) ParentSKU
					, ParentOmsSavedCartLineItemId
		FROM #ZnodeOmsSavedCartLineItemNew TBL_A
		WHERE OrderLineItemRelationshipTypeId IS NOT NULL AND OrderLineItemRelationshipTypeId <> @AddOnOrderLineItemRelationshipTypeId
	 
	 ;With Cte_UpdateNew AS 
	 (
		SELECT ParentOmsSavedCartLineItemId , SUBSTRING((SELECT ','+SKU FROM #ZnodeOmsSavedCartLineItemNew t WHERE t.ParentOmsSavedCartLineItemId = a.ParentOmsSavedCartLineItemId FOR XML PATH('') ),2,4000)  SKU
		     , SUBSTRING((SELECT ','+CAST(OmsSavedCartLineItemId AS NVARCHAR(max)) FROM #ZnodeOmsSavedCartLineItemNew t WHERE t.ParentOmsSavedCartLineItemId = a.ParentOmsSavedCartLineItemId FOR XML PATH('') ),2,4000)  OmsSavedCartLineItemId
		FROM #ZnodeOmsSavedCartLineItemNew a 
		WHERE a.OrderLineItemRelationshipTypeId = @AddOnOrderLineItemRelationshipTypeId
	 )
	 
	 UPDATE TBL_O
	 SET TBL_O.AddOnSKU =  TBL_ON.SKU
		,TBL_O.OmsSavedCartLineItemIdAddOn =  TBL_ON.OmsSavedCartLineItemId
	 FROM @TBL_OmsSavedCartNew TBL_O 
	 INNER JOIN Cte_UpdateNew TBL_ON ON (TBL_ON.ParentOmsSavedCartLineItemId  = TBL_O.OmsSavedCartLineItemId )
	   

    
	 UPDATE TBL_O
	 SET TBL_O.PersonalizeCode = SUBSTRING((SELECT ','+TBL_ON.PersonalizeCode FROM #ZnodeOmsPersonalizeCartItemNew TBL_ON WHERE TBL_ON.OmsSavedCartLineItemId  = TBL_O.ParentOmsSavedCartLineItemId   FOR XML PATH ('')),2,4000) 
		,TBL_O.PersonalizeValue =  SUBSTRING((SELECT ','+TBL_ON.PersonalizeValue FROM #ZnodeOmsPersonalizeCartItemNew TBL_ON WHERE TBL_ON.OmsSavedCartLineItemId  = TBL_O.ParentOmsSavedCartLineItemId   FOR XML PATH ('')),2,4000)  
	 FROM @TBL_OmsSavedCartNew TBL_O 
	

	  UPDATE TBL_O
	 SET TBL_O.PersonalizeCode = SUBSTRING((SELECT ','+TBL_ON.PersonalizeCode FROM #ZnodeOmsPersonalizeCartItemOld TBL_ON WHERE  TBL_ON.OmsOrderLineItemsId  = TBL_O.ParentOmsSavedCartLineItemId  FOR XML PATH ('')),2,4000)
		,TBL_O.PersonalizeValue =  SUBSTRING((SELECT ','+TBL_ON.PersonalizeValue FROM #ZnodeOmsPersonalizeCartItemOld TBL_ON WHERE  TBL_ON.OmsOrderLineItemsId  = TBL_O.ParentOmsSavedCartLineItemId  FOR XML PATH ('')),2,4000) 
	 FROM @TBL_OmsSavedCartOld TBL_O 


	 UPDATE a 
	 SET   a.PersonalizeCode = ISNULL((SELECT TOP 1 PersonalizeCode FROM @TBL_OmsSavedCartOld RT WHERE RT.OmsSavedCartLineItemId = a.ParentOmsSavedCartLineItemId  ),a.PersonalizeCode)
	  ,a.PersonalizeValue = ISNULL((SELECT TOP 1 PersonalizeValue FROM @TBL_OmsSavedCartOld RT WHERE RT.OmsSavedCartLineItemId = a.ParentOmsSavedCartLineItemId  ),a.PersonalizeValue)
	  FROM @TBL_OmsSavedCartOld a 
	  WHERE a.ParentOmsSavedCartLineItemId IS NOT NULL 

	
	 UPDATE a 
	 SET   a.PersonalizeCode = ISNULL((SELECT TOP 1 PersonalizeCode FROM @TBL_OmsSavedCartNew RT WHERE RT.OmsSavedCartLineItemId = a.ParentOmsSavedCartLineItemId  ),a.PersonalizeCode)
	  ,a.PersonalizeValue = ISNULL((SELECT TOP 1 PersonalizeValue FROM @TBL_OmsSavedCartNew RT WHERE RT.OmsSavedCartLineItemId = a.ParentOmsSavedCartLineItemId  ),a.PersonalizeValue)
	  FROM @TBL_OmsSavedCartNew a 
	  WHERE a.ParentOmsSavedCartLineItemId IS NOT NULL 


	 
	 UPDATE a 
	 SET  a.Quantity =  a.Quantity+d.Quantity 
	 FROM ZnodeOmsSavedCartLineItem a 
	 INNER JOIN @TBL_OmsSavedCartNew b ON (b.OmsSavedCartLineItemId = a.OmsSavedCartLineItemId)
	 INNER JOIN @TBL_OmsSavedCartOld c ON (c.SKU = b.SKU AND c.ParentSKU = b.ParentSKU AND ISNULL(c.AddOnSKU,'-1') = ISNULL(b.AddOnSKU,'-1') AND ISNULL(c.PersonalizeCode,'-1') = ISNULL(b.PersonalizeCode,'-1') AND ISNULL(c.PersonalizeValue,'-1') = ISNULL(b.PersonalizeValue,'-1')) 
	 INNER JOIN #ZnodeOmsSavedCartLineItemOld d ON (d.OmsOrderLineItemsId = c.OmsSavedCartLineItemId)
	 


	 
	 ;WITH CTE_UpdateOrder 
	 AS 
	 (
	   SELECT Sequence,  ROW_NUMBER()Over(order BY OmsSavedCartLineItemId ASC) RowId
	   FROM ZnodeOmsSavedCartLineItem
	   WHERE  OmsSavedCartId = @OmsSavedCartId
	 
	 ) 
	 UPDATE CTE_UpdateOrder 
	 SET Sequence =RowId


	 DECLARE @DeletedId TABLE (OmsSavedCartLineItemId INT )

	
	 DELETE  FROM #ZnodeOmsOrderLineItems_temp OUTPUT DELETED.OmsOrderLineItemsId INTO @DeletedId WHERE OmsOrderLineItemsId IN (SELECT c.OmsSavedCartLineItemId FROM ZnodeOmsSavedCartLineItem a 
	 INNER JOIN @TBL_OmsSavedCartNew b ON (b.OmsSavedCartLineItemId = a.OmsSavedCartLineItemId)
	 INNER JOIN @TBL_OmsSavedCartOld c ON (c.SKU = b.SKU AND c.ParentSKU = b.ParentSKU AND ISNULL(c.AddOnSKU,'-1') = ISNULL(b.AddOnSKU,'-1') AND ISNULL(c.PersonalizeCode,'-1') = ISNULL(b.PersonalizeCode,'-1') AND ISNULL(c.PersonalizeValue,'-1') = ISNULL(b.PersonalizeValue,'-1')) 
	 INNER JOIN #ZnodeOmsSavedCartLineItemOld d ON (d.OmsOrderLineItemsId = c.OmsSavedCartLineItemId))


	 DELETE FROM #ZnodeOmsOrderLineItems_temp WHERE ParentOmsOrderLineItemsId IN (SELECT OmsSavedCartLineItemId FROM @DeletedId)

	 DELETE TR FROM #ZnodeOmsOrderLineItems_temp TR WHERE NOT EXISTS (SELECT TOP 1 1 FROM #ZnodeOmsOrderLineItems_temp YU WHERE TR.OmsOrderLineItemsId = YU.ParentOmsOrderLineItemsId  ) 
	 AND TR.ParentOmsOrderLineItemsId IS NULL 



   UPDATE #ZnodeOmsOrderLineItems_temp 
  SET ParentRowId = (SELECT TOP 1 RowId FROM #ZnodeOmsOrderLineItems_temp a WHERE a.OmsOrderLineItemsId = #ZnodeOmsOrderLineItems_temp.ParentOmsOrderLineItemsId ) 

  


  INSERT INTO ZnodeOmsSavedCartLineItem (ParentOmsSavedCartLineItemId,OmsSavedCartId,SKU,Quantity,OrderLineItemRelationshipTypeId,CustomText,CartAddOnDetails
										,Sequence,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,AutoAddon,OmsOrderId,Custom1,Custom2,Custom3,Custom4,Custom5,GroupId
										,ProductName,Description)
  OUTPUT INSERTED.OmsSavedCartLineItemId,INSERTED.Sequence  INTO @TBL_ZnodeOmsSavedCartLineItem
  SELECT NULL,@OmsSavedCartId,SKU,Quantity,OrderLineItemRelationshipTypeId,NULL CustomText, NULL CartAddOnDetails
										,RowId,@UserId,@GetDate,@UserId,@GetDate,AutoAddonSKU,NULL OmsOrderId,Custom1,Custom2,Custom3,Custom4,Custom5,GroupId
										,ProductName,Description
  FROM #ZnodeOmsOrderLineItems_temp 
  ORDER BY OmsOrderLineItemsId 

            
  UPDATE ab 
  SET ab.ParentOmsSavedCartLineItemId = (SELECT TOP 1 OmsSavedCartLineItemId FROM @TBL_ZnodeOmsSavedCartLineItem av WHERE av.RowId = b.ParentRowId   )
  FROM ZnodeOmsSavedCartLineItem ab 
  INNER JOIN @TBL_ZnodeOmsSavedCartLineItem a ON (a.OmsSavedCartLineItemId = ab.OmsSavedCartLineItemId ) 
  INNER JOIN #ZnodeOmsOrderLineItems_temp b ON  (b.RowId = a.RowId  ) 

  INSERT INTO ZnodeOmsPersonalizeCartItem (OmsSavedCartLineItemId,PersonalizeCode,PersonalizeValue,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,DesignId,ThumbnailURL)
  SELECT c.OmsSavedCartLineItemId,PersonalizeCode,PersonalizeValue,@UserId,@GetDate,@UserId,@GetDate,DesignId,ThumbnailURL
  FROM ZnodeOmsPersonalizeItem  a 
  INNER JOIN #ZnodeOmsOrderLineItems_temp b ON (a.OmsOrderLineItemsId = b.OmsOrderLineItemsId)
  INNER JOIN @TBL_ZnodeOmsSavedCartLineItem c ON (c.RowId = b.RowId )

  
  UPDATE ZnodeOmsSavedCart
  SET ModifiedDate = GETDATE()
  WHERE OmsSavedCartId = @OmsSavedCartId


   SET @status = 1 

   
 END TRY 
 BEGIN CATCH 
 
 SELECT ERROR_MESSAGE()
 SET @status = 0 
 END CATCH 
END