﻿ CREATE PROCEDURE [dbo].[Znode_DeletePimCatalogProducts](
       @ProfileCatalogId INT = NULL ,
       @PimCatalogId     INT ,
       @PimCategoryId    INT ,
       @PimProductId     VARCHAR(2000) ,
	   @PimCategoryHierarchyId INT =0 ,
       @Status           BIT OUT 
	
	   )
AS
   
/*
     Summary : Remove PimCatalog Category data from ZnodePimCatalogCategory, remove corresponding records from ZnodeProfileCatalogCategory  
   
     Unit Testing 
     Begin transaction 
	 Select * from ZnodeProfileCatalogCategory where ProfileCatalogCategoryId in (5898,5899)
	 Declare @Status bit 
     EXEC Znode_DeletePimCatalogProducts 
														@ProfileCatalogId  =415,
                                                        @PimCatalogId     =16,
                                                        @PimCategoryId    =11,
                                                        @PimProductId    = '137,138',
                                                        @Status =@Status OUT 
     SELECT @Status 
	 Select * from ZnodeProfileCatalogCategory where ProfileCatalogCategoryId in (5898,5899)
	 Rollback transaction 
  */
 BEGIN
         BEGIN TRAN DeletePimCatalogProducts;
         BEGIN TRY
             SET NOCOUNT ON;
			  -- table used to hold the CMSContentPageGroupId 
             DECLARE @TBL_DeleteProfileCatalogCategory TABLE (
                                                             ProfileCatalogCategoryId INT
                                                             );
			-- table used to hold the CMSContentPageGroupId 
             DECLARE @TBL_DeletePimCatalogCategory TABLE (
                                                         PimCatalogCategoryId INT
                                                         ); 

             INSERT INTO @TBL_DeletePimCatalogCategory
                    SELECT PimCatalogCategoryId
                    FROM ZnodePimCatalogCategory
                    WHERE PimCatalogId = @PimCatalogId AND PimCategoryId = @PimCategoryId  AND PimCategoryHierarchyId =@PimCategoryHierarchyId
					AND PimProductId IN ( SELECT item
                                            FROM dbo.Split ( @PimProductId , ','));
								 
             IF ISNULL(@ProfileCatalogId , 0) = 0
                 BEGIN
			  	 INSERT INTO @TBL_DeleteProfileCatalogCategory
                            SELECT zpcc.ProfileCatalogCategoryId
                            FROM ZnodeProfileCatalogCategory AS zpcc
                            WHERE EXISTS ( SELECT TOP 1 1
                                           FROM @TBL_DeletePimCatalogCategory AS dpcc
                                           WHERE dpcc.PimCatalogCategoryId = zpcc.PimCatalogCategoryId
                                         );
                 END;
             ELSE
                 BEGIN
			
                     INSERT INTO @TBL_DeleteProfileCatalogCategory
                            SELECT zpcc.ProfileCatalogCategoryId
                            FROM ZnodeProfileCatalogCategory AS zpcc
                            WHERE zpcc.ProfileCatalogId = @ProfileCatalogId
                                  AND
                                  EXISTS ( SELECT TOP 1 1
                                           FROM @TBL_DeletePimCatalogCategory AS dpcc
                                           WHERE dpcc.PimCatalogCategoryId = zpcc.PimCatalogCategoryId
                                         );
														 
                 END;
             DELETE FROM ZnodeProfileCatalogCategory
             WHERE EXISTS ( SELECT TOP 1 1
                            FROM @TBL_DeleteProfileCatalogCategory AS DPCC
                            WHERE DPCC.ProfileCatalogCategoryId = ZnodeProfileCatalogCategory.ProfileCatalogCategoryId
                          );

				
             IF ISNULL(@ProfileCatalogId , 0) = 0
                 BEGIN
                     DELETE FROM ZnodePimCatalogCategory
                     WHERE EXISTS ( SELECT TOP 1 1
                                    FROM @TBL_DeletePimCatalogCategory AS zpcc
                                    WHERE zpcc.PimCatalogCategoryId = ZnodePimCatalogCategory.PimCatalogCategoryId
                                  );
                 END;
             IF ( SELECT COUNT(1) FROM @TBL_DeletePimCatalogCategory) = ( SELECT COUNT(1)
                      FROM dbo.Split ( @PimProductId , ','
                                     )
                    ) -- if both count are same then data set status return true other wise false 
                 BEGIN
                     SELECT 1 AS ID , CAST(1 AS BIT) AS [Status];
                     SET @Status = 1;
                 END;
             ELSE
                 BEGIN
                     SELECT 0 AS ID , CAST(0 AS BIT) AS [Status];
                     SET @Status = 0;
                 END;
             COMMIT TRAN DeletePimCatalogProducts;
         END TRY
         BEGIN CATCH
             DECLARE @Error_procedure VARCHAR(1000)= ERROR_PROCEDURE() , @ErrorMessage NVARCHAR(MAX)= ERROR_MESSAGE() , @ErrorLine VARCHAR(100)= ERROR_LINE() , 
			 @ErrorCall NVARCHAR(MAX)= 'EXEC Znode_DeletePimCatalogCategory @ProfileCatalogId = '+CONVERT(VARCHAR(100) , @ProfileCatalogId)+',@PimCatalogId = '+CONVERT(VARCHAR(100) , @PimCatalogId)+' ,@PimCategoryId =  '+CONVERT(VARCHAR(100) , @PimCategoryId)+',@PimProductId = '+@PimProductId+',@Status='+CAST(@Status AS VARCHAR(50));
             SET @Status = 0;
             SELECT 0 AS ID , CAST(0 AS BIT) AS [Status];
             ROLLBACK TRAN DeletePimCatalogProducts;
             EXEC Znode_InsertProcedureErrorLog
			  @ProcedureName = 'Znode_DeletePimCatalogCategory' ,
			  @ErrorInProcedure = @Error_procedure ,
			  @ErrorMessage = @ErrorMessage ,
			  @ErrorLine = @ErrorLine ,
			  @ErrorCall = @ErrorCall;
         END CATCH;
     END;