﻿CREATE TYPE [dbo].[SearchRuleTriggerDetail] AS TABLE (
    [SearchTriggerKeyword]   VARCHAR (100) NULL,
    [SearchTriggerCondition] VARCHAR (50)  NULL,
    [SearchTriggerValue]     VARCHAR (600) NULL,
    [SearchCatalogRuleId]    INT           NULL,
    [SearchTriggerRuleId]    INT           NULL);

