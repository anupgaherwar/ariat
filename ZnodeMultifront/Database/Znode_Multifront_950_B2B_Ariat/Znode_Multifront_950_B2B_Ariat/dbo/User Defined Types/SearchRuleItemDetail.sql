﻿CREATE TYPE [dbo].[SearchRuleItemDetail] AS TABLE (
    [SearchItemKeyword]    VARCHAR (100) NULL,
    [SearchItemCondition]  VARCHAR (50)  NULL,
    [SearchItemValue]      VARCHAR (600) NULL,
    [SearchItemBoostValue] VARCHAR (10)  NULL,
    [SearchCatalogRuleId]  INT           NULL,
    [SearchItemRuleId]     INT           NULL);

