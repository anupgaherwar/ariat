﻿CREATE TYPE [dbo].[RecommendationProcessedData] AS TABLE (
    [RecommendationBaseProductsId]   BIGINT          NULL,
    [BaseSKU]                        NVARCHAR (600)  NULL,
    [PortalID]                       INT             NULL,
    [RecommendationProcessingLogsId] INT             NULL,
    [RecommendedProductsId]          BIGINT          NULL,
    [RecommendedSKU]                 NVARCHAR (600)  NULL,
    [Quantity]                       NUMERIC (28, 6) NULL,
    [Occurrence]                     INT             NULL);

