﻿CREATE PROCEDURE [dbo].[Znode_RecommendationProcessedData] 
(
	@UserID int = 2,
	@TableName varchar(500),
	@ProcessingTimeLimit int,
	@Status bit out
)
--Exec [Znode_RecommendationProcessedData] @TableName = '[##RecommendationData_1810e12a-cbed-4ceb-903c-0640f15c1e78]'
--,@ProcessingTimeLimit = 1000,@Status=0
as
begin

	begin tran
	set nocount on;
	declare @getdate datetime= getdate()
	
	if OBJECT_ID ('tempdb..#RecommendationProcessedData') is not null
		drop table #RecommendationProcessedData

	CREATE TABLE #RecommendationProcessedData(
	    ID int Primary Key identity,
		[RecommendationBaseProductsId] [bigint] NULL,
		[BaseSKU] [nvarchar](600) NULL,
		[PortalID] [int] NULL,
		[RecommendationProcessingLogsId] [int] NULL,
		[RecommendedProductsId] [bigint] NULL,
		[RecommendedSKU] [nvarchar](600) NULL,
		[Quantity] [numeric](28, 6) NULL,
		[Occurrence] [int] NULL
	)

	DECLARE @SQL VARCHAR(MAX)

	SET @SQL = '
	SELECT [RecommendationBaseProductsId],[BaseSKU],[PortalID],[RecommendationProcessingLogsId],
	       [RecommendedProductsId],[RecommendedSKU],[Quantity],[Occurrence]   FROM '+@TableName
	
	INSERT INTO #RecommendationProcessedData
	EXEC (@SQL)

	DECLARE @MaxCount INT, @MinRow INT, @MaxRow INT, @Rows numeric(10,2),@RowId int = 1, @ProcessTimeInLoop int = 0;
	SELECT @MaxCount = COUNT(*) FROM #RecommendationProcessedData 

	SELECT @Rows = 20000
        
	SELECT @MaxCount = CEILING(@MaxCount / @Rows);

	---- To get the min and max rows for import in loop
	;WITH cte AS 
	(
		SELECT RowId = 1, 
				MinRow = 1, 
                MaxRow = cast(@Rows as int)
        UNION ALL
        SELECT RowId + 1, 
                MinRow + cast(@Rows as int), 
                MaxRow + cast(@Rows as int)
        FROM cte
        WHERE RowId + 1 <= @MaxCount
	)
    SELECT RowId, MinRow, MaxRow
    INTO #Temp_ImportLoop
    FROM cte
	option (maxrecursion 0);
	
	while ( @ProcessTimeInLoop <= @ProcessingTimeLimit and @RowId <= @MaxCount )
	begin
		select @MinRow = MinRow, @MaxRow = MaxRow from #Temp_ImportLoop where RowId = @RowId
		
		----updating RecommendationBaseProducts data
		update ZRBP set ModifiedBy = @UserID, ModifiedDate = @getdate
		from #RecommendationProcessedData RPD
		inner join ZnodeRecommendationBaseProducts ZRBP ON RPD.BaseSKU = ZRBP.SKU 
						 and ISNULL(RPD.PortalId,0) = isnull(ZRBP.PortalId,0) and RPD.RecommendationProcessingLogsId = ZRBP.RecommendationProcessingLogsId
        where RPD.Id BETWEEN @MinRow AND @MaxRow

		----Inserting RecommendationBaseProducts data against portal
		insert into ZnodeRecommendationBaseProducts (SKU,PortalId,RecommendationProcessingLogsId,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
		select distinct BaseSKU, PortalID, RecommendationProcessingLogsId, @UserID, @getdate, @UserID, @getdate 
		from #RecommendationProcessedData RPD
		where not exists(select * from ZnodeRecommendationBaseProducts ZRBP where RPD.BaseSKU = ZRBP.SKU 
						 and ISNULL(RPD.PortalId,0) = isnull(ZRBP.PortalId,0) and RPD.RecommendationProcessingLogsId = ZRBP.RecommendationProcessingLogsId)
		and isnull(RPD.RecommendationBaseProductsId,0) = 0
		and RPD.Id BETWEEN @MinRow AND @MaxRow

		----updating new inserted base SKU RecommendationBaseProductsId in temp table
		update RPD set RecommendationBaseProductsId = ZRBP.RecommendationBaseProductsId
		from #RecommendationProcessedData RPD
		inner join ZnodeRecommendationBaseProducts ZRBP ON RPD.BaseSKU = ZRBP.SKU 
						 and ISNULL(RPD.PortalId,0) = isnull(ZRBP.PortalId,0) and RPD.RecommendationProcessingLogsId = ZRBP.RecommendationProcessingLogsId
		where isnull(RPD.RecommendationBaseProductsId,0) = 0 and RPD.Id BETWEEN @MinRow AND @MaxRow	 

		----Updating RecommendedProducts data for base SKU
		update ZRBP set Quantity = RPD.Quantity, Occurrence = RPD.Occurrence,ModifiedBy = @UserID, ModifiedDate = @getdate
		from #RecommendationProcessedData RPD
		inner join ZnodeRecommendedProducts ZRBP ON RPD.RecommendedSKU = ZRBP.SKU AND RPD.RecommendationBaseProductsId = ZRBP.RecommendationBaseProductsId
		where RPD.Id BETWEEN @MinRow AND @MaxRow

		----Inserting RecommendedProducts data against base SKU
		insert into ZnodeRecommendedProducts (RecommendationBaseProductsId,SKU,Quantity,Occurrence,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
		select distinct RecommendationBaseProductsId, RecommendedSKU, Quantity, Occurrence, @UserID, @getdate, @UserID, @getdate
		from #RecommendationProcessedData RPD
		where not exists(select * from ZnodeRecommendedProducts ZRBP where RPD.RecommendedSKU = ZRBP.SKU AND RPD.RecommendationBaseProductsId = ZRBP.RecommendationBaseProductsId)
	    and RPD.Id BETWEEN @MinRow AND @MaxRow
		
		set @RowId = @RowId+1
		set @ProcessTimeInLoop = (cast(Datediff(ms, @getdate,getdate()) AS bigint))
	end


	if (@ProcessTimeInLoop>@ProcessingTimeLimit)
	begin
		rollback tran
		set @Status = 0  
		--select @Status
	end
	else 
	begin
		commit tran
		set @Status = 1
		--select @Status
	end

end