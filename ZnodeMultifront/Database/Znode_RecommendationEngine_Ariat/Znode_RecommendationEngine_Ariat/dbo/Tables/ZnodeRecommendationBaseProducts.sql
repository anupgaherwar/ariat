﻿CREATE TABLE [dbo].[ZnodeRecommendationBaseProducts] (
    [RecommendationBaseProductsId]   BIGINT         IDENTITY (1, 1) NOT NULL,
    [SKU]                            NVARCHAR (600) NOT NULL,
    [PortalId]                       INT            NULL,
    [RecommendationProcessingLogsId] INT            NOT NULL,
    [CreatedBy]                      INT            NOT NULL,
    [CreatedDate]                    DATETIME       NOT NULL,
    [ModifiedBy]                     INT            NOT NULL,
    [ModifiedDate]                   DATETIME       NOT NULL,
    PRIMARY KEY CLUSTERED ([RecommendationBaseProductsId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_ZnodeRecommendationBaseProducts_RecommendationProcessingLogsId] FOREIGN KEY ([RecommendationProcessingLogsId]) REFERENCES [dbo].[ZnodeRecommendationProcessingLogs] ([RecommendationProcessingLogsId])
);

