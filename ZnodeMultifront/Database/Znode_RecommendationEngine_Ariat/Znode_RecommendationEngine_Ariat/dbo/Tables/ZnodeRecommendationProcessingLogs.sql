﻿CREATE TABLE [dbo].[ZnodeRecommendationProcessingLogs] (
    [RecommendationProcessingLogsId] INT            IDENTITY (1, 1) NOT NULL,
    [PortalId]                       INT            NULL,
    [Status]                         NVARCHAR (600) NOT NULL,
    [LastProcessedOrderId]           INT            NOT NULL,
    [LastProcessedOrderDate]         DATETIME       NOT NULL,
    [CreatedBy]                      INT            NOT NULL,
    [CreatedDate]                    DATETIME       NOT NULL,
    [ModifiedBy]                     INT            NOT NULL,
    [ModifiedDate]                   DATETIME       NOT NULL,
    PRIMARY KEY CLUSTERED ([RecommendationProcessingLogsId] ASC) WITH (FILLFACTOR = 90)
);

