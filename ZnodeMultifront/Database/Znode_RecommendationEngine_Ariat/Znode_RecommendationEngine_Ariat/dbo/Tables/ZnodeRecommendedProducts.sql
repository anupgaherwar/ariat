﻿CREATE TABLE [dbo].[ZnodeRecommendedProducts] (
    [RecommendedProductsId]        BIGINT          IDENTITY (1, 1) NOT NULL,
    [RecommendationBaseProductsId] BIGINT          NOT NULL,
    [SKU]                          NVARCHAR (600)  NOT NULL,
    [Quantity]                     DECIMAL (28, 6) NULL,
    [Occurrence]                   INT             NOT NULL,
    [CreatedBy]                    INT             NOT NULL,
    [CreatedDate]                  DATETIME        NOT NULL,
    [ModifiedBy]                   INT             NOT NULL,
    [ModifiedDate]                 DATETIME        NOT NULL,
    PRIMARY KEY CLUSTERED ([RecommendedProductsId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_ZnodeRecommendedProducts_RecommendationBaseProductsId] FOREIGN KEY ([RecommendationBaseProductsId]) REFERENCES [dbo].[ZnodeRecommendationBaseProducts] ([RecommendationBaseProductsId])
);

